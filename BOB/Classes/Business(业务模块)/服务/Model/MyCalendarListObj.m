//
//  MyCalendarListObj.m
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyCalendarListObj.h"

@implementation MyCalendarListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

- (NSString *)formatLevel {
    if (!_formatLevel) {
        _formatLevel = @"重要";
    }
    return _formatLevel;
}

- (NSString *)remind_time {
    if (!_remind_time) {
        _remind_time = StrF(@"%@ %@",
                            [StringUtil formatDayStringEx:self.task_date],
                            [StringUtil formatTimeString:self.task_time]);
    }
    return _remind_time;
}

- (NSString *)formatCre_date {
    if (!_formatCre_date) {
        _formatCre_date = StrF(@"%@", [StringUtil formatDayString:self.cre_date unit:@"/"]);
    }
    return _formatCre_date;
}

@end
