//
//  MyCalendarListObj.h
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyCalendarListObj : BaseObject

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *title;
///0-否 1-是
@property (nonatomic, assign) BOOL is_notify;

@property (nonatomic, copy) NSString *task_date;

@property (nonatomic, copy) NSString *task_time;

@property (nonatomic, copy) NSString *cre_date;

@property (nonatomic, copy) NSString *cre_time;

@property (nonatomic, copy) NSString *remind_time;

@property (nonatomic, copy) NSString *level;
///0-否 1-是
@property (nonatomic, copy) NSString *status;

@property (nonatomic, copy) NSString *formatLevel;

@property (nonatomic, copy) NSString *formatCre_date;

@end

NS_ASSUME_NONNULL_END
