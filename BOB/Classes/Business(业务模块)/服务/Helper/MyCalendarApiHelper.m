//
//  MyCalendarApiHelper.m
//  BOB
//
//  Created by mac on 2020/7/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyCalendarApiHelper.h"

@implementation MyCalendarApiHelper

///查询服务
+ (void)getServeNotifyList:(NSString *)last_id
                  complete:(MXHttpRequestResultObjectCallBack)complete {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot, @"api/user/serve/getServeNotifyList");
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{@"last_id":last_id?:@"0"})
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                NSDictionary* dataDict = dict[@"data"];
                complete(YES,dataDict,nil);
            }else{
                complete(NO,nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            complete(NO,nil,nil);
        })
        .execute();
    }];
}

///加服务
+ (void)addServeNotify:(NSDictionary *)param
              complete:(MXHttpRequestResultObjectCallBack)complete {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot, @"api/user/serve/addServeNotify");
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                complete(YES,nil,nil);
            }else{
                complete(NO,nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            complete(NO,nil,nil);
        })
        .execute();
    }];
}

///加服务
+ (void)editServeNotify:(NSDictionary *)param
               complete:(MXHttpRequestResultObjectCallBack)complete {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot, @"api/user/serve/editServeNotify");
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                complete(YES,nil,nil);
            }else{
                complete(NO,nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            complete(NO,nil,nil);
        })
        .execute();
    }];
}

///删除服务
+ (void)delServeNotify:(NSString *)id_list
              complete:(MXHttpRequestResultObjectCallBack)complete {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot, @"api/user/serve/delServeNotify");
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{@"id_list":id_list?:@""})
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                complete(YES,nil,nil);
            }else{
                complete(NO,nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            complete(NO,nil,nil);
        })
        .execute();
    }];
}

///查询当天服务数量
+ (void)getServeNotifyNumToday:(MXHttpRequestResultObjectCallBack)complete {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot, @"api/user/serve/getServeNotifyNumToday");
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{})
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                complete(YES,dict[@"data"],nil);
            }else{
                complete(NO,nil,nil);
//                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            complete(NO,nil,nil);
        })
        .execute();
    }];
}

@end
