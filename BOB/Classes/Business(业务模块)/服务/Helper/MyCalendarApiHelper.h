//
//  MyCalendarApiHelper.h
//  BOB
//
//  Created by mac on 2020/7/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyCalendarApiHelper : NSObject

///查询服务
+ (void)getServeNotifyList:(NSString *)last_id
                  complete:(MXHttpRequestResultObjectCallBack)complete;

///增加服务
+ (void)addServeNotify:(NSDictionary *)param
              complete:(MXHttpRequestResultObjectCallBack)complete;

///编辑服务
+ (void)editServeNotify:(NSDictionary *)param
               complete:(MXHttpRequestResultObjectCallBack)complete;

///删除服务
+ (void)delServeNotify:(NSString *)id_list
              complete:(MXHttpRequestResultObjectCallBack)complete;

///查询当天服务数量
+ (void)getServeNotifyNumToday:(MXHttpRequestResultObjectCallBack)complete;

@end

NS_ASSUME_NONNULL_END
