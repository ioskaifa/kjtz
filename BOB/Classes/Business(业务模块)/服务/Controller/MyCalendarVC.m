//
//  MyCalendarVC.m
//  BOB
//
//  Created by mac on 2020/8/7.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyCalendarVC.h"
#import "MyCalendarListObj.h"
#import "MyCalendarListCell.h"
#import "MyCalendarApiHelper.h"

@interface MyCalendarVC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)  UITableView    *tableView;
@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, assign) BOOL isEdit;

@end

@implementation MyCalendarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.last_id = @"0";
    [self fetchData];
}

- (void)navBarRightBtnAction:(id)sender {
    self.isEdit = !self.isEdit;
    if (self.isEdit) {
        [self setNavBarRightBtnWithTitle:@"完成" andImageName:nil];
    } else {
        [self setNavBarRightBtnWithTitle:@"编辑" andImageName:nil];
    }
    [self.tableView reloadData];
}

- (void)fetchData {
    if ([self.last_id isEqualToString:@"0"]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [MyCalendarApiHelper getServeNotifyList:self.last_id?:@"0"
                                 complete:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSArray *dataArr = object[@"serveNotifyList"];
            dataArr = [MyCalendarListObj modelListParseWithArray:dataArr];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            MyCalendarListObj *last = [self.dataSourceMArr lastObject];
            if (last) {
                self.last_id = last.ID;
            } else {
                self.last_id = @"0";
            }
            [self.tableView reloadData];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MyCalendarListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    
    if (![cell isKindOfClass:MyCalendarListCell.class]) {
        return;
    }
    MyCalendarListObj *obj = self.dataSourceMArr[indexPath.row];
    MyCalendarListCell *listCell = (MyCalendarListCell *)cell;
    if (self.isEdit) {
        listCell.arrowImgView.visibility = MyVisibility_Visible;
    } else {
        listCell.arrowImgView.visibility = MyVisibility_Invisible;
    }
    [listCell configureView:obj];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return 0.01;
    }
    return [MyCalendarListCell viewHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (!self.isEdit) {
        return;
    }
    MyCalendarListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MyCalendarListObj.class]) {
        return;
    }
    MXRoute(@"MyCalendarManageVC", (@{@"type":@(1), @"obj":obj}));
}

#pragma mark - UITableView 左滑删除
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (indexPath.row >= self.dataSourceMArr.count) {
            return;
        }
        MyCalendarListObj *obj = self.dataSourceMArr[indexPath.row];
        if (![obj isKindOfClass:MyCalendarListObj.class]) {
            return;
        }
        [MyCalendarApiHelper delServeNotify:obj.ID
                                 complete:^(BOOL success, id object, NSString *error) {
            if (success) {
                [self.dataSourceMArr removeObject:obj];
                MyCalendarListObj *last = [self.dataSourceMArr lastObject];
                if (last) {
                    self.last_id = last.ID;
                } else {
                    self.last_id = @"0";
                }
                [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

#pragma mark - InitUI
- (void)createUI {
    self.last_id = @"0";
    [self setNavBarTitle:@"备忘录"];
    [self setNavBarRightBtnWithTitle:@"编辑" andImageName:nil];
    self.view.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:[self topView]];
    [layout addSubview:self.tableView];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.weight = 1;
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        Class cls = MyCalendarListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.tableFooterView = [UIView new];
        @weakify(self)
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self)
            self.last_id = @"0";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self)
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (MyBaseLayout *)topView {
    MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    object.size = CGSizeMake(SCREEN_WIDTH, 42);
    object.myLeft = 0;
    object.mySize = object.size;
    
    SPButton *btn = [SPButton new];
    btn.imagePosition = SPButtonImagePositionLeft;
    btn.imageTitleSpace = 5;
    btn.titleLabel.font = [UIFont font14];
    [btn setTitleColor:[UIColor colorWithHexString:@"#0088FF"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"icon_remind"] forState:UIControlStateNormal];
    [btn setTitle:@"提醒事项" forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.mySize = btn.size;
    btn.myCenterY = 0;
    btn.myLeft = 30;
    
    [object addSubview:btn];
    
    UIButton *addBtn = [UIButton new];
    [addBtn setImage:[UIImage imageNamed:@"icon_server_add"] forState:UIControlStateNormal];
    [addBtn sizeToFit];
    addBtn.mySize = addBtn.size;
    addBtn.myCenterY = 0;
    addBtn.myLeft = SCREEN_WIDTH - 60 - btn.width - addBtn.width;
    [addBtn addAction:^(UIButton *btn) {
        MXRoute(@"MyCalendarManageVC", @{@"type":@(0)});
    }];
    [object addSubview:addBtn];
    [object addBottomSingleLine:[UIColor colorWithHexString:@"#AEAEAE"]];
    return object;
}

@end
