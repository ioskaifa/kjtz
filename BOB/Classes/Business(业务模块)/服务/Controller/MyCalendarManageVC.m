//
//  MyCalendarManageVC.m
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyCalendarManageVC.h"
#import "THDatePickerView.h"
#import "MyCalendarApiHelper.h"

@interface MyCalendarManageVC ()<THDatePickerViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *headerView;

@property (nonatomic, strong) UIButton *switchBtn;

@property (nonatomic, strong) UILabel *timeLbl;

@property (nonatomic, strong) UITextField *tf;

@property (nonatomic, strong) UIButton *commitBtn;

@property (nonatomic, strong) THDatePickerView *datePicker;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@end

@implementation MyCalendarManageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    if (self.type == ServerManageTypeEdit) {
        [self configure:self.obj];
    }
}

- (void)navBarRightBtnAction:(id)sender {
    if ([self valiParam]) {
        [self editServe];
    }
}

#pragma mark - IBAction
- (void)commitBtnClick:(UIButton *)sender {
    if (self.type == ServerManageTypeAdd) {
        if ([self valiParam]) {
            [self commit];
        }
    } else {
        ///提示是否删除
        [MXAlertViewHelper showAlertViewWithMessage:@"确认删除？" completion:^(BOOL cancelled, NSInteger buttonIndex) {
           if (buttonIndex == 1) {
               [self delServer];
           }
        }];
    }
}

#pragma mark - Private Method
- (void)configure:(MyCalendarListObj *)obj {
    if (![obj isKindOfClass:MyCalendarListObj.class]) {
        return;
    }
    if (self.type != ServerManageTypeEdit) {
        return;
    }
    self.switchBtn.selected = obj.is_notify;
    self.timeLbl.text = StrF(@"选择时间  %@", obj.remind_time);
    self.tf.text = obj.title;
}

- (BOOL)valiParam {
    if ([StringUtil isEmpty:self.obj.task_date]) {
        [NotifyHelper showMessageWithMakeText:@"请选择时间"];
        return NO;
    }
    NSDate *date = [NSDate date];
    NSString *currentDate = [self.dateFormatter stringFromDate:date];
    NSString *selectDate = StrF(@"%@%@", self.obj.task_date, self.obj.task_time);
    //不能小于当前时间
    if ([currentDate compare:selectDate] == NSOrderedDescending) {
        [NotifyHelper showMessageWithMakeText:@"选择的时间不能小于当前时间"];
        return NO;
    }
    if ([StringUtil isEmpty:self.tf.text]) {
        [NotifyHelper showMessageWithMakeText:@"事项不能为空"];
        return NO;
    }
    return YES;
}

- (void)commit {
    NSDictionary *param = @{@"title":self.tf.text,
                            @"is_notify":self.switchBtn.selected ? @"1":@"0",
                            @"task_date":self.obj.task_date,
                            @"task_time":self.obj.task_time
    };
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [MyCalendarApiHelper addServeNotify:param
                             complete:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)delServer {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [MyCalendarApiHelper delServeNotify:self.obj.ID
                             complete:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)editServe {
    NSDictionary *param = @{@"title":self.tf.text,
                            @"is_notify":self.switchBtn.selected ? @"1":@"0",
                            @"task_date":self.obj.task_date,
                            @"task_time":self.obj.task_time,
                            @"record_id":self.obj.ID ? :@"0"
    };
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [MyCalendarApiHelper editServeNotify:param
                              complete:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            [NotifyHelper showMessageWithMakeText:@"修改成功"];
        }
    }];
}

#pragma mark - Delegate
- (void)datePickerViewSaveBtnClickDelegate:(NSString *)year
                                     month:(NSString *)month
                                       day:(NSString *)day
                                      hour:(NSString *)hour
                                       min:(NSString *)min {
    NSString *time = StrF(@"%@年%@月%@日 %@:%@", year, month, day, hour, min);
    self.obj.task_date = StrF(@"%@%@%@", year, month, day);
    self.obj.task_time = StrF(@"%@%@00", hour, min);
    self.timeLbl.text = StrF(@"选择时间  %@", time);
    [UIView animateWithDuration:0.3 animations:^{
        self.datePicker.frame = CGRectMake(0, self.view.height, self.view.width, 300);
    }];
}

- (void)datePickerViewCancelBtnClickDelegate {
    [UIView animateWithDuration:0.3 animations:^{
        self.datePicker.frame = CGRectMake(0, self.view.height, self.view.width, 300);
    }];
}

#pragma mark - InitUI
- (void)configureNav {
    switch (self.type) {
        case ServerManageTypeAdd: {
            [self setNavBarTitle:@"添加事项"];
            [self.commitBtn setTitle:@"保存" forState:UIControlStateNormal];
            break;
        }
        case ServerManageTypeEdit: {
            [self setNavBarTitle:@"编辑事项"];
            [self setNavBarRightBtnWithTitle:@"保存" andImageName:nil];
            [self.commitBtn setTitle:@"删除" forState:UIControlStateNormal];
            break;
        }
        default:
            [self setNavBarTitle:@"添加事项"];
            break;
    }
}

- (void)createUI {
    if (!self.obj) {
        self.obj = [MyCalendarListObj new];
    }
    [self configureNav];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.datePicker];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view);
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableHeaderView = self.headerView;
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

- (UIView *)headerView {
    if (!_headerView) {
        _headerView = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor whiteColor];
            MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            layout.myTop = 0;
            layout.myLeft = 0;
            layout.myRight = 0;
            layout.myHeight = MyLayoutSize.wrap;
            [object addSubview:layout];
            
            UILabel *lbl = [UILabel new];
            lbl.font = [UIFont font17];
            lbl.textColor = [UIColor colorWithHexString:@"#0088FF"];
            lbl.text = @"今天";
            [lbl sizeToFit];
            lbl.mySize = lbl.size;
            lbl.myLeft = 20;
            lbl.myTop = 10;
            [layout addSubview:lbl];
            
            MyLinearLayout *switchLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            switchLayout.myLeft = 20;
            switchLayout.myRight = 20;
            switchLayout.myHeight = 40;
            lbl = [UILabel new];
            lbl.font = [UIFont font14];
            lbl.textColor = [UIColor blackColor];
            lbl.text = @"在指定日期提醒我";
            [lbl sizeToFit];
            lbl.mySize = lbl.size;
            lbl.myCenterY = 0;
            [switchLayout addSubview:lbl];
            UIView *view = [UIView new];
            view.weight = 1;
            view.myCenterY = 0;
            view.myHeight = 1;
            [switchLayout addSubview:view];
            [switchLayout addSubview:self.switchBtn];
            [layout addSubview:switchLayout];
            [layout addSubview:[UIView line]];
            
            MyLinearLayout *dateLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            dateLayout.size = CGSizeMake(SCREEN_WIDTH - 40, 40);
            dateLayout.mySize = dateLayout.size;
            dateLayout.myLeft = 20;
            [dateLayout addSubview:self.timeLbl];
            UIImageView *imgView = [UIImageView new];
            imgView.image = [UIImage imageNamed:@"icon_server_date"];
            [imgView sizeToFit];
            imgView.mySize = imgView.size;
            imgView.myCenterY = 0;
            [dateLayout addSubview:self.timeLbl];
            [dateLayout addSubview:imgView];
            @weakify(self)
            [dateLayout addAction:^(UIView *view) {
                @strongify(self)
                [UIView animateWithDuration:0.3 animations:^{
                    self.datePicker.frame = CGRectMake(0, self.view.height - 300, self.view.width, 300);
                    [self.datePicker show];
                }];
            }];
            [layout addSubview:dateLayout];
            [layout addSubview:[UIView line]];
            
            MyLinearLayout *titleLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            titleLayout.size = CGSizeMake(SCREEN_WIDTH - 40, 40);
            titleLayout.mySize = titleLayout.size;
            titleLayout.myLeft = 20;
            lbl = [UILabel new];
            lbl.font = [UIFont font14];
            lbl.textColor = [UIColor blackColor];
            lbl.text = @"事项";
            [lbl sizeToFit];
            lbl.mySize = lbl.size;
            lbl.myCenterY = 0;
            lbl.myRight = 10;
            [titleLayout addSubview:lbl];
            [titleLayout addSubview:self.tf];
            [layout addSubview:titleLayout];
            [layout addSubview:self.commitBtn];
            
            [layout layoutSubviews];
            object.size = layout.size;
            
            object;
        });
    }
    return _headerView;
}

- (UIButton *)switchBtn {
    if (!_switchBtn) {
        UIButton *btn = [UIButton new];
        [btn setImage:[UIImage imageNamed:@"icon_switch_on"] forState:UIControlStateSelected];
        [btn setImage:[UIImage imageNamed:@"icon_switch_off"] forState:UIControlStateNormal];
        btn.selected = YES;
        [btn addAction:^(UIButton *btn) {
            btn.selected = !btn.selected;
        }];
        [btn sizeToFit];
        btn.mySize = btn.size;
        btn.myCenterY = 0;;
        _switchBtn = btn;
    }
    return _switchBtn;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font14];
            object.textColor = [UIColor blackColor];
            object.weight = 1;
            object.text = @"选择时间";
            object.myCenterY = 0;
            object.myHeight = 20;
            object;
        });
    }
    return _timeLbl;
}

-(UITextField* )tf{
    if (!_tf) {
        _tf = [UITextField new];
        _tf.placeholder = @"请输入事项";
        _tf.textColor = [UIColor blackColor];
        [_tf setFont:[UIFont font15]];
        _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
        _tf.myCenterY = 0;
        _tf.weight = 1;
        _tf.myHeight = 30;
    }
    return _tf;
}

- (UIButton *)commitBtn {
    if (!_commitBtn) {
        _commitBtn = [UIButton new];
        _commitBtn.titleLabel.font = [UIFont font15];
        _commitBtn.backgroundColor = [UIColor colorWithHexString:@"#0088FF"];
        _commitBtn.size = CGSizeMake(SCREEN_WIDTH - 30, 34);
        _commitBtn.mySize = _commitBtn.size;
        [_commitBtn setViewCornerRadius:_commitBtn.height/2.0];
        _commitBtn.myTop = 50;
        _commitBtn.myLeft = 15;
        @weakify(self)
        [_commitBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self commitBtnClick:btn];
        }];
    }
    return _commitBtn;
}

- (THDatePickerView *)datePicker {
    if (!_datePicker) {
        _datePicker = [[THDatePickerView alloc] initWithFrame:CGRectMake(0, self.view.height, self.view.width, 300)];
        _datePicker.delegate = self;
        _datePicker.isSlide = NO;
        _datePicker.title = @"请选择时间";
    }
    return _datePicker;
}

- (NSDateFormatter *)dateFormatter {
    if (!_dateFormatter) {
        _dateFormatter = [NSDateFormatter new];
        [_dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    }
    return _dateFormatter;
}

@end
