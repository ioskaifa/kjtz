//
//  MyCalendarManageVC.h
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "MyCalendarListObj.h"

typedef NS_ENUM(NSInteger, ServerManageType) {
    ///新增
    ServerManageTypeAdd,
    ///编辑
    ServerManageTypeEdit
};

NS_ASSUME_NONNULL_BEGIN

@interface MyCalendarManageVC : BaseViewController

@property (nonatomic, assign) ServerManageType type;

@property (nonatomic, strong) MyCalendarListObj *obj;

@end

NS_ASSUME_NONNULL_END
