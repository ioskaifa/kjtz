//
//  MyServerVC.m
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyServerVC.h"
#import "MyServerListCell.h"
#import "MainViewController.h"

@interface MyServerVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@property (nonatomic, strong) MainViewController *mallVC;

@end

@implementation MyServerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MyServerListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    
    if (![cell isKindOfClass:UITableViewCell.class]) {
        return;
    }
    if (![cell isKindOfClass:MyServerListCell.class]) {
        return;
    }
    NSDictionary *dataDic = self.dataSourceArr[indexPath.row];
    MyServerListCell *listCell = (MyServerListCell *)cell;
    [listCell configureView:dataDic];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return 0.01;
    }
    return [MyServerListCell viewHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    NSDictionary *dataDic = self.dataSourceArr[indexPath.row];
    if (![dataDic isKindOfClass:NSDictionary.class]) {
        return;
    }
    if ([StringUtil isEmpty:dataDic[ServerVC]]) {
        [NotifyHelper showMessageWithMakeText:@"暂未开放"];
        return;
    }
    if ([@"商城" isEqualToString:[dataDic[ServerTitle] description]]) {
        [self presentViewController:self.mallVC animated:YES completion:nil];
        return;
    }
    if ([@"数字系统" isEqualToString:[dataDic[ServerTitle] description]]) {
        MXRoute(@"MBWebVC", (@{@"url":[dataDic[ServerVC] description], @"navTitle":@"数字系统"}));
        return;
    }
    MXRoute([dataDic[ServerVC] description], nil);
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"发现"];
    self.view.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.tableView];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.weight = 1;
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        Class cls = MyServerListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

- (NSArray *)dataSourceArr {
    if (!_dataSourceArr) {
        NSDictionary *liveDic = @{ServerLeftImgName:@"直播", ServerTitle:@"直播", ServerVC:@"LiveVC"};
        NSDictionary *mallDic = @{ServerLeftImgName:@"商城", ServerTitle:@"商城", ServerVC:@"MainViewController"};
        NSDictionary *cslaDic = @{ServerLeftImgName:@"CSLA", ServerTitle:@"CSLA", ServerVC:@"https://ibd.qbccwebsites.com/"};
        _dataSourceArr = @[mallDic,liveDic,cslaDic];
    }
    return _dataSourceArr;
}

@end
