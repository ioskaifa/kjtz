//
//  MyCalendarListCell.m
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyCalendarListCell.h"

@interface MyCalendarListCell ()

@property (nonatomic, strong) UIView *flagView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *timeLbl;

@property (nonatomic, strong) SPButton *remindBtn;

@property (nonatomic, strong) UILabel *levelLbl;

@end

@implementation MyCalendarListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
        [self.contentView addBottomSingleLine:[UIColor moBackground]];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 70;
}

- (void)configureView:(MyCalendarListObj *)obj {
    if (![obj isKindOfClass:MyCalendarListObj.class]) {
        return;
    }
    self.titleLbl.text = obj.title;
    self.timeLbl.text = obj.formatCre_date;
    [self.remindBtn setTitle:obj.remind_time forState:UIControlStateNormal];
    [self.remindBtn sizeToFit];
    self.remindBtn.mySize = self.remindBtn.size;
    self.levelLbl.text = obj.formatLevel;
}

#pragma mark - InitUI
- (void)createUI {
    self.contentView.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.flagView];
    
    MyLinearLayout *centerLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    centerLayout.myCenterY = 0;
    centerLayout.weight = 1;
    centerLayout.myHeight = MyLayoutSize.wrap;
    
    [centerLayout addSubview:self.titleLbl];
    [centerLayout addSubview:self.remindBtn];
    [layout addSubview:centerLayout];
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myWidth = self.timeLbl.width;
    rightLayout.myCenterY = 0;
    rightLayout.myHeight = MyLayoutSize.wrap;
    rightLayout.myRight = 10;
    
    [rightLayout addSubview:self.timeLbl];
    [rightLayout addSubview:self.levelLbl];
    [layout addSubview:rightLayout];
    
    [layout addSubview:self.arrowImgView];
}

- (UIView *)flagView {
    if (!_flagView) {
        _flagView = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor redColor];
            object.size = CGSizeMake(6, 6);
            [object setViewCornerRadius:object.size.height/2.0];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 5;
            object.myRight = 10;
            object;
        });
    }
    return _flagView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font14];
            object.textColor = [UIColor blackColor];
            object.myHeight = 16;
            object.myWidth = MyLayoutSize.fill;
            object.myHeight = 20;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = ({
            UILabel *object = [UILabel new];
            object.textAlignment = NSTextAlignmentRight;
            object.font = [UIFont font12];
            object.textColor = [UIColor blackColor];
            object.size = CGSizeMake(100, 20);
            object.mySize = object.size;
            object;
        });
    }
    return _timeLbl;
}

- (UIImageView *)arrowImgView {
    if (!_arrowImgView) {
        _arrowImgView = ({
            UIImageView *object = [UIImageView new];
            object.visibility = MyVisibility_Invisible;
            object.image = [UIImage imageNamed:@"Arrow"];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myRight = 15;
            object;
        });
    }
    return _arrowImgView;
}

- (UILabel *)levelLbl {
    if (!_levelLbl) {
        _levelLbl = ({
            UILabel *object = [UILabel new];
            object.textAlignment = NSTextAlignmentCenter;
            object.textColor = [UIColor redColor];
            object.layer.borderColor = [UIColor redColor].CGColor;
            object.font = [UIFont font12];
            object.size = CGSizeMake(40, 20);
            object.myTop = 15;
            object.layer.borderWidth = 1;
            object.myLeft = self.timeLbl.width - object.width;
            object;
        });
    }
    return _levelLbl;
}

- (SPButton *)remindBtn {
    if (!_remindBtn) {
        _remindBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionLeft;
            object.imageTitleSpace = 5;
            object.titleLabel.font = [UIFont font11];
            [object setTitleColor:[UIColor colorWithHexString:@"#AEAEAE"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"icon_server_time"] forState:UIControlStateNormal];
            object.myTop = 10;
            object;
        });
    }
    return _remindBtn;
}

@end
