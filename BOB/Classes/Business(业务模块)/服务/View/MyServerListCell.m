//
//  MyServerListCell.m
//  BOB
//
//  Created by mac on 2020/8/7.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyServerListCell.h"

@interface MyServerListCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UIView *bottomLine;

@end

@implementation MyServerListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 50;
}

- (void)configureView:(NSDictionary *)obj {
    if (![obj isKindOfClass:NSDictionary.class]) {
        return;
    }
    if (obj[ServerLeftImgName]) {
        self.imgView.image = [UIImage imageNamed:obj[ServerLeftImgName]];
        [self.imgView sizeToFit];
        self.imgView.mySize = self.imgView.size;
    }
    
    if (obj[ServerTitle]) {
        self.titleLbl.text = [obj[ServerTitle] description];
        [self.titleLbl sizeToFit];
        self.titleLbl.mySize = self.titleLbl.size;
    }
}

#pragma mark - InitUI
- (void)createUI {
    self.contentView.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.imgView];
    [layout addSubview:self.titleLbl];
    
    UIView *view = [UIView new];
    view.weight = 1;
    view.myCenterY = 0;
    view.myHeight = 1;
    [layout addSubview:view];
    
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"Arrow"];
    [imgView sizeToFit];
    imgView.myRight = 15;
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    [layout addSubview:imgView];
    
    [self.contentView addSubview:self.bottomLine];
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLbl.mas_left);
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(self.contentView.mas_right);
        make.top.mas_equalTo(self.contentView.mas_bottom);
    }];
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.myCenterY = 0;
            object.myLeft = 25;
            object.myRight = 10;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor blackColor];
            object.myRight = 10;
            object.myCenterY = 0;
            object;
        });
    }
    return _titleLbl;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine = [UIView new];
        _bottomLine.backgroundColor = [UIColor moBackground];
    }
    return _bottomLine;
}

@end
