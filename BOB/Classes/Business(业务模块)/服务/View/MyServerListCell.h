//
//  MyServerListCell.h
//  BOB
//
//  Created by mac on 2020/8/7.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

static NSString * const ServerLeftImgName = @"ServerLeftImgName";

static NSString * const ServerTitle = @"ServerTitle";

static NSString * const ServerVC = @"ServerVC";

@interface MyServerListCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(NSDictionary *)obj;

@end

NS_ASSUME_NONNULL_END
