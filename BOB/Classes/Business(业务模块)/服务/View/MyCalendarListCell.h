//
//  MyCalendarListCell.h
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCalendarListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyCalendarListCell : UITableViewCell

@property (nonatomic, strong) UIImageView *arrowImgView;

+ (CGFloat)viewHeight;

- (void)configureView:(MyCalendarListObj *)obj;

@end

NS_ASSUME_NONNULL_END
