//
//  withdrawalRecordModel.m
//  BOB
//
//  Created by mac on 2019/7/12.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "WithdrawalRecordModel.h"
#import "NSDate+String.h"
#import "StringUtil.h"
@implementation WithdrawalRecordModel

-(NSString*)sectionTime{
    if (![StringUtil isEmpty:_sectionTime]) {
        return _sectionTime;
    }
    if ([StringUtil isEmpty:_cre_datetime]) {
        return @"";
    }
    _sectionTime = [NSDate dateString:self.cre_datetime format:@"yyyy-MM"];
    return _sectionTime;
}

+(NSString *)getRecordKeys:(NSMutableArray*)record{
    __block NSString* keyStr = @"";
    [record enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        WithdrawalRecordModel* model = (WithdrawalRecordModel*)obj;
        NSString* key = model.sectionTime;
        NSInteger location = [keyStr rangeOfString:key].location;
        if (location == NSNotFound) {
           keyStr = [keyStr stringByAppendingString:key];
           keyStr = [keyStr stringByAppendingString:@"|"];
        }
    }];
    return [keyStr substringToIndex:keyStr.length-1];
}

+(NSMutableArray*)getSequRecords:(NSMutableArray*)record keys:(NSString* )keys{
    NSMutableArray* recordsArray = [NSMutableArray arrayWithCapacity:10];
    NSArray* keyArray = [keys componentsSeparatedByString:@"|"];
    for (int i=0; i<keyArray.count; i++) {
        [recordsArray addObject: [NSMutableArray arrayWithCapacity:10]];
    }
    [record enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        WithdrawalRecordModel* model = (WithdrawalRecordModel*)obj;
        NSString* key = model.sectionTime;
        NSInteger location = [keys rangeOfString:key].location;
        if (location != NSNotFound) {
            NSInteger index = [keyArray indexOfObject:key];
            NSMutableArray* tmpArray= recordsArray[index];
            [tmpArray addObject:model];
        }
        
    }];
    return recordsArray;
}

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"hashCode" : @"hash"};
}
@end
