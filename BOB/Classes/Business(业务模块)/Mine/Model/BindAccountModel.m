//
//  BindAccountModel.m
//  BOB
//
//  Created by mac on 2019/7/15.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BindAccountModel.h"

@implementation BindAccountModel

-(id)init{
    if (self = [super init]) {
        self.img_id = @"";
        self.img_io = @"";
    }
    return self;
}

-(UIImage*)ioImage{
    if(self.img_io == nil) {
        return nil;
    }
    NSData * decodeData = [[NSData alloc] initWithBase64EncodedString:self.img_io options:(NSDataBase64DecodingIgnoreUnknownCharacters)];
    if (decodeData) {
        UIImage *decodedImage = [UIImage imageWithData:decodeData];
        if (decodedImage) {
            return decodedImage;
        }
    }
    
    return nil;
}
@end
