//
//  MessageCenterModel.m
//  BOB
//
//  Created by mac on 2019/7/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MessageCenterModel.h"

@implementation MessageCenterModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"_id" : @"id"};
}
@end
