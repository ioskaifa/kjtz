//
//  withdrawalRecordModel.h
//  BOB
//
//  Created by mac on 2019/7/12.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawalRecordModel : BaseObject

@property (nonatomic ,copy) NSString* id;
//订单号
@property (nonatomic ,copy) NSString* order_id;
//提币地址
@property (nonatomic ,copy) NSString* address;
//充币数量
@property (nonatomic ,copy) NSString* number;
//提币数量
@property (nonatomic ,copy) NSString* account;
//状态
@property (nonatomic ,copy) NSString* status;
//Hash凭证
@property (nonatomic ,copy) NSString* hashCode;
//用户编号
@property (nonatomic ,copy) NSString* user_id;
//手续费
@property (nonatomic ,copy) NSString* charge;
//时间
@property (nonatomic ,copy) NSString* cre_date;
//创建时间
@property (nonatomic ,copy) NSString* cre_datetime;
//更新时间
@property (nonatomic ,copy) NSString* up_datetime;

@property (nonatomic ,copy) NSString* sectionTime;


+(NSString*)getRecordKeys:(NSMutableArray*)record;

+(NSMutableArray*)getSequRecords:(NSMutableArray*)record keys:(NSString* )keys;

@end

NS_ASSUME_NONNULL_END
