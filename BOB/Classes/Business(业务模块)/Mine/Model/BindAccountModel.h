//
//  BindAccountModel.h
//  BOB
//
//  Created by mac on 2019/7/15.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BindAccountModel : NSObject

@property (nonatomic ,copy) NSString* type;

@property (nonatomic ,copy) NSString* account;

@property (nonatomic ,copy) NSString* account_name;

@property (nonatomic ,copy) NSString* bank_name;

@property (nonatomic ,copy) NSString* bank_branch_name;

@property (nonatomic ,copy) NSString* sms_code;

@property (nonatomic ,copy) NSString* img_code;

@property (nonatomic ,copy) NSString* img_id;

@property (nonatomic ,copy) NSString* img_io;

-(UIImage*)ioImage;

@end

NS_ASSUME_NONNULL_END
