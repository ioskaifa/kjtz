//
//  WalletCategoryModel.m
//  BOB
//
//  Created by mac on 2020/1/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletCategoryModel.h"

@implementation WalletCategoryModel
// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    NSString *priceStr = [NSString stringWithFormat:@"1%@≈%@",[dic valueForKey:@"name"],[dic valueForKey:@"price"]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:@[[dic valueForKey:@"name"],priceStr] colors:@[[UIColor colorWithHexString:@"#1F1F1F"],[UIColor colorWithHexString:@"#666666"]] fonts:@[[UIFont boldSystemFontOfSize:15],[UIFont systemFontOfSize:11]] placeHolder:@"\n"];
    [att setPerLineSpacing:12];
    self.text = att;
    
    NSMutableAttributedString *attStr = [NSMutableAttributedString initWithTitles:@[[dic valueForKey:@"available"],[NSString stringWithFormat:@"≈%@",[dic valueForKey:@"amount"]]] colors:@[[UIColor colorWithHexString:@"#253FD8"],[UIColor colorWithHexString:@"#666666"]] fonts:@[[UIFont systemFontOfSize:15],[UIFont systemFontOfSize:11]] placeHolder:@"\n"];
    
    [attStr addAlignment:NSTextAlignmentRight substring:attStr.string];
    [attStr setLineSpacing:12 substring:[dic valueForKey:@"available"] alignment:NSTextAlignmentRight];
    self.detailText = attStr;
    
    self.textNumberOfLines = 2;
    self.detailTextNumberOfLines = 2;
    return YES;
}
@end
