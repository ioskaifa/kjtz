//
//  WalletFlowModel.m
//  BOB
//
//  Created by mac on 2020/1/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletFlowModel.h"

@implementation WalletFlowModel
// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:@[[dic valueForKey:@"remark"],[dic valueForKey:@"create_time"]] colors:@[[UIColor colorWithHexString:@"#1F1F1F"],[UIColor colorWithHexString:@"#666666"]] fonts:@[[UIFont boldSystemFontOfSize:15],[UIFont systemFontOfSize:11]] placeHolder:@"\n"];
    [att setPerLineSpacing:12];
    self.text = att;
    
    NSString *price = [NSString stringWithFormat:@"%@%.7f",[dic valueForKey:@"state"],[[dic valueForKey:@"val"] doubleValue]];
    NSMutableAttributedString *attStr = [NSMutableAttributedString initWithTitles:@[price,[dic valueForKey:@"type"]] colors:@[([[dic valueForKey:@"state"] isEqualToString:@"+"] ? [UIColor colorWithHexString:@"#FB5553"] : [UIColor blackColor]),[UIColor colorWithHexString:@"#666666"]] fonts:@[[UIFont systemFontOfSize:15],[UIFont systemFontOfSize:11]] placeHolder:@"\n"];
    
    [attStr addAlignment:NSTextAlignmentRight substring:attStr.string];
    [attStr setLineSpacing:12 substring:price alignment:NSTextAlignmentRight];
    self.detailText = attStr;
    
    self.textNumberOfLines = 2;
    self.detailTextNumberOfLines = 2;
    return YES;
}
@end
