//
//  AuthManageListObj.m
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AuthManageListObj.h"

@implementation AuthManageListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"
    };
}

- (NSString *)head_photo {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _head_photo);
}

- (NSString *)cre_date {
    return [StringUtil formatDayString:_cre_date unit:@"."];
}

@end
