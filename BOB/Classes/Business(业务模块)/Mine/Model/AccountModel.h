//
//  AccountModel.h
//  BOB
//
//  Created by mac on 2019/7/15.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccountModel : BaseObject

@property (nonatomic ,copy) NSString* account_id;

@property (nonatomic ,copy) NSString* bank_branch_name;

@property (nonatomic ,copy) NSString* user_id;

@property (nonatomic ,copy) NSString* account_name;

@property (nonatomic ,copy) NSString* bank_name;
/*
 账户类型：
 1—银行卡
 2—支付宝
 3—微信
 */
@property (nonatomic ,copy) NSString* type;

@property (nonatomic ,copy) NSString* account;

@end

NS_ASSUME_NONNULL_END
