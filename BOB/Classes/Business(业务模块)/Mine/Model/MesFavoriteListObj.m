//
//  MesFavoriteListObj.m
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteListObj.h"
#import "NSDate+Category.h"
#import "MXChatDefines.h"

@implementation MesFavoriteListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

- (NSString *)format_send_time {
    if (!_format_send_time) {
        NSDate *date = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[self.send_time doubleValue]];
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
        _format_send_time = [dateFormatter stringFromDate:date];
    }
    return _format_send_time;
}

- (NSString *)clsString {
    if (!_clsString) {
        switch (self.type) {
            case kMXMessageTypeText:{
                _clsString = @"MesFavoriteListTextCell";
                break;
            }
            case kMXMessageTypeImage:{
                _clsString = @"MesFavoriteListImageCell";
                break;
            }
            case kMXMessageTypeVoice:{
                _clsString = @"MesFavoriteListVoiceCell";
                break;
            }
            case kMXMessageTypeLocation:{
                _clsString = @"MesFavoriteListAddressCell";
                break;
            }
            case kMXMessageTypeFile:{
                _clsString = @"MesFavoriteListFileCell";
                break;
            }
            case kMxmessageTypeVideo:{
                _clsString = @"MesFavoriteListVideoCell";
                break;
            }
            default:
                _clsString = @"MesFavoriteListTextCell";
                break;
        }
    }
    return _clsString;
}

@end
