//
//  YKCDetailModel.h
//  Lcwl
//
//  Created by mac on 2019/1/5.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YKCDetailModel : NSObject

@property (nonatomic ,copy) NSString* balance_id;

@property (nonatomic ,copy) NSString* order_id;

@property (nonatomic ,copy) NSString* user_id;

@property (nonatomic ,copy) NSString* num;

@property (nonatomic ,copy) NSString* curr_type;

@property (nonatomic ,copy) NSString* op_type;

@property (nonatomic ,copy) NSString* op_name;

@property (nonatomic ,copy) NSString* cre_date;

@property (nonatomic ,copy) NSString* cre_time;

+(YKCDetailModel* )dictionayToModel:(NSDictionary* )dict;

+(NSString* )opTypeStr:(NSString* )type;
@end

NS_ASSUME_NONNULL_END
