//
//  ChangeTypeModel.h
//  Lcwl
//
//  Created by mac on 2019/3/20.
//  Copyright © 2019年 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChangeTypeModel : NSObject

@property (nonatomic, copy) NSString* op_name;
@property (nonatomic, copy) NSString* change_type;
@property (nonatomic, copy) NSString* op_type;
@property (nonatomic, copy) NSString* balance_type_id;

+(ChangeTypeModel* )dictionaryToModel:(NSDictionary* )dict;
@end

NS_ASSUME_NONNULL_END
