//
//  MesFavoriteListObj.h
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MesFavoriteListObj : BaseObject

@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , copy) NSString              * ID;
///消息类型 1-文本 2-图片 3-语音 4-位置 5-GIF  6文件 7视频
@property (nonatomic , assign) NSInteger           type;
@property (nonatomic , copy) NSString              * attr1;
@property (nonatomic , copy) NSString              * attr2;
@property (nonatomic , copy) NSString              * attr3;
@property (nonatomic , copy) NSString              * attr4;
@property (nonatomic , copy) NSString              * attr5;
///1-用户 2-群
@property (nonatomic , copy) NSString              * send_type;
///群ID 用户ID
@property (nonatomic , copy) NSString              * send_id;

@property (nonatomic , copy) NSString              * send_name;

@property (nonatomic , copy) NSString              * send_time;
///时间戳 -> 年月日
@property (nonatomic , copy) NSString              *format_send_time;
@property (nonatomic , copy) NSString              *clsString;
@property (nonatomic , assign) CGFloat             viewHeight;
///是否在播放
@property (nonatomic , assign) BOOL             isPlaying;

@end

NS_ASSUME_NONNULL_END
