//
//  MessageCenterModel.h
//  BOB
//
//  Created by mac on 2019/7/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MessageCenterModel : BaseObject
/*
 读取状态：
 0—未读
 1—已读
 */
@property (nonatomic, assign) NSInteger read_status;
@property (nonatomic, copy)   NSString *cre_date;
@property (nonatomic, copy)   NSString *notice_title;
@property (nonatomic, copy)   NSString *notice_content;
@property (nonatomic, copy) NSString *_id;
@end

NS_ASSUME_NONNULL_END
