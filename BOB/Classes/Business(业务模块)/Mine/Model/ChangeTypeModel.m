//
//  ChangeTypeModel.m
//  Lcwl
//
//  Created by mac on 2019/3/20.
//  Copyright © 2019年 lichangwanglai. All rights reserved.
//

#import "ChangeTypeModel.h"

@implementation ChangeTypeModel

+(ChangeTypeModel* )dictionaryToModel:(NSDictionary* )dict{
    ChangeTypeModel* model = [[ChangeTypeModel alloc]init];
    model.op_name =  [NSString stringWithFormat:@"%@",dict[@"op_name"]];
    model.change_type   =  [NSString stringWithFormat:@"%@",dict[@"change_type"]];
    model.op_type =  [NSString stringWithFormat:@"%@",dict[@"op_type"]];
    model.balance_type_id   =  [NSString stringWithFormat:@"%@",dict[@"balance_type_id"]];
    return model;
}
@end
