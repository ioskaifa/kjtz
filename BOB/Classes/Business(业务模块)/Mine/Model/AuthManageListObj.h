//
//  AuthManageListObj.h
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthManageListObj : BaseObject

@property (nonatomic, copy) NSString *ID;
/// 性别 1男 2女
@property (nonatomic, copy) NSString *sex;
///地点
@property (nonatomic, copy) NSString *area;

@property (nonatomic, copy) NSString *system_user_id;

@property (nonatomic, copy) NSString *head_photo;

@property (nonatomic, copy) NSString *user_tel;

@property (nonatomic, copy) NSString *user_email;
///昵称
@property (nonatomic, copy) NSString *nick_name;
///用户名
@property (nonatomic, copy) NSString *sys_user_account;
///签名
@property (nonatomic, copy) NSString *sign_name;
///注册类型 1-手机号 2-邮箱（当前系统只支持手机号注册）
@property (nonatomic, copy) NSString *register_type;
///创建日期
@property (nonatomic, copy) NSString *cre_date;
///创建时间
@property (nonatomic, copy) NSString *cre_time;
///是否授权
@property (nonatomic, assign) BOOL is_valid;
///是否实名
@property (nonatomic, assign) BOOL is_realname;
///是否通过
@property (nonatomic, assign) BOOL is_auth;
///是否冻结
@property (nonatomic, assign) BOOL is_freeze;
///证件照
@property (nonatomic, copy) NSString *realname_photo;

@end

NS_ASSUME_NONNULL_END
