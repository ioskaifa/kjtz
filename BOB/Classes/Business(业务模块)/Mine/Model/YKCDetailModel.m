//
//  YKCDetailModel.m
//  Lcwl
//
//  Created by mac on 2019/1/5.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "YKCDetailModel.h"

@implementation YKCDetailModel
+(YKCDetailModel* )dictionayToModel:(NSDictionary* )dict{
    YKCDetailModel* detail = [[YKCDetailModel alloc]init];
   
    detail.balance_id =  [NSString stringWithFormat:@"%@",dict[@"balance_id"]];
    detail.order_id   =  [NSString stringWithFormat:@"%@",dict[@"order_id"]];
    detail.user_id    =  [NSString stringWithFormat:@"%@",dict[@"user_id"]];
    detail.num        =  [NSString stringWithFormat:@"%@",dict[@"num"]];
    detail.curr_type  =  [NSString stringWithFormat:@"%@",dict[@"curr_type"]];
    detail.op_type    =  [NSString stringWithFormat:@"%@",dict[@"op_type"]];
    detail.cre_date   =  [NSString stringWithFormat:@"%@",dict[@"cre_date"]];
    detail.cre_time   =  [NSString stringWithFormat:@"%@",dict[@"cre_time"]];
    detail.op_name   =  [NSString stringWithFormat:@"%@",dict[@"op_name"]];
    
    
    return detail;
}

+(NSString* )opTypeStr:(NSString* )type{
    NSString* str = @"";
    if ([StringUtil isEmpty:type]) {
        return @"";
    }
    if ([type isEqualToString:@"01"]) {
        str = @"兑换";
    }else if([type isEqualToString:@"02"]){
        str = @"充币";
    }else if([type isEqualToString:@"03"]){
        str = @"提币";
    }else if([type isEqualToString:@"04"]){
        str = @"购买币";
    }else if([type isEqualToString:@"05"]){
        str = @"售卖币";
    }else if([type isEqualToString:@"06"]){
        str = @"换取宠物质押美元";
    }else if([type isEqualToString:@"07"]){
        str = @"完善资料";
    }else if([type isEqualToString:@"08"]){
        str = @"实名认证";
    }else if([type isEqualToString:@"09"]){
        str = @"推荐奖";
    }else if([type isEqualToString:@"10"]){
        str = @"偷取美元";
    }else if([type isEqualToString:@"11"]){
        str = @"摘取美元";
    }else if([type isEqualToString:@"12"]){
        str = @"等级奖励";
    }else if([type isEqualToString:@"13"]){
        str = @"打赏";
    }else if([type isEqualToString:@"14"]){
        str = @"被赏";
    }else if([type isEqualToString:@"15"]){
        str = @"参与夺宝";
    }else if([type isEqualToString:@"16"]){
        str = @"夺宝失败";
    }else if([type isEqualToString:@"17"]){
        str = @"商品兑换";
    }else if([type isEqualToString:@"18"]){
        str = @"发红包";
    }else if([type isEqualToString:@"19"]){
        str = @"抢红包";
    }else if([type isEqualToString:@"20"]){
        str = @"买家撤单";
    }else if([type isEqualToString:@"21"]){
        str = @"交易完成";
    }else if([type isEqualToString:@"22"]){
        str = @"退还红包";
    }else if([type isEqualToString:@"23"]){
        str = @"提币失败";
    }else if([type isEqualToString:@"24"]){
        str = @"邀请好友";
    }else if([type isEqualToString:@"25"]){
        str = @"添加好友";
    }else if([type isEqualToString:@"26"]){
        str = @"意外收获";
    }else if([type isEqualToString:@"27"]){
        str = @"系统放行";
    }else if([type isEqualToString:@"28"]){
        str = @"系统撤销";
    }else if([type isEqualToString:@"29"]){
        str = @"系统回退";
    }else if([type isEqualToString:@"30"]){
        str = @"系统回收";
    }else if([type isEqualToString:@"31"]){
        str = @"挂单手续费";
    }
    return str;
}
@end
