//
//  YKCDetailCell.m
//  Lcwl
//
//  Created by mac on 2018/12/12.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "YKCDetailView.h"
@interface YKCDetailView()

@property (nonatomic , strong) UILabel* titleLbl;

@property (nonatomic , strong) UILabel* timeLbl;

@property (nonatomic , strong) UILabel* numLbl;

@property (nonatomic , strong) UILabel* orderIdLbl;

@property (nonatomic , strong) MXSeparatorLine* line;


@end

@implementation YKCDetailView


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}
-(void)initUI{
    [self addSubview:self.titleLbl];
    [self addSubview:self.timeLbl];
    [self addSubview:self.numLbl];
    [self addSubview:self.orderIdLbl];
    self.line = [MXSeparatorLine initHorizontalLineWidth:[UIScreen mainScreen].bounds.size.width orginX:0 orginY:0];
    [self addSubview:self.line];
    [self layoutSubviews];
}

-(void)layoutSubviews{
    @weakify(self)
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
     @strongify(self)
        make.left.equalTo(self.mas_left).offset(10);
        make.centerY.equalTo(self.mas_centerY).offset(-10);
        make.width.equalTo(@(150));
        make.height.equalTo(@(20));
    }];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(10);
        make.centerY.equalTo(self.mas_centerY).offset(15);
        make.width.equalTo(@(150));
        make.height.equalTo(@(20));
    }];
    [self.orderIdLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.timeLbl.mas_centerY);
        make.width.equalTo(@(200));
        make.height.equalTo(@(20));
    }];
    
    [self.numLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.mas_right).offset(-10);
        make.centerY.equalTo(self.titleLbl.mas_centerY);
        make.width.equalTo(@(150));
        make.height.equalTo(@(20));
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.numLbl.mas_right);
        make.left.equalTo(self.titleLbl.mas_left);
        make.height.equalTo(@(0.5));
        make.bottom.equalTo(self.mas_bottom);
    }];
}

- (UILabel *)numLbl
{
    if (!_numLbl) {
        _numLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _numLbl.text = @"+100";
        _numLbl.font = [UIFont font14];
        _numLbl.textAlignment = NSTextAlignmentRight;
         _numLbl.textColor = [UIColor themeColor];
    }
    return _numLbl;
}

- (UILabel *)timeLbl
{
    if (!_timeLbl) {
        _timeLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _timeLbl.font = [UIFont font11];
        _timeLbl.text = @"2018-12-09 16:15";
        _timeLbl.textColor = [UIColor lightGrayColor];
    }
    return _timeLbl;
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.text = @"充币";
        _titleLbl.font = [UIFont font17];
    }
    return _titleLbl;
}

- (UILabel *)orderIdLbl
{
    if (!_orderIdLbl) {
        _orderIdLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _orderIdLbl.text = @"流水号:";
        _orderIdLbl.font = [UIFont font11];
        _orderIdLbl.textAlignment = NSTextAlignmentRight;
        _orderIdLbl.textColor = [UIColor lightGrayColor];
    }
    return _orderIdLbl;
}

-(void)reloadData:(YKCDetailModel *)model{
    if ([model.op_type isEqualToString:@"01"]) {
        if ([model.curr_type isEqualToString:@"1"]) {
            if ([model.num hasPrefix:@"-"]) {
                 self.titleLbl.text = @"YKC兑换美元";
            }else{
                self.titleLbl.text = @"美元兑换YKC";
            }
        }else{
            if ([model.num hasPrefix:@"-"]) {
                self.titleLbl.text = @"美元兑换YKC";
            }else{
                self.titleLbl.text = @"YKC兑换美元";
            }
        }
    }else{
//        self.titleLbl.text = [YKCDetailModel opTypeStr:model.op_type];
        if (![StringUtil isEmpty:model.op_name]) {
            self.titleLbl.text = model.op_name;
        }else{
            self.titleLbl.text = [YKCDetailModel opTypeStr:model.op_type];
        }
        
    }
    if (![StringUtil isEmpty:model.cre_date] && ![StringUtil isEmpty:model.cre_time] ) {
        self.timeLbl.text = [NSString stringWithFormat:@"%@ %@",model.cre_date,model.cre_time];
    }else{
        self.timeLbl.text = @"";
    }
    self.numLbl.text = model.num;
    NSString* orderId = [NSString stringWithFormat:@"流水号:%@",model.order_id];
    self.orderIdLbl.text = orderId;
    
}
@end
