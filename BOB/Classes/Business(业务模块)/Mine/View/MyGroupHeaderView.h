//
//  MyGroupHeaderView.h
//  BOB
//
//  Created by mac on 2019/7/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyGroupHeaderView : UIView
@property(nonatomic, strong) UIImageView *headImgView;
@property(nonatomic, strong) UILabel *teamCountLbl;
@property(nonatomic, strong) UILabel *directCountLbl;
@property(nonatomic, strong) UIView *roundView;
@end

NS_ASSUME_NONNULL_END
