//
//  AuthManageListCell.h
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthManageListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthManageListCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(AuthManageListObj *)obj;

@end

NS_ASSUME_NONNULL_END
