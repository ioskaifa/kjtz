//
//  MesFavoriteListCell.h
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXChatDefines.h"
#import "MesFavoriteListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MesFavoriteListCell : UITableViewCell

@property (nonatomic, strong) MyLinearLayout *baseLayout;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *timeLbl;

- (void)createUI;

+ (CGFloat)viewHeight;

- (void)configureView:(MesFavoriteListObj *)obj;

- (MyBaseLayout *)bottomLayout;

@end

NS_ASSUME_NONNULL_END
