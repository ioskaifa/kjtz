//
//  FastTradingItemCell.m
//  Lcwl
//
//  Created by mac on 2018/12/13.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "YKCSelectItem.h"
@interface YKCSelectItem ()

@property (nonatomic, strong) UIImageView *signImg;
@property (nonatomic, strong) UIButton *btn;

@end

@implementation YKCSelectItem

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

#pragma mark - UI
- (void)initUI
{
    self.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:self.btn];
    [self.btn addSubview:self.signImg];
    [self layoutView];
}

-(void)layoutView{
    @weakify(self)
    [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.edges.mas_equalTo(self);
    }];
    [self.signImg mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.width.height.equalTo(@(20));
        make.right.equalTo(self.btn.mas_right);
        make.bottom.equalTo(self.btn.mas_bottom);
    }];
    
    return;
}

- (UIButton *)btn
{
    if (!_btn) {
        _btn = [UIButton buttonWithType:UIButtonTypeCustom];
        _btn.titleLabel.adjustsFontSizeToFitWidth = YES;
//        _btn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [_btn setTitleColor:[UIColor colorWithHexString:@"#696A6D"] forState:UIControlStateNormal];
        [_btn setTitleColor:[UIColor themeColor] forState:UIControlStateSelected];
        [_btn setTitle:@"" forState:UIControlStateNormal];
        _btn.titleLabel.font = [UIFont font14];
        _btn.layer.masksToBounds = YES;
//        _btn.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        _btn.layer.borderWidth = 0.5;
        _btn.userInteractionEnabled = NO;
    }
    return _btn;
}


- (UIImageView *)signImg
{
    if (!_signImg) {
        _signImg = [[UIImageView alloc]init];
        _signImg.image = [UIImage imageNamed:@"分类勾选"];
    }
    
    return _signImg;
}

-(void)reload:(NSString* )text state:(BOOL)isSelect{
    
    [self setSelectState:isSelect];
    [self setText:text];
}

-(void)setSelectState:(BOOL)isSelect{
    self.btn.selected = isSelect;
    self.signImg.hidden = !isSelect;
    if (isSelect) {
        self.btn.backgroundColor = [UIColor colorWithHexString:@"#D5EBFE"];
    }else{
        self.btn.backgroundColor = [UIColor colorWithHexString:@"#EFF3F6"];
    }
}

-(void)setText:(NSString *)text{
     [self.btn setTitle:text forState:UIControlStateNormal];
}

@end
