//
//  MesFavoriteListTextCell.m
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteListTextCell.h"
#import <YYKit/YYKit.h>
#import "YYTextHelper.h"

@interface MesFavoriteListTextCell ()

@property (nonatomic, strong) YYLabel *contentLbl;

@property (nonatomic, strong) MesFavoriteListObj *obj;

@end

@implementation MesFavoriteListTextCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {        
        [self createUI];
    }
    return self;
}

- (CGFloat)viewHeight {
    if (!self.obj) {
        return 0;
    }
    [self.contentLbl MXupdateOuterTextProperties];
    CGSize introSize = CGSizeMake(SCREEN_WIDTH - 30, CGFLOAT_MAX);
    YYTextLayout *layout = [YYTextLayout layoutWithContainerSize:introSize text:self.contentLbl.attributedText];
    CGSize size = layout.textBoundingSize;
    if (size.height > 47) {
        size = CGSizeMake(size.width, 47);
    }
    return size.height + 35;
}

- (void)configureView:(MesFavoriteListObj *)obj {
    [super configureView:obj];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    if (obj.type != kMXMessageTypeText) {
        return;
    }
    self.obj = obj;
    NSString* content = obj.attr1;
    // 测试文本
    NSString *text = content;
    // 转成可变属性字符串
    NSMutableAttributedString * mAttributedString = [NSMutableAttributedString new];
    // 调整行间距段落间距
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    [paragraphStyle setLineSpacing:0];
    [paragraphStyle setParagraphSpacing:0];

    // 设置文本属性
    NSDictionary *attri = [NSDictionary dictionaryWithObjects:@[[UIFont font14], [UIColor blackColor], paragraphStyle] forKeys:@[NSFontAttributeName, NSForegroundColorAttributeName, NSParagraphStyleAttributeName]];
    [mAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:text attributes:attri]];

    // 匹配条件
    NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";

    NSError *error = NULL;
    // 根据匹配条件，创建了一个正则表达式
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    if (!regex) {
        NSLog(@"正则创建失败error！= %@", [error localizedDescription]);
    } else {
        NSArray *allMatches = [regex matchesInString:mAttributedString.string options:NSMatchingReportCompletion range:NSMakeRange(0, mAttributedString.string.length)];
        for (NSTextCheckingResult *match in allMatches) {
            NSString *substrinsgForMatch2 = [mAttributedString.string substringWithRange:match.range];
            NSMutableAttributedString *one = [[NSMutableAttributedString alloc] initWithString:substrinsgForMatch2];
            // 利用YYText设置一些文本属性
            one.font = [UIFont font15];
            one.underlineStyle = NSUnderlineStyleSingle;
            one.color = [UIColor colorWithRed:0.093 green:0.492 blue:1.000 alpha:1.000];
            
            YYTextBorder *border = [YYTextBorder new];
            border.cornerRadius = 3;
    //                border.insets = UIEdgeInsetsMake(-2, -1, -2, -1);
            border.insets = UIEdgeInsetsMake(-2, 0, -2, 0);
            border.fillColor = [UIColor colorWithWhite:0.000 alpha:0.220];
            
            YYTextHighlight *highlight = [YYTextHighlight new];
            [highlight setBorder:border];
            [one setTextHighlightRange:one.rangeOfAll color:[UIColor blueColor]  backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                NSString* urlStr = [text.string substringWithRange:range];
                NSURL * url = [NSURL URLWithString:urlStr];
                if ([[UIApplication sharedApplication]canOpenURL:url]) {
                    [[UIApplication sharedApplication]openURL:url options:@{UIApplicationLaunchOptionsURLKey : @YES} completionHandler:^(BOOL success) {
                        //成功后的回调
                        if (!success) {
                            //失败后的回调
                        }
                    }];
                }else{
                    NSString* str = [NSString stringWithFormat:@"http://%@",urlStr];
                    url = [NSURL URLWithString:str];
                    [[UIApplication sharedApplication]openURL:url options:@{UIApplicationLaunchOptionsURLKey : @YES} completionHandler:^(BOOL success) {
                        //成功后的回调
                        if (!success) {
                            //失败后的回调
                            [NotifyHelper showMessageWithMakeText:@"链接无法打开"];
                        }
                    }];
                }
               
            }];
            
            // 根据range替换字符串
            [mAttributedString replaceCharactersInRange:match.range withAttributedString:one];
        }
    }
    self.contentLbl.attributedText = mAttributedString;
    [self.contentLbl MXupdateOuterTextProperties];

    YYTextContainer *container = [YYTextContainer containerWithSize:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    YYTextLayout *textLayout = [YYTextLayout layoutWithContainer:container text:self.contentLbl.attributedText];
    self.contentLbl.textLayout = textLayout;
}

- (void)createUI {
    [super createUI];
    [self.baseLayout addSubview:self.contentLbl];
    [self.baseLayout addSubview:[self bottomLayout]];
}

- (YYLabel *)contentLbl {
    if (!_contentLbl) {
        _contentLbl = [self.class emojiLabel];
        _contentLbl.myLeft = 15;
        _contentLbl.myRight = 15;
        _contentLbl.weight = 1;
        _contentLbl.myTop = 5;
    }
    return _contentLbl;
}

+ (YYLabel *)emojiLabel{
    YYLabel *yyLabel = [YYTextHelper yyLabel];
    yyLabel.font = [UIFont font17];
    return yyLabel;
}

@end
