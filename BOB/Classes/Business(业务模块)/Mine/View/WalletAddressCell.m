//
//  WalletAddressCell.m
//  BOB
//
//  Created by mac on 2019/6/27.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "WalletAddressCell.h"

@interface WalletAddressCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *addressCopyBnt;

@end

@implementation WalletAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(14));
        make.left.equalTo(@(15));
    }];
    
    [self.addressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(14));
        make.right.equalTo(@(-15));
        make.left.equalTo(self.titleLbl.mas_right).offset(50);
    }];
    
    [self.addressCopyBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@(-10));
        make.right.equalTo(@(-15));
        make.height.equalTo(@(18));
    }];
    
    [self.titleLbl setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    [self.addressLbl setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
//    [self.addressLbl setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}


- (void)loadContent {
    self.titleLbl.text = Lang(@"钱包地址");
    self.addressLbl.text = self.data ?: @"";
    [self.addressCopyBnt setTitle:Lang(@"复制") forState:UIControlStateNormal];
    
    [self.addressCopyBnt sizeToFit];
    [self.addressCopyBnt mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(self.addressCopyBnt.width+20));
    }];
}

- (IBAction)addressCopyClick:(id)sender {
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.addressLbl.text;
    [NotifyHelper showMessageWithMakeText:Lang(@"复制成功")];
}

@end
