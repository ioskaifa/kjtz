//
//  WithdrawalRecordView.h
//  BOB
//
//  Created by mac on 2019/7/12.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawalRecordView : UIView

-(void)reloadTitle:(NSString *)title time:(NSString *)time num:(NSString *)num;

@end

NS_ASSUME_NONNULL_END
