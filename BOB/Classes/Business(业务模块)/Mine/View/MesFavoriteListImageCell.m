//
//  MesFavoriteListImageCell.m
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteListImageCell.h"

@interface MesFavoriteListImageCell ()

@property (nonatomic, strong) UIImageView *imgView;

@end

@implementation MesFavoriteListImageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 100;
}

- (void)configureView:(MesFavoriteListObj *)obj {
    [super configureView:obj];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    if (obj.type != kMXMessageTypeImage) {
        return;
    }
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:StrF(@"%@/%@", UDetail.user.qiniu_domain, obj.attr1)]];
}

- (void)createUI {
    [super createUI];
    [self.baseLayout addSubview:self.imgView];
    [self.baseLayout addSubview:[self bottomLayout]];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.contentMode = UIViewContentModeScaleAspectFit;
            object.backgroundColor = [UIColor moBackground];
            object.myTop = 5;
            object.myLeft = 15;
            object.height = [self.class viewHeight] - 30 - 5;
            object.myHeight = object.height;
            object.myWidth = object.height;
            object;
        });
    }
    return _imgView;
}

@end
