//
//  YKCSelectItemsVC.h
//  Lcwl
//
//  Created by mac on 2019/3/20.
//  Copyright © 2019年 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChangeTypeModel;
NS_ASSUME_NONNULL_BEGIN

@interface YKCSelectItemsView : UIView

@property (nonatomic, strong) FinishedBlock block;

@property (nonatomic , copy) NSString* curr_type;

-(void)reloadSelectItem:(ChangeTypeModel*)model;

@end

NS_ASSUME_NONNULL_END
