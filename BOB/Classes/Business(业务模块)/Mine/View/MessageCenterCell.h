//
//  MessageCenterCell.h
//  BOB
//
//  Created by mac on 2019/7/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "STDTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MessageCenterCell : STDTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *whiteBG;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *redIcon;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (strong, nonatomic) MASConstraint *titleRightConstraint;
@end

NS_ASSUME_NONNULL_END
