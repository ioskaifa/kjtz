//
//  MineWalletRemainingTC.m
//  BOB
//
//  Created by mac on 2019/6/27.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MineWalletRemainingTC.h"
#import "NSMutableAttributedString+Attributes.h"

@interface MineWalletRemainingTC ()
@property (weak, nonatomic) IBOutlet UILabel *walletLbl;
@property (weak, nonatomic) IBOutlet UIButton *rechargeBnt;
@property (weak, nonatomic) IBOutlet UIButton *withdrawBnt;
@end

@implementation MineWalletRemainingTC

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.walletLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.centerY.equalTo(self.contentView);
    }];
    
    [self.withdrawBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-15));
        make.centerY.equalTo(self.contentView);
        make.width.equalTo(@(70));
        make.height.equalTo(@(24));
    }];
    
    [self.rechargeBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.withdrawBnt.mas_left).offset(-15);
        make.centerY.equalTo(self.contentView);
        make.width.equalTo(@(70));
        make.height.equalTo(@(24));
    }];
}

- (void)loadContent
{
    NSString *money = [NSString stringWithFormat:@"RB %@",self.data];
    NSString *moneyRemaining = Lang(@"钱包余额");
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",money,moneyRemaining]];
    [attStr setLineSpacing:10 substring:money alignment:NSTextAlignmentCenter];
    [attStr addFont:[UIFont systemFontOfSize:21] substring:money];
    [attStr addFont:[UIFont systemFontOfSize:12] substring:moneyRemaining];
    [attStr addColor:[UIColor colorWithHexString:@"#1E1F1F"] substring:money];
    [attStr addColor:[UIColor colorWithHexString:@"#AFB3B3"] substring:moneyRemaining];
    self.walletLbl.attributedText = attStr;
    
    [self.withdrawBnt setTitle:LangLPH(@"提现", @"  ") forState:UIControlStateNormal];
    [self.rechargeBnt setTitle:LangLPH(@"充值", @"  ") forState:UIControlStateNormal];
}

- (IBAction)rechargeBntClick:(id)sender {
    [MXRouter openURL:@"lcwl://ChargeCoinVC"];
}

- (IBAction)withDrawBntClik:(id)sender {
    [MXRouter openURL:@"lcwl://WithdrawVC"];
}

@end
