//
//  MessageCenterCell.m
//  BOB
//
//  Created by mac on 2019/7/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MessageCenterCell.h"
#import "MessageCenterModel.h"

@implementation MessageCenterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.subTitleLbl.textColor = [UIColor colorWithHexString:@"#C1C1C1"];
    self.contentView.backgroundColor = [UIColor moBackground];
    self.timeLbl.hidden = YES;
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.top.equalTo(@(20));
    }];
    
    [self.whiteBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(10, 15, 5, 15));
    }];
    [self.redIcon sizeToFit];
    [self.redIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.redIcon.size);
        make.centerY.mas_equalTo(self.whiteBG);
        make.right.mas_equalTo(self.whiteBG.mas_right).offset(-10);
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.whiteBG.mas_left).offset(10);
        make.top.mas_equalTo(self.whiteBG.mas_top).offset(10);
        make.right.mas_equalTo(self.redIcon.mas_left).offset(-10);
        make.height.mas_equalTo(25);
    }];
    
    [self.subTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLbl.mas_left);
        make.right.mas_equalTo(self.titleLbl.mas_right);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(10);
        make.height.mas_equalTo(15);
    }];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)loadContent {
    MessageCenterModel *model = self.data;
    self.titleLbl.text = model.notice_title;
    //self.subTitleLbl.text = model.notice_content;
    self.subTitleLbl.text = model.cre_date;
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[model.notice_content dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    self.subTitleLbl.attributedText = attrStr;//用于显示
}

@end
