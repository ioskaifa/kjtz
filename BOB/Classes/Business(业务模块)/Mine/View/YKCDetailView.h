//
//  YKCDetailCell.h
//  Lcwl
//
//  Created by mac on 2018/12/12.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKCDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface YKCDetailView : UIView

-(void)reloadData:(YKCDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
