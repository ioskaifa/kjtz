//
//  YKCDetailTopView.m
//  Lcwl
//
//  Created by mac on 2019/3/20.
//  Copyright © 2019年 lichangwanglai. All rights reserved.
//

#import "YKCDetailTopView.h"
#import "SPButton.h"

@interface YKCDetailTopView()

@property (nonatomic, strong) SPButton *uploadBtn;

@property (nonatomic, strong) UIImageView *arrowImg;

@end

@implementation YKCDetailTopView
static int padding = 10;
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self layout];
    }
    return self;
}

-(void)initUI{
    [self addSubview:self.uploadBtn];
    //[self addSubview:self.arrowImg];
    self.backgroundColor = [UIColor whiteColor];
}

-(void)layout{
    @weakify(self)
    [self.uploadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        //make.left.equalTo(self.mas_left).offset(padding);
        //make.centerY.equalTo(self.mas_centerY);
        make.center.equalTo(self);
    }];
    
//    [self.arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self)
//        make.right.equalTo(self.mas_right).offset(-padding);
//        make.centerY.equalTo(self.uploadBtn.mas_centerY);
//    }];
}

- (UIImageView *)arrowImg
{
    if (!_arrowImg) {
        _arrowImg = [[UIImageView alloc]initWithFrame:CGRectZero];
        _arrowImg.image = [UIImage imageNamed:@"downArrow"];
        
    }
    return _arrowImg;
}

- (SPButton *)uploadBtn{
    if (!_uploadBtn) {
        _uploadBtn = [[SPButton alloc] initWithImagePosition:SPButtonImagePositionTop];
        _uploadBtn.frame = CGRectMake(0, 0, 65, 45);
        _uploadBtn.imagePosition = SPButtonImagePositionRight;
        [_uploadBtn setTitle:LangRPH(@"请选择您要查询的分类", @" ") forState:UIControlStateNormal];
        [_uploadBtn setTitleColor:[UIColor colorWithHexString:@"#1E1F1F"] forState:UIControlStateNormal];
        _uploadBtn.titleLabel.font = [UIFont font14];
        [_uploadBtn setImage:[UIImage imageNamed:@"downArrow"] forState:UIControlStateNormal];
        _uploadBtn.userInteractionEnabled = NO;
    }
    return _uploadBtn;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (self.block) {
        self.block(nil);
    }
}

-(void)reload:(NSString *)title select:(BOOL)select{
    [self reloadTitle:[NSString stringWithFormat:@"%@ ",title]];
    [self reloadSelect:select];
}

-(void)reloadTitle:(NSString *)title {
     [self.uploadBtn setTitle:title forState:UIControlStateNormal];
}

-(void)reloadSelect:(BOOL)select {
    //NSString* imageName = select?@"展开":@"折叠";
    //self.arrowImg.image = [UIImage imageNamed:imageName];
}
@end
