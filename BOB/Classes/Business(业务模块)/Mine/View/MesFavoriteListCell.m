//
//  MesFavoriteListCell.m
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteListCell.h"

@implementation MesFavoriteListCell

+ (CGFloat)viewHeight {
    return 0;
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    [self.contentView addSubview:self.baseLayout];
}

- (void)configureView:(MesFavoriteListObj *)obj {
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    self.timeLbl.text = obj.format_send_time;
    self.titleLbl.text = obj.send_name;
    [self.titleLbl sizeToFit];
    self.titleLbl.mySize = self.titleLbl.size;
}

- (MyBaseLayout *)bottomLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = 30;
    [layout addSubview:self.titleLbl];
    [layout addSubview:self.timeLbl];
    return layout;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
            object.font = [UIFont font11];
            object.myCenterY = 0;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
            object.font = [UIFont font11];
            object.myHeight = 20;
            object.myCenterY = 0;
            object.myLeft = 10;
            object.weight = 1;
            object;
        });
    }
    return _timeLbl;
}

- (MyLinearLayout *)baseLayout {
    if (!_baseLayout) {
        _baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _baseLayout.backgroundColor = [UIColor whiteColor];
        _baseLayout.myTop = 0;
        _baseLayout.myLeft = 0;
        _baseLayout.myRight = 0;
        _baseLayout.myBottom = 0;
    }
    return _baseLayout;
}

@end
