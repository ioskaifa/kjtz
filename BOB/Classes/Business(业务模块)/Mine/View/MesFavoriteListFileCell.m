//
//  MesFavoriteListFileCell.m
//  BOB
//
//  Created by mac on 2020/8/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteListFileCell.h"

@interface MesFavoriteListFileCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *sizeLbl;

@property (nonatomic, strong) UIView *placeholderView;

@end

@implementation MesFavoriteListFileCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 100;
}

- (void)configureView:(MesFavoriteListObj *)obj {
    [super configureView:obj];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    if (obj.type != kMXMessageTypeFile) {
        return;
    }
    NSString *fileName = obj.attr4;
    NSString *fileSize = obj.attr3;
    NSString *fileType = obj.attr2;
    fileType = fileType.uppercaseString;
    self.nameLbl.text = fileName;
    CGSize size = [self.nameLbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 40 - self.imgView.width, MAXFLOAT)];
    size.width = SCREEN_WIDTH - 40 - self.imgView.width;
    self.nameLbl.mySize = size;
    self.sizeLbl.text = StrF(@"%@ %@", fileType, fileSize);
    
    fileType = fileType.lowercaseString;
    kMXFileType type = [MessageModel fileType:fileType];
    self.imgView.image = [UIImage imageNamed:[self.class iconWith:type]];
}

+ (NSString *)iconWith:(kMXFileType)type {
    NSString *icon = @"icon_msg_unknow";
    switch (type) {
        case kMXFileTypePDF:{
            icon = @"icon_msg_pdf";
            break;
        }
        case kMXFileTypeWord:{
            icon = @"icon_msg_word";
            break;
        }
        case kMXFileTypeExcel:{
            icon = @"icon_msg_excel";
            break;
        }
        default:
            break;
    }
    return icon;
}

- (void)createUI {
    [super createUI];
    [self.baseLayout addSubview:[self topLayout]];
    [self.baseLayout addSubview:[self bottomLayout]];
}

- (MyBaseLayout *)topLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 5;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.weight = 1;
        
    [layout addSubview:self.imgView];
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myLeft = 10;
    rightLayout.weight = 1;
    rightLayout.myTop = 0;
    rightLayout.myBottom = 0;
    [layout addSubview:rightLayout];
    [rightLayout addSubview:self.nameLbl];
    [rightLayout addSubview:self.sizeLbl];
    
    return layout;
}

#pragma mark - Init
- (UIView *)placeholderView {
    if (!_placeholderView) {
        _placeholderView = ({
            UIView *object = [UIView new];
            object.size = CGSizeMake(10, 1);
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _placeholderView;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.image = [UIImage imageNamed:@"icon_msg_unknow"];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
//            object.lineBreakMode = NSLineBreakByTruncatingMiddle;
            object.font = [UIFont font15];
            object.textColor = [UIColor moBlack];
            object.numberOfLines = 2;
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)sizeLbl {
    if (!_sizeLbl) {
        _sizeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font13];
            object.textColor = [UIColor grayColor];
            object.myTop = 5;
            object.myHeight = 20;
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _sizeLbl;
}

@end
