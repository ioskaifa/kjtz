//
//  AuthManageListCell.m
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AuthManageListCell.h"

@interface AuthManageListCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *phoneLbl;

@property (nonatomic, strong) UILabel *IDLbl;

@property (nonatomic, strong) UILabel *timeLbl;

@property (nonatomic, strong) UIButton *authBtn;

@property (nonatomic, strong) AuthManageListObj *obj;

@end

@implementation AuthManageListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
        [self.contentView addBottomSingleLine:[UIColor moBackground]];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 78;
}

- (void)configureView:(AuthManageListObj *)obj {
    if (![obj isKindOfClass:AuthManageListObj.class]) {
        return;
    }
    self.obj = obj;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.head_photo] placeholderImage:[UIImage defaultAvatar]];
    self.timeLbl.text = StrF(@"%@: %@", @"注册时间", obj.cre_date);
    
    self.nameLbl.text = obj.nick_name;
    self.phoneLbl.text = StrF(@"%@: %@", @"手机号", obj.user_tel);
    self.IDLbl.text = StrF(@"%@: %@", @"ID", obj.ID);
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.imgView];
    MyLinearLayout *centerLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    centerLayout.myHeight = MyLayoutSize.wrap;
    centerLayout.myCenterY = 0;
    centerLayout.weight = 1;
    [centerLayout addSubview:self.nameLbl];
    [centerLayout addSubview:self.phoneLbl];
    [centerLayout addSubview:self.IDLbl];
    [layout addSubview:centerLayout];
    
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myCenterY = 0;
    rightLayout.myLeft = 10;
    rightLayout.myRight = 15;
    rightLayout.myHeight = MyLayoutSize.wrap;
    rightLayout.myWidth = self.timeLbl.width;
    [rightLayout addSubview:self.timeLbl];
    [rightLayout addSubview:self.authBtn];
    [layout addSubview:rightLayout];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            object.size = CGSizeMake(38, 38);
            object.mySize = object.size;
            [object setViewCornerRadius:object.height/2.0];
            object.myCenterY = 0;
            object.myLeft = 15;
            object.myRight = 10;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor blackColor];
            object.myWidth = MyLayoutSize.fill;
            object.myHeight = 20;
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = ({
            UILabel *object = [UILabel new];
            object.textAlignment = NSTextAlignmentRight;
            object.font = [UIFont font11];
            object.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
            object.size = CGSizeMake(120, 20);
            object.mySize = object.size;
            object;
        });
    }
    return _timeLbl;
}

- (UILabel *)phoneLbl {
    if (!_phoneLbl) {
        _phoneLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font11];
            object.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
            object.myHeight = 16;
            object.myWidth = MyLayoutSize.fill;
            object;
        });
    }
    return _phoneLbl;
}

- (UILabel *)IDLbl {
    if (!_IDLbl) {
        _IDLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font11];
            object.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
            object.myHeight = 16;
            object.myWidth = MyLayoutSize.fill;
            object;
        });
    }
    return _IDLbl;
}

- (UIButton *)authBtn {
    if (!_authBtn) {
        _authBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font11];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor colorWithHexString:@"#0088FF"];
            object.size = CGSizeMake(60, 25);
            object.mySize = object.size;
            object.myTop = 15;
            object.myLeft = self.timeLbl.width - object.width;
            object.visibility = MyVisibility_Gone;
            object;
        });
    }
    return _authBtn;
}

@end
