//
//  ChatRoomSettingCell.m
//  BOB
//
//  Created by mac on 2020/8/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChatRoomSettingCell.h"

@interface ChatRoomSettingCell ()

@property (nonatomic, strong) UILabel *textLbl;

@property (nonatomic, strong) UILabel *detailTextLbl;

@end

@implementation ChatRoomSettingCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60;
}

- (void)configureView:(NSString *)text detailText:(NSString *)detailText {
    self.textLbl.text = text?:@"";
    [self.textLbl sizeToFit];
    self.textLbl.mySize = self.textLbl.size;
    
    self.detailTextLbl.text = detailText?:@"";
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    [layout addSubview:self.textLbl];
    [layout addSubview:self.detailTextLbl];
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"Arrow"];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myRight = 15;
    [layout addSubview:imgView];
}

- (UILabel *)textLbl {
    if (!_textLbl) {
        _textLbl = [UILabel new];
        _textLbl.font = [UIFont font17];
        _textLbl.textColor = [UIColor blackColor];
        _textLbl.myLeft = 15;
        _textLbl.myCenterY = 0;
    }
    return _textLbl;
}

- (UILabel *)detailTextLbl {
    if (!_detailTextLbl) {
        _detailTextLbl = [UILabel new];
        _detailTextLbl.textAlignment = NSTextAlignmentRight;
        _detailTextLbl.font = [UIFont font17];
        _detailTextLbl.textColor = [UIColor grayColor];
        _detailTextLbl.myLeft = 10;
        _detailTextLbl.myRight = 10;
        _detailTextLbl.myCenterY = 0;
        _detailTextLbl.weight = 1;
        _detailTextLbl.myHeight = 20;
    }
    return _detailTextLbl;
}

@end
