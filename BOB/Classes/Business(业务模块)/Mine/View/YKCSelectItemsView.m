//
//  YKCSelectItemsVC.m
//  Lcwl
//
//  Created by mac on 2019/3/20.
//  Copyright © 2019年 lichangwanglai. All rights reserved.
//

#import "YKCSelectItemsView.h"
#import "YKCSelectItem.h"
#import "MineHelper.h"
#import "ChangeTypeModel.h"
static int padding = 15;
@interface YKCSelectItemsView()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSArray* dataArray;

@property (nonatomic, strong) UIButton *leftBtn;

@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic) NSInteger  selectIndex;
 
@end

@implementation YKCSelectItemsView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self layout];
    }
    return self;
}
-(void)setCurr_type:(NSString* )type{
    _curr_type = type;
    [self initData];
}
-(void)initData{
    [MineHelper getBalanceTypeList:@{@"change_type":self.curr_type} completion:^(id array, NSString *error) {
        if (array) {
            NSMutableArray* tmpArray = [[NSMutableArray alloc]initWithCapacity:10];
            ChangeTypeModel* model = [[ChangeTypeModel alloc]init];
            model.op_name = @"全部分类";
            [tmpArray addObject:model];
            [tmpArray addObjectsFromArray:array];
            self.dataArray = tmpArray;
            [self.collectionView reloadData];
        }
    }];
}
-(void)initUI{
    [self addSubview:self.collectionView];
    [self addSubview:self.leftBtn];
    [self addSubview:self.rightBtn];
    MXSeparatorLine*upLine=[MXSeparatorLine initHorizontalLineWidth:[UIScreen mainScreen].bounds.size.width orginX:0 orginY:0];
    [self addSubview:upLine];
    
    self.dataArray = @[];
    [self.collectionView reloadData];
    self.backgroundColor = [UIColor whiteColor];
}

-(void)layout{
    @weakify(self)
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(padding);
        make.right.equalTo(self.mas_right).offset(-padding);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom).offset(-44);
    }];
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.bottom.equalTo(self.mas_bottom);
        make.width.equalTo(@(SCREEN_WIDTH/2));
        make.height.equalTo(@(44));
    }];
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(44));
        make.width.equalTo(@(SCREEN_WIDTH/2));
    }];
}

-(UIButton*)leftBtn{
    if (!_leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftBtn.titleLabel.font = [UIFont font17];
        [_leftBtn setTitle:@"取消" forState:UIControlStateNormal];
        [_leftBtn addTarget:self action:@selector(cancelClick:) forControlEvents:UIControlEventTouchUpInside];
        [_leftBtn setTitleColor: [UIColor themeColor] forState:UIControlStateNormal];
        _leftBtn.backgroundColor = [UIColor colorWithHexString:@"#d6ebfe"];
    }
    
    return _leftBtn;
}

-(void)cancelClick:(id)sender{
    if (self.block) {
        self.block(nil);
    }
}

-(UIButton*)rightBtn{
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightBtn.titleLabel.font = [UIFont font17];
        [_rightBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_rightBtn addTarget:self action:@selector(confirmClick:) forControlEvents:UIControlEventTouchUpInside];
        _rightBtn.backgroundColor = [UIColor themeColor];
    }
    
    return _rightBtn;
}

-(void)confirmClick:(id)sender{
    if (self.block) {
        if (self.dataArray.count ==0) {
            return;
        }
        self.block(self.dataArray[self.selectIndex]);
    }
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
        CGFloat width = (SCREEN_WIDTH-4*padding)/3;
        
        [layout setItemSize:CGSizeMake(width, width/3.0)];
        layout.sectionInset = UIEdgeInsetsMake(10, 0, 10, 0);
        
        [layout setMinimumInteritemSpacing:padding];
        [layout setMinimumLineSpacing: padding];
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.showsHorizontalScrollIndicator = NO;
         _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView registerClass:[YKCSelectItem class] forCellWithReuseIdentifier:@"YKCSelectItem"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
    }
    return _collectionView;
}



#pragma mark - UICollectionViewDataSource

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    cell.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        cell.transform = CGAffineTransformIdentity;
    } completion:nil];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YKCSelectItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YKCSelectItem" forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithHexString:@"#eff3f6"];
    ChangeTypeModel* model = self.dataArray[indexPath.row];
    if (self.selectIndex == indexPath.row) {
        [cell reload:model.op_name state:YES];
    }else{
         [cell reload:model.op_name state:NO];
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSIndexPath* path = [NSIndexPath indexPathForRow:_selectIndex inSection:0];
    YKCSelectItem * cell = (YKCSelectItem*)[collectionView cellForItemAtIndexPath:path];
    [cell setSelectState:NO];
    YKCSelectItem * cell1 = (YKCSelectItem*)[collectionView cellForItemAtIndexPath:indexPath];
    [cell1 setSelectState:YES];
    self.selectIndex = indexPath.row;
   
}

-(void)reloadSelectItem:(ChangeTypeModel*)model{
    if (model != nil) {
        self.selectIndex = [self.dataArray indexOfObject:model];
    }else{
        self.selectIndex = 0;
    }
     [self.collectionView reloadData];
}
@end
