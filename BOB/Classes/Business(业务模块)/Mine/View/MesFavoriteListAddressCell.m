//
//  MesFavoriteListAddressCell.m
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteListAddressCell.h"

@interface MesFavoriteListAddressCell ()

@property (nonatomic, strong) UILabel *addressLbl;

@property (nonatomic, strong) UILabel *desLbl;

@end

@implementation MesFavoriteListAddressCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {        
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 80;
}

- (void)configureView:(MesFavoriteListObj *)obj {
    [super configureView:obj];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    if (obj.type != kMXMessageTypeLocation) {
        return;
    }
    
    self.addressLbl.text = obj.attr1;
    [self.addressLbl sizeToFit];
    self.addressLbl.mySize = self.addressLbl.size;
    
    self.desLbl.text = obj.attr4;
    [self.desLbl sizeToFit];
    self.desLbl.mySize = self.desLbl.size;
}

- (void)createUI {
    [super createUI];
    [self.baseLayout addSubview:[self topLayout]];
    [self.baseLayout addSubview:[self bottomLayout]];
}

- (MyBaseLayout *)topLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 5;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = MyLayoutSize.wrap;
    
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"icon_collection_address"];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myTop = 10;
    [layout addSubview:imgView];
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myLeft = 10;
    rightLayout.weight = 1;
    rightLayout.myCenterY = 0;
    [layout addSubview:rightLayout];
    [rightLayout addSubview:self.addressLbl];
    [rightLayout addSubview:self.desLbl];
    
    return layout;
}

- (UILabel *)addressLbl {
    if (!_addressLbl) {
        _addressLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor blackColor];
            object.font = [UIFont font15];
            object;
        });
    }
    return _addressLbl;
}

- (UILabel *)desLbl {
    if (!_desLbl) {
        _desLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
            object.font = [UIFont font12];
            object.myTop = 5;
            object;
        });
    }
    return _desLbl;
}

@end
