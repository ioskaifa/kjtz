//
//  YKCSelectItem.h
//  Lcwl
//
//  Created by mac on 2019/3/20.
//  Copyright © 2019年 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YKCSelectItem : UICollectionViewCell

-(void)reload:(NSString* )text state:(BOOL)isSelect;

-(void)setSelectState:(BOOL)isSelect;

-(void)setText:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
