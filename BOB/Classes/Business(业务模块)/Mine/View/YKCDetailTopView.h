//
//  YKCDetailTopView.h
//  Lcwl
//
//  Created by mac on 2019/3/20.
//  Copyright © 2019年 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YKCDetailTopView : UIView

@property (nonatomic, strong) FinishedBlock block;

-(void)reloadTitle:(NSString *)title ;

-(void)reloadSelect:(BOOL)select ;


-(void)reload:(NSString *)title select:(BOOL)select;

@end

NS_ASSUME_NONNULL_END
