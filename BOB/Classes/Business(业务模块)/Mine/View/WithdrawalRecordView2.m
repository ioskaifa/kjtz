//
//  YKCDetailCell.m
//  Lcwl
//
//  Created by mac on 2018/12/12.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "WithdrawalRecordView2.h"
static NSInteger padding = 15;
@interface WithdrawalRecordView2()

@property (nonatomic , strong) UILabel* titleLbl;

@property (nonatomic , strong) UILabel* descLbl;

@property (nonatomic , strong) UIButton* stateBtn;

@property (nonatomic , strong) MXSeparatorLine* line;

@end

@implementation WithdrawalRecordView2


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}
-(void)initUI{
    [self addSubview:self.titleLbl];
    [self addSubview:self.descLbl];
    [self addSubview:self.stateBtn];
    self.line = [MXSeparatorLine initHorizontalLineWidth:[UIScreen mainScreen].bounds.size.width orginX:0 orginY:0];
    [self addSubview:self.line];
    [self layoutSubviews];
}

-(void)layoutSubviews{
    @weakify(self)
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(padding);
        make.centerY.equalTo(self.stateBtn.mas_top);
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.titleLbl);
        make.top.equalTo(self.titleLbl.mas_bottom).offset(5);
    }];
    
    [self.stateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.mas_right).offset(-padding);
        make.centerY.equalTo(self.mas_centerY);
//        make.width.equalTo(@(55));
        make.height.equalTo(@(24));
    }];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.stateBtn.mas_right);
        make.left.equalTo(self.titleLbl.mas_left);
        make.height.equalTo(@(0.5));
        make.bottom.equalTo(self.mas_bottom);
    }];
}

- (UIButton *)stateBtn
{
    if (!_stateBtn) {
        _stateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_stateBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        _stateBtn.titleLabel.font = [UIFont font15];
        [_stateBtn setTitle:@"处理中" forState:UIControlStateNormal];
        _stateBtn.layer.masksToBounds = YES;
        _stateBtn.layer.cornerRadius = 3;
        _stateBtn.layer.borderColor = [UIColor redColor].CGColor;
        _stateBtn.layer.borderWidth = 0.5;
    }
    return _stateBtn;
}

- (UILabel *)descLbl
{
    if (!_descLbl) {
        _descLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _descLbl.font = [UIFont systemFontOfSize:13];
        _descLbl.textColor = [UIColor lightGrayColor];
    }
    return _descLbl;
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.font = [UIFont boldSystemFontOfSize:23];
        _titleLbl.textColor = [UIColor moBlack];
    }
    return _titleLbl;
}

-(void)reloadTitle:(NSString *)title desc:(NSString *)desc state:(NSString *)state{
    self.titleLbl.text = title;
    self.descLbl.text = desc;
    [self.stateBtn setTitle:[NSString stringWithFormat:@"  %@  ",state] forState:UIControlStateNormal];
}

@end
