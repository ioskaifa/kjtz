//
//  MesFavoriteListVoiceCell.m
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteListVoiceCell.h"

@interface MesFavoriteListVoiceCell ()

@property (nonatomic, strong) SPButton *voiceBtn;

@end

@implementation MesFavoriteListVoiceCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60;
}

- (void)configureView:(MesFavoriteListObj *)obj {
    [super configureView:obj];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    if (obj.type != kMXMessageTypeVoice) {
        return;
    }
    NSInteger duration = [obj.attr2 integerValue];
    NSString *minu = [NSString stringWithFormat:@"%02ld",(duration%3600)/60];
    NSString *sec = [NSString stringWithFormat:@"%02ld",duration%60];
     
    [self.voiceBtn setTitle:StrF(@"%@:%@", minu, sec) forState:UIControlStateNormal];
    [self.voiceBtn sizeToFit];
    self.voiceBtn.mySize = self.voiceBtn.size;
}

- (void)createUI {
    [super createUI];
    [self.baseLayout addSubview:self.voiceBtn];
    [self.baseLayout addSubview:[self bottomLayout]];
}

- (SPButton *)voiceBtn {
    if (!_voiceBtn) {
        _voiceBtn = ({
            SPButton *object = [SPButton new];
            object.userInteractionEnabled = NO;
            [object setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font15];
            object.imageTitleSpace = 10;
            [object setImage:[UIImage imageNamed:@"icon_collection_voice"] forState:UIControlStateNormal];
            object.myTop = 10;
            object.myLeft = 15;
            object;
        });
    }
    return _voiceBtn;
}

@end
