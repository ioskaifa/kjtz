//
//  MyGroupHeaderView.m
//  BOB
//
//  Created by mac on 2019/7/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MyGroupHeaderView.h"

@implementation MyGroupHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (UIImageView *)headImgView {
    if(!_headImgView) {
        _headImgView = [[UIImageView alloc] init];
    }
    return _headImgView;
}

- (UIView *)roundView {
    if(!_roundView) {
        _roundView = [[UIView alloc] init];
    }
    return _roundView;
}
@end
