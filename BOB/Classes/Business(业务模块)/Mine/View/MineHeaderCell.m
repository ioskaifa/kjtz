//
//  MineHeaderCell.m
//  BOB
//
//  Created by mac on 2019/6/27.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MineHeaderCell.h"
#import "MXSeparatorLine.h"
#import "NSMutableAttributedString+Attributes.h"
#import "UIButton+WebCache.h"

@interface MineHeaderCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIButton *qrCodeBnt;
@property (weak, nonatomic) IBOutlet UILabel *infoLbl;
@property (weak, nonatomic) IBOutlet UILabel *teamCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *recommandCountLbl;
@property (weak, nonatomic) IBOutlet UIButton *headerPicBnt;
@property (weak, nonatomic) IBOutlet UIButton *editInfoBnt;
@property (weak, nonatomic) IBOutlet UIImageView *headerImgView;
@property (nonatomic, strong) UIButton *sexBtn;
@end

@implementation MineHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.infoLbl.textColor = [UIColor blackColor];
    self.infoLbl.font = [UIFont font18];
    [self.qrCodeBnt setBackgroundImage:[UIImage imageNamed:@"chat二维码"] forState:UIControlStateNormal];
    
    [self.contentView addSubview:self.sexBtn];
    
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerImgView.mas_top).offset(6);
        make.left.equalTo(self.headerImgView.mas_right).offset(16.5);
    }];

    [self.infoLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLbl.mas_left);
        make.bottom.equalTo(self.headerImgView.mas_bottom).offset(-5.5);
    }];
    
    [self.sexBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.sexBtn.size);
        make.centerY.mas_equalTo(self.nameLbl);
        make.left.mas_equalTo(self.nameLbl.mas_right).offset(5);
    }];
    
    [self.headerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@(-32));
        make.left.equalTo(@(17));
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    [self.qrCodeBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(25, 25));
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.centerY.equalTo(self.headerImgView.mas_centerY);
    }];
}

- (void)loadContent {
    self.nameLbl.text = UDetail.user.smartName ?: @"";
    [self.nameLbl sizeToFit];
    [self.nameLbl mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.nameLbl.size);
    }];
    
    if ([UDetail.user.sex isEqualToString:@"1"]) {
        self.sexBtn.selected = YES;
    } else {
        self.sexBtn.selected = NO;
    }
    
    self.infoLbl.text = [NSString stringWithFormat:@"UID:%@",UDetail.user.user_uid];
    
    [self.headerImgView sd_setImageWithURL:[NSURL URLWithString:UDetail.user.head_photo] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    
}

- (IBAction)editInfoClick:(id)sender {
    [MXRouter openURL:@"lcwl://ModifyInfoVC"];
}

- (IBAction)qrCodeBntClick:(id)sender {
    MXRoute(@"MyQRCodeVC", @{@"title":@"我的二维码"})
}

- (UIButton *)sexBtn {
    if (!_sexBtn) {
        _sexBtn = [UIButton new];
        [_sexBtn setImage:[UIImage imageNamed:@"icon_sex_female"] forState:UIControlStateNormal];
        [_sexBtn setImage:[UIImage imageNamed:@"icon_sex_male"] forState:UIControlStateSelected];
        [_sexBtn sizeToFit];
    }
    return _sexBtn;
}

@end
