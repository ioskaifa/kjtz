//
//  YKCDetailCell.m
//  Lcwl
//
//  Created by mac on 2018/12/12.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "WithdrawalRecordView.h"
static NSInteger padding = 15;
@interface WithdrawalRecordView()

@property (nonatomic , strong) UILabel* titleLbl;

@property (nonatomic , strong) UILabel* timeLbl;

@property (nonatomic , strong) UILabel* numLbl;

@property (nonatomic , strong) MXSeparatorLine* line;

@end

@implementation WithdrawalRecordView


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}
-(void)initUI{
    [self addSubview:self.titleLbl];
    [self addSubview:self.timeLbl];
    [self addSubview:self.numLbl];
    self.line = [MXSeparatorLine initHorizontalLineWidth:[UIScreen mainScreen].bounds.size.width orginX:0 orginY:0];
    [self addSubview:self.line];
    [self layoutSubviews];
}

-(void)layoutSubviews{
    @weakify(self)
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(padding);
        make.centerY.equalTo(self.mas_centerY).offset(-10);
    }];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.titleLbl);
        make.centerY.equalTo(self.mas_centerY).offset(10);
    }];
    
    [self.numLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.mas_right).offset(-padding);
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@(150));
        make.height.equalTo(@(20));
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.numLbl.mas_right);
        make.left.equalTo(self.titleLbl.mas_left);
        make.height.equalTo(@(0.5));
        make.bottom.equalTo(self.mas_bottom);
    }];
}

- (UILabel *)numLbl
{
    if (!_numLbl) {
        _numLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _numLbl.text = @"+100";
        _numLbl.font = [UIFont boldFont17];
        _numLbl.textAlignment = NSTextAlignmentRight;
        _numLbl.textColor = [UIColor moBlack];
    }
    return _numLbl;
}

- (UILabel *)timeLbl
{
    if (!_timeLbl) {
        _timeLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _timeLbl.font = [UIFont font12];
        _timeLbl.textColor = [UIColor lightGrayColor];
    }
    return _timeLbl;
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.font = [UIFont boldFont15];
        _titleLbl.textColor = [UIColor moDarkGray];
    }
    return _titleLbl;
}

-(void)reloadTitle:(NSString *)title time:(NSString *)time num:(NSString *)num{
    self.titleLbl.text = title;
    self.numLbl.text = num;
    self.timeLbl.text = time;
    self.numLbl.textColor = [UIColor moBlack];
    
}

@end
