//
//  MesFavoriteListVideoCell.m
//  BOB
//
//  Created by mac on 2020/8/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteListVideoCell.h"

@interface MesFavoriteListVideoCell ()

@property (nonatomic, strong) UIImageView *imgView;

@end

@implementation MesFavoriteListVideoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 100;
}

- (void)configureView:(MesFavoriteListObj *)obj {
    [super configureView:obj];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    if (obj.type != kMxmessageTypeVideo) {
        return;
    }
    NSString *shotUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, obj.attr1);
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:VideoFirstScreenShot(shotUrl)]];
}

- (void)createUI {
    [super createUI];
    [self.baseLayout addSubview:self.imgView];
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"icon_play"];
    [self.imgView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(15, 15));
        make.center.mas_equalTo(self.imgView);
    }];
    [self.baseLayout addSubview:[self bottomLayout]];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.contentMode = UIViewContentModeScaleAspectFit;
            object.backgroundColor = [UIColor moBackground];
            object.myTop = 5;
            object.myLeft = 15;
            object.height = [self.class viewHeight] - 30 - 5;
            object.myHeight = object.height;
            object.myWidth = object.height;
            object;
        });
    }
    return _imgView;
}

@end
