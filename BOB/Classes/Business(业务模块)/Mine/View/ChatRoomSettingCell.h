//
//  ChatRoomSettingCell.h
//  BOB
//
//  Created by mac on 2020/8/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatRoomSettingCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(NSString *)text detailText:(NSString *)detailText;

@end

NS_ASSUME_NONNULL_END
