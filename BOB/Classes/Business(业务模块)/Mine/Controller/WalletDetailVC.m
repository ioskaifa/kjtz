//
//  WalletDetailListVC.m
//  BOB
//
//  Created by mac on 2020/1/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletDetailVC.h"

@interface WalletDetailVC ()

@property (nonatomic, strong) NSArray *data1314Arr;

@property (nonatomic, strong) NSArray *data03Arr;

@property (nonatomic, strong) NSArray *data09Arr;

@property (nonatomic, strong) NSArray *data0723Arr;

@property (nonatomic, strong) NSArray *data1112Arr;

@property (nonatomic, strong) NSArray *data2122Arr;

@property (nonatomic, strong) NSArray *data24Arr;

@property (nonatomic, strong) NSArray *data28Arr;

@property (nonatomic, strong) NSArray *data34Arr;

@property (nonatomic, strong) NSArray *data27Arr;

@property (nonatomic, strong) NSArray *data25Arr;

@property (nonatomic, strong) NSArray *data313233Arr;

@end

@implementation WalletDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)fetchData {
    [self request:@"/api/wallet/info/getWalletRecordDetail"
            param:@{@"op_type":self.op_type?:@"",
                    @"order_id":self.op_order_no?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            self.obj = [WalletRecordDetailObj modelParseWithDict:object[@"data"][@"walletRecordDetail"]];
            self.obj.op_type = self.op_type;
            [self initHeaderView];
        }
    }];
}

- (void)initHeaderView {
    NSArray *dataArr = [self dataArr];
    if (![dataArr isKindOfClass:NSArray.class]) {
        return;
    }
    for (NSInteger i = 0; i < dataArr.count; i++) {
        NSDictionary *dic = dataArr[i];
        if (![dic isKindOfClass:NSDictionary.class]) {
            continue;
        }
        NSString *value = [self.obj valueForKey:dic.allValues.firstObject];
        if ([StringUtil isEmpty:value]) {
            continue;
        }
        if ([@"--" isEqualToString:value]) {
            continue;
        }
        
        UIView *view = [UIView factoryLeft:dic.allKeys.firstObject rightText:value];
        if (i == 0) {
            view.myTop = 20;
        }
        [self.headerView addSubview:view];
    }
    [self.headerView layoutIfNeeded];
    self.tableView.tableHeaderView = self.headerView;
}

- (void)createUI {
    [self setNavBarTitle:@"流水详情"];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
    }
    return _tableView;
}

- (MyBaseLayout *)headerView {
    if (!_headerView) {
        MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        layout.backgroundColor = [UIColor whiteColor];
        layout.myHeight = MyLayoutSize.wrap;
        layout.myWidth = SCREEN_WIDTH;
        [layout addSubview:[UIView fullLine]];
        _headerView = layout;
    }
    return _headerView;
}

- (NSArray *)dataArr {
    if ([@"03" isEqualToString:self.op_type]) {
        return self.data03Arr;
    } else if ([@"07" isEqualToString:self.op_type] ||
               [@"23" isEqualToString:self.op_type] ||
               [@"26" isEqualToString:self.op_type] ||
               [@"30" isEqualToString:self.op_type]) {
        return self.data0723Arr;
    } else if ([@"09" isEqualToString:self.op_type]) {
        return self.data09Arr;
    } else if ([@"11" isEqualToString:self.op_type] ||
               [@"12" isEqualToString:self.op_type]) {
        return self.data1112Arr;
    } else if ([@"13" isEqualToString:self.op_type] ||
               [@"14" isEqualToString:self.op_type]) {
        return self.data1314Arr;
    } else if ([@"21" isEqualToString:self.op_type] ||
               [@"22" isEqualToString:self.op_type] ||
               [@"27" isEqualToString:self.op_type] ||
               [@"29" isEqualToString:self.op_type]) {
        return self.data2122Arr;
    } else if ([@"24" isEqualToString:self.op_type]) {
        return self.data24Arr;
    } else if ([@"25" isEqualToString:self.op_type]) {
        return self.data25Arr;
    } else if ([@"28" isEqualToString:self.op_type]) {
        return self.data28Arr;
    } else if ([@"31" isEqualToString:self.op_type] ||
               [@"32" isEqualToString:self.op_type] ||
               [@"33" isEqualToString:self.op_type]) {
        return self.data313233Arr;
    } else if ([@"34" isEqualToString:self.op_type]) {
        return self.data34Arr;
    }
    return @[];
}

- (NSArray *)data03Arr {
    return @[
        @{@"订单号:":@"order_id"},
        @{@"钱包类型:":@"purse_type"},
        @{@"拨款数量:":@"num"},
        @{@"创建时间:":@"cre_time"},
        @{@"备注:":@"remark"},
    ];
}

- (NSArray *)data0723Arr {
    return @[
        @{@"订单号:":@"order_id"},
        @{@"商户订单号:":@"out_trade_no"},
        @{@"支付余额:":@"pay_balance_num"},
        @{@"支付SLA:":@"pay_sla_num"},
        @{@"支付SLA价格:":@"pay_sla_price"},
        @{@"创建时间:":@"cre_time"},
        @{@"备注:":@"remark"},
    ];
}

- (NSArray *)data1112Arr {
    return @[
        @{@"订单号:":@"order_id"},
        @{@"提币地址:":@"address"},
        @{@"提币类型:":@"send_type"},
        @{@"提币数量:":@"account"},
        @{@"手续费:":@"charge"},
        @{@"手续费率:":@"rate"},
        @{@"状态:":@"status"},
        @{@"创建时间:":@"cre_time"},
        @{@"备注:":@"remark"},
    ];
}

- (NSArray *)data1314Arr {
    return @[
        @{@"订单号:":@"order_id"},
        @{@"订单类型:":@"order_type"},
        @{@"商户订单号:":@"out_trade_no"},
        @{@"商品数量:":@"goods_num"},
        @{@"快递费:":@"express_cash"},
        
        @{@"供应链运费:":@"freight_fee"},
        @{@"商品金额:":@"cash_num"},
        @{@"订单总金额:":@"total_cash_num"},
        @{@"订单折扣金额::":@"discount_cash_num"},
        @{@"支付类型:":@"order_pay_type"},
        
        @{@"支付数量:":@"pay_num"},
        @{@"支付余额:":@"pay_balance_num"},
        @{@"支付SLA:":@"pay_sla_num"},
        @{@"支付SLA价格:":@"pay_sla_price"},
        @{@"支付SLA折扣比例:":@"sla_discount_rate"},
        @{@"状态:":@"status"},
        
        @{@"创建时间:":@"cre_time"},
        @{@"更新时间:":@"up_date"},
        @{@"支付时间:":@"pay_date"},
        @{@"发货时间:":@"send_date"},
        @{@"收货时间:":@"receive_date"},
        
        @{@"是否免单:":@"is_free"},
        @{@"免单状态::":@"free_status"},
        @{@"备注:":@"remark"},
    ];
}

- (NSArray *)data2122Arr {
    return @[
        @{@"卖家账号:":@"user_tel"},
        @{@"卖家昵称:":@"nick_name"},
        @{@"奖励金额:":@"benefit_money"},
        @{@"奖励比例:":@"rate"},
        @{@"SLA价格:":@"sla_price"},
        
        @{@"SLA数量:":@"sla_num"},
        @{@"流水号:":@"order_no"},
        @{@"购物订单号:":@"buy_order_no"},
        @{@"创建时间:":@"cre_time"},
        @{@"备注:":@"remark"},
    ];
}

- (NSArray *)data24Arr {
    return @[
        @{@"订单号:":@"order_no"},
        @{@"充币地址:":@"address"},
        @{@"充币数量:":@"number"},
        @{@"充币HASH值:":@"hashString"},
        @{@"创建时间:":@"cre_time"},
        @{@"备注:":@"remark"},
    ];
}

- (NSArray *)data09Arr {
    return @[
        @{@"数量:":@"num"},
        @{@"创建时间:":@"cre_time"},
    ];
}

- (NSArray *)data28Arr {
    return @[
//        @{@"商户订单号:":@"out_trade_no"},
        @{@"数量:":@"goods_num"},
        @{@"快递费:":@"express_cash"},
        
        @{@"订单金额:":@"cash_num"},
        @{@"订单总金额:":@"total_cash_num"},
        @{@"折扣金额:":@"discount_cash_num"},
        //（01:":@"支付宝  02:":@"微信  04:":@"SLA）
        @{@"支付类型:":@"order_pay_type"},
        //（支付宝和微信）
        //    @{@"支付金额:":@"pay_num"},
        //（00-待付款 02-待发货 03-待收货 04-已取消 05-退款 08-已冻结 09-交易完成）
        
        @{@"状态:":@"status"},
        @{@"支付SLA:":@"pay_sla_num"},
        @{@"支付SLA价格:":@"pay_sla_price"},
        @{@"折扣比例:":@"sla_discount_rate"},
        @{@"创建时间:":@"cre_date"},
        
        @{@"更新时间:":@"up_date"},
        @{@"支付时间:":@"pay_date"},
        @{@"发货时间:":@"send_date"},
        @{@"收货时间:":@"receive_date"},
    ];
}

- (NSArray *)data34Arr {
    return @[
        @{@"订单号:":@"order_id"},
//        @{@"用户编号:":@"user_id"},
//        @{@"商户订单号:":@"out_trade_no"},
        @{@"数量:":@"goods_num"},
        @{@"快递费:":@"express_cash"},
        
        @{@"订单金额:":@"cash_num"},
        @{@"订单总金额:":@"total_cash_num"},
        @{@"折扣金额:":@"discount_cash_num"},
        @{@"支付类型:":@"order_pay_type"},//（01:":@"支付宝  02:":@"微信  04:":@"SLA）
//        @{@"支付人民币金额:":@"pay_num"},//（支付宝和微信）
        
        @{@"状态:":@"status"},//（00-待付款 02-待发货 03-待收货 04-已取消 05-退款  08-已冻结 09-交易完成 10-订单已失效）
        @{@"支付SLA:":@"pay_sla_num"},
        @{@"支付SLA价格:":@"pay_sla_price"},
        @{@"折扣比例:":@"sla_discount_rate"},
        @{@"创建时间:":@"cre_date"},
        
        @{@"更新时间:":@"up_date"},
        @{@"支付时间:":@"pay_date"},
        @{@"发货时间:":@"send_date"},
        @{@"收货时间:":@"receive_date"},
    ];
}

- (NSArray *)data27Arr {
    return @[
    @{@"奖金金额:":@"benefit_money"},
    @{@"SLA价格:":@"sla_price"},
    @{@"奖励SLA数量:":@"sla_num"},
    @{@"订单号:":@"order_no"},
    @{@"奖励比例:":@"rate"},
    @{@"奖励来源订单编号:":@"buy_order_id"},
    
    @{@"奖励来源订单号:":@"buy_order_no"},
    @{@"奖励来源用户手机号:":@"user_tel"},
    @{@"奖励来源用户昵称:":@"nick_name"},
    @{@"奖励时间:":@"cre_time"},
    ];
}

- (NSArray *)data25Arr {
    return @[
    @{@"订单号:":@"order_no"},
//    @{@"用户编号:":@"user_id"},
//    @{@"礼物编号:":@"gift_id"},
    @{@"礼物名称:":@"gift_name"},
    //@{@"gift_show:":@"礼物展示图"},
    @{@"礼物SLA数量:":@"gift_sla_num"},
    
    @{@"礼物数量:":@"gift_num"},
    @{@"支付SLA:":@"pay_sla_num"},
//    @{@"主播编号:":@"anchor_user_id"},
//    @{@"直播编号:":@"live_id"},
    @{@"创建时间:":@"cre_time"},
    ];
}

- (NSArray *)data313233Arr {
    return @[
    @{@"订单号:":@"order_no"},
//    @{@"用户编号:":@"user_id"},
//    @{@"商品编号:":@"goods_id"},
    //@{@"goods_show:":@"展示图"},
    @{@"商品名称:":@"goods_name"},
//    @{@"商品号:":@"goods_no"},
    
    @{@"状态:":@"status"},//（00-待开奖 02-正在开奖 08-拼团失败 09-已开奖 ）
    @{@"退还状态:":@"refund_status"},//（00-奖金待返还 02-奖金返还中   09-奖金返还成功）
    @{@"支付SLA数量:":@"pay_sla_num"},
    @{@"退还SLA数量:":@"refund_sla_num"},
//    @{@"退还SLA比例（%）:":@"refund_ben_rate"},
    
    @{@"是否中奖:":@"is_win"},//（0-否 1-是）
    @{@"中奖用户开奖商品订单号:":@"win_order_no"},
    @{@"拼团支付SLA数量:":@"spell_sla_num"},
    @{@"单个商品拼团参与人数:":@"spell_peo_num"},
//    @{@"返还比例（%）:":@"spell_ben_rate"},
    
    @{@"拼团代金券:":@"spell_cash_coupon"},
    @{@"市场价格:":@"goods_market_cash"},
    @{@"抵扣券状态:":@"coupon_status"},//（00-待使用 04-已失效 09-已使用）
    @{@"创建时间:":@"cre_date"},
    @{@"更新时间:":@"up_date"},
    
    @{@"退款时间:":@"refund_date"},
    @{@"中奖时间:":@"win_date"},
    ];
}

@end
