//
//  MyGroupVC.m
//  BOB
//
//  Created by mac on 2019/7/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MyGroupVC.h"

@interface MyGroupVC ()

@end

@implementation MyGroupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNavBarLeftBtnImg:@"我的团队"];
    [self setNavBarRightBtnWithTitle:nil andImageName:@"消息中心"];
    [self updateForLanguageChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)updateForLanguageChanged {
    
}

- (void)navBarRightBtnAction:(id)sender {
    
}

#pragma mark - setup

- (void)configTableView:(UITableView *)tableView
{
//    [tableView std_registerCellNibClass:[MineHeaderCell class]];
//    [tableView std_registerCellClass:[UITableViewCell class]];
//    [tableView std_registerCellNibClass:[MineWalletRemainingTC class]];
//
//    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[MineHeaderCell class]];
//    [tableView std_addSection:sectionData];
//
//    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
//    [tableView std_addSection:sectionData];
//
//    sectionData = [[STDTableViewSection alloc] initWithCellClass:[MineWalletRemainingTC class]];
//    [tableView std_addSection:sectionData];
//
//    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
//    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
//    NSArray *itemList = @[[BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"订单" text:Lang(@"我的订单") rightIcon:@"Arrow" tip:@"3" jumpVC:@""] cellHeight:50],
//                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"团队" text:Lang(@"我的团队") rightIcon:@"Arrow" jumpVC:@""] cellHeight:50],
//                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"收货地址" text:Lang(@"收货地址") rightIcon:@"Arrow" jumpVC:@"MyAddressListVC"] cellHeight:50],
//                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"充值记录" text:Lang(@"充值记录") rightIcon:@"Arrow" jumpVC:@""] cellHeight:50],
//                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"提现记录" text:Lang(@"提现记录") rightIcon:@"Arrow" jumpVC:@""] cellHeight:50],
//                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"意见反馈" text:Lang(@"意见反馈") rightIcon:@"Arrow" jumpVC:@"HelpViewController"] cellHeight:50],
//                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"设置" text:Lang(@"设置") rightIcon:@"Arrow" jumpVC:@"SettingVC"] cellHeight:50]];
//
//
//    [self.tableView std_addItems:@[[MineHeaderCell cellItemWithData:nil cellHeight:210]] atSection:0];
//    [self.tableView std_addItems:@[[MineWalletRemainingTC cellItemWithData:@"1,008,612.00" cellHeight:80]] atSection:1];
//    [self.tableView std_addItems:itemList atSection:2];
//
//    //[self.tableView std_insertRows:itemList.count atEmptySection:0 withRowAnimation:UITableViewRowAnimationNone];
//
//    //[self.tableView std_insertSection:2 withRowAnimation:UITableViewRowAnimationNone];
//    [self.tableView reloadData];
}

@end
