//
//  MesFavoriteTextInfoVC.m
//  BOB
//
//  Created by mac on 2020/7/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteTextInfoVC.h"
#import <YYKit/YYKit.h>
#import "YYTextHelper.h"

@interface MesFavoriteTextInfoVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YYLabel *contentLbl;

@end

@implementation MesFavoriteTextInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)createUI {
    [self setNavBarTitle:@"详情"];
    [self.view addSubview:self.tableView];
    [self layoutUI];
}

- (void)layoutUI {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (MyBaseLayout *)tableHeaderView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font12];
    lbl.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
    lbl.text = StrF(@"来自%@ %@", self.obj.send_name, self.obj.format_send_time);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myTop = 10;
    [layout addSubview:lbl];
    
    NSString* content = self.obj.attr1;
    // 测试文本
    NSString *text = content;
    // 转成可变属性字符串
    NSMutableAttributedString * mAttributedString = [NSMutableAttributedString new];
    // 调整行间距段落间距
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    [paragraphStyle setLineSpacing:0];
    [paragraphStyle setParagraphSpacing:0];

    // 设置文本属性
    NSDictionary *attri = [NSDictionary dictionaryWithObjects:@[[UIFont font14], [UIColor blackColor], paragraphStyle] forKeys:@[NSFontAttributeName, NSForegroundColorAttributeName, NSParagraphStyleAttributeName]];
    [mAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:text attributes:attri]];

    // 匹配条件
    NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";

    NSError *error = NULL;
    // 根据匹配条件，创建了一个正则表达式
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    if (!regex) {
        NSLog(@"正则创建失败error！= %@", [error localizedDescription]);
    } else {
        NSArray *allMatches = [regex matchesInString:mAttributedString.string options:NSMatchingReportCompletion range:NSMakeRange(0, mAttributedString.string.length)];
        for (NSTextCheckingResult *match in allMatches) {
            NSString *substrinsgForMatch2 = [mAttributedString.string substringWithRange:match.range];
            NSMutableAttributedString *one = [[NSMutableAttributedString alloc] initWithString:substrinsgForMatch2];
            // 利用YYText设置一些文本属性
            one.font = [UIFont font15];
            one.underlineStyle = NSUnderlineStyleSingle;
            one.color = [UIColor colorWithRed:0.093 green:0.492 blue:1.000 alpha:1.000];
            
            YYTextBorder *border = [YYTextBorder new];
            border.cornerRadius = 3;
    //                border.insets = UIEdgeInsetsMake(-2, -1, -2, -1);
            border.insets = UIEdgeInsetsMake(-2, 0, -2, 0);
            border.fillColor = [UIColor colorWithWhite:0.000 alpha:0.220];
            
            YYTextHighlight *highlight = [YYTextHighlight new];
            [highlight setBorder:border];
            [one setTextHighlightRange:one.rangeOfAll color:[UIColor blueColor]  backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                NSString* urlStr = [text.string substringWithRange:range];
                NSURL * url = [NSURL URLWithString:urlStr];
                if ([[UIApplication sharedApplication]canOpenURL:url]) {
                    [[UIApplication sharedApplication]openURL:url options:@{UIApplicationLaunchOptionsURLKey : @YES} completionHandler:^(BOOL success) {
                        //成功后的回调
                        if (!success) {
                            //失败后的回调
                        }
                    }];
                }else{
                    NSString* str = [NSString stringWithFormat:@"http://%@",urlStr];
                    url = [NSURL URLWithString:str];
                    [[UIApplication sharedApplication]openURL:url options:@{UIApplicationLaunchOptionsURLKey : @YES} completionHandler:^(BOOL success) {
                        //成功后的回调
                        if (!success) {
                            //失败后的回调
                            [NotifyHelper showMessageWithMakeText:@"链接无法打开"];
                        }
                    }];
                }
               
            }];
            
            // 根据range替换字符串
            [mAttributedString replaceCharactersInRange:match.range withAttributedString:one];
        }
    }
    self.contentLbl.attributedText = mAttributedString;
    [self.contentLbl MXupdateOuterTextProperties];

    YYTextContainer *container = [YYTextContainer containerWithSize:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    YYTextLayout *textLayout = [YYTextLayout layoutWithContainer:container text:self.contentLbl.attributedText];
    self.contentLbl.textLayout = textLayout;
    self.contentLbl.mySize = textLayout.textBoundingSize;
    [layout addSubview:self.contentLbl];
    [layout layoutSubviews];
    return layout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
        _tableView.tableHeaderView = [self tableHeaderView];
    }
    return _tableView;
}

- (YYLabel *)contentLbl {
    if (!_contentLbl) {
        _contentLbl = [self.class emojiLabel];
        _contentLbl.userInteractionEnabled = YES;
        _contentLbl.myLeft = 15;
        _contentLbl.myRight = 15;
        _contentLbl.myTop = 10;
    }
    return _contentLbl;
}

+ (YYLabel *)emojiLabel{
    YYLabel *yyLabel = [YYTextHelper yyLabel];
    yyLabel.font = [UIFont font17];
    return yyLabel;
}

@end
