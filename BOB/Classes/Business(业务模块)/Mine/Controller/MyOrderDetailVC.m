//
//  MyOrderDetailVC.m
//  BOB
//
//  Created by mac on 2019/7/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MyOrderDetailVC.h"
#import "AutoTableViewCell.h"

@interface MyOrderDetailVC ()

@end

@implementation MyOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:Lang(@"详情")];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - setup
- (void)configTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor moBackground];
    
    [tableView std_registerCellClass:[BaseSTDTableViewCell class]];
    [tableView std_registerCellClass:[AutoTableViewCell class]];

    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];

    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
    NSMutableAttributedString *attStr = [NSMutableAttributedString initWithTitles:@[@"+RB 100.00",@"红包-来自天空之城机械人"] colors:@[[UIColor blackColor],[UIColor colorWithHexString:@"#5A5C5C"]] fonts:@[[UIFont systemFontOfSize:25],[UIFont systemFontOfSize:15]] placeHolder:@"\n"];
    [attStr setLineSpacing:5 substring:@"+RB 100.00" alignment:NSTextAlignmentLeft];
    BaseSTDCellModel *model = [[BaseSTDCellModel alloc] init];
    model.text = attStr;
    model.textNumberOfLines = 2;
    model.detailTextFont = [UIFont systemFontOfSize:13];
    model.detailText = LangLRPH(@"已存入钱包", @"  ");
    model.detailTextCornerRadius = 3;
    model.detailTextBorderWidth = 1;
    model.detailTextBorderColor = [UIColor colorWithHexString:@"#F0223A"];
    model.detailTextColor = [UIColor colorWithHexString:@"#F0223A"];
    model.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    
    BaseSTDCellModel *model1 = [BaseSTDCellModel model:Lang(@"红包详情") detailText:Lang(@"查看") jumpVC:@"" rightIcon:@"Arrow"];
    model1.hideBottomLine = YES;
    model1.detailTextColor = [UIColor themeColor];

    BaseSTDCellModel *model2 = [BaseSTDCellModel model:Lang(@"收款时间") detailText:@"2019-06-16 12:16" rightIcon:@"" jumpVC:@""];
    model2.hideBottomLine = YES;

    BaseSTDCellModel *model3 = [BaseSTDCellModel model:LangRPH(@"订单编号", @"    ") detailText:@"0xbb7fb5effafd78d9b2e211a40a73adba80d0381fb2e211a40a73adba80d0381f0xbb7fb5effafd78d9b2e211a40a73adba80d00381f" rightIcon:@"" jumpVC:@""];
    model3.hideBottomLine = YES;
    model3.textNumberOfLines = 0;
    model3.detailTextNumberOfLines = 0;
    model3.textColor = [UIColor colorWithHexString:@"#323333"];
    model3.detailTextColor = [UIColor colorWithHexString:@"#AFB2B3"];
    model3.textFont = [UIFont systemFontOfSize:14];
    model3.detailTextFont = [UIFont systemFontOfSize:14];

    NSArray *itemList = @[[BaseSTDTableViewCell cellItemWithData:model1 cellHeight:60],
                          [BaseSTDTableViewCell cellItemWithData:model2 cellHeight:60],
                          [AutoTableViewCell cellItemWithData:model3 cellHeight:UITableViewAutomaticDimension]];


    [self.tableView std_addItems:@[[BaseSTDTableViewCell cellItemWithData:model cellHeight:120]] atSection:0];
    [self.tableView std_addItems:itemList atSection:1];

    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    //self.tableView.rowHeight = UITableViewAutomaticDimension;
    //[self.tableView std_insertRows:itemList.count atEmptySection:0 withRowAnimation:UITableViewRowAnimationNone];

    //[self.tableView std_insertSection:2 withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView reloadData];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return UITableViewAutomaticDimension;
//}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return UITableViewAutomaticDimension;
//}

@end
