//
//  ChangeHeaderVC.h
//  Lcwl
//
//  Created by mac on 2018/11/23.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangeHeaderVC : BaseViewController
@property(nonatomic, copy) NSString *imageUrl;
@property(nonatomic, copy) FinishedBlock block;
@end

NS_ASSUME_NONNULL_END
