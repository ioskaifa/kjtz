//
//  MyQRCodeVC.m
//  Lcwl
//
//  Created by AlphaGO on 2018/11/16.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "MyQRCodeVC.h"

@interface MyQRCodeVC ()

@property (strong, nonatomic)  UIImageView *logo;
@property (strong, nonatomic)  UILabel *name;
@property (strong, nonatomic)  UIView *whiteBG;
@property (strong, nonatomic)  UIImageView *qrCode;
@property (strong, nonatomic)  UIImageView *watermarkImgView;
@property (strong, nonatomic)  UIButton *savePicBtn;

@end

@implementation MyQRCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self layout];
    [self configureView];
}

- (void)configureView {
    self.name.text = UDetail.user.smartName ?: @"";
    NSURL *url = [NSURL URLWithString:UDetail.user.head_photo];
    [self.logo sd_setImageWithURL:url
                 placeholderImage:[UIImage imageNamed:@"avatar_default"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        self.watermarkImgView.image = image;
        self.watermarkImgView.hidden = NO;
    }];
    
    UIImage *qrCode = [UIImage encodeQRImageWithContent:[NSString stringWithFormat:@"user?userId=%@",UDetail.user.chatUser_id] size:CGSizeMake(SCREEN_HEIGHT, SCREEN_HEIGHT)];
    self.qrCode.image = qrCode;
}

- (void)savePicToLocal {
    UIImage *image = [self.whiteBG convertViewToImageWithCornerRadius:self.whiteBG.layer.cornerRadius];
    if(image != nil) {
        UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        [NotifyHelper showMessageWithMakeText:@"保存失败"];
    } else {
        [NotifyHelper showMessageWithMakeText:@"保存成功"];
    }
}

-(void)setupUI{
    if ([StringUtil isEmpty:self.title]) {
        [self setNavBarTitle:@"我的二维码"];
    } else {
        [self setNavBarTitle:self.title color:[UIColor moBlack]];
    }
    self.edgesForExtendedLayout = UIRectEdgeAll;

    [self.view addSubview:self.whiteBG];
    [self.view addSubview:self.savePicBtn];

    [self.whiteBG addSubview:self.logo];
    [self.whiteBG addSubview:self.name];
    [self.whiteBG addSubview:self.qrCode];
    [self.qrCode addSubview:self.watermarkImgView];
}

- (void)layout {
    [self.logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBG).offset(10);
        make.centerX.equalTo(self.whiteBG);
        make.width.height.mas_equalTo(self.logo.height);
    }];
    
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.logo.mas_bottom).offset(5);
        make.left.equalTo(self.whiteBG).offset(10);
        make.right.equalTo(self.whiteBG).offset(-10);
    }];
     
    [self.qrCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.name.mas_bottom).offset(15);
        make.left.equalTo(@(20));
        make.right.equalTo(@(-20));
        make.height.equalTo(self.qrCode.mas_width);
    }];

    [self.whiteBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.bottom.equalTo(self.qrCode.mas_bottom).offset(20);
        make.top.equalTo(self.view.mas_top).offset(80);
    }];
    
    [self.savePicBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBG.mas_bottom).mas_offset(10);
        make.left.right.equalTo(self.whiteBG);
        make.height.equalTo(@(40));
    }];
    
    [self.watermarkImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.watermarkImgView.size);
        make.center.mas_equalTo(self.qrCode);
    }];
}

-(UIImageView *)qrCode{
    if (!_qrCode) {
        _qrCode = [[UIImageView alloc]initWithImage: [UIImage imageNamed:@"my_qr_bg"]];
        _qrCode.backgroundColor = [UIColor moBackground];
    }
    return _qrCode;
}

-(UIView *)whiteBG{
    if (!_whiteBG) {
        _whiteBG = [[UIView alloc]init];
        _whiteBG.backgroundColor = [UIColor whiteColor];
        _whiteBG.clipsToBounds = YES;
        _whiteBG.layer.cornerRadius = 10;
    }
    return _whiteBG;
}
-(UIImageView *)logo{
    if (!_logo) {
        _logo = [[UIImageView alloc]init];
        _logo.size = CGSizeMake(60, 60);
        [_logo setViewCornerRadius:5];
    }
    return _logo;
}

-(UIImageView *)watermarkImgView{
    if (!_watermarkImgView) {
        _watermarkImgView = [UIImageView new];
        _watermarkImgView.size = CGSizeMake(60, 60);
        [_watermarkImgView setViewCornerRadius:5];
//        _watermarkImgView.layer.borderWidth = 1;
//        _watermarkImgView.layer.borderColor = [UIColor whiteColor].CGColor;
        _watermarkImgView.hidden = YES;
    }
    return _watermarkImgView;
}

-(UILabel *)name{
    if (!_name) {
        _name = [[UILabel alloc]init];
        _name.textAlignment = NSTextAlignmentCenter;
        _name.font = [UIFont systemFontOfSize:15];
        _name.textColor = [UIColor blackColor];
    }
    return _name;
}

- (UIButton *)savePicBtn {
    if (!_savePicBtn) {
        _savePicBtn = [UIButton new];
        [_savePicBtn setViewCornerRadius:10];
        _savePicBtn.titleLabel.font = [UIFont font15];
        [_savePicBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_savePicBtn setTitle:@"保存图片" forState:UIControlStateNormal];
        [_savePicBtn setBackgroundColor:[UIColor whiteColor]];
        @weakify(self)
        [_savePicBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self savePicToLocal];
        }];
    }
    return _savePicBtn;
}

@end
