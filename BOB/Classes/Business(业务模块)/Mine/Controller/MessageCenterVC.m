//
//  MessageCenterVC.m
//  BOB
//
//  Created by mac on 2019/7/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MessageCenterVC.h"
#import "MessageCenterCell.h"
#import "MessageCenterModel.h"
#import "YJSliderView.h"

@interface MessageCenterVC ()<YJSliderViewDelegate>

@property (nonatomic, strong) YJSliderView *sliderView;

@property (nonatomic, strong) NSArray *sliderViewArr;

@property (nonatomic, assign) NSInteger currentIndex;

@end

@implementation MessageCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNavBarTitle:Lang(@"消息中心")];
    [self.view addSubview:self.sliderView];
    self.currentIndex = 0;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentAutomatic;
    } else {
        self.automaticallyAdjustsScrollViewInsets = YES;
    }
    [self.sliderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
    }];
    [self.view removeConstraints:self.tableView.constraints];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sliderView.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
}

- (void)addTableViewConstraints {
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - Override Super Class Method
- (HTTPRequestMethod)httpMethod {
    return HTTPRequestMethodPost;
}

- (NSDictionary *)requestArgument {
    if (self.currentIndex == 0) {
        return @{@"notice_type":@"01"};
    } else {
        return @{@"notice_type":@"02"};
    }
}

- (NSString *)requestUrl {
    return @"api/system/notice/getSysNoticeList";
}

///请求时传入last id的key
- (NSString *)httpRequstLastIdKey {
    return @"last_id";
}

///上拉刷新时从数据源数组中最后一个元素取值的key
- (NSString *)getValueFromItemLastIdKey {
    return @"_id";;
}

///后台返回数据中数组的keypath
- (NSString *)dataListKeyPath {
    return @"data.sysNoticeList";
}

///数据源中的model,需要继承BaseObject
- (Class)model {
    return [MessageCenterModel class];
}

///是否需要上拉刷新
- (BOOL)needPullUpRefresh {
    return YES;
}

///是否需要下拉刷新
- (BOOL)needPullDownRefresh {
    return YES;
}

#pragma mark YJSliderViewDelegate
- (NSInteger)numberOfItemsInYJSliderView:(YJSliderView *)sliderView {
    return self.sliderViewArr.count;
}

- (UIView *)yj_SliderView:(YJSliderView *)sliderView viewForItemAtIndex:(NSInteger)index {
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (NSString *)yj_SliderView:(YJSliderView *)sliderView titleForItemAtIndex:(NSInteger)index {
    if (index >= self.sliderViewArr.count) {
        return @"";
    }
    return self.sliderViewArr[index];
}

- (NSInteger)initialzeIndexFoYJSliderView:(YJSliderView *)sliderView {
    return 0;
}

- (void)yj_SliderView:(YJSliderView *)sliderView didSelectItemAtIndex:(NSIndexPath *)indexPath {
    if (self.currentIndex == indexPath.row) {
        return;
    }
    self.currentIndex = indexPath.row;
    [self reloadData];
}

#pragma mark - setup
///配置tableVeiw
- (void)configTableView:(UITableView *)tableView
{
    tableView.rowHeight = 65;
    
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellNibClass:[MessageCenterCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[MessageCenterCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
    
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    MessageCenterModel *model = [tableView std_itemAtIndexPath:indexPath].data;
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[model.notice_content dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    MXRoute(@"ShowPlainextVC", (@{@"content": attrStr ?: @"",
                                  @"title":model.notice_title,
                                  @"time":model.cre_date,
                                }));
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (YJSliderView *)sliderView {
    if (!_sliderView) {
        _sliderView = [[YJSliderView alloc] initWithFrame:CGRectZero];
        _sliderView.delegate = self;
        _sliderView.themeColor = [UIColor moGreen];
        _sliderView.lineColor = [UIColor moGreen];
    }
    return _sliderView;
}

- (NSArray *)sliderViewArr {
    if (!_sliderViewArr) {
        _sliderViewArr = @[@"数字商城", @"免单商城"];
    }
    return _sliderViewArr;
}

@end
