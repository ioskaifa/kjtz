//
//  WalletFlowVC.m
//  BOB
//
//  Created by mac on 2020/1/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletFlowVC.h"

@interface WalletFlowVC ()

@end

@implementation WalletFlowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:Lang(@"钱包分类")];
}

#pragma mark - Override Super Class Method
- (HTTPRequestMethod)httpMethod {
    return HTTPRequestMethodPost;
}

- (NSString *)requestUrl {
    return @"index/flow";
}

- (NSDictionary *)requestArgument {
    return @{@"dtype": @"1",@"type": @"0"};
}

///请求时传入pageNum的key
- (NSString *)pageNumKey {
    return @"page";
}

///分页pagenum的默认值
- (NSString *)pageNumDefaultValue {
    return @"1";
}

///后台返回数据中数组的keypath
- (NSString *)dataListKeyPath {
    return @"data";
}

///数据源中的model,需要继承BaseObject
- (Class)model {
    return NSClassFromString(@"WalletFlowModel");
}

///是否需要上拉刷新
- (BOOL)needPullUpRefresh {
    return YES;
}

///是否需要下拉刷新
- (BOOL)needPullDownRefresh {
    return YES;
}

#pragma mark - setup
///配置tableVeiw
- (void)configTableView:(UITableView *)tableView
{
    //tableView.rowHeight = UITableViewAutomaticDimension;
    //tableView.estimatedRowHeight = 100;
    
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellClass:[BaseSTDTableViewCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
    
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 77;
}

@end
