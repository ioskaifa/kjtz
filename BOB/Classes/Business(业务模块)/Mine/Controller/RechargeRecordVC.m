//
//  WithdrawRecordVC.m
//  BOB
//
//  Created by mac on 2019/7/5.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "RechargeRecordVC.h"
#import "MineHelper.h"
#import "WithdrawalRecordView.h"
#import "WithdrawalRecordModel.h"
@interface RechargeRecordVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic , strong) UITableView* tableView;

@property (nonatomic , strong) NSString* last_id;

@property (nonatomic , strong) NSMutableArray* orignArray;

@property (nonatomic , strong) NSMutableArray* dataArray;

@property (nonatomic , strong) NSString* keys;

@end

@implementation RechargeRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self initData];
}

-(void)initUI{
    [self setNavBarTitle:@"充值记录"];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

-(void)initData{
    self.orignArray = [[NSMutableArray alloc]initWithCapacity:10];
    self.dataArray = [[NSMutableArray alloc]initWithCapacity:10];
    self.keys = @"";
    self.last_id = @"";
    @weakify(self)
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self)
        [MineHelper getUserAcceptList:@{@"token":UDetail.user.token,@"last_id":self.last_id} completion:^(id array, NSString *error) {
            [self.tableView.mj_footer endRefreshing];
            if (array) {
                NSMutableArray* tmpArray = array;
                WithdrawalRecordModel* lastModel = tmpArray.lastObject;
                if (lastModel) {
                    self.last_id = lastModel.id;
                }else{
                    self.last_id = @"";
                }
                [self.orignArray addObjectsFromArray:tmpArray];
                self.keys = [WithdrawalRecordModel getRecordKeys:self.orignArray];
                self.dataArray = [WithdrawalRecordModel getSequRecords:self.orignArray keys:self.keys];
                [self.tableView reloadData];
            }
        }];
    }];

    self.tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        @strongify(self)
        self.last_id = @"";
        [MineHelper getUserAcceptList:@{@"token":UDetail.user.token,@"last_id":self.last_id} completion:^(id array, NSString *error) {
            [self.tableView.mj_header endRefreshing];
            if (array) {
                NSMutableArray* tmpArray = array;
                WithdrawalRecordModel* lastModel = tmpArray.lastObject;
                if (lastModel) {
                    self.last_id = lastModel.id;
                }else{
                    self.last_id = @"";
                }
                [self.orignArray removeAllObjects];
                self.orignArray = tmpArray;
                self.keys = [WithdrawalRecordModel getRecordKeys:self.orignArray];
                self.dataArray = [WithdrawalRecordModel getSequRecords:self.orignArray keys:self.keys];
                [self.tableView reloadData];
            }
        }];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
}

- (UITableView*)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _tableView;
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray* array = self.dataArray[section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    header.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, SCREEN_WIDTH-30, 30)];
    [header addSubview:label];
    label.textColor = [UIColor colorWithHexString:@"#1E1F1F"];
    label.font = [UIFont systemFontOfSize:13];
    label.text = [self.keys componentsSeparatedByString:@"|"][section];
    return header;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}
#pragma mark - Table view delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        WithdrawalRecordView* view = [[WithdrawalRecordView alloc]initWithFrame:CGRectZero];
        view.tag = 101;
        [cell.contentView addSubview:view];
        
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
    }
    WithdrawalRecordView* view = [cell.contentView viewWithTag:101];
    NSArray* array = [self.dataArray objectAtIndex:indexPath.section];
    WithdrawalRecordModel* model = array[indexPath.row];
    [view reloadTitle:model.order_id time:model.cre_date num:model.account];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray* array = [self.dataArray objectAtIndex:indexPath.section];
    WithdrawalRecordModel* model = array[indexPath.row];
    [MXRouter openURL:@"lcwl://WithdrawDetailVC" parameters:@{@"model":model}];
}

@end
