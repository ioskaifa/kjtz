//
//  WalletListVC.m
//  BOB
//
//  Created by mac on 2020/1/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletListVC.h"
#import "BaseSTDTableViewCell.h"
#import "MineHeaderCell.h"
#import "ModifyInfoVC.h"
#import "MXAlertViewHelper.h"

@interface WalletListVC ()

@end

@implementation WalletListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"钱包"];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self updateForLanguageChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.tableView reloadData];
}

- (void)updateForLanguageChanged {
    //[self setNavBarTitle:Lang(@"个人中心")];
}

- (void)navBarRightBtnAction:(id)sender {
    [MXRouter openURL:@"lcwl://MessageCenterVC"];
}

#pragma mark - setup
- (void)configTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellNibClass:[MineHeaderCell class]];
    [tableView std_registerCellClass:[BaseSTDTableViewCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[MineHeaderCell class]];
    [tableView std_addSection:sectionData];
    
    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
    
    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
    
    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
    BaseSTDCellModel *model1 = [BaseSTDCellModel model:@"银行卡" text:Lang(@"银行卡") rightIcon:@"Arrow" jumpVC:@"PaymentHomeVC"];
    model1.jumpParam = @{@"selectIndex":@(2)};
    
    BaseSTDCellModel *model2 = [BaseSTDCellModel model:@"微信" text:Lang(@"微信") rightIcon:@"Arrow" jumpVC:@"PaymentHomeVC"];
    model2.jumpParam = @{@"selectIndex":@(1)};
    
    BaseSTDCellModel *model3 = [BaseSTDCellModel model:@"支付宝" text:Lang(@"支付宝") rightIcon:@"Arrow" jumpVC:@"PaymentHomeVC"];
    model3.jumpParam = @{@"selectIndex":@(0)};
    
    [self.tableView std_addItems:@[[BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"零钱" text:@"零钱" detailText:@"零钱" rightIcon:@"Arrow" jumpVC:@"LooseChangeVC"] cellHeight:53]] atSection:0];
    [self.tableView std_addItems:@[[BaseSTDTableViewCell cellItemWithData:model1 cellHeight:53],
                                   [BaseSTDTableViewCell cellItemWithData:model2 cellHeight:53],
                                   [BaseSTDTableViewCell cellItemWithData:model3 cellHeight:53]] atSection:1];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

@end
