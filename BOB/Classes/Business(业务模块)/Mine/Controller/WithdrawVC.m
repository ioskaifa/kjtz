//
//  WithdrawVC.m
//  BOB
//
//  Created by mac on 2019/7/3.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "WithdrawVC.h"
#import "YQPayKeyWordVC.h"

@interface WithdrawVC ()
@property (weak, nonatomic) IBOutlet UIImageView *blueBG;
@property (weak, nonatomic) IBOutlet UIImageView *logoImgView;
@property (weak, nonatomic) IBOutlet UILabel *moneyLbl;
@property (weak, nonatomic) IBOutlet UIView *whiteBG;

@property (weak, nonatomic) IBOutlet UILabel *widrawAddressLbl;
@property (weak, nonatomic) IBOutlet UILabel *widrawMoneyLbl;
@property (weak, nonatomic) IBOutlet UITextField *widrawAddressTF;
@property (weak, nonatomic) IBOutlet UITextField *widrawMoneyTF;
@property (weak, nonatomic) IBOutlet UIButton *addressPastBnt;

@property (weak, nonatomic) IBOutlet UILabel *feeLbl;
@property (weak, nonatomic) IBOutlet UILabel *feeValueLbl;
@property (weak, nonatomic) IBOutlet UIButton *sureBnt;
@end

@implementation WithdrawVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    [self setNavBarTitle:Lang(@"提币")];
    [self setNavBarRightBtnWithTitle:Lang(@"提币记录") andImageName:nil];
    
    self.moneyLbl.text = UDetail.user.sla_num;
    self.widrawAddressLbl.text = Lang(@"提币地址");
    self.widrawAddressTF.placeholder = Lang(@"请输入提币地址");
    
    self.widrawMoneyLbl.text = Lang(@"提币金额");
    self.widrawMoneyTF.placeholder = @"0.00";
    
    self.feeLbl.text = Lang(@"手续费：");
    self.feeValueLbl.text = @"0.00 RB";
    
    [self.addressPastBnt setTitle:LangLRPH(@"粘贴",@"  ") forState:UIControlStateNormal];
    [self.sureBnt setTitle:LangLRPH(@"确定提币",@"         ") forState:UIControlStateNormal];
    
    
    [self.widrawAddressTF setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    [self.addressPastBnt setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    
    [UDetail.user updateWallet:^(BOOL success) {
        if(success) {
            self.moneyLbl.text = UDetail.user.sla_num;
        }
    }];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    [self.blueBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(@(0));
        make.height.equalTo(@(180));
    }];
    
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(16));
        make.centerX.equalTo(self.view);
    }];
    
    [self.moneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.logoImgView.mas_bottom).offset(10);
        make.centerX.equalTo(self.view);
    }];
    
    [self.whiteBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.blueBG.mas_bottom).offset(-60);
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.height.equalTo(@(280));
    }];
    
    [self.widrawAddressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(25));
        make.left.equalTo(@(12));
    }];
    
    [self.widrawAddressTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.widrawAddressLbl.mas_bottom).offset(10);
        make.left.equalTo(@(12));
        make.height.equalTo(@(60));
        make.right.equalTo(self.addressPastBnt.mas_left).offset(-10);
    }];
    
    [self.addressPastBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.widrawAddressTF.mas_centerY);
        make.height.equalTo(@(18));
        make.right.equalTo(@(-18));
    }];
    
    [self.widrawMoneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.widrawAddressTF.mas_bottom).offset(25);
        make.left.equalTo(@(12));
    }];
    
    [self.widrawMoneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.widrawMoneyLbl.mas_bottom).offset(10);
        make.left.equalTo(@(12));
        make.height.equalTo(@(60));
        make.right.equalTo(@(-15));
    }];
    
    [self.feeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.widrawMoneyTF.mas_bottom).offset(25);
        make.left.equalTo(@(12));
    }];
    
    [self.feeValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.widrawMoneyTF.mas_bottom).offset(25);
        make.right.equalTo(@(-12));
    }];
    
    [self.sureBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBG.mas_bottom).offset(80);
        make.centerX.equalTo(self.whiteBG);
        make.height.equalTo(@(44));
    }];
}

- (IBAction)addressPastBntClick:(id)sender {
    NSString *address = [[UIPasteboard generalPasteboard] string];
    if(address.length > 0) {
        self.widrawAddressTF.text = address;
    }
}

- (NSString *)url {
    return @"api/exchange/send/getSendRate";
}

- (BOOL)useHUD {
    return YES;
}

- (void)requestResponse:(BOOL)success object:(id)object error:(NSString *)error {
    if(success) {
        self.feeValueLbl.text = [NSString stringWithFormat:@"%@ RB",[object valueForKeyPath:@"data.sendLogChargeRate"]];
    }
}

- (IBAction)commitBntClick:(id)sender {
    @weakify(self)
    [[YQPayKeyWordVC alloc] showInViewController:self tiltle:@"请输入支付密码" subtitle:nil block:^(NSString * password) {
        @strongify(self)
        NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
        [param setValue:self.widrawAddressTF.text forKey:@"address"];
        [param setValue:self.widrawMoneyTF.text forKey:@"num"];
        [param setValue:password forKey:@"pay_password"];
        
        [self request:@"api/exchange/send/applySendLog" param:param useEncrypt:YES useMsg:YES useHud:YES completion:^(BOOL success, id object, NSString *error) {
            if(success) {
                self.widrawMoneyTF.text = @"";
            }
        }];
    }];
    
    
}
-(void)navBarRightBtnAction:(id)sender{
    [MXRouter openURL:@"lcwl://WithdrawRecordVC"];
}
@end
