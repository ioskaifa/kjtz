//
//  ShowPlainextVC.h
//  Lcwl
//
//  Created by mac on 2019/1/25.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShowPlainextVC : BaseViewController

@property (strong, nonatomic) id content;

@property (nonatomic, copy) NSString *time;

@end

NS_ASSUME_NONNULL_END
