//
//  AuthManageListVC.m
//  BOB
//
//  Created by mac on 2020/7/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AuthManageListVC.h"
#import "AuthManageListObj.h"
#import "AuthManageListCell.h"

@interface AuthManageListVC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)  UITableView    *tableView;
@property(nonatomic, strong)  UIView         *headerView;
@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UITextField *searchTF;
@property (nonatomic, strong) NSMutableArray *searchDataMArr;

@property (nonatomic, copy) NSString *search_text;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation AuthManageListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)fetchData {
    if ([self.last_id isEqualToString:@"0"]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [self request:@"api/user/info/getAllChatUserList"
            param:@{@"last_id":self.last_id?:@"",
                    @"key_word":self.search_text?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSArray *dataArr = object[@"data"][@"chatUserList"];
            dataArr = [AuthManageListObj modelListParseWithArray:dataArr];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            AuthManageListObj *last = [self.dataSourceMArr lastObject];
            if (last) {
                self.last_id = last.ID;
            }
            [self.tableView reloadData];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - Private Method
- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        self.last_id = @"0";
        if ([StringUtil isEmpty:textField.text]) {
            self.search_text = @"";
        } else {
            self.search_text = textField.text;
        }
        [self fetchData];
    }
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = AuthManageListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    [cell addBottomSingleLine:[UIColor colorWithHexString:@"#DCDCDC"]];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    
    if (![cell isKindOfClass:AuthManageListCell.class]) {
        return;
    }
    AuthManageListObj *obj = self.dataSourceMArr[indexPath.row];
    AuthManageListCell *listCell = (AuthManageListCell *)cell;
    [listCell configureView:obj];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return 0.01;
    }
    return [AuthManageListCell viewHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    AuthManageListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:AuthManageListObj.class]) {
        return;
    }
    MXRoute(@"AuthManageVC", @{@"user_id":obj.ID});
}

#pragma mark - InitUI
- (void)createUI {
    self.view.backgroundColor = [UIColor moBackground];
    [self initUI];
}

- (void)initUI {
    [self setNavBarTitle:@"授权管理"];
    self.last_id = @"0";
    [self.view addSubview:self.tableView];
    
    [self layoutUI];
}

- (void)layoutUI {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.headerView;
        Class cls = AuthManageListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.tableFooterView = [UIView new];
        @weakify(self)
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self)
            self.last_id = @"0";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self)
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UIView *)headerView {
    if (!_headerView) {
        _headerView = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor colorWithHexString:@"#EDEDED"];
            MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            layout.myTop = 0;
            layout.myLeft = 0;
            layout.myRight = 0;
            layout.myHeight = 54;
            [object addSubview:layout];
            [layout addSubview:self.searchTF];
            [layout layoutSubviews];
            object.size = layout.size;
            object;
        });
    }
    return _headerView;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = ({
            UITextField *object = [UITextField new];
            object.delegate = self;
            [object addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
            [object setViewCornerRadius:15];
            object.font = [UIFont font15];
            object.backgroundColor = [UIColor whiteColor];
            UIView *leftView = [UIView new];
            leftView.size = CGSizeMake(40, 30);
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"im_search_icon"] forState:UIControlStateNormal];
            [leftBtn sizeToFit];
            leftBtn.x = 10;
            leftBtn.y = 5;
            [leftView addSubview:leftBtn];
            object.leftView = leftView;
            object.returnKeyType = UIReturnKeySearch;
            object.leftViewMode = UITextFieldViewModeAlways;
            object.clearButtonMode = UITextFieldViewModeWhileEditing;
            NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索联系人"] colors:@[[UIColor moBlack]] fonts:@[[UIFont font14]]];
            object.attributedPlaceholder = att;
            object.myLeft = 5;
            object.myWidth = SCREEN_WIDTH - 10;
            object.myHeight = 35;
            object.myCenterY = 0;
            [object setViewCornerRadius:17];
            object;
        });
    }
    return _searchTF;
}

@end
