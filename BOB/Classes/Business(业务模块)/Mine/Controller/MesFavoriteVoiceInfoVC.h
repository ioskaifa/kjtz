//
//  MesFavoriteVoiceInfoVC.h
//  BOB
//
//  Created by mac on 2020/7/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "MesFavoriteListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MesFavoriteVoiceInfoVC : BaseViewController

@property (nonatomic, strong) MesFavoriteListObj *obj;

@end

NS_ASSUME_NONNULL_END
