//
//  PhoneSettingVC.m
//  BOB
//
//  Created by mac on 2019/12/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "PhoneSettingVC.h"
#import "InvitationCodeView.h"
#import "TextCaptchaView.h"
#import "NSObject+Mine.h"
@interface PhoneSettingVC ()

@property (nonatomic,strong) InvitationCodeView* phoneView;

@property (nonatomic,strong) TextCaptchaView     *codeView;

@property (nonatomic,strong) UIButton*      nextBtn;

@property (nonatomic,strong) NSString*      mobile;

@property (nonatomic,strong) NSString*      code;

@end

@implementation PhoneSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

-(void)setupUI{
    [self setNavBarTitle:@"更换手机号" color:[UIColor moBlack]];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.phoneView];
    [self.view addSubview:self.codeView];
    [self.view addSubview:self.nextBtn];
    [self.phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(50));
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.top.equalTo(@(0));
    }];
    [self.codeView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.height.equalTo(@(50));
       make.left.equalTo(@(15));
       make.right.equalTo(@(-15));
       make.top.equalTo(self.phoneView.mas_bottom);
   }];
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.phoneView);
        make.top.equalTo(self.codeView.mas_bottom).offset(120);
        make.height.equalTo(@(35));
        
    }];
}

-(InvitationCodeView* )phoneView{
    if (!_phoneView) {
        _phoneView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
        [_phoneView setTitleWidth:60];
        [_phoneView reloadTitle:@"新手机号" placeHolder:@"请输入新的手机号码"];
        @weakify(self)
        _phoneView.getText = ^(id data) {
            @strongify(self)
            self.mobile = data;
        };
    }
    return _phoneView;
}

-(TextCaptchaView* )codeView{
    if (!_codeView) {
        _codeView = [[TextCaptchaView alloc]initWithFrame:CGRectZero];
        [_codeView setTitleWidth:60];
        [_codeView reloadTitle:@"验证码" placeHolder:@"请输入验证码"];
        @weakify(self)
        _codeView.getText = ^(id data) {
            @strongify(self)
            self.code = data;
        };
        _codeView.sendCodeBlock  = ^(id data) {
           @strongify(self)
            [self setup_get_code:@{@"type":@"5",@"mobile":self.mobile} completion:^(BOOL success, NSString *error) {
                  if (success) {
                      [NotifyHelper showMessageWithMakeText:@"发送成功"];
                  }
              }];
        };
    }
    return _codeView;
}

-(UIButton*)nextBtn{
    if (!_nextBtn) {
        _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextBtn.backgroundColor = [UIColor moBlueColor];
        [_nextBtn setTitle:@"保存" forState:UIControlStateNormal];
        _nextBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _nextBtn.layer.cornerRadius = 4;
        @weakify(self)
        [_nextBtn addAction:^(UIButton *btn) {
            @strongify(self)
            if ([StringUtil isEmpty:self.mobile]) {
                [NotifyHelper showMessageWithMakeText:@"请输入新的手机号码"];
                return ;
            }
            if ([StringUtil isEmpty:self.code]) {
               [NotifyHelper showMessageWithMakeText:@"请输入短信验证码"];
               return ;
           }
            [self setup_edit_mobile:@{@"old_code":self.captcha,@"mobile":self.mobile,@"new_code":self.code} completion:^(BOOL success, NSString *error) {
                if (success) {
                    UDetail.user.user_tel = self.mobile;
                    [MXRouter openURL:@"lcwl://BindResultVC"];
                }
            }];
        }];
    }
    return _nextBtn;
}
@end
