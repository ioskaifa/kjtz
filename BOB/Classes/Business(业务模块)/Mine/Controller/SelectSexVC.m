//
//  LegalTenderTransactionVC.m
//  BOB
//
//  Created by mac on 2019/12/10.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "SelectSexVC.h"
#import "NSObject+Mine.h"
static NSString* identifier = @"cell";
@interface SelectSexVC ()<UITableViewDelegate,UITableViewDataSource>

@property (strong , nonatomic) UITableView *tableView;

@property (strong , nonatomic) NSArray *dataArray;

@property (strong , nonatomic) NSString *selectStr;


@end

@implementation SelectSexVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self initData];
}

-(void)setupUI{
    [self setNavBarTitle:@"设置性别" color:[UIColor moBlack]];
    [self setNavBarRightBtnWithTitle:@"完成" andImageName:nil];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    AdjustTableBehavior(_tableView);
}

-(void)initData{
    self.dataArray = @[@{@"type":@"男",@"id":@"1"},@{@"type":@"女",@"id":@"2"}];
    self.selectStr = UDetail.user.sex;
}

- (UITableView*)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.backgroundColor =  [UIColor whiteColor];
        _tableView.estimatedRowHeight = 44;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:identifier];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = [UIFont font15];
        UIImageView* image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_public_ok"]];
        image.frame = CGRectMake(0, 0, 15, 15);
        cell.accessoryView = image;
        cell.accessoryView.hidden = YES;
    }
    NSDictionary* dict = self.dataArray[indexPath.row];
    cell.textLabel.text = dict[@"type"];
    NSString* idStr = dict[@"id"];
    if ([self.selectStr isEqualToString:idStr]) {
        cell.accessoryView.hidden = NO;
    }else{
        cell.accessoryView.hidden = YES;
    }
    return cell;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* dict = self.dataArray[indexPath.row];
    self.selectStr = dict[@"id"];
    [self.tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

-(void)navBarRightBtnAction:(id)sender{
    [self setup_edit_userinfo:@{@"sex":self.selectStr,@"type":@"3"} completion:^(BOOL success, NSString *error) {
        if (success) {
            UDetail.user.sex = self.selectStr;
        }
        [NotifyHelper hideHUDForView:self.view animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
@end
