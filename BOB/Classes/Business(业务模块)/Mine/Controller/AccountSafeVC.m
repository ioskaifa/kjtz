//
//  SwitchAccountVC.m
//  BOB
//
//  Created by mac on 2019/12/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "AccountSafeVC.h"
#import "SPButton.h"
@interface AccountSafeVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView      *tableView;

@property (nonatomic, strong) NSMutableArray   *dataArray;

@end

@implementation AccountSafeVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refreshData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

-(void)setupUI{
    [self setNavBarTitle:@"切换账号" color:[UIColor moBlack]];
    [self.view addSubview:self.tableView];
    self.dataArray = @[];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    AdjustTableBehavior(_tableView);
}

-(void)refreshData{
    self.dataArray = @[
                          @[
                              @{@"title":@"币火号",@"desc":UDetail.user.account},
                              @{@"title":@"手机号",@"desc":UDetail.user.user_tel,@"block":^(){
                                  [MXRouter openURL:@"lcwl://BindPhoneVC"];
                              }}
                          ],
                          @[
                              @{@"title":@"修改/设置登录密码",@"desc":@"",@"block":^(){
                                  [MXRouter openURL:@"lcwl://LoginPwdSettingVC"];
                              }},
                              @{@"title":@"修改/设置交易密码",@"desc":@"",@"block":^(){
                                  [MXRouter openURL:@"lcwl://BusinessPwdSettingVC"];
                              }}
                          ]
                      ];
    [self.tableView reloadData];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray* array = self.dataArray[section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.01;
    }
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* identifier = [NSString stringWithFormat:@"%ld %ld",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont font15];
        cell.detailTextLabel.font = [UIFont font11];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        MXSeparatorLine* line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
        [cell addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(cell);
            make.height.equalTo(@(.5));
        }];
    }
    NSDictionary* data = self.dataArray[indexPath.section][indexPath.row];
    cell.textLabel.text = data[@"title"];
    cell.detailTextLabel.text = data[@"desc"];
    if (data[@"block"]) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray* rowData = self.dataArray[indexPath.section];
    NSDictionary* dict = rowData[indexPath.row];
    dispatch_block_t block = dict[@"block"];
    !block?:block();
}
@end
