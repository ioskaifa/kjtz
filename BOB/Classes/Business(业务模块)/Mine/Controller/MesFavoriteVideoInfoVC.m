//
//  MesFavoriteVideoInfoVC.m
//  BOB
//
//  Created by mac on 2020/8/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteVideoInfoVC.h"
#import "VideoPlayManager.h"
#import "ChatCacheFileUtil.h"
#import "TalkManager.h"

@interface MesFavoriteVideoInfoVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MyBaseLayout *headerView;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) MessageModel *message;

@end

@implementation MesFavoriteVideoInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[VideoPlayManager share] playStop];
}

- (void)palyVideo {
    if ([self.message messageFileExists]) {
        [[VideoPlayManager share] playLocal:self.message.fullPath
                                  superView:self.imgView];
    } else {
        [TalkManager downloadFile:self.message completion:^(BOOL success, NSString *error) {
            
        }];
        [[VideoPlayManager share] play:self.message.urlString
                             superView:self.imgView];
    }    
}

- (void)createUI {
    [self setNavBarTitle:@"详情"];
    [self.view addSubview:self.tableView];
    [self layoutUI];
    [self initUI];
}

- (void)layoutUI {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)initUI {
    UIImageView *imgView = [UIImageView new];
    self.imgView = imgView;
    imgView.myLeft = 15;
    imgView.myRight = 15;
    imgView.myTop = 10;
    NSString *url = StrF(@"%@/%@", UDetail.user.qiniu_domain, self.obj.attr1);
    [imgView sd_setImageWithURL:[NSURL URLWithString:VideoFirstScreenShot(url)] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
       if (image) {
           CGSize size = image.size;
           CGFloat height = ((SCREEN_WIDTH - 30) * size.height / size.width);
           size = CGSizeMake(SCREEN_WIDTH - 30, height);
           imgView.mySize = size;
           [self.headerView addSubview:imgView];
           [self.headerView layoutSubviews];
           self.tableView.tableHeaderView = self.headerView;
       }
    }];
    UIImageView *playImgView = [UIImageView new];
    playImgView.image = [UIImage imageNamed:@"icon_play"];
    [self.imgView addSubview:playImgView];
    [playImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(50, 50));
        make.center.mas_equalTo(self.imgView);
    }];
    @weakify(self)
    [self.imgView addAction:^(UIView *view) {
        @strongify(self)
        [self palyVideo];
    }];
}

- (MyBaseLayout *)headerView {
    if (!_headerView) {
        _headerView = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _headerView.backgroundColor = [UIColor whiteColor];
        _headerView.myTop = 0;
        _headerView.myLeft = 0;
        _headerView.myRight = 0;
        _headerView.myHeight = MyLayoutSize.wrap;
        
        UILabel *lbl = [UILabel new];
        lbl.font = [UIFont font12];
        lbl.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
        lbl.text = StrF(@"来自%@ %@", self.obj.send_name, self.obj.format_send_time);
        [lbl sizeToFit];
        lbl.mySize = lbl.size;
        lbl.myCenterX = 0;
        lbl.myTop = 10;
        [_headerView addSubview:lbl];

    }
    return _headerView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

- (MessageModel *)message {
    if (!_message) {
        MessageModel *message = [MessageModel new];
        message.subtype = kMxmessageTypeVideo;
        NSDictionary *bodyDic = @{@"attr1":self.obj.attr1?:@"",
                                  @"attr2":self.obj.attr2?:@"",
                                  @"attr3":self.obj.attr3?:@""};
        message.body = [MXJsonParser dictionaryToJsonString:bodyDic];
        _message = message;
    }
    return _message;
}

@end
