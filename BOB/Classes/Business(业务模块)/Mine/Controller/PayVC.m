//
//  PayVC.m
//  BOB
//
//  Created by mac on 2020/1/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "PayVC.h"
#import "ChatSettingHeader2.h"
#import "UIView+Utils.h"
#import "SPButton.h"
//#import "NSObject+Asset.h"
#import "UILabel+Extension.h"

@interface PayVC ()<UITableViewDelegate,UITableViewDataSource>
@property (strong , nonatomic)UITableView *tableView;
@property (strong , nonatomic)UIView *header;
@property (strong , nonatomic)SPButton *getPayBnt;
@property (strong , nonatomic)SPButton *walletBnt;
@property (strong , nonatomic)UILabel *moneyLbl;
@end

@implementation PayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavBarTitle:@"支付"];
    self.tableView.tableHeaderView = self.header;
    
//    [self wallet_money_sum:@{@"account_type":@"1"} completion:^(id object, NSString *error) {
//        UDetail.user.total_bb_money = [object valueForKeyPath:@"total_money"];
//        self.moneyLbl.text = StrF(@"￥%@",UDetail.user.total_bb_money);
//        
//    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - <UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell1"];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell1"];
            
            ChatSettingHeader2* view = [[ChatSettingHeader2 alloc] initWithFrame:CGRectMake(0, 60, SCREEN_WIDTH-15, 240) num:3 itemH:120];
            [view reloadData:@[  @{@"image":@"余额宝",@"text":@"余额宝"},
                                 @{@"image":@"币生币",@"text":@"币生币"},
                                 @{@"image":@"矿机",@"text":@"矿机"},
                                 @{@"image":@"交易所",@"text":@"交易所"},
                                 @{@"image":@"Games",@"text":@"Games"},
                                 @{@"image":@"二元期权",@"text":@"二元期权"},
                                 ]];
            view.tag = 101;
            [cell.contentView addSubview:view];
            cell.backgroundColor = [UIColor clearColor];
            
            UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 120, SCREEN_WIDTH-30, 1)];
            line1.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
            [view addSubview:line1];
            
            UIView *vline1 = [[UIView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH-30)/3), 0, 1, 240)];
            vline1.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
            [view addSubview:vline1];
            
            UIView *vline2 = [[UIView alloc] initWithFrame:CGRectMake(((SCREEN_WIDTH-30)/3)*2, 0, 1, 240)];
            vline2.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
            [view addSubview:vline2];
            
            view.block = ^(id data) {
                NSInteger index = [data integerValue];
                if(index == 0) {
                    //[MXRouter openURL:@"lcwl://MyTreasureVC"];
                } else if(index == 1) {
                    
                } else if(index == 2) {
                    
                } else if(index == 3) {
                    
                } else if(index == 4) {
                    
                } else if(index == 5) {
                    
                }
                [NotifyHelper showMessageWithMakeText:@"暂未开放"];
            };
            
            UILabel *titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-15, 60)];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"    币火服务" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0]}];
            
            titleLbl.attributedText = string;
            titleLbl.backgroundColor = [UIColor whiteColor];
            
            UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(7.5, 0, SCREEN_WIDTH-15, 300)];
            [cell.contentView addSubview:bgView];
            bgView.clipsToBounds = YES;
            bgView.layer.cornerRadius = 10;
            
            [bgView addSubview:titleLbl];
            [bgView addSubview:view];
            
            UIView *vline3 = [[UIView alloc] initWithFrame:CGRectMake(0, 60, SCREEN_WIDTH-15, 1)];
            vline3.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
            [bgView addSubview:vline3];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [MXRouter openURL:@"lcwl://ApplicationForAgencyVC" parameters:nil];
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView =  [[UITableView alloc] initWithFrame:CGRectZero];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
           
             make.edges.equalTo(self.view);
        
            
//          self.pageViewController.view.frame = CGRectMake(0, NavBarHeight+StatuBarHeight, ScreenW, ScreenH-(NavBarHeight+StatuBarHeight+SafeAreaBottomHeight));
        }];
    }
    return _tableView;
}

- (void)headerAction:(UIButton *)sender {
    if(self.getPayBnt == sender) {
        
    } else {
        [MXRouter openURL:@"lwcl://WalletListVC"];
    }
}

- (UIView *)header {
    if (!_header) {
        _header = [[UIView alloc] init];
        _header.frame = CGRectMake(0, 0, SCREEN_WIDTH, 180);
        
        UIView *v = [[UIView alloc] init];
        v.clipsToBounds = YES;
        v.layer.cornerRadius = 9;
        v.frame = CGRectMake(7.5, 20, SCREEN_WIDTH-15, 160);
        [v az_setGradientBackgroundWithColors:@[[UIColor colorWithHexString:@"#07BE04"],[UIColor colorWithHexString:@"#0B9A26"]] locations:nil startPoint:CGPointMake(0, 0) endPoint:CGPointMake(0, 1)];
        [_header addSubview:v];
        
        [v addSubview:self.getPayBnt];
        [v addSubview:self.walletBnt];
        
        CGFloat centerX = (SCREEN_WIDTH-15)/4;
        [self.getPayBnt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@(34));
            make.left.equalTo(@(centerX-25));
            make.width.equalTo(@(50));
            make.height.equalTo(@(65));
        }];
        
        [self.walletBnt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@(34));
            make.right.equalTo(@(-(centerX-25)));
            make.width.equalTo(@(50));
            make.height.equalTo(@(65));
        }];
        
        [v addSubview:self.moneyLbl];
        
        [self.moneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_walletBnt.mas_centerX);
            make.top.equalTo(self.walletBnt.mas_bottom).offset(7.5);
            make.height.equalTo(@(17));
        }];
    }
    return _header;
}

- (SPButton *)getPayBnt{
    if (!_getPayBnt) {
        _getPayBnt = [[SPButton alloc] initWithImagePosition:SPButtonImagePositionTop];
        _getPayBnt.frame = CGRectMake(0, 0, 50, 65);
        [_getPayBnt setTitle:Lang(@"收付款") forState:UIControlStateNormal];
        [_getPayBnt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _getPayBnt.titleLabel.font = [UIFont font15];
        [_getPayBnt setImage:[UIImage imageNamed:@"收付款"] forState:UIControlStateNormal];
        [_getPayBnt setImageTitleSpace:12];
        [_getPayBnt addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _getPayBnt;
}

- (SPButton *)walletBnt{
    if (!_walletBnt) {
        _walletBnt = [[SPButton alloc] initWithImagePosition:SPButtonImagePositionTop];
        _walletBnt.frame = CGRectMake(0, 0, 50, 65);
        [_walletBnt setTitle:Lang(@"钱包") forState:UIControlStateNormal];
        [_walletBnt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _walletBnt.titleLabel.font = [UIFont font15];
        [_walletBnt setImage:[UIImage imageNamed:@"钱包"] forState:UIControlStateNormal];
        [_walletBnt setImageTitleSpace:12];
        [_walletBnt addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _walletBnt;
}

- (UILabel *)moneyLbl {
    if (!_moneyLbl) {
        _moneyLbl = [[UILabel alloc] init];
        _moneyLbl.font = [UIFont systemFontOfSize:15];
        _moneyLbl.textColor = [UIColor colorWithHexString:@"#C2FFC2"];
        _moneyLbl.text = UDetail.user.total_bb_money ?: @"";
        [_moneyLbl addGestureRecognizer:self action:@selector(headerAction:)];
    }
    return _moneyLbl;
}
@end
