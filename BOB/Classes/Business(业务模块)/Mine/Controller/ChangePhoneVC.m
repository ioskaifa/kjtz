//
//  BindPhoneVC.m
//  BOB
//
//  Created by mac on 2019/12/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "ChangePhoneVC.h"
#import "TextCaptchaView.h"
#import "NSObject+Mine.h"
@interface ChangePhoneVC ()

@property (nonatomic,strong) UIImageView* phoneImg;

@property (nonatomic,strong) UILabel* phoneLbl;

@property (nonatomic,strong) UILabel* tipLbl;

@property (nonatomic,strong) TextCaptchaView     *codeView;

@property (nonatomic,strong) UIButton* nextBtn;

@property (nonatomic,strong) MXSeparatorLine* line;

@property (nonatomic,strong) NSString* captcha;

@end

@implementation ChangePhoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

-(void)setupUI{
    [self setNavBarTitle:@"更换手机号" color:[UIColor moBlack]];
    self.line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    self.line.backgroundColor = [UIColor colorWithHexString:@"#EFEFEF"];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.phoneImg];
    [self.view addSubview:self.phoneLbl];
    [self.view addSubview:self.tipLbl];
    [self.view addSubview:self.line];
    [self.view addSubview:self.codeView];
    [self.view addSubview:self.nextBtn];
    
    [self.phoneImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(169));
        make.height.equalTo(@(162));
        make.top.equalTo(@(44));
        make.centerX.equalTo(@(0));
    }];
    [self.phoneLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneImg.mas_bottom).offset(26);
        make.left.right.equalTo(self.view);
    }];
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneLbl.mas_bottom).offset(26);
         make.left.equalTo(@(15));
               make.right.equalTo(@(-15));
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(7.5));
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.codeView.mas_top);
    }];
    [self.codeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(55));
        make.top.equalTo(self.tipLbl.mas_bottom).offset(44);
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
    }];
    [self.nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeView.mas_bottom).offset(35);
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.height.equalTo(@(35));
    }];
}

-(UIImageView*)phoneImg{
    if (!_phoneImg) {
        _phoneImg = [UIImageView new];
        _phoneImg.image = [UIImage imageNamed:@"交易所_手机"];
    }
    return _phoneImg;
}

-(UILabel*)phoneLbl{
    if (!_phoneLbl) {
        _phoneLbl = [UILabel new];
        _phoneLbl.text = [NSString stringWithFormat:@"当前绑定的手机号:%@",UDetail.user.user_tel];
        _phoneLbl.textColor = [UIColor moBlack];
        _phoneLbl.font = [UIFont font15];
        _phoneLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _phoneLbl;
}

-(UILabel*)tipLbl{
    if (!_tipLbl) {
        _tipLbl = [UILabel new];
        _tipLbl.text = @"更换手机号后，下次登录可使用新手机号登录。";
        _tipLbl.numberOfLines = 2;
        _tipLbl.textColor = [UIColor grayColor];
        _tipLbl.font = [UIFont font15];
        _tipLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _tipLbl;
}

-(UIButton*)nextBtn{
    if (!_nextBtn) {
        _nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _nextBtn.backgroundColor = [UIColor moBlueColor];
        [_nextBtn setTitle:@"下一步" forState:UIControlStateNormal];
        _nextBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _nextBtn.layer.cornerRadius = 4;
        @weakify(self)
        [_nextBtn addAction:^(UIButton *btn) {
            @strongify(self)
            if ([StringUtil isEmpty:self.captcha]) {
                [NotifyHelper showMessageWithMakeText:@"请输入短信验证码"];
                return ;
            }
            [MXRouter openURL:@"lcwl://PhoneSettingVC" parameters:@{@"captcha":self.captcha}];
        }];
    }
    return _nextBtn;
}



-(TextCaptchaView* )codeView{
    if (!_codeView) {
        _codeView = [[TextCaptchaView alloc]initWithFrame:CGRectZero];
        [_codeView setTitleWidth:60];
        [_codeView reloadTitle:@"验证码" placeHolder:@"请输入验证码"];
        @weakify(self)
        _codeView.getText = ^(id data) {
            @strongify(self)
            self.captcha = data;
        };
        _codeView.sendCodeBlock  = ^(id data) {
            @strongify(self)
            [self setup_get_code:@{@"type":@"4"} completion:^(BOOL success, NSString *error) {
                if (success) {
                    [NotifyHelper showMessageWithMakeText:@"发送成功"];
                }
            }];
        };
    }
    return _codeView;
}

@end
