//
//  MoreVC.h
//  BOB
//
//  Created by mac on 2019/12/30.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MoreVC : BaseSTDTableViewVC
@property (nonatomic, strong) FriendModel *otherModel;
@end

NS_ASSUME_NONNULL_END
