//
//  MesFavoriteListVC.m
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteListVC.h"
#import "MesFavoriteApi.h"
#import "MesFavoriteListTextCell.h"
#import "MesFavoriteListImageCell.h"
#import "MesFavoriteListVoiceCell.h"
#import "MesFavoriteListAddressCell.h"
#import "MesFavoriteListFileCell.h"
#import "MesFavoriteListVideoCell.h"

#import "LocationNavigateVC.h"
#import "MXDeviceMediaManager.h"

@interface MesFavoriteListVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) MesFavoriteListTextCell *staticTxtCell;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation MesFavoriteListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setWhiteNavStyle];
}

#pragma mark - Private Method
- (void)fetchData {
    if ([self.last_id isEqualToString:@"0"]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [MesFavoriteApi getMesFavoriteList:self.last_id
                              complete:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSArray *dataArr = object[@"mesFavoriteList"];
            dataArr = [MesFavoriteListObj modelListParseWithArray:dataArr];
            for (MesFavoriteListObj *obj in dataArr) {
                if (![obj isKindOfClass:MesFavoriteListObj.class]) {
                    continue;
                }
                switch (obj.type) {
                    case kMXMessageTypeText:{
                        [self.staticTxtCell configureView:obj];
                        obj.viewHeight = [self.staticTxtCell viewHeight];
                        break;
                    }
                    case kMXMessageTypeImage:{
                        obj.viewHeight = [MesFavoriteListImageCell viewHeight];
                        break;
                    }
                    case kMXMessageTypeVoice:{
                        obj.viewHeight = [MesFavoriteListVoiceCell viewHeight];
                        break;
                    }
                    case kMXMessageTypeLocation:{
                        obj.viewHeight = [MesFavoriteListAddressCell viewHeight];
                        break;
                    }
                    case kMXMessageTypeFile:{
                        obj.viewHeight = [MesFavoriteListFileCell viewHeight];
                        break;
                    }
                    case kMxmessageTypeVideo:{
                        obj.viewHeight = [MesFavoriteListVideoCell viewHeight];
                        break;
                    }
                    default:
                        obj.viewHeight = [MesFavoriteListImageCell viewHeight];
                        break;
                }
            }
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            MesFavoriteListObj *last = [self.dataSourceMArr lastObject];
            if (last) {
                self.last_id = last.ID;
            } else {
                self.last_id = @"0";
            }
            [self.tableView reloadData];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - 查看地图详情
- (void)chatLocationCellBubblePressed:(MesFavoriteListObj *)obj {
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    LocationNavigateVC *vc = [LocationNavigateVC new];
    vc.city = Lang(@"位置分享");
    vc.address = obj.attr1;
    vc.toCoordinate2D = CLLocationCoordinate2DMake([obj.attr2 doubleValue], [obj.attr3 doubleValue]);
    vc.mapViewType = MXMapViewTypeAppleMap;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 语音播放
- (void)chatAudioCellBubblePressed:(MesFavoriteListObj *)obj {
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    MXRoute(@"MesFavoriteVoiceInfoVC", @{@"obj":obj});
}

#pragma mark - 查看文本
- (void)chatTextCellBubblePressed:(MesFavoriteListObj *)obj {
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    MXRoute(@"MesFavoriteTextInfoVC", @{@"obj":obj});
}

#pragma mark - 查看图片
- (void)chatImageCellBubblePressed:(MesFavoriteListObj *)obj {
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    MXRoute(@"MesFavoriteImageInfoVC", @{@"obj":obj});
}

#pragma mark - 查看文件
- (void)chatFileCellBubblePressed:(MesFavoriteListObj *)obj {
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    MessageModel *message = [MessageModel new];
    message.subtype = kMXMessageTypeFile;
    NSDictionary *bodyDic = @{@"attr1":obj.attr1?:@"",
                              @"attr2":obj.attr2?:@"",
                              @"attr3":obj.attr3?:@"",
                              @"attr4":obj.attr4?:@"",
                              @"attr5":obj.attr5?:@""};
    message.body = [MXJsonParser dictionaryToJsonString:bodyDic];    
    MXRoute(@"FilePreviewVC", (@{@"meaage":message}))
}

#pragma mark - 查看视频
- (void)chatVideoCellBubblePressed:(MesFavoriteListObj *)obj {
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    MXRoute(@"MesFavoriteVideoInfoVC", @{@"obj":obj});
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView; {
    return self.dataSourceMArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceMArr.count) {
        return [UITableViewCell new];
    }
    MesFavoriteListObj *obj = self.dataSourceMArr[indexPath.section];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return [UITableViewCell new];
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:obj.clsString forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceMArr.count) {
        return;
    }
    MesFavoriteListObj *obj = self.dataSourceMArr[indexPath.section];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    if (![cell isKindOfClass:MesFavoriteListCell.class]) {
        return;
    }
    MesFavoriteListCell *baseCell = (MesFavoriteListCell *)cell;
    [baseCell configureView:obj];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceMArr.count) {
        return 0.01;
    }
    MesFavoriteListObj *obj = self.dataSourceMArr[indexPath.section];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return 0.01;
    }
    return obj.viewHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceMArr.count) {
        return;
    }
    MesFavoriteListObj *obj = self.dataSourceMArr[indexPath.section];
    if (![obj isKindOfClass:MesFavoriteListObj.class]) {
        return;
    }
    switch (obj.type) {
        case kMXMessageTypeText:{
            [self chatTextCellBubblePressed:obj];
            break;
        }
        case kMXMessageTypeImage:{
            [self chatImageCellBubblePressed:obj];
            break;
        }
        case kMXMessageTypeVoice:{
            [self chatAudioCellBubblePressed:obj];
            break;
        }
        case kMXMessageTypeLocation:{
            [self chatLocationCellBubblePressed:obj];
            break;
        }
        case kMXMessageTypeFile:{
            [self chatFileCellBubblePressed:obj];
            break;
        }
        case kMxmessageTypeVideo:{
            [self chatVideoCellBubblePressed:obj];
            break;
        }
        default:
            
            break;
    }
}

#pragma mark - UITableView 左滑删除
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (indexPath.section >= self.dataSourceMArr.count) {
            return;
        }
        MesFavoriteListObj *obj = self.dataSourceMArr[indexPath.section];
        if (![obj isKindOfClass:MesFavoriteListObj.class]) {
            return;
        }
        [MesFavoriteApi delMesFavorite:obj.ID
                              complete:^(BOOL success, id object, NSString *error) {
            if (success) {
                [self.dataSourceMArr removeObject:obj];
                MesFavoriteListObj *last = [self.dataSourceMArr lastObject];
                if (last) {
                    self.last_id = last.ID;
                } else {
                    self.last_id = @"0";
                }
                [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

#pragma mark - InitUI
- (void)createUI {
    self.view.backgroundColor = [UIColor moBackground];
    [self initUI];
}

- (void)initUI {
    [self setNavBarTitle:@"我的收藏"];
    self.last_id = @"0";
    [self.view addSubview:self.tableView];
    
    [self layoutUI];
}

- (void)layoutUI {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        Class cls = MesFavoriteListTextCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        cls = MesFavoriteListImageCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        cls = MesFavoriteListVoiceCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        cls = MesFavoriteListAddressCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        cls = MesFavoriteListFileCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        cls = MesFavoriteListVideoCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.tableFooterView = [UIView new];
        @weakify(self)
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self)
            self.last_id = @"0";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self)
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (MesFavoriteListTextCell *)staticTxtCell {
    if (!_staticTxtCell) {
        _staticTxtCell = [[MesFavoriteListTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MesFavoriteListTextCell"];
    }
    return _staticTxtCell;
}

@end
