//
//  ModifyPhoneStep1VC.m
//  BOB
//
//  Created by mac on 2019/7/13.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BindBankVC.h"
#import "InvitationCodeView.h"
#import "ImgCaptchaView.h"
#import "TextCaptchaView.h"
#import "MineHelper.h"
#import "NSObject+LoginHelper.h"
#import "BindAccountModel.h"
static NSInteger padding = 15;
@interface BindBankVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) InvitationCodeView* nameView;

@property (nonatomic, strong) InvitationCodeView* openBankView;

@property (nonatomic, strong) InvitationCodeView* bankNameView;

@property (nonatomic, strong) InvitationCodeView* bankCardView;

@property (nonatomic, strong) InvitationCodeView* phoneView;

@property (nonatomic, strong) ImgCaptchaView    *imgVerifyView;

@property (nonatomic, strong) TextCaptchaView   *verifyView;

@property (nonatomic, strong) UIButton          *submitBtn;

@property (nonatomic, strong) NSArray          *dataArray;

@property (nonatomic, strong) UITableView      *tableView;

@property (nonatomic, strong) BindAccountModel      *model;

@end

@implementation BindBankVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self initData];
}

-(void)initUI{
    [self setNavBarTitle:@"银行卡绑定"];
    self.dataArray = @[@[
                           @{@"title":Lang(@"姓名"),@"placeholder":Lang(@"请输入您的真实姓名"),@"desc":self.account.account_name},
                           @{@"title":Lang(@"开户行"),@"placeholder":Lang(@"请输入开户行名称"),@"desc":self.account.bank_name},
                           @{@"title":Lang(@"开户支行"),@"placeholder":Lang(@"请输入开户行支行名称"),@"desc":self.account.bank_branch_name},
                           @{@"title":Lang(@"银行卡号"),@"placeholder":Lang(@"请输入您的银行卡号"),@"desc":self.account.account}
                           ],
                       @[
                           @{@"title":Lang(@"当前手机号"),@"desc":UDetail.user.user_tel},
                           @{@"title":Lang(@"图形验证码"),@"placeholder":Lang(@"请输入验证码")},
                           @{@"title":Lang(@"短信验证码"),@"placeholder":Lang(@"请输入验证码")}
                           ]
                       ];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];

    
}

-(void)initData{
    self.model = [[BindAccountModel alloc]init];
    [self getImgCode];
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitBtn.layer.masksToBounds = YES;
        _submitBtn.layer.cornerRadius = 22;
        [_submitBtn setTitle:Lang(@"立即绑定") forState:UIControlStateNormal];
        _submitBtn.clipsToBounds =NO;
        [_submitBtn addTarget:self action:@selector(goTo:) forControlEvents:UIControlEventTouchUpInside];
        [_submitBtn setBackgroundColor:[UIColor moBlueColor]];
    }
    return _submitBtn;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor clearColor];
        AdjustTableBehavior(_tableView);
        _tableView.separatorColor = [UIColor moBackground];
        UIView* foot = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
        [foot addSubview:self.submitBtn];
        [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(foot.mas_centerX);
            make.height.equalTo(@(44));
            make.width.equalTo(@(180));
            make.bottom.equalTo(foot);
        }];
        _tableView.tableFooterView = foot;
    }
    
    return _tableView;
}

-(void)goTo:(id)sender{
    [MineHelper updateUserAccount:@{@"type":@"1",@"account":self.model.account,@"sms_code":self.model.sms_code,@"account_id":self.account.account_id,@"account_name":self.model.account_name,@"bank_name":self.model.bank_name,@"bank_branch_name":self.model.bank_branch_name} completion:^(BOOL success, NSString *error) {
        if (success) {
            [NotifyHelper showMessageWithMakeText:@"绑定成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray* array = self.dataArray[section];
    return array.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor moBackground];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 1) {
        return 10;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* identifier = [NSString stringWithFormat:@"%ld %ld",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView* tmpView = [self getViewFromIndexPath:indexPath];
        [cell.contentView addSubview:tmpView];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor whiteColor];
        [tmpView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView).insets(UIEdgeInsetsMake(0, 15, 0, 15));
        }];
    }
    
    return cell;
}

-(UIView*)getViewFromIndexPath:(NSIndexPath *)indexPath{
    NSArray* array = self.dataArray[indexPath.section];
    NSDictionary* dict = array[indexPath.row];
    NSString* title = dict[@"title"];
    NSString* placehoder = dict[@"placeholder"];
    UIView* tmpView = nil;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            tmpView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
            self.nameView = (InvitationCodeView*)tmpView;
            [self.nameView reloadTitle:title placeHolder:placehoder];
            self.nameView.tf.text = dict[@"desc"];
            @weakify(self)
            self.nameView.getText = ^(id data) {
                @strongify(self)
                self.model.account_name = data;
            };
        }else  if (indexPath.row == 1) {
            tmpView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
            self.openBankView = (InvitationCodeView*)tmpView;
            [self.openBankView reloadTitle:title placeHolder:placehoder];
             self.openBankView.tf.text = dict[@"desc"];
            @weakify(self)
            self.openBankView.getText = ^(id data) {
                 @strongify(self)
                self.model.bank_name = data;
            };
        } if (indexPath.row == 2) {
            tmpView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
            self.bankNameView = (InvitationCodeView*)tmpView;
            [self.bankNameView reloadTitle:title placeHolder:placehoder];
            self.bankNameView.tf.text = dict[@"desc"];
            @weakify(self)
            self.bankNameView.getText = ^(id data) {
                @strongify(self)
                self.model.bank_branch_name = data;
            };
        } if (indexPath.row == 3) {
            tmpView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
            self.bankCardView = (InvitationCodeView*)tmpView;
            [self.bankCardView reloadTitle:title placeHolder:placehoder];
            [self.bankCardView isHiddenLine:YES];
            self.bankCardView.tf.text = dict[@"desc"];
            @weakify(self)
            self.bankCardView.getText = ^(id data) {
                @strongify(self)
                self.model.account = data;
            };
        }
    }else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            tmpView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
            self.phoneView = (InvitationCodeView*)tmpView;
            self.phoneView.tf.enabled = NO;
            [self.phoneView reloadTitle:title placeHolder:@""];
            self.phoneView.tf.text = dict[@"desc"];
        }else if(indexPath.row == 1){
            tmpView = [[ImgCaptchaView alloc]initWithFrame:CGRectZero];
            self.imgVerifyView = (ImgCaptchaView*)tmpView;
            [self.imgVerifyView reloadTitle:title placeHolder:placehoder];
            @weakify(self)
            self.imgVerifyView.getText = ^(id data) {
                @strongify(self)
                self.model.img_code = data;
            };
            self.imgVerifyView.picBlock = ^(id data) {
                @strongify(self)
                [self getImgCode];
                
            };
        }else if(indexPath.row == 2){
            tmpView = [[TextCaptchaView alloc]initWithFrame:CGRectZero];
            self.verifyView = (TextCaptchaView*)tmpView;
            [self.verifyView reloadTitle:title placeHolder:placehoder];
            [self.verifyView isHiddenLine:YES];
            @weakify(self)
            self.verifyView.getText = ^(id data) {
                @strongify(self)
                self.model.sms_code = data;
            };
            self.verifyView.sendCodeBlock = ^(id data) {
                @strongify(self)
                NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
                [param setValue:UDetail.user.user_tel forKey:@"user_account"];
                [param setValue:@"FrontUpdateUserAccount" forKey:@"bus_type"];
                [param setValue:self.model.img_id forKey:@"img_id"];
                [param setValue:self.model.img_code forKey:@"img_code"];
                
                [self sendSmsCodeToken:param completion:^(BOOL success, NSString *error) {
                    if (!success) {
                        [self getImgCode];
                    }else{
                        [self.verifyView startCountDowView];
                    }
                }];
            };
        }
    }
    return tmpView;
}

- (void)getImgCode {
    @weakify(self)
    [self createImgCode:@{@"interface_type":@"createImgCode"} completion:^(id object, NSString *error) {
        @strongify(self)
        if (object) {
            NSDictionary* tempDic = (NSDictionary*)object;
            NSString * img_id = [tempDic valueForKeyPath:@"img_id"];
            NSString * img_io = [tempDic valueForKeyPath:@"img_io"];
            self.model.img_id = img_id;
            self.model.img_io = img_io;
            [self.imgVerifyView reloadRightImg:self.model.ioImage];
        }
    }];
}
@end
