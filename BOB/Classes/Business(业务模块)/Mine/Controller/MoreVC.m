//
//  ModifyInfoVC.m
//  BOB
//
//  Created by mac on 2019/6/27.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MoreVC.h"
#import "BaseSTDTableViewCell.h"
#import "AccountModel.h"
#import "NSObject+Mine.h"
#import "ChooseLocationView.h"
@interface MoreVC ()

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic,strong) ChooseLocationView *chooseLocationView;

@end

@implementation MoreVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self updateViewForChanged];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNavBarTitle:Lang(@"更多信息")];
    self.dataArray = [[NSMutableArray alloc]init];
}

- (void)updateViewForChanged {
    [self setupTableViewDataSource];
}

#pragma mark - setup

- (void)configTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellClass:[BaseSTDTableViewCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
    
}

- (void)setupTableViewDataSource
{
    [self.tableView std_removeAllItemAtSection:0];
    
    BOOL isMySelf = self.otherModel == nil ? YES : [self.otherModel.userid isEqualToString:UDetail.user.chatUser_id];

    NSString* sex = UDetail.user.sex;
    NSString* area = UDetail.user.area;
//    NSString* sign_name = self.otherModel ? self.otherModel.sign_name : UDetail.user.sign_name;
    NSArray* itemList1 = nil;
    NSString *arrow = !isMySelf ? @"" : @"Arrow";
    if([StringUtil isEmpty:sex]) {
        sex = @"1";
    }
    sex = sex == nil ? @"" : [sex isEqualToString:@"1"]?@"男":@"女";
    itemList1 = @[
            [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:Lang(@"性别") detailText:sex rightIcon:arrow jumpVC:@""] cellHeight:50],
            [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:Lang(@"地区") detailText:area rightIcon:arrow jumpVC:@""] cellHeight:50],
//            [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:Lang(@"个人签名") detailText:sign_name rightIcon:arrow jumpVC:@""] cellHeight:50]
    ];




    [self.tableView std_addItems:itemList1 atSection:0];

    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    STDTableViewItem *item = [tableView std_itemAtIndexPath:indexPath];
    
    return item.cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if(self.otherModel) {
        return;
    }
    
    if (indexPath.row == 0) {
        [MXRouter openURL:@"lcwl://SelectSexVC"];
    }else if(indexPath.row == 1){
        if([self.chooseLocationView superview] != nil) {
            return;
        }

        NSString *address = UDetail.user.area;
        self.chooseLocationView = [ChooseLocationView show:address];
        @weakify(self)
        [self.chooseLocationView setChooseFinish:^(NSString *address) {
            @strongify(self)
            [self setup_edit_userinfo:@{@"type":@"5",@"area":address} completion:^(BOOL success, NSString *error) {
              if (success) {
                  UDetail.user.area = address;
                  [self updateViewForChanged];
              }
          }];
        }];
    }else if(indexPath.row == 2){
        [MXRouter openURL:@"lcwl://TextChangeVC" parameters:@{@"text":UDetail.user.sign_name,@"titleStr":@"个性签名",@"block":
                                                                             ^(NSString* data){
           [self setup_edit_userinfo:@{@"type":@"4",@"sign_name":data} completion:^(BOOL success, NSString *error) {
               if (success) {
                   UDetail.user.sign_name = data;
                   [self updateViewForChanged];
               }
           }];
       },@"placeholder":@"请输入昵称",@"text":UDetail.user.user_name}];
    }
}

@end
