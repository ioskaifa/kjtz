//
//  MyAddressListVC.h
//  Lcwl
//
//  Created by mac on 2018/12/22.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyAddressListVC : BaseViewController
@property(nonatomic, copy) FinishedBlock selectBlock;
@end

NS_ASSUME_NONNULL_END
