//
//  MyAddressModel.h
//  Lcwl
//
//  Created by mac on 2018/12/22.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyAddressModel : NSObject
@property (nonatomic, copy)   NSString *address;
@property (nonatomic, copy)   NSString *detailAddress;
@property (nonatomic, copy)   NSString *isdefault;
@property (nonatomic, copy)   NSString *address_id;
@property (nonatomic, copy)   NSString *create_time;
@property (nonatomic, assign) NSInteger user_id;
@property (nonatomic, copy)   NSString *tel;
@property (nonatomic, copy)   NSString *name;
@end

NS_ASSUME_NONNULL_END
