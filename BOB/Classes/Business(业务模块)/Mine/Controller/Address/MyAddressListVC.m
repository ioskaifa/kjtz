//
//  MyAddressListVC.m
//  Lcwl
//
//  Created by mac on 2018/12/22.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "MyAddressListVC.h"
#import "MyAddressModel.h"
#import "NSObject+YYModelExt.h"
#import "NSString+NumFormat.h"
#import "NSMutableAttributedString+Attributes.h"
#import "LYEmptyViewHeader.h"

@interface MyAddressListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (copy, nonatomic) NSString *last_id;
@property (strong, nonatomic) UIImageView *defautImgV;
@end

@implementation MyAddressListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F5F4"];
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"#F4F5F4"];
    
    UIImage *icon = [UIImage imageNamed:@"defaultAddress"];
    self.defautImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"defaultAddress"]];
    self.defautImgV.x = SCREEN_WIDTH-icon.size.width;
    self.defautImgV.y = 0;
    
    [self setNavBarTitle:Lang(@"我的收货地址")];
    [self setNavBarRightBtnWithTitle:Lang(@"添加新地址") andImageName:nil];
    
    self.dataSource = [NSMutableArray arrayWithCapacity:1];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view addSubview:self.tableView];
    AdjustTableBehavior(self.tableView);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    [self getAddressList];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAddressList) name:@"updateAddressList" object:nil];
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)navBarRightBtnAction:(id)sender {
    [MXRouter openURL:@"lcwl://AddNewAddressVC" parameters:nil];
}

- (void)getAddressList {
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"api/user/address/getUserAddressList"];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data) {
            [self endTableViewRefreshing];
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                NSArray *modelsArr = [NSArray modelsArrayWithClass:[MyAddressModel class] array:[data valueForKeyPath:@"data.userAddressList"]];
                [self.dataSource removeAllObjects];
                [self.dataSource addObjectsFromArray:modelsArr];
                [self.tableView reloadData];
            }
            
            [self showEmpy];
        }).failure(^(id error){
            [self endTableViewRefreshing];
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

- (void)showEmpy {
    if(self.dataSource.count <= 0) {
        LYEmptyView *emptyView = [LYEmptyView emptyActionViewWithImageStr:@"暂无收货地址"
                                                                 titleStr:Lang(@"暂时还没有收货地址哦")
                                                                detailStr:nil
                                                              btnTitleStr:Lang(@"立即添加")
                                                            btnClickBlock:^(){
                                                                [MXRouter openURL:@"lcwl://AddNewAddressVC" parameters:nil]
                                                                ;                                                            }];
        emptyView.subViewMargin = 36.f;
        
        emptyView.titleLabFont = [UIFont systemFontOfSize:13.f];
        emptyView.titleLabTextColor = [UIColor colorWithHexString:@"#B1B3B3"];
        
        emptyView.actionBtnFont = [UIFont systemFontOfSize:16.f];
        emptyView.actionBtnTitleColor = [UIColor whiteColor];
        emptyView.actionBtnHeight = 44.f;
        emptyView.actionBtnHorizontalMargin = 74.f;
        emptyView.actionBtnCornerRadius = 22.f;
        emptyView.actionBtnBackGroundColor = [UIColor colorWithHexString:@"#1DAAAC"];
        self.tableView.ly_emptyView = emptyView;
        [self.tableView ly_showEmptyView];
    } else {
        [self.tableView ly_hideEmptyView];
    }
}

///新增、编辑、删除用户收货地址接口
- (void)operateAddress:(NSString *)userAddressOper model:(MyAddressModel *)model {
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"api/user/address/updateUserAddress"];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    [param setValue:userAddressOper forKey:@"user_address_oper"];
    [param setValue:model.address_id forKey:@"address_id"];
    [param setValue:model.name forKey:@"name"];
    [param setValue:model.tel forKey:@"tel"];
    [param setValue:model.address forKey:@"address"];
    [param setValue:model.isdefault forKey:@"isdefault"];
    
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data) {
            [self endTableViewRefreshing];
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                NSString *address_id = [tempDic valueForKeyPath:@"data.address_id"];
                if([userAddressOper isEqualToString:@"user_address_add"]) {
                    model.address_id = address_id;
                } else if([userAddressOper isEqualToString:@"user_address_edit"]) {
                    
                } else if([userAddressOper isEqualToString:@"user_address_del"]) {
                    [self.dataSource removeObject:model];
                }
                [self.tableView reloadData];
                [self showEmpy];
            }
        }).failure(^(id error){
            [self endTableViewRefreshing];
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

//停止tableview刷新
- (void)endTableViewRefreshing
{
    if (self.tableView.mj_header.isRefreshing) {
        [self.tableView.mj_header endRefreshing];
    } else if (self.tableView.mj_footer.isRefreshing) {
        [self.tableView.mj_footer endRefreshing];
    }
    //self.isRefreshing = NO;
}

- (void)editAddress:(NSInteger)index {
    MyAddressModel *model = [self.dataSource safeObjectAtIndex:index];
    NSArray *arr = [model.address componentsSeparatedByString: @" "];
    model.address = [arr firstObject];
    if(arr.count >= 2) {
        model.detailAddress = [arr lastObject];
    }

    [MXRouter openURL:@"lcwl://AddNewAddressVC" parameters:@{@"model":model}];
}

#pragma mark - Table view data source

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

//// 定义编辑样式
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return UITableViewCellEditingStyleDelete;
//}
//
//// 进入编辑模式，按下出现的编辑按钮后,进行删除操作
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        [self operateAddress:@"user_address_del" model:[self.dataSource safeObjectAtIndex:indexPath.row]];
//    }
//}

//// 修改编辑按钮文字
//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return @"删除";
//}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    //添加一个删除按钮
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleDestructive) title:Lang(@"删除") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self operateAddress:@"user_address_del" model:[self.dataSource safeObjectAtIndex:indexPath.row]];
    }];
    //删除按钮颜色
    deleteAction.backgroundColor = [UIColor redColor];
    
    //添加一个编辑按钮
    UITableViewRowAction *eidtAction =[UITableViewRowAction rowActionWithStyle:(UITableViewRowActionStyleDestructive) title:Lang(@"编辑") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self editAddress:indexPath.section];
    }];
    //编辑按钮颜色
    eidtAction.backgroundColor = [UIColor themeColor];
    return @[deleteAction,eidtAction];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 130;
//}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *lineV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
//    lineV.backgroundColor = [UIColor colorWithHexString:@"#F4F5F4"];
    return lineV;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

#pragma mark - Table view delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"UITableViewCell"];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
        
        cell.detailTextLabel.textColor = [UIColor moTextGray];
        cell.detailTextLabel.numberOfLines = 0;
    }
    
    MyAddressModel *model = [self.dataSource safeObjectAtIndex:indexPath.section];
    cell.textLabel.text = [NSString stringWithFormat:@"%@   %@",model.name,[model.tel  starReplace]];
    
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:model.address];
    [attri addBaselineOffset:-2 substring:attri.string];
    cell.detailTextLabel.attributedText = attri;
    
    if([model.isdefault integerValue] == 1) {
        [cell.contentView addSubview:self.defautImgV];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //如果从设置中进来不要回调
    UIViewController *vc = [self.navigationController.viewControllers safeObjectAtIndex:self.navigationController.viewControllers.count-2];
    if([vc isMemberOfClass:NSClassFromString(@"MineVC")]) {
        [self editAddress:indexPath.section];
        return;
    }
    
    MyAddressModel *model = [self.dataSource safeObjectAtIndex:indexPath.section];
    Block_Exec(self.selectBlock,model);
    [self.navigationController popViewControllerAnimated:YES];
}


- (UITableView*)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.estimatedRowHeight = 80;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        //[_tableView registerNib:[UINib nibWithNibName:@"MyParticipantCell" bundle:nil] forCellReuseIdentifier:@"MyParticipantCell"];
        //[_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    }
    return _tableView;
}


@end
