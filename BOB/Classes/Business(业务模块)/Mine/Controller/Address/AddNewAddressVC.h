//
//  AddNewAddressVC.h
//  Lcwl
//
//  Created by mac on 2018/12/22.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "XLForm.h"
#import "XLFormViewController.h"
#import "MyAddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddNewAddressVC : XLFormViewController<XLFormRowDescriptorViewController>
@property(nonatomic, strong) MyAddressModel *model;
@property (nonatomic) XLFormRowDescriptor * rowDescriptor;

@end

NS_ASSUME_NONNULL_END
