//
//  AddNewAddressVC.m
//  Lcwl
//
//  Created by mac on 2018/12/22.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "AddNewAddressVC.h"
//#import "UIViewController+Customize.h"
#import "ChooseLocationView.h"
#import "UIViewController+Customize.h"

@interface AddNewAddressVC ()
@property(nonatomic, strong) UIView *footer;
@property(nonatomic, strong) XLFormRowDescriptor * nameLineRow;
@property(nonatomic, strong) XLFormRowDescriptor * phoneLineRow;
@property(nonatomic, strong) XLFormRowDescriptor * addressLineRow;
@property(nonatomic, strong) XLFormRowDescriptor * detailAddressLineRow;
@property(nonatomic, strong) UISwitch *switchBnt;
@property (nonatomic,strong) ChooseLocationView *chooseLocationView;
@end

@implementation AddNewAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:Lang(@"添加收货地址")];
    self.view.backgroundColor = BackGroundColor;
    self.tableView.backgroundColor = BackGroundColor;
    [self addDefaultNavLeftBtnItem];
//    [self addNavConfirmButtonWithDefaultAction:<#(NSString *)#>:@"保存"];
    
    XLFormDescriptor * form = [XLFormDescriptor formDescriptorWithTitle:nil];
    XLFormSectionDescriptor * section;
    
    // Section
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];
    
    // Name
    self.nameLineRow = [self addNewRow:Lang(@"收货人       ") placeHolder:Lang(@"请输入收货人名字") rowType:XLFormRowDescriptorTypeText];
    self.nameLineRow.value = self.model.name ?: @"";
    [section addFormRow:self.nameLineRow];
    
    self.phoneLineRow = [self addNewRow:Lang(@"手机号       ") placeHolder:Lang(@"请输入收货人手机号") rowType:XLFormRowDescriptorTypePhone];
    self.phoneLineRow.value = self.model.tel ?: @"";
    [section addFormRow:self.phoneLineRow];
    
    self.addressLineRow = [self addNewRow:Lang(@"所在地区   ") placeHolder:Lang(@"请选择收货地区") rowType:XLFormRowDescriptorTypeText];
    self.addressLineRow.value = self.model.address ?: @"";
    [self.addressLineRow.cellConfig setObject:@(NO) forKey:@"textField.enabled"];
    [section addFormRow:self.addressLineRow];
    
//    self.addressLineRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"所在地区   " rowType:XLFormRowDescriptorTypeButton title:@"所在地区   "];
//    [self.addressLineRow.cellConfig setObject:[UIColor moDarkGray] forKey:@"textLabel.color"];
//    [self.addressLineRow.cellConfig setObject:[UIFont systemFontOfSize:14] forKey:@"textField.font"];
//    self.addressLineRow.value = self.model.address ?: @"";
    
    self.detailAddressLineRow = [XLFormRowDescriptor formRowDescriptorWithTag:XLFormRowDescriptorTypeTextView rowType:XLFormRowDescriptorTypeTextView];
    [self.detailAddressLineRow.cellConfigAtConfigure setObject:Lang(@"详细地址：如道路、门牌号、小区、楼栋号、单元室等") forKey:@"textView.placeholder"];
    [self.detailAddressLineRow.cellConfig setObject:[UIFont systemFontOfSize:14] forKey:@"textView.font"];
    [self.detailAddressLineRow.cellConfigAtConfigure setObject:[UIFont systemFontOfSize:14] forKey:@"textView.font"];
    self.detailAddressLineRow.value = self.model.detailAddress ?: @"";
    [section addFormRow:self.detailAddressLineRow];
    
    self.form = form;
    
    self.tableView.tableFooterView = self.footer;
    
    [self initNaviMoreBtn];
}

- (void)navLeftBtnItemClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)initNaviMoreBtn
{
    UIButton *naviMoreButton=[UIButton buttonWithType:UIButtonTypeCustom];
    
    naviMoreButton.frame=CGRectMake(0, 0, 80, 40);
    [naviMoreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    naviMoreButton.titleLabel.font = [UIFont font14];
    [naviMoreButton setTitle:Lang(@"保存") forState:UIControlStateNormal];
    [naviMoreButton addTarget:self action:@selector(confirmBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    naviMoreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    UIBarButtonItem *nextItem=[[UIBarButtonItem alloc] initWithCustomView:naviMoreButton];
    self.navigationItem.rightBarButtonItem = nextItem;
}

- (void)confirmBtnClick:(id)sender {
    NSString *address_id = self.model.address_id;
    [self operateAddress:(address_id ? @"user_address_edit" : @"user_address_add") address:address_id];
}

///新增、编辑、删除用户收货地址接口
- (void)operateAddress:(NSString *)userAddressOper address:(NSString *)addressId {
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"api/user/address/updateUserAddress"];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    [param setValue:userAddressOper forKey:@"user_address_oper"];
    [param setValue:addressId forKey:@"address_id"];
    [param setValue:self.nameLineRow.value ?: @"" forKey:@"name"];
    [param setValue:self.phoneLineRow.value ?: @"" forKey:@"tel"];
    [param setValue:(self.switchBnt.isOn ? @"1" : @"0") forKey:@"isdefault"];
    [param setValue:[NSString stringWithFormat:@"%@ %@",self.addressLineRow.value ?: @"",self.detailAddressLineRow.value ?: @""] forKey:@"address"];
    
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data) {
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                [NotifyHelper showMessageWithMakeText:Lang(@"成功")];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateAddressList" object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

- (XLFormRowDescriptor *)addNewRow:(NSString *)name placeHolder:(NSString *)placeHolder rowType:(NSString *)rowType {
    XLFormRowDescriptor *row = [XLFormRowDescriptor formRowDescriptorWithTag:name rowType:rowType title:name];
    [row.cellConfigAtConfigure setObject:placeHolder forKey:@"textField.placeholder"];
    [row.cellConfig setObject:[UIColor moDarkGray] forKey:@"tintColor"];
    [row.cellConfig setObject:[UIColor moDarkGray] forKey:@"textLabel.color"];
    [row.cellConfig setObject:[UIFont systemFontOfSize:14] forKey:@"textField.font"];
    [row.cellConfigAtConfigure setObject:[UIFont systemFontOfSize:14] forKey:@"textField.font"];
    
    return row;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 3) {
        return 100;
    }
    return 50.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return  0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if(indexPath.row == 2) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else if(indexPath.row == 3) {
        //因为内部写死了字体，所以这里手动校正一下textview的字体
        for(UIView *view in cell.contentView.subviews) {
            if([view isKindOfClass:[UITextView class]]) {
                [(UITextView *)view setFont:[UIFont systemFontOfSize:14]];
                for(UILabel *placehoder in view.subviews) {
                    if([placehoder isKindOfClass:[UILabel class]]) {
                        [placehoder setFont:[UIFont systemFontOfSize:14]];
                    }
                }
            }
        }
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row == 2) {
        [self.view endEditing:YES];
        @weakify(self)
        if([self.chooseLocationView superview] != nil) {
            return;
        }
        
        self.chooseLocationView = [ChooseLocationView show:self.model.address];
        [self.chooseLocationView setChooseFinish:^(NSString *address) {
            @strongify(self)
            self.model.address = address;
            self.addressLineRow.value = address ?: @"";
            [self.tableView reloadData];
        }];
    }
}

- (UIView *)footer {
    if(!_footer) {
        _footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
        _footer.backgroundColor = BackGroundColor;
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 50)];
        bgView.backgroundColor = [UIColor whiteColor];
        
        UILabel *defaultLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 100, 50)];
        defaultLbl.text = Lang(@"设为默认地址");
        defaultLbl.font = [UIFont systemFontOfSize:14];
        defaultLbl.textAlignment = NSTextAlignmentLeft;
        [bgView addSubview:defaultLbl];
        
        self.switchBnt = [[UISwitch alloc] init];
        self.switchBnt.x = SCREEN_WIDTH-70;
        self.switchBnt.y = 9;
        [self.switchBnt setOn:[self.model.isdefault intValue] == 1 animated:NO];
        [bgView addSubview:self.switchBnt];
        [_footer addSubview:bgView];
    }
    return _footer;
}
@end
