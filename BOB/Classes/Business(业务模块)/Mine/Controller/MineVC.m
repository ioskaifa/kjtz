//
//  MineVC.m
//  BOB
//
//  Created by mac on 2019/6/24.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MineVC.h"
#import "BaseSTDTableViewCell.h"
#import "MineHeaderCell.h"
#import "ModifyInfoVC.h"
#import "MXAlertViewHelper.h"

@interface MineVC ()
{
    
}
@end

@implementation MineVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self updateForLanguageChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.tableView reloadData];
    [UDetail get_userinfo:^(BOOL success, NSString *error) {
        if (success) {
            [self.tableView reloadData];
        }
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)updateForLanguageChanged {
    //[self setNavBarTitle:Lang(@"个人中心")];
}

#pragma mark - setup
- (void)configTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellNibClass:[MineHeaderCell class]];
    [tableView std_registerCellClass:[BaseSTDTableViewCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[MineHeaderCell class]];
    [tableView std_addSection:sectionData];
    
    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
    
    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
    
    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource {
    [self.tableView std_addItems:@[[MineHeaderCell cellItemWithData:nil cellHeight:(150+NavigationBar_Height)]] atSection:0];
    
    NSArray *itemList = @[[BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"资产" text:@"资产" rightIcon:@"Arrow" jumpVC:@""] cellHeight:60]];
    [self.tableView std_addItems:itemList atSection:1];
    
    itemList = @[[BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"收藏" text:@"我的收藏" rightIcon:@"Arrow" jumpVC:@""] cellHeight:60],
    [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"我的名片" text:Lang(@"我的名片") rightIcon:@"Arrow" jumpVC:@""] cellHeight:60],];
    [self.tableView std_addItems:itemList atSection:2];
    
    itemList = @[[BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"我的设置" text:@"设置" rightIcon:@"Arrow" jumpVC:@""] cellHeight:60]];
    [self.tableView std_addItems:itemList atSection:3];
    
//    if (UDetail.user.identity) {
//        itemList = @[
//        [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:@"授权管理" text:Lang(@"授权管理") rightIcon:@"Arrow" jumpVC:@""] cellHeight:60]];
//        [self.tableView std_addItems:itemList atSection:3];
//    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        MXRoute(@"SettingVC", nil);
        return;
    }
    
    if (indexPath.section == 1) {
        MXRoute(@"MyAssetsVC", nil)
        return;
    }
    
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            MXRoute(@"MesFavoriteListVC", nil)
            return;
        }
        if (indexPath.row == 1) {
            MXRoute(@"MyQRCodeVC", @{@"title":@"我的名片"})
            return;
        }
    }
    if (indexPath.section == 3) {
        MXRoute(@"SettingVC", nil);
        return;
    }
//    if (indexPath.section == 3) {
//        MXRoute(@"AuthManageListVC", nil);
//        return;
//    }
    
    [NotifyHelper showMessageWithMakeText:Lang(@"暂未开放")];
    
}

@end
