//
//  WithdrawDetailVC.h
//  BOB
//
//  Created by mac on 2019/7/12.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "WithdrawalRecordModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface WithdrawDetailVC : BaseViewController

@property (nonatomic, strong) WithdrawalRecordModel* model;

@end

NS_ASSUME_NONNULL_END
