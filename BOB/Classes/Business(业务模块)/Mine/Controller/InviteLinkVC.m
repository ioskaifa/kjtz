//
//  MyQRCodeVC.m
//  Lcwl
//
//  Created by AlphaGO on 2018/11/16.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "InviteLinkVC.h"
#import "UIImage+Color.h"
#import "MBProgressHUD.h"
#import "UIView+Utils.h"
#import "UIImage+Utils.h"
#import "UINavigationBar+Alpha.h"
#import "UIActionSheet+MKBlockAdditions.h"

@interface InviteLinkVC ()

@property (strong, nonatomic)  UIImageView *topBgImageView;

@property (strong, nonatomic)  UIImageView *bgImageView;

@property (strong, nonatomic)  UIImageView *qrCode;

@property (strong, nonatomic)  UIButton *backBtn;

@property (strong, nonatomic)  UILabel *topLbl;

@property (strong, nonatomic)  UILabel *bottomLbl;

@property (strong, nonatomic)  UILabel *linkLbl;

@property (strong, nonatomic)  UIButton *bottomBtn;

@end

@implementation InviteLinkVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //[self.navigationController.navigationBar barReset];
}

- (void)navBarRightBtnAction:(id)sender {
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupUI];
    [self layout];
}

-(void)setupUI{
    [self.view addSubview:self.topBgImageView];
    [self.view addSubview:self.bgImageView];
    [self.bgImageView addSubview:self.topLbl];
    [self.bgImageView addSubview:self.qrCode];
    [self.bgImageView addSubview:self.bottomLbl];
    [self.bgImageView addSubview:self.linkLbl];
    [self.bgImageView addSubview:self.bottomBtn];
    
    
    self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backBtn setTitle:@" 邀请链接" forState:UIControlStateNormal];
    self.backBtn.titleLabel.font = [UIFont font14];
    [self.backBtn setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
    @weakify(self)
    [self.backBtn addAction:^(UIButton *btn) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self.view addSubview:self.backBtn];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *qrCode = [UIImage encodeQRImageWithContent:[NSString stringWithFormat:@"%@",UDetail.user.invite_url] size:CGSizeMake(SCREEN_HEIGHT, SCREEN_HEIGHT)];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.qrCode.image = qrCode;
        });
    });
}

- (void)layout {
    [self.topBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.equalTo(@(240));
    }];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(88+safeAreaInset().top));
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.height.equalTo(@(500));
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(safeAreaInset().top));
        make.left.equalTo(@(15));
        make.width.equalTo(@(80));
        make.height.equalTo(@(44));
    }];
    [self.topLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(44));
        make.left.equalTo(@(0));
        make.right.equalTo(@(0));
    }];
    [self.qrCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(190));
        make.top.equalTo(self.topLbl.mas_bottom).offset(44);
        make.centerX.equalTo(@(0));
       }];
    
    [self.bottomLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.qrCode.mas_bottom).offset(20);
        make.right.left.equalTo(@(0));
    }];
    
    [self.linkLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomLbl.mas_bottom).offset(5);
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
    }];
    [self.bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.linkLbl.mas_bottom).offset(20);
        make.width.equalTo(@(200));
        make.height.equalTo(@(35));
        make.centerX.equalTo(@(0));
    }];
}

-(UIImageView *)topBgImageView{
    if (!_topBgImageView) {
        _topBgImageView = [UIImageView new];
        _topBgImageView.image = [UIImage imageNamed:@"邀请链接_顶部"];
    }
    return _topBgImageView;
}

-(UIImageView *)bgImageView{
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
        _bgImageView.image = [UIImage imageNamed:@"邀请链接_背景"];
        _bgImageView.userInteractionEnabled = YES;
    }
    return _bgImageView;
}

-(UIImageView *)qrCode{
    if (!_qrCode) {
        _qrCode = [UIImageView new];
        _qrCode.backgroundColor = [UIColor lightGrayColor];
    }
    return _qrCode;
}

-(UILabel*)bottomLbl{
    if (!_bottomLbl) {
        _bottomLbl = [UILabel new];
        _bottomLbl.text = @"邀请链接";
        _bottomLbl.textColor = [UIColor moBlueColor];
        _bottomLbl.textAlignment = NSTextAlignmentCenter;
        _bottomLbl.font = [UIFont font14];
    }
    return _bottomLbl;
}

-(UILabel*)linkLbl{
    if (!_linkLbl) {
        _linkLbl = [UILabel new];
        _linkLbl.text = UDetail.user.invite_url;
        _linkLbl.textColor = [UIColor grayColor];
        _linkLbl.textAlignment = NSTextAlignmentCenter;
        _linkLbl.font = [UIFont font12];
        _linkLbl.numberOfLines= 0;
    }
    return _linkLbl;
}


-(UILabel*)topLbl{
    if (!_topLbl) {
        _topLbl = [UILabel new];
        _topLbl.font = [UIFont boldSystemFontOfSize:36];
        _topLbl.textColor = [UIColor moBlueColor];
        _topLbl.numberOfLines = 0;
        NSString* str1 = @"邀好友扫一扫";
        NSString* str2 = @"共创新型财富";
        NSString* str = [NSString stringWithFormat:@"%@\n%@",str1,str2];
        NSMutableAttributedString* attr = [[NSMutableAttributedString alloc]initWithString:str];
        [attr addFont:[UIFont boldSystemFontOfSize:32] substring:str1];
        [attr addFont:[UIFont boldFont14] substring:str2];
        [attr addColor:[UIColor moBlueColor] substring:str1];
        [attr addColor:[UIColor moBlueColor] substring:str2];
        [attr setLineSpacing:8 substring:attr.string alignment:NSTextAlignmentCenter];
        _topLbl.attributedText = attr;
    }
    return _topLbl;
}

-(UIButton*)bottomBtn{
    if (!_bottomBtn) {
        _bottomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bottomBtn setTitle:@"复制链接" forState:UIControlStateNormal];
        _bottomBtn.titleLabel.font = [UIFont font15];
        [_bottomBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_bottomBtn setBackgroundColor:[UIColor moBlueColor]];
        _bottomBtn.layer.cornerRadius = 18;
        [_bottomBtn addAction:^(UIButton *btn) {
            [NotifyHelper showMessageWithMakeText:@"复制成功"];
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = UDetail.user.invite_url;
        }];
    }
    return _bottomBtn;
}
@end
