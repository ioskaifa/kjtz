//
//  RealNameResultVC.m
//  BOB
//
//  Created by mac on 2019/12/24.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "RealNameResultVC.h"
#import "SPButton.h"
@interface RealNameResultVC ()

@property (nonatomic,strong) SPButton* stateBtn;

@property (nonatomic,strong) UILabel* tipLbl;

@property (nonatomic,strong) UIButton* submitBtn;

@end

@implementation RealNameResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

-(void)setupUI{
    [self setNavBarTitle:@"身份认证" color:[UIColor moBlack]];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.stateBtn];
    [self.stateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(80));
        make.top.equalTo(@(60));
        make.centerX.equalTo(@(0));
    }];
    [self.view addSubview:self.tipLbl];
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.stateBtn.mas_bottom).offset(15);
        make.left.right.equalTo(self.view);
    }];
    [self.view addSubview:self.submitBtn];
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.height.equalTo(@(35));
        make.top.equalTo(self.tipLbl.mas_bottom).offset(88);
    }];
    
}
//交易所_认证中@3x
//交易所_成功@3x
//交易所_失败@3x
- (SPButton *)stateBtn{
    if (!_stateBtn) {
        _stateBtn = [[SPButton alloc] initWithImagePosition:SPButtonImagePositionTop];
        [_stateBtn setTitle:Lang(@"认证失败") forState:UIControlStateNormal];
        [_stateBtn setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
        _stateBtn.titleLabel.font = [UIFont font14];
        [_stateBtn setImage:[UIImage imageNamed:@"交易所_失败"] forState:UIControlStateNormal];
        [_stateBtn setImageTitleSpace:12];
   }
    return _stateBtn;
}

-(UILabel*)tipLbl{
    if (!_tipLbl) {
        _tipLbl = [UILabel new];
        _tipLbl.font = [UIFont font15];
        _tipLbl.text = @"请核实你提交的信息是否准确";
        _tipLbl.textColor = [UIColor grayColor];
        _tipLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _tipLbl;
}

-(UIButton*)submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.backgroundColor = [UIColor moBlueColor];
        [_submitBtn setTitle:@"重新认证" forState:UIControlStateNormal];
        _submitBtn.titleLabel.font = [UIFont font15];
        _submitBtn.layer.cornerRadius = 4;
        [_submitBtn addAction:^(UIButton *btn) {
            [MXRouter openURL:@"lcwl://RealNameAuthVC"];
        }];
    }
    return _submitBtn;
}

@end
