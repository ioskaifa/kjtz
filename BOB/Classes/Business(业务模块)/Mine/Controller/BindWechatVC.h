//
//  BindWechatVC.h
//  BOB
//
//  Created by mac on 2019/7/15.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "AccountModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface BindWechatVC : BaseViewController

@property (nonatomic, strong) AccountModel* account;

@end

NS_ASSUME_NONNULL_END
