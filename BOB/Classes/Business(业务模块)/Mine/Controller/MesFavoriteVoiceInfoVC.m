//
//  MesFavoriteVoiceInfoVC.m
//  BOB
//
//  Created by mac on 2020/7/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteVoiceInfoVC.h"
#import "MXDeviceMediaManager.h"
#import "ChatCacheFileUtil.h"
#import "TalkManager.h"
#import <AVFoundation/AVFoundation.h>

#define ANIMATION_IMAGEVIEW_IMAGE_DEFAULT @"VoiceNodePlaying" // 小喇叭默认图片
#define ANIMATION_IMAGEVIEW_IMAGE_01 @"VoiceNodePlaying-1" // 小喇叭动画第一帧
#define ANIMATION_IMAGEVIEW_IMAGE_02 @"VoiceNodePlaying-2" // 小喇叭动画第二帧
#define ANIMATION_IMAGEVIEW_IMAGE_03 @"VoiceNodePlaying-3" // 小喇叭动画第二帧

@interface MesFavoriteVoiceInfoVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIImageView *animationImageView;

@property (nonatomic, strong) NSArray *animationImagesArr;

@property (nonatomic, strong) MessageModel *message;

@end

@implementation MesFavoriteVoiceInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sensorStateChange:)
                                                 name:@"UIDeviceProximityStateDidChangeNotification"
                                               object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[MXDeviceMediaManager sharedInstance].deviceMedia stopPlaying];
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
}

- (void)sensorStateChange:(NSNotificationCenter *)notification{
    AVAudioSession* audioSession = [AVAudioSession sharedInstance];
    if ([[UIDevice currentDevice] proximityState] == YES) {
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    } else {
        [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    }
}

- (void)playAudio {
    [[MXDeviceMediaManager sharedInstance].deviceMedia stopPlaying];
    if ([self.message messageFileExists]) {
        ///播放
        [self.animationImageView startAnimating];
        [[MXDeviceMediaManager sharedInstance].deviceMedia
         playAudioWithPath:self.message.fullPath
         completion:^(NSError *error) {
            [self.animationImageView stopAnimating];
        }];
        //开启监听 靠近耳朵使用听筒播放
        [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
    } else {
        ///下载
        [TalkManager downloadAudioFileByMessage:self.message
                                     completion:^(BOOL success, NSString *error) {
            if (success) {
                Block_Exec_Main_Async_Safe(^{
                    [self.animationImageView startAnimating];
                    [[MXDeviceMediaManager sharedInstance].deviceMedia
                     playAudioWithPath:error
                     completion:^(NSError *error) {
                        [self.animationImageView stopAnimating];
                    }];
                    //开启监听 靠近耳朵使用听筒播放
                    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
                });
            }
        }];
    }
}

- (void)createUI {
    [self setNavBarTitle:@"详情"];
    [self.view addSubview:self.tableView];
    [self layoutUI];
}

- (void)layoutUI {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (MyBaseLayout *)tableHeaderView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font12];
    lbl.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
    lbl.text = StrF(@"来自%@ %@", self.obj.send_name, self.obj.format_send_time);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myTop = 10;
    [layout addSubview:lbl];
    
    MyLinearLayout *contentLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    @weakify(self)
    [contentLayout addAction:^(UIView *view) {
        @strongify(self)
        [self playAudio];
    }];
    contentLayout.backgroundColor = [UIColor moBackground];
    [contentLayout setViewCornerRadius:5];
    contentLayout.myTop = 10;
    contentLayout.myLeft = 15;
    contentLayout.myRight = 15;
    contentLayout.myHeight = 60;
    [contentLayout addSubview:self.animationImageView];
    
    NSInteger duration = [self.obj.attr2 integerValue];
    NSString *minu = [NSString stringWithFormat:@"%02ld",(duration%3600)/60];
    NSString *sec = [NSString stringWithFormat:@"%02ld",duration%60];
    lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor blackColor];
    lbl.text = StrF(@"%@:%@", minu, sec);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    [contentLayout addSubview:lbl];
    [layout addSubview:contentLayout];
    
    [layout layoutSubviews];
    return layout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
        _tableView.tableHeaderView = [self tableHeaderView];
    }
    return _tableView;
}

- (NSArray *)animationImagesArr {
    if (!_animationImagesArr) {
        _animationImagesArr = @[[UIImage imageNamed:ANIMATION_IMAGEVIEW_IMAGE_01],
                                [UIImage imageNamed:ANIMATION_IMAGEVIEW_IMAGE_02],
                                [UIImage imageNamed:ANIMATION_IMAGEVIEW_IMAGE_03]];
    }
    return _animationImagesArr;
}

- (UIImageView *)animationImageView {
    if (!_animationImageView) {
        _animationImageView = [UIImageView new];
        _animationImageView.animationImages = self.animationImagesArr;
        _animationImageView.animationDuration = 1;
        _animationImageView.myCenterY = 0;
        _animationImageView.myLeft = 15;
        _animationImageView.mySize = CGSizeMake(30, 30);
        _animationImageView.myRight = 10;
        _animationImageView.image = [UIImage imageNamed:ANIMATION_IMAGEVIEW_IMAGE_01];
    }
    return _animationImageView;
}

- (MessageModel *)message {
    if (!_message) {
        MessageModel *message = [MessageModel new];
        message.subtype = kMXMessageTypeVoice;
        NSDictionary *bodyDic = @{@"attr1":self.obj.attr1?:@"",
                                  @"attr2":self.obj.attr2?:@"",
                                  @"attr3":self.obj.attr3?:@""};
        message.body = [MXJsonParser dictionaryToJsonString:bodyDic];
        _message = message;
    }
    return _message;
}

@end
