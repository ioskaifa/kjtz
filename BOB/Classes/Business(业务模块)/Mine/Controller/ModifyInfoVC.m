//
//  ModifyInfoVC.m
//  BOB
//
//  Created by mac on 2019/6/27.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "ModifyInfoVC.h"
#import "BaseSTDTableViewCell.h"
#import "AccountModel.h"
#import "NSObject+Mine.h"
@interface ModifyInfoVC ()

@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation ModifyInfoVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.receiveUpdateNoti = YES;
    [self setNavBarTitle:Lang(@"个人信息")];
    self.dataArray = [[NSMutableArray alloc]init];
}

- (void)updateViewForChanged {
    [self setupTableViewDataSource];
}

#pragma mark - setup

- (void)configTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellClass:[BaseSTDTableViewCell class]];
    [tableView std_registerCellClass:[BaseSTDTableViewCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
    
}

- (void)setupTableViewDataSource
{
    [self.tableView std_removeAllItemAtSection:0];
//    [self.tableView std_removeAllItemAtSection:1];
    
    NSArray *itemList1 = @[[BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:Lang(@"头像") rightImgUrl:UDetail.user.head_photo rightIcon:@"Arrow" jumpVC:@""] cellHeight:75],
                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:Lang(@"昵称") detailText:UDetail.user.nickname rightIcon:@"Arrow" jumpVC:@""] cellHeight:60],
                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:Lang(@"ID号") detailText:UDetail.user.format_user_id rightIcon:nil jumpVC:@""] cellHeight:60],
                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:Lang(@"我的二维码") detailText:nil rightIcon:@"Arrow" jumpVC:@""] cellHeight:60],
                          [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:Lang(@"更多") detailText:nil rightIcon:@"Arrow" jumpVC:@""] cellHeight:60]];
    [self.tableView std_addItems:itemList1 atSection:0];

    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    STDTableViewItem *item = [tableView std_itemAtIndexPath:indexPath];
    
    return item.cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [MXRouter openURL:@"lcwl://ChangeHeaderVC" parameters:@{@"imageUrl":UDetail.user.head_photo,@"block":
                    ^(NSString* data){
                        if(![StringUtil isEmpty:data]) {
                            [NotifyHelper showHUDAddedTo:self.view animated:YES];
                            [self setup_edit_userinfo:@{@"pic":data,@"head_photo":data,@"type":@"1"} completion:^(BOOL success, NSString *error) {
                                [NotifyHelper hideHUDForView:self.view animated:YES];
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"SocialContactReloadHeader" object:nil];
                            }];
                        }
                    }}];
        }else if(indexPath.row == 1){
            [MXRouter openURL:@"lcwl://TextChangeVC" parameters:@{@"titleStr":@"修改昵称",@"text":UDetail.user.smartName,@"block":
                                                                      ^(NSString* data){
                NSLog(@"%@.....",data);
                if(![StringUtil isEmpty:data]) {
                    [self setup_edit_userinfo:@{@"type":@"2",@"nickname":data,@"nick_name":data} completion:^(BOOL success, NSString *error) {
                        if (success) {
                            UDetail.user.nickname = data;
                            [self updateViewForChanged];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"SocialContactReloadHeader" object:nil];
                        }
                    }];
                }
            },@"placeholder":@"请输入昵称",@"text":UDetail.user.nickname}];
        }else if(indexPath.row == 3){
            MXRoute(@"MyQRCodeVC", @{@"title":@"我的二维码"})
        }else if(indexPath.row == 4){
            MXRoute(@"MoreVC", nil);
        }
    }else if(indexPath.section == 2){
        __block AccountModel* weixinAccount = nil;
        __block AccountModel* alipayAccount = nil;
        __block AccountModel* bankAccount = nil;
        [self.dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            AccountModel* model = obj;
            if ([model isKindOfClass:[AccountModel class]]) {
                if ([model.type isEqualToString:@"1"]) {
                    bankAccount = model;
                }else  if ([model.type isEqualToString:@"2"]) {
                    alipayAccount = model;
                }else  if ([model.type isEqualToString:@"3"]) {
                    weixinAccount = model;
                }
            }
        }];
        
        if (indexPath.row == 0) {
            [MXRouter openURL:@"lcwl://BindWechatVC" parameters:@{@"account":weixinAccount}];
        }else if(indexPath.row == 1){
            [MXRouter openURL:@"lcwl://BindAlipayVC" parameters:@{@"account":alipayAccount}];
        }else if(indexPath.row == 2){
            [MXRouter openURL:@"lcwl://BindBankVC" parameters:@{@"account":bankAccount}];
        }
    }
}

@end
