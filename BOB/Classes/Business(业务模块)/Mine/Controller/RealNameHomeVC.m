//
//  LegalTenderTransactionVC.m
//  BOB
//
//  Created by mac on 2019/12/10.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "RealNameHomeVC.h"
#import "NSObject+Mine.h"
static NSString* identifier = @"cell";
@interface RealNameHomeVC ()<UITableViewDelegate,UITableViewDataSource>

@property (strong , nonatomic) UITableView *tableView;

@end

@implementation RealNameHomeVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setup_my_info:@{} completion:^(BOOL success, NSString *error) {
        if (success) {
            [self.tableView reloadData];
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

-(void)setupUI{
    [self setNavBarTitle:@"实名认证" color:[UIColor moBlack]];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    AdjustTableBehavior(_tableView);
}

- (UITableView*)tableView{
    if (!_tableView) {
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.backgroundColor =  [UIColor whiteColor];
    _tableView.estimatedRowHeight = 44;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.tableFooterView = [UIView new];
    
    }
    return _tableView;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:identifier];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.attributedText = [self leftAttr:@"身份认证" desc:@"上传身份证信息,\n认证后提升提币额度"];
        NSString* auth = UDetail.user.auth;
        if ([auth isEqualToString:@"1"]) {
            cell.detailTextLabel.text = @"已认证";
        }else if([auth isEqualToString:@"2"]){
            cell.detailTextLabel.text = @"审核中";
        }else if([auth isEqualToString:@"3"]){
            cell.detailTextLabel.text = @"认证失败";
        }else{
            cell.detailTextLabel.text = @"未认证";
        }
        cell.detailTextLabel.font = [UIFont font15];
        cell.detailTextLabel.textColor = [UIColor moBlueColor];
    }
    return cell;
}

-(NSMutableAttributedString*)leftAttr:(NSString*)title desc:(NSString*)desc{
    NSMutableAttributedString* attr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\n%@",title,desc]];
    [attr addFont:[UIFont font15] substring:title];
    [attr addFont:[UIFont font11] substring:desc];
    [attr addColor:[UIColor moBlack] substring:title];
    [attr addColor:[UIColor grayColor] substring:desc];
    [attr setLineSpacing:13 substring:title alignment:NSTextAlignmentLeft];
    [attr setLineSpacing:10 substring:desc alignment:NSTextAlignmentLeft];
    return attr;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* auth = UDetail.user.auth;
    if ([auth isEqualToString:@"0"]) {
        [MXRouter openURL:@"lcwl://RealNameAuthVC"];
    }else if([auth isEqualToString:@"3"]){
        [MXRouter openURL:@"lcwl://RealNameResultVC"];
    }
    
}

@end
