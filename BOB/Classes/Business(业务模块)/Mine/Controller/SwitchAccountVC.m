//
//  SwitchAccountVC.m
//  BOB
//
//  Created by mac on 2019/12/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "SwitchAccountVC.h"
#import "SPButton.h"
@interface SwitchAccountVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView      *tableView;

@property (nonatomic, strong) NSMutableArray   *dataArray;

@end

@implementation SwitchAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

-(void)setupUI{
    [self setNavBarTitle:@"切换账号" color:[UIColor moBlack]];
    [self.view addSubview:self.tableView];
    self.dataArray = @[@{@"title":@"账号",@"desc":@"1888888888"},
                        @{@"title":@"账号",@"desc":@"1888888888"},
                       @{@"title":@"账号",@"desc":@"1888888888"}];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    AdjustTableBehavior(_tableView);
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIView* footer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 55)];
        footer.backgroundColor = [UIColor whiteColor];
        SPButton* addBtn = [[SPButton alloc] initWithImagePosition:SPButtonImagePositionLeft];
        addBtn.imageTitleSpace = 7;
        [addBtn setTitle:@"添加账号" forState:UIControlStateNormal];
        [addBtn setTitleColor:[UIColor moBlueColor] forState:UIControlStateNormal];
        addBtn.titleLabel.font = [UIFont font15];
        [addBtn setImage:[UIImage imageNamed:@"交易所_加"] forState:UIControlStateNormal];
        [footer addSubview:addBtn];
        [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@(0));
            make.left.equalTo(@(15));
        }];
        _tableView.tableFooterView = footer;
        
        
    }
    
    return _tableView;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray* array = self.dataArray[section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* identifier = [NSString stringWithFormat:@"%ld %ld",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont font15];
        cell.detailTextLabel.font = [UIFont font11];
        cell.detailTextLabel.textColor = [UIColor grayColor];
        UIImageView* image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"交易所_对"]];
        image.frame = CGRectMake(0, 0, 15, 15);
        cell.accessoryView = image;
        MXSeparatorLine* line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
        [cell addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(cell);
            make.height.equalTo(@(.5));
        }];
    }
    NSDictionary* data = self.dataArray[indexPath.row];
    cell.textLabel.text = data[@"title"];
    cell.detailTextLabel.text = data[@"desc"];
    return cell;
}
@end
