//
//  ShowPlainextVC.m
//  Lcwl
//
//  Created by mac on 2019/1/25.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "ShowPlainextVC.h"

@interface ShowPlainextVC ()

@property (weak, nonatomic) IBOutlet UITextView *plainText;

@end

@implementation ShowPlainextVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarTitle:@"消息详情"];
    if(self.content == nil) {
        return;
    }
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:[UIView fullLine]];
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor moBlack];
    lbl.font = [UIFont boldFont15];
    lbl.text = self.title;
    lbl.myTop = 15;
    lbl.myLeft = 15;
    lbl.myRight = 15;
    lbl.myBottom = 15;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    lbl.size = CGSizeMake(SCREEN_WIDTH - 30, size.height);
    lbl.mySize = lbl.size;
    [layout addSubview:lbl];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];;
    subLayout.myLeft = 15;
    subLayout.myRight = 15;
    subLayout.myHeight = MyLayoutSize.wrap;
    subLayout.myBottom = 20;
    [layout addSubview:subLayout];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo圆"]];
    imgView.mySize = CGSizeMake(20, 20);
    imgView.myCenterY = 0;
    [subLayout addSubview:imgView];
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#C1C1C1"];
    lbl.font = [UIFont font12];
    lbl.text = [DeviceManager getAppName];
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    lbl.myLeft = 15;
    [subLayout addSubview:lbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#C1C1C1"];
    lbl.font = [UIFont font12];
    lbl.text = self.time;
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    lbl.myRight = 0;
    [subLayout addSubview:lbl];
    [layout addSubview:self.plainText];
    if([self.content isKindOfClass:[NSString class]]) {
        self.plainText.text = self.content ?: @"";
    } else if([self.content isKindOfClass:[NSAttributedString class]]) {
        self.plainText.attributedText = self.content;
    }
    self.plainText.weight = 1;
    self.plainText.myLeft = 15;
    self.plainText.myRight = 15;
}

@end
