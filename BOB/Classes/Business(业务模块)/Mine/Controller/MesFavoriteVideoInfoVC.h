//
//  MesFavoriteVideoInfoVC.h
//  BOB
//
//  Created by mac on 2020/8/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "MesFavoriteListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MesFavoriteVideoInfoVC : BaseViewController

@property (nonatomic, strong) MesFavoriteListObj *obj;

@end

NS_ASSUME_NONNULL_END
