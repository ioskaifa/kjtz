//
//  MyQRCodeVC.m
//  Lcwl
//
//  Created by AlphaGO on 2018/11/16.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "ChargeCoinVC.h"
#import "UIImage+Color.h"
#import "MBProgressHUD.h"
#import "UIView+Utils.h"
#import "UIImage+Utils.h"
#import "UINavigationBar+Alpha.h"
#import "UIActionSheet+MKBlockAdditions.h"

@interface ChargeCoinVC ()
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIView *whiteBG;
@property (weak, nonatomic) IBOutlet UIImageView *qrCode;
@property (weak, nonatomic) IBOutlet UILabel *info;
@property (weak, nonatomic) IBOutlet UIView *shadeView;
@property (weak, nonatomic) IBOutlet UIImageView *blueBar;
@property (weak, nonatomic) IBOutlet UIImageView *BOBLogo;
@property (weak, nonatomic) IBOutlet UIButton *iconCopyBnt;
@property (weak, nonatomic) IBOutlet UIButton *addressBnt;

@property (assign, nonatomic) BOOL firstEntry;
@end

@implementation ChargeCoinVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = Lang(@"充币");
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#1DAAAC"];
    self.firstEntry = YES;
    self.whiteBG.clipsToBounds = YES;
    self.whiteBG.layer.cornerRadius = 15;
    
    self.logo.clipsToBounds = YES;
    self.logo.layer.cornerRadius = 12;
    //self.info.textColor = [UIColor moTextGray];
    //self.name.text = UDetail.user.nickName;
    
    NSURL *url = [NSURL URLWithString:UDetail.user.head_photo];
    [self.logo sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    
    //self.shadeView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
    self.shadeView.hidden = YES;
    [self.view insertSubview:self.shadeView belowSubview:self.whiteBG];
    
    [self setNavBarRightBtnWithTitle:@"充币记录" andImageName:nil];
    [self.navBarRightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.backBut setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateNormal];
    self.qrCode.backgroundColor = [UIColor moBackground];
    
    [self.addressBnt setTitle:UDetail.user.sla_address forState:UIControlStateNormal];
    
    [self.addressBnt setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    [self.iconCopyBnt setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    
    if(![StringUtil isEmpty:UDetail.user.sla_address]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *qrCode = [UIImage encodeQRImageWithContent:UDetail.user.sla_address size:CGSizeMake(SCREEN_HEIGHT, SCREEN_HEIGHT)];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.qrCode.image = qrCode;
            });
        });
    }
    
    [self layout];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setNavBarCurrentColor:[UIColor clearColor] titleTextColor:[UIColor whiteColor]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //[self.navigationController.navigationBar barReset];
}

- (void)navBarRightBtnAction:(id)sender {
    [MXRouter openURL:@"lcwl://RechargeRecordVC"];
    return;
    
    __block NSArray *arr = @[@"保存图片"];
    @weakify(self)
    [UIActionSheet actionSheetWithTitle:nil message:nil buttons:arr showInView:MoApp.window onDismiss:^(NSInteger buttonIndex) {
        @strongify(self)
        if(buttonIndex == 0) {
            UIImage *image = [self.whiteBG convertViewToImageWithCornerRadius:0];
            if(image != nil) {
                UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
            }
        } else if(buttonIndex == 1) {
            
        }
        
    } onCancel:nil];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        [NotifyHelper showMessageWithMakeText:@"保存失败"];
    } else {
        [NotifyHelper showMessageWithMakeText:@"保存成功"];
    }
}

- (void)layout {
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.blueBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBG.mas_left);
        make.right.equalTo(self.whiteBG.mas_right);
        make.top.equalTo(@(0));
        make.height.equalTo(@(80));
    }];
    
    [self.shadeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBG).offset(40);
        make.left.equalTo(self.whiteBG).offset(15);
        make.width.height.equalTo(@(50));
    }];
    
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.whiteBG.mas_left);
        make.right.equalTo(self.whiteBG.mas_right);
        make.top.equalTo(@(20));
        make.height.equalTo(@(24));
    }];
    
    [self.addressBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.whiteBG);
        make.bottom.equalTo(self.blueBar.mas_bottom).offset(-5);
        make.right.equalTo(self.iconCopyBnt.mas_left).offset(-10);
    }];
    
    [self.iconCopyBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.addressBnt.mas_centerY);
        make.right.equalTo(@(-15));
    }];
    
    [self.qrCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.blueBar.mas_bottom).offset(42);
        make.centerX.equalTo(self.whiteBG);
        make.width.height.equalTo(@(200));
    }];
    
    [self.info mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.qrCode.mas_bottom).offset(35);
        make.left.right.equalTo(self.whiteBG);
        make.width.height.equalTo(@(12));
    }];
    
    [self.whiteBG mas_makeConstraints:^(MASConstraintMaker *make) {
        //CGFloat contentHeight = SCREEN_HEIGHT - NavigationBar_Height;
        make.centerY.equalTo(self.view).offset(-44);
        make.left.equalTo(self.view).offset(40);
        make.right.equalTo(self.view).offset(-40);
        make.height.equalTo(@(394)).priorityHigh();
    }];
    
    [self.BOBLogo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBG.mas_bottom).offset(45);
        make.centerX.equalTo(self.whiteBG.mas_centerX);
        make.width.equalTo(@(243.5));
        make.height.equalTo(@(88));
    }];
    
}
- (IBAction)addressCopyClick:(id)sender {
    if([StringUtil isEmpty:self.addressBnt.titleLabel.text]) {
        return;
    }
    
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.addressBnt.titleLabel.text;
    [NotifyHelper showMessageWithMakeText:Lang(@"复制成功")];
}

@end
