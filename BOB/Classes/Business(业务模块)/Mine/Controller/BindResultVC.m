//
//  BindPhoneVC.m
//  BOB
//
//  Created by mac on 2019/12/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BindResultVC.h"

@interface BindResultVC ()

@property (nonatomic,strong) UIImageView* phoneImg;

@property (nonatomic,strong) UILabel* phoneLbl;

@property (nonatomic,strong) UILabel* tipLbl;

@property (nonatomic,strong) UIButton* contactBtn;


@end

@implementation BindResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

-(void)setupUI{
    [self setNavBarTitle:@"更换手机号" color:[UIColor moBlack]];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.phoneImg];
    [self.view addSubview:self.phoneLbl];
    [self.view addSubview:self.tipLbl];
    [self.view addSubview:self.contactBtn];
    [self.phoneImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(169));
        make.height.equalTo(@(162));
        make.top.equalTo(@(44));
        make.centerX.equalTo(@(0));
    }];
    [self.phoneLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneImg.mas_bottom).offset(26);
        make.left.right.equalTo(self.view);
    }];
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneLbl.mas_bottom).offset(26);
         make.left.equalTo(@(15));
               make.right.equalTo(@(-15));
    }];
    [self.contactBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLbl.mas_bottom).offset(44);
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.height.equalTo(@(35));
    }];
}

-(UIImageView*)phoneImg{
    if (!_phoneImg) {
        _phoneImg = [UIImageView new];
        _phoneImg.image = [UIImage imageNamed:@"交易所_手机"];
    }
    return _phoneImg;
}

-(UILabel*)phoneLbl{
    if (!_phoneLbl) {
        _phoneLbl = [UILabel new];
        _phoneLbl.text = @"更换手机成功";
        _phoneLbl.textColor = [UIColor moBlack];
        _phoneLbl.font = [UIFont font15];
        _phoneLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _phoneLbl;
}

-(UILabel*)tipLbl{
    if (!_tipLbl) {
        _tipLbl = [UILabel new];
        _tipLbl.text = @"下次登录可使用新手机号登录。";
        _tipLbl.numberOfLines = 2;
        _tipLbl.textColor = [UIColor grayColor];
        _tipLbl.font = [UIFont font15];
        _tipLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _tipLbl;
}

-(UIButton*)contactBtn{
    if (!_contactBtn) {
        _contactBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _contactBtn.backgroundColor = [UIColor moBlueColor];
        [_contactBtn setTitle:@"我知道了" forState:UIControlStateNormal];
        _contactBtn.titleLabel.font = [UIFont font15];
        _contactBtn.layer.cornerRadius = 4;
        @weakify(self)
        [_contactBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
    }
    return _contactBtn;
}


@end
