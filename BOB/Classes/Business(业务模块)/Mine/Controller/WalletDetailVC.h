//
//  WalletDetailListVC.h
//  BOB
//
//  Created by mac on 2020/1/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseSTDTableViewVC.h"
#import "WalletRecordDetailObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletDetailVC : BaseViewController

@property (nonatomic, copy) NSString *op_type;

@property (nonatomic, copy) NSString *op_order_no;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) WalletRecordDetailObj *obj;

@property (nonatomic, strong) MyBaseLayout *headerView;

@property (nonatomic, copy) FinishedBlock finishedBlock;

@end

NS_ASSUME_NONNULL_END
