//
//  LooseChangeVC.m
//  BOB
//
//  Created by mac on 2020/1/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LooseChangeVC.h"
#import "SPButton.h"
//#import "TransferVC.h"

@interface LooseChangeVC ()
@property (strong , nonatomic)SPButton *walletBnt;
@property (strong , nonatomic)UILabel *moneyLbl;
@property (strong , nonatomic)UIButton *catogryBnt;
@property (strong , nonatomic)UIView *bgView;
@property (strong , nonatomic)UIButton *transBnt;
@end

@implementation LooseChangeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarRightBtnWithTitle:@"钱包明细" andImageName:nil];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.bgView addSubview:self.walletBnt];
    [self.bgView addSubview:self.moneyLbl];
    [self.bgView addSubview:self.catogryBnt];
    [self.bgView addSubview:self.transBnt];
    
    [self.view addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(@(0));
        make.top.equalTo(@(50));
        make.height.equalTo(@(400));
    }];
    
    [self.walletBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.bgView.mas_centerX);
        make.top.equalTo(@(0));
        make.height.equalTo(@(104));
    }];
    
    [self.moneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_walletBnt.mas_centerX);
        make.top.equalTo(self.walletBnt.mas_bottom).offset(20);
        make.height.equalTo(@(34));
    }];
    
    [self.catogryBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_walletBnt.mas_centerX);
        make.top.equalTo(self.moneyLbl.mas_bottom).offset(34);
        make.height.equalTo(@(14));
    }];
    
    [self.transBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_walletBnt.mas_centerX);
        make.top.equalTo(self.catogryBnt.mas_bottom).offset(58);
        make.height.equalTo(@(37));
        make.width.equalTo(@(173));
    }];
    
    [self.transBnt addAction:^(UIButton *btn) {
//        [MXRouter openURL:@"lcwl://SelectCoinVC" parameters:@{@"selected":^(id data){
//            TransferVC* takeVC = [TransferVC new];
//            takeVC.select = data;
//            [MXRouter openVC:takeVC from:self];
//        }}];
    }];
}

- (void)navBarRightBtnAction:(id)sender {
    [MXRouter openURL:@"lcwl://WalletFlowVC"];
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
    }
    return _bgView;
}

- (SPButton *)walletBnt{
    if (!_walletBnt) {
        _walletBnt = [[SPButton alloc] initWithImagePosition:SPButtonImagePositionTop];
        _walletBnt.frame = CGRectMake(0, 0, 50, 65);
        [_walletBnt setTitle:Lang(@"我的钱包") forState:UIControlStateNormal];
        [_walletBnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _walletBnt.titleLabel.font = [UIFont font15];
        [_walletBnt setImage:[UIImage imageNamed:@"零钱Logo"] forState:UIControlStateNormal];
        [_walletBnt setImageTitleSpace:38];
        [_walletBnt addTarget:self action:@selector(headerAction:) forControlEvents:UIControlEventTouchUpInside];

    }
    return _walletBnt;
}

- (UILabel *)moneyLbl {
    if (!_moneyLbl) {
        _moneyLbl = [[UILabel alloc] init];
        _moneyLbl.font = [UIFont boldSystemFontOfSize:40];
        _moneyLbl.textColor = [UIColor blackColor];
        _moneyLbl.text = StrF(@"￥%@",UDetail.user.total_bb_money);
    }
    return _moneyLbl;
}

- (UIButton *)catogryBnt {
    if (!_catogryBnt) {
        _catogryBnt = [[UIButton alloc] init];
        
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"查看钱包分类 >" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFang SC" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:222/255.0 green:174/255.0 blue:116/255.0 alpha:1.0]}];

        [_catogryBnt setAttributedTitle:string forState:UIControlStateNormal];
        [_catogryBnt addAction:^(UIButton *btn) {
            [MXRouter openURL:@"lcwl://WalletCategoryVC"];
        }];
    }
    return _catogryBnt;
}

- (UIButton *)transBnt {
    if (!_transBnt) {
        _transBnt = [[UIButton alloc] init];
        _transBnt.backgroundColor = [UIColor colorWithHexString:@"#253FD8"];
        [_transBnt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _transBnt.titleLabel.font = [UIFont systemFontOfSize:15];
        _transBnt.layer.cornerRadius = 5;
        [_transBnt setTitle:@"转账" forState:UIControlStateNormal];
    }
    return _transBnt;
}
@end
