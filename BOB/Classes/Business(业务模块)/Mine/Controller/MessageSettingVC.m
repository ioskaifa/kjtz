//
//  MessageSettingVC.m
//  BOB
//
//  Created by mac on 2020/7/4.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MessageSettingVC.h"
#import "NSObject+Mine.h"

@interface MessageSettingVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MyLinearLayout *layout;

@property (nonatomic, strong) MyLinearLayout *setLayout;

@property (nonatomic, strong) UIButton *notifyBtn;

@property (nonatomic, strong) UIButton *voiceBtn;

@property (nonatomic, strong) UIButton *vibrateBtn;

@end

@implementation MessageSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self configureSwitchBtnState];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self commit];
}

#pragma mark - Private Method
- (void)configureSwitchBtnState {
    self.notifyBtn.selected = UDetail.user.is_notify;
    self.voiceBtn.selected = UDetail.user.is_voice;
    self.vibrateBtn.selected = UDetail.user.is_vibrate;

    if (self.notifyBtn.selected) {
        self.setLayout.visibility = MyVisibility_Visible;
    } else {
        self.setLayout.visibility = MyVisibility_Gone;
    }
}

- (void)commit {
    NSMutableDictionary *paraMDic = [NSMutableDictionary dictionary];
    NSString *setting = self.notifyBtn.selected ? @"1":@"0";
    if (UDetail.user.is_notify != self.notifyBtn.selected) {
        UDetail.user.is_notify = self.notifyBtn.selected;
        [paraMDic setValue:setting forKey:@"is_notify"];
    }
    setting = self.voiceBtn.selected ? @"1":@"0";
    if (UDetail.user.is_voice != self.voiceBtn.selected) {
        UDetail.user.is_voice = self.voiceBtn.selected;
        [paraMDic setValue:setting forKey:@"is_voice"];
    }
    setting = self.vibrateBtn.selected ? @"1":@"0";
    if (UDetail.user.is_vibrate != self.vibrateBtn.selected) {
        UDetail.user.is_vibrate = self.vibrateBtn.selected;
        [paraMDic setValue:setting forKey:@"is_vibrate"];
    }
    if (paraMDic.allKeys.count == 0) {
        return;
    }
    [self modifyUserInfo:paraMDic showMsg:NO completion:nil];
}

#pragma mark - IBAction
- (void)newSwitchClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.setLayout.visibility = MyVisibility_Visible;
    } else {
        self.setLayout.visibility = MyVisibility_Gone;
    }
}

- (void)switchClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"新消息通知"];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self initUI];
}

- (void)initUI {
    MyLinearLayout *lineLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    lineLayout.myHeight = 60;
    lineLayout.myLeft = 0;
    lineLayout.myRight = 0;
    UILabel *lbl = [self factoryLbl:@"新消息通知"];
    [lineLayout addSubview:lbl];
    UIView *view = [UIView new];
    view.weight = 1;
    view.myCenterY = 0;
    view.myHeight = 1;
    [lineLayout addSubview:view];
    UIButton *btn = [self factorySwitchBtn];
    [btn addTarget:self action:@selector(newSwitchClick:) forControlEvents:UIControlEventTouchUpInside];
    [lineLayout addSubview:btn];
    self.notifyBtn = btn;
    [self.layout addSubview:lineLayout];
    [self.layout addSubview:[UIView gapLine]];
    
    lineLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    lineLayout.myHeight = 60;
    lineLayout.myLeft = 0;
    lineLayout.myRight = 0;
    lbl = [self factoryLbl:@"声音"];
    [lineLayout addSubview:lbl];
    view = [UIView new];
    view.weight = 1;
    view.myCenterY = 0;
    view.myHeight = 1;
    [lineLayout addSubview:view];
    btn = [self factorySwitchBtn];
    [btn addTarget:self action:@selector(switchClick:) forControlEvents:UIControlEventTouchUpInside];
    [lineLayout addSubview:btn];
    self.voiceBtn = btn;
    [self.setLayout addSubview:lineLayout];
    [self.setLayout addSubview:[UIView line]];
    
    lineLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    lineLayout.myHeight = 60;
    lineLayout.myLeft = 0;
    lineLayout.myRight = 0;
    lbl = [self factoryLbl:@"震动"];
    [lineLayout addSubview:lbl];
    view = [UIView new];
    view.weight = 1;
    view.myCenterY = 0;
    view.myHeight = 1;
    [lineLayout addSubview:view];
    btn = [self factorySwitchBtn];
    [btn addTarget:self action:@selector(switchClick:) forControlEvents:UIControlEventTouchUpInside];
    [lineLayout addSubview:btn];
    self.vibrateBtn = btn;
    [self.setLayout addSubview:lineLayout];
    [self.layout addSubview:self.setLayout];
    
    [self.setLayout layoutSubviews];
    [self.layout layoutSubviews];
    self.tableView.tableHeaderView = self.layout;
}

- (UILabel *)factoryLbl:(NSString *)txt {
    UILabel *object = [UILabel new];
    object.font = [UIFont font14];
    object.textColor = [UIColor blackColor];
    object.text = txt;
    [object sizeToFit];
    object.mySize = object.size;
    object.myLeft = 15;
    object.myCenterY = 0;
    return object;
}

- (UIButton *)factorySwitchBtn {
    UIButton *object = [UIButton new];
    [object setImage:[UIImage imageNamed:@"icon_switch_on"] forState:UIControlStateSelected];
    [object setImage:[UIImage imageNamed:@"icon_switch_off"] forState:UIControlStateNormal];
    [object sizeToFit];
    object.selected = YES;
    object.mySize = object.size;
    object.myCenterY = 0;
    object.myRight = 15;
    return object;
}

- (UIButton *)factoryArrowBtn {
    UIButton *object = [UIButton new];
    [object setImage:[UIImage imageNamed:@"Arrow"] forState:UIControlStateNormal];
    object.userInteractionEnabled = NO;
    [object sizeToFit];
    object.mySize = object.size;
    object.myCenterY = 0;
    return object;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor moBackground];
    }
    return _tableView;
}

- (MyLinearLayout *)layout {
    if (!_layout) {
        _layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _layout.backgroundColor = [UIColor whiteColor];
        _layout.myTop = 0;
        _layout.myLeft = 0;
        _layout.myRight = 0;
        _layout.myHeight = MyLayoutSize.wrap;
    }
    return _layout;
}

- (MyLinearLayout *)setLayout {
    if (!_setLayout) {
        _setLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _setLayout.backgroundColor = [UIColor whiteColor];
        _setLayout.myTop = 0;
        _setLayout.myLeft = 0;
        _setLayout.myRight = 0;
        _setLayout.myHeight = MyLayoutSize.wrap;
    }
    return _setLayout;
}

@end
