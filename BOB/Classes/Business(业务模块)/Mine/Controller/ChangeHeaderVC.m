//
//  ChangeHeaderVC.m
//  Lcwl
//
//  Created by mac on 2018/11/23.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "ChangeHeaderVC.h"
#import "UINavigationBar+Alpha.h"
#import "UIImage+Color.h"
#import "PhotoBrowser.h"
#import "QNManager.h"
#import "NSObject+Mine.h"
@interface ChangeHeaderVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *changeBnt;

@end

@implementation ChangeHeaderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#323332"];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view).offset(-20);
        make.width.height.equalTo(@(SCREEN_WIDTH));
    }];
    
    [self.changeBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView.mas_bottom).offset(15);
        make.width.equalTo(@(70));
        make.height.equalTo(@(30));
        make.centerX.equalTo(self.view);
    }];
    
    self.changeBnt.clipsToBounds = YES;
    self.changeBnt.layer.borderColor = [UIColor whiteColor].CGColor;
    self.changeBnt.layer.borderWidth = 1;
    self.changeBnt.layer.cornerRadius = 6;
    
    [self.backBut setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:self.imageUrl] ?: nil placeholderImage:[UIImage imageNamed:@"avatar_default"]];
}

- (IBAction)changeSelector:(id)sender {
    [[PhotoBrowser shared] showSelectSinglePhotoLibrary:self completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
        if([images isKindOfClass:[NSArray class]]) {
            if([[images firstObject] isKindOfClass:[UIImage class]]) {
                UIImage *image = [images firstObject];
                self.imageView.image = image;
                [NotifyHelper showHUDAddedTo:self.view animated:YES];
                [[QNManager shared] uploadImage:image completion:^(id data) {
                    [NotifyHelper hideHUDForView:self.view animated:YES];
                    if(data && [NSURL URLWithString:data]) {
                        UDetail.user.head_photo = data;
                        Block_Exec(self.block,data);
                        
                        
//                        [MineHelper modifyUserInfo:@{@"head_photo": data} completion:^(BOOL success, id object, NSString *error) {
//                            [NotifyHelper hideHUDForView:self.view animated:YES];
//                            [[NSNotificationCenter defaultCenter] postNotificationName:@"ModifyInfoVC" object:nil];
//                        }];
                    }
                }];
            }
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar barReset];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setNavBarCurrentColor:[UIColor clearColor] titleTextColor:[UIColor blackColor]];
}

@end
