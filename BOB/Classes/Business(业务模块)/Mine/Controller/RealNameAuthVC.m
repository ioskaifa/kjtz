//
//  RealNameAuthVC.m
//  BOB
//
//  Created by mac on 2019/12/24.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "RealNameAuthVC.h"
#import "InvitationCodeView.h"
#import "UploadIdCardCell.h"
#import "PhotoBrowser.h"
#import "QNManager.h"
#import "UIButton+WebCache.h"
#import "NSObject+Mine.h"
static NSString* identifier = @"upuload";
@interface RealNameAuthVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) InvitationCodeView* name;

@property (nonatomic,strong) InvitationCodeView* idCard;

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) NSString *nameStr;

@property (nonatomic,strong) NSString *idCardStr;

@property (nonatomic,strong) NSString *frontPic;

@property (nonatomic,strong) NSString *backPic;

@property (nonatomic,strong) NSString *verifyCode;

@end

@implementation RealNameAuthVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

-(void)setupUI{
    [self setNavBarTitle:@"身份认证" color:[UIColor moBlack]];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.edges.equalTo(@(UIEdgeInsetsZero));
    }];
    AdjustTableBehavior(_tableView);
}

-(InvitationCodeView*)name{
    if (!_name) {
        _name = [InvitationCodeView new];
        [_name reloadTitle:@"姓名" placeHolder:@"请输入您的姓名"];
        [_name setTitleWidth:60];
        @weakify(self)
        _name.getText = ^(id data) {
            @strongify(self)
            self.nameStr = data;
        };
    }
    return _name;
}

-(InvitationCodeView*)idCard{
    if (!_idCard) {
        _idCard = [InvitationCodeView new];
        [_idCard reloadTitle:@"身份证号" placeHolder:@"请输入身份证号码"];
        [_idCard setTitleWidth:60];
        @weakify(self)
        _idCard.getText = ^(id data) {
            @strongify(self)
            self.idCardStr = data;
       };
    }
    return _idCard;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[UploadIdCardCell class] forCellReuseIdentifier:identifier];
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 44;
    }
    return _tableView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            NSString* identifier = @"cell0";
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell addSubview:self.name];
                [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
                   make.edges.equalTo(cell).insets(UIEdgeInsetsMake(0, 15, 0, 15));
                }];
            }
            return cell;
        }else if(indexPath.row == 1){
            NSString* identifier = @"cell1";
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell addSubview:self.idCard];
                [self.idCard mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.edges.equalTo(cell).insets(UIEdgeInsetsMake(0, 15, 0, 15));
                }];
            }
            return cell;
        }
    }else if(indexPath.section == 1){
        UploadIdCardCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.front = ^{
            [[PhotoBrowser shared] showPhotoLibrary:self completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
                    if([images isKindOfClass:[NSArray class]]) {
                        if([[images firstObject] isKindOfClass:[UIImage class]]) {
                            UIImage *image = [images firstObject];
                            [NotifyHelper showHUDAddedTo:self.view animated:YES];
                            [[QNManager shared] uploadImage:image completion:^(id data) {
                                [NotifyHelper hideAllHUDsForView:self.view animated:YES];
                                if(data && [NSURL URLWithString:data]) {
                                    self.frontPic = data;
                                    NSString* imageUrl = [NSString stringWithFormat:@"%@/%@",[QNManager shared].qnHost,data];
                                    [cell.frontBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:imageUrl] forState:UIControlStateNormal];
                                    [cell.frontBtn setTitle:@"" forState:UIControlStateNormal];
                                    [cell.frontBtn setImage:[UIImage new] forState:UIControlStateNormal];
                                }
                            }];
                        }
                    }
            }];
        };
        cell.back = ^{
            [[PhotoBrowser shared] showPhotoLibrary:self completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
                   if([images isKindOfClass:[NSArray class]]) {
                       if([[images firstObject] isKindOfClass:[UIImage class]]) {
                           UIImage *image = [images firstObject];
                           [NotifyHelper showHUDAddedTo:self.view animated:YES];
                           [[QNManager shared] uploadImage:image completion:^(id data) {
                                [NotifyHelper hideAllHUDsForView:self.view animated:YES];
                               if(data && [NSURL URLWithString:data]) {
                                   self.backPic = data;
                                   NSString* imageUrl = [NSString stringWithFormat:@"%@/%@",[QNManager shared].qnHost,data];
                                    [cell.backBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:imageUrl] forState:UIControlStateNormal];
                                   [cell.backBtn setTitle:@"" forState:UIControlStateNormal];
                                   [cell.backBtn setImage:[UIImage new] forState:UIControlStateNormal];
                               }
                           }];
                       }
                   }
               }];
        };
        cell.getText = ^(id data) {
            self.verifyCode = data;
        };
        return cell;
    }else{
        NSString* identifier = @"cell3";
       UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
       if (!cell) {
           cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
           cell.selectionStyle = UITableViewCellSelectionStyleNone;
           UIView* bottom = [UIView new];
           [cell addSubview:bottom];
           [bottom mas_makeConstraints:^(MASConstraintMaker *make) {
               make.top.left.right.equalTo(cell);
               make.height.equalTo(@(88));
               make.bottom.equalTo(cell);
           }];
           
           UIButton* btn = [UIButton new];
           btn.layer.cornerRadius = 4;
           btn.titleLabel.font = [UIFont font15];
           [btn setBackgroundColor:[UIColor moBlueColor]];
           [btn setTitle:@"提交" forState:UIControlStateNormal];
           [bottom addSubview:btn];
           [btn mas_makeConstraints:^(MASConstraintMaker *make) {
               make.left.equalTo(@(15));
               make.right.equalTo(@(-15));
               make.height.equalTo(@(35));
               make.centerY.equalTo(bottom);
           }];
           [btn addAction:^(UIButton *btn) {
               [self setup_get_realname:@{@"realname":self.nameStr,@"idcard":self.idCardStr,@"pic1":self.frontPic,@"pic2":self.backPic,@"code":self.verifyCode} completion:^(BOOL success, NSString *error) {
                   if (success) {
                       [self.navigationController popToRootViewControllerAnimated:YES];
                   }
               }];
           }];
       }
       return cell;
    }
    return nil;
  
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 2;
    }else if(section == 1){
        return 1;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    return 10;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
}
@end
