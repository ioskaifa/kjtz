//
//  ModifyPhoneStep1VC.m
//  BOB
//
//  Created by mac on 2019/7/13.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "ModifyPhoneStep1VC.h"
#import "InvitationCodeView.h"
#import "ImgCaptchaView.h"
#import "TextCaptchaView.h"
static NSInteger padding = 15;
@interface ModifyPhoneStep1VC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) InvitationCodeView* phoneView;

@property (nonatomic, strong) ImgCaptchaView    *imgVerifyView;

@property (nonatomic, strong) TextCaptchaView   *verifyView;

@property (nonatomic, strong) UIButton          *submitBtn;

@property (nonatomic, strong) NSArray          *dataArray;

@property (nonatomic, strong) UITableView      *tableView;

@end

@implementation ModifyPhoneStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self initData];
}

-(void)initUI{
    [self setNavBarTitle:@"手机号修改"];
    self.dataArray = @[
                      @{@"title":Lang(@"当前手机号"),@"desc":UDetail.user.user_tel},
                      @{@"title":Lang(@"图形验证码"),@"placeholder":Lang(@"请输入验证码")},
                      @{@"title":Lang(@"短信验证码"),@"placeholder":Lang(@"请输入验证码")}
                      ];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
  
    
}

-(void)initData{
//    [self.phoneView reloadTitle:@"当前手机号" placeHolder:@""];
//    self.phoneView.tf.text = @"150****2226";
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitBtn.layer.masksToBounds = YES;
        _submitBtn.layer.cornerRadius = 22;
        [_submitBtn setTitle:Lang(@"下一步") forState:UIControlStateNormal];
        _submitBtn.clipsToBounds =NO;
        [_submitBtn addTarget:self action:@selector(goTo:) forControlEvents:UIControlEventTouchUpInside];
        [_submitBtn setBackgroundColor:[UIColor moBlueColor]];
    }
    return _submitBtn;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor clearColor];
        AdjustTableBehavior(_tableView);
        _tableView.separatorColor = [UIColor moBackground];
        UIView* foot = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
        [foot addSubview:self.submitBtn];
        [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(foot.mas_centerX);
            make.height.equalTo(@(44));
            make.width.equalTo(@(180));
            make.bottom.equalTo(foot);
        }];
        _tableView.tableFooterView = foot;
        
    }
    
    return _tableView;
}

-(void)goTo:(id)sender{
    [MXRouter openURL:@"lcwl://ModifyPhoneStep2VC"];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UIView *view = [UIView new];
//    view.backgroundColor = [UIColor moBackground];
//    return view;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    if(section == 1) {
//        return 10;
//    }
//    return 0;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* identifier = [NSString stringWithFormat:@"%ld %ld",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView* tmpView = [self getViewFromIndexPath:indexPath];
        [cell.contentView addSubview:tmpView];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor whiteColor];
        [tmpView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView).insets(UIEdgeInsetsMake(0, 15, 0, 15));
        }];
    }
    
    return cell;
}

-(UIView*)getViewFromIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* dict = self.dataArray[indexPath.row];
    NSString* title = dict[@"title"];
    NSString* placehoder = dict[@"placeholder"];
    UIView* tmpView = nil;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            tmpView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
            self.phoneView = (InvitationCodeView*)tmpView;
            self.phoneView.tf.enabled = NO;
            [self.phoneView reloadTitle:title placeHolder:@""];
            self.phoneView.tf.text = dict[@"desc"];
        }else if(indexPath.row == 1){
            tmpView = [[ImgCaptchaView alloc]initWithFrame:CGRectZero];
            self.imgVerifyView = (ImgCaptchaView*)tmpView;
            [self.imgVerifyView reloadTitle:title placeHolder:placehoder];
            @weakify(self)
            self.imgVerifyView.getText = ^(id data) {
                @strongify(self)
                
            };
            self.imgVerifyView.picBlock = ^(id data) {
                @strongify(self)
                
            };
        }else if(indexPath.row == 2){
            tmpView = [[TextCaptchaView alloc]initWithFrame:CGRectZero];
            self.verifyView = (TextCaptchaView*)tmpView;
            [self.verifyView reloadTitle:title placeHolder:placehoder];
            [self.verifyView isHiddenLine:YES];
            @weakify(self)
            self.verifyView.getText = ^(id data) {
                @strongify(self)
            };
            self.verifyView.sendCodeBlock = ^(id data) {
                @strongify(self)
                
            };
        }
    }
    return tmpView;
}
@end
