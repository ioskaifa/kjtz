//
//  WithdrawDetailVC.m
//  BOB
//
//  Created by mac on 2019/7/12.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "WithdrawDetailVC.h"
#import "WithdrawalRecordView2.h"
#import "PhoneNumView2.h"
#import "MineHelper.h"
@interface WithdrawDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic , strong) UITableView* tableView;

@property (nonatomic , strong) WithdrawalRecordView2* recordView;

@property (nonatomic , strong) NSMutableArray* dataArray;


@end

@implementation WithdrawDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self initData];
}

-(void)initUI{
    [self setNavBarTitle:@"详情"];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

-(void)initData{
    self.dataArray = @[@{@"title":@"提币地址",@"desc":self.model.address},
                         @{@"title":@"提币凭证\n(hash值)",@"desc":self.model.hashCode},
                       @{@"title":@"手续费",@"desc":self.model.charge,@"textColor":[UIColor redColor]},
                       @{@"title":@"交易时间",@"desc":self.model.cre_datetime},
                               @{@"title":@"完成时间",@"desc":self.model.up_datetime},
                                 @{@"title":@"订单编号",@"desc":self.model.order_id}];
}

-(WithdrawalRecordView2*)recordView{
    if (!_recordView) {
        _recordView = [[WithdrawalRecordView2 alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 120)];
        _recordView.backgroundColor = [UIColor whiteColor];
        [_recordView reloadTitle:self.model.account desc:@"提币金额" state:[MineHelper getStringByState:self.model.status]];
    }
    return _recordView;
}



- (UITableView*)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableHeaderView = self.recordView;
    }
    return _tableView;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 || indexPath.row == 1) {
        return 65;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark - Table view delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];
        cell.contentView.backgroundColor =  [UIColor whiteColor];
        PhoneNumView2* view = [[PhoneNumView2 alloc]initWithFrame:CGRectZero];
        view.line.hidden = YES;
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView).insets(UIEdgeInsetsMake(0, 15, 0, 15));
        }];
    }
    PhoneNumView2* view = [cell.contentView viewWithTag:101];
    NSDictionary* dict = [self.dataArray objectAtIndex:indexPath.row];
    [view reloadTitle:dict[@"title"] desc:dict[@"desc"]];
    view.descLbl.textColor = dict[@"textColor"]?:[UIColor lightGrayColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}

@end
