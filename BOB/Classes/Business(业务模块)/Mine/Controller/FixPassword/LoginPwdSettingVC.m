//
//  ModifyPhoneStep1VC.m
//  ScanPay
//
//  Created by mac on 2019/7/13.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "LoginPwdSettingVC.h"
#import "InvitationCodeView.h"
#import "TextCaptchaView.h"
#import "CipherView.h"
#import "NSObject+Mine.h"

static NSInteger padding = 15;
@interface LoginPwdSettingVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) InvitationCodeView* phoneView;

@property (nonatomic, strong) TextCaptchaView   *verifyView;

@property (nonatomic,strong) CipherView          *pwdView1;

@property (nonatomic,strong) CipherView          *pwdView2;

@property (nonatomic, strong) UIButton          *submitBtn;

@property (nonatomic, strong) NSArray          *dataArray;

@property (nonatomic, strong) UITableView      *tableView;

@property (nonatomic, strong) NSString      *captchat;

@property (nonatomic, strong) NSString      *pwd;

@property (nonatomic, strong) NSString      *repwd;

@end

@implementation LoginPwdSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self initData];
}

-(void)initUI{
    [self setNavBarTitle:@"修改/设置登录密码"];
    self.dataArray = @[
        @{@"title":Lang(@"当前手机号"),@"desc":UDetail.user.user_tel ?: @""},
        @{@"title":Lang(@"短信验证码"),@"placeholder":Lang(@"请输入验证码")},
        @{@"title":Lang(@"新密码"),@"placeholder":Lang(@"请输入新的登录密码")},
        @{@"title":Lang(@"确认密码"),@"placeholder":Lang(@"请再次输入你的登录密码")},
                    
                      ];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
  
    
}

-(void)initData{
//    [self.phoneView reloadTitle:@"当前手机号" placeHolder:@""];
//    self.phoneView.tf.text = @"150****2226";
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.titleLabel.font = [UIFont font15];
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitBtn.layer.masksToBounds = YES;
        _submitBtn.layer.cornerRadius = 4;
        [_submitBtn setTitle:Lang(@"完成") forState:UIControlStateNormal];
        _submitBtn.clipsToBounds =NO;
        [_submitBtn addTarget:self action:@selector(goTo:) forControlEvents:UIControlEventTouchUpInside];
        [_submitBtn setBackgroundColor:[UIColor themeColor]];
    }
    return _submitBtn;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor clearColor];
        AdjustTableBehavior(_tableView);
        _tableView.separatorColor = [UIColor moBackground];
        UIView* foot = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
        [foot addSubview:self.submitBtn];
        [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(foot.mas_centerX);
            make.height.equalTo(@(35));
            make.width.equalTo(@(SCREEN_WIDTH-30));
            make.bottom.equalTo(foot);
        }];
        _tableView.tableFooterView = foot;
        
    }
    
    return _tableView;
}

-(void)goTo:(id)sender{
    [self.view endEditing:YES];
    if ([StringUtil isEmpty:self.captchat]) {
        [NotifyHelper showMessageWithMakeText:@"请输入验证码"];
        return;
    }
    if ([StringUtil isEmpty:self.pwd]) {
       [NotifyHelper showMessageWithMakeText:@"请输入新的登录密码"];
       return;
   }
    if ([StringUtil isEmpty:self.repwd]) {
       [NotifyHelper showMessageWithMakeText:@"请再次输入登录密码"];
       return;
   }
    [self setup_set_password:@{@"password":self.pwd,@"re_password":self.repwd,@"type":@"1",@"code":self.captchat,@"mobile":UDetail.user.user_tel} completion:^(BOOL success, NSString *error) {
        if (success) {
            [NotifyHelper showMessageWithMakeText:@"修改成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* identifier = [NSString stringWithFormat:@"%ld %ld",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView* tmpView = [self getViewFromIndexPath:indexPath];
        [cell.contentView addSubview:tmpView];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.backgroundColor = [UIColor whiteColor];
        [tmpView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView).insets(UIEdgeInsetsMake(0, 15, 0, 15));
        }];
    }
    
    return cell;
}

-(UIView*)getViewFromIndexPath:(NSIndexPath *)indexPath{
    UIView* tmpView = nil;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            tmpView = self.phoneView;
        }else if(indexPath.row == 1){
            tmpView = self.verifyView;
        }else if(indexPath.row == 2){
            tmpView = self.pwdView1;
        }else if(indexPath.row == 3){
            tmpView = self.pwdView2;
        }
    }
    return tmpView;
}

-(InvitationCodeView*)phoneView{
    if (!_phoneView) {
        _phoneView = [InvitationCodeView new];
        NSDictionary* dict = self.dataArray[0];
        [_phoneView reloadTitle:dict[@"title"] placeHolder:@""];
        [_phoneView setTitleWidth:80];
        _phoneView.tf.text = dict[@"desc"];
        _phoneView.tf.enabled = NO;
        
    }
    return _phoneView;
}

-(TextCaptchaView*)verifyView{
    if (!_verifyView) {
        _verifyView = [TextCaptchaView new];
        NSDictionary* dict = self.dataArray[1];
        [_verifyView reloadTitle:dict[@"title"] placeHolder:dict[@"placeholder"]];
        [_verifyView setTitleWidth:80];
        [self.verifyView isHiddenLine:YES];
        @weakify(self)
        self.verifyView.getText = ^(id data) {
            @strongify(self)
            self.captchat = data;
        };
        self.verifyView.sendCodeBlock = ^(id data) {
          @strongify(self)
          [self setup_get_code:@{@"type":@"2"} completion:^(BOOL success, NSString *error) {
              if (success) {
                  [NotifyHelper showMessageWithMakeText:@"发送成功"];
              }
          }];
            
        };
    }
    return _verifyView;
}

-(CipherView* )pwdView1{
    if (!_pwdView1) {
        _pwdView1 = [CipherView new];
        [_pwdView1 setTitleWidth:60];
        [_pwdView1 reloadTitle:Lang(@"新密码") placeHolder:Lang(@"请输入新的登录密码")];
        @weakify(self)
        _pwdView1.getText = ^(id data) {
            @strongify(self)
            self.pwd = data;
        };
    }
    return _pwdView1;
}

-(CipherView* )pwdView2{
    if (!_pwdView2) {
        _pwdView2 = [CipherView new];
        [_pwdView2 setTitleWidth:60];
        [_pwdView2 reloadTitle:Lang(@"确认密码") placeHolder:Lang(@"请再次输入登录密码")];
        @weakify(self)
        _pwdView2.getText = ^(id data) {
            @strongify(self)
            self.repwd = data;
        };
    }
    return _pwdView2;
}

@end
