//
//  MesFavoriteImageInfoVC.m
//  BOB
//
//  Created by mac on 2020/7/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteImageInfoVC.h"
#import "YBImageBrowser.h"

@interface MesFavoriteImageInfoVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MyBaseLayout *headerView;

@property (nonatomic, strong) UIImageView *imgView;

@end

@implementation MesFavoriteImageInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

//点击预览图片
- (void)showPhoto {
    YBImageBrowseCellData *cell = [YBImageBrowseCellData new];
    NSString *url = StrF(@"%@/%@", UDetail.user.qiniu_domain, self.obj.attr1);
    cell.url = [NSURL URLWithString:url];
    cell.sourceObject = self.imgView;
    NSArray *photoArr = @[cell];
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = photoArr;
    browser.currentIndex = 0;
    [browser show];
}

- (void)createUI {
    [self setNavBarTitle:@"详情"];
    [self.view addSubview:self.tableView];
    [self layoutUI];
    [self initUI];
}

- (void)layoutUI {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)initUI {
    UIImageView *imgView = [UIImageView new];
    self.imgView = imgView;
    imgView.myLeft = 15;
    imgView.myRight = 15;
    imgView.myTop = 10;
    NSString *url = StrF(@"%@/%@", UDetail.user.qiniu_domain, self.obj.attr1);
    [imgView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
       if (image) {
           CGSize size = image.size;
           CGFloat height = ((SCREEN_WIDTH - 30) * size.height / size.width);
           size = CGSizeMake(SCREEN_WIDTH - 30, height);
           imgView.mySize = size;
           [self.headerView addSubview:imgView];
           [self.headerView layoutSubviews];
           self.tableView.tableHeaderView = self.headerView;
       }
    }];
    @weakify(self)
    [imgView addAction:^(UIView *view) {
        @strongify(self)
        [self showPhoto];
    }];
}

- (MyBaseLayout *)headerView {
    if (!_headerView) {
        _headerView = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _headerView.backgroundColor = [UIColor whiteColor];
        _headerView.myTop = 0;
        _headerView.myLeft = 0;
        _headerView.myRight = 0;
        _headerView.myHeight = MyLayoutSize.wrap;
        
        UILabel *lbl = [UILabel new];
        lbl.font = [UIFont font12];
        lbl.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
        lbl.text = StrF(@"来自%@ %@", self.obj.send_name, self.obj.format_send_time);
        [lbl sizeToFit];
        lbl.mySize = lbl.size;
        lbl.myCenterX = 0;
        lbl.myTop = 10;
        [_headerView addSubview:lbl];

    }
    return _headerView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

@end
