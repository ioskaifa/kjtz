//
//  BindPhoneVC.m
//  BOB
//
//  Created by mac on 2019/12/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BindPhoneVC.h"

@interface BindPhoneVC ()

@property (nonatomic,strong) UIImageView* phoneImg;

@property (nonatomic,strong) UILabel* phoneLbl;

@property (nonatomic,strong) UILabel* tipLbl;

@property (nonatomic,strong) UIButton* contactBtn;

@property (nonatomic,strong) UIButton* phoneBtn;

@end

@implementation BindPhoneVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refreshData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}

-(void)setupUI{
    [self setNavBarTitle:@"绑定手机号" color:[UIColor moBlack]];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.phoneImg];
    [self.view addSubview:self.phoneLbl];
    [self.view addSubview:self.tipLbl];
    [self.view addSubview:self.contactBtn];
    [self.view addSubview:self.phoneBtn];
    [self.phoneImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(169));
        make.height.equalTo(@(162));
        make.top.equalTo(@(44));
        make.centerX.equalTo(@(0));
    }];
    [self.phoneLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneImg.mas_bottom).offset(26);
        make.left.right.equalTo(self.view);
    }];
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneLbl.mas_bottom).offset(26);
         make.left.equalTo(@(15));
               make.right.equalTo(@(-15));
    }];
    [self.contactBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipLbl.mas_bottom).offset(44);
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.height.equalTo(@(35));
    }];
    [self.phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contactBtn.mas_bottom).offset(15);
        make.left.right.equalTo(self.contactBtn);
        make.height.equalTo(@(35));
    }];
}

-(void)refreshData{
    _phoneLbl.text = [NSString stringWithFormat:@"绑定的手机号:%@",UDetail.user.user_tel];
}

-(UIImageView*)phoneImg{
    if (!_phoneImg) {
        _phoneImg = [UIImageView new];
        _phoneImg.image = [UIImage imageNamed:@"交易所_手机"];
    }
    return _phoneImg;
}

-(UILabel*)phoneLbl{
    if (!_phoneLbl) {
        _phoneLbl = [UILabel new];
        _phoneLbl.textColor = [UIColor moBlack];
        _phoneLbl.font = [UIFont font15];
        _phoneLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _phoneLbl;
}

-(UILabel*)tipLbl{
    if (!_tipLbl) {
        _tipLbl = [UILabel new];
        _tipLbl.text = @"已启用手机通讯录匹配，点击下方按钮可查看手机通讯录中哪些朋友注册了币火账号。";
        _tipLbl.numberOfLines = 2;
        _tipLbl.textColor = [UIColor grayColor];
        _tipLbl.font = [UIFont font15];
        _tipLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _tipLbl;
}

-(UIButton*)contactBtn{
    if (!_contactBtn) {
        _contactBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _contactBtn.backgroundColor = [UIColor moBlueColor];
        [_contactBtn setTitle:@"查看手机通讯录" forState:UIControlStateNormal];
        _contactBtn.titleLabel.font = [UIFont font15];
        _contactBtn.layer.cornerRadius = 4;
    }
    return _contactBtn;
}

-(UIButton*)phoneBtn{
    if (!_phoneBtn) {
        _phoneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _phoneBtn.backgroundColor = [UIColor colorWithHexString:@"#DCDCDCFF"];
        [_phoneBtn setTitle:@"更换手机号" forState:UIControlStateNormal];
        _phoneBtn.titleLabel.font = [UIFont font15];
        _phoneBtn.layer.cornerRadius = 4;
        [_phoneBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_phoneBtn addAction:^(UIButton *btn) {
            [MXRouter openURL:@"lcwl://ChangePhoneVC"];
        }];
    }
    return _phoneBtn;
}

@end
