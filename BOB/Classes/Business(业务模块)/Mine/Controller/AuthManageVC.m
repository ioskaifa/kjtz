//
//  AuthManageVC.m
//  BOB
//
//  Created by mac on 2020/8/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AuthManageVC.h"
#import "AuthManageListObj.h"
#import "YBImageBrowser.h"

@interface AuthManageVC ()

@property (nonatomic, strong) AuthManageListObj *obj;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataMArr;

@property (nonatomic, strong) NSMutableArray *imgViewMArr;

@end

@implementation AuthManageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)navBarRightBtnAction:(id)sender {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot, @"api/user/info/adminSetUserStatus");
    NSDictionary *param = @{@"user_id":self.obj.ID?:@"",
                            @"is_valid":self.obj.is_valid?@"1":@"0",
                            @"is_realname":self.obj.is_realname?@"1":@"0",
                            @"is_freeze":self.obj.is_freeze?@"1":@"0",
                            @"is_auth":self.obj.is_auth?@"1":@"0",};
    [self request:url
            param:param
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)fetchData {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot, @"api/user/info/getAllChatUserDetail");
    [self request:url
            param:@{@"user_id":self.user_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            self.obj = [AuthManageListObj modelParseWithDict:object[@"data"][@"chatUserDetail"]];
            [self initHeaderView];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

//点击预览图片
- (void)showPhoto:(NSInteger)clickIndex {
    NSMutableArray *photoMArr = [NSMutableArray array];
    for (int i = 0 ; i < self.dataMArr.count; i++) {
        YBImageBrowseCellData *cell = [YBImageBrowseCellData new];
        cell.url = [NSURL URLWithString:self.dataMArr[i]];
        cell.sourceObject = self.imgViewMArr[i];
        [photoMArr addObject:cell];
    }
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = photoMArr;
    browser.currentIndex = clickIndex;
    [browser show];
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"授权管理"];
    [self setNavBarRightBtnWithTitle:@"提交" andImageName:nil];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)initHeaderView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    
    [layout addSubview:[UIView gapLine]];
    ///头像
    MyLinearLayout *lineLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    lineLayout.myHeight = 64;
    lineLayout.myLeft = 0;
    lineLayout.myRight = 0;
    UILabel *lbl = [self.class factoryKeyLbl:@"头像"];
    [lineLayout addSubview:lbl];
    UIView *view = [UIView new];
    view.myHeight = 1;
    view.weight = 1;
    [lineLayout addSubview:view];
    UIImageView *imgView = [UIImageView new];
    [imgView sd_setImageWithURL:[NSURL URLWithString:self.obj.head_photo] placeholderImage:[UIImage defaultAvatar]];
    imgView.size = CGSizeMake(54, 54);
    [imgView setViewCornerRadius:imgView.height/2.0];
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myRight = 15;
    [lineLayout addSubview:imgView];
    [layout addSubview:lineLayout];
    [layout addSubview:[UIView line]];
    
    [layout addSubview:[self.class factoryKey:@"昵称" value:self.obj.nick_name]];
    [layout addSubview:[UIView line]];
    
    [layout addSubview:[self.class factoryKey:@"手机号" value:self.obj.user_tel]];
    [layout addSubview:[UIView line]];
    
    [layout addSubview:[self.class factoryKey:@"地址" value:self.obj.area]];
    [layout addSubview:[UIView line]];
    
    [layout addSubview:[self.class factoryKey:@"创建日期" value:self.obj.cre_date]];
    [layout addSubview:[UIView line]];
    [layout addSubview:[UIView gapLine]];
        
    [layout addSubview:[self.class factoryKey:@"是否实名" switchBtn:self.obj.is_realname click:^(UIButton *btn) {
        self.obj.is_realname = btn.selected;
    }]];
    [layout addSubview:[UIView line]];
    [layout addSubview:[self.class factoryKey:@"是否授权" switchBtn:self.obj.is_valid click:^(UIButton *btn) {
        self.obj.is_valid = btn.selected;
    }]];
    [layout addSubview:[UIView line]];
    [layout addSubview:[self.class factoryKey:@"是否冻结" switchBtn:self.obj.is_freeze click:^(UIButton *btn) {
        self.obj.is_freeze = btn.selected;
    }]];
    [layout addSubview:[UIView line]];
    [layout addSubview:[UIView gapLine]];
    
    [layout addSubview:[self.class factoryKey:@"是否通过" switchBtn:self.obj.is_auth click:^(UIButton *btn) {
        self.obj.is_auth = btn.selected;
    }]];
    [layout addSubview:[UIView line]];
    
    lineLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    lineLayout.myTop = 0;
    lineLayout.myLeft = 0;
    lineLayout.myRight = 0;
    lineLayout.myHeight = MyLayoutSize.wrap;
    lbl = [self.class factoryKeyLbl:@"实名信息"];
    lbl.myTop = 10;
    lbl.myBottom = 10;
    [lineLayout addSubview:lbl];
    if (self.obj.realname_photo) {
        NSArray *imgUrlArr = [self.obj.realname_photo componentsSeparatedByString:@","];
        if (imgUrlArr.count > 0) {
            [self.dataMArr removeAllObjects];
            [self.imgViewMArr removeAllObjects];
            MyLinearLayout *imgLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            imgLayout.myTop = 0;
            imgLayout.myLeft = 15;
            imgLayout.myRight = 15;
            imgLayout.myBottom = 10;
            imgLayout.myHeight = MyLayoutSize.wrap;
            [lineLayout addSubview:imgLayout];
            CGFloat width = (SCREEN_WIDTH - 30 - 30) / 3.0;
            for (NSInteger i = 0; i< imgUrlArr.count; i++) {
                NSString *urlString = imgUrlArr[i];
                NSString *urlStr = StrF(@"%@/%@", UDetail.user.qiniu_domain, urlString);
                [self.dataMArr addObject:urlStr];
                NSURL *url = [NSURL URLWithString:urlStr];
                UIImageView *imgView = [UIImageView new];
                imgView.tag = i;
                [imgView sd_setImageWithURL:url];
                imgView.layer.borderColor = [UIColor moBackground].CGColor;
                imgView.layer.borderWidth = 1;
                [imgView setViewCornerRadius:3];
                imgView.contentMode = UIViewContentModeScaleAspectFit;
                imgView.size = CGSizeMake(width, 90);
                imgView.mySize = imgView.size;
                imgView.myRight = 10;
                @weakify(self)
                [imgView addAction:^(UIView *view) {
                    @strongify(self)
                    [self showPhoto:view.tag];
                }];
                [imgLayout addSubview:imgView];
                [self.imgViewMArr addObject:imgView];
            }
            [imgLayout layoutSubviews];
        }
    }
    [lineLayout layoutSubviews];
    
    [layout addSubview:lineLayout];
    
    [layout layoutSubviews];
    layout.mySize = layout.size;
    self.tableView.tableHeaderView = layout;
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor moBackground];
        AdjustTableBehavior(_tableView);
    }
    return _tableView;
}

+ (MyBaseLayout *)factoryKey:(NSString *)key value:(NSString *)value {
    MyLinearLayout *lineLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    lineLayout.myHeight = 50;
    lineLayout.myLeft = 0;
    lineLayout.myRight = 0;
    UILabel *lbl = [self factoryKeyLbl:key];
    [lineLayout addSubview:lbl];
    UIView *view = [UIView new];
    view.myHeight = 1;
    view.weight = 1;
    [lineLayout addSubview:view];
    lbl = [self factoryValueLbl:value];
    [lineLayout addSubview:lbl];
    return lineLayout;
}

+ (MyBaseLayout *)factoryKey:(NSString *)key switchBtn:(BOOL)on click:(ActionBlock)block{
    MyLinearLayout *lineLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    lineLayout.myHeight = 50;
    lineLayout.myLeft = 0;
    lineLayout.myRight = 0;
    UILabel *lbl = [self factoryKeyLbl:key];
    [lineLayout addSubview:lbl];
    UIView *view = [UIView new];
    view.myHeight = 1;
    view.weight = 1;
    [lineLayout addSubview:view];
    UIButton *btn = [self factorySwitchBtn];
    btn.selected = on;
    [btn addAction:^(UIButton *btn) {
        btn.selected = !btn.selected;
        block(btn);
    }];
    [lineLayout addSubview:btn];
    return lineLayout;
}

+ (UILabel *)factoryKeyLbl:(NSString *)txt {
    UILabel *object = [UILabel new];
    object.font = [UIFont font15];
    object.textColor = [UIColor blackColor];
    object.text = txt;
    [object sizeToFit];
    object.mySize = object.size;
    object.myLeft = 15;
    object.myCenterY = 0;
    return object;
}

+ (UILabel *)factoryValueLbl:(NSString *)txt {
    UILabel *object = [UILabel new];
    object.font = [UIFont font15];
    object.textColor = [UIColor blackColor];
    object.text = txt;
    [object sizeToFit];
    object.mySize = object.size;
    object.myRight = 15;
    object.myCenterY = 0;
    return object;
}

+ (UIButton *)factorySwitchBtn {
    UIButton *object = [UIButton new];
    [object setImage:[UIImage imageNamed:@"icon_switch_off"] forState:UIControlStateNormal];
    [object setImage:[UIImage imageNamed:@"icon_switch_on"] forState:UIControlStateSelected];
    [object addAction:^(UIButton *btn) {
        btn.selected = !btn.selected;
    }];
    [object sizeToFit];
    object.mySize = object.size;
    object.myRight = 15;
    object.myCenterY = 0;
    return object;
}

- (NSMutableArray *)dataMArr {
    if (!_dataMArr) {
        _dataMArr = [NSMutableArray array];
    }
    return _dataMArr;
}

- (NSMutableArray *)imgViewMArr {
    if (!_imgViewMArr) {
        _imgViewMArr = [NSMutableArray array];
    }
    return _imgViewMArr;
}

@end
