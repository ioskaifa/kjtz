//
//  AuthManageVC.h
//  BOB
//
//  Created by mac on 2020/8/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthManageVC : BaseViewController

@property (nonatomic, copy) NSString *user_id;

@end

NS_ASSUME_NONNULL_END
