//
//  MineHelper.h
//  JiuJiuEcoregion
//
//  Created by mac on 2019/6/6.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineHelper : NSObject

+ (void)getMyTeamList:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
+ (void)getMyReferInfo:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
+ (void)getUserBenefitInfo:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
+ (void)addUserFeedBack:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
+ (void)getUserDepositInfo:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
///套餐加单
+ (void)addDepositNum:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
///套餐复投
+ (void)redelivery:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
///解约操作参数查询
+ (void)getCancelParamLoad:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
///套餐解约
+ (void)cancel:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
//用户修改交易密码
+ (void)modifyPayPass:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion ;
//用户修改登录密码
+ (void)modifyLoginPass:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion;
///用户修改手机号第一步
+ (void)modifyPhone:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
///用户修改手机号第二步
+ (void)modifyPhoneNext:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion;
///绑定交易所 (RSA加密方式）
+ (void)addBindBourseAddress:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
///查询意见反馈列表 (MD5验签方式）
+ (void)getUserFeedBackList:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
///用户设置绑定账户（银行卡、支付宝、微信） (RSA加密）
+ (void)updateUserAccount:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion;

+ (void)getUserAccountList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion;

+ (void)getBalanceList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion;

+ (void)getBalanceTypeList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion;
//查询提币日志列表（交易所模块）
+ (void)getUserSendList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion;
//查询用户充币日志列表（交易所模块）
+ (void)getUserAcceptList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion;


+(NSString* )getStringByState:(NSString* )state;

@end

NS_ASSUME_NONNULL_END
