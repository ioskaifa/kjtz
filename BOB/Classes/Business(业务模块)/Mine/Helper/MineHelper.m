//
//  MineHelper.m
//  JiuJiuEcoregion
//
//  Created by mac on 2019/6/6.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "MineHelper.h"
#import "MXNet.h"
#import "YKCDetailModel.h"
#import "ChangeTypeModel.h"
#import "WithdrawalRecordModel.h"
#import "AccountModel.h"
#import "NSObject+Mine.h"

@implementation MineHelper

+ (void)getMyTeamList:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/team/getMyTeamList",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(false,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

+ (void)getMyReferInfo:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/team/getMyReferInfo",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(false,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

+ (void)getUserBenefitInfo:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/info/getUserBenefitInfo",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(false,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

+ (void)addUserFeedBack:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/feedBack/addUserFeedBack",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(false,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

+ (void)getUserDepositInfo:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/deposit/getUserDepositInfo",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(false,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

///套餐加单
+ (void)addDepositNum:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/deposit/addDepositNum",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(false,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

///套餐复投
+ (void)redelivery:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/deposit/redelivery",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(false,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

///解约操作参数查询
+ (void)getCancelParamLoad:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/deposit/getCancelParamLoad",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(false,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

///套餐解约
+ (void)cancel:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/deposit/cancel",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(false,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

///用户修改交易密码 /user/info/modifyPayPass
+ (void)modifyPayPass:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/setting/modifyPayPass",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,nil);
            }else{
                completion(NO,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}


+ (void)modifyLoginPass:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/setting/modifyLoginPass",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,nil);
            }else{
                completion(NO,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

///用户修改手机号第一步
+ (void)modifyPhone:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/setting/modifyTelFirst",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param).useMsg()
        .finish(^(id data){
            Block_Exec(completion,[[data valueForKey:@"success"] boolValue],data,nil);
        }).failure(^(id error){
            Block_Exec(completion,NO,nil,[error description]);
        })
        .execute();
    }];
}
///用户修改手机号第二步
+ (void)modifyPhoneNext:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/setting/modifyTelSecond",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,nil);
                [self modifyChatUserInfo:@{@"user_tel":param[@"sys_user_account"]?:@""} completion:nil];
            }else{
                completion(NO,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

///绑定交易所 (RSA加密方式）
+ (void)addBindBourseAddress:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/sendspn/addBindBourseAddress",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,nil,nil);
            }else{
                completion(NO,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

///绑定交易所 (RSA加密方式）
+ (void)getUserFeedBackList:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/feedBack/getUserFeedBackList",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(NO,nil,nil);
            }
            //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

+ (void)getBalanceList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"givinggift/asset/getBalanceList"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                NSDictionary* dataDict = tempDic[@"data"];
                if (dataDict) {
                    NSArray* dataArray = dataDict[@"balanceList"];
                    if (dataArray.count >0) {
                        NSMutableArray* data = [[NSMutableArray alloc]initWithCapacity:10];
                        [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            NSDictionary* tmpDict = (NSDictionary*)obj;
                            YKCDetailModel* model = [YKCDetailModel dictionayToModel:tmpDict];
                            [data addObject:model];
                        }];
                        
                        completion(data,nil);
                    }else{
                        completion(@[],nil);
                    }
                }
            }
        }).failure(^(id error){
            if (completion) {
                completion(nil,error);
            }
        })
        .execute();
    }];
}

+ (void)getBalanceTypeList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"givinggift/asset/getBalanceTypeList"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                if (tempDic[@"data"]) {
                    NSArray* dataArray = tempDic[@"data"][@"balanceTypeList"];
                    if (dataArray) {
                        if (dataArray.count >0) {
                            NSMutableArray* data = [[NSMutableArray alloc]initWithCapacity:10];
                            [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                NSDictionary* tmpDict = (NSDictionary*)obj;
                                ChangeTypeModel* model = [ChangeTypeModel dictionaryToModel:tmpDict];
                                [data addObject:model];
                            }];
                            
                            completion(data,nil);
                        }else{
                            completion(@[],nil);
                        }
                    }
                }
                
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
            
            if (completion) {
                completion(nil,error);
            }
        })
        .execute();
    }];
}

/*
 00—待处理
 02—处理中
 03—待审核
 05—冻结
 06—审核拒绝
 08—提币失败
 09—提币成功
 */
+(NSString* )getStringByState:(NSString* )state{
    NSString* desc = @"";
    if ([state isEqualToString:@"00"]) {
         desc = @"待处理";
    }else if ([state isEqualToString:@"02"]) {
         desc = @"处理中";
    }else if ([state isEqualToString:@"03"]) {
         desc = @"待审核";
    }else if ([state isEqualToString:@"05"]) {
         desc = @"冻结";
    }else if ([state isEqualToString:@"06"]) {
         desc = @"审核拒绝";
    }else if ([state isEqualToString:@"08"]) {
         desc = @"提币失败";
    }else if ([state isEqualToString:@"09"]) {
         desc = @"提币成功";
    }
    return desc;
}

+ (void)updateUserAccount:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/api/user/account/updateUserAccount",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.useEncrypt()
        .apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,nil);
            }else{
                completion(NO,nil);
            }
            [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil);
            
            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

+ (void)getUserAccountList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"api/user/account/getUserAccountList"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                NSDictionary* dataDict = tempDic[@"data"];
                if (dataDict) {
                    NSArray* dataArray = dataDict[@"userAccountList"];
                    if (dataArray.count >0) {
                        NSMutableArray* data = [[NSMutableArray alloc]initWithCapacity:10];
                        [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            NSDictionary* tmpDict = (NSDictionary*)obj;
                            AccountModel* model = [AccountModel modelParseWithDict:tmpDict];
                            [data addObject:model];
                        }];
                        
                        completion(data,nil);
                    }else{
                        completion(@[],nil);
                    }
                }
            }
        }).failure(^(id error){
            if (completion) {
                completion(nil,error);
            }
        })
        .execute();
    }];
}



+ (void)getUserSendList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"api/exchange/send/getUserSendList"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                if (tempDic[@"data"]) {
                    NSArray* dataArray = tempDic[@"data"][@"userSendList"];
                    if (dataArray) {
                        if (dataArray.count >0) {
                            NSMutableArray* data = [[NSMutableArray alloc]initWithCapacity:10];
                            [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                NSDictionary* tmpDict = (NSDictionary*)obj;
                                WithdrawalRecordModel* model = [WithdrawalRecordModel modelParseWithDict:tmpDict];
                                [data addObject:model];
                            }];
                            completion(data,nil);
                        }else{
                            completion(nil,nil);
                        }
                    }
                }
                
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
            
            if (completion) {
                completion(nil,error);
            }
        })
        .execute();
    }];
}




+ (void)getUserAcceptList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"api/exchange/accept/getUserAcceptList"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                if (tempDic[@"data"]) {
                    NSArray* dataArray = tempDic[@"data"][@"userAcceptList"];
                    if (dataArray) {
                        if (dataArray.count >0) {
                            NSMutableArray* data = [[NSMutableArray alloc]initWithCapacity:10];
                            [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                NSDictionary* tmpDict = (NSDictionary*)obj;
                                WithdrawalRecordModel* model = [WithdrawalRecordModel modelParseWithDict:tmpDict];
                                [data addObject:model];
                            }];
                            completion(data,nil);
                        }else{
                            completion(nil,nil);
                        }
                    }
                }
                
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
            
            if (completion) {
                completion(nil,error);
            }
        })
        .execute();
    }];
}
@end
