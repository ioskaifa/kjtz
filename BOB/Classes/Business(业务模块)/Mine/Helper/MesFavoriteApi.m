//
//  MesFavoriteApi.m
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MesFavoriteApi.h"

@implementation MesFavoriteApi

///查询服务
+ (void)getMesFavoriteList:(NSString *)last_id
                  complete:(MXHttpRequestResultObjectCallBack)complete {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot2, @"api/user/mesfavorite/getMesFavoriteList");
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{@"last_id":last_id?:@"0"})
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                NSDictionary* dataDict = dict[@"data"];
                complete(YES,dataDict,nil);
            }else{
                complete(NO,nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            complete(NO,nil,nil);
        })
        .execute();
    }];
}

///增加服务
+ (void)addMesFavorite:(NSDictionary *)param
              complete:(MXHttpRequestResultObjectCallBack)complete {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot2, @"api/user/mesfavorite/addMesFavorite");
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                complete(YES,nil,nil);
            }else{
                complete(NO,nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            complete(NO,nil,nil);
        })
        .execute();
    }];
}

///删除服务
+ (void)delMesFavorite:(NSString *)id_list
              complete:(MXHttpRequestResultObjectCallBack)complete {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot2, @"api/user/mesfavorite/delMesFavorite");
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{@"id_list":id_list?:@""})
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                complete(YES,nil,nil);
            }else{
                complete(NO,nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            complete(NO,nil,nil);
        })
        .execute();
    }];
}

@end
