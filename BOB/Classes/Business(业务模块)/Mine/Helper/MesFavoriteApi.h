//
//  MesFavoriteApi.h
//  BOB
//
//  Created by mac on 2020/7/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MesFavoriteApi : NSObject

///查询收藏列表
+ (void)getMesFavoriteList:(NSString *)last_id
                  complete:(MXHttpRequestResultObjectCallBack)complete;

///增加收藏
+ (void)addMesFavorite:(NSDictionary *)param
              complete:(MXHttpRequestResultObjectCallBack)complete;

///删除收藏
+ (void)delMesFavorite:(NSString *)id_list
              complete:(MXHttpRequestResultObjectCallBack)complete;

@end

NS_ASSUME_NONNULL_END
