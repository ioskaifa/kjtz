//
//  FreeSliderView.h
//  BOB
//
//  Created by colin on 2020/11/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FreeSliderView;

NS_ASSUME_NONNULL_BEGIN

@protocol FreeSliderViewDelegate <NSObject>

@optional
- (void)freeSliderView:(FreeSliderView *)sliderView didSelectItemAtIndex:(NSInteger)index;

@end

@interface FreeSliderView : UIView

@property (nonatomic, weak) id<FreeSliderViewDelegate> delegate;

@property (nonatomic, strong) SPButton *searchBtn;

+ (CGFloat)viewHeight;

- (instancetype)initWithData:(NSArray *)dataArr;

- (void)reloadData;

@end

NS_ASSUME_NONNULL_END
