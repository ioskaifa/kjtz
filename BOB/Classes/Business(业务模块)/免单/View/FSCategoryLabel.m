//
//  FSCategoryLabel.m
//  BOB
//
//  Created by colin on 2021/1/17.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "FSCategoryLabel.h"

@implementation FSCategoryLabel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.font = [UIFont font13];
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    [self setViewCornerRadius:25/2.0];
    self.textAlignment = NSTextAlignmentCenter;
    [self normalStyle];
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    if (selected) {
        [self selectedStyle];
    } else {
        [self normalStyle];
    }
}

- (void)normalStyle {
    [self setBackgroundColor:[UIColor colorWithHexString:@"#00A99B"]];
    self.textColor = [UIColor whiteColor];
}

- (void)selectedStyle {
    [self setBackgroundColor:[UIColor whiteColor]];
    self.textColor = [UIColor colorWithHexString:@"#00A99B"];
}

@end
