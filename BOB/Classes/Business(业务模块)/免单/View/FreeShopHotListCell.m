//
//  FreeShopHotListCell.m
//  BOB
//
//  Created by Colin on 2020/10/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeShopHotListCell.h"

@interface FreeShopHotListCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *priceLbl;

@end

@implementation FreeShopHotListCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

+ (CGSize)itemSize {
    return CGSizeMake(250, 300);
}

- (void)configureView:(GoodsListObj *)obj {
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.photo]];
    self.titleLbl.text = obj.title;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake([self.class itemSize].width - 40, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    self.priceLbl.text = StrF(@"￥%0.2f", obj.price);
    [self.priceLbl autoMyLayoutSize];
}

- (void)createUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    baseLayout.backgroundColor = [UIColor whiteColor];
    [baseLayout setViewCornerRadius:5];
    [self.contentView addSubview:baseLayout];
    
    [baseLayout addSubview:self.imgView];
    [baseLayout addSubview:self.titleLbl];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 20;
    layout.myRight = 20;
    layout.myTop = 10;
    layout.myBottom = 15;
    layout.myHeight = MyLayoutSize.wrap;
    [baseLayout addSubview:layout];
    [layout addSubview:self.priceLbl];
    [layout addSubview:[UIView placeholderHorzView]];
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"免单加入购物车"];
    imgView.myCenterY = 0;
    imgView.myRight = 0;
    [layout addSubview:imgView];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        _imgView.myLeft = 0;
        _imgView.myRight = 0;
        _imgView.myTop = 0;
        _imgView.weight = 1;
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.font = [UIFont lightFont15];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _titleLbl.numberOfLines = 2;
        _titleLbl.myLeft = 20;
        _titleLbl.myRight = 20;
        _titleLbl.myTop = 10;
    }
    return _titleLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.font = [UIFont boldFont18];
        _priceLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _priceLbl.myBottom = 5;
    }
    return _priceLbl;
}

@end
