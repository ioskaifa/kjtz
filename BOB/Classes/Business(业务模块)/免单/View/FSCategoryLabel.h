//
//  FSCategoryLabel.h
//  BOB
//
//  Created by colin on 2021/1/17.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FSCategoryLabel : UILabel

@property (nonatomic, assign) BOOL selected;

@end

NS_ASSUME_NONNULL_END
