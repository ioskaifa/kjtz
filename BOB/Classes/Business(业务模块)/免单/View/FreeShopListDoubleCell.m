//
//  FreeShopListDoubleCell.m
//  BOB
//
//  Created by colin on 2021/1/17.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "FreeShopListDoubleCell.h"
#import "GoodListView.h"

@interface FreeShopListDoubleCell ()

@property (nonatomic, strong) GoodListView *leftView;

@property (nonatomic, strong) GoodListView *rightView;

@property (nonatomic, strong) NSArray *dataArr;

@end

@implementation FreeShopListDoubleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 225;
}

- (void)configureView:(NSArray *)arr {
    self.rightView.visibility = MyVisibility_Visible;
    if (![arr isKindOfClass:NSArray.class]) {
        return;
    }
    self.dataArr = arr;
    if (arr.count == 2) {
        [self.leftView configureView:arr.firstObject];
        [self.rightView configureView:arr.lastObject];
    }
    if (arr.count == 1) {
        [self.leftView configureView:arr.firstObject];
        self.rightView.visibility = MyVisibility_Invisible;
    }
}

- (void)contentViewClick:(NSInteger)tag {
    if (tag >= self.dataArr.count) {
        return;
    }
    [self routerEventWithName:@"FreeShopListDoubleCellDidSelect" userInfo:@{@"obj":self.dataArr[tag]}];
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor colorWithHexString:@"#00A99B"];
    MyLinearLayout *baseLayout = [self.contentView addMyLinearLayout:MyOrientation_Horz];
    baseLayout.myLeft = 15;
    baseLayout.myRight = 15;
    baseLayout.myBottom = 10;
    baseLayout.gravity = MyGravity_Horz_Fill;
    
    [baseLayout addSubview:self.leftView];
    [baseLayout addSubview:self.rightView];
}


#pragma mark - Init
- (GoodListView *)leftView {
    if (!_leftView) {
        _leftView = [GoodListView new];
        [_leftView setViewCornerRadius:5];
        _leftView.height = [self.class viewHeight] - 10;
        _leftView.myHeight = _leftView.height;
        _leftView.myRight = 10;
        _leftView.tag = 0;
        @weakify(self)
        [_leftView addAction:^(UIView *view) {
         @strongify(self)
            [self contentViewClick:view.tag];
        }];
    }
    return _leftView;
}

- (GoodListView *)rightView {
    if (!_rightView) {
        _rightView = [GoodListView new];
        [_rightView setViewCornerRadius:5];
        _rightView.height = [self.class viewHeight] - 10;
        _rightView.myHeight = _rightView.height;
        _rightView.tag = 1;
        @weakify(self)
        [_rightView addAction:^(UIView *view) {
         @strongify(self)
            [self contentViewClick:view.tag];
        }];
    }
    return _rightView;
}

@end
