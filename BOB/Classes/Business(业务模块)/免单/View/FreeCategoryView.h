//
//  FreeCategoryView.h
//  BOB
//
//  Created by colin on 2020/11/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FreeCategoryView;

NS_ASSUME_NONNULL_BEGIN
@protocol FreeCategoryViewDelegate <NSObject>

@optional
- (void)freeCategoryView:(FreeCategoryView *)sliderView didSelectItemAtIndex:(NSInteger)index;

@end

@interface FreeCategoryView : UIView

@property (nonatomic, weak) id<FreeCategoryViewDelegate> delegate;

- (CGFloat)viewHeight;

- (instancetype)initWithDataArr:(NSArray *)arr;

- (void)reloadData;

@end

NS_ASSUME_NONNULL_END
