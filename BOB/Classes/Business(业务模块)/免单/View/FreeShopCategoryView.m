//
//  FreeShopCategoryView.m
//  BOB
//
//  Created by colin on 2021/1/17.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "FreeShopCategoryView.h"

@interface FreeShopCategoryCell : UICollectionViewCell

@property (nonatomic, strong) UILabel *valueLbl;

@property (nonatomic, strong) CategoryObj *obj;

@end

@implementation FreeShopCategoryCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)configureView:(CategoryObj *)obj {
    if (![obj isKindOfClass:CategoryObj.class]) {
        return;
    }
    self.obj = obj;
    self.valueLbl.text = obj.category_name;
//    self.valueLbl.size = obj.itemSize;
    self.valueLbl.mySize = self.valueLbl.size;
    
    if (self.obj.isSelected) {
        
    } else {
        
    }
}

- (void)configureValueNormal {
    [self.valueLbl setViewCornerRadius:2];
    self.valueLbl.backgroundColor = [UIColor colorWithHexString:@"#F5F5FA"];
    self.valueLbl.textColor = [UIColor colorWithHexString:@"#5C5C66"];
    self.valueLbl.layer.borderColor = [UIColor colorWithHexString:@"#F5F5FA"].CGColor;
    self.valueLbl.layer.borderWidth = 1;
}

- (void)configureValueSelected {
    [self.valueLbl setViewCornerRadius:2];
    self.valueLbl.layer.borderColor = [UIColor blackColor].CGColor;
    self.valueLbl.layer.borderWidth = 1;
    self.valueLbl.backgroundColor = [UIColor whiteColor];
    self.valueLbl.textColor = [UIColor blackColor];
}

- (void)configureValueDisEnable {
    self.valueLbl.textColor = [UIColor colorWithHexString:@"#C2C2CC"];
    self.valueLbl.backgroundColor = [UIColor whiteColor];
}

- (void)createUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.valueLbl];
}

- (UILabel *)valueLbl {
    if (!_valueLbl) {
        _valueLbl = [UILabel new];
        _valueLbl.font = [UIFont font13];
        _valueLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _valueLbl;
}

@end

@interface FreeShopCategoryView ()

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) NSArray *dataSourceArr;

@property (nonatomic, strong) UICollectionView *colView;

@end

@implementation FreeShopCategoryView



@end
