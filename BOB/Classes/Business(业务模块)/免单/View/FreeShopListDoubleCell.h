//
//  FreeShopListDoubleCell.h
//  BOB
//
//  Created by colin on 2021/1/17.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface FreeShopListDoubleCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(NSArray *)arr;

@end

NS_ASSUME_NONNULL_END
