//
//  MyFreeShopNameListCell.m
//  BOB
//
//  Created by colin on 2020/11/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyFreeShopNameListCell.h"

@interface MyFreeShopNameListCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) UILabel *saleLbl;

@end

@implementation MyFreeShopNameListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 150;
}

- (void)configureView:(BuyGoodsListObj *)obj {
    if (![obj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.photo]];
    self.titleLbl.text = obj.title;
    CGFloat width = SCREEN_WIDTH - 24 - self.imgView.width - 20;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    self.priceLbl.text = StrF(@"￥%0.2f", obj.price);
    [self.priceLbl autoMyLayoutSize];
    
    self.saleLbl.text = StrF(@"x%zd", obj.number);
    [self.saleLbl autoMyLayoutSize];
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    baseLayout.myTop = 10;
    baseLayout.myLeft = 15;
    baseLayout.myRight = 15;
    baseLayout.myBottom = 0;
    baseLayout.backgroundColor = [UIColor whiteColor];
    [baseLayout setViewCornerRadius:5];
    [self.contentView addSubview:baseLayout];
    
    [baseLayout addSubview:self.imgView];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myTop = 10;
    layout.myBottom = 10;
    layout.weight = 1;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.titleLbl];
    [layout addSubview:[UIView placeholderVertView]];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myBottom = 5;
    subLayout.myHeight = MyLayoutSize.wrap;
    [layout addSubview:subLayout];
    
    [subLayout addSubview:self.priceLbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    [subLayout addSubview:self.saleLbl];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        _imgView.myLeft = 10;
        _imgView.myCenterY = 0;
        _imgView.size = CGSizeMake(120, 120);
        _imgView.mySize = _imgView.size;
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.font = [UIFont lightFont15];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _titleLbl.numberOfLines = 2;
        _titleLbl.myLeft = 0;
        _titleLbl.myRight = 0;
        _titleLbl.myTop = 0;
    }
    return _titleLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.font = [UIFont boldFont18];
        _priceLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _priceLbl.myBottom = 5;
    }
    return _priceLbl;
}

- (UILabel *)saleLbl {
    if (!_saleLbl) {
        _saleLbl = [UILabel new];
        _saleLbl.font = [UIFont font12];
        _saleLbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
        _saleLbl.myRight = 0;
    }
    return _saleLbl;
}

@end
