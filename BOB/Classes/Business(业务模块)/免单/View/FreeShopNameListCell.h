//
//  FreeShopNameListCell.h
//  BOB
//
//  Created by colin on 2020/11/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FreeChargeOrderListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface FreeShopNameListCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(FreeChargeOrderListObj *)obj;

@end

NS_ASSUME_NONNULL_END
