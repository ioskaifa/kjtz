//
//  FreeShopHotListCell.h
//  BOB
//
//  Created by Colin on 2020/10/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface FreeShopHotListCell : UICollectionViewCell

+ (CGSize)itemSize;

- (void)configureView:(GoodsListObj *)obj;

@end

NS_ASSUME_NONNULL_END
