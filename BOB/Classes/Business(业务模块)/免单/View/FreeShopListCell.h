//
//  FreeShopListCell.h
//  BOB
//
//  Created by colin on 2020/11/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface FreeShopListCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(GoodsListObj *)obj;

@end

NS_ASSUME_NONNULL_END
