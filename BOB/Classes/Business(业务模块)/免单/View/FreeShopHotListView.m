//
//  FreeShopHotListView.m
//  BOB
//
//  Created by Colin on 2020/10/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeShopHotListView.h"
#import "FreeShopHotListCell.h"
#import "GLCoverFlowLayout.h"

@interface FreeShopHotListView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@end

@implementation FreeShopHotListView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 300;
}

- (void)configureView:(NSArray *)dataArr {
    if (![dataArr isKindOfClass:NSArray.class]) {
        return;
    }
    self.dataSourceArr = dataArr;
    [self.colView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = FreeShopHotListCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item >= self.dataSourceArr.count) {
        return;
    }
    if (![cell isKindOfClass:FreeShopHotListCell.class]) {
        return;
    }
    FreeShopHotListCell *listCell = (FreeShopHotListCell *)cell;
    [listCell configureView:self.dataSourceArr[indexPath.item]];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    GoodsListObj *obj = self.dataSourceArr[indexPath.row];
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    MXRoute(@"GoodInfoVC", (@{@"goods_id":obj.ID, @"isFree":@(1)}))
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    [layout addSubview:self.colView];
}

- (UICollectionView *)colView {
    if (!_colView) {
        GLCoverFlowLayout *layout = [GLCoverFlowLayout new];
        layout.itemSize = [FreeShopHotListCell itemSize];
        layout.minimumInteritemSpacing = 20;
        layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _colView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _colView.showsVerticalScrollIndicator = NO;
        _colView.showsHorizontalScrollIndicator = NO;
        _colView.backgroundColor = [UIColor colorWithHexString:@"#00A99B"];
        _colView.delegate = self;
        _colView.dataSource = self;
        Class cls = FreeShopHotListCell.class;
        [_colView registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
        _colView.weight = 1;
        _colView.myTop = 0;
        _colView.myLeft = 0;
        _colView.myRight = 0;
    }
    return _colView;
}


@end
