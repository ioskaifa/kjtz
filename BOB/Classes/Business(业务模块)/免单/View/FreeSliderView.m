//
//  FreeSliderView.m
//  BOB
//
//  Created by colin on 2020/11/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeSliderView.h"
#import "CategoryObj.h"
#import "FSCategoryLabel.h"

@interface FreeSliderView()

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) NSMutableArray *btnMArr;

@property (nonatomic, strong) FSCategoryLabel *selectedBtn;

@end

@implementation FreeSliderView

- (instancetype)initWithData:(NSArray *)dataArr {
    if (self = [super init]) {
        self.dataArr = dataArr;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 55;
}

- (void)reloadData {
    for (FSCategoryLabel *btn in self.btnMArr) {
        if (![btn isKindOfClass:FSCategoryLabel.class]) {
            continue;
        }
        if (btn.tag >= self.dataArr.count) {
            continue;
        }
        CategoryObj *obj = self.dataArr[btn.tag];
        if (![obj isKindOfClass:CategoryObj.class]) {
            continue;
        }        
        btn.selected = obj.isSelected;
        if (btn.selected) {
            self.selectedBtn = btn;
        }
//        [self configureBtnState:btn];
    }
}

#pragma mark - IBAction
- (void)btnClick:(FSCategoryLabel *)sender {
    if (self.selectedBtn == sender) {
        return;
    } else {
        self.selectedBtn.selected = NO;
//        [self configureBtnState:self.selectedBtn];
    }
    
    sender.selected = YES;
    self.selectedBtn = sender;
//    [self configureBtnState:self.selectedBtn];
//    [self updateBottomLinePosition:self.selectedBtn];
    if (self.delegate && [self.delegate respondsToSelector:@selector(freeSliderView:didSelectItemAtIndex:)]) {
        [self.delegate freeSliderView:self didSelectItemAtIndex:sender.tag];
    }
}

- (void)updateBottomLinePosition:(UIButton *)btn {
    CGRect btnFrame = [btn.superview.superview convertRect:btn.frame toView:self.lineView.superview];
    if (CGSizeEqualToSize(btnFrame.size, CGSizeZero)) {
        return;
    }
    CGFloat left = CGRectGetMinX(btnFrame) + (CGRectGetWidth(btnFrame) - self.lineView.width)/2.0;
    if (left == self.lineView.x) {
        return;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.lineView.x = left;
    }];
}

- (void)configureBtnState:(UIButton *)sender {
    if (sender.selected) {
        [self configureBtnSelected:sender];
    } else {
        [self configureBtnNormal:sender];
    }
}

- (void)configureBtnNormal:(UIButton *)sender {
    sender.titleLabel.font = [UIFont font14];
}

- (void)configureBtnSelected:(UIButton *)sender {
    sender.titleLabel.font = [UIFont boldFont18];
}

- (void)createUI {
    self.backgroundColor = [UIColor colorWithHexString:@"#00A99B"];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    for (NSInteger i = 0; i < self.dataArr.count; i++) {
        CategoryObj *obj = self.dataArr[i];
        NSString *title = obj.category_name;
        FSCategoryLabel *btn = [FSCategoryLabel new];
        btn.text = title;
        [btn sizeToFit];
        btn.size = CGSizeMake(btn.width + 20, 24);
        btn.mySize = btn.size;
        btn.tag = i;
        btn.myCenterY = 0;
        btn.myLeft = 15;
        if (obj.isSelected) {
            btn.selected = YES;
            self.selectedBtn = btn;
        } else {
            btn.selected = NO;
        }
//        [self configureBtnState:btn];
        [self.btnMArr addObject:btn];
        @weakify(self)
        @weakify(btn)
        [btn addAction:^(UIView *view) {
            @strongify(self)
            @strongify(btn)
            [self btnClick:btn];
        }];
        [layout addSubview:btn];
    }
    [layout addSubview:[UIView placeholderHorzView]];
    [self addSubview:self.searchBtn];
    [self addSubview:self.lineView];
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = [UIColor whiteColor];
        _lineView.size = CGSizeMake(20, 3);
        UIView *firstView = self.btnMArr.firstObject;
        _lineView.x = (firstView.width - _lineView.width) / 2.0 + 15;
        _lineView.y = [self.class viewHeight] - _lineView.height - 5;
        _lineView.hidden = YES;
    }
    return _lineView;
}

- (NSMutableArray *)btnMArr {
    if (!_btnMArr) {
        _btnMArr = [NSMutableArray array];
    }
    return _btnMArr;
}

- (SPButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = ({
            SPButton *object = [SPButton new];
            [object setBackgroundColor:[UIColor colorWithHexString:@"#00A99B"]];
            object.imagePosition = SPButtonImagePositionLeft;
            object.imageTitleSpace = 0;
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font12];
            [object setImage:[UIImage imageNamed:@"免单分类"] forState:UIControlStateNormal];
            [object sizeToFit];
            object.size = CGSizeMake(object.width + 10, object.height + 10);
            object.x = SCREEN_WIDTH - object.width;
            object.y = ([self.class viewHeight] - object.height)/2.0;
            object;
        });
    }
    return _searchBtn;
}

@end
