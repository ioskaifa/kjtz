//
//  FreeShopNameListCell.m
//  BOB
//
//  Created by colin on 2020/11/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeShopNameListCell.h"

@interface FreeShopNameListCell ()

@property (nonatomic, strong) UILabel *nick_nameLbl;

@property (nonatomic, strong) UILabel *goods_nameLbl;

@end

@implementation FreeShopNameListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 40;
}

- (void)configureView:(FreeChargeOrderListObj *)obj {
    if (![obj isKindOfClass:FreeChargeOrderListObj.class]) {
        return;
    }
    self.nick_nameLbl.text = obj.nick_name;
    self.goods_nameLbl.text = obj.goods_name;
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    CGFloat width = SCREEN_WIDTH - 60 - 20;
    [layout addSubview:self.nick_nameLbl];
    self.goods_nameLbl.myWidth = width/2;
    [layout addSubview:self.goods_nameLbl];
}

- (UILabel *)nick_nameLbl {
    if (!_nick_nameLbl) {
        _nick_nameLbl = [UILabel new];
        _nick_nameLbl.font = [UIFont font12];
        _nick_nameLbl.textColor = [UIColor colorWithHexString:@"#80808C"];
        _nick_nameLbl.myCenterY = 0;
        _nick_nameLbl.myLeft = 20;
        _nick_nameLbl.weight = 1;
        _nick_nameLbl.myHeight = 20;
        _nick_nameLbl.myRight = 10;
    }
    return _nick_nameLbl;
}

- (UILabel *)goods_nameLbl {
    if (!_goods_nameLbl) {
        _goods_nameLbl = [UILabel new];
        _goods_nameLbl.font = [UIFont font12];
        _goods_nameLbl.textColor = [UIColor colorWithHexString:@"#80808C"];
        _goods_nameLbl.myCenterY = 0;
        _goods_nameLbl.myHeight = 20;
        _goods_nameLbl.myRight = 20;
    }
    return _goods_nameLbl;
}

@end
