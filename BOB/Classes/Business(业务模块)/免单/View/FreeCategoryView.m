//
//  FreeCategoryView.m
//  BOB
//
//  Created by colin on 2020/11/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeCategoryView.h"
#import "CategoryObj.h"
#import "FSCategoryLabel.h"

@interface FreeCategoryView ()

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) NSMutableArray *btnMArr;

@property (nonatomic, strong) MyBaseLayout *baseLayout;

@property (nonatomic, strong) FSCategoryLabel *selectedBtn;

@end

@implementation FreeCategoryView

- (instancetype)initWithDataArr:(NSArray *)arr {
    if (self = [super init]) {
        self.dataArr = arr;
        [self createUI];
    }
    return self;
}

- (CGFloat)viewHeight {
    [self.baseLayout layoutIfNeeded];
    return self.baseLayout.height;
}

- (void)reloadData {
    for (FSCategoryLabel *btn in self.btnMArr) {
        if (![btn isKindOfClass:FSCategoryLabel.class]) {
            continue;
        }
        if (btn.tag >= self.dataArr.count) {
            continue;
        }
        CategoryObj *obj = self.dataArr[btn.tag];
        if (![obj isKindOfClass:CategoryObj.class]) {
            continue;
        }
        btn.selected = obj.isSelected;
        if (btn.selected) {
            self.selectedBtn = btn;
        }
    }
}

#pragma mark - IBAction
- (void)btnClick:(FSCategoryLabel *)sender {
    if (self.selectedBtn == sender) {
        return;
    } else {
        self.selectedBtn.selected = NO;
    }
    
    sender.selected = YES;
    self.selectedBtn = sender;
    if (self.delegate && [self.delegate respondsToSelector:@selector(freeCategoryView:didSelectItemAtIndex:)]) {
        [self.delegate freeCategoryView:self didSelectItemAtIndex:sender.tag];
    }
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    self.baseLayout = baseLayout;
    baseLayout.backgroundColor = [UIColor colorWithHexString:@"#00A99B"];
    baseLayout.myHeight = MyLayoutSize.wrap;
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myHeight = 40;
    layout.myLeft = 0;
    layout.myRight = 0;
//            layout.gravity = MyGravity_Vert_Fill;
    [baseLayout addSubview:layout];
    CGFloat width = SCREEN_WIDTH;
    for (NSInteger i = 0; i < self.dataArr.count; i++) {
        CategoryObj *obj = self.dataArr[i];
        FSCategoryLabel *btn = [FSCategoryLabel new];
        btn.text = obj.category_name;
        [btn sizeToFit];
        btn.size = CGSizeMake(btn.width + 20, 24);
        btn.mySize = btn.size;
        btn.myCenterY = 0;
        btn.myLeft = 15;
        if ((width - (btn.width + 15)) <= 15) {
            width = SCREEN_WIDTH;
            layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            layout.myHeight = 40;
            layout.myLeft = 0;
            layout.myRight = 0;
            [baseLayout addSubview:layout];
        }
        width -= (btn.width + 15);
        btn.tag = i;
        btn.selected = obj.isSelected;
        if (obj.isSelected) {
            self.selectedBtn = btn;
        }
        @weakify(self)
        @weakify(btn)
        [btn addAction:^(UIView *view) {
            @strongify(self)
            @strongify(btn)
            [self btnClick:btn];
        }];
        [layout addSubview:btn];
        [self.btnMArr addObject:btn];
    }
    
}

- (NSMutableArray *)btnMArr {
    if (!_btnMArr) {
        _btnMArr = [NSMutableArray arrayWithCapacity:self.dataArr.count];
    }
    return _btnMArr;
}

@end
