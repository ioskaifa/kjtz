//
//  FreeShopSearchVC.m
//  BOB
//
//  Created by colin on 2020/11/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeShopSearchVC.h"
#import "HotKeywordListObj.h"

static NSString * const historyKey = @"FreeShopSearchVChistoryKey";

@interface FreeShopSearchVC ()<UICollectionViewDelegate, UICollectionViewDataSource, UITextViewDelegate>

@property (nonatomic, strong) UIView *navTitleView;

@property (nonatomic, strong) UITextField *searchTF;

@property (nonatomic, strong) MyBaseLayout *baseLayout;

@property (nonatomic, strong) UILabel *constLbl;

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) UICollectionView *historyColView;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) NSMutableArray *historyDataMArr;

@end

@implementation FreeShopSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadHistoryColView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [MXCache setValue:self.historyDataMArr forKey:historyKey];
}

- (void)fetchData {
    [self request:@"/api/freeshop/goods/getHotKeywordList"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSArray *arr = object[@"data"][@"hotKeywordList"];
            arr = [arr valueForKeyPath:@"@distinctUnionOfObjects.key_word"];
            NSMutableArray *MArr = [NSMutableArray arrayWithCapacity:arr.count];
            for (NSString *title in arr) {
                self.constLbl.text = title;
                [self.constLbl sizeToFit];
                HotKeywordListObj *obj = [HotKeywordListObj new];
                obj.key = title;
                obj.itemSize = self.constLbl.size;
                [MArr addObject:obj];
            }
            self.dataArr = MArr;
            [self.colView reloadData];
        }
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.colView) {
        return self.dataArr.count;
    } else {
        return self.historyDataMArr.count;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = UICollectionViewCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    UILabel *lbl = [cell.contentView viewWithTag:1000];
    if (!lbl) {
        lbl = [UILabel new];
        lbl.tag = 1000;
        lbl.font = self.constLbl.font;
        lbl.textColor = [UIColor blackColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
        [lbl setViewCornerRadius:5];
        [cell.contentView addSubview:lbl];
        
        [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(cell.contentView);
        }];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *dataArr = self.dataArr;
    if (collectionView == self.historyColView) {
        dataArr = self.historyDataMArr;
    }
    if (indexPath.item >= dataArr.count) {
        return;
    }
    HotKeywordListObj *obj = dataArr[indexPath.item];
    if (![obj isKindOfClass:HotKeywordListObj.class]) {
        return;
    }
    UILabel *lbl = [cell.contentView viewWithTag:1000];
    if (lbl) {
        lbl.text = obj.key;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *dataArr = self.dataArr;
    if (collectionView == self.historyColView) {
        dataArr = self.historyDataMArr;
    }
    if (indexPath.item >= dataArr.count) {
        return;
    }
    HotKeywordListObj *obj = dataArr[indexPath.item];
    if (![obj isKindOfClass:HotKeywordListObj.class]) {
        return;
    }
    [self addHistoryDataMArr:obj.key];
    MXRoute(@"GoodsSortListVC", (@{@"isFree":@(YES), @"key_word":obj.key}));
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *dataArr = self.dataArr;
    if (collectionView == self.historyColView) {
        dataArr = self.historyDataMArr;
    }
    if (indexPath.item >= dataArr.count) {
        return CGSizeZero;
    }
    HotKeywordListObj *obj = dataArr[indexPath.item];
    if (![obj isKindOfClass:HotKeywordListObj.class]) {
        return CGSizeZero;
    }
    return CGSizeMake(obj.itemSize.width + 10, obj.itemSize.height + 5);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.searchTF) {
        NSString *key_word = [StringUtil trim:self.searchTF.text];
        if ([StringUtil isEmpty:key_word]) {
            return YES;
        }
        [self addHistoryDataMArr:key_word];
        MXRoute(@"GoodsSortListVC", (@{@"isFree":@(YES), @"key_word":key_word}));
    }
    return YES;
}

- (void)addHistoryDataMArr:(NSString *)key {
    if ([StringUtil isEmpty:key]) {
        return;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"key == '%@'", key)];
    NSArray *tempArr = [self.historyDataMArr filteredArrayUsingPredicate:predicate];
    if (tempArr.count == 0) {
        HotKeywordListObj *obj = [HotKeywordListObj new];
        obj.key = key;
        self.constLbl.text = key;
        [self.constLbl sizeToFit];
        obj.itemSize = self.constLbl.size;
        [self.historyDataMArr insertObject:obj atIndex:0];
    } else {
        HotKeywordListObj *obj = tempArr.firstObject;
        if (obj) {
            [self.historyDataMArr removeObject:obj];
            [self.historyDataMArr insertObject:obj atIndex:0];
        }
    }
    if (self.historyDataMArr.count > 10) {
        [self.historyDataMArr removeLastObject];
    }
    [self reloadHistoryColView];
}

- (void)reloadHistoryColView {
    [self.historyColView reloadData];
    [self.historyColView layoutIfNeeded];
    self.historyColView.myHeight = self.historyColView.contentSize.height;
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self.view addMyLinearLayout:MyOrientation_Vert];
    self.baseLayout = baseLayout;
    baseLayout.backgroundColor = [UIColor whiteColor];
    [baseLayout addSubview:self.navTitleView];
    
    MyBaseLayout *layout = [self.class factoryLbl:@"搜索历史"];
    layout.myTop = 25;
    [baseLayout addSubview:layout];
    [baseLayout addSubview:self.historyColView];
    
    layout = [self.class factoryLbl:@"热搜榜"];
    layout.myTop = 30;
    [baseLayout addSubview:layout];
    [baseLayout addSubview:self.colView];
}

#pragma mark - init
- (UIView *)navTitleView {
    if (!_navTitleView) {
        _navTitleView = ({
            UIView *view = [UIView new];
            view.frame = CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom);
            view.backgroundColor = [UIColor whiteColor];
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateNormal];
            [leftBtn setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateHighlighted];
            leftBtn.imageView.tintColor = [UIColor blackColor];
            [leftBtn addAction:^(UIButton *btn) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
                        
            [view addSubview:leftBtn];
            [view addSubview:self.searchTF];
            
            [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(30, 30));
                make.left.mas_equalTo(view.mas_left).offset(10);
                make.bottom.mas_equalTo(view.mas_bottom).offset(-5);
            }];
            
            [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(30);
                make.left.mas_equalTo(leftBtn.mas_right).offset(5);
                make.right.mas_equalTo(view.mas_right).offset(-10);
                make.centerY.mas_equalTo(leftBtn.mas_centerY);
            }];
                                    
            view.mySize = view.size;
            view.myLeft = 0;
            view.myTop = 0;
            view;
        });
    }
    return _navTitleView;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = [UITextField new];
        _searchTF.delegate = self;
        _searchTF.height = 30;
        [_searchTF setViewCornerRadius:_searchTF.height/2.0];
        _searchTF.font = [UIFont font15];
        _searchTF.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
        UIView *leftView = [UIView new];
        leftView.size = CGSizeMake(40, 30);
        
        UIView *rightView = [UIView new];
        rightView.size = CGSizeMake(40, 30);
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
        [leftBtn sizeToFit];
        leftBtn.x = 10;
        leftBtn.y = 5;
        [leftView addSubview:leftBtn];
//        _searchTF.rightView = rightView;
        _searchTF.leftView = leftView;
        _searchTF.returnKeyType = UIReturnKeySearch;
        _searchTF.leftViewMode = UITextFieldViewModeAlways;
        _searchTF.rightViewMode = UITextFieldViewModeAlways;
        _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索"]
                                                                     colors:@[[UIColor colorWithHexString:@"#C1C1C1"]]
                                                                      fonts:@[[UIFont font14]]];
        _searchTF.attributedPlaceholder = att;
    }
    return _searchTF;
}

- (UILabel *)constLbl {
    if (!_constLbl) {
        _constLbl = [UILabel new];
        _constLbl.font = [UIFont font13];
    }
    return _constLbl;
}

- (UICollectionView *)colView {
    if (!_colView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.sectionInset = UIEdgeInsetsMake(10, 15, 0, 15);
        //cell 靠左显示
        SEL sel = NSSelectorFromString(@"_setRowAlignmentsOptions:");
        if ([layout respondsToSelector:sel]) {
            NSDictionary *dic =  @{@"UIFlowLayoutCommonRowHorizontalAlignmentKey":@(NSTextAlignmentLeft),
                                   @"UIFlowLayoutLastRowHorizontalAlignmentKey" : @(NSTextAlignmentLeft),
                                   @"UIFlowLayoutRowVerticalAlignmentKey" : @(NSTextAlignmentCenter)};
            ((void(*)(id,SEL,NSDictionary*))objc_msgSend)(layout,sel,dic);
        }
        
        _colView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _colView.backgroundColor = [UIColor whiteColor];
        _colView.delegate = self;
        _colView.dataSource = self;
        Class cls = UICollectionViewCell.class;
        [_colView registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
        _colView.myLeft = 0;
        _colView.myTop = 0;
        _colView.myRight = 0;
        _colView.weight = 1;
    }
    return _colView;
}

- (UICollectionView *)historyColView {
    if (!_historyColView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.sectionInset = UIEdgeInsetsMake(10, 15, 0, 15);
        //cell 靠左显示
        SEL sel = NSSelectorFromString(@"_setRowAlignmentsOptions:");
        if ([layout respondsToSelector:sel]) {
            NSDictionary *dic =  @{@"UIFlowLayoutCommonRowHorizontalAlignmentKey":@(NSTextAlignmentLeft),
                                   @"UIFlowLayoutLastRowHorizontalAlignmentKey" : @(NSTextAlignmentLeft),
                                   @"UIFlowLayoutRowVerticalAlignmentKey" : @(NSTextAlignmentCenter)};
            ((void(*)(id,SEL,NSDictionary*))objc_msgSend)(layout,sel,dic);
        }
        
        _historyColView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _historyColView.backgroundColor = [UIColor whiteColor];
        _historyColView.delegate = self;
        _historyColView.dataSource = self;
        Class cls = UICollectionViewCell.class;
        [_historyColView registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
        _historyColView.myLeft = 0;
        _historyColView.myTop = 0;
        _historyColView.myRight = 0;
        _historyColView.myHeight = MyLayoutSize.wrap;
    }
    return _historyColView;
}

- (NSMutableArray *)historyDataMArr {
    if (!_historyDataMArr) {
        _historyDataMArr = [NSMutableArray array];
        NSArray *arr = [MXCache valueForKey:historyKey];
        if ([arr isKindOfClass:NSArray.class]) {
            [_historyDataMArr addObjectsFromArray:arr];
        }
    }
    return _historyDataMArr;
}

+ (MyBaseLayout *)factoryLbl:(NSString *)title {
    MyLinearLayout *layout= [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myWidth = MyLayoutSize.wrap;
    layout.myHeight = MyLayoutSize.wrap;
    layout.myLeft = 15;
    
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor moGreen];
    view.size = CGSizeMake(6, 18);
    view.mySize = view.size;
    view.myCenterY = 0;
    [layout addSubview:view];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont16];
    lbl.textColor = [UIColor blackColor];
    lbl.myCenterY = 0;
    lbl.myLeft = 10;
    lbl.text = title;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    [layout addSubview:lbl];
    
    [layout sizeToFit];
    
    return layout;
}

@end
