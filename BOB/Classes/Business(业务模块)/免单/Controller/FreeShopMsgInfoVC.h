//
//  FreeShopMsgInfoVC.h
//  BOB
//
//  Created by colin on 2020/11/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FreeShopMsgInfoVC : BaseViewController

@property (strong, nonatomic) id content;

@property (nonatomic, copy) NSString *time;

@end

NS_ASSUME_NONNULL_END
