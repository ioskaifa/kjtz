//
//  FreeShopMsgCenterVC.m
//  BOB
//
//  Created by mac on 2019/7/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "FreeShopMsgCenterVC.h"
#import "MessageCenterCell.h"
#import "MessageCenterModel.h"
#import "FreeShopMsgInfoVC.h"

@interface FreeShopMsgCenterVC ()


@end

@implementation FreeShopMsgCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNavBarTitle:@"历史公告"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - Override Super Class Method
- (HTTPRequestMethod)httpMethod {
    return HTTPRequestMethodPost;
}

- (NSDictionary *)requestArgument {
    return @{@"notice_type":@"03"};
}

- (NSString *)requestUrl {
    return @"api/system/notice/getSysNoticeList";
}

///请求时传入last id的key
- (NSString *)httpRequstLastIdKey {
    return @"last_id";
}

///上拉刷新时从数据源数组中最后一个元素取值的key
- (NSString *)getValueFromItemLastIdKey {
    return @"_id";
}

///后台返回数据中数组的keypath
- (NSString *)dataListKeyPath {
    return @"data.sysNoticeList";
}

///数据源中的model,需要继承BaseObject
- (Class)model {
    return [MessageCenterModel class];
}

///是否需要上拉刷新
- (BOOL)needPullUpRefresh {
    return YES;
}

///是否需要下拉刷新
- (BOOL)needPullDownRefresh {
    return YES;
}

#pragma mark - setup
///配置tableVeiw
- (void)configTableView:(UITableView *)tableView
{
    tableView.rowHeight = 65;
    
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellNibClass:[MessageCenterCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[MessageCenterCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
    
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    MessageCenterModel *model = [tableView std_itemAtIndexPath:indexPath].data;
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[model.notice_content dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
//    MXRoute(@"FreeShopMsgInfoVC", (@{@"content": attrStr ?: @"",
//                                  @"title":model.notice_title,
//                                  @"time":model.cre_date,
//                                }));
    __block FreeShopMsgInfoVC *vc = nil;
    [self.navigationController.viewControllers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:FreeShopMsgInfoVC.class]) {
            vc = obj;
            *stop = YES;
        }
    }];
    if (vc) {
        vc.content = attrStr;
        vc.title = model.notice_title;
        [self.navigationController popToViewController:vc animated:YES];
    } else {
        MXRoute(@"FreeShopMsgInfoVC", nil);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

@end
