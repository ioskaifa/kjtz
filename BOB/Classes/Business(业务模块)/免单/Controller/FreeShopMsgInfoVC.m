//
//  FreeShopMsgInfoVC.m
//  BOB
//
//  Created by colin on 2020/11/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeShopMsgInfoVC.h"
#import "FreeChargeOrderListObj.h"
#import "FreeShopNameListCell.h"

@interface FreeShopMsgInfoVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UITextView *plainText;

@end

@implementation FreeShopMsgInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    if([self.content isKindOfClass:[NSString class]]) {
        self.plainText.text = self.content ?: @"";
    } else if([self.content isKindOfClass:[NSAttributedString class]]) {
        self.plainText.attributedText = self.content;
    }
    self.titleLbl.text = self.title;
}

- (void)navBarRightBtnAction:(id)sender {
    __block UIViewController *vc = nil;
    [self.navigationController.viewControllers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:NSClassFromString(@"FreeShopMsgCenterVC")]) {
            vc = obj;
            *stop = YES;
        }
    }];
    if (vc) {
        [self.navigationController popToViewController:vc animated:YES];
    } else {
        MXRoute(@"FreeShopMsgCenterVC", nil);
    }
}

- (void)fetchData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [self request:@"api/freeshop/freecharge/getPastFreeChargeOrderList"
            param:@{@"last_id":self.last_id?:@""
            }
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSArray *dataArr = [FreeChargeOrderListObj modelListParseWithArray:object[@"data"][@"freeChargeOrderList"]];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            FreeChargeOrderListObj *obj = [self.dataSourceMArr lastObject];
            if ([obj isKindOfClass:FreeChargeOrderListObj.class]) {
                self.last_id = obj.ID;
            }
            [self.tableView reloadData];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = FreeShopNameListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:FreeShopNameListCell.class]) {
        return;
    }
    FreeShopNameListCell *listCell = (FreeShopNameListCell *)cell;
    [listCell configureView:self.dataSourceMArr[indexPath.row]];
}

- (void)createUI {
    [self setNavBarTitle:@"公告详情"];
    [self setNavBarRightBtnWithTitle:@"历史公告" andImageName:nil];
    self.last_id = @"";
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    baseLayout.backgroundColor = [UIColor colorWithHexString:@"#038887"];
    [self.view addSubview:baseLayout];
    
//    UILabel *lbl = [UILabel new];
//    lbl.font = [UIFont fontWithName:@"DINAlternate-Bold" size:40];
//    lbl.textColor = [UIColor colorWithHexString:@"#1A9295"];
//    lbl.text = @"FREE LIST";
//    [lbl autoMyLayoutSize];
//    lbl.myCenterX = 0;
//    lbl.myTop = 30;
//    [baseLayout addSubview:lbl];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont20];
    lbl.textColor = [UIColor whiteColor];
    lbl.text = self.title;
    self.titleLbl = lbl;
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myTop = 30;
    [baseLayout addSubview:lbl];
    
//    CGFloat width = SCREEN_WIDTH - 60;
//    MyFrameLayout *layout = [MyFrameLayout new];
//    layout.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
//    layout.myLeft = 30;
//    layout.size = CGSizeMake(width, 45);
//    layout.mySize = layout.size;
//    layout.myTop = 30;
//    [baseLayout addSubview:layout];
//
//    lbl = [UILabel new];
//    lbl.font = [UIFont boldFont13];
//    lbl.textColor = [UIColor colorWithHexString:@"#038887"];
//    lbl.text = @"买家用户名";
//    [lbl autoMyLayoutSize];
//    lbl.myCenterY = 0;
//    lbl.myLeft = 20;
//    [layout addSubview:lbl];
//
//    lbl = [UILabel new];
//    lbl.font = [UIFont boldFont13];
//    lbl.textColor = [UIColor colorWithHexString:@"#038887"];
//    lbl.text = @"商品名称";
//    [lbl autoMyLayoutSize];
//    lbl.myCenterY = 0;
//    lbl.myLeft = width/2.0;
//    [layout addSubview:lbl];
    
    [baseLayout addSubview:self.plainText];
    if([self.content isKindOfClass:[NSString class]]) {
        self.plainText.text = self.content ?: @"";
    } else if([self.content isKindOfClass:[NSAttributedString class]]) {
        self.plainText.attributedText = self.content;
    }
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [FreeShopNameListCell viewHeight];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        Class cls = FreeShopNameListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.myTop = 0;
        _tableView.myLeft = 30;
        _tableView.myRight = 30;
        _tableView.weight = 1;
        _tableView.myBottom = 100 + safeAreaInsetBottom();
        @weakify(self)
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
    }
    return _tableView;
}

- (UITextView *)plainText {
    if (!_plainText) {
        _plainText = [UITextView new];
        _plainText.editable = NO;
        [_plainText setViewCornerRadius:5];
        _plainText.myTop = 20;
        _plainText.myLeft = 30;
        _plainText.myRight = 30;
        _plainText.weight = 1;
        _plainText.myBottom = 100 + safeAreaInsetBottom();
    }
    return _plainText;
}

@end
