//
//  MyFreeShopNameListVC.m
//  BOB
//
//  Created by colin on 2020/11/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyFreeShopNameListVC.h"
#import "MyFreeShopNameListCell.h"
#import "MyOrderListObj.h"

@interface MyFreeShopNameListVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation MyFreeShopNameListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)fetchData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [self request:@"api/freeshop/freecharge/getUserFreeChargeOrderList"
            param:@{@"last_id":self.last_id}
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSArray *dataArr = [MyOrderListObj modelListParseWithArray:object[@"data"][@"orderList"]];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            MyOrderListObj *obj = [self.dataSourceMArr lastObject];
            if ([obj isKindOfClass:MyOrderListObj.class]) {
                self.last_id = obj.ID;
            }
            [self.tableView reloadData];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MyFreeShopNameListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    MyOrderListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    BuyGoodsListObj *infoObj = obj.orderList.firstObject;
    if (![infoObj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    if (![cell isKindOfClass:MyFreeShopNameListCell.class]) {
        return;
    }
    MyFreeShopNameListCell *listCell = (MyFreeShopNameListCell *)cell;
    [listCell configureView:infoObj];
}

- (void)createUI {
    [self setNavBarTitle:@"我的免单"];
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    baseLayout.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:baseLayout];
    [baseLayout addSubview:self.tableView];
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [MyFreeShopNameListCell viewHeight];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        Class cls = MyFreeShopNameListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
        _tableView.myBottom = 0;
        
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
    }
    return _tableView;
}

@end
