//
//  FreeShopVC.m
//  BOB
//
//  Created by Colin on 2020/10/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeShopVC.h"
#import "FreeShopHotListView.h"
#import "GoodsListObj.h"
#import "CategoryObj.h"
#import "YJSliderView.h"
//#import "FreeShopListCell.h"
#import "FreeShopListDoubleCell.h"
#import "FreeChargeOrderListObj.h"
#import "OttoCycleLabel.h"

#import "DXPopover.h"
#import "FreeCategoryView.h"

#import "FreeSliderView.h"
#import "NSDate+String.h"
#import "MessageCenterModel.h"

static NSString * const isFirstTime = @"FreeShopVCisFirstTime";

@interface FreeShopVC ()<UITableViewDelegate, UITableViewDataSource, FreeSliderViewDelegate, FreeCategoryViewDelegate>

@property (nonatomic, strong) MyBaseLayout *navTitleView;

@property (nonatomic, strong) MyBaseLayout *baseLayout;

@property (nonatomic, strong) FreeShopHotListView *hotListView;

@property (nonatomic, strong) FreeSliderView *sliderView;

@property (nonatomic, strong) CategoryObj *selectCategoryObj;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, assign) NSInteger page_num;

@property (nonatomic, strong) OttoCycleLabel *notiLbl;

@property (nonatomic, strong) UILabel *notiExLbl;

@property (nonatomic, strong) NSArray *freeChargeOrderListArr;

@property (nonatomic, strong) FreeCategoryView *freeCategoryView;

@property (strong, nonatomic) DXPopover            *popover;

@property (nonatomic, strong) NSArray *categoryArr;

@property (nonatomic, strong) NSArray *hotGoodsListArr;

@property (nonatomic, strong) MyBaseLayout *headerLayout;

@property (nonatomic, strong) MessageCenterModel *msgObj;


@end

@implementation FreeShopVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
    [self getIndexHotGoodsList];
    [self fetchNewNotice];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)fetchData {
    [self request:@"api/freeshop/goods/getCategoryList"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *tempArr = dataDic[@"data"][@"categoryList"];
            self.categoryArr = [CategoryObj modelListParseWithArray:tempArr];
            self.selectCategoryObj = [self.categoryArr firstObject];
            self.selectCategoryObj.isSelected = YES;
            [self fetchCategoryGoodsListData];
            if (!self.tableView.superview) {
                [self.baseLayout addSubview:self.tableView];
            }
        }
    }];
    [self fetchFreeChargeData];
}

- (void)getIndexHotGoodsList {
    [self request:@"api/freeshop/goods/getIndexHotGoodsList"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSArray *dataArr = [GoodsListObj modelListParseWithArray:object[@"data"][@"goodsList"]];
            self.hotGoodsListArr = dataArr;
            [self.hotListView configureView:dataArr];
            [self initUI];
        }
    }];
}

- (void)fetchNewNotice {
    if (![self isFirtTime]) {
        return;
    }
    [self request:@"api/system/notice/getNewNotice"
            param:@{@"notice_type":@"02"}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSString *notice_content = object[@"data"][@"noticeInfo"][@"notice_content"]?:@"";
            NSString *notice_title = object[@"data"][@"noticeInfo"][@"notice_title"]?:@"";
            if ([StringUtil isEmpty:notice_title]) {
                notice_title = @"公告";
            }
            NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[notice_content dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            UIAlertController *alterController = [UIAlertController alertControllerWithTitle:notice_title
                                                                                     message:nil
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *actionSubmit = [UIAlertAction actionWithTitle:@"确定"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {}];
            
            [alterController addAction:actionSubmit];
            [alterController setValue:attrStr forKey:@"attributedMessage"];
            [self presentViewController:alterController animated:YES completion:^{}];
        }
    }];
}

//是否是今天第一次
- (BOOL)isFirtTime {
    NSString *key = StrF(@"%@%@", isFirstTime, UDetail.user.user_id?:@"");
    NSString *string = [MXCache valueForKey:key]?:@"";
    NSString *currentDate = [NSDate dateString:[NSDate date]];
    if ([StringUtil isEmpty:string]) {
        [MXCache setValue:currentDate forKey:key];
        return YES;
    } else {
        if ([currentDate isEqualToString:string]) {
            return NO;
        } else {
            [MXCache setValue:currentDate forKey:key];
        }
    }
    return YES;
}

- (void)fetchCategoryGoodsListData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [self request:@"api/freeshop/goods/getCategoryGoodsList"
            param:@{@"category_id":self.selectCategoryObj.ID,
                    //                    @"page_num":[NSNumber numberWithInteger:self.page_num],
                    @"last_id":self.last_id,
            }
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *tempArr = dataDic[@"data"][@"goodsList"];
            NSArray *dataArr = [GoodsListObj modelListParseWithArray:tempArr];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            GoodsListObj *obj = self.dataSourceMArr.lastObject;
            if ([obj isKindOfClass:GoodsListObj.class]) {
                self.last_id = obj.order_num;
            }
            [self.tableView reloadData];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)fetchFreeChargeData {
    [self request:@"api/system/notice/getNewNotice"
            param:@{@"notice_type":@"03"
            }
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            MessageCenterModel *obj = [MessageCenterModel modelParseWithDict:object[@"data"][@"noticeInfo"]];
            self.msgObj = obj;
            self.notiExLbl.text = obj.notice_title?:@"";
        }
    }];
}

- (void)configureNotiView {
    NSArray *tempArr = [self.freeChargeOrderListArr valueForKeyPath:@"@distinctUnionOfObjects.des"];
    self.notiLbl.textsArr = tempArr;
}

#pragma mark FreeSliderViewDelegate
- (void)freeSliderView:(FreeSliderView *)sliderView didSelectItemAtIndex:(NSInteger)index {
    if (index >= self.categoryArr.count) {
        return;
    }
    self.selectCategoryObj.isSelected = NO;
    self.selectCategoryObj = self.categoryArr[index];
    self.selectCategoryObj.isSelected = YES;
    self.last_id = @"";
    [self fetchCategoryGoodsListData];
}

- (void)freeCategoryView:(FreeCategoryView *)sliderView didSelectItemAtIndex:(NSInteger)index {
    [self.popover dismiss];
    if (index >= self.categoryArr.count) {
        return;
    }
    self.selectCategoryObj.isSelected = NO;
    self.selectCategoryObj = self.categoryArr[index];
    self.selectCategoryObj.isSelected = YES;
    self.last_id = @"";
    [self fetchCategoryGoodsListData];
    [self.sliderView reloadData];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ceil(self.dataSourceMArr.count / 2.0);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = FreeShopListDoubleCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return self.sliderView;
    } else {
        return [UIView new];
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return self.sliderView.height;
    } else {
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:FreeShopListDoubleCell.class]) {
        return;
    }
    
    FreeShopListDoubleCell *listCell = (FreeShopListDoubleCell *)cell;
    NSInteger index = indexPath.row * 2;
    NSInteger nextIndex = index+1;
    if (nextIndex < self.dataSourceMArr.count) {
        [listCell configureView:@[self.dataSourceMArr[index], self.dataSourceMArr[nextIndex]]];
    } else {
        [listCell configureView:@[self.dataSourceMArr[index]]];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row >= self.dataSourceMArr.count) {
//        return;
//    }
//    GoodsListObj *obj = self.dataSourceMArr[indexPath.row];
//    MXRoute(@"GoodInfoVC", (@{@"goods_id":obj.ID, @"isFree":@(1)}))
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    if ([@"FreeShopListDoubleCellDidSelect" isEqualToString:eventName]) {
        GoodsListObj *obj = userInfo[@"obj"];
        if (![obj isKindOfClass:GoodsListObj.class]) {
            return;
        }
        MXRoute(@"GoodInfoVC", (@{@"goods_id":obj.ID, @"isFree":@(1)}))
    } else {
        [self.nextResponder routerEventWithName:eventName userInfo:userInfo];
    }
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor colorWithHexString:@"#00A99B"];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    self.baseLayout = layout;
    [self.view addSubview:layout];
    
    [layout addSubview:self.navTitleView];
    [layout addSubview:[self notifiView]];
    
    self.last_id = @"";
    self.page_num = 0;
    
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"免单购物车"];
    imgView.size = CGSizeMake(56, 56);
    [imgView addAction:^(UIView *view) {
        MXRoute(@"FreeShoppingCartVC", nil)
    }];
    [self.view addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(imgView.size);
        make.left.mas_equalTo(self.view.mas_left).mas_offset(15);
        make.bottom.mas_equalTo(self.view.mas_bottom).mas_offset(-(96+safeAreaInsetBottom()));
    }];
    
    imgView = [UIImageView autoLayoutImgView:@"免单订单中心"];
    imgView.size = CGSizeMake(56, 56);
    [imgView addAction:^(UIView *view) {
        MXRoute(@"MyOrderVC", @{@"isFree":@(1)})
    }];
    [self.view addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(imgView.size);
        make.left.mas_equalTo(self.view.mas_left).mas_offset(15);
        make.bottom.mas_equalTo(self.view.mas_bottom).mas_offset(-(25+safeAreaInsetBottom()));
    }];
}

- (void)initUI {
    if (!self.tableView.tableHeaderView && self.hotGoodsListArr.count) {
        self.tableView.tableHeaderView = [self headerView];
    }
}

- (MyBaseLayout *)headerView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    self.headerLayout = layout;
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myHeight = MyLayoutSize.wrap;
    
    [layout addSubview:self.hotListView];
    layout.myWidth = SCREEN_WIDTH;
    [layout layoutIfNeeded];
    return layout;
}

#pragma mark - init
- (MyBaseLayout *)navTitleView {
    if (!_navTitleView) {
        _navTitleView = ({
            MyFrameLayout *view = [MyFrameLayout new];
            view.frame = CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom);
            view.backgroundColor = [UIColor colorWithHexString:@"#00A99B"];
            
            MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            layout.myBottom = 0;
            layout.myLeft = 0;
            layout.myRight = 0;
            layout.myHeight = 30;
            [view addSubview:layout];
            
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"free_black_back"] forState:UIControlStateNormal];
            [leftBtn setImage:[UIImage imageNamed:@"free_black_back"] forState:UIControlStateHighlighted];
            leftBtn.imageView.tintColor = [UIColor whiteColor];
            [leftBtn addAction:^(UIButton *btn) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            leftBtn.size = CGSizeMake(30, 30);
            leftBtn.myLeft = 10;
            leftBtn.myCenterY = 0;
            leftBtn.visibility = MyVisibility_Gone;
            [layout addSubview:leftBtn];
            
            UIImageView *imgView = [UIImageView autoLayoutImgView:@"免单title"];
            imgView.myCenterY = 0;
            imgView.myLeft = 15;
            [layout addSubview:imgView];
            
            imgView = [UIImageView autoLayoutImgView:@"免单提示"];
            imgView.myCenterY = 0;
            imgView.myLeft = 10;
            imgView.visibility = MyVisibility_Invisible;
            [layout addSubview:imgView];
            [layout addSubview:[UIView placeholderHorzView]];
            
            imgView = [UIImageView autoLayoutImgView:@"免单搜索"];
            imgView.myCenterY = 0;
            imgView.myRight = 12;
            //            imgView.visibility = MyVisibility_Invisible;
            [imgView addAction:^(UIView *view) {
                MXRoute(@"FreeShopSearchVC", nil)
            }];
            [layout addSubview:imgView];
            
            view;
        });
    }
    return _navTitleView;
}

- (MyBaseLayout *)notifiView {
    MyLinearLayout *layuot = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layuot.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.22];
    layuot.myHeight = 30;
    layuot.myLeft = 15;
    layuot.myRight = 15;
    layuot.myTop = 20;
    [layuot setViewCornerRadius:3];
    
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"免单广播"];
    imgView.size = CGSizeMake(20, 20);
    imgView.mySize = imgView.size;
    imgView.myLeft = 10;
    imgView.myRight = 10;
    imgView.myCenterY = 0;
    [layuot addSubview:imgView];
    
    [layuot addSubview:self.notiExLbl];
    
    imgView = [UIImageView autoLayoutImgView:@"public_white_back"];
    imgView.size = CGSizeMake(6, 11);
    imgView.transform = CGAffineTransformScale(imgView.transform, -1.0, 1.0);
    imgView.mySize = imgView.size;
    imgView.myLeft = 5;
    imgView.myRight = 10;
    imgView.myCenterY = 0;
    [layuot addSubview:imgView];
    @weakify(self)
    [layuot addAction:^(UIView *view) {
        @strongify(self)
        //        MXRoute(@"FreeShopNameListVC", nil)
        if (!self.msgObj) {
            return;
        }
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[self.msgObj.notice_content dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        MXRoute(@"FreeShopMsgInfoVC", (@{@"content": attrStr ?: @"",
                                         @"title":self.msgObj.notice_title,
                                         @"time":self.msgObj.cre_date,
                                       }));
    }];
    
    return layuot;
}

- (FreeShopHotListView *)hotListView {
    if (!_hotListView) {
        _hotListView = [FreeShopHotListView new];
        _hotListView.myHeight = [FreeShopHotListView viewHeight];
        _hotListView.myTop = 0;
        _hotListView.myLeft = 0;
        _hotListView.myRight = 0;
    }
    return _hotListView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray new];
    }
    return _dataSourceMArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor colorWithHexString:@"#00A99B"];
        _tableView.tableFooterView = [UIView new];
        Class cls = FreeShopListDoubleCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.myTop = 5;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
        _tableView.rowHeight = [FreeShopListDoubleCell viewHeight];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            self.page_num = 0;
            [self fetchCategoryGoodsListData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            self.page_num++;
            [self fetchCategoryGoodsListData];
        }];
    }
    return _tableView;
}

- (OttoCycleLabel *)notiLbl {
    if (!_notiLbl) {
        _notiLbl = [[OttoCycleLabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 70, 20) texts:nil];
        _notiLbl.timeInterval = 2;
        _notiLbl.directionMode = DirectionTransitionFromTop;
        _notiLbl.font = [UIFont font12];
        _notiLbl.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
        _notiLbl.textAlignment = NSTextAlignmentLeft;
        _notiLbl.myCenterY = 0;
        _notiLbl.myHeight = 20;
        _notiLbl.weight = 1;
    }
    return _notiLbl;
}

- (UILabel *)notiExLbl {
    if (!_notiExLbl) {
        _notiExLbl = [UILabel new];
        _notiExLbl.font = [UIFont font12];
        _notiExLbl.textColor = [UIColor whiteColor];
        _notiExLbl.myCenterY = 0;
        _notiExLbl.myHeight = 20;
        _notiExLbl.weight = 1;
    }
    return _notiExLbl;
}

- (DXPopover *)popover {
    if (!_popover) {
        _popover = [DXPopover popover];
        self.popover.backgroundColor = [UIColor whiteColor];
        self.popover.contentInset = UIEdgeInsetsZero;
    }
    return _popover;
}

- (FreeSliderView *)sliderView {
    if (!_sliderView) {
        _sliderView = [[FreeSliderView alloc] initWithData:self.categoryArr];
        _sliderView.delegate = self;
        _sliderView.size = CGSizeMake(SCREEN_WIDTH, [FreeSliderView viewHeight]);
        _sliderView.mySize = _sliderView.size;
        @weakify(self)
        [_sliderView.searchBtn addAction:^(UIButton *btn) {
            @strongify(self)
            if ([self.popover superview]) {
                return;
            }
            UIView *titleView = btn;
            UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
            
            CGRect rect= [titleView convertRect: titleView.bounds toView:window];
            
            CGPoint startPoint =
            CGPointMake(CGRectGetMinX(rect)+rect.size.width/2, CGRectGetMaxY(rect));
            [self.freeCategoryView reloadData];
            [self.popover showAtPoint:startPoint
                       popoverPostion:DXPopoverPositionDown
                      withContentView:self.freeCategoryView
                               inView:[UIApplication sharedApplication].keyWindow];
        }];
    }
    return _sliderView;
}

- (FreeCategoryView *)freeCategoryView {
    if (!_freeCategoryView) {
        _freeCategoryView = [[FreeCategoryView alloc] initWithDataArr:self.categoryArr];
        _freeCategoryView.size = CGSizeMake(SCREEN_WIDTH, [_freeCategoryView viewHeight]);
        _freeCategoryView.delegate = self;
    }
    return _freeCategoryView;
}

@end
