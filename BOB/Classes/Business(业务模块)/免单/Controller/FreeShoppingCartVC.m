//
//  FreeShoppingCartVC.m
//  BOB
//
//  Created by colin on 2020/11/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeShoppingCartVC.h"
#import "BuyNullHeaderView.h"
#import "BuyHeaderView.h"
#import "BuyFooterView.h"
#import "CartGoodsListObjCell.h"
#import "BuyGoodsListObj.h"
#import "BuySettlementView.h"
#import "UserAddressListObj.h"

@interface FreeShoppingCartVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) BuyNullHeaderView *nullHeaderView;

@property (nonatomic, strong) BuyHeaderView *headerView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) BuyFooterView *footerView;
///区分上拉还是下拉  下拉更新购物车和为您推荐 上拉只更新为您推荐
@property (nonatomic, assign) NSInteger pageIndex;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, strong) SPButton *clearBtn;
@property (nonatomic, strong) UIButton *editBtn;
///去结算
@property (nonatomic, strong) BuySettlementView *buyView;

@end

@implementation FreeShoppingCartVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchDefualtAddress];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.last_id = @"";
    [self fetchData];
}

- (void)navBarRightBtnAction:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    sender.selected = !sender.selected;
    [sender setTitle:sender.selected?@"完成":@"编辑" forState:UIControlStateNormal];
    if (sender.selected) {
//        self.tableView.tableFooterView = [UIView new];
        self.buyView.viewType = BuySettlementViewTypeDel;
    } else {
//        self.tableView.tableFooterView = self.footerView;
        self.buyView.viewType = BuySettlementViewTypeNormal;
    }
    sender.userInteractionEnabled = YES;
}

#pragma mark - Network Method
- (void)fetchData {
    [self fetchCartNum];
//    [self fetchFooterViewData];
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [self request:@"api/freeshop/cart/getUserGoodsCartList"
            param:@{@"last_id":self.last_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object[@"data"];
            NSArray *dataArr = [BuyGoodsListObj modelListParseWithArray:dataDic[@"shopCartList"]];
            BuyGoodsListObj *obj = [dataArr lastObject];
            if ([obj isKindOfClass:BuyGoodsListObj.class]) {
                self.last_id = obj.ID;
            }
            [self.dataSourceMArr addObjectsFromArray:dataArr];
        }
        [self reloadTableView];
    }];
}

- (BOOL)valiParam {
    if (self.dataSourceMArr.count == 0) {
        [NotifyHelper showMessageWithMakeText:@"购物车中没有商品"];
        return NO;
    }
    if ([self selectedGoods].count == 0) {
        [NotifyHelper showMessageWithMakeText:@"请选择要结算的商品"];
        return NO;
    }
    if ([self invalidSelectGoods].count > 0) {
        [NotifyHelper showMessageWithMakeText:@"选中了失效商品, 无法结算"];
        return NO;
    }
    return YES;
}

#pragma mark - 创建临时订单
- (void)createTempOrder {
    NSArray *IDArr = [[self selectedGoods] valueForKeyPath:@"ID"];
    NSString *ids_list = [IDArr componentsJoinedByString:@","];
    [self request:@"api/freeshop/cart/userGoodsCartSettle"
            param:@{@"ids_list":ids_list}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSString *orderId = object[@"data"][@"order_id"];
            MXRoute(@"ConfirmOrderVC", (@{@"order_id":orderId?:@"", @"fromType":@(1), @"isFree":@(1)}))
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark - 查询默认地址
- (void)fetchDefualtAddress {
    [self request:@"api/user/address/getUserAddressInfo"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            UserAddressListObj *obj = [UserAddressListObj modelParseWithDict:object[@"data"][@"userAddress"]];
            [self.headerView configureView:obj.address];
        }
    }];
}

#pragma mark - 获取购物车数量
- (void)fetchCartNum {
    [self request:@"api/freeshop/cart/getUserCartTotalNum"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSInteger cart_total_num = [object[@"data"][@"cart_total_num"] integerValue];
            [self setNavBarTitle:StrF(@"购物车(%lu)", cart_total_num)];
        }
    }];
}

#pragma mark - 获取为您推荐
- (void)fetchFooterViewData {
    if (self.pageIndex == 0) {
        [self.footerView fetchLastData:^(CGFloat height) {
            if (self.footerView.height == height) {
                return;
            }
            self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
            self.tableView.tableFooterView = self.footerView;
        }];
    } else {
        [self.footerView fetchMoreData:^(CGFloat height) {
            if (self.footerView.height == height) {
                return;
            }
            self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
            self.tableView.tableFooterView = self.footerView;
        }];
    }
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark 获取选中的商品
- (NSArray *)selectedGoods {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected == YES"];
    NSArray *tempArr = [self.dataSourceMArr filteredArrayUsingPredicate:predicate];
    return tempArr;
}

#pragma mark 获取失效的商品
- (NSArray *)invalidGoods {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isInvalid == YES"];
    NSArray *tempArr = [self.dataSourceMArr filteredArrayUsingPredicate:predicate];
    return tempArr;
}

#pragma mark 获取选中的失效的商品
- (NSArray *)invalidSelectGoods {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isInvalid == YES && isSelected == YES"];
    NSArray *tempArr = [self.dataSourceMArr filteredArrayUsingPredicate:predicate];
    return tempArr;
}

#pragma mark 设置所有商品选中与否
- (void)setAllGoodsSelected:(BOOL)selected {
    for (BuyGoodsListObj *obj in self.dataSourceMArr) {
        if (![obj isKindOfClass:BuyGoodsListObj.class]) {
            continue;
        }
        obj.isSelected = selected;
    }
    [self.tableView reloadData];
    [self configureBuyView];
}

#pragma mark - 购物车商品数量变化
- (void)goodNumberChange:(NSDictionary *)paramDic {
    BOOL status = [paramDic[@"status"] boolValue];
    BuyGoodsListObj *obj = paramDic[@"cellObj"];
    NSString *urlString = @"api/freeshop/cart/addUserGoodsCartGoods";
    if (!status) {
        urlString = @"api/freeshop/cart/reduceUserGoodsCartGoods";
    }
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [self request:urlString
            param:@{@"goods_detail_id":obj.goods_detail_id?:@"",
                    @"goods_type":obj.goods_type,
                    @"goods_id":obj.goods_id,
            }
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            if (status) {
                obj.number++;
            } else {
                obj.number--;
            }
            [self.tableView reloadData];
            [self configureBuyView];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark 刷新buyView状态
- (void)configureBuyView {
    NSArray *tempArr = [self selectedGoods];
    CGFloat totalPrice = 0.0;
    NSInteger totalNum = 0;
    totalNum = [[tempArr valueForKeyPath:@"@sum.number"] integerValue];
    for (BuyGoodsListObj *obj in tempArr) {
        if (![obj isKindOfClass:BuyGoodsListObj.class]) {
            continue;
        }
        totalPrice += obj.price * obj.number;
    }
    [self.buyView configureView:totalPrice number:totalNum];
    if ((tempArr.count == self.dataSourceMArr.count) && (self.dataSourceMArr.count != 0)) {
        self.buyView.selectBtn.selected = YES;
    } else {
        self.buyView.selectBtn.selected = NO;
    }
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    if ([eventName isEqualToString:@"CartGoodsListObjCellNumberChange"]) {
        [self goodNumberChange:userInfo];
    } else if ([eventName isEqualToString:@"BuySettlementViewDelAll"]) {
        [self delAllGoods];
    } else if ([eventName isEqualToString:@"BuySettlementViewDelSelected"]) {
        [self delSelectedGoods];
    } else if ([@"CartGoodsListObjCellSelect" isEqualToString:eventName]) {
        [self goodsDidSelect:userInfo[@"cellObj"]];
    } else {
        [super routerEventWithName:eventName userInfo:userInfo];
    }
}

#pragma mark - IBAction
- (void)clearBtnClick:(UIButton *)sender {
    NSArray *tempArr = [self invalidGoods];
    if (tempArr.count == 0) {
        [NotifyHelper showMessageWithMakeText:@"购物车中没有失效的商品"];
        return;
    }
    [MXAlertViewHelper showAlertViewWithMessage:@"是否删除失效商品?" completion:^(BOOL cancelled, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [self request:@"api/freeshop/cart/clearInvalidCartGoods"
                    param:@{}
               completion:^(BOOL success, id object, NSString *error) {
                if (success) {
                    [self deleteGoods:tempArr];
                } else {
                    [NotifyHelper showMessageWithMakeText:error];
                }
            }];
        }
    }];
}

#pragma mark - 商品选中
- (void)goodsDidSelect:(BuyGoodsListObj *)obj {
    if (![obj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    obj.isSelected = !obj.isSelected;
    [self.tableView reloadData];
    [self configureBuyView];
}

#pragma mark - Private Method
#pragma mark 删除指定商品
- (void)deleteGoods:(NSArray *)dataArr {
    NSMutableArray *indexPathMArr = [NSMutableArray array];
    for (BuyGoodsListObj *obj in dataArr) {
        if (![obj isKindOfClass:BuyGoodsListObj.class]) {
            continue;
        }
        if ([self.dataSourceMArr containsObject:obj]) {
            NSInteger index = [self.dataSourceMArr indexOfObject:obj];
            [indexPathMArr addObject:[NSIndexPath indexPathForRow:index inSection:0]];
            [self.dataSourceMArr removeObject:obj];
        }
        if (indexPathMArr.count > 0) {
            [self.tableView deleteRowsAtIndexPaths:indexPathMArr withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    [self reloadTableView];
}

#pragma mark 一键删除
- (void)delAllGoods {
    if (self.dataSourceMArr.count == 0) {
        [NotifyHelper showMessageWithMakeText:@"购物车暂无商品"];
        return;
    }
    [MXAlertViewHelper showAlertViewWithMessage:@"一键删除所有商品?" completion:^(BOOL cancelled, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [self request:@"api/freeshop/cart/clearAllCartGoods"
                    param:@{}
               completion:^(BOOL success, id object, NSString *error) {
                if (success) {
                    [self.dataSourceMArr removeAllObjects];
                    [self reloadTableView];
                    [self setNavBarTitle:@"购物车(0)"];
                } else {
                    [NotifyHelper showMessageWithMakeText:error];
                }
            }];
        }
    }];
}

#pragma mark 删除选中商品
- (void)delSelectedGoods {
    if (self.dataSourceMArr.count == 0) {
        [NotifyHelper showMessageWithMakeText:@"购物车暂无商品"];
        return;
    }
    NSArray *tempArr = [self selectedGoods];
    if (tempArr.count == 0) {
        [NotifyHelper showMessageWithMakeText:@"请选择商品"];
        return;
    }
    NSArray *tempIDArr = [tempArr valueForKeyPath:@"ID"];
    [MXAlertViewHelper showAlertViewWithMessage:@"删除选中商品?" completion:^(BOOL cancelled, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [self request:@"api/freeshop/cart/delUserGoodsCartGoods"
                    param:@{@"cart_id_list":[tempIDArr componentsJoinedByString:@","]?:@""}
               completion:^(BOOL success, id object, NSString *error) {
                if (success) {
                    [self deleteGoods:tempArr];
                    [self reloadTableView];
                }
            }];
        }
    }];
}

#pragma mark 全选/反选
- (void)allSelectClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    [self setAllGoodsSelected:sender.selected];
}

- (void)reloadTableView {
    [self.tableView reloadData];
    if (self.dataSourceMArr.count == 0) {
        self.tableView.tableHeaderView = self.nullHeaderView;
//        self.tableView.tableFooterView = self.footerView;
    } else {
        self.tableView.tableHeaderView = self.headerView;
//        self.tableView.tableFooterView = [UIView new];
    }
    [self configureBuyView];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = CartGoodsListObjCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    BuyGoodsListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    if (![cell isKindOfClass:CartGoodsListObjCell.class]) {
        return;
    }
    CartGoodsListObjCell *listCell = (CartGoodsListObjCell *)cell;
    [listCell configureView:obj];
    if (indexPath.row == self.dataSourceMArr.count - 1) {
        listCell.line.hidden = YES;
    } else {
        listCell.line.hidden = NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    BuyGoodsListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    MXRoute(@"GoodInfoVC", @{@"goods_id":obj.goods_id})
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.dataSourceMArr.count == 0) {
        return 0.01;
    } else {
        return 15;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (self.dataSourceMArr.count == 0) {
        return 0.01;
    } else {
        return 15;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.dataSourceMArr.count == 0) {
        return nil;
    }
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = CGRectMake(10, 5, SCREEN_WIDTH - 20, 10);
    [bgView setBorderWithCornerRadius:5 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 15);
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:bgView];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (self.dataSourceMArr.count == 0) {
        return nil;
    }
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = CGRectMake(10, 0, SCREEN_WIDTH - 20, 10);
    [bgView setBorderWithCornerRadius:5 byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight];
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 15);
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:bgView];
    return view;
}

#pragma mark - InitUI
- (void)createUI {
    self.last_id = @"";
    [self setNavBarTitle:@"购物车"];
    self.view.backgroundColor = [UIColor moBackground];
//    [self setNavBarRightBtnWithTitle:@"编辑" andImageName:nil];
    [self.view addSubview:self.tableView];
    UIView *buyView = [UIView addBottomSafeAreaWithContentView:self.buyView];
    buyView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:buyView];

    [buyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(buyView.height);
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view);
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.buyView.mas_top);
    }];
    
    UIBarButtonItem *clearBtnItem = [[UIBarButtonItem alloc] initWithCustomView:self.clearBtn];
    UIBarButtonItem *editBtnItem = [[UIBarButtonItem alloc] initWithCustomView:self.editBtn];
    self.navigationItem.rightBarButtonItems = @[editBtnItem, clearBtnItem];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        AdjustTableBehavior(_tableView);
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.tableFooterView = self.footerView;
        _tableView.rowHeight = [CartGoodsListObjCell viewHeight];
        _tableView.sectionFooterHeight = 0.01;
        Class cls = [CartGoodsListObjCell class];
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.pageIndex = 0;
            self.last_id = @"";
            [self fetchData];
        }];
//        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
//            @strongify(self);
//            self.pageIndex++;
//            [self fetchData];
//        }];
    }
    return _tableView;
}

- (BuyNullHeaderView *)nullHeaderView {
    if (!_nullHeaderView) {
        _nullHeaderView = [BuyNullHeaderView new];
        _nullHeaderView.frame = CGRectMake(0, 0, SCREEN_WIDTH, [BuyNullHeaderView viewHeight]);
    }
    return _nullHeaderView;
}


- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (BuyFooterView *)footerView {
    if (!_footerView) {
        _footerView = [BuyFooterView new];
    }
    return _footerView;
}


- (BuyHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [BuyHeaderView new];
        _headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, [BuyHeaderView viewHeight]);
        [_headerView configureView:@""];
    }
    return _headerView;
}

- (SPButton *)clearBtn {
    if (!_clearBtn) {
        _clearBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionLeft;
            object.imageTitleSpace = 5;
            [object setTitleColor:[UIColor colorWithHexString:@"#7F7F8E"] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font12];
            [object setImage:[UIImage imageNamed:@"清除失效产品"] forState:UIControlStateNormal];
            [object setTitle:@"清除失效商品" forState:UIControlStateNormal];
            [object sizeToFit];
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                [self clearBtnClick:btn];
            }];
            object;
        });
    }
    return _clearBtn;
}

- (BuySettlementView *)buyView {
    if (!_buyView) {
        _buyView = [BuySettlementView new];
        _buyView.height = [BuySettlementView viewHeight];
        _buyView.viewType = BuySettlementViewTypeNormal;
        @weakify(self)
        [_buyView.selectBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self allSelectClick:btn];
        }];
        [_buyView.buyBtn addAction:^(UIButton *btn) {
            @strongify(self)
            if ([self valiParam]) {
                [self createTempOrder];
            }
        }];
    }
    return _buyView;
}

- (UIButton *)editBtn {
    if (!_editBtn) {
        _editBtn = [UIButton new];
        [_editBtn setTitle:@"编辑" forState:UIControlStateNormal];
        _editBtn.titleLabel.font = [UIFont font14];
        [_editBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_editBtn setTitleColor:[UIColor moDarkGray] forState:UIControlStateDisabled];
        [_editBtn addTarget:self action:@selector(navBarRightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_editBtn sizeToFit];
    }
    
    return _editBtn;
}

@end
