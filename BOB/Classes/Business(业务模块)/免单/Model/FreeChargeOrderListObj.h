//
//  FreeChargeOrderListObj.h
//  BOB
//
//  Created by colin on 2020/11/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface FreeChargeOrderListObj : BaseObject

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *nick_name;
@property (nonatomic, copy) NSString *goods_name;
@property (nonatomic, copy) NSString *des;

+ (NSArray *)freeChargeOrderListObj;

@end

NS_ASSUME_NONNULL_END
