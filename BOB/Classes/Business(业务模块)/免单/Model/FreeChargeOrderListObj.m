//
//  FreeChargeOrderListObj.m
//  BOB
//
//  Created by colin on 2020/11/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FreeChargeOrderListObj.h"

@implementation FreeChargeOrderListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

- (NSString *)nick_name {
    return [StringUtil anonymizationName:_nick_name];
}

- (NSString *)des {
    if (!_des) {
        _des = StrF(@"恭喜 %@ 获得免单商品 %@", self.nick_name, self.goods_name);
    }
    return _des;
}

+ (NSArray *)freeChargeOrderListObj {
    FreeChargeOrderListObj *obj = [FreeChargeOrderListObj new];
    obj.ID = @"";
    obj.nick_name = @"人***里";
    obj.goods_name = @"阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋";
    
    
    FreeChargeOrderListObj *obj1 = [FreeChargeOrderListObj new];
    obj1.ID = @"";
    obj1.nick_name = @"人***里1";
    obj1.goods_name = @"阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋";
    
    
    FreeChargeOrderListObj *obj2 = [FreeChargeOrderListObj new];
    obj2.ID = @"";
    obj2.nick_name = @"人***里2";
    obj2.goods_name = @"阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋";
    
    FreeChargeOrderListObj *obj3 = [FreeChargeOrderListObj new];
    obj3.ID = @"";
    obj3.nick_name = @"人***里3fewfewfewfewfokokkokkopkofwkeofkweofew";
    obj3.goods_name = @"阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋";
    
    FreeChargeOrderListObj *obj4 = [FreeChargeOrderListObj new];
    obj4.ID = @"";
    obj4.nick_name = @"人***里4fewfewfewfewfewfewfew";
    obj4.goods_name = @"阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋";
    
    FreeChargeOrderListObj *obj5 = [FreeChargeOrderListObj new];
    obj5.ID = @"";
    obj5.nick_name = @"人***里5";
    obj5.goods_name = @"阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋阿迪达斯篮球鞋";

    return @[obj, obj1, obj2, obj3, obj4, obj5];
}

@end
