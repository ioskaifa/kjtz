//
//  HotKeywordListObj.h
//  BOB
//
//  Created by colin on 2020/11/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface HotKeywordListObj : BaseObject

@property (nonatomic, copy) NSString *key;

@property (nonatomic, assign) CGSize itemSize;

@end

NS_ASSUME_NONNULL_END
