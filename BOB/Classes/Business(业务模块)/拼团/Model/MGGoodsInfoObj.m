//
//  MGGoodsInfoObj.m
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGGoodsInfoObj.h"

@implementation MGGoodsInfoObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID":@"id",
        @"goods_sla_price":@"spell_sla_num",
             @"cash_min":@"goods_market_cash",
             @"goods_coupon_price":@"spell_cash_coupon",
    };
}

- (NSArray *)goods_describeArr {
    if (!_goods_describeArr) {
        NSArray *tempArr = [self.goods_describe componentsSeparatedByString:@","];
        NSMutableArray *dataMArr = [NSMutableArray arrayWithCapacity:tempArr.count];
        for (NSString *string in tempArr) {
            NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, string);
            [dataMArr addObject:imgUrl];
        }
        _goods_describeArr = dataMArr;
    }
    return _goods_describeArr;
}

- (NSArray *)goods_navigationArr {
    if (!_goods_navigationArr) {
        NSArray *tempArr = [self.goods_navigation componentsSeparatedByString:@","];
        NSMutableArray *dataMArr = [NSMutableArray arrayWithCapacity:tempArr.count];
        for (NSString *string in tempArr) {
            NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, string);
            [dataMArr addObject:imgUrl];
        }
        _goods_navigationArr = dataMArr;
    }
    return _goods_navigationArr;
}

- (NSString *)goods_name {
    return StrF(@".         %@", _goods_name);
}

- (NSAttributedString *)couponPriceAtt {
    if (!_couponPriceAtt) {
        CGFloat price = self.goods_coupon_price;
        NSString *priceString = StrF(@"%0.2f", price);
        NSArray *tempArr = [priceString componentsSeparatedByString:@"."];
        NSString *integer = tempArr.firstObject;
        NSString *decimal = StrF(@".%@ ", tempArr.lastObject);
        NSArray *txtArr = @[@"抵扣券￥", integer, decimal, self.cash_min_string];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#FF4757"], [UIColor colorWithHexString:@"#FF4757"], [UIColor colorWithHexString:@"#FF4757"], [UIColor blackColor]];
        NSArray *fontArr = @[[UIFont font14], [UIFont boldSystemFontOfSize:33], [UIFont boldFont16], [UIFont font13]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        _couponPriceAtt = att;
    }
    return _couponPriceAtt;
}

- (NSString *)cash_min_string {
    return StrF(@" 原价￥%0.2f", self.cash_min);
}

@end
