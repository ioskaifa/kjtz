//
//  MGGoodsInfoObj.h
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGGoodsInfoObj : BaseObject

@property (nonatomic, copy) NSString *ID;
///轮播图
@property (nonatomic , copy) NSString              * goods_navigation;
///分类编号
@property (nonatomic , copy) NSString              *category_id;
///商品名称
@property (nonatomic , copy) NSString              * goods_name;
///商品详情图 多张图以英文逗号拼接的
@property (nonatomic , copy) NSString              * goods_describe;
///商品价格
@property (nonatomic , assign) CGFloat              cash_min;
///商品抵扣券价格
@property (nonatomic , assign) CGFloat              goods_coupon_price;
///SLA 价格
@property (nonatomic , assign) CGFloat              goods_sla_price;
///拼团进度
@property (nonatomic , assign) CGFloat        progress;
///拼团规则
@property (nonatomic, copy) NSString *spell_rule;
///：上架日期
@property (nonatomic, copy) NSString *shelves_date;
///：上架时间
@property (nonatomic, copy) NSString *shelves_time;
///系统时间
@property (nonatomic, assign) NSTimeInterval sys_time;
///商品详情图 多张图以英文逗号拼接的
@property (nonatomic , strong) NSArray              * goods_describeArr;
///轮播图
@property (nonatomic , strong) NSArray              * goods_navigationArr;

@property (nonatomic, copy) NSAttributedString *couponPriceAtt;

@property (nonatomic, copy) NSString *cash_min_string;

@end

NS_ASSUME_NONNULL_END
