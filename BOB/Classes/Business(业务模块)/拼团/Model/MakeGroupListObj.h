//
//  MakeGroupListObj.h
//  BOB
//
//  Created by colin on 2020/12/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MGOrderStatus) {
    ///null
    MGOrderStatusNull     = -1,
    ///全部
    MGOrderStatusAll      = 0,
    ///待开奖
    MGOrderStatusWait     = 1,
    ///未中奖
    MGOrderStatusNot      = 2,
    ///中奖了
    MGOrderStatusWin      = 3,
    ///拼团失败
    MGOrderStatusFail     = 4,
    ///已开奖
    MGOrderStatusEnd      = 20,
    ///疯抢中
    MGOrderStatusIng      = 21,
    ///订单详情
    MGOrderStatusInfo     = 22,
};

@interface MakeGroupListObj : BaseObject

///订单状态 00-待开奖  08-拼团失败 09-已开奖
@property (nonatomic, copy) NSString *status;

@property (nonatomic, assign) MGOrderStatus orderStatus;
@property (nonatomic, copy) NSString *ID;
///商品ID
@property (nonatomic, copy) NSString *goodsID;
@property (nonatomic, copy) NSString *goods_id;
///商品名称
@property (nonatomic, copy) NSString *title;
///价格
@property (nonatomic, copy) NSString *price;
///图片
@property (nonatomic, copy) NSString *photo;
///抵扣券价格
@property (nonatomic, copy) NSString *couponPrice;

@property (nonatomic, assign) CGFloat payPrice;

@property (nonatomic, assign) BOOL is_win;

@property (nonatomic, copy) NSString *order_no;
///：创建日期
@property (nonatomic, copy) NSString *cre_date;
///创建时间
@property (nonatomic, copy) NSString *cre_time;
@property (nonatomic, copy) NSString *format_cre_time;
///拼团未中奖返还比例
@property (nonatomic, copy) NSString *spell_ben_rate;

@property (nonatomic, copy) NSString *goods_no;

@property (nonatomic, copy) NSString *pay_sla_num;

@property (nonatomic, copy) NSString *refund_sla_num;
///抵扣券状态 00-待使用 04-已失效 09-已使用
@property (nonatomic, copy) NSString *coupon_status;
@property (nonatomic, copy) NSString *coupon_status_string;

///系统时间
@property (nonatomic, assign) NSTimeInterval sys_time;
///：过期时间
@property (nonatomic, assign) NSInteger over_time;
///：抵扣券创建日期
@property (nonatomic, copy) NSString *win_date;
///：抵扣券创建时间
@property (nonatomic, copy) NSString *win_time;
///剩余倒计时
@property (nonatomic, assign) NSInteger countDownNum;

@property (nonatomic, copy) NSAttributedString *couponPriceAtt;
@property (nonatomic, copy) NSAttributedString *couponPriceExAtt;

@property (nonatomic, copy) NSAttributedString *refund_sla_numAtt;

@property (nonatomic, copy) NSString *formatStatus;

@property (nonatomic, copy) NSString *win_order_no;

+ (NSArray *)makeGroupListObj;

+ (NSString *)orderStatusToString:(MGOrderStatus)status;

+ (NSString *)orderStatusToDescription:(MGOrderStatus)orderStatus;

- (void)calcCountDown;

@end

NS_ASSUME_NONNULL_END
