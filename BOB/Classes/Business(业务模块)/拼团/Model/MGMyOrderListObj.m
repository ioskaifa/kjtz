//
//  MGMyOrderListObj.m
//  BOB
//
//  Created by colin on 2020/12/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGMyOrderListObj.h"

@implementation MGMyOrderListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

- (NSString *)goods_name {
    return StrF(@".        %@", _goods_name);
}

- (NSString *)goods_show {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _goods_show);
}

- (MGGoodsOrderStatus)orderStatus {
    if ([@"00" isEqualToString:self.status]) {
        return MGGoodsOrderStatusToPay;
    } else if ([@"02" isEqualToString:self.status]) {
        return MGGoodsOrderStatusToDelive;
    } else if ([@"03" isEqualToString:self.status]) {
        return MGGoodsOrderStatusToReceive;
    } else if ([@"04" isEqualToString:self.status]) {
        return MGGoodsOrderStatusToCancel;
    } else if ([@"05" isEqualToString:self.status]) {
        return MGGoodsOrderStatusToRefund;
    } else if ([@"08" isEqualToString:self.status]) {
        return MGGoodsOrderStatusToFrozen;
    } else if ([@"09" isEqualToString:self.status]) {
        return MGGoodsOrderStatusToComplete;
    } else {
        return MGGoodsOrderStatusToCancel;
    }
}

- (NSString *)format_refund_status {
    if (self.orderStatus != MGGoodsOrderStatusToRefund) {
        return @"- -";
    }
    NSString *string = @"- -";
    if ([@"00" isEqualToString:self.refund_status]) {
        string = @"退款待审核";
    } else if ([@"04" isEqualToString:self.refund_status]) {
        string = @"退款中";
    } else if ([@"07" isEqualToString:self.refund_status]) {
        string = @"退款失败";
    } else if ([@"08" isEqualToString:self.refund_status]) {
        string = @"退款拒绝";
    } else if ([@"09" isEqualToString:self.refund_status]) {
        string = @"退款成功";
    }
    return string;
}

+ (NSString *)orderStatusToString:(MGGoodsOrderStatus)status {
    NSString *string = @"";
    switch (status) {
        case MGGoodsOrderStatusToAll:{
            string = @"";
            break;
        }
        case MGGoodsOrderStatusToPay:{
            string = @"00";
            break;
        }
        case MGGoodsOrderStatusToDelive:{
            string = @"02";
            break;
        }
        case MGGoodsOrderStatusToReceive:{
            string = @"03";
            break;
        }
        case MGGoodsOrderStatusToComplete:{
            string = @"09";
            break;
        }
        case MGGoodsOrderStatusToCancel:{
            string = @"04";
            break;
        }
        case MGGoodsOrderStatusToRefund:{
            string = @"05";
            break;
        }
        default:
            string = @"";
            break;
    }
    return string;
}

- (NSString *)formatStatus {
    NSString *string = @"";
    switch (self.orderStatus) {
        case MGGoodsOrderStatusToDelive:{
            string = @"待发货";
            break;
        }
        case MGGoodsOrderStatusToPay:{
            string = @"待付款";
            break;
        }
        case MGGoodsOrderStatusToReceive:{
            string = @"待收货";
            break;
        }
        case MGGoodsOrderStatusToComplete:{
            string = @"已完成";
            break;
        }
        case MGGoodsOrderStatusToCancel:{
            string = @"已取消";
            break;
        }
        case MGGoodsOrderStatusToRefund:{
            string = self.format_refund_status;
            break;
        }
        case MGGoodsOrderStatusToFrozen:{
            string = @"已冻结";
            break;
        }
        default:
            string = @"已取消";
            break;
    }
    return string;
}

- (NSAttributedString *)priceAtt {
    if (!_priceAtt) {
        NSArray *txtArr = @[StrF(@"￥%0.2f\n", self.goods_market_cash), @"x1"];
        NSArray *fontArr = @[[UIFont boldFont15], [UIFont font14]];
        NSArray *colorArr = @[[UIColor blackColor], [UIColor colorWithHexString:@"#AAAABB"]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                     colors:colorArr
                                                                      fonts:fontArr];
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        paragraphStyle.alignment = NSTextAlignmentRight;
        NSRange range = [att.string rangeOfString:att.string];
        [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
        _priceAtt = att;
    }
    return _priceAtt;
}

- (NSString *)couponString {
    if (!_couponString) {
        _couponString = StrF(@"抵扣券-%ld", (long)self.spell_cash_coupon);
    }
    return _couponString;
}

- (NSAttributedString *)totalAtt {
    if (!_totalAtt) {
        NSArray *txtArr = @[@"共1件商品，合计：", StrF(@"￥%0.2f", self.total_cash_num)];
        NSArray *fontArr = @[[UIFont font13], [UIFont boldFont15]];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#AAAABB"], [UIColor blackColor]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                     colors:colorArr
                                                                      fonts:fontArr];
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        paragraphStyle.alignment = NSTextAlignmentRight;
        NSRange range = [att.string rangeOfString:att.string];
        [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
        
        _totalAtt = att;
    }
    return _totalAtt;
}

@end
