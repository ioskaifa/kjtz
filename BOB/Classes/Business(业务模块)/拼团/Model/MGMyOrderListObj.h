//
//  MGMyOrderListObj.h
//  BOB
//
//  Created by colin on 2020/12/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MGGoodsOrderStatus) {
    ///全部
    MGGoodsOrderStatusToAll      = 0,
    ///待付款
    MGGoodsOrderStatusToPay      = 1,
    ///待发货
    MGGoodsOrderStatusToDelive   = 2,
    ///待收货
    MGGoodsOrderStatusToReceive  = 3,
    ///已完成
    MGGoodsOrderStatusToComplete = 4,
    ///退款
    MGGoodsOrderStatusToRefund   = 5,
    ///已取消
    MGGoodsOrderStatusToCancel   = 6,
    ///已冻结
    MGGoodsOrderStatusToFrozen   = 7,
};

@interface MGMyOrderListObj : BaseObject

///订单状态
@property (nonatomic, assign) MGGoodsOrderStatus orderStatus;

@property (nonatomic , copy) NSString              * goods_name;
@property (nonatomic , copy) NSString              *goods_id;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , assign) NSInteger              express_cash;
@property (nonatomic , assign) CGFloat              goods_market_cash;
@property (nonatomic , assign) CGFloat              cash_num;
@property (nonatomic , assign) NSInteger              discount_cash_num;
@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , copy) NSString              *user_id;
@property (nonatomic , copy) NSString              *goods_num;
@property (nonatomic , copy) NSString              *ID;
@property (nonatomic , copy) NSString              * order_id;
@property (nonatomic , assign) CGFloat              total_cash_num;
@property (nonatomic , copy) NSString              * goods_show;
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , assign) NSInteger              spell_cash_coupon;
@property (nonatomic , copy) NSString              *formatStatus;
@property (nonatomic , copy) NSAttributedString              *priceAtt;
@property (nonatomic , copy) NSString              *couponString;
@property (nonatomic , copy) NSAttributedString              *totalAtt;

@property (nonatomic, copy) NSString *refund_status;
@property (nonatomic, copy) NSString *format_refund_status;

+ (NSString *)orderStatusToString:(MGGoodsOrderStatus)status;

@end

NS_ASSUME_NONNULL_END
