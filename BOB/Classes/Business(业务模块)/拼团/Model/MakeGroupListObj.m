//
//  MakeGroupListObj.m
//  BOB
//
//  Created by colin on 2020/12/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MakeGroupListObj.h"
#import "NSString+DateFormatter.h"

@implementation MakeGroupListObj

- (instancetype)init {
    self = [super init];
    if (self) {
        self.orderStatus = MGOrderStatusNull;
    }
    return self;
}

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"order_num",
             @"title" : @"goods_name",
             @"price":@"goods_market_cash",
             @"photo":@"goods_show",
             @"payPrice":@"spell_sla_num",
             @"couponPrice":@"spell_cash_coupon",
             @"goodsID":@"id"
    };
}

- (NSString *)photo {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _photo);
}

- (NSString *)title {
    return StrF(@".          %@", _title);
}

- (NSString *)price {
    CGFloat value = [_price floatValue];
    return StrF(@"￥%0.2f", value);
}

- (NSAttributedString *)couponPriceAtt {
    if (!_couponPriceAtt) {
        CGFloat price = [self.couponPrice floatValue];
        NSString *string = StrF(@"%0.2f", price);
        NSArray *txtArr = @[@"￥", string, @" 抵扣券"];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#FF4757"], [UIColor colorWithHexString:@"#FF4757"], [UIColor colorWithHexString:@"#FF4757"]];
        NSArray *fontArr = @[[UIFont font11], [UIFont boldFont20], [UIFont font11]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        _couponPriceAtt = att;
    }
    return _couponPriceAtt;
}

- (NSAttributedString *)couponPriceExAtt {
    if (!_couponPriceExAtt) {
        NSArray *txtArr = @[@"￥", self.couponPrice];
        NSArray *colorArr = @[[UIColor whiteColor], [UIColor whiteColor]];
        NSArray *fontArr = @[[UIFont font15], [UIFont boldFont30]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        _couponPriceExAtt = att;
    }
    return _couponPriceExAtt;
}

- (MGOrderStatus)orderStatus {
    if (_orderStatus != MGOrderStatusNull) {
        return _orderStatus;
    }
    if ([@"00" isEqualToString:self.status]) {
        return MGOrderStatusWait;
    } else if ([@"09" isEqualToString:self.status] && !self.is_win) {
        return MGOrderStatusNot;
    } else if ([@"09" isEqualToString:self.status] && self.is_win) {
        return MGOrderStatusWin;
    } else if ([@"08" isEqualToString:self.status]) {
        return MGOrderStatusFail;
    } else if ([@"20" isEqualToString:self.status]) {
        return MGOrderStatusEnd;
    } else if ([@"21" isEqualToString:self.status]) {
        return MGOrderStatusIng;
    } else if ([@"22" isEqualToString:self.status]) {
        return MGOrderStatusInfo;
    } else {
        return MGOrderStatusInfo;
    }
}

- (NSString *)formatStatus {
    return [self.class orderStatusToDescription:self.orderStatus];
}

+ (NSString *)orderStatusToDescription:(MGOrderStatus)orderStatus {
    NSString *string = @"";
    switch (orderStatus) {
        case MGOrderStatusWait:{
            string = @"拼团中";
            break;
        }
        case MGOrderStatusNot:{
            string = @"未中奖";
            break;
        }
        case MGOrderStatusWin:{
            string = @"已中奖";
            break;
        }
        case MGOrderStatusFail:{
            string = @"拼团失败";
            break;
        }
        default:
            string = @"待开奖";
            break;
    }
    return string;
}

+ (NSString *)orderStatusToString:(MGOrderStatus)status {
    NSString *string = @"";
    switch (status) {
        case MGOrderStatusAll:{
            string = @"";
            break;
        }
        case MGOrderStatusWait:{
            string = @"01";
            break;
        }
        case MGOrderStatusNot:{
            string = @"02";
            break;
        }
        case MGOrderStatusWin:{
            string = @"03";
            break;
        }
        case MGOrderStatusFail:{
            string = @"04";
            break;
        }
        default:
            string = @"";
            break;
    }
    return string;
}

- (NSString *)format_cre_time {
    return StrF(@"%@ %@", [StringUtil formatDayString:self.cre_date unit:@"-"], [StringUtil formatTimeStringmm:self.cre_time]);
}

- (NSString *)pay_sla_num {
    CGFloat value = [_pay_sla_num floatValue];
    return StrF(@"%0.4f", value);
}

- (NSString *)refund_sla_num {
    CGFloat value = [_refund_sla_num floatValue];
    return StrF(@"%0.4f", value);
}

- (NSString *)spell_ben_rate {
    return StrF(@"%@%@", _spell_ben_rate, @"%");
}

- (NSAttributedString *)refund_sla_numAtt {
    if (!_refund_sla_numAtt) {
        NSArray *txtArr = @[StrF(@"SLA %@ ", self.refund_sla_num), @"已退还"];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#FF4757"], [UIColor colorWithHexString:@"#FF4757"]];
        NSArray *fontArr = @[[UIFont font13], [UIFont font10]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        _refund_sla_numAtt = att;
    }
    return _refund_sla_numAtt;
}

- (NSString *)coupon_status_string {
    if ([@"00" isEqualToString:self.coupon_status]) {
        return @"未使用";
    } else if ([@"04" isEqualToString:self.coupon_status]) {
        return @"已过期";
    } else {
        return @"已使用";
    }
}

- (void)calcCountDown {
    NSString *createTimeString = StrF(@"%@%@", self.win_date, self.win_time);
    NSTimeInterval createTime = [createTimeString yyyyMMddHHmmssToTimeInterval];
    NSTimeInterval diff = self.sys_time - createTime;
    //剩余有效时间
    diff = self.over_time - diff;
    if (diff <= 0) {
        diff = 0;
    }
    self.countDownNum = diff;
}

+ (NSArray *)makeGroupListObj {
    MakeGroupListObj *obj = [MakeGroupListObj new];
    obj.title = @"         DanielWellington丹尼尔惠灵顿28mm女士简约气质时分为非威锋网飞飞飞纷纷为范围";
    obj.price = @"3000";
    obj.couponPrice = @"100";
    obj.payPrice = 100;
    obj.orderStatus = MGOrderStatusWait;
    
    MakeGroupListObj *obj1 = [MakeGroupListObj new];
    obj1.title = @"         DanielWellington丹尼尔惠灵顿28mm女士简约气质时分为非威锋网飞飞飞纷纷为范围";
    obj1.price = @"3001";
    obj1.couponPrice = @"100";
    obj1.payPrice = 100;
    obj1.orderStatus = MGOrderStatusNot;
    
    MakeGroupListObj *obj2 = [MakeGroupListObj new];
    obj2.title = @"         DanielWellington丹尼尔惠灵顿28mm女士简约气质时分为非威锋网飞飞飞纷纷为范围";
    obj2.price = @"3002";
    obj2.couponPrice = @"100";
    obj2.payPrice = 100;
    obj2.orderStatus = MGOrderStatusWin;
    
    MakeGroupListObj *obj3 = [MakeGroupListObj new];
    obj3.title = @"         DanielWellington丹尼尔惠灵顿28mm女士简约气质时分为非威锋网飞飞飞纷纷为范围";
    obj3.price = @"3003";
    obj3.couponPrice = @"100";
    obj3.payPrice = 100;
    obj3.orderStatus = MGOrderStatusFail;
    
    return @[obj, obj1, obj2, obj3];
}

@end
