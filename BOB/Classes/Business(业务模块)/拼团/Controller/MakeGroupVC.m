//
//  MakeGroupVC.m
//  BOB
//
//  Created by colin on 2020/12/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MakeGroupVC.h"
#import "GKCycleScrollView.h"
#import "MakeGroupSectionView.h"
#import "MakeGroupListCell.h"

@interface MakeGroupVC ()<GKCycleScrollViewDataSource, GKCycleScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, MakeGroupSectionViewDelegate>

///轮播图
@property (nonatomic, strong) GKCycleScrollView *cycleScrollView;
///轮播数据源
@property (nonatomic, strong) NSArray *cycleDataArr;

@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, copy) NSString *last_id;
///是否已开奖
@property (nonatomic, assign) NSInteger selectIndex;

@end

@implementation MakeGroupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchAdvertiseData];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)navBarRightBtnAction:(id)sender {
    MXRoute(@"MGMyOrderVC", nil)
}

- (void)fetchAdvertiseData {
    [self request:@"api/system/appImg/getAppImgList"
            param:@{@"img_type":@"02"}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *dataArr = dataDic[@"data"][@"appImgList"];
            self.cycleDataArr = dataArr;
            [self.cycleScrollView reloadData];
            if (dataArr.count > 0) {
                self.tableView.tableHeaderView = self.cycleScrollView;
            }
        }
    }];
}

- (void)fetchData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    NSString *api = @"api/spell/goods/getFinishedSpellGoodsList";
    if (self.selectIndex == 1) {
        api = @"api/spell/goods/getRushPurchaseSpellGoodsList";
    }
    api = @"api/spell/goods/getRushPurchaseSpellGoodsList";
    [self request:api
            param:@{@"last_id":self.last_id,}
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *tempArr = dataDic[@"data"][@"spellGoodsList"];
            NSArray *dataArr = [MakeGroupListObj modelListParseWithArray:tempArr];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
//            [self.dataSourceMArr addObjectsFromArray:[MakeGroupListObj makeGroupListObj]];
            MakeGroupListObj *obj = self.dataSourceMArr.lastObject;
            if ([obj isKindOfClass:MakeGroupListObj.class]) {
                self.last_id = obj.ID;
            }
            [self.tableView reloadData];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - Delegate
#pragma mark GKCycleScrollViewDataSource
- (NSInteger)numberOfCellsInCycleScrollView:(GKCycleScrollView *)cycleScrollView {
    return self.cycleDataArr.count;
}

- (GKCycleScrollViewCell *)cycleScrollView:(GKCycleScrollView *)cycleScrollView cellForViewAtIndex:(NSInteger)index {
    GKCycleScrollViewCell *cell = [cycleScrollView dequeueReusableCell];
    if (!cell) {
        cell = [GKCycleScrollViewCell new];
    }
    if (index < self.cycleDataArr.count) {
        NSDictionary *dict = self.cycleDataArr[index];
        NSString *img_url = dict[@"img_url"];
        if ([img_url isKindOfClass:NSString.class]) {
            img_url = StrF(@"%@/%@", UDetail.user.qiniu_domain, img_url);
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                
            }];
        }
    }
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    return cell;
}

#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    Class cls = MakeGroupSectionView.class;
//    MakeGroupSectionView *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass(cls)];
//    sectionView.delegate = self;
//    return sectionView;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return [MakeGroupSectionView viewHeight];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MakeGroupListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:MakeGroupListCell.class]) {
        return;
    }
    MakeGroupListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MakeGroupListObj.class]) {
        return;
    }
    obj.status = @"20";
    if (self.selectIndex == 1) {
        obj.status = @"21";
    }
    obj.status = @"21";
    MakeGroupListCell *listCell = (MakeGroupListCell *)cell;
    [listCell configureView:obj];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    MakeGroupListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MakeGroupListObj.class]) {
        return;
    }
    MXRoute(@"MGGoodInfoVC", (@{@"goods_id":obj.goodsID, @"orderStatus":@(obj.orderStatus)}))
}

#pragma mark - MakeGroupSectionViewDelegate
- (void)MakeGroupView:(MakeGroupSectionView *)view didSelectCellAtIndex:(NSInteger)index {
    if (self.selectIndex == index) {
        return;
    }
    self.selectIndex = index;
    self.last_id = @"";
    [self.dataSourceMArr removeAllObjects];
    [self.tableView reloadData];
    [self fetchData];
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"拼团商城"];
    [self setNavBarRightBtnWithTitle:@"我的拼团" andImageName:nil];
    self.selectIndex = 5;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (GKCycleScrollView *)cycleScrollView {
    if (!_cycleScrollView) {
        _cycleScrollView.height = 150;
        _cycleScrollView = [[GKCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        _cycleScrollView.pageControl = self.pageControl;
        _cycleScrollView.isAutoScroll = YES;
        _cycleScrollView.dataSource = self;
        _cycleScrollView.delegate = self;
        _cycleScrollView.myTop = 15;
        _cycleScrollView.myLeft = 15;
        _cycleScrollView.myRight = 15;
        _cycleScrollView.myBottom = 15;
        _cycleScrollView.myHeight = _cycleScrollView.height;
        [_cycleScrollView addSubview:self.pageControl];
        _cycleScrollView.backgroundColor = [UIColor moBackground];
    }
    return _cycleScrollView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [UIPageControl new];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.numberOfPages = self.dataSourceMArr.count;
        _pageControl.frame = CGRectMake(20, self.cycleScrollView.height - 20, SCREEN_WIDTH - 40 - 30, 20);
    }
    return _pageControl;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [MakeGroupListCell viewHeight];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        Class cls = MakeGroupSectionView.class;
        [_tableView registerClass:cls forHeaderFooterViewReuseIdentifier:NSStringFromClass(cls)];
        cls = MakeGroupListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

@end
