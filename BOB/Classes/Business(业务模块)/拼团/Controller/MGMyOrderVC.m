//
//  MGMyOrderVC.m
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGMyOrderVC.h"
#import "MyOrderNullView.h"
#import "YJSliderView.h"
#import "MGMyOrderListCell.h"
#import "MGWinListCell.h"

@interface MGMyOrderVC ()<UITableViewDelegate, UITableViewDataSource, YJSliderViewDelegate>

@property (nonatomic, assign) MGOrderStatus orderStatus;

@property (nonatomic, strong) MyOrderNullView *nullView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YJSliderView *sliderView;

@property (nonatomic, strong) NSArray *sliderDataSourceArr;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation MGMyOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.last_id = @"";
    [self fetchData];
}

#pragma mark - Private Method
- (void)fetchData {
    NSString *api = @"api/spell/part/getUserSpellPartInfoList";
    [self request:api
            param:@{@"last_id":self.last_id?:@"",
                    @"type":[MakeGroupListObj orderStatusToString:self.orderStatus]
            }
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            if ([@"" isEqualToString:self.last_id]) {
                [self.dataSourceMArr removeAllObjects];
            }
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *dataArr = [MakeGroupListObj modelListParseWithArray:dataDic[@"data"][@"userSpellPartList"]];
            for (MakeGroupListObj *obj in dataArr) {
                if (![obj isKindOfClass:MakeGroupListObj.class]) {
                    continue;
                }
                obj.sys_time = [dataDic[@"data"][@"sys_time"] integerValue];
                if (obj.orderStatus != MGOrderStatusWin) {
                    continue;
                }
                [obj calcCountDown];
            }
            [self.dataSourceMArr addObjectsFromArray:dataArr];
//            [self.dataSourceMArr addObjectsFromArray:[MakeGroupListObj makeGroupListObj]];
            MakeGroupListObj *obj = self.dataSourceMArr.lastObject;
            if ([obj isKindOfClass:MakeGroupListObj.class]) {
                self.last_id = obj.goodsID;
            }
            [self.tableView reloadData];
        }
        [self.tableView reloadData];
        if (self.dataSourceMArr.count == 0) {
            self.nullView.hidden = NO;
        } else {
            self.nullView.hidden = YES;
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)configureCountDownLbl {
    for (MakeGroupListObj *obj in self.dataSourceMArr) {
        if (![obj isKindOfClass:MakeGroupListObj.class]) {
            continue;
        }
        if (obj.orderStatus != MGOrderStatusWin) {
            continue;
        }
        if (obj.countDownNum == 0) {
            continue;
        }
        obj.countDownNum--;
    }
    NSArray *cellArr = [self.tableView visibleCells];
    @autoreleasepool {
        NSMutableArray *indexPathMArr = [NSMutableArray arrayWithCapacity:cellArr.count];
        for (UITableViewCell *cell in cellArr) {
            if (![cell isKindOfClass:MGWinListCell.class]) {
                continue;
            }
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            if (indexPath) {
                [indexPathMArr addObject:indexPath];
            }
        }
        if (indexPathMArr.count >= 0) {
            [self.tableView reloadRowsAtIndexPaths:indexPathMArr withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MGMyOrderListCell.class;
    if (indexPath.row < self.dataSourceMArr.count) {
        MakeGroupListObj *obj = self.dataSourceMArr[indexPath.row];
        if (obj.orderStatus == MGOrderStatusWin) {
            cls = MGWinListCell.class;
        }
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return 0;
    }
    MakeGroupListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MakeGroupListObj.class]) {
        return 0;
    }
    if (obj.orderStatus == MGOrderStatusWin) {
        return [MGWinListCell viewHeight];
    } else {
        return [MGMyOrderListCell viewHeight];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    MakeGroupListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MakeGroupListObj.class]) {
        return;
    }
    if (!([cell isKindOfClass:MGMyOrderListCell.class] ||
        [cell isKindOfClass:MGWinListCell.class])) {
        return;
    }
        
    if (obj.orderStatus == MGOrderStatusWin) {
        MGWinListCell *listCell = (MGWinListCell *)cell;
        [listCell configureView:obj];
    } else {
        MGMyOrderListCell *listCell = (MGMyOrderListCell *)cell;
        [listCell configureView:obj];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    MakeGroupListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MakeGroupListObj.class]) {
        return;
    }
    if (obj.orderStatus == MGOrderStatusWin) {
        MXRoute(@"MGGoodsOrderInfoVC", @{@"orderID":obj.win_order_no})
    } else {
        MXRoute(@"MGOrderInfoVC", @{@"part_id":obj.goodsID})
    }
}

#pragma mark YJSliderViewDelegate
- (NSInteger)numberOfItemsInYJSliderView:(YJSliderView *)sliderView {
    return self.sliderDataSourceArr.count;
}

- (UIView *)yj_SliderView:(YJSliderView *)sliderView viewForItemAtIndex:(NSInteger)index {
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (NSString *)yj_SliderView:(YJSliderView *)sliderView titleForItemAtIndex:(NSInteger)index {
    if (index >= self.sliderDataSourceArr.count) {
        return @"";
    }
    return self.sliderDataSourceArr[index];
}

- (NSInteger)initialzeIndexFoYJSliderView:(YJSliderView *)sliderView {
    return self.orderStatus;
}

- (void)yj_SliderView:(YJSliderView *)sliderView didSelectItemAtIndex:(NSIndexPath *)indexPath {
    self.orderStatus = indexPath.row;
    self.last_id = @"";
    [self fetchData];
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"我的拼团"];
    self.view.backgroundColor = [UIColor whiteColor];
    self.last_id = @"";
    self.orderStatus = MGOrderStatusAll;
    
    [self.view addSubview:self.sliderView];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.nullView];
        
    [self layoutUI];
    [self.timer setFireDate:[NSDate date]];
}

- (void)layoutUI {
    [self.sliderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sliderView.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sliderView.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(self.nullView.height);
    }];
}


#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        Class cls = MGMyOrderListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        cls = MGWinListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.tableFooterView = [UIView new];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (YJSliderView *)sliderView {
    if (!_sliderView) {
        _sliderView = [[YJSliderView alloc] initWithFrame:CGRectZero];
        _sliderView.delegate = self;
        _sliderView.themeColor = [UIColor colorWithHexString:@"#FF4757"];
        _sliderView.lineColor = [UIColor colorWithHexString:@"#FF4757"];
    }
    return _sliderView;
}

- (NSArray *)sliderDataSourceArr {
    if (!_sliderDataSourceArr) {
        _sliderDataSourceArr = @[@"全部", @"待开奖", @"未中奖", @"中奖了", @"拼团失败"];
    }
    return _sliderDataSourceArr;
}

- (MyOrderNullView *)nullView {
    if (!_nullView) {
        _nullView = [MyOrderNullView new];
        _nullView.size = CGSizeMake(SCREEN_WIDTH, [MyOrderNullView viewHeight]);
        _nullView.hidden = YES;
    }
    return _nullView;
}

- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(configureCountDownLbl) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        [_timer setFireDate:[NSDate distantFuture]];
    }
    return _timer;
}

@end
