//
//  MGGoodsOrderInfoVC.m
//  BOB
//
//  Created by colin on 2020/12/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGGoodsOrderInfoVC.h"
#import "OrderInfoAddressView.h"
#import "OrderInfoGoodsView.h"
#import "OrderInfoSubView.h"
#import "OrderInfoBottomView.h"
#import "OrderSelectPayView.h"
#import "HWPanModelVC.h"
#import "YQPayKeyWordVC.h"
#import "PolymerPayManager.h"
#import "NSString+DateFormatter.h"
#import "UserAddressListObj.h"
#import "ShipperInfoObj.h"
#import "ShipperView.h"

@interface MGGoodsOrderInfoVC ()

@property (nonatomic, strong) MyOrderListObj *obj;

@property (nonatomic, strong) UITableView *tableView;
///地址信息
@property (nonatomic, strong) OrderInfoAddressView *addressView;
///商品信息
@property (nonatomic, strong) OrderInfoGoodsView *goodsView;
///订单信息
@property (nonatomic, strong) OrderInfoSubView *infoView;

@property (nonatomic, strong) OrderInfoBottomView *bottomView;

@property (nonatomic, strong) UIButton *backBtn;

@property (nonatomic, strong) OrderSelectPayView *orderSelectPayView;

@property (nonatomic, strong) HWPanModelVC *panVC;

@property (nonatomic, strong) UILabel *titleLbl;
///倒计时
@property (nonatomic, strong) UILabel *countDownLbl;

@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, assign) NSInteger countDownNum;
///物流信息
@property (nonatomic, strong) ShipperView *shipperView;

@property (nonatomic, assign) CGFloat sla_price;
///sla支付开关 - 直正对免单商城
@property (nonatomic, assign) BOOL sla_pay_lock;
///超时取消订单时间（秒）
@property (nonatomic, assign) NSTimeInterval validityTime;

@end

@implementation MGGoodsOrderInfoVC

- (void)dealloc {
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData:self.orderID];
    [self fetchSlaPrice];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.view bringSubviewToFront:self.backBtn];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.timer invalidate];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSArray *vcArr = self.navigationController.viewControllers;
    NSMutableArray *vcMArr = [NSMutableArray arrayWithCapacity:vcArr.count];
    for (UIViewController *vc in vcArr) {
        if ([vc isKindOfClass:NSClassFromString(@"ConfirmOrderVC")]) {
            continue;
        }
        [vcMArr addObject:vc];
    }
    if (vcMArr.count != vcArr.count) {
        self.navigationController.viewControllers = vcMArr;
    }
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    if ([@"OrderInfoBottomViewCancel" isEqualToString:eventName]) {
        [MXAlertViewHelper showAlertViewWithMessage:@"是否取消该订单？" completion:^(BOOL cancelled, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self cancelOrder];
            }
        }];
    } else if ([@"OrderInfoBottomViewPay" isEqualToString:eventName]) {
        if ([StringUtil isEmpty:self.obj.userAddress]) {
            [NotifyHelper showMessageWithMakeText:@"请选择收货地址"];
            return;
        }
        [OrderSelectPayView checkSetPayPassword:^(id data) {
            [self.panVC show];
        }];
    } else if ([@"OrderInfoBottomViewConfirm" isEqualToString:eventName]) {
        [self confirmOrder];
    } else if ([@"OrderInfoBottomViewRefund" isEqualToString:eventName]) {
        [self refundOrder];
    } else {
     [super routerEventWithName:eventName userInfo:userInfo];
    }
}

#pragma mark - 申请退款
- (void)refundOrder {
    [MXAlertViewHelper showAlertViewWithTextFiled:@"退款原因" title:@"发起退款"
                                          okTitle:@"确定" cancelTitle:@"取消"
                                       completion:^(NSString *text, NSInteger buttonIndex) {
        NSString *refund_reason = text;
        if ([StringUtil isEmpty:refund_reason]) {
            [NotifyHelper showMessageWithMakeText:@"退款原因不能为空"];
            return;
        }
        NSString *api = @"/api/spell/order/applyOrderRefund";
        [self request:api
                param:@{@"order_id":self.obj.order_id?:@"",
                        @"refund_reason":refund_reason?:@""}
           completion:^(BOOL success, id object, NSString *error) {
            [NotifyHelper hideHUDForView:self.view animated:YES];
            if (success) {
                self.obj.status = @"05";
                [self configureView];
                [NotifyHelper showMessageWithMakeText:@"申请已提交"];
            } else {
                if ([StringUtil isEmpty:object[@"msg"]]) {
                    [NotifyHelper showMessageWithMakeText:error];
                } else {
                    [NotifyHelper showMessageWithMakeText:object[@"msg"]];
                }
            }
        }];
    }];
}

- (void)fetchData:(NSString *)orderID {
    NSString *api = @"api/spell/order/getOrderDetail";
    [self request:api
            param:@{@"order_id":orderID?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *goodDic = object[@"data"][@"orderInfo"];
            MyOrderListObj *obj = [MyOrderListObj modelParseWithDict:goodDic];
            obj.is_makeGroup = YES;
            self.validityTime = [goodDic[@"over_time"] integerValue];
            obj.sys_time = [object[@"data"][@"sys_time"] doubleValue];
            if (object[@"data"][@"sla_discount_rate"]) {
                obj.sla_discount_rate = [object[@"data"][@"sla_discount_rate"] floatValue];
            }
            BuyGoodsListObj *goodObj = [BuyGoodsListObj modelParseWithDict:goodDic];
            goodObj.photo = goodDic[@"goods_show"];
            goodObj.price = [goodDic[@"cash_num"] floatValue];
            obj.orderList = @[goodObj];
            self.obj = obj;
            self.sla_pay_lock = [goodDic[@"is_sla"] boolValue];
            [self initUI];
            if (![StringUtil isEmpty:obj.shipper_id]) {
                [self fetchShipperInfo:obj.order_id];
            }
        }
    }];
}

#pragma mark 获取物流信息
- (void)fetchShipperInfo:(NSString *)order_id {
    if ([StringUtil isEmpty:order_id]) {
        return;
    }
    NSString *api = @"api/spell/order/getOrderShipperInfo";
    [self request:api
            param:@{@"order_id":order_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            ShipperInfoObj *obj = [ShipperInfoObj modelParseWithDict:object[@"data"][@"shipperInfo"]];
            self.obj.shipperObj = obj;
            [self.shipperView configureView:obj.tracesArr.firstObject];
            self.shipperView.visibility = MyVisibility_Visible;
        }
    }];
}

#pragma mark  获取sla_price
- (void)fetchSlaPrice {
    NSString *url = @"api/spell/spellPay/getSlaPrice";
    [self request:url param:@{} completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object[@"data"];
            if (dataDic[@"sla_price"]) {
                self.sla_price = [dataDic[@"sla_price"] floatValue];
                self.orderSelectPayView.sla_price = self.sla_price;
            }
        }
    }];
}

#pragma mark - 提示信息
- (NSString *)tipString {
    NSString *string = @"";
    switch (self.obj.orderStatus) {
        case OrderStatusToDelive:{
            string = @"请耐心等待";
            break;
        }
        case OrderStatusToReceive:{
            string = @"请注意物流信息，确认收货";
            break;
        }
        default:
            break;
    }
    return string;
}

#pragma mark - 倒计时
- (void)configureCountDown:(MyOrderListObj *)obj {
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    if (obj.orderStatus != OrderStatusToPay) {
        return;
    }
    NSTimeInterval sysTime = obj.sys_time;
    NSString *createTimeString = StrF(@"%@%@", obj.cre_date, obj.cre_time);
    NSTimeInterval createTime = [createTimeString yyyyMMddHHmmssToTimeInterval];
    NSTimeInterval diff = sysTime - createTime;
    //剩余有效时间
    diff = self.validityTime - diff;
    if (diff <= 0) {
        return;
    }
    self.countDownNum = diff;
    [self.timer setFireDate:[NSDate date]];
}

- (void)configureCountDownLbl {
    if (self.countDownNum == 0) {
        [self.timer setFireDate:[NSDate distantFuture]];
        [self.timer invalidate];
        self.timer = nil;
        return;
    }
    self.countDownNum--;
    NSInteger timeout = self.countDownNum;
    NSInteger days = timeout/(3600*24);
    NSInteger hours = (timeout-days*24*3600)/3600;
    NSInteger minute = (timeout-days*24*3600-hours*3600)/60;
    NSInteger second = timeout-days*24*3600-hours*3600-minute*60;
    NSString *text = StrF(@"剩%02ld天%02ld小时%02ld分%02ld秒", days, hours, minute, second);
    self.countDownLbl.text = text;
}

#pragma mark 取消订单
- (void)cancelOrder {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    NSString *api = @"api/shop/order/cancelOrder";
    [self request:api
            param:@{@"order_id":self.obj.order_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            self.obj.status = @"04";
            [self configureView];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark - 更换地址
- (void)updateTempOrderAddress:(UserAddressListObj *)obj {
    if (![obj isKindOfClass:UserAddressListObj.class]) {
        return;
    }
    [NotifyHelper showMXLoadingHUDAddedTo:self.view animated:YES];
    NSString *api = @"api/spell/order/updateOrderAddress";
    [self request:api
            param:@{@"address_id":obj.address_id,
                    @"order_id":self.obj.order_id
            }
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideAllHUDsForView:self.view animated:YES];
        if (success) {
            self.obj.userName = obj.name;
            self.obj.userTel = obj.tel;
            self.obj.userAddress = obj.address;
            self.obj.address_id = obj.address_id;
            [self.addressView configureView:self.obj];
            if (!self.obj.isSelfSupport) {
                NSArray *tempArr = object[@"data"][@"orderFreightFeeList"];
                NSDictionary *dic = tempArr.firstObject;
                if ([dic isKindOfClass:NSDictionary.class]) {
                    self.obj.freight_fee =  [dic[@"freight_fee"] floatValue];
                    //刷新运费
                    [self.goodsView configureFreightLayout];
                    //刷新订单总价
                    [self.goodsView configureTotalLayout];
                }
            }
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark 确认收货
- (void)confirmOrder {
    [[YQPayKeyWordVC alloc] showInViewController:[[MXRouter sharedInstance] getMallTopNavigationController]
                                            type:NormalPayType
                                        dataDict:@{@"title":@"支付密码"}
                                           block:^(NSString * password) {
        [NotifyHelper showHUDAddedTo:self.view animated:YES];
        NSString *api = @"api/spell/order/confirmOrder";
         [self request:api
                 param:@{@"order_id":self.obj.order_id,
                         @"pay_password":password
                 }
            completion:^(BOOL success, id object, NSString *error) {
             [NotifyHelper hideHUDForView:self.view animated:YES];
             if (success) {
                 [NotifyHelper showMessageWithMakeText:@"确认收货"];
                 self.obj.status = @"09";
                 [self configureView];
             } else {
                 [NotifyHelper showMessageWithMakeText:object[@"msg"]];
             }
         }];
    }];
}

- (void)payOrder:(NSString *)selectPayType {
    PolymerPayType payType = [self formatPayType:selectPayType];
    if (payType == PolymerPayTypeExchange) {
        [[YQPayKeyWordVC alloc] showInViewController:[[MXRouter sharedInstance] getMallTopNavigationController]
                                                type:NormalPayType
                                            dataDict:@{@"title":@"支付密码"}
                                               block:^(NSString * password) {
            [NotifyHelper showHUDAddedTo:self.view animated:YES];
            NSString *api = @"api/spell/spellPay/paySpellSingleOrderApp";
             [self request:api
                     param:@{@"order_id":self.obj.order_id,
                             @"order_pay_type":@"04",
                             @"pay_password":password
                     }
                completion:^(BOOL success, id object, NSString *error) {
                 [NotifyHelper hideHUDForView:self.view animated:YES];
                 if (success) {
                     [NotifyHelper showMessageWithMakeText:@"支付成功"];
                     self.obj.status = @"02";
                     [self configureView];
                 } else {
                     [NotifyHelper showMessageWithMakeText:object[@"msg"]];
                 }
             }];
        }];
        return;
    }
    NSDictionary *param = @{@"order_id":self.obj.order_id,
                            @"order_pay_type":[self order_pay_type:selectPayType],
    };

    NSString *api = @"/api/spell/spellPay/paySpellSingleOrderApp";
    [[PolymerPayManager shared] pay:payType url:api
                              param:param viewController:self block:^(id  _Nullable data) {
        if([data isSuccess]) {
            [NotifyHelper showMessageWithMakeText:@"支付成功"];
            self.obj.status = @"02";
            [self configureView];
        } else {
            [NotifyHelper showMessageWithMakeText:[data valueForKey:@"msg"]];
        }
    }];
}

- (PolymerPayType)formatPayType:(NSString *)selectPayType {
    PolymerPayType payType = PolymerPayTypeExchange;
    if ([@"支付宝" isEqualToString:selectPayType]) {
        payType = PolymerPayTypeAliPay;
    } else if ([@"微信" isEqualToString:selectPayType]) {
        payType = PolymerPayTypeWePay;
    }
    return payType;
}

- (NSString *)order_pay_type:(NSString *)selectPayType {
    if ([@"支付宝" isEqualToString:selectPayType]) {
        return @"01";
    } else if ([@"微信" isEqualToString:selectPayType]) {
        return @"02";
    }
    return @"04";
}

- (void)configureView {
    [self configureTitleLbl];
    self.countDownLbl.text = [self tipString];
    [self.timer invalidate];
    [self configureBottomView];
    //待付款到其它状态 地址栏右侧图标隐藏
    [self.addressView configureView:self.obj];
}

- (void)configureBottomView {
    if (self.obj.orderStatus == OrderStatusToReceive ||
        self.obj.orderStatus == OrderStatusToPay ||
        self.obj.isRefund) {
        self.bottomView.visibility = MyVisibility_Visible;
    } else {
        self.bottomView.visibility = MyVisibility_Gone;
    }
    [self.bottomView configureView:self.obj];
}

- (void)configureTitleLbl {
    self.titleLbl.text = self.obj.formatInfoTips;
    [self.titleLbl sizeToFit];
    self.titleLbl.mySize = self.titleLbl.size;
}

- (void)createUI {
    self.view.backgroundColor = [UIColor moBackground];
    UIImageView *bgImgView = [UIImageView autoLayoutImgView:@"订单详情背景"];
    CGSize size = bgImgView.size;
    bgImgView.size = CGSizeMake(SCREEN_WIDTH, SCREEN_WIDTH * size.height/size.width);
    [self.view addSubview:bgImgView];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.tableView];
    [layout addSubview:self.bottomView];
    [self.view addSubview:self.backBtn];
}

- (void)initUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldSystemFontOfSize:24];
    lbl.textColor = [UIColor whiteColor];
    lbl.myCenterX = 0;
    lbl.myTop = safeAreaInset().top + 20 + 10;
    lbl.myBottom = 20;
    self.titleLbl = lbl;
    [self configureTitleLbl];
    [layout addSubview:lbl];
    [layout addSubview:self.countDownLbl];
    if (self.obj.orderStatus == OrderStatusToPay) {
        [self configureCountDown:self.obj];
    } else {
        self.countDownLbl.text = [self tipString];
    }
    [layout addSubview:self.shipperView];
    [layout addSubview:self.addressView];
    [self.addressView configureView:self.obj];
    
    [layout addSubview:self.goodsView];
    
    [layout addSubview:self.infoView];
    
    [layout layoutIfNeeded];
    layout.height += 70 + safeAreaInsetBottom();
    self.tableView.tableHeaderView = layout;
    [self configureBottomView];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.weight = 1;
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        AdjustTableBehavior(_tableView);
    }
    return _tableView;
}

- (OrderInfoAddressView *)addressView {
    if (!_addressView) {
        _addressView = ({
            OrderInfoAddressView *object = [OrderInfoAddressView new];
            object.height = [OrderInfoAddressView viewHeight];
            object.myHeight = object.height;
            object.myBottom = 10;
            object.myLeft = 0;
            object.myRight = 0;
            @weakify(self)
            [object addAction:^(UIView *view) {
                if (self.obj.orderStatus != OrderStatusToPay) {
                    return;
                }
                void (^selectBlock)(id) = ^(UserAddressListObj *obj) {
                    @strongify(self)
                    [self updateTempOrderAddress:obj];
                };
                MXRoute(@"ShippingAddressVC", @{@"block":selectBlock});
            }];
            object;
        });
        
    }
    return _addressView;
}

- (OrderInfoGoodsView *)goodsView {
    if (!_goodsView) {
        _goodsView = [[OrderInfoGoodsView alloc] initWithOrderObj:self.obj];
        _goodsView.myHeight = [_goodsView viewHeight];
        _goodsView.myBottom = 10;
        _goodsView.myLeft = 0;
        _goodsView.myRight = 0;
    }
    return _goodsView;
}

- (OrderInfoSubView *)infoView {
    if (!_infoView) {
        _infoView = [[OrderInfoSubView alloc] initWithOrderObj:self.obj];
        _infoView.myHeight = [OrderInfoSubView viewHeight];
        _infoView.myBottom = 10;
        _infoView.myLeft = 0;
        _infoView.myRight = 0;
    }
    return _infoView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = ({
            UIButton *object = [UIButton new];
            object.x = 17;
            object.y = safeAreaInset().top + 20;
            object.size = CGSizeMake(40, 40);
            [object setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
//                NSArray *navArr = self.navigationController.viewControllers;
//                BaseViewController *findVC = nil;
//                for (NSInteger i = navArr.count - 1; i >= 0; i--) {
//                    BaseViewController *vc = navArr[i];
//                    if (![vc isKindOfClass:BaseViewController.class]) {
//                        continue;
//                    }
//                    if ([vc isKindOfClass:NSClassFromString(@"GoodInfoVC")]) {
//                        findVC = vc;
//                        break;
//                    }
//
//                }
//                if (findVC) {
//                    [self.navigationController popToViewController:findVC animated:YES];
//                    return;
//                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
            object;
        });
    }
    return _backBtn;
}

- (OrderInfoBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [OrderInfoBottomView new];
        _bottomView.size = CGSizeMake(SCREEN_WIDTH, [OrderInfoBottomView viewHeight]);
        _bottomView.mySize = _bottomView.size;
        _bottomView.visibility = MyVisibility_Gone;
    }
    return _bottomView;
}

- (OrderSelectPayView *)orderSelectPayView {
    if (!_orderSelectPayView) {
        _orderSelectPayView = [[OrderSelectPayView alloc] initWithAmount:self.obj.total_cash_num
                                                                withRate:self.obj.sla_discount_rate
                                                            withSlaPrice:self.sla_price
                                                                noSLAPay:!self.sla_pay_lock];
        _orderSelectPayView.size = CGSizeMake(SCREEN_WIDTH, [OrderSelectPayView viewHeight]);
        @weakify(self)
        [_orderSelectPayView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.panVC dismiss];
        }];
        _orderSelectPayView.block = ^(id data) {
            @strongify(self)
            NSString *payType = data;
            MLog(@"选择支付方式....%@", payType);
            [self.panVC dismiss];
            [self payOrder:payType];
        };
    }
    return _orderSelectPayView;
}

- (HWPanModelVC *)panVC {
    if (!_panVC) {
        _panVC = ({
            HWPanModelVC *object = [HWPanModelVC showInVC:self
                                               customView:self.orderSelectPayView];
            object;
        });
    }
    return _panVC;
}

- (UILabel *)countDownLbl {
    if (!_countDownLbl) {
        UILabel *lbl = [UILabel new];
        lbl.font = [UIFont font17];
        lbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.myLeft = 0;
        lbl.myHeight = 25;
        lbl.myRight = 0;
        lbl.myBottom = 20;
        _countDownLbl = lbl;
    }
    return _countDownLbl;
}

- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(configureCountDownLbl) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        [_timer setFireDate:[NSDate distantFuture]];
    }
    return _timer;
}

- (ShipperView *)shipperView {
    if (!_shipperView) {
        _shipperView = [ShipperView new];
        _shipperView.size = CGSizeMake(SCREEN_WIDTH - 20, [ShipperView viewHeight]);
        _shipperView.mySize = _shipperView.size;
        _shipperView.myBottom = 10;
        _shipperView.myLeft = 10;
        _shipperView.visibility = MyVisibility_Gone;
        @weakify(self)
        [_shipperView addAction:^(UIView *view) {
            @strongify(self)
            MXRoute(@"LogisticsVC", @{@"obj":self.obj})
        }];
    }
    return _shipperView;
}

@end
