//
//  MGGoodsOrderInfoVC.h
//  BOB
//
//  Created by colin on 2020/12/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGGoodsOrderInfoVC : BaseViewController

@property (nonatomic, strong) NSString *orderID;

@end

NS_ASSUME_NONNULL_END
