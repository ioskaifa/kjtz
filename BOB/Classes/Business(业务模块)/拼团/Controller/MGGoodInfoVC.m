//
//  MGGoodInfoVC.m
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGGoodInfoVC.h"
#import "XHWebImageAutoSize.h"
#import "MGGoodsInfoObj.h"
#import "MGGoodInfoHeaderView.h"
#import "MGGoodInfoBottomView.h"
#import "HWPanModelVC.h"
#import "MGRuleView.h"
#import "YQPayKeyWordVC.h"
#import "NSString+DateFormatter.h"

@interface MGGoodInfoVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) MGGoodInfoHeaderView *headerView;

@property (nonatomic, strong) MGGoodInfoBottomView *bottomView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MGGoodsInfoObj *goodInfoObj;

@property (nonatomic, strong) UIImageView *backImgView;
@property (nonatomic, strong) MGRuleView *ruleView;
@property (nonatomic, strong) HWPanModelVC *panVC;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger countDownNum;
///超时取消订单时间（秒）
@property (nonatomic, assign) NSTimeInterval validityTime;

@end

@implementation MGGoodInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self.view bringSubviewToFront:self.backImgView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)configureBuyBtnStatus:(BOOL)enable {
    if (enable) {
        self.bottomView.buyBtn.userInteractionEnabled = YES;
        self.bottomView.buyBtn.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
    } else {
        self.bottomView.buyBtn.userInteractionEnabled = NO;
        self.bottomView.buyBtn.backgroundColor = [UIColor colorWithHexString:@"#7F7F7F"];
    }
}

- (void)fetchProgress {
    NSString *api = @"api/spell/goods/getSpellGoodsDetail";
    [self request:api
            param:@{@"goods_id":self.goods_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            self.goodInfoObj.progress = [object[@"data"][@"pro_rate"] floatValue];
            [self.headerView configureProgressView:self.goodInfoObj.progress];
        }
    }];
}

- (void)fetchData {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    NSString *api = @"api/spell/goods/getSpellGoodsDetail";
    [self request:api
            param:@{@"goods_id":self.goods_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideAllHUDsForView:self.view animated:YES];
        if (success) {
            self.goodInfoObj = [MGGoodsInfoObj modelParseWithDict:object[@"data"][@"spellGoodsDetail"]];
            self.validityTime = [object[@"data"][@"spellGoodsDetail"][@"spell_time"] integerValue];
            self.goodInfoObj.sys_time = [object[@"data"][@"sys_time"] doubleValue];
            self.goodInfoObj.progress = [object[@"data"][@"pro_rate"] floatValue];
            if (![self.goodInfoObj isKindOfClass:MGGoodsInfoObj.class]) {
                return;
            }
            [self initUI];
            [self.view bringSubviewToFront:self.backImgView];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)commit {
    [[YQPayKeyWordVC alloc] showInViewController:[[MXRouter sharedInstance] getMallTopNavigationController]
                                            type:NormalPayType
                                        dataDict:@{@"title":@"支付密码"}
                                           block:^(NSString * password) {
        [NotifyHelper showHUDAddedTo:self.view animated:YES];
        NSString *api = @"api/spell/goods/partSpellGoods";
         [self request:api
                 param:@{@"goods_id":self.goodInfoObj.ID,
                         @"pay_password":password
                 }
            completion:^(BOOL success, id object, NSString *error) {
             [NotifyHelper hideHUDForView:self.view animated:YES];
             if (success) {
                 [self configureBuyBtnStatus:NO];
                 NSString *ID = object[@"data"][@"id"];
                 if ([StringUtil isEmpty:ID]) {
                     MXRoute(@"MGOrderInfoVC", @{@"part_id":ID})
                 } else {
                     MXRoute(@"MGMyOrderVC", nil)
                 }
             } else {
                 [NotifyHelper showMessageWithMakeText:object[@"msg"]];
             }
         }];
    }];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.goodInfoObj.goods_describeArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *urlArr = self.goodInfoObj.goods_describeArr;
    if (indexPath.row >= urlArr.count) {
        return 0.01;
    }
    NSString *imgUrl = urlArr[indexPath.row];
    return [XHWebImageAutoSize imageHeightForURL:[NSURL URLWithString:imgUrl] layoutWidth:SCREEN_WIDTH estimateHeight:0.01];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class cls = UITableViewCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    UIImageView *imgView = [cell.contentView viewWithTag:1000];
    if (!imgView) {
        imgView = [UIImageView new];
        imgView.tag = 1000;
        [cell.contentView addSubview:imgView];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.edges.equalTo(@(0));
        }];
    }
    NSArray *urls = self.goodInfoObj.goods_describeArr;
    if (indexPath.row < self.goodInfoObj.goods_describeArr.count) {
        NSString *imageURL = [urls safeObjectAtIndex:indexPath.row];
        //加载网络图片使用SDWebImage
        [imgView sd_setImageWithURL:[NSURL URLWithString:imageURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            /** 缓存image size */
            [XHWebImageAutoSize storeImageSize:image forURL:imageURL completed:^(BOOL result) {
                /** reload  */
                if(result) {
                    [tableView xh_reloadDataForURL:imageURL];
                };
            }];
        }];
    }
    return cell;
}

#pragma mark - 倒计时
- (void)configureCountDown:(MGGoodsInfoObj *)obj {
    if (![obj isKindOfClass:MGGoodsInfoObj.class]) {
        return;
    }
    NSTimeInterval sysTime = obj.sys_time;
    NSString *createTimeString = StrF(@"%@%@", obj.shelves_date, obj.shelves_time);
    NSTimeInterval createTime = [createTimeString yyyyMMddHHmmssToTimeInterval];
    NSTimeInterval diff = sysTime - createTime;
    //剩余有效时间
    diff = self.validityTime - diff;
    if (diff <= 0) {
        diff = 0;
        [self configureBuyBtnStatus:NO];
        return;
    }
    self.countDownNum = diff;
    [self.timer setFireDate:[NSDate date]];
}

- (void)configureCountDownLbl {
    if (self.countDownNum == 0) {
        [self.timer setFireDate:[NSDate distantFuture]];
        [self.timer invalidate];
        self.timer = nil;
        [self.headerView.countDownNumView configureView:0 min:0 second:0];
        [self configureBuyBtnStatus:NO];
        return;
    }
    self.countDownNum--;
    NSInteger time = self.countDownNum;
    NSInteger h = time / 3600;
    NSInteger m = (time % 3600) / 60;
    NSInteger s = time % 60;
    [self.headerView.countDownNumView configureView:h min:m second:s];
}

- (void)createUI {
    [self.view addSubview:self.backImgView];
}

- (void)initUI {
    self.tableView.tableHeaderView = self.headerView;
    [self.bottomView configureSLAPrice:self.goodInfoObj.goods_sla_price];
    [self.view addSubview:self.tableView];
    UIView *bottomView = [UIView addBottomSafeAreaWithContentView:self.bottomView];
    [self.view addSubview:bottomView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, bottomView.height, 0));
    }];
    
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(bottomView.size);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[self.goodInfoObj.spell_rule dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.ruleView.tv.attributedText = attrStr;
    if (self.goodInfoObj.progress >= 1.0) {
        [self configureBuyBtnStatus:NO];
    } else {
        [self configureBuyBtnStatus:YES];
    }
    [self configureCountDown:self.goodInfoObj];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        Class cls = UITableViewCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        AdjustTableBehavior(_tableView);
        @weakify(self)
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchProgress];
        }];
    }
    return _tableView;
}

- (UIImageView *)backImgView {
    if (!_backImgView) {
        _backImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"商品详情返回"];
            object.x = 20;
            object.y = safeAreaInset().top + 20;
            [object addAction:^(UIView *view) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            object;
        });
    }
    return _backImgView;
}

- (MGGoodInfoHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[MGGoodInfoHeaderView alloc] initWithGoodsInfoObj:self.goodInfoObj];
        _headerView.size = CGSizeMake(SCREEN_WIDTH, [_headerView viewHeight]);
        @weakify(self)
        [_headerView.ruleView addAction:^(UIView *view) {
         @strongify(self)
            [self.panVC show];
        }];
    }
    return _headerView;
}

- (MGGoodInfoBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [MGGoodInfoBottomView new];
        _bottomView.size = CGSizeMake(SCREEN_WIDTH, [MGGoodInfoBottomView viewHeight]);
        @weakify(self)
        [_bottomView.buyBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self commit];
        }];
        if (self.orderStatus == MGOrderStatusEnd) {
            [self configureBuyBtnStatus:NO];
        } else {
            [self configureBuyBtnStatus:YES];
        }
    }
    return _bottomView;
}

- (MGRuleView *)ruleView {
    if (!_ruleView) {
        _ruleView = [MGRuleView new];
        _ruleView.size = CGSizeMake(SCREEN_WIDTH, [MGRuleView viewHeight]);
        @weakify(self)
        [_ruleView.knownlbl addAction:^(UIView *view) {
            @strongify(self)
            [self.panVC dismiss];
        }];
    }
    return _ruleView;
}

- (HWPanModelVC *)panVC {
    if (!_panVC) {
        _panVC = ({
            HWPanModelVC *object = [HWPanModelVC showInVC:self
                                               customView:self.ruleView];
            object;
        });
    }
    return _panVC;
}

- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(configureCountDownLbl) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        [_timer setFireDate:[NSDate distantFuture]];
    }
    return _timer;
}

@end
