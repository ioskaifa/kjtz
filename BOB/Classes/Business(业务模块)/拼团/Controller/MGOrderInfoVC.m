//
//  MGOrderInfoVC.m
//  BOB
//
//  Created by colin on 2020/12/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGOrderInfoVC.h"
#import "MGOrderInfoStatusView.h"
#import "MakeGroupListView.h"
#import "MGOrderInfoSubView.h"
#import "MGWinInfoView.h"

#import "MakeGroupListObj.h"

@interface MGOrderInfoVC ()

@property (nonatomic, strong) MakeGroupListObj *infoObj;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MGOrderInfoStatusView *statusView;

@property (nonatomic, strong) MakeGroupListView *goodsView;

@property (nonatomic, strong) MGOrderInfoSubView *orderInfoView;
@property (nonatomic, strong) MGWinInfoView *winInfoView;

@end

@implementation MGOrderInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)fetchData {
    [self request:@"api/spell/part/getUserSpellPartDetail"
            param:@{@"part_id":self.part_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            self.infoObj = [MakeGroupListObj modelParseWithDict:object[@"data"][@"spellPartDetail"]];
            self.tableView.tableHeaderView = [self headerView];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)createUI {
    [self setNavBarTitle:@"订单详情"];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (MyBaseLayout *)headerView {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor moBackground];
    baseLayout.myWidth = SCREEN_WIDTH;
    baseLayout.myHeight = MyLayoutSize.wrap;
    
    [baseLayout addSubview:self.statusView];
    [baseLayout addSubview:self.goodsView];
    
    if (self.infoObj.orderStatus == MGOrderStatusNot) {
        [baseLayout addSubview:[self orderNotView]];
    } else if (self.infoObj.orderStatus == MGOrderStatusFail) {
        [baseLayout addSubview:[self orderFailView]];
    }
    
    if (self.infoObj.orderStatus == MGOrderStatusWin) {
        [baseLayout addSubview:self.winInfoView];
    }
    
    [baseLayout addSubview:self.orderInfoView];
    
    [self.statusView configureView:self.infoObj.orderStatus];
    self.infoObj.orderStatus = MGOrderStatusInfo;
    [self.goodsView configureView:self.infoObj];
    
    [baseLayout layoutIfNeeded];
    baseLayout.myHeight = baseLayout.height;
    
    return baseLayout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor moBackground];
    }
    return _tableView;
}

- (MGOrderInfoStatusView *)statusView {
    if (!_statusView) {
        _statusView = [MGOrderInfoStatusView new];
        _statusView.size = CGSizeMake(SCREEN_WIDTH - 30, [MGOrderInfoStatusView viewHeight]);
        _statusView.mySize = _statusView.size;
        _statusView.myLeft = 15;
        _statusView.myTop = 10;
        _statusView.myBottom = 10;
    }
    return _statusView;
}

- (MakeGroupListView *)goodsView {
    if (!_goodsView) {
        _goodsView = [MakeGroupListView new];
        [_goodsView setViewCornerRadius:5];
        _goodsView.size = CGSizeMake(SCREEN_WIDTH - 30, 150);
        _goodsView.mySize = _goodsView.size;
        _goodsView.myLeft = 15;
        _goodsView.myBottom = 10;
    }
    return _goodsView;
}

- (MGOrderInfoSubView *)orderInfoView {
    if (!_orderInfoView) {
        _orderInfoView = [[MGOrderInfoSubView alloc] initWithOrderObj:self.infoObj];
        [_orderInfoView setViewCornerRadius:5];
        _orderInfoView.size = CGSizeMake(SCREEN_WIDTH - 30, [MGOrderInfoSubView viewHeight]);
        _orderInfoView.mySize = _orderInfoView.size;
        _orderInfoView.myLeft = 15;
        _orderInfoView.myBottom = 10;
    }
    return _orderInfoView;
}

- (MGWinInfoView *)winInfoView {
    if (!_winInfoView) {
        _winInfoView = [[MGWinInfoView alloc] initWithOrderObj:self.infoObj];
        [_winInfoView setViewCornerRadius:5];
        _winInfoView.size = CGSizeMake(SCREEN_WIDTH - 30, [MGWinInfoView viewHeight]);
        _winInfoView.mySize = _winInfoView.size;
        _winInfoView.myLeft = 15;
        _winInfoView.myBottom = 10;
        @weakify(self)
        [_winInfoView addAction:^(UIView *view) {
            @strongify(self)
            if ([StringUtil isEmpty:self.infoObj.win_order_no]) {
                return;
            }
            MXRoute(@"MGGoodsOrderInfoVC", @{@"orderID":self.infoObj.win_order_no})
        }];
    }
    return _winInfoView;
}

- (MyBaseLayout *)orderFailView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    [layout setViewCornerRadius:5];
    layout.myLeft = 15;
    layout.size = CGSizeMake(SCREEN_WIDTH - 30, 100);
    layout.mySize = layout.size;
    layout.myBottom = 10;
    layout.myLeft = 15;
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont font15];
    lbl.text = @"开奖信息";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 10;
    lbl.myTop = 10;
    [layout addSubview:lbl];
    
    NSArray *txtArr = @[@"人数不足"];
    NSArray *fontArr = @[[UIFont font13]];
    NSArray *colorArr = @[[UIColor moRed]];
    NSAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                 colors:colorArr
                                                                  fonts:fontArr];
    MyBaseLayout *subLayout = [self.class factoryLayout:@"拼团失败"
                                           leftTxtColor:[UIColor colorWithHexString:@"#666666"]
                                            leftTxtFont:[UIFont font13]
                                               rightAtt:att];
    [layout addSubview:subLayout];
    
    txtArr = @[self.infoObj.refund_sla_num];
    fontArr = @[[UIFont font13]];
    colorArr = @[[UIColor moRed]];
    att = [NSMutableAttributedString initWithTitles:txtArr
                                                                 colors:colorArr
                                                                  fonts:fontArr];
    subLayout = [self.class factoryLayout:@"退还本金(SLA)"
                                           leftTxtColor:[UIColor colorWithHexString:@"#666666"]
                                            leftTxtFont:[UIFont font13]
                                               rightAtt:att];
    [layout addSubview:subLayout];
    return layout;
}

- (MyBaseLayout *)orderNotView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    [layout setViewCornerRadius:5];
    layout.myLeft = 15;
    layout.size = CGSizeMake(SCREEN_WIDTH - 30, 70);
    layout.mySize = layout.size;
    layout.myBottom = 10;
    layout.myLeft = 15;
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont font15];
    lbl.text = @"开奖信息";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 10;
    lbl.myTop = 10;
    [layout addSubview:lbl];
    
    NSArray *txtArr = @[self.infoObj.refund_sla_num];
    NSArray *fontArr = @[[UIFont font13]];
    NSArray *colorArr = @[[UIColor moRed]];
    NSAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                 colors:colorArr
                                                                  fonts:fontArr];
    MyBaseLayout *subLayout = [self.class factoryLayout:@"返还数量(SLA)"
                                           leftTxtColor:[UIColor colorWithHexString:@"#666666"]
                                            leftTxtFont:[UIFont font13]
                                               rightAtt:att];
    [layout addSubview:subLayout];
    
//    txtArr = @[self.infoObj.spell_ben_rate];
//    fontArr = @[[UIFont font13]];
//    colorArr = @[[UIColor moRed]];
//    att = [NSMutableAttributedString initWithTitles:txtArr
//                                                                 colors:colorArr
//                                                                  fonts:fontArr];
//    subLayout = [self.class factoryLayout:@"返还比例(SLA)"
//                                           leftTxtColor:[UIColor colorWithHexString:@"#666666"]
//                                            leftTxtFont:[UIFont font13]
//                                               rightAtt:att];
    [layout addSubview:subLayout];
    return layout;
}

+ (MyBaseLayout *)factoryLayout:(NSString *)leftTxt
                   leftTxtColor:(UIColor *)leftColor
                    leftTxtFont:(UIFont *)leftFont
                       rightAtt:(NSAttributedString *)rightAtt {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myTop = 0;
    layout.myHeight = 30;
    UILabel *lbl = [UILabel new];
    lbl.textColor = leftColor;
    lbl.font = leftFont;
    lbl.text = leftTxt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView placeholderHorzView]];
    
    lbl = [UILabel new];
    lbl.attributedText = rightAtt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.tag = 1;
    [layout addSubview:lbl];
    return layout;
}

@end
