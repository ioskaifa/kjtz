//
//  MGGoodsOrderVC.m
//  BOB
//
//  Created by colin on 2020/12/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGGoodsOrderVC.h"
#import "MyOrderNullView.h"
#import "YJSliderView.h"
#import "MGMyOrderListObj.h"
#import "MGGoodsOrderListCell.h"

@interface MGGoodsOrderVC ()<UITableViewDelegate, UITableViewDataSource, YJSliderViewDelegate>

@property (nonatomic, assign) MGGoodsOrderStatus orderStatus;

@property (nonatomic, strong) MyOrderNullView *nullView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YJSliderView *sliderView;

@property (nonatomic, strong) NSArray *sliderDataSourceArr;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation MGGoodsOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.last_id = @"";
    [self fetchData];
}

#pragma mark - Private Method
- (void)fetchData {
    NSString *api = @"api/spell/order/getOrderList";
    [self request:api
            param:@{@"last_id":self.last_id?:@"",
                    @"status":[MGMyOrderListObj orderStatusToString:self.orderStatus]
            }
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            if ([@"" isEqualToString:self.last_id]) {
                [self.dataSourceMArr removeAllObjects];
            }
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *dataArr = [MGMyOrderListObj modelListParseWithArray:dataDic[@"data"][@"orderList"]];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
//            [self.dataSourceMArr addObjectsFromArray:[MGMyOrderListObj MGMyOrderListObj]];
            MGMyOrderListObj *obj = self.dataSourceMArr.lastObject;
            if ([obj isKindOfClass:MGMyOrderListObj.class]) {
                self.last_id = obj.ID;
            }
            [self.tableView reloadData];
        }
        [self.tableView reloadData];
        if (self.dataSourceMArr.count == 0) {
            self.nullView.hidden = NO;
        } else {
            self.nullView.hidden = YES;
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MGGoodsOrderListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    MGMyOrderListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MGMyOrderListObj.class]) {
        return;
    }
    if (![cell isKindOfClass:MGGoodsOrderListCell.class]) {
        return;
    }
    
    MGGoodsOrderListCell *listCell = (MGGoodsOrderListCell *)cell;
    [listCell configureView:obj];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    MGMyOrderListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MGMyOrderListObj.class]) {
        return;
    }
    MXRoute(@"MGGoodsOrderInfoVC", @{@"orderID":obj.order_id})
}

#pragma mark YJSliderViewDelegate
- (NSInteger)numberOfItemsInYJSliderView:(YJSliderView *)sliderView {
    return self.sliderDataSourceArr.count;
}

- (UIView *)yj_SliderView:(YJSliderView *)sliderView viewForItemAtIndex:(NSInteger)index {
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (NSString *)yj_SliderView:(YJSliderView *)sliderView titleForItemAtIndex:(NSInteger)index {
    if (index >= self.sliderDataSourceArr.count) {
        return @"";
    }
    return self.sliderDataSourceArr[index];
}

- (NSInteger)initialzeIndexFoYJSliderView:(YJSliderView *)sliderView {
    return self.orderStatus;
}

- (void)yj_SliderView:(YJSliderView *)sliderView didSelectItemAtIndex:(NSIndexPath *)indexPath {
    self.orderStatus = indexPath.row;
    self.last_id = @"";
    [self fetchData];
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"我的订单"];
    self.view.backgroundColor = [UIColor whiteColor];
    self.last_id = @"";
    self.orderStatus = MGGoodsOrderStatusToAll;
    
    [self.view addSubview:self.sliderView];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.nullView];
        
    [self layoutUI];
}

- (void)layoutUI {
    [self.sliderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sliderView.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sliderView.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(self.nullView.height);
    }];
}


#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = [MGGoodsOrderListCell viewHeight];
        Class cls = MGGoodsOrderListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];        
        _tableView.tableFooterView = [UIView new];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (YJSliderView *)sliderView {
    if (!_sliderView) {
        _sliderView = [[YJSliderView alloc] initWithFrame:CGRectZero];
        _sliderView.delegate = self;
        _sliderView.themeColor = [UIColor blackColor];
        _sliderView.lineColor = [UIColor blackColor];
    }
    return _sliderView;
}

- (NSArray *)sliderDataSourceArr {
    if (!_sliderDataSourceArr) {
        _sliderDataSourceArr = @[@"全部", @"待付款", @"待发货", @"待收货", @"已完成"];
    }
    return _sliderDataSourceArr;
}

- (MyOrderNullView *)nullView {
    if (!_nullView) {
        _nullView = [MyOrderNullView new];
        _nullView.size = CGSizeMake(SCREEN_WIDTH, [MyOrderNullView viewHeight]);
        _nullView.hidden = YES;
    }
    return _nullView;
}

@end
