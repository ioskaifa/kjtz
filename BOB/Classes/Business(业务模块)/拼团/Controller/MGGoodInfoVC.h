//
//  MGGoodInfoVC.h
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "MakeGroupListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGGoodInfoVC : BaseViewController

@property (nonatomic, copy) NSString *goods_id;

@property (nonatomic, assign) MGOrderStatus orderStatus;

@end

NS_ASSUME_NONNULL_END
