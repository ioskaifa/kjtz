//
//  MGOrderInfoVC.h
//  BOB
//
//  Created by colin on 2020/12/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGOrderInfoVC : BaseViewController

@property (nonatomic, copy) NSString *part_id;

@end

NS_ASSUME_NONNULL_END
