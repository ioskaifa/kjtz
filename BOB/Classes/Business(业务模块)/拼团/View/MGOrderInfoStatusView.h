//
//  MGOrderInfoStatusView.h
//  BOB
//
//  Created by colin on 2020/12/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MakeGroupListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGOrderInfoStatusView : UIView

+ (CGFloat)viewHeight;

- (void)configureView:(MGOrderStatus)orderStatus;

@end

NS_ASSUME_NONNULL_END
