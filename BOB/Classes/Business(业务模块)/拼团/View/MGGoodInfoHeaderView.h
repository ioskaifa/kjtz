//
//  MGGoodInfoHeaderView.h
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGGoodsInfoObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MXCountDownNumView : UIView

+ (CGSize)viewSize;

- (void)configureView:(NSInteger)hour min:(NSInteger)min second:(NSInteger)sec;

@end

@interface MGGoodInfoHeaderView : UIView

@property (nonatomic, strong) MXCountDownNumView *countDownNumView;

@property (nonatomic, strong) UIView *ruleView;

- (instancetype)initWithGoodsInfoObj:(MGGoodsInfoObj *)obj;

- (CGFloat)viewHeight;

- (void)configureProgressView:(CGFloat)progress;

@end

NS_ASSUME_NONNULL_END
