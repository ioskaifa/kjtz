//
//  MGGoodInfoBottomView.m
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGGoodInfoBottomView.h"

@interface MGGoodInfoBottomView ()

@property (nonatomic, strong) UILabel *SLAPriceLbl;

@end

@implementation MGGoodInfoBottomView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60;
}

- (void)configureSLAPrice:(CGFloat)price {
    NSString *string = StrF(@"%0.4f", price);
    NSArray *txtArr = @[@"SLA: ", string];
    NSArray *colorArr = @[[UIColor blackColor], [UIColor colorWithHexString:@"#FF4757"]];
    NSArray *fontArr = @[[UIFont font16], [UIFont boldFont20]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    self.SLAPriceLbl.attributedText = att;
    [self.SLAPriceLbl autoMyLayoutSize];
}

- (void)createUI {
    MyLinearLayout *layout = [self addMyLinearLayout:MyOrientation_Horz];
    layout.gravity = MyGravity_Horz_Between;
    layout.myLeft = 15;
    layout.myRight = 15;
    
    UILabel *lbl = [UILabel new];
    lbl.myCenterY = 0;
    self.SLAPriceLbl = lbl;
    [layout addSubview:lbl];
    
    [layout addSubview:self.buyBtn];
}

- (UIButton *)buyBtn {
    if (!_buyBtn) {
        _buyBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(115, 45);
            [object setViewCornerRadius:5];
            object.titleLabel.font = [UIFont boldFont15];
            object.titleLabel.textAlignment = NSTextAlignmentCenter;
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setBackgroundColor:[UIColor colorWithHexString:@"#FF4757"]];
            [object setTitle:@"立即参团" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _buyBtn;
}

@end
