//
//  MGGoodInfoBottomView.h
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGGoodInfoBottomView : UIView
///立即购买
@property (nonatomic, strong) UIButton *buyBtn;

+ (CGFloat)viewHeight;

- (void)configureSLAPrice:(CGFloat)price;

@end

NS_ASSUME_NONNULL_END
