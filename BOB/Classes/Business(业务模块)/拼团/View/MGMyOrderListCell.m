//
//  MGMyOrderListCell.m
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGMyOrderListCell.h"

@interface MGMyOrderListCell ()

@property (nonatomic, strong) UILabel *payLbl;

@end

@implementation MGMyOrderListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 190;
}

- (void)configureView:(MakeGroupListObj *)obj {
    if (![obj isKindOfClass:MakeGroupListObj.class]) {
        return;
    }
    
    [self.goodView configureView:obj];
    NSString *slaPrice = StrF(@"%0.4f", obj.payPrice);
    self.payLbl.text = StrF(@"实付：SLA %@", slaPrice);
    [self.payLbl autoMyLayoutSize];
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor whiteColor];
    MyBaseLayout *baseLayout = [self.contentView addMyLinearLayout:MyOrientation_Vert];
    baseLayout.myLeft = 15;
    baseLayout.myRight = 15;
    baseLayout.myTop = 10;
    baseLayout.myBottom = 5;
    baseLayout.layer.borderColor = [UIColor moBackground].CGColor;
    baseLayout.layer.borderWidth = 1;
    [baseLayout setViewCornerRadius:5];
    
    [baseLayout addSubview:self.goodView];
    [baseLayout addSubview:[UIView line]];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.weight = 1;
    layout.gravity = MyGravity_Horz_Between;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.payLbl];
    SPButton *btn = [SPButton new];
    btn.imagePosition = SPButtonImagePositionRight;
    btn.imageTitleSpace = 10;
    btn.userInteractionEnabled = NO;
    btn.titleLabel.font = [UIFont font12];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitle:@"查看详情" forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"pt_查看详情"] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.mySize = btn.size;
    btn.myCenterY = 0;
    [layout addSubview:btn];
}

- (MakeGroupListView *)goodView {
    if (!_goodView) {
        _goodView = [MakeGroupListView new];
        _goodView.myBottom = 10;
        _goodView.myTop = 10;
        _goodView.myLeft = 10;
        _goodView.myRight = 10;
        _goodView.height = 120;
        _goodView.myHeight = _goodView.height;
    }
    return _goodView;
}

- (UILabel *)payLbl {
    if (!_payLbl) {
        _payLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor blackColor];
            object.font = [UIFont font15];
            object.myCenterY = 0;
            object;
        });
    }
    return _payLbl;
}

@end
