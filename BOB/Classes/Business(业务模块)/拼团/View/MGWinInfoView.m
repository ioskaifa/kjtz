//
//  MGWinInfoView.m
//  BOB
//
//  Created by colin on 2020/12/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGWinInfoView.h"

@interface MGWinInfoView ()

@property (nonatomic, strong) MakeGroupListObj *obj;

@end

@implementation MGWinInfoView

- (instancetype)initWithOrderObj:(MakeGroupListObj *)obj {
    if (self = [super init]) {
        self.obj = obj;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 145;
}

- (void)createUI {
    MyLinearLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont font16];
    lbl.text = @"开奖信息";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 15;
    lbl.myTop = 10;
    lbl.myBottom = 10;
    [baseLayout addSubview:lbl];
    
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myLeft = 15;
    layout.myRight = 10;
    layout.weight = 1;
    layout.backgroundImage = [UIImage imageNamed:@"折扣券"];
        
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#C95A38"];
    lbl.font = [UIFont systemFontOfSize:42];
    lbl.text = self.obj.couponPrice;
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    lbl.myLeft = 80;
    [layout addSubview:lbl];
    
    [baseLayout addSubview:layout];
}

@end
