//
//  MGWinListCell.h
//  BOB
//
//  Created by colin on 2020/12/22.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MakeGroupListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGCountDownNumView : UIView

+ (CGSize)viewSize;

- (void)configureView:(NSInteger)day hour:(NSInteger)hour min:(NSInteger)min second:(NSInteger)sec;

@end

@interface MGWinListCell : UITableViewCell

@property (nonatomic, strong) MGCountDownNumView *countDownNumView;

+ (CGFloat)viewHeight;

- (void)configureView:(MakeGroupListObj *)obj;

@end

NS_ASSUME_NONNULL_END
