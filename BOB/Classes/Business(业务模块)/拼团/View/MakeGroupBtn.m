//
//  MakeGroupBtn.m
//  BOB
//
//  Created by colin on 2020/12/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MakeGroupBtn.h"

@interface MakeGroupBtn ()

@property (nonatomic, strong) UIButton *btn;

@property (nonatomic, strong) UIImageView *img;

@end

@implementation MakeGroupBtn

- (instancetype)init {
    if (self = [super init]) {
        [self createUI];
    }
    return self;
}

+ (CGSize)viewSize {
    return CGSizeMake(50, 60);
}

- (void)setTitle:(NSString *)title {
    _title = title;
    [self.btn setTitle:title forState:UIControlStateNormal];
    [self.btn setTitle:title forState:UIControlStateSelected];
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    self.btn.selected = selected;
    if (selected) {
        self.img.visibility = MyVisibility_Visible;
    } else {
        self.img.visibility = MyVisibility_Invisible;
    }
}
    
#pragma mark - InitUI
- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    [baseLayout addSubview:self.btn];
    [baseLayout addSubview:self.img];
}
    
- (UIButton *)btn {
    if (!_btn) {
        _btn = [UIButton buttonWithType:UIButtonTypeCustom];
        _btn.size = CGSizeMake(55, 20);
        [_btn setBackgroundImage:[UIImage imageNamed:@"疯抢中"] forState:UIControlStateNormal];
        [_btn setBackgroundImage:[UIImage imageNamed:@"已开奖"] forState:UIControlStateSelected];
        _btn.titleLabel.font = [UIFont font11];
        [_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];        
        _btn.mySize = _btn.size;
        _btn.myCenterX = 0;
        _btn.myTop = 15;
        _btn.userInteractionEnabled = NO;
    }
    return _btn;
}

- (UIImageView *)img {
    if (!_img) {
        _img = [UIImageView new];
        _img.image = [UIImage imageNamed:@"pt-select"];
        [_img sizeToFit];
        _img.mySize = _img.size;
        _img.myCenterX = 0;
        _img.myTop = 5;
    }
    return _img;
}

@end
