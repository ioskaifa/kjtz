//
//  MakeGroupListView.h
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MakeGroupListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MakeGroupListView : UIView

- (void)configureView:(MakeGroupListObj *)obj;

@end

NS_ASSUME_NONNULL_END
