//
//  MGGoodsOrderListCell.h
//  BOB
//
//  Created by colin on 2020/12/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGMyOrderListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGGoodsOrderListCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(MGMyOrderListObj *)obj;

@end

NS_ASSUME_NONNULL_END
