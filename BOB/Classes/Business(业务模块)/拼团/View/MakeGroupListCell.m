//
//  MakeGroupListCell.m
//  BOB
//
//  Created by colin on 2020/12/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MakeGroupListCell.h"
#import "MakeGroupListView.h"

@interface MakeGroupListCell ()

@property (nonatomic, strong) MakeGroupListView *listView;

@end

@implementation MakeGroupListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 150;
}

- (void)configureView:(MakeGroupListObj *)obj {
    if (![obj isKindOfClass:MakeGroupListObj.class]) {
        return;
    }
    [self.listView configureView:obj];
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor whiteColor];
    MyBaseLayout *baseLayout = [self.contentView addMyLinearLayout:MyOrientation_Vert];
    baseLayout.myLeft = 15;
    baseLayout.myRight = 15;
    baseLayout.myTop = 10;
    baseLayout.layer.borderColor = [UIColor moBackground].CGColor;
    baseLayout.layer.borderWidth = 1;
    [baseLayout setViewCornerRadius:5];
    baseLayout.gravity = MyGravity_Vert_Fill;
    
    [baseLayout addSubview:self.listView];
}

- (MakeGroupListView *)listView {
    if (!_listView) {
        _listView = [MakeGroupListView new];
        _listView.myLeft = 10;
        _listView.myRight = 10;
    }
    return _listView;
}

@end
