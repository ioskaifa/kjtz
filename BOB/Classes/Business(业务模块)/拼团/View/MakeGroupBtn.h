//
//  MakeGroupBtn.h
//  BOB
//
//  Created by colin on 2020/12/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MakeGroupBtn : UIView

@property (nonatomic, assign) BOOL selected;

@property (nonatomic, copy) NSString *title;

+ (CGSize)viewSize;

@end

NS_ASSUME_NONNULL_END
