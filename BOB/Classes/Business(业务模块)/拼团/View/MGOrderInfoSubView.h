//
//  MGOrderInfoSubView.h
//  BOB
//
//  Created by colin on 2020/12/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MakeGroupListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MGOrderInfoSubView : UIView

+ (CGFloat)viewHeight;

- (instancetype)initWithOrderObj:(MakeGroupListObj *)obj;

@end

NS_ASSUME_NONNULL_END
