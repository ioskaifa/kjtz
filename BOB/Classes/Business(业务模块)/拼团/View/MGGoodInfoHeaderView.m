//
//  MGGoodInfoHeaderView.m
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGGoodInfoHeaderView.h"
#import "GKCycleScrollView.h"

@interface MXCountDownNumView ()
///时
@property (nonatomic, strong) UILabel *hourLbl;
///分
@property (nonatomic, strong) UILabel *minLbl;
///秒
@property (nonatomic, strong) UILabel *secLbl;

@end

@implementation MXCountDownNumView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGSize)viewSize {
    return CGSizeMake(120, 30);
}

- (void)configureView:(NSInteger)hour min:(NSInteger)min second:(NSInteger)sec {
    self.hourLbl.text = StrF(@"%02ld", (long)hour);
    self.minLbl.text = StrF(@"%02ld", (long)min);
    self.secLbl.text = StrF(@"%02ld", (long)sec);
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Horz];
    baseLayout.gravity = MyGravity_Horz_Stretch;
    
    [baseLayout addSubview:self.hourLbl];
    UILabel *lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
    lbl.font = [UIFont font15];
    lbl.text = @":";
    lbl.myHeight = MyLayoutSize.fill;
    [baseLayout addSubview:lbl];
    
    [baseLayout addSubview:self.minLbl];
    lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
    lbl.font = [UIFont font15];
    lbl.text = @":";
    lbl.myHeight = MyLayoutSize.fill;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:self.secLbl];
}

- (UILabel *)hourLbl {
    if (!_hourLbl) {
        _hourLbl = [self.class factoryLbl];
    }
    return _hourLbl;
}

- (UILabel *)minLbl {
    if (!_minLbl) {
        _minLbl = [self.class factoryLbl];
    }
    return _minLbl;
}

- (UILabel *)secLbl {
    if (!_secLbl) {
        _secLbl = [self.class factoryLbl];
    }
    return _secLbl;
}

+ (UILabel *)factoryLbl {
    UILabel *lbl = [UILabel new];
    lbl.text = @"00";
    lbl.size = CGSizeMake(30, 30);
    lbl.mySize = lbl.size;
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont font14];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
    return lbl;
}

@end

@interface MGGoodInfoHeaderView ()<GKCycleScrollViewDataSource, GKCycleScrollViewDelegate>

@property (nonatomic, strong) MyBaseLayout *baseLayout;

///轮播图
@property (nonatomic, strong) GKCycleScrollView *cycleScrollView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, strong) UIProgressView *progressView;

@property (nonatomic, strong) MGGoodsInfoObj *goodObj;

@end

@implementation MGGoodInfoHeaderView

- (instancetype)initWithGoodsInfoObj:(MGGoodsInfoObj *)obj {
    if (self = [super init]) {
        self.goodObj = obj;
        [self createUI];
    }
    return self;
}

- (CGFloat)viewHeight {
    [self.baseLayout layoutIfNeeded];
    return self.baseLayout.height;
}

- (void)configureView:(MGGoodsInfoObj *)obj {
    if (![obj isKindOfClass:MGGoodsInfoObj.class]) {
        return;
    }
    [self.dataSourceMArr removeAllObjects];
    [self.dataSourceMArr addObjectsFromArray:obj.goods_navigationArr];
    [self.cycleScrollView reloadData];
}

- (void)configureProgressView:(CGFloat)progress {
    if (progress <= 0) {
        progress = 0;
    }
    if (progress >= 1) {
        progress = 1;
    }
    self.progressView.progress = progress;
}

#pragma mark - Delegate
#pragma mark GKCycleScrollViewDataSource
- (NSInteger)numberOfCellsInCycleScrollView:(GKCycleScrollView *)cycleScrollView {
    return self.dataSourceMArr.count;
}

- (GKCycleScrollViewCell *)cycleScrollView:(GKCycleScrollView *)cycleScrollView cellForViewAtIndex:(NSInteger)index {
    GKCycleScrollViewCell *cell = [cycleScrollView dequeueReusableCell];
    if (!cell) {
        cell = [GKCycleScrollViewCell new];
    }
    if (index < self.dataSourceMArr.count) {
        NSString *img_url = self.dataSourceMArr[index];
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
        }];
    }
    cell.imageView.contentMode = UIViewContentModeScaleToFill;
    return cell;
}

- (void)cycleScrollView:(GKCycleScrollView *)cycleScrollView didSelectCellAtCell:(GKCycleScrollViewCell *)cell
                atIndex:(NSInteger)index {
    if (index >= self.dataSourceMArr.count) {
        return;
    }
}

- (void)createUI {
    [self addSubview:self.baseLayout];
    [self.baseLayout addSubview:self.cycleScrollView];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = 50;
    [self.baseLayout addSubview:layout];
    
    SPButton *btn = [SPButton new];
    btn.userInteractionEnabled = NO;
    btn.imagePosition = SPButtonImagePositionLeft;
    btn.imageTitleSpace = 10;
    btn.titleLabel.font = [UIFont font15];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"拼团" forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"pt_logo"] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.mySize = btn.size;
    btn.myCenterY = 0;
    btn.myLeft = 15;
    [layout addSubview:btn];
    [layout addSubview:[UIView placeholderHorzView]];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myWidth = MyLayoutSize.wrap;
    subLayout.myCenterY = 0;
    subLayout.myRight = 15;
    [layout addSubview:subLayout];
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont font11];
    lbl.text = @"当前进度";
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    [subLayout addSubview:lbl];
    
//    self.goodObj.progress = 0.89;
    UIProgressView *progressView = [UIProgressView new];
    [progressView setViewCornerRadius:1.5];
    progressView.backgroundColor = [UIColor whiteColor];
    progressView.progressTintColor = [UIColor colorWithHexString:@"#02C1C4"];
    progressView.progress = self.goodObj.progress;
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 1.0f);
    progressView.transform = transform;
    progressView.myWidth = 80;
    progressView.myCenterY = 0;
    progressView.myLeft = 10;
    progressView.myRight = 10;
    self.progressView = progressView;
    [subLayout addSubview:progressView];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont font16];
    lbl.text = StrF(@"%0.0f%@", self.goodObj.progress*100, @"%");
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    [subLayout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.attributedText = self.goodObj.couponPriceAtt;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    lbl.myLeft = 15;
    lbl.myTop = 10;
    lbl.myBottom = 10;
    [self.baseLayout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.numberOfLines = 0;
    lbl.font = [UIFont boldFont15];
    lbl.text = self.goodObj.goods_name;
    size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    lbl.myLeft = 15;
    lbl.myBottom = 10;
    [self.baseLayout addSubview:lbl];
    
    UIButton *subBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [subBtn setBackgroundImage:[UIImage imageNamed:@"拼团"] forState:UIControlStateNormal];
    subBtn.userInteractionEnabled = NO;
    subBtn.size = CGSizeMake(35, 20);
    [subBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [subBtn setTitle:@"拼团" forState:UIControlStateNormal];
    subBtn.titleLabel.font = [UIFont font11];
    [lbl addSubview:subBtn];
    [subBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(subBtn.size);
        make.left.mas_equalTo(lbl.mas_left);
        make.top.mas_equalTo(lbl.mas_top).mas_offset(-2);
    }];
    
    [self.baseLayout addSubview:[UIView fullLine]];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = 50;
    layout.gravity = MyGravity_Horz_Between;
    [self.baseLayout addSubview:layout];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.numberOfLines = 0;
    lbl.font = [UIFont boldFont16];
    lbl.text = @"距拼团结束时间";
    lbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    [layout addSubview:lbl];
    [layout addSubview:self.countDownNumView];
    
    
    [self.baseLayout addSubview:[UIView gapLine]];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myTop = 10;
    layout.myBottom = 10;
    layout.myHeight = MyLayoutSize.wrap;
    self.ruleView = layout;
    [self.baseLayout addSubview:layout];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont boldFont15];
    lbl.text = @"拼团规则";
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    lbl.myLeft = 0;
    lbl.myRight = 20;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont font12];
    lbl.text = @"1.拼团参团 > 2.满员开团 > 3.中奖查询";
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    lbl.myLeft = 0;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView placeholderHorzView]];
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"Arrow"];
    imgView.myRight = 0;
    imgView.myCenterY = 0;
    [layout addSubview:imgView];
    
    [self.baseLayout addSubview:[UIView gapLine]];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont boldFont15];
    lbl.text = @"商品详情";
    [lbl autoMyLayoutSize];
    lbl.myTop = 10;
    lbl.myLeft = 15;
    lbl.myBottom = 10;
    [self.baseLayout addSubview:lbl];
    
    [layout addSubview:[UIView gapLine]];
    [self configureView:self.goodObj];
}

#pragma mark - Init
- (MyBaseLayout *)baseLayout {
    if (!_baseLayout) {
        _baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _baseLayout.myTop = 0;
        _baseLayout.myLeft = 0;
        _baseLayout.myRight = 0;
        _baseLayout.myHeight = MyLayoutSize.wrap;
        _baseLayout.backgroundColor = [UIColor whiteColor];
    }
    return _baseLayout;
}

#pragma mark - 轮播图
- (GKCycleScrollView *)cycleScrollView {
    if (!_cycleScrollView) {
        _cycleScrollView = [[GKCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 350)];
        _cycleScrollView.pageControl = self.pageControl;
        _cycleScrollView.isAutoScroll = YES;
        _cycleScrollView.dataSource = self;
        _cycleScrollView.delegate = self;
        _cycleScrollView.backgroundColor = [UIColor moBackground];
        _cycleScrollView.myTop = 0;
        _cycleScrollView.myLeft = 0;
        _cycleScrollView.myRight = 0;
        _cycleScrollView.myBottom = 0;
        _cycleScrollView.myHeight = _cycleScrollView.height;
        _cycleScrollView.isAutoScroll = NO;
        [_cycleScrollView addSubview:self.pageControl];
        _cycleScrollView.backgroundColor = [UIColor moBackground];
    }
    return _cycleScrollView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [UIPageControl new];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.numberOfPages = self.dataSourceMArr.count;
        _pageControl.frame = CGRectMake(20, self.cycleScrollView.height - 20, SCREEN_WIDTH - 40 - 30, 20);
    }
    return _pageControl;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (MXCountDownNumView *)countDownNumView {
    if (!_countDownNumView) {
        _countDownNumView = [MXCountDownNumView new];
        _countDownNumView.size = [MXCountDownNumView viewSize];
        _countDownNumView.mySize = _countDownNumView.size;
        _countDownNumView.myCenterY = 0;
    }
    return _countDownNumView;
}

@end
