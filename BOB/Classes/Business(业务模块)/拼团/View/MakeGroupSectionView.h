//
//  MakeGroupSectionView.h
//  BOB
//
//  Created by colin on 2020/12/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MakeGroupSectionView;

NS_ASSUME_NONNULL_BEGIN

@protocol MakeGroupSectionViewDelegate <NSObject>

- (void)MakeGroupView:(MakeGroupSectionView *)view didSelectCellAtIndex:(NSInteger)index;

@end

@interface MakeGroupSectionView : UITableViewHeaderFooterView

@property (nonatomic, weak) id <MakeGroupSectionViewDelegate>delegate;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
