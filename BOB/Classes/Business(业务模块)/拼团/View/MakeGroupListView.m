//
//  MakeGroupListView.m
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MakeGroupListView.h"

@interface MakeGroupListView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) UILabel *couponLbl;

@property (nonatomic, strong) UILabel *statuLbl;

@property (nonatomic, strong) UILabel *imgStatuLbl;

@end

@implementation MakeGroupListView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)configureView:(MakeGroupListObj *)obj {
    if (![obj isKindOfClass:MakeGroupListObj.class]) {
        return;
    }
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.photo]];
    self.titleLbl.text = obj.title;
    CGFloat width = SCREEN_WIDTH - 60 - self.imgView.width;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    self.priceLbl.text = obj.price;
    if (obj.orderStatus == MGOrderStatusNot ||
        obj.orderStatus == MGOrderStatusFail) {
        self.couponLbl.attributedText = obj.refund_sla_numAtt;
    } else {
        self.couponLbl.attributedText = obj.couponPriceAtt;
    }
    
    [self configureStatu:obj];
}

- (void)configureStatu:(MakeGroupListObj *)obj {
    switch (obj.orderStatus) {
        case MGOrderStatusIng:{
            [self configureStatuIng];
            break;
        }
        case MGOrderStatusEnd:{
            [self configureStatuEnd];
            break;
        }
        case MGOrderStatusWait:{
            [self configureStatuWait];
            break;
        }
        case MGOrderStatusNot:{
            [self configureStatuNot];
            break;
        }
        case MGOrderStatusWin:{
            [self configureStatuWin];
            break;
        }
        case MGOrderStatusFail:{
            [self configureStatuFail];
            break;
        }
        case MGOrderStatusInfo:{
            [self configureStatuInfo];
            break;
        }
        default:
            break;
    }
}

- (void)configureStatuIng {
    self.statuLbl.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
    self.statuLbl.text = @"立即参与";
    
    self.imgStatuLbl.text = @"即将开奖";
    self.imgStatuLbl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    self.imgStatuLbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
}

- (void)configureStatuEnd {
    self.statuLbl.backgroundColor = [UIColor colorWithHexString:@"#7F7F7F"];
    self.statuLbl.text = @"已结束";
    
    self.imgStatuLbl.text = @"已结束";
    self.imgStatuLbl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    self.imgStatuLbl.textColor = [UIColor whiteColor];
}

- (void)configureStatuWait {
    self.statuLbl.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
    self.statuLbl.text = @"待开奖";
    
    self.imgStatuLbl.text = @"即将开奖";
    self.imgStatuLbl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    self.imgStatuLbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
}

- (void)configureStatuNot {
    self.statuLbl.backgroundColor = [UIColor colorWithHexString:@"#7F7F7F"];
    self.statuLbl.text = @"未中奖";
    
    self.imgStatuLbl.text = @"已开奖";
    self.imgStatuLbl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    self.imgStatuLbl.textColor = [UIColor whiteColor];
}

- (void)configureStatuWin {
    self.statuLbl.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
    self.statuLbl.text = @"中奖了";
    
    self.imgStatuLbl.text = @"已开奖";
    self.imgStatuLbl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    self.imgStatuLbl.textColor = [UIColor whiteColor];
}

- (void)configureStatuFail {
    self.statuLbl.backgroundColor = [UIColor colorWithHexString:@"#7F7F7F"];
    self.statuLbl.text = @"拼团失败";
    
    self.imgStatuLbl.text = @"已开奖";
    self.imgStatuLbl.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
    self.imgStatuLbl.textColor = [UIColor whiteColor];
}

- (void)configureStatuInfo {
    [self configureStatuNot];
    self.imgStatuLbl.hidden = YES;
    self.statuLbl.visibility = MyVisibility_Invisible;
    self.imgView.myLeft = 10;
}

- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Horz];
    
    [baseLayout addSubview:self.imgView];
    [self.imgView addSubview:self.imgStatuLbl];
    [self.imgStatuLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.imgStatuLbl.height);
        make.left.and.right.and.bottom.mas_equalTo(self.imgView);
    }];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.weight = 1;
    layout.myLeft = 10;
    layout.myRight = 0;
    layout.height = self.imgView.height;
    layout.myHeight = layout.height;
    layout.myCenterY = 0;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.titleLbl];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"拼团"] forState:UIControlStateNormal];
    btn.userInteractionEnabled = NO;
    btn.size = CGSizeMake(35, 20);
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"拼团" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont font11];
    [self.titleLbl addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(btn.size);
        make.left.mas_equalTo(self.titleLbl.mas_left);
        make.top.mas_equalTo(self.titleLbl.mas_top).mas_offset(-2);
    }];
    
    [layout addSubview:self.priceLbl];
    [layout addSubview:[UIView placeholderVertView]];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myHeight = 25;
    subLayout.myBottom = 0;
    [layout addSubview:subLayout];
    
    [subLayout addSubview:self.couponLbl];
    [subLayout addSubview:self.statuLbl];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.size = CGSizeMake(120, 120);
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 0;
            [object setViewCornerRadius:5];
            object.backgroundColor = [UIColor moBackground];
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 2;
            object.font = [UIFont font15];
            object.textColor = [UIColor blackColor];
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor blackColor];
            object.myLeft = 0;
            object.myTop = 10;
            object.myRight = 0;
            object.myHeight = 20;
            object;
        });
    }
    return _priceLbl;
}

- (UILabel *)couponLbl {
    if (!_couponLbl) {
        _couponLbl = ({
            UILabel *object = [UILabel new];
            object.myLeft = 0;
            object.myRight = 5;
            object.weight = 1;
            object.myHeight = 20;
            object;
        });
    }
    return _couponLbl;
}

- (UILabel *)statuLbl {
    if (!_statuLbl) {
        _statuLbl = ({
            UILabel *object = [UILabel new];
            object.textAlignment = NSTextAlignmentCenter;
            object.size = CGSizeMake(60, 25);
            object.mySize = object.size;
            [object setViewCornerRadius:5];
            object.font = [UIFont font12];
            object.textColor = [UIColor whiteColor];
            object;
        });
    }
    return _statuLbl;
}

- (UILabel *)imgStatuLbl {
    if (!_imgStatuLbl) {
        _imgStatuLbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 1;
            object.textAlignment = NSTextAlignmentCenter;
            object.font = [UIFont font15];
            object.height = 30;
            object;
        });
    }
    return _imgStatuLbl;
}

@end
