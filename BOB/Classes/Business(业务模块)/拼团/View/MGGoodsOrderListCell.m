//
//  MGGoodsOrderListCell.m
//  BOB
//
//  Created by colin on 2020/12/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGGoodsOrderListCell.h"

@interface MGGoodsOrderListCell ()

@property (nonatomic, strong) UILabel *statusLbl;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *couponLbl;

@property (nonatomic, strong) UILabel *totalLbl;

@property (nonatomic, strong) MGMyOrderListObj *obj;

@end

@implementation MGGoodsOrderListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 185;
}

- (void)configureView:(MGMyOrderListObj *)obj {
    if (![obj isKindOfClass:MGMyOrderListObj.class]) {
        return;
    }
    self.statusLbl.text = obj.formatStatus;
    [self.statusLbl autoMyLayoutSize];
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.goods_show]];
    self.priceLbl.attributedText = obj.priceAtt;
    [self.priceLbl autoMyLayoutSize];
    
    self.titleLbl.text = obj.goods_name;
    CGFloat width = SCREEN_WIDTH - 70 - self.imgView.width - self.priceLbl.width;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    self.couponLbl.text = obj.couponString;
    [self.couponLbl autoMyLayoutSize];
    size = self.couponLbl.size;
    self.couponLbl.size = CGSizeMake(size.width + 10, size.height + 5);
    self.couponLbl.mySize = self.couponLbl.size;
    
    self.totalLbl.attributedText = obj.totalAtt;
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    MyBaseLayout *baseLayout = [self.contentView addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    [baseLayout setViewCornerRadius:5];
    baseLayout.myTop = 5;
    baseLayout.myLeft = 15;
    baseLayout.myRight = 15;
    baseLayout.myBottom = 5;
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 10;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 10;
    layout.myHeight = MyLayoutSize.wrap;
    [baseLayout addSubview:layout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor blackColor];
    lbl.myCenterY = 0;
    lbl.text = @"数字商城";
    [lbl autoMyLayoutSize];
    [layout addSubview:lbl];
    [layout addSubview:[UIView placeholderHorzView]];
    [layout addSubview:self.statusLbl];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.height = self.imgView.height;
    layout.myHeight = layout.height;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.imgView];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.weight = 1;
    subLayout.myLeft = 10;
    subLayout.myRight = 0;
    subLayout.height = self.imgView.height;
    subLayout.myHeight = subLayout.height;
    [layout addSubview:subLayout];
    
    MyLinearLayout *topLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    topLayout.weight = 1;
    topLayout.myLeft = 0;
    topLayout.myRight = 0;
    [subLayout addSubview:topLayout];
    
    [topLayout addSubview:self.titleLbl];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"拼团"] forState:UIControlStateNormal];
    btn.userInteractionEnabled = NO;
    btn.size = CGSizeMake(35, 20);
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"拼团" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont font11];
    [self.titleLbl addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(btn.size);
        make.left.mas_equalTo(self.titleLbl.mas_left);
        make.top.mas_equalTo(self.titleLbl.mas_top).mas_offset(-2);
    }];
    [topLayout addSubview:[UIView placeholderHorzView]];
    [topLayout addSubview:self.priceLbl];
    topLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    topLayout.myHeight = 25;
    topLayout.myLeft = 0;
    topLayout.myRight = 0;
    [topLayout addSubview:[UIView placeholderHorzView]];
    [topLayout addSubview:self.couponLbl];
    [subLayout addSubview:topLayout];
    
    [baseLayout addSubview:self.totalLbl];
}

- (UILabel *)statusLbl {
    if (!_statusLbl) {
        _statusLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor colorWithHexString:@"#FF4757"];
            object.font = [UIFont font15];
            object.myCenterY = 0;
            object.myRight = 0;
            object;
        });
    }
    return _statusLbl;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.size = CGSizeMake(100, 100);
            object.mySize = object.size;
            object.myLeft = 0;
            [object setViewCornerRadius:5];
            object.backgroundColor = [UIColor moBackground];
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 2;
            object.font = [UIFont lightFont15];
            object.textColor = [UIColor blackColor];
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.numberOfLines = 0;
        _priceLbl.myLeft = 10;
        _priceLbl.myRight = 0;
    }
    return _priceLbl;
}

- (UILabel *)couponLbl {
    if (!_couponLbl) {
        _couponLbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 1;
            object.textAlignment = NSTextAlignmentCenter;
            [object setViewCornerRadius:2];
            object.font = [UIFont font14];
            object.textColor = [UIColor colorWithHexString:@"#FF4757"];
            object.layer.borderColor = [UIColor colorWithHexString:@"#FF4757"].CGColor;
            object.layer.borderWidth = 1;
            object;
        });
    }
    return _couponLbl;
}

- (UILabel *)totalLbl {
    if (!_totalLbl) {
        _totalLbl = ({
            UILabel *object = [UILabel new];
            object.myHeight = 25;
            object.myTop = 10;
            object.myBottom = 0;
            object.myLeft = 0;
            object.myRight = 10;
            object;
        });
    }
    return _totalLbl;
}

@end
