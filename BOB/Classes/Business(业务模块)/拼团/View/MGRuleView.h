//
//  MGRuleView.h
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGRuleView : UIView

@property (nonatomic, strong) UILabel *knownlbl;

@property (nonatomic, strong) UITextView *tv;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
