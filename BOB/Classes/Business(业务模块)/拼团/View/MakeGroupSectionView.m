//
//  MakeGroupSectionView.m
//  BOB
//
//  Created by colin on 2020/12/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MakeGroupSectionView.h"
#import "MakeGroupBtn.h"

@interface MakeGroupSectionView ()

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) NSMutableArray *btnMArr;

@property (nonatomic, strong) MakeGroupBtn *selectBtn;

@end

@implementation MakeGroupSectionView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 50;
}

- (void)MakeGroupBtnClick:(MakeGroupBtn *)sender {
    if (sender == self.selectBtn) {
        return;
    }
    self.selectBtn.selected = NO;
    self.selectBtn = sender;
    self.selectBtn.selected = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(MakeGroupView:didSelectCellAtIndex:)]) {
        [self.delegate MakeGroupView:self didSelectCellAtIndex:sender.tag];
    }
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor whiteColor];
    MyBaseLayout *baseLayout = [self.contentView addMyLinearLayout:MyOrientation_Horz];    
    baseLayout.gravity = MyGravity_Horz_Around;
    for (NSInteger i = 0; i < self.dataArr.count; i++) {
        MakeGroupBtn *btn = [MakeGroupBtn new];
        btn.size = [MakeGroupBtn viewSize];
        btn.mySize = btn.size;
        btn.tag = i;
        btn.title = self.dataArr[i];
        if (i == 0) {
            btn.selected = YES;
            self.selectBtn = btn;
        } else {
            btn.selected = NO;
        }
        [baseLayout addSubview:btn];
        [self.btnMArr addObject:btn];
        @weakify(self)
        [btn addAction:^(UIView *view) {
            @strongify(self)
            if (![view isKindOfClass:MakeGroupBtn.class]) {
                return;
            }
            [self MakeGroupBtnClick:(MakeGroupBtn *)view];
        }];
    }
}

#pragma mark - Init
- (NSArray *)dataArr {
    if (!_dataArr) {
        _dataArr = @[@"已结束", @"疯抢中"];
    }
    return _dataArr;
}

- (NSMutableArray *)btnMArr {
    if (!_btnMArr) {
        _btnMArr = [NSMutableArray arrayWithCapacity:self.dataArr.count];
    }
    return _btnMArr;
}

@end
