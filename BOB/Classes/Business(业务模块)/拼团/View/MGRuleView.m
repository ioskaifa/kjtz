//
//  MGRuleView.m
//  BOB
//
//  Created by colin on 2020/12/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGRuleView.h"

@implementation MGRuleView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return SCREEN_HEIGHT * 0.6;
}

#pragma mark - InitUI
- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [baseLayout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"拼团规则";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 20;
    lbl.myBottom = 15;
    lbl.myCenterX = 0;
    [baseLayout addSubview:lbl];
    
    UITextView *tv = [UITextView new];
    tv.showsVerticalScrollIndicator = NO;
    tv.showsHorizontalScrollIndicator = NO;
    tv.editable = NO;
    tv.weight = 1;
    tv.textColor = [UIColor blackColor];
    tv.text = @"1.点击“立即参团”按钮\n\n2.提交并成功支付订单\n\n3.满员开奖，在“我的拼团”查询拼团结果";
    tv.myLeft = 25;
    tv.myRight = 25;
    tv.myBottom = 10;
    self.tv = tv;
    [baseLayout addSubview:tv];
    
    lbl = [UILabel new];
    lbl.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
    lbl.textColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.numberOfLines = 0;
    lbl.font = [UIFont font15];
    lbl.text = @"我知道了";
    lbl.size = CGSizeMake(SCREEN_WIDTH, 60);
    lbl.mySize = lbl.size;
    lbl.myLeft = 0;
    lbl.myBottom = 0;
    self.knownlbl = lbl;
    [baseLayout addSubview:lbl];
}

@end
