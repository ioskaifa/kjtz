//
//  MakeGroupListCell.h
//  BOB
//
//  Created by colin on 2020/12/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MakeGroupListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MakeGroupListCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(MakeGroupListObj *)obj;

@end

NS_ASSUME_NONNULL_END
