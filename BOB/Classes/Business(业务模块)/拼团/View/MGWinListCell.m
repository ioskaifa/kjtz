//
//  MGWinListCell.m
//  BOB
//
//  Created by colin on 2020/12/22.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGWinListCell.h"

@interface MGCountDownNumView ()
///天
@property (nonatomic, strong) UILabel *dayLbl;
///时
@property (nonatomic, strong) UILabel *hourLbl;
///分
@property (nonatomic, strong) UILabel *minLbl;
///秒
@property (nonatomic, strong) UILabel *secLbl;

@end

@implementation MGCountDownNumView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGSize)viewSize {
    return CGSizeMake(160, 25);
}

- (void)configureView:(NSInteger)countDownNum {
    NSInteger timeout = countDownNum;
    NSInteger days = timeout/(3600*24);
    NSInteger hours = (timeout-days*24*3600)/3600;
    NSInteger minute = (timeout-days*24*3600-hours*3600)/60;
    NSInteger second = timeout-days*24*3600-hours*3600-minute*60;
    [self configureView:days hour:hours min:minute second:second];
}

- (void)configureView:(NSInteger)day hour:(NSInteger)hour min:(NSInteger)min second:(NSInteger)sec {
    self.dayLbl.text = StrF(@"%02ld", (long)day);
    self.hourLbl.text = StrF(@"%02ld", (long)hour);
    self.minLbl.text = StrF(@"%02ld", (long)min);
    self.secLbl.text = StrF(@"%02ld", (long)sec);
}

- (void)createUI {
    MyFrameLayout *baseLayout = [MyFrameLayout new];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.size = [self.class viewSize];
    baseLayout.mySize = baseLayout.size;
    [baseLayout az_setGradientBackgroundWithColors:@[[UIColor whiteColor],[UIColor colorWithHexString:@"#F4DDDC"],[UIColor colorWithHexString:@"#DC816E"]] locations:nil startPoint:CGPointMake(0, 0) endPoint:CGPointMake(1, 0)];
    [self addSubview:baseLayout];
    
    MyBaseLayout *layout = [self addMyLinearLayout:MyOrientation_Horz];
    layout.gravity = MyGravity_Horz_Among;
    layout.myLeft = 15;
    layout.myTop = 2.5;
    layout.myRight = 15;
    layout.myHeight = MyLayoutSize.fill;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.dayLbl];
    UILabel *lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor colorWithHexString:@"#E4735B"];
    lbl.font = [UIFont font14];
    lbl.text = @"天";
    lbl.myCenterY = 0;
    [lbl autoMyLayoutSize];
    [layout addSubview:lbl];
    
    [layout addSubview:self.hourLbl];
    lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor colorWithHexString:@"#E4735B"];
    lbl.font = [UIFont font14];
    lbl.text = @":";
    lbl.myCenterY = -1;
    [lbl autoMyLayoutSize];
    [layout addSubview:lbl];
    
    [layout addSubview:self.minLbl];
    lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor colorWithHexString:@"#E4735B"];
    lbl.font = [UIFont font14];
    lbl.text = @":";
    lbl.myCenterY = -1;
    [lbl autoMyLayoutSize];
    [layout addSubview:lbl];
    [layout addSubview:self.secLbl];
}

- (UILabel *)dayLbl {
    if (!_dayLbl) {
        _dayLbl = [self.class factoryLbl];
    }
    return _dayLbl;
}

- (UILabel *)hourLbl {
    if (!_hourLbl) {
        _hourLbl = [self.class factoryLbl];
    }
    return _hourLbl;
}

- (UILabel *)minLbl {
    if (!_minLbl) {
        _minLbl = [self.class factoryLbl];
    }
    return _minLbl;
}

- (UILabel *)secLbl {
    if (!_secLbl) {
        _secLbl = [self.class factoryLbl];
    }
    return _secLbl;
}

+ (UILabel *)factoryLbl {
    UILabel *lbl = [UILabel new];
    lbl.text = @"00";
    lbl.size = CGSizeMake(20, 20);
    lbl.mySize = lbl.size;
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont font14];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.backgroundColor = [UIColor colorWithHexString:@"#E4735B"];
    return lbl;
}

@end

@interface MGWinListCell ()

@property (nonatomic, strong) UIImageView *statusImgView;

@property (nonatomic, strong) UILabel *couponLbl;

@property (nonatomic, strong) UILabel *tipLbl;

@end

@implementation MGWinListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 130;
}

- (void)configureView:(MakeGroupListObj *)obj {
    if (![obj isKindOfClass:MakeGroupListObj.class]) {
        return;
    }
    self.statusImgView.image = [UIImage imageNamed:obj.coupon_status_string];
    self.couponLbl.attributedText = obj.couponPriceExAtt;
    [self.couponLbl autoMyLayoutSize];
    [self.countDownNumView configureView:obj.countDownNum];
    if ([@"00" isEqualToString:obj.coupon_status]) {
        self.tipLbl.text = @"立即使用";
    } else {
        self.tipLbl.text = @"查看详情";
    }
}

- (void)createUI {
    UIView *superView = self.contentView;
    superView.backgroundColor = [UIColor whiteColor];
    MyBaseLayout *baseLayout = [superView addMyLinearLayout:MyOrientation_Horz];
    baseLayout.myLeft = 15;
    baseLayout.myRight = 15;
    baseLayout.myTop = 10;
    baseLayout.myBottom = 10;
    baseLayout.backgroundImage = [UIImage imageNamed:@"抵扣券bg"];
    
    MyLinearLayout *leftLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    leftLayout.weight = 1;
    leftLayout.myHeight = MyLayoutSize.fill;
    [baseLayout addSubview:leftLayout];
    
    [leftLayout addSubview:[UIView placeholderVertView]];
    [leftLayout addSubview:self.countDownNumView];
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont font13];
    lbl.text = @"有效期剩余";
    [lbl autoMyLayoutSize];
    lbl.myLeft = (self.countDownNumView.width - lbl.width)/2.0;
    lbl.myTop = 2;
    lbl.myBottom = 2;
    [leftLayout addSubview:lbl];
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myHeight = MyLayoutSize.fill;
    rightLayout.myWidth = MyLayoutSize.wrap;
    [baseLayout addSubview:rightLayout];
    [rightLayout addSubview:[UIView placeholderVertView]];
    [rightLayout addSubview:self.couponLbl];
    lbl = [UILabel new];
    lbl.size = CGSizeMake(65, 20);
    lbl.mySize = lbl.size;
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont font13];
    lbl.text = @"立即使用";
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.layer.borderWidth = 1;
    lbl.layer.borderColor = [UIColor whiteColor].CGColor;
    [lbl setViewCornerRadius:lbl.height/2.0];
    self.tipLbl = lbl;
    lbl.myTop = 10;
    lbl.myCenterX = 0;
    lbl.myBottom = 10;
    [rightLayout addSubview:lbl];
    
    [superView addSubview:self.statusImgView];
    [self.statusImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.statusImgView.size);
        make.top.mas_equalTo(superView.mas_top).mas_offset(10);
        make.right.mas_equalTo(superView.mas_right).mas_offset(-15);
    }];
}

- (MGCountDownNumView *)countDownNumView {
    if (!_countDownNumView) {
        _countDownNumView = [MGCountDownNumView new];
        _countDownNumView.size = [MGCountDownNumView viewSize];
        _countDownNumView.mySize = _countDownNumView.size;
        _countDownNumView.myLeft = 2;
    }
    return _countDownNumView;
}

- (UIImageView *)statusImgView {
    if (!_statusImgView) {
        _statusImgView = [UIImageView new];
        _statusImgView.size = CGSizeMake(70, 70);
    }
    return _statusImgView;
}

- (UILabel *)couponLbl {
    if (!_couponLbl) {
        _couponLbl = [UILabel new];
        _couponLbl.myRight = 15;
    }
    return _couponLbl;
}

@end
