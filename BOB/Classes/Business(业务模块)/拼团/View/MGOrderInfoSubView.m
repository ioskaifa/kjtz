//
//  MGOrderInfoSubView.m
//  BOB
//
//  Created by colin on 2020/12/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGOrderInfoSubView.h"

@interface MGOrderInfoSubView ()

@property (nonatomic, strong) MakeGroupListObj *obj;

@end

@implementation MGOrderInfoSubView

- (instancetype)initWithOrderObj:(MakeGroupListObj *)obj {
    if (self = [super init]) {
        self.obj = obj;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 190;
}

- (void)createUI {
    MyLinearLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont font15];
    lbl.text = @"订单信息";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 15;
    lbl.myTop = 10;
    [baseLayout addSubview:lbl];
    
    NSString *string = @"否";
    if ([@"09" isEqualToString:self.obj.status]) {
        string = @"是";
    }
    [baseLayout addSubview:[self.class factoryLayout:@"是否开奖:" rightTitle:string]];
    [baseLayout addSubview:[self.class factoryLayout:@"支付方式:" rightTitle:@"SLA"]];
    [baseLayout addSubview:[self.class factoryLayout:@"支付数量:" rightTitle:self.obj.pay_sla_num]];
    [baseLayout addSubview:[self.class factoryLayout:@"订单编号:" rightTitle:self.obj.order_no]];
    [baseLayout addSubview:[self.class factoryLayout:@"付款时间:" rightTitle:self.obj.format_cre_time]];
}

+ (MyBaseLayout *)factoryLayout:(NSString *)leftTxt
                     rightTitle:(NSString *)rightTxt {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 14;
    layout.myRight = 10;
    layout.myTop = 10;
    layout.myHeight = 20;
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#666666"];
    lbl.font = [UIFont font14];
    lbl.text = leftTxt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#666666"];
    lbl.font = [UIFont font14];
    lbl.text = rightTxt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 30;
    lbl.weight = 1;
    lbl.tag = 1;
    [layout addSubview:lbl];
    return layout;
}

@end
