//
//  MGOrderInfoStatusView.m
//  BOB
//
//  Created by colin on 2020/12/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MGOrderInfoStatusView.h"

@interface MGStatusBtn : UIView

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UIView *leftLine;

@property (nonatomic, strong) UIView *rightLine;

@end

@implementation MGStatusBtn

- (instancetype)initWithTitle:(NSString *)title {
    if (self = [super init]) {
        [self createUI:title];
    }
    return self;
}

- (void)createUI:(NSString *)title {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    self.titleLbl.text = title;
    [self.titleLbl autoMyLayoutSize];
    [baseLayout addSubview:self.titleLbl];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 10;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    layout.gravity = MyGravity_Horz_Stretch;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.leftLine];
    [layout addSubview:self.imgView];
    [layout addSubview:self.rightLine];
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor blackColor];
            object.myCenterX = 0;
            object;
        });
    }
    return _titleLbl;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView autoLayoutImgView:@"pt_select"];
        _imgView.myCenterY = 0;
    }
    return _imgView;
}

- (UIView *)leftLine {
    if (!_leftLine) {
        _leftLine = [UIView new];
        _leftLine.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
        _leftLine.myHeight = 1;
        _leftLine.myCenterY = 0;
    }
    return _leftLine;
}

- (UIView *)rightLine {
    if (!_rightLine) {
        _rightLine = [UIView new];
        _rightLine.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
        _rightLine.myHeight = 1;
        _rightLine.myCenterY = 0;
    }
    return _rightLine;
}

@end

@interface MGOrderInfoStatusView ()

@property (nonatomic, strong) UIButton *statusBtn;
///已付款
@property (nonatomic, strong) MGStatusBtn *payBtn;
///已成团
@property (nonatomic, strong) MGStatusBtn *hasMGBtn;
///已开奖
@property (nonatomic, strong) MGStatusBtn *endBtn;

@end

@implementation MGOrderInfoStatusView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 110;
}

- (void)configureView:(MGOrderStatus)orderStatus {
    [self.statusBtn setTitle:[MakeGroupListObj orderStatusToDescription:orderStatus] forState:UIControlStateNormal];
    switch (orderStatus) {
        case MGOrderStatusWait:{
            self.endBtn.imgView.image = [UIImage imageNamed:@"pt_normal"];
            self.hasMGBtn.rightLine.backgroundColor = [UIColor colorWithHexString:@"#999999"];
            self.endBtn.leftLine.backgroundColor = [UIColor colorWithHexString:@"#999999"];
            break;
        }
        case MGOrderStatusNot:{
            
            break;
        }
        case MGOrderStatusWin:{
            
            break;
        }
        case MGOrderStatusFail:{
            self.hasMGBtn.imgView.image = [UIImage imageNamed:@"pt_fail"];
            self.endBtn.imgView.image = [UIImage imageNamed:@"pt_normal"];
            self.hasMGBtn.rightLine.backgroundColor = [UIColor colorWithHexString:@"#999999"];
            self.endBtn.leftLine.backgroundColor = [UIColor colorWithHexString:@"#999999"];
            break;
        }
        default:
            break;
    }
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    [baseLayout setViewCornerRadius:5];
    baseLayout.backgroundColor = [UIColor whiteColor];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont font13];
    [btn setBackgroundImage:[UIImage imageNamed:@"订单详情状态"] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"" forState:UIControlStateNormal];
    btn.size = CGSizeMake(70, 25);
    btn.mySize = btn.size;
    btn.myLeft = 0;
    btn.myTop = 10;
    self.statusBtn = btn;
    [baseLayout addSubview:btn];
    
    [baseLayout addSubview:[UIView placeholderVertView]];
    
    MyFrameLayout *frameLayout = [MyFrameLayout new];
    frameLayout.myLeft = 0;
    frameLayout.myRight = 0;
    frameLayout.myHeight = MyLayoutSize.wrap;
    frameLayout.myBottom = 20;
    [baseLayout addSubview:frameLayout];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.gravity = MyGravity_Horz_Stretch;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [frameLayout addSubview:layout];
    NSArray *arr = @[@"已付款", @"已成团", @"已开奖"];
    for (NSInteger i = 0; i < arr.count; i++) {
        NSString *title = arr[i];
        MGStatusBtn *btn = [self.class factoryBtn:title];
        [layout addSubview:btn];
        if (i == 0) {
            self.payBtn = btn;
            btn.leftLine.visibility = MyVisibility_Invisible;
        } else if (i == 1) {
            self.hasMGBtn = btn;
        } else {
            self.endBtn = btn;
            btn.rightLine.visibility = MyVisibility_Invisible;
        }
    }
    
}

+ (MGStatusBtn *)factoryBtn:(NSString *)title {
    MGStatusBtn *btn = [[MGStatusBtn alloc] initWithTitle:title];
    btn.height = 40;
    btn.myHeight = btn.height;
    return btn;
}

@end
