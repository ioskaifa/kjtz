//
//  UserSendLogListVC.m
//  BOB
//
//  Created by mac on 2020/10/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserSendLogListVC.h"
#import "WalletRecordListCell.h"
#import "UserSendLogListObj.h"

@interface UserSendLogListVC ()

@end

@implementation UserSendLogListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNavBarTitle:self.title];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Override Super Class Method
- (HTTPRequestMethod)httpMethod {
    return HTTPRequestMethodPost;
}

- (NSDictionary *)requestArgument {
    return @{@"send_type":@"01"};
}

- (NSString *)requestUrl {
    return @"api/wallet/send/getUserSendLogList";
}

///请求时传入last id的key
- (NSString *)httpRequstLastIdKey {
    return @"last_id";
}

///上拉刷新时从数据源数组中最后一个元素取值的key
- (NSString *)getValueFromItemLastIdKey {
    return @"ID";
}

///后台返回数据中数组的keypath
- (NSString *)dataListKeyPath {
    return @"data.userSendLogList";
}

///数据源中的model,需要继承BaseObject
- (Class)model {
    return [UserSendLogListObj class];
}

///是否需要上拉刷新
- (BOOL)needPullUpRefresh {
    return YES;
}

///是否需要下拉刷新
- (BOOL)needPullDownRefresh {
    return YES;
}

#pragma mark - setup
///配置tableVeiw
- (void)configTableView:(UITableView *)tableView {
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellClass:[WalletRecordListCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[WalletRecordListCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
    
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 170;
}

@end
