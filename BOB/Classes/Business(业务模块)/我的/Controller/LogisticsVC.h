//
//  LogisticsVC.h
//  BOB
//
//  Created by Colin on 2020/10/30.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "MyOrderListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface LogisticsVC : BaseViewController

@property (nonatomic, strong) MyOrderListObj *obj;

@end

NS_ASSUME_NONNULL_END
