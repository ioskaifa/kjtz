//
//  MyCollectionVC.m
//  BOB
//
//  Created by colin on 2021/1/16.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "MyCollectionVC.h"
#import "YJSliderView.h"
#import "GoodsCollectionAPI.h"
#import "MyCollectionCell.h"

@interface MyCollectionVC ()<YJSliderViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) YJSliderView *sliderView;

@property (nonatomic, strong) NSArray *sliderViewArr;

@property (nonatomic, assign) NSInteger currentIndex;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation MyCollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)fetchData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    
    [GoodsCollectionAPI getUserGoodsCollectInfoList:[self mall_type] last_id:self.last_id completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *tempArr = dataDic[@"data"][@"shopCollectList"];
            NSArray *dataArr = [MyCollectionListObj modelListParseWithArray:tempArr];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            MyCollectionListObj *obj = self.dataSourceMArr.lastObject;
            if ([obj isKindOfClass:MyCollectionListObj.class]) {
                self.last_id = obj.ID;
            }
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - 删除收藏夹
- (void)delUserGoodsCollectGoods:(MyCollectionListObj *)obj indexPath:(NSIndexPath *)indexPath {
    [GoodsCollectionAPI delUserGoodsCollectGoods:@[obj.ID] completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            [self.dataSourceMArr removeObject:obj];
            MyCollectionListObj *last = [self.dataSourceMArr lastObject];
            if ([last isKindOfClass:MyCollectionListObj.class]) {
                self.last_id = last.ID;
            } else {
                self.last_id = @"";
            }
            [self.dataSourceMArr removeObject:obj];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath]
                                  withRowAnimation:UITableViewRowAnimationNone];
            if ([@"" isEqualToString:self.last_id]) {
                [self fetchData];
            }
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (NSString *)mall_type {
    if (0 == self.currentIndex) {
        return @"";
    } else if (1 == self.currentIndex) {
        return @"01";
    } else {
        return @"02";
    }
}

#pragma mark YJSliderViewDelegate
- (NSInteger)numberOfItemsInYJSliderView:(YJSliderView *)sliderView {
    return self.sliderViewArr.count;
}

- (UIView *)yj_SliderView:(YJSliderView *)sliderView viewForItemAtIndex:(NSInteger)index {
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (NSString *)yj_SliderView:(YJSliderView *)sliderView titleForItemAtIndex:(NSInteger)index {
    if (index >= self.sliderViewArr.count) {
        return @"";
    }
    return self.sliderViewArr[index];
}

- (NSInteger)initialzeIndexFoYJSliderView:(YJSliderView *)sliderView {
    return 0;
}

- (void)yj_SliderView:(YJSliderView *)sliderView didSelectItemAtIndex:(NSIndexPath *)indexPath {
    if (self.currentIndex == indexPath.row) {
        return;
    }
    self.currentIndex = indexPath.row;
    self.last_id = @"";
    [self fetchData];
}

#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MyCollectionCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:MyCollectionCell.class]) {
        return;
    }
    MyCollectionListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MyCollectionListObj.class]) {
        return;
    }
    MyCollectionCell *listCell = (MyCollectionCell *)cell;
    [listCell configureView:obj];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    MyCollectionListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MyCollectionListObj.class]) {
        return;
    }
    
    if ([@"02" isEqualToString:obj.mall_type]) {
        MXRoute(@"GoodInfoVC", (@{@"goods_id":obj.goods_id, @"isFree":@(1)}))
    } else {
        MXRoute(@"GoodInfoVC", (@{@"goods_id":obj.goods_id}))
    }
    
}

#pragma mark - UITableView 左滑删除
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (indexPath.row >= self.dataSourceMArr.count) {
            return;
        }
        MyCollectionListObj *obj = self.dataSourceMArr[indexPath.row];
        if (![obj isKindOfClass:MyCollectionListObj.class]) {
            return;
        }
        [MXAlertViewHelper showAlertViewWithMessage:@"是否要删除？" completion:^(BOOL cancelled, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self delUserGoodsCollectGoods:obj indexPath:indexPath];
            }
        }];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

- (void)createUI {
    [self setNavBarTitle:@"收藏夹"];
    self.currentIndex = 0;
    [self.view addSubview:self.sliderView];
    [self.sliderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
    }];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sliderView.mas_bottom);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

- (YJSliderView *)sliderView {
    if (!_sliderView) {
        _sliderView = [[YJSliderView alloc] initWithFrame:CGRectZero];
        _sliderView.delegate = self;
        _sliderView.themeColor = [UIColor colorWithHexString:@"#FF4757"];
        _sliderView.lineColor = [UIColor colorWithHexString:@"#FF4757"];
    }
    return _sliderView;
}

- (NSArray *)sliderViewArr {
    if (!_sliderViewArr) {
        _sliderViewArr = @[@"全部商品", @"数字商城", @"免单商城"];
    }
    return _sliderViewArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [MyCollectionCell viewHeight];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        Class cls = MyCollectionCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}


@end
