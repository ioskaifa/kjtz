//
//  ShippingAddressVC.m
//  BOB
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ShippingAddressVC.h"
#import "UserAddressListObj.h"
#import "UserAddressListCell.h"
#import "MyAddressNullView.h"

@interface ShippingAddressVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) MyAddressNullView *nullView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, strong) UIView *bottomView;

@property (nonatomic, strong) UIButton *submitBtn;

@end

@implementation ShippingAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.last_id = @"";
    [self fetchData];
}

- (void)fetchData {
    [self request:@"api/user/address/getUserAddressList"
            param:@{@"last_id":self.last_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            if ([@"" isEqualToString:self.last_id]) {
                [self.dataSourceMArr removeAllObjects];
            }
            NSArray *dataArr = [UserAddressListObj modelListParseWithArray:object[@"data"][@"userAddressList"]];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            UserAddressListObj *obj = [dataArr lastObject];
            if ([obj isKindOfClass:UserAddressListObj.class]) {
                self.last_id = obj.address_id;
            }
            [self.tableView reloadData];
            if (self.dataSourceMArr.count == 0) {
                self.nullView.hidden = NO;
            } else {
                self.nullView.hidden = YES;
            }
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - 删除地址
- (void)delAddress:(UserAddressListObj *)obj indexPath:(NSIndexPath *)indexPath {
    [self request:@"api/user/address/updateUserAddress"
            param:@{@"address_id":obj.address_id?:@"",
                    @"user_address_oper":@"user_address_del"}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            [self.dataSourceMArr removeObject:obj];
            UserAddressListObj *last = [self.dataSourceMArr lastObject];
            if ([last isKindOfClass:UserAddressListObj.class]) {
                self.last_id = last.address_id;
            } else {
                self.last_id = @"";
            }
            [self.dataSourceMArr removeObject:obj];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath]
                                  withRowAnimation:UITableViewRowAnimationNone];
            if ([@"" isEqualToString:self.last_id]) {
                [self fetchData];
            }
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = UserAddressListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls)
                                                            forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    UserAddressListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:UserAddressListObj.class]) {
        return;
    }
    if (![cell isKindOfClass:UserAddressListCell.class]) {
        return;
    }
    UserAddressListCell *listCell = (UserAddressListCell *)cell;
    [listCell configureView:obj];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    UserAddressListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:UserAddressListObj.class]) {
        return;
    }
    if (self.block) {
        self.block(obj);
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        MXRoute(@"AddressManageVC", (@{@"type":@(1), @"obj":obj}))
    }
}

#pragma mark - UITableView 左滑删除
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (indexPath.row >= self.dataSourceMArr.count) {
            return;
        }
        UserAddressListObj *obj = self.dataSourceMArr[indexPath.row];
        if (![obj isKindOfClass:UserAddressListObj.class]) {
            return;
        }
        [MXAlertViewHelper showAlertViewWithMessage:@"是否要删除该地址？" completion:^(BOOL cancelled, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self delAddress:obj indexPath:indexPath];
            }
        }];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

- (void)createUI {
    [self setNavBarTitle:@"收货地址"];
    self.last_id = @"";
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.tableView addSubview:self.nullView];
    [layout addSubview:self.tableView];
    [layout addSubview:self.bottomView];
    [self.view addSubview:layout];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [UserAddressListCell viewHeight];
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        Class cls = UserAddressListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = ({
            UIView *view = [UIView new];
            view.backgroundColor = [UIColor whiteColor];
            [view addTopSingleLine:[UIColor moBackground]];
            view.height = safeAreaInsetBottom() + 56;
            view.myHeight = view.height;
            view.myLeft = 0;
            view.myBottom = 0;
            view.myRight = 0;
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.myBottom = safeAreaInsetBottom();
            [object addSubview:self.submitBtn];
            [view addSubview:object];
            view;
        });
    }
    return _bottomView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"添加新地址" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myCenterY = 0;
            object.myLeft = 15;
            object.myRight = 15;
            object.weight = 1;
            [object addAction:^(UIButton *btn) {
                MXRoute(@"AddressManageVC", @{@"type":@(0)});
            }];
            object;
        });
    }
    return _submitBtn;
}

- (MyAddressNullView *)nullView {
    if (!_nullView) {
        _nullView = [MyAddressNullView new];
        _nullView.x = 1;
        _nullView.size = CGSizeMake(SCREEN_WIDTH, [MyAddressNullView viewHeight]);
        _nullView.hidden = YES;
    }
    return _nullView;
}

@end
