//
//  AddressManageVC.h
//  BOB
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "UserAddressListObj.h"

typedef NS_ENUM(NSInteger, AddressManageVCType) {
    ///新增
    AddressManageVCTypeAdd     = 0,
    ///编辑
    AddressManageVCTypeEdit    = 1,
    ///设置默认地址
    AddressManageVCTypeSet     = 2,
};

NS_ASSUME_NONNULL_BEGIN

@interface AddressManageVC : BaseViewController

@property (nonatomic, assign)AddressManageVCType type;

@property (nonatomic, strong)UserAddressListObj *obj;

@end

NS_ASSUME_NONNULL_END
