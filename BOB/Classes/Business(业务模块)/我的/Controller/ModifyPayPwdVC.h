//
//  ModifyPayPwdVC.h
//  BOB
//
//  Created by mac on 2020/9/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ModifyPayPwdVC : BaseViewController

@property (nonatomic, copy) FinishedBlock complete;

@end

NS_ASSUME_NONNULL_END
