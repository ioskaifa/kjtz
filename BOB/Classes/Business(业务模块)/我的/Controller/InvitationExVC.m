//
//  InvitationExVC.m
//  BOB
//
//  Created by colin on 2020/12/30.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "InvitationExVC.h"

@interface InvitationExVC ()

@property (nonatomic, strong) UIButton *backBtn;

@property (nonatomic, strong) UIImageView *qrCode;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIImageView *tipImgView;

@end

@implementation InvitationExVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)fetchData {
    [self request:@"api/user/info/getRegisterRewardNum"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSString *give_sla_num = [object[@"data"][@"give_sla_num"] description];
            if ([StringUtil isEmpty:give_sla_num]) {
                return;
            }
            self.tipImgView.visibility = MyVisibility_Visible;
        }
    }];
}

#pragma mark - 保存View到本地相册
- (void)saveImageToPhotosAlbum:(UIView *)view {
    // 设置绘制图片的大小
    UIGraphicsBeginImageContextWithOptions(view.size, NO, 0.0);
    // 绘制图片
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // 保存图片到相册   如果需要获取保存成功的事件第二和第三个参数需要设置响应对象和方法，该方法为固定格式。
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

#pragma mark - Deleaget
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if (!error) {
        [NotifyHelper showMessageWithMakeText:@"保存成功!"];
    }
}

- (void)createUI {
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.backBtn];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    self.tableView.tableHeaderView = [self headerView];
}

- (UIView *)headerView {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myHeight = MyLayoutSize.wrap;
    baseLayout.myWidth = SCREEN_WIDTH;
    
    UIImage *img = [UIImage imageNamed:@"推广中心top"];
    CGSize size = CGSizeMake(SCREEN_WIDTH, SCREEN_Height*0.6);
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.size = size;
    layout.mySize = layout.size;
    layout.backgroundImage = img;
    [baseLayout addSubview:layout];
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"logo_sla"];
    imgView.myTop = size.height/2.0 - 90;
    imgView.myCenterX = 0;
    [layout addSubview:imgView];
    
    imgView = [UIImageView autoLayoutImgView:@"从一到万"];
    imgView.myTop = 20;
    imgView.myCenterX = 0;
    [layout addSubview:imgView];
    
    UIImageView *shareTop = [UIImageView autoLayoutImgView:@"shareTop"];
    shareTop.myCenterX = 0;
    shareTop.myTop = 10;
    
    imgView = [UIImageView autoLayoutImgView:@"1SLA"];
    imgView.myCenterX = 0;
    imgView.myTop = -(imgView.height + shareTop.height + 30);
    imgView.visibility = MyVisibility_Invisible;
    self.tipImgView = imgView;
    [baseLayout addSubview:imgView];
    
    [baseLayout addSubview:shareTop];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor moBackground];
    layout.myCenterX = 0;
    layout.height = 25;
    layout.myHeight = layout.height;
    layout.myWidth = MyLayoutSize.wrap;
    [layout setViewCornerRadius:layout.height/2.0];
    layout.myTop = 30;
    [baseLayout addSubview:layout];
    
    imgView = [UIImageView new];
    imgView.size = CGSizeMake(20, 20);
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myLeft = 10;
    [imgView setViewCornerRadius:imgView.height/2.0];
    [imgView sd_setImageWithURL:[NSURL URLWithString:StrF(@"%@/%@", UDetail.user.qiniu_domain, UDetail.user.head_photo)]];
    [layout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor blackColor];
    lbl.text = StrF(@"%@ %@", UDetail.user.nickname?:@"", @"邀您加入数字商城");
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    lbl.myLeft = 10;
    lbl.myRight = 15;
    [layout addSubview:lbl];
    
    [baseLayout addSubview:self.qrCode];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"微信扫码打开";
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myTop = 10;
    [baseLayout addSubview:lbl];
    
    UILabel *tipLbl = [UILabel new];
    tipLbl.font = [UIFont boldFont15];
    tipLbl.textColor = [UIColor moGreen];
    tipLbl.text = @"下载APP  开启数字之旅";
    [tipLbl autoMyLayoutSize];
    tipLbl.myCenterX = 0;
    tipLbl.myTop = 20;
    tipLbl.visibility = MyVisibility_Gone;
    [baseLayout addSubview:tipLbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor whiteColor];
    lbl.text = @"保存海报";
    lbl.backgroundColor = [UIColor moGreen];
    [lbl setViewCornerRadius:5];
    lbl.size = CGSizeMake(100, 30);
    lbl.myCenterX = 0;
    lbl.myTop = 10;
    lbl.myBottom = 20;
    [baseLayout addSubview:lbl];
    @weakify(self)
    [lbl addAction:^(UIView *view) {
        @strongify(self)
        tipLbl.visibility = MyVisibility_Visible;
        view.visibility = MyVisibility_Gone;
        [self saveImageToPhotosAlbum:self.tableView];
    }];
    
    [baseLayout layoutIfNeeded];
    return baseLayout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.tableFooterView = [UIView new];
        AdjustTableBehavior(_tableView);
    }
    return _tableView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = ({
            UIButton *object = [UIButton new];
            object.x = 17;
            object.y = safeAreaInset().top + 20;
            object.size = CGSizeMake(40, 40);
            [object setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                [self.navigationController popViewControllerAnimated:YES];
            }];
            object;
        });
    }
    return _backBtn;
}

- (UIImageView *)qrCode {
    if (!_qrCode) {
        _qrCode = [UIImageView new];
        CGSize size = CGSizeMake(115, 115);
        UIImage *qrImg = [UIImage encodeQRImageWithContent:UDetail.user.qr_code_url?:@"" size:size];
        _qrCode.image = qrImg;
        _qrCode.size = size;
        _qrCode.mySize = _qrCode.size;
        _qrCode.myCenterX = 0;
        _qrCode.myTop = 15;
    }
    return _qrCode;
}

@end
