//
//  ModifyNickNameVC.m
//  BOB
//
//  Created by mac on 2020/9/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ModifyNickNameVC.h"
#import "NSObject+Mine.h"

@interface ModifyNickNameVC ()

@property (nonatomic, strong) UITextField *tf;

@end

@implementation ModifyNickNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (BOOL)valiParam {
    NSString *nickName = [StringUtil trim:self.tf.text];
    if ([StringUtil isEmpty:nickName]) {
        [NotifyHelper showMessageWithMakeText:@"昵称不能为空"];
        return NO;
    }
    if (nickName.length > 15) {
        [NotifyHelper showMessageWithMakeText:@"昵称长度不能超过15位"];
        return NO;
    }
    return YES;
}

- (void)commit {
    NSString *nickName = [StringUtil trim:self.tf.text];
    [self modifyUserInfo:@{@"nick_name":nickName?:@""} showMsg:YES
              completion:^(BOOL success, NSString *error) {
        if (success) {
            UDetail.user.nickname = nickName;
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)createUI {
    [self setNavBarTitle:@"昵称"];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    
    MyLinearLayout *topLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    topLayout.backgroundColor = [UIColor whiteColor];
    topLayout.myTop = 0;
    topLayout.myLeft = 0;
    topLayout.myRight = 0;
    topLayout.myHeight = 50;
    
    [layout addSubview:topLayout];
    UILabel *lbl = [UILabel new];
    lbl.text = @"昵称";
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 15;
    [topLayout addSubview:lbl];
    [topLayout addSubview:self.tf];
    
    UIButton *btn = [UIButton new];
    btn.height = 44;
    [btn setViewCornerRadius:5];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor moGreen]];
    btn.titleLabel.font = [UIFont boldFont15];
    [btn setTitle:@"保存修改" forState:UIControlStateNormal];
    btn.myHeight = btn.height;
    btn.myLeft = 15;
    btn.myRight = 15;
    btn.myTop = 30;
    @weakify(self)
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self.tf resignFirstResponder];
        if ([self valiParam]) {
            [self commit];
        }
    }];
    [layout addSubview:btn];
}

#pragma mark - Init
- (UITextField *)tf {
    if (!_tf) {
        _tf = [UITextField new];
        _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
        _tf.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _tf.myLeft = 50;
        _tf.myRight = 15;
        _tf.weight = 1;
        _tf.height = 20;
        _tf.myHeight = _tf.height;
        _tf.myCenterY = 0;
        _tf.text = UDetail.user.nickname;
        _tf.placeholder = @"请输入昵称";
    }
    return _tf;
}

@end
