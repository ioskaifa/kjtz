//
//  FeedBackVC.m
//  BOB
//
//  Created by mac on 2020/9/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FeedBackVC.h"
#import "DCPlaceholderTextView.h"
#import "MQImageDragView.h"
#import "PhotoBrowser.h"
#import <Photos/Photos.h>
#import "QNManager.h"
#import "IQKeyboardManager.h"
#import "FeedBackTypeView.h"
#import "HWPanModelVC.h"

@interface FeedBackVC () <MQImageDragViewDelegate, UITextViewDelegate>

@property (nonatomic,strong) NSMutableArray<PHAsset *> *curSelectAssets;

@property (nonatomic,strong) NSMutableArray<UIImage *> *curSelectImages;
///反馈类型
@property (nonatomic, strong) MyBaseLayout *typeLayout;

@property (nonatomic, strong) DCPlaceholderTextView *contentTxtView;

@property (nonatomic, strong) MQImageDragView *dragView;

@property (nonatomic, strong) UIButton        *submitBtn;

@property (nonatomic, strong) FeedBackTypeView *typeView;

@property (nonatomic, strong) HWPanModelVC *panVC;

@property (nonatomic, copy) NSString *type;

@end

@implementation FeedBackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (BOOL)valiParam {
    if ([StringUtil isEmpty:self.type]) {
        [NotifyHelper showMessageWithMakeText:@"请选择反馈类型"];
        return NO;
    }
    NSString *content = [StringUtil trim:self.contentTxtView.text];
    if ([StringUtil isEmpty:content]) {
        [NotifyHelper showMessageWithMakeText:@"反馈内容不能为空"];
        return NO;
    }
    if (content.length > 200) {
        [NotifyHelper showMessageWithMakeText:@"反馈内容不能超过200字"];
        return NO;
    }
    return YES;
}

- (void)commit {
    NSString *content = [StringUtil trim:self.contentTxtView.text];
    if (self.curSelectImages.count == 0) {
        [self request:@"api/user/feedBack/addUserFeedBack"
                param:@{@"feedback_type":self.type,
                        @"feedback_content":content?:@"",
                        @"feedback_img":@"",
                        @"contact_way":UDetail.user.user_tel?:@""}
           completion:^(BOOL success, id object, NSString *error) {
            if(success) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    } else {
        [NotifyHelper showHUDAddedTo:self.view animated:YES];
        [[QNManager shared] uploadImages:self.curSelectImages
                              completion:^(id data) {
            [NotifyHelper hideAllHUDsForView:self.view animated:YES];
            NSArray *array = data;
            if(array && array.count > 0) {
                [self request:@"api/user/feedBack/addUserFeedBack"
                        param:@{@"feedback_type":self.type,
                        @"feedback_content":content?:@"",
                        @"feedback_img":[array componentsJoinedByString:@","],
                        @"contact_way":UDetail.user.user_tel?:@""}
                   completion:^(BOOL success, id object, NSString *error) {
                    if(success) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }];
            }
        }];
    }
}

#pragma mark - Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (textView == self.contentTxtView) {
        IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
        manager.shouldShowToolbarPlaceholder = NO;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView == self.contentTxtView) {
        IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
        manager.shouldShowToolbarPlaceholder = YES;
    }
}

- (void)imageDragViewAddButtonClicked {
    NSLog(@"imageDragView---点击了添加按钮");
    @weakify(self)
    [[PhotoBrowser shared] dynamicShowPhotoLibrary:self allowSelectVideo:NO lastSelectAssets:self.curSelectAssets completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
        @strongify(self)
        [self updateDragView:images assets:assets];
    }];
}

- (void)imageDragViewDeleteButtonClickedAtIndex:(NSInteger)index{
    NSLog(@"imageDragView---删除了%ld",index);
    
    //刷新frame
    [self.curSelectAssets safeRemoveObjectAtIndex:index];
    [self.curSelectImages safeRemoveObjectAtIndex:index];
    self.dragView.myHeight = [self.dragView getHeightThatFit];
}

- (void)imageDragViewButtonClickedAtIndex:(NSInteger)index{
    NSLog(@"imageDragView---点击了%ld",index);
    [[PhotoBrowser shared] dynamicPreviewSelectPhotos:self didSelectItemAtIndex:index completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
        [self updateDragView:images assets:assets];
    }];
}

- (void)imageDragViewDidMoveButtonFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{
    NSLog(@"imageDragView---移动：从%ld至%ld",fromIndex,toIndex);
    [self.curSelectAssets exchangeObjectAtIndex:toIndex withObjectAtIndex:fromIndex];
    [self.curSelectImages exchangeObjectAtIndex:toIndex withObjectAtIndex:fromIndex];
}

#pragma mark - Private Method
- (void)updateDragView:(NSArray *)images assets:(NSArray *)assets {
    if([images isKindOfClass:[NSArray class]]) {
        [self.dragView removeAll];
        [images enumerateObjectsUsingBlock:^(UIImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            PHAsset *asset = [assets safeObjectAtIndex:idx];
//            self.curSelectVideo = NO;
            if(asset.mediaType == PHAssetMediaTypeVideo) {
//                self.dragView.kMaxCount = 1;
//                self.curSelectVideo = YES;
            } else if([obj isKindOfClass:[UIImage class]]) {
//                self.dragView.kMaxCount = 9;
            }
            
            [self.dragView addImage:obj];
        }];
        
//        [self layoutHeader];
        self.dragView.myHeight = [self.dragView getHeightThatFit];
//        [self configureScrollViewContevtSize];
        self.curSelectAssets = [NSMutableArray arrayWithArray:assets];
        self.curSelectImages = [NSMutableArray arrayWithArray:images];
    }
}

- (void)createUI {
    [self setNavBarTitle:@"意见反馈"];
    self.view.backgroundColor = [UIColor moBackground];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 1;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    
    MyBaseLayout *typeLayout = [UIView stdCellView:@"请选择反馈类型" right:@"" arrow:@"Arrow" rowHeight:52];
    @weakify(self)
    [typeLayout addAction:^(UIView *view) {
        @strongify(self)
        [self.panVC show];
    }];
    [layout addSubview:typeLayout];
    self.typeLayout = typeLayout;
    
    [layout addSubview:[UIView gapLine]];
    
    [layout addSubview:self.contentTxtView];
//    [layout addSubview:[UIView gapLine]];
    [layout addSubview:self.dragView];
    [layout addSubview:self.submitBtn];
        
    [self.view addSubview:layout];
}

- (DCPlaceholderTextView *)contentTxtView {
    if (!_contentTxtView) {
        _contentTxtView = ({
            DCPlaceholderTextView *object = [DCPlaceholderTextView new];
            object.delegate = self;
            object.backgroundColor = [UIColor whiteColor];
            object.placeholder = @"非常感谢您的支持与关注，请在这里留下您宝贵的意见和建议！（内容不超过200字）";
            object.placeholderColor = [UIColor colorWithHexString:@"#CCCCCC"];
            object.placeholderFont = [UIFont font15];
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.myHeight = 130;
            object;
        });
    }
    return _contentTxtView;
}

- (MQImageDragView *)dragView {
    if (!_dragView) {
        _dragView = ({
            MQImageDragView *object = [[MQImageDragView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 30, 100)];
            object.backgroundColor = [UIColor whiteColor];
            object.kMarginLRTB = 5;
            object.kMarginB = 4;
            object.kMaxCount = 8;
            object.kCountInRow = 4;
            object.dragViewDelegete = self;
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.mySize = object.size;
            object;
        });
    }
    return _dragView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"提交" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myTop = 50;
            object.myLeft = 15;
            object.myRight = 15;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                if ([self valiParam]) {
                    [self commit];
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

- (FeedBackTypeView *)typeView {
    if (!_typeView) {
        _typeView = [FeedBackTypeView new];
        _typeView.size = CGSizeMake(SCREEN_WIDTH, [FeedBackTypeView viewHeight]);
        @weakify(self)
        [_typeView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.panVC dismiss];
        }];
        _typeView.block = ^(id data) {
            @strongify(self)
            self.type = data;
            MLog(@"选择反馈类型....%@", self.type);
            [self.panVC dismiss];
            UILabel *lbl = [self.typeLayout viewWithTag:100];
            if (lbl) {
                lbl.text = self.type;
                [lbl sizeToFit];
                lbl.mySize = lbl.size;
            }
        };
    }
    return _typeView;
}

- (HWPanModelVC *)panVC {
    if (!_panVC) {
        _panVC = ({
            HWPanModelVC *object = [HWPanModelVC showInVC:self
                                               customView:self.typeView];
            object;
        });
    }
    return _panVC;
}

@end
