//
//  MyTeamVC.m
//  BOB
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyTeamVC.h"
#import "UserTeamListObj.h"
#import "UserTeamListCell.h"

@interface MyTeamVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) MyBaseLayout *headerView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation MyTeamVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)fetchData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [self request:@"api/user/info/getUserTeamList"
            param:@{@"last_id":self.last_id?:@"",
                    @"team_type":@"01"
            }
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object[@"data"];
            NSArray *dataArr = [UserTeamListObj modelListParseWithArray:dataDic[@"userTeamList"]];
            UserTeamListObj *obj = [dataArr lastObject];
            if ([obj isKindOfClass:UserTeamListObj.class]) {
                self.last_id = obj.ID;
            }
            [self.dataSourceMArr addObjectsFromArray:dataArr];
        }
        [self.tableView reloadData];
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = UserTeamListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    UserTeamListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:UserTeamListObj.class]) {
        return;
    }
    if (![cell isKindOfClass:UserTeamListCell.class]) {
        return;
    }
    UserTeamListCell *listCell = (UserTeamListCell *)cell;
    [listCell configureView:obj];
    if (indexPath.row == self.dataSourceMArr.count - 1) {
        listCell.line.hidden = YES;
    } else {
        listCell.line.hidden = NO;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (self.dataSourceMArr.count == 0) {
        return 0.01;
    } else {
        return 10;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (self.dataSourceMArr.count == 0) {
        return nil;
    }
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = CGRectMake(15, 0, SCREEN_WIDTH - 30, 10);
    [bgView setBorderWithCornerRadius:5 byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight];
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 15);
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:bgView];
    return view;
}

- (void)createUI {
    [self setNavBarTitle:@"我的社群"];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = [UserTeamListCell viewHeight];
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.tableHeaderView = self.headerView;
        _tableView.tableFooterView = [UIView new];
        Class cls = UserTeamListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
    }
    return _tableView;
}

- (MyBaseLayout *)headerView {
    if (!_headerView) {
        _headerView = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _headerView.size = CGSizeMake(SCREEN_WIDTH, 200);
        _headerView.mySize = _headerView.size;
        _headerView.backgroundImage = [UIImage imageNamed:@"我的团队bg"];
        
        [_headerView addSubview:[self userInfoLayout]];
        [_headerView addSubview:[self teamInfoLayout]];
        
        MyFrameLayout *bottomLayout = [MyFrameLayout new];
        bottomLayout.backgroundColor = [UIColor whiteColor];
        bottomLayout.myLeft = 15;
        bottomLayout.size = CGSizeMake(SCREEN_WIDTH - 30, 30);
        bottomLayout.mySize = bottomLayout.size;
        [bottomLayout setBorderWithCornerRadius:5 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
        MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
        layout.myCenter = CGPointZero;
        layout.myWidth = MyLayoutSize.wrap;
        layout.myHeight = MyLayoutSize.wrap;
        UIImageView *imgView = [UIImageView autoLayoutImgView:@"我的团队推荐列表"];
        imgView.myCenterY = 0;
        imgView.myRight = 10;
        [layout addSubview:imgView];
        
        UILabel *lbl = [UILabel new];
        lbl.textColor = [UIColor moGreen];
        lbl.font = [UIFont font14];
        lbl.text = @"粉丝列表";
        [lbl autoMyLayoutSize];
        lbl.myCenterY = 0;
        lbl.myRight = 10;
        [layout addSubview:lbl];
        
        imgView = [UIImageView autoLayoutImgView:@"我的团队推荐列表"];
        imgView.myCenterY = 0;
        imgView.myRight = 10;
        [layout addSubview:imgView];
        [bottomLayout addSubview:layout];
        
        [_headerView addSubview:bottomLayout];
        
        [_headerView layoutIfNeeded];
    }
    return _headerView;
}

#pragma mark - 用户信息
- (MyBaseLayout *)userInfoLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 20;
    layout.myRight = 20;
    layout.myHeight = 85;
    
    UserModel *user = UDetail.user;
    UIImageView *imgView = [UIImageView new];
    imgView.backgroundColor = [UIColor moBackground];
    imgView.size = CGSizeMake(54, 54);
    [imgView setViewCornerRadius:imgView.height/2.0];
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    NSString *headUrl = StrF(@"%@/%@", user.qiniu_domain, user.head_photo);
    [imgView sd_setImageWithURL:[NSURL URLWithString:headUrl]];
    [layout addSubview:imgView];
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myCenterY = 0;
    rightLayout.myLeft = 10;
    rightLayout.myRight = 10;
    rightLayout.weight = 1;
    [layout addSubview:rightLayout];
    
    MyLinearLayout *nameLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    nameLayout.myLeft = 0;
    nameLayout.myRight = 0;
    nameLayout.myHeight = MyLayoutSize.wrap;
    nameLayout.myBottom = 10;
    [rightLayout addSubview:nameLayout];
    
    UILabel *object = [UILabel new];
    object.font = [UIFont boldFont18];
    object.textColor = [UIColor whiteColor];
    object.myCenterY = 0;
    object.text = user.nickname;
    [object autoMyLayoutSize];
    [nameLayout addSubview:object];
    
    SPButton *btn = [SPButton new];
    btn.userInteractionEnabled = NO;
    btn.imagePosition = SPButtonImagePositionLeft;
    btn.imageTitleSpace = 5;
    btn.height = 20;
    btn.myCenterY = 0;
    btn.myLeft = 10;
    btn.myHeight = btn.height;
    [btn setViewCornerRadius:2];
    btn.backgroundColor = [UIColor whiteColor];
    btn.titleLabel.font = [UIFont font12];
    [btn setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
    [btn setTitle:StrF(@"V%@", user.user_grade) forState:UIControlStateNormal];
    [btn sizeToFit];
    CGSize size = btn.size;
    btn.mySize = CGSizeMake(size.width, 20);
    btn.visibility = MyVisibility_Invisible;
    [nameLayout addSubview:btn];
    
    
    object = [UILabel new];
    object.font = [UIFont font14];
    object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    object.myCenterY = 0;
    object.text = [StringUtil telNumberFormat344:user.user_tel];
    [object autoMyLayoutSize];
    [rightLayout addSubview:object];
    
    imgView = [UIImageView autoLayoutImgView:@"我的团队二维码"];
    imgView.myCenterY = 0;
    imgView.myRight = 15;
    [imgView addAction:^(UIView *view) {
        MXRoute(@"InvitationExVC", nil)
    }];
    [layout addSubview:imgView];
    
    [layout layoutIfNeeded];
    return layout;
}

- (MyBaseLayout *)teamInfoLayout{
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.weight = 1;
    layout.gravity = MyGravity_Horz_Stretch;
    
    UserModel *user = UDetail.user;
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.myHeight = MyLayoutSize.fill;
    NSArray *txtArr = @[user.under_num, @"\n社群人数"];
    NSArray *fontArr = @[[UIFont boldFont14], [UIFont font12]];
    NSArray *colorArr = @[[UIColor whiteColor], [UIColor colorWithHexString:@"#AAAABB"]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                        colors:colorArr
                                                                         fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 6; // 调整行间距
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
    [layout addSubview:lbl];
    
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithHexString:@"#AAAABB"];
    line.myWidth = 0.5;
    line.myTop = 15;
    line.myBottom = 15;
    [layout addSubview:line];
    
    lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.myHeight = MyLayoutSize.fill;
    txtArr = @[user.referer_num, @"\n粉丝人数"];
    fontArr = @[[UIFont boldFont14], [UIFont font12]];
    colorArr = @[[UIColor whiteColor], [UIColor colorWithHexString:@"#AAAABB"]];
    att = [NSMutableAttributedString initWithTitles:txtArr
                                             colors:colorArr
                                              fonts:fontArr];
    paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 6; // 调整行间距
    paragraphStyle.alignment = NSTextAlignmentCenter;
    range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
    [layout addSubview:lbl];
    
    [layout layoutIfNeeded];
    return layout;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

@end
