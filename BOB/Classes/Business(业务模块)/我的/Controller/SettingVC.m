//
//  SettingVC.m
//  BOB
//
//  Created by mac on 2020/9/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SettingVC.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "NSObject+LoginHelper.h"
#import "LcwlChat.h"
#import <Photos/Photos.h>
#import "QNManager.h"
#import "PhotoBrowser.h"
#import "NSObject+Mine.h"

@interface SettingVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MyLinearLayout *layout;

@property (nonatomic, strong) UIButton *exitBtn;

@property (nonatomic, strong) MyBaseLayout *nickNameLayout;

@property (nonatomic, strong) MyBaseLayout *sexLayout;

@property (nonatomic, strong) MyBaseLayout *photoLayout;

@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureNicknameLbl];
    [self configureSexLbl];
}

- (void)configureNicknameLbl {
    NSString *nickName = UDetail.user.nickname;
    UILabel *lbl = [self.nickNameLayout viewWithTag:100];
    if (lbl) {
        lbl.text = nickName?:@"";
        [lbl sizeToFit];
        lbl.mySize = lbl.size;
    }
}

- (void)configureSexLbl {
    NSString *sex = UDetail.user.formatSex;
    UILabel *lbl = [self.sexLayout viewWithTag:100];
    if (lbl) {
        lbl.text = sex?:@"";
        [lbl sizeToFit];
        lbl.mySize = lbl.size;
    }
}

- (void)configurePhoto:(UIImage *)img {
    UIImageView *imgView = [self.photoLayout viewWithTag:100];
    if (imgView) {
        imgView.image = img;
    }
}

#pragma mark - IBAction
- (void)logout:(id)sender {
    [UIActionSheet actionSheetWithTitle:nil message:nil buttons:@[@"退出登录"] showInView:self.view onDismiss:^(NSInteger buttonIndex) {
        [[LcwlChat shareInstance].chatManager loginOut];
        if ([MoApp respondsToSelector:NSSelectorFromString(@"loginOutLogic")]) {
            [MoApp performSelector:NSSelectorFromString(@"loginOutLogic") withObject:nil];
        }
    } onCancel:nil];
}

#pragma mark 修改头像
- (void)modifyHeadPhoto {
    [[PhotoBrowser shared] showSelectSinglePhotoLibrary:self completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
            if([images isKindOfClass:[NSArray class]]) {
                if([[images firstObject] isKindOfClass:[UIImage class]]) {
                    UIImage *image = [images firstObject];
                    [NotifyHelper showHUDAddedTo:self.view animated:YES];
                    [[QNManager shared] uploadImage:image completion:^(id data) {
                        [NotifyHelper hideHUDForView:self.view animated:YES];
                        if(data && [NSURL URLWithString:data]) {
                            [self configurePhoto:image];
                            [self modifyUserInfo:@{@"head_photo":data} showMsg:YES completion:^(BOOL success, NSString *error) {
                                if (success) {
                                    UDetail.user.head_photo = data;
                                }
                            }];
                        }
                    }];
                }
            }
    }];
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"设置中心"];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self initUI];
}

- (void)initUI {
    UserModel *user = UDetail.user;
    [self.layout addSubview:self.photoLayout];
    [self.layout addSubview:[UIView line]];
    [self.layout addSubview:self.nickNameLayout];
    [self.layout addSubview:[UIView line]];
    self.sexLayout = [UIView stdCellView:@"性别" right:user.formatSex?:@"" arrow:@"Arrow" rowHeight:52];
    [self.sexLayout addAction:^(UIView *view) {
        MXRoute(@"SelectSexVC", nil);
    }];
    [self.layout addSubview:self.sexLayout];
    [self.layout addSubview:[UIView line]];
    UIView *view = [UIView stdCellView:@"手机号" right:user.user_tel?:@"" arrow:@"Arrow" rowHeight:52];
    [view addAction:^(UIView *view) {
        MXRoute(@"ModifyPhoneVC", nil);
    }];
    [self.layout addSubview:view];
    [self.layout addSubview:[UIView line]];
    [self.layout addSubview:[UIView stdCellView:@"UID" right:user.user_uid?:@"" arrow:@"" rowHeight:52]];
    [self.layout addSubview:[UIView line]];
    view = [UIView stdCellView:@"新消息通知" right:@"" arrow:@"Arrow" rowHeight:52];
    [self.layout addSubview:view];
    [view addAction:^(UIView *view) {
        MXRoute(@"MessageSettingVC", nil);
    }];
    [self.layout addSubview:[UIView gapLine]];
    
    view = [UIView stdCellView:@"修改登录密码" right:@"" arrow:@"Arrow" rowHeight:52];
    [view addAction:^(UIView *view) {
        MXRoute(@"ResetPwdVC", nil);
    }];
    [self.layout addSubview:view];
    [self.layout addSubview:[UIView line]];
    view = [UIView stdCellView:@"修改支付密码" right:@"" arrow:@"Arrow" rowHeight:52];
    [view addAction:^(UIView *view) {
        MXRoute(@"ModifyPayPwdVC", nil);
    }];
    [self.layout addSubview:view];
    [self.layout addSubview:self.exitBtn];
    [self.layout addSubview:[UIView placeholderVertView]];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.text = StrF(@"版本号：V%@", [DeviceManager getAppBundleVersion]);
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myBottom = 20;
    [self.layout addSubview:lbl];
    [self.layout layoutIfNeeded];
    self.tableView.tableHeaderView = self.layout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor moBackground];
    }
    return _tableView;
}

- (MyLinearLayout *)layout {
    if (!_layout) {
        _layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _layout.backgroundColor = [UIColor moBackground];
        _layout.myTop = 0;
        _layout.myLeft = 0;
        _layout.myRight = 0;
        _layout.myHeight = SCREEN_HEIGHT - NavigationBar_Bottom;
    }
    return _layout;
}

- (UIButton *)exitBtn {
    if(!_exitBtn) {
        _exitBtn = [UIButton new];
        _exitBtn.size = CGSizeMake(180, 44);
        [_exitBtn setViewCornerRadius:5];
        _exitBtn.layer.borderColor = [UIColor moGreen].CGColor;
        _exitBtn.layer.borderWidth = 1;
        _exitBtn.backgroundColor = [UIColor moBackground];
        _exitBtn.titleLabel.font = [UIFont boldFont15];
        [_exitBtn setTitleColor:[UIColor moGreen] forState:UIControlStateNormal];
        [_exitBtn setTitle:@"退出登录" forState:UIControlStateNormal];
        [_exitBtn addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
        _exitBtn.mySize = _exitBtn.size;
        _exitBtn.myCenterX = 0;
        _exitBtn.myTop = 50;
    }
    return _exitBtn;
}

- (MyBaseLayout *)nickNameLayout {
    if (!_nickNameLayout) {
        _nickNameLayout = [UIView stdCellView:@"昵称" right:UDetail.user.nickname?:@"" arrow:@"Arrow" rowHeight:52];
        [_nickNameLayout addAction:^(UIView *view) {
            MXRoute(@"ModifyNickNameVC", nil);
        }];
    }
    return _nickNameLayout;
}

+ (MyBaseLayout *)photoLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.height = 72;
    layout.myHeight = layout.height;
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor colorWithHexString:@"#1D1D1F"];
    lbl.myCenterY = 0;
    lbl.myLeft = 15;
    lbl.text = @"头像";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView placeholderHorzView]];
    
    UIImageView *imgView = [UIImageView new];
    imgView.tag = 100;
    imgView.backgroundColor = [UIColor moBackground];
    NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, UDetail.user.head_photo);
    [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    imgView.size = CGSizeMake(42, 42);
    [imgView setCircleView];
    imgView.myRight = 15;
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    [layout addSubview:imgView];
    
    imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"Arrow"];
    imgView.myRight = 15;
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    [layout addSubview:imgView];
    return layout;
}

- (MyBaseLayout *)photoLayout {
    if (!_photoLayout) {
        _photoLayout = [self.class photoLayout];
        @weakify(self)
        [_photoLayout addAction:^(UIView *view) {
            @strongify(self)
            [self modifyHeadPhoto];
        }];
    }
    return _photoLayout;
}

@end
