//
//  MyAssetsVC.m
//  BOB
//
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyAssetsVC.h"
#import "MyAssetsSubView.h"

@interface MyAssetsVC ()

@property (nonatomic, strong) MyFrameLayout *navTitleView;

@property (nonatomic, strong) MyAssetsSubView *balanceView;

@property (nonatomic, strong) MyAssetsSubView *integralView;

@end

@implementation MyAssetsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self.balanceView configureView];
    [self.integralView configureView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)createUI {
    UIImageView *bgImgView = [self bgImgView];
    [self.view addSubview:bgImgView];
    [bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self.view addSubview:self.navTitleView];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 30 + self.navTitleView.height;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self.view addSubview:layout];
    
    [layout addSubview:self.balanceView];
    [layout addSubview:self.integralView];
}

- (UIImageView *)bgImgView {
    UIImageView *imgView = [UIImageView new];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"我的资产背景" ofType:@"png"];
    UIImage *img = [UIImage imageWithContentsOfFile:path];
    imgView.image = img;
    return imgView;
}

- (MyFrameLayout *)navTitleView {
    if (!_navTitleView) {
        _navTitleView = ({
            MyFrameLayout *view = [MyFrameLayout new];
            view.size = CGSizeMake(SCREEN_WIDTH, NavigationBar_Bottom);
            view.backgroundColor = [UIColor clearColor];
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
            [leftBtn setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateHighlighted];
            leftBtn.imageView.tintColor = [UIColor whiteColor];
            [leftBtn addAction:^(UIButton *btn) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            leftBtn.size = CGSizeMake(30, 30);
            leftBtn.mySize = leftBtn.size;
            leftBtn.myLeft = 10;
            leftBtn.myBottom = 5;
            [view addSubview:leftBtn];
            
            UILabel *lbl = [UILabel new];
            lbl.font = [UIFont font18];
            lbl.textColor = [UIColor whiteColor];
            lbl.text = @"我的资产";
            [lbl autoMyLayoutSize];
            lbl.myCenterX = 0;
            lbl.myBottom = 5;
            [view addSubview:lbl];
            
            view;
        });
    }
    return _navTitleView;
}

- (MyAssetsSubView *)balanceView {
    if (!_balanceView) {
        _balanceView = ({
            MyAssetsSubView *object = [self.class factoryMyAssetsSubView:MyAssetsSubViewTypeBalance];
            [object.inputBtn addAction:^(UIView *view) {
                MXRoute(@"RechargeVC", nil)
            }];
            [object.listBtn addAction:^(UIButton *btn) {
                MXRoute(@"WalletRecordListVC", @{@"title":@"余额记录"})
            }];
            object;
        });
    }
    return _balanceView;
}

- (MyAssetsSubView *)integralView {
    if (!_integralView) {
        _integralView = ({
            MyAssetsSubView *object = [self.class factoryMyAssetsSubView:MyAssetsSubViewTypeIntegral];
            [object.inputBtn addAction:^(UIView *view) {
                MXRoute(@"TransformationVC", nil)
            }];
            [object.listBtn addAction:^(UIButton *btn) {
                MXRoute(@"WalletRecordListVC", @{@"title":@"积分记录"})
            }];
            object;
        });
    }
    return _integralView;
}

+ (MyAssetsSubView *)factoryMyAssetsSubView:(MyAssetsSubViewType)type {
    MyAssetsSubView *object = [[MyAssetsSubView alloc] initWithViewType:type];
    object.myTop = 20;
    object.myLeft = 10;
    object.myRight = 10;
    object.myHeight = 250;
    return object;
}

@end
