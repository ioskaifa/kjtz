//
//  RechargeListVC.m
//  BOB
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "RechargeListVC.h"
#import "YJSliderView.h"
#import "UserRechargeListVC.h"
#import "SLARechargeVC.h"

@interface RechargeListVC ()<YJSliderViewDelegate>

@property (nonatomic, strong) YJSliderView *sliderView;

@property (nonatomic, strong) UserRechargeListVC *rechargeListVC;

@property (nonatomic, strong) SLARechargeVC *slaRechargeListVC;

@property (nonatomic, strong) NSArray *sliderDataSourceArr;

@property (nonatomic, strong) NSArray *sliderViewArr;

@end

@implementation RechargeListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarTitle:@"充值记录"];
    [self createUI];
}

#pragma mark - Delegate
#pragma mark YJSliderViewDelegate
- (NSInteger)numberOfItemsInYJSliderView:(YJSliderView *)sliderView {
    return self.sliderDataSourceArr.count;
}

- (UIView *)yj_SliderView:(YJSliderView *)sliderView viewForItemAtIndex:(NSInteger)index {
    if (index >= self.sliderViewArr.count) {
        return nil;
    }
    return self.sliderViewArr[index];
}

- (NSString *)yj_SliderView:(YJSliderView *)sliderView titleForItemAtIndex:(NSInteger)index {
    if (index >= self.sliderDataSourceArr.count) {
        return @"";
    }
    return self.sliderDataSourceArr[index];
}

- (NSInteger)initialzeIndexFoYJSliderView:(YJSliderView *)sliderView {
    return 0;
}

#pragma mark - InitUI
- (void)createUI {
    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.view addSubview:self.sliderView];
    [self layoutUI];
}

- (void)layoutUI {
    [self.sliderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (UserRechargeListVC *)rechargeListVC {
    if (!_rechargeListVC) {
        _rechargeListVC = [UserRechargeListVC new];
    }
    return _rechargeListVC;
}

- (SLARechargeVC *)slaRechargeListVC {
    if (!_slaRechargeListVC) {
        _slaRechargeListVC = [SLARechargeVC new];
    }
    return _slaRechargeListVC;
}

- (YJSliderView *)sliderView {
    if (!_sliderView) {
        _sliderView = [[YJSliderView alloc] initWithFrame:CGRectZero];
        _sliderView.delegate = self;
        _sliderView.themeColor = [UIColor moGreen];
        _sliderView.lineWith = 60;
        _sliderView.lineColor = [UIColor moGreen];
    }
    return _sliderView;
}

- (NSArray *)sliderDataSourceArr {
    if (!_sliderDataSourceArr) {
        _sliderDataSourceArr = @[@"现金充值", @"SLA充值"];
    }
    return _sliderDataSourceArr;
}

- (NSArray *)sliderViewArr {
    if (!_sliderViewArr) {
        [self addChildViewController:self.rechargeListVC];
        [self addChildViewController:self.slaRechargeListVC];
        _sliderViewArr = @[self.rechargeListVC.view, self.slaRechargeListVC.view];
    }
    return _sliderViewArr;
}

@end
