//
//  WalletAddressViewVC.m
//  BOB
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletAddressViewVC.h"
#import "WalletAddressView.h"

@interface WalletAddressViewVC ()

@property (nonatomic, strong) UIView *backGroundView;

@property (nonatomic, strong) WalletAddressView *contentView;

@end

@implementation WalletAddressViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

#pragma mark - Public Method
+ (instancetype)showInViewController:(UIViewController *)vc {
    WalletAddressViewVC *showVC = [WalletAddressViewVC new];
    if (vc) {
        [vc addChildViewController:showVC];
        [vc.view addSubview:showVC.view];
        [showVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.insets(UIEdgeInsetsZero);
        }];
        [showVC show];
    }
    return showVC;
}

- (void)addContentViewAnimation {
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.calculationMode = kCAAnimationCubic;
    animation.values = @[@1.07,@1.06,@1.05,@1.03,@1.02,@1.01,@1.0];
    animation.duration = 0.2;
    [self.contentView.layer addAnimation:animation forKey:@"transform.scale"];
}

- (void)disMiss {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

#pragma mark - Private Method
- (void)show {
    self.backGroundView.alpha = 0.7;
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.contentView.alpha = 1;
    } completion:nil];
    
    [self addContentViewAnimation];
}

#pragma mark - InitUI
- (void)createUI {
    self.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.backGroundView];
    [self.view addSubview:self.contentView];
    
    [self layoutUI];
}

- (void)layoutUI {
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.contentView.size);
        make.center.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (UIView *)backGroundView {
    if (!_backGroundView) {
        _backGroundView = [UIView new];
        _backGroundView.backgroundColor = [UIColor moBlack];
        _backGroundView.alpha = 0;
        @weakify(self);
        [_backGroundView addAction:^(UIView *view) {
            @strongify(self);
            [self disMiss];
        }];
    }
    return _backGroundView;
}

- (WalletAddressView *)contentView {
    if (!_contentView) {
        _contentView = [[WalletAddressView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 80, [WalletAddressView viewHeight])];
        @weakify(self)
        [_contentView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self disMiss];
        }];
        _contentView.alpha = 0;
    }
    return _contentView;
}

@end
