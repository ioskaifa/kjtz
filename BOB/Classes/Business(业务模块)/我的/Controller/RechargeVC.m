//
//  RechargeVC.m
//  BOB
//
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "RechargeVC.h"
#import "STDTableCellView.h"
#import "PolymerPayManager.h"

@interface RechargeVC ()

@property (nonatomic, strong) UITableView *tableView;
///充值金额
@property (nonatomic, strong) UITextField *tf;
///支付宝
@property (nonatomic, strong) STDTableCellView *aliPayView;
///微信
@property (nonatomic, strong) STDTableCellView *wechatView;
///
@property (nonatomic, strong) STDTableCellView *selectedView;

@property (nonatomic, strong) UIButton *submitBtn;

@end

@implementation RechargeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)navBarRightBtnAction:(id)sender {
    MXRoute(@"RechargeListVC", nil)
}

#pragma mark 选择的充值类型
- (NSString *)selectPayType {
    if (self.selectedView == self.aliPayView) {
        return @"支付宝";
    } else if (self.selectedView == self.wechatView) {
        return @"微信";
    } else {
        return @"支付宝";
    }
}

#pragma mark 选择充值类型
- (PolymerPayType)formatPayType:(NSString *)selectPayType {
    PolymerPayType payType = PolymerPayTypeAliPay;
    if ([@"支付宝" isEqualToString:selectPayType]) {
        payType = PolymerPayTypeAliPay;
    } else if ([@"微信" isEqualToString:selectPayType]) {
        payType = PolymerPayTypeWePay;
    }
    return payType;
}

- (NSString *)formatRechargeType:(NSString *)selectPayType {
    NSString *string;
    if ([@"支付宝" isEqualToString:selectPayType]) {
        string = @"01";
    } else if ([@"微信" isEqualToString:selectPayType]) {
        string = @"02";
    }
    return string;
}

- (BOOL)valiPara {
    [self.tf resignFirstResponder];
    NSString *value = [StringUtil trim:self.tf.text];
    if ([StringUtil isEmpty:value]) {
        [NotifyHelper showMessageWithMakeText:@"请输入充值金额"];
        return NO;
    }
    return YES;
}

- (void)commit:(NSString *)selectPayType {
    NSString *value = [StringUtil trim:self.tf.text];
    PolymerPayType payType = [self formatPayType:selectPayType];
    NSDictionary *param = @{@"recharge_type":[self formatRechargeType:selectPayType],
                            @"recharge_num":value,
                            @"account_type":@"01"
    };
    [[PolymerPayManager shared] pay:payType
                                url:@"/api/user/recharge/userRechargeOnLineApp"
                              param:param viewController:self block:^(id  _Nullable data) {
        if([data isSuccess]) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [NotifyHelper showMessageWithMakeText:[data valueForKey:@"msg"]];
        }
    }];
}

- (void)selectPayClick:(UIView *)sender {
    if (![sender isKindOfClass:STDTableCellView.class]) {
        return;
    }
    if (sender == self.selectedView) {
        return;
    }
    [self.selectedView configureRightImg:@"icon_red_unselect"];
    STDTableCellView *cellView = (STDTableCellView *)sender;
    [cellView configureRightImg:@"icon_green_select"];
    self.selectedView = cellView;
}

- (void)createUI {
    [self setNavBarTitle:@"充值"];
    [self setNavBarRightBtnWithTitle:@"充值记录" andImageName:nil];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self initUI];
    self.selectedView = self.aliPayView;
}

- (void)initUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myWidth = SCREEN_WIDTH;
    baseLayout.myHeight = MyLayoutSize.wrap;
    
    [baseLayout addSubview:[UIView fullLine]];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [baseLayout addSubview:layout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor colorWithHexString:@"#5C5C5C"];
    lbl.text = @"充值金额";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 20;
    lbl.myLeft = 20;
    [layout addSubview:lbl];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 20;
    subLayout.myLeft = 20;
    subLayout.myRight = 20;
    subLayout.myHeight = MyLayoutSize.wrap;
    subLayout.myBottom = 20;
    [layout addSubview:subLayout];
    
    lbl = [UILabel new];
    lbl.font = [UIFont boldFont20];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"￥";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 0;
    [subLayout addSubview:lbl];
    [subLayout addSubview:self.tf];
    
    [layout addSubview:[UIView gapLine]];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"选择充值方式";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 20;
    lbl.myLeft = 20;
    lbl.myBottom = 0;
    [layout addSubview:lbl];
    
    [layout addSubview:self.aliPayView];
    [layout addSubview:self.wechatView];
    [layout layoutSubviews];
    
    [baseLayout addSubview:self.submitBtn];
    
    [baseLayout layoutSubviews];
    self.tableView.tableHeaderView = baseLayout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = ({
            UITableView *object = [UITableView new];
            object.tableFooterView = [UIView new];
            object.backgroundColor = [UIColor moBackground];
            object;
        });
    }
    return _tableView;
}

- (UITextField *)tf {
    if (!_tf) {
        _tf = [UITextField new];
        _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
        _tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _tf.myLeft = 15;
        _tf.myRight = 0;
        _tf.weight = 1;
        _tf.height = 30;
        _tf.myHeight = _tf.height;
        _tf.myCenterY = 0;
        _tf.font = [UIFont font20];
        NSArray *txtArr = @[@"请输入充值金额"];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#DBDBDB"]];
        NSArray *fontArr = @[[UIFont font20]];
        NSAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        _tf.attributedPlaceholder = att;
    }
    return _tf;
}

- (STDTableCellView *)aliPayView {
    if (!_aliPayView) {
        _aliPayView = ({
            NSArray *txtArr = @[@"支付宝"];
            NSArray *colorArr = @[[UIColor moBlack]];
            NSArray *fontArr = @[[UIFont font15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                                colors:colorArr
                                                                                 fonts:fontArr];
            NSAttributedString *leftAtt = att;
            NSAttributedString *rightAtt = [[NSAttributedString alloc] initWithString:@""];
            STDTableCellView *object = [[STDTableCellView alloc] initWithLeftImg:@"支付宝"
                                                                         leftTxt:leftAtt
                                                                        rightTxt:rightAtt
                                                                        rightImg:@"icon_green_select"];
            object.myLeft = 20;
            object.myRight = 20;
            object.myHeight = 50;
            object.myTop = 10;
            @weakify(self)
            [object addAction:^(UIView *view) {
                @strongify(self)
                [self selectPayClick:view];
            }];
            object;
        });
    }
    return _aliPayView;
}

- (STDTableCellView *)wechatView {
    if (!_wechatView) {
        _wechatView = ({
            NSArray *txtArr = @[@"微信"];
            NSArray *colorArr = @[[UIColor moBlack]];
            NSArray *fontArr = @[[UIFont font15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                                colors:colorArr
                                                                                 fonts:fontArr];
            NSAttributedString *leftAtt = att;
            NSAttributedString *rightAtt = [[NSAttributedString alloc] initWithString:@""];
            STDTableCellView *object = [[STDTableCellView alloc] initWithLeftImg:@"微信"
                                                                         leftTxt:leftAtt
                                                                        rightTxt:rightAtt
                                                                        rightImg:@"icon_red_unselect"];
            object.myLeft = 20;
            object.myRight = 20;
            object.myHeight = 50;
            @weakify(self)
            [object addAction:^(UIView *view) {
                @strongify(self)
                [self selectPayClick:view];
            }];
            object;
        });
    }
    return _wechatView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"立即充值" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myLeft = 15;
            object.myRight = 15;
            object.myTop = 50;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                if ([self valiPara]) {
                    [self commit:[self selectPayType]];
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

@end
