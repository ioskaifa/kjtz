//
//  ModifyPayPwdVC.m
//  BOB
//
//  Created by mac on 2020/9/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ModifyPayPwdVC.h"
#import "VerityCodeView.h"
#import "ImageVerifyView.h"
#import "NSObject+LoginHelper.h"
#import "MineHelper.h"
#import "LeftRightTextFieldView.h"

@interface ModifyPayPwdVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *numView;

@property (nonatomic, strong) VerityCodeView  *codeView;

@property (nonatomic, strong) ImageVerifyView  *imgVerView;

@property (nonatomic, strong) LeftRightTextFieldView  *pwdView;
@property (nonatomic, strong) LeftRightTextFieldView  *rePwdView;
@property (nonatomic, strong) UIButton        *submitBtn;
@property (nonatomic, copy) NSString *img_id;
@property (nonatomic, copy) NSString *img_code;

@end

@implementation ModifyPayPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self reloadImgVerCode];
}

#pragma mark - Private Method
- (void)reloadImgVerCode {
    [self createImgCode:@{@"interface_type":@"createImgCode"}
             completion:^(id object, NSString *error) {
        if (object) {
            if (object[@"img_io"]) {
                [self.imgVerView reloadImage:[UIImage imageWithDataString:object[@"img_io"]]];
            }
            self.img_id = object[@"img_id"]?:@"";
        }
    }];
}

- (NSString *)phoneNo {
    NSString *phone = UDetail.user.user_tel?:@"";
    if ([StringUtil isEmpty:phone]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不能为空"];
        return @"";
    }
    if (![DCCheckRegular dc_checkTelNumber:phone]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不正确"];
        return @"";
    }
    return phone;
}

- (BOOL)valiParam {
    if ([StringUtil isEmpty:[self phoneNo]]) {
        [NotifyHelper showMessageWithMakeText:@"手机不能为空"];
        return NO;
    }
    if (![DCCheckRegular dc_checkTelNumber:[self phoneNo]]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不正确"];
        return NO;
    }
    if ([StringUtil isEmpty:self.img_code]) {
        [NotifyHelper showMessageWithMakeText:@"请输入图形验证码"];
        return NO;
    }
    if ([StringUtil isEmpty:self.codeView.tf.text]) {
        [NotifyHelper showMessageWithMakeText:@"请输入验证码"];
        return NO;
    }
    if ([StringUtil isEmpty:self.pwdView.value]) {
        [NotifyHelper showMessageWithMakeText:@"请输入密码"];
        return NO;
    }
    if (![self.pwdView.value isEqualToString:self.rePwdView.value]) {
        [NotifyHelper showMessageWithMakeText:@"两次密码不一致"];
        return NO;
    }
    if (self.pwdView.value.length != 6) {
        [NotifyHelper showMessageWithMakeText:@"支付密码长度应为6位"];
        return NO;
    }
    if (![StringUtil isNumberString:self.pwdView.value]) {
        [NotifyHelper showMessageWithMakeText:@"支付密码应为6位数字"];
        return NO;
    }
    return YES;
}

- (void)commit {
    NSDictionary *param = @{@"pay_password":self.pwdView.value?:@"",
                            @"sms_code":self.codeView.tf.text?:@""};
    [MineHelper modifyPayPass:param
                   completion:^(BOOL success, NSString *error) {
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];
            UDetail.user.isSetPayPassword = YES;            
            if (self.complete) {
                self.complete(nil);
            }
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark - InitUI
-(void)setupUI {
    if ([StringUtil isEmpty:self.title]) {
        [self setNavBarTitle:@"修改支付密码"];
    } else {
        [self setNavBarTitle:self.title];
    }
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    MyLinearLayout *layout= [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 10;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self.view addSubview:layout];
    
    [layout addSubview:self.numView];
    [layout addSubview:[UIView gapLine]];
    [layout addSubview:self.imgVerView];
    [layout addSubview:[UIView fullLine]];
    [layout addSubview:self.codeView];
    [layout addSubview:self.pwdView];
    [layout addSubview:[UIView fullLine]];
    [layout addSubview:self.rePwdView];
    [layout addSubview:self.submitBtn];
    
    [layout layoutSubviews];
    self.tableView.tableHeaderView = layout;
}

-(UIView* )numView{
    if (!_numView) {
        _numView = ({
            UIView *object = [UIView stdCellView:@"手机号码"
                                           right:[StringUtil telNumberFormat344:UDetail.user.user_tel]
                                           arrow:@""
                                       rowHeight:50];
            object;
        });

    }
    return _numView;
}

-(VerityCodeView* )codeView{
    if (!_codeView) {
        _codeView = [[VerityCodeView alloc] initWithFrame:CGRectZero];
        _codeView.backgroundColor = [UIColor whiteColor];
        _codeView.myTop = 0;
        _codeView.myLeft = 0;
        _codeView.myRight = 0;
        _codeView.myHeight = 52;
        UILabel *object = [UILabel new];
        object.font = [UIFont boldFont15];
        object.textColor = [UIColor moBlack];
        object.text = @"验证码";
        object.size = CGSizeMake(100, 15);
        [_codeView addSubview:object];
        [object mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_codeView.mas_left).offset(15);
            make.size.mas_equalTo(object.size);
            make.centerY.mas_equalTo(_codeView.centerY);
        }];
        
        [_codeView.tf mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(_codeView.mas_centerY);
            make.left.mas_equalTo(object.mas_right).offset(15);
            make.right.mas_equalTo(_codeView.countDowView.mas_left).offset(-5);
        }];
        
        @weakify(self)
        _codeView.sendCode = ^(id data) {
            @strongify(self)
            if ([StringUtil isEmpty:[self phoneNo]]) {
                return NO;
            }
            if ([StringUtil isEmpty:self.img_id]) {
                [NotifyHelper showMessageWithMakeText:@"请刷新图形验证码"];
                return NO;
            }
            if ([StringUtil isEmpty:self.img_code]) {
                [NotifyHelper showMessageWithMakeText:@"请输入图形验证码"];
                return NO;
            }
            [self send_codeToken:@{@"bus_type":@"FrontModifyPayPass",
                                   @"img_id":self.img_id?:@"",
                                   @"img_code":self.img_code?:@""}
                 completion:^(BOOL success, NSString *error) {
                
            }];
            return YES;
            };
    }
    return _codeView;
}

- (ImageVerifyView *)imgVerView {
    if (!_imgVerView) {
        _imgVerView = [[ImageVerifyView alloc] initWithFrame:CGRectZero];
        _imgVerView.backgroundColor = [UIColor whiteColor];
        _imgVerView.myTop = 0;
        _imgVerView.myLeft = 0;
        _imgVerView.myRight = 0;
        _imgVerView.myHeight = 52;
        UILabel *object = [UILabel new];
        object.font = [UIFont boldFont15];
        object.textColor = [UIColor moBlack];
        object.text = @"图形验证";
        object.size = CGSizeMake(100, 15);
        [_imgVerView addSubview:object];
        [object mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_imgVerView.mas_left).offset(15);
            make.size.mas_equalTo(object.size);
            make.centerY.mas_equalTo(_imgVerView.centerY);
        }];
        
        [_imgVerView.tf mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(_imgVerView.mas_centerY);
            make.left.mas_equalTo(object.mas_right).offset(15);
            make.right.mas_equalTo(_imgVerView.btn.mas_left).offset(-5);
        }];
        @weakify(self)
        _imgVerView.getText = ^(NSString * data) {
            @strongify(self)
            self.img_code = data;
        };
        _imgVerView.picBlock = ^(id  _Nullable data) {
            @strongify(self)
            [self reloadImgVerCode];
        };
    }
    return _imgVerView;
}

-(LeftRightTextFieldView* )pwdView{
    if (!_pwdView) {
        _pwdView = ({
            LeftRightTextFieldView *object = [LeftRightTextFieldView leftRightView:@"新密码"
                                                                   withPlaceholder:@"新支付密码"];
            object.backgroundColor = [UIColor whiteColor];
            object.rightTF.keyboardType = UIKeyboardTypeNumberPad;
            object.rightTF.clearButtonMode = UITextFieldViewModeWhileEditing;
            object.myTop = 0;
            object.myLeft = 0;
            object.myWidth = SCREEN_WIDTH;
            object.myHeight = 52;
            object.rightTF.secureTextEntry = YES;
            object;
        });
    }
    return _pwdView;
}

-(LeftRightTextFieldView* )rePwdView{
    if (!_rePwdView) {
        _rePwdView = ({
            LeftRightTextFieldView *object = [LeftRightTextFieldView leftRightView:@"确认密码"
                                                                   withPlaceholder:@"确认密码"];
            object.backgroundColor = [UIColor whiteColor];
            object.rightTF.keyboardType = UIKeyboardTypeNumberPad;
            object.rightTF.clearButtonMode = UITextFieldViewModeWhileEditing;
            object.myTop = 0;
            object.myLeft = 0;
            object.myWidth = SCREEN_WIDTH;
            object.myHeight = 52;
            object.rightTF.secureTextEntry = YES;
            object;
        });
    }
    return _rePwdView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"确认修改" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myTop = 50;
            object.myLeft = 15;
            object.myRight = 15;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                if ([self valiParam]) {
                    [self commit];
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor moBackground];
    }
    return _tableView;
}

@end
