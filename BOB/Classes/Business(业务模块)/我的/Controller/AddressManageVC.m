//
//  AddressManageVC.m
//  BOB
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AddressManageVC.h"
#import "ChooseLocationView.h"
#import "DCPlaceholderTextView.h"

@interface AddressManageVC ()

@property (nonatomic, strong) MyBaseLayout *nameLayout;

@property (nonatomic, strong) MyBaseLayout *telLayout;

@property (nonatomic, strong) MyBaseLayout *areaLayout;

@property (nonatomic, strong) MyBaseLayout *addressLayout;
///设置默认地址
@property (nonatomic, strong) MyBaseLayout *defualtLayout;
///详细地址
@property (nonatomic, strong) DCPlaceholderTextView *txtView;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIButton *submitBtn;

@property (nonatomic, strong) ChooseLocationView *chooseLocationView;

@property (nonatomic, copy) NSString *area;

@end

@implementation AddressManageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)defualtLayoutClick:(UIView *)sender {
    SPButton *btn = [sender viewWithTag:1];
    if (![btn isKindOfClass:SPButton.class]) {
        return;
    }
    btn.selected = !btn.selected;
}

- (void)areaLayoutClick:(UIView *)sender {
    [self.view endEditing:YES];
    if([self.chooseLocationView superview] != nil) {
        return;
    }
    self.chooseLocationView = [ChooseLocationView show:@""];
    @weakify(self)
    [self.chooseLocationView setChooseFinish:^(NSString *address) {
        @strongify(self)
        [self setAreaLayoutValue:address];
    }];
}

- (void)setNameLayoutValue:(NSString *)string {
    UITextField *tf = [self.nameLayout viewWithTag:1];
    tf.text = string;
}

- (NSString *)getNameLayoutValue {
    UITextField *tf = [self.nameLayout viewWithTag:1];
    return [StringUtil trim:tf.text];
}

- (void)setTelLayoutValue:(NSString *)string {
    UITextField *tf = [self.telLayout viewWithTag:1];
    tf.text = string;
}

- (NSString *)getTelLayoutValue {
    UITextField *tf = [self.telLayout viewWithTag:1];
    return [StringUtil trim:tf.text];
}

- (void)setAreaLayoutValue:(NSString *)string {
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@"·"];
    self.area = string;
    UITextField *tf = [self.areaLayout viewWithTag:1];
    tf.text = string;
}

- (NSString *)getAreaLayoutValue {
    UITextField *tf = [self.areaLayout viewWithTag:1];
    return [StringUtil trim:tf.text];
}

- (void)setAddressLayoutValue:(NSString *)string {
    self.txtView.text = string;
}

- (NSString *)getAddressLayoutValue {
    return [StringUtil trim:self.txtView.text];
}

- (NSString *)getDefualtLayoutValue {
    SPButton *btn = [self.defualtLayout viewWithTag:1];
    if (![btn isKindOfClass:SPButton.class]) {
        return @"0";
    }
    return btn.selected ? @"1":@"0";
}

- (void)setDefualtLayoutValue:(BOOL)value {
    SPButton *btn = [self.defualtLayout viewWithTag:1];
    if (![btn isKindOfClass:SPButton.class]) {
        return;
    }
    btn.selected = value;
}

- (NSString *)getAddress {
    NSString *string = StrF(@"%@·%@", self.area, [self getAddressLayoutValue]);
    return string;
}

- (NSDictionary *)apiParam {
    NSDictionary *param;
    switch (self.type) {
        case AddressManageVCTypeAdd: {
            param = @{
                @"user_address_oper":@"user_address_add",
                @"name":[self getNameLayoutValue],
                @"tel":[self getTelLayoutValue],
                @"address":[self getAddress],
                @"isdefault":[self getDefualtLayoutValue]
            };
            break;
        }
        case AddressManageVCTypeEdit: {
            param = @{
                @"user_address_oper":@"user_address_edit",
                @"name":[self getNameLayoutValue],
                @"tel":[self getTelLayoutValue],
                @"address":[self getAddress],
                @"isdefault":[self getDefualtLayoutValue],
                @"address_id":self.obj.address_id?:@""
            };
            break;
        }
        case AddressManageVCTypeSet: {
            param = @{
                @"user_address_oper":@"user_address_set",
                @"isdefault":[self getDefualtLayoutValue],
                @"address_id":self.obj.address_id?:@"",
            };
            break;
        }
        default:
            break;
    }
    return param;
}

- (NSString *)navTitle {
    NSString *string;
    switch (self.type) {
        case AddressManageVCTypeAdd: {
            string = @"添加新地址";
            break;
        }
        case AddressManageVCTypeEdit: {
            string = @"修改收货地址";
            break;
        }
        case AddressManageVCTypeSet: {
            string = @"修改收货地址";
            break;
        }
        default:
            string = @"修改收货地址";
            break;
    }
    return string;
}

- (NSString *)btnTitle {
    NSString *string;
    switch (self.type) {
        case AddressManageVCTypeAdd: {
            string = @"添加新地址";
            break;
        }
        case AddressManageVCTypeEdit: {
            string = @"保存";
            break;
        }
        case AddressManageVCTypeSet: {
            string = @"保存";
            break;
        }
        default:
            string = @"修改收货地址";
            break;
    }
    return string;
}

- (BOOL)valiParam {
    NSString *name = [self getNameLayoutValue];
    if ([StringUtil isEmpty:name]) {
        [NotifyHelper showMessageWithMakeText:@"收货人姓名不能为空"];
        return NO;
    }
    if (name.length > 10) {
        [NotifyHelper showMessageWithMakeText:@"收货人姓名长度不能超过10位"];
        return NO;
    }
    NSString *tel = [self getTelLayoutValue];
    if ([StringUtil isEmpty:tel]) {
        [NotifyHelper showMessageWithMakeText:@"手机号码不能为空"];
        return NO;
    }
    if (![DCCheckRegular dc_checkTelNumber:tel]) {
        [NotifyHelper showMessageWithMakeText:@"手机号码有误"];
        return NO;
    }
    if ([StringUtil isEmpty:self.area]) {
        [NotifyHelper showMessageWithMakeText:@"请选择所在地区"];
        return NO;
    }
    if ([StringUtil isEmpty:[self getAddressLayoutValue]]) {
        [NotifyHelper showMessageWithMakeText:@"详细地址不能为空"];
        return NO;
    }
    return YES;
}

- (void)commit {
    [self request:@"api/user/address/updateUserAddress"
            param:[self apiParam]
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)createUI {
    [self setNavBarTitle:[self navTitle]];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    layout.backgroundColor = [UIColor moBackground];
    
    [layout addSubview:self.nameLayout];
    [layout addSubview:self.telLayout];
    [layout addSubview:self.areaLayout];
    [layout addSubview:self.addressLayout];
    [layout addSubview:self.defualtLayout];
    [layout addSubview:self.submitBtn];
    
    [layout layoutIfNeeded];
    self.tableView.tableHeaderView = layout;
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    if ([self.obj isKindOfClass:UserAddressListObj.class]) {
        [self setNameLayoutValue:self.obj.name];
        [self setTelLayoutValue:self.obj.tel];
        [self setAreaLayoutValue:self.obj.address_area];
        [self setAddressLayoutValue:self.obj.address_des];
        [self setDefualtLayoutValue:self.obj.isdefault];
    }
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor moBackground];
    }
    return _tableView;
}

- (MyBaseLayout *)nameLayout {
    if (!_nameLayout) {
        _nameLayout = ({
            MyBaseLayout *object = [self.class factoryLblTxtField:@"收货人"
                                                      placeholder:@"收货人姓名"
                                                       rightArrow:@""];
            object;
        });
    }
    return _nameLayout;
}

- (MyBaseLayout *)telLayout {
    if (!_telLayout) {
        _telLayout = ({
            MyBaseLayout *object = [self.class factoryLblTxtField:@"手机号"
                                                      placeholder:@"手机号码"
                                                       rightArrow:@""];
            object;
        });
    }
    return _telLayout;
}

- (MyBaseLayout *)areaLayout {
    if (!_areaLayout) {
        _areaLayout = ({
            MyBaseLayout *object = [self.class factoryLblTxtField:@"所在地区"
                                                      placeholder:@"请选择"
                                                       rightArrow:@"Arrow"];
            UITextField *tf = [object viewWithTag:1];
            tf.userInteractionEnabled = NO;
            [object addAction:^(UIView *view) {
                [self areaLayoutClick:view];
            }];
            object;
        });
    }
    return _areaLayout;
}

- (MyBaseLayout *)addressLayout {
    if (!_addressLayout) {
        _addressLayout = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            object.backgroundColor = [UIColor whiteColor];
            object.height = 100;
            object.myHeight = object.height;
            object.myLeft = 0;
            object.myRight = 0;
            object.myTop = 1;
            
            UILabel *lbl = [UILabel new];
            lbl.font = [UIFont boldFont15];
            lbl.textColor = [UIColor moBlack];
            lbl.mySize = CGSizeMake(100, 20);
            lbl.text = @"详细地址";
            lbl.myTop = 22;
            lbl.myLeft = 20;
            [object addSubview:lbl];
            [object addSubview:self.txtView];
            object;
        });
    }
    return _addressLayout;
}

- (DCPlaceholderTextView *)txtView {
    if (!_txtView) {
        _txtView = ({
            DCPlaceholderTextView *tv = [DCPlaceholderTextView new];
            tv.placeholder = @"请详细填写收货地址";
            tv.placeholderFont = [UIFont font15];
            tv.placeholderColor = [UIColor colorWithHexString:@"#C1C1C1"];
            tv.textColor = [UIColor moBlack];
            tv.font = [UIFont font15];
            tv.myLeft = 0;
            tv.myRight = 15;
            tv.myTop = 15;
            tv.weight = 1;
            tv.myHeight = 100 - 30;
            tv;
        });
    }
    return _txtView;
}

- (MyBaseLayout *)defualtLayout {
    if (!_defualtLayout) {
        _defualtLayout = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            object.backgroundColor = [UIColor whiteColor];
            object.myTop = 20;
            object.myHeight = 44;
            object.myLeft = 0;
            object.myRight = 0;
            SPButton *btn = [SPButton new];
            btn.imagePosition = SPButtonImagePositionLeft;
            btn.imageTitleSpace = 10;
            btn.titleLabel.font = [UIFont boldFont15];
            [btn setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
            [btn setImage:[UIImage imageNamed:@"icon_red_unselect"] forState:UIControlStateNormal];
            [btn setImage:[UIImage imageNamed:@"icon_green_select"] forState:UIControlStateSelected];
            [btn setTitle:@"设为默认地址" forState:UIControlStateNormal];
            [btn sizeToFit];
            btn.mySize = btn.size;
            btn.myLeft = 15;
            btn.myCenterY = 0;
            btn.userInteractionEnabled = NO;
            btn.selected = YES;
            btn.tag = 1;
            [object addSubview:btn];
            @weakify(self)
            [object addAction:^(UIView *view) {
                @strongify(self)
                [self defualtLayoutClick:view];
            }];
            object;
        });
    }
    return _defualtLayout;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:[self btnTitle] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myTop = 50;
            object.myLeft = 15;
            object.myRight = 15;
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                if ([self valiParam]) {
                    [self commit];
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

+ (MyBaseLayout *)factoryLblTxtField:(NSString *)title
                         placeholder:(NSString *)placeholder rightArrow:(NSString *)rightArrow {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myHeight = 50;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myTop = 1;
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor moBlack];
    lbl.mySize = CGSizeMake(100, 20);
    lbl.text = title;
    lbl.myCenterY = 0;
    lbl.myLeft = 20;
    [layout addSubview:lbl];
    
    UITextField *tf = [UITextField new];
    tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    tf.textColor = [UIColor moBlack];
    tf.font = [UIFont font15];
    tf.myLeft = 0;
    tf.myCenterY = 0;
    tf.myHeight = 25;
    tf.myRight = 15;
    tf.weight = 1;
    tf.tag = 1;
    NSArray *txtArr = @[placeholder];
    NSArray *fontArr = @[[UIFont font15]];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#C1C1C1"]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    tf.attributedPlaceholder = att;
    [layout addSubview:tf];
    
    if (![StringUtil isEmpty:rightArrow]) {
        UIImageView *imgView = [UIImageView autoLayoutImgView:rightArrow];
        imgView.myCenterY = 0;
        imgView.myRight = 15;
        [layout addSubview:imgView];
    }
    
    return layout;
}

@end
