//
//  SLAWalletVC.m
//  BOB
//
//  Created by mac on 2020/10/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SLAWalletVC.h"
#import "WalletAddressViewVC.h"

@interface SLAWalletVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UILabel *sla_numLbl;

@property (nonatomic, strong) UILabel *freeze_sla_numLbl;

@end

@implementation SLAWalletVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.sla_numLbl.text = StrF(@"%0.4f", UDetail.user.sla_num);
    [self.sla_numLbl sizeToFit];
    self.sla_numLbl.mySize = self.sla_numLbl.size;
    
    self.freeze_sla_numLbl.text = StrF(@"（冻结的SLA：%0.4f）", UDetail.user.freeze_sla_num);
    [self.freeze_sla_numLbl sizeToFit];
    self.freeze_sla_numLbl.mySize = self.freeze_sla_numLbl.size;
}

- (void)navBarRightBtnAction:(id)sender {
    MXRoute(@"WalletRecordListVC", @{@"title":@"钱包明细"});
}

#pragma mark 获取钱包地址
- (void)fetchWalletAddress {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [self request:@"api/user/info/distributeAddress"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            WalletInfoObj *obj = [WalletInfoObj modelParseWithDict:object[@"data"][@"userInfo"]];
            if ([StringUtil isEmpty:obj.eth_address]) {
                [NotifyHelper showMessageWithMakeText:@"暂未开放"];
                return;
            }
            UDetail.user.eth_address = obj.eth_address?:@"";
            [WalletAddressViewVC showInViewController:[[MXRouter sharedInstance] getMallTopNavigationController]];
        }
    }];
}

#pragma mark - IBAction
#pragma mark - 充值
- (void)rechargeClick {
    [self fetchWalletAddress];
}

#pragma mark - 提现
- (void)withdrawal {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [self request:@"/api/wallet/send/getWalletSendLock"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            BOOL flag = [object[@"data"][@"userSendSlaLock"] boolValue];
            if (flag) {
                MXRoute(@"SLAWithdrawalVC", nil);
            } else {
                [NotifyHelper showMessageWithMakeText:@"暂未开放"];
            }
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)createUI {
    [self setNavBarTitle:@"钱包"];
    [self setNavBarRightBtnWithTitle:@"明细" andImageName:nil];
    
    MyFrameLayout *baseLayout = [MyFrameLayout new];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.view addSubview:baseLayout];
    
    [baseLayout addSubview:self.tableView];
    self.tableView.tableHeaderView = [self headerView];
}

- (MyBaseLayout *)headerView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"sla_logo"];
    imgView.myCenterX = 0;
    imgView.myTop = 50;
    [layout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font18];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"SLA余额";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myTop = 30;
    lbl.myBottom = 10;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont fontWithName:@"DINAlternate-Bold" size:50];
    lbl.textColor = [UIColor blackColor];
    lbl.text = StrF(@"%0.4f", UDetail.user.sla_num);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    self.sla_numLbl = lbl;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font12];
    lbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    lbl.text = StrF(@"（冻结的SLA：%0.4f）", UDetail.user.freeze_sla_num);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myTop = 5;
    self.freeze_sla_numLbl = lbl;
    lbl.visibility = MyVisibility_Gone;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont boldFont16];
    [lbl setViewCornerRadius:5];
    lbl.textColor = [UIColor whiteColor];
    lbl.backgroundColor = [UIColor colorWithHexString:@"#02C1C4"];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.text = @"充值";
    lbl.myLeft = 35;
    lbl.myRight = 35;
    lbl.myHeight = 50;
    lbl.myTop = 100;
    lbl.myBottom = 10;
    @weakify(self)
    [lbl addAction:^(UIView *view) {
        @strongify(self)
        [self rechargeClick];
    }];
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont boldFont16];
    [lbl setViewCornerRadius:5];
    lbl.textColor = [UIColor whiteColor];
    lbl.backgroundColor = [UIColor colorWithHexString:@"#14151A"];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.text = @"提现";
    lbl.myLeft = 35;
    lbl.myRight = 35;
    lbl.myBottom = 10;
    lbl.myHeight = 50;    
    [lbl addAction:^(UIView *view) {
        @strongify(self)
        [self withdrawal];
    }];
    [layout addSubview:lbl];
    
    layout.myWidth = SCREEN_WIDTH;
    [layout layoutIfNeeded];
    return layout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.weight = 1;
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.myBottom = 0;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

@end
