//
//  ShippingAddressVC.h
//  BOB
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShippingAddressVC : BaseViewController

@property (nonatomic, copy)FinishedBlock block;

@end

NS_ASSUME_NONNULL_END
