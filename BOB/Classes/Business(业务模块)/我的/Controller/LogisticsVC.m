//
//  LogisticsVC.m
//  BOB
//
//  Created by Colin on 2020/10/30.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LogisticsVC.h"
#import "ShipperListCell.h"

@interface LogisticsVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@end

@implementation LogisticsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = ShipperListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    if (![cell isKindOfClass:ShipperListCell.class]) {
        return;
    }
    ShipperListCell *listCell = (ShipperListCell *)cell;
    [listCell configureView:self.dataSourceArr[indexPath.row]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return 0;
    }
    
    TracesInfoObj *obj = self.dataSourceArr[indexPath.row];
    return obj.viewHeight;
}

- (void)createUI {
    [self setNavBarTitle:@"物流追踪"];
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.view addSubview:baseLayout];
    
    [baseLayout addSubview:[UIView fullLine]];
    [baseLayout addSubview:[self goodInfoView]];
    [baseLayout addSubview:[self logisticsView]];
    [baseLayout addSubview:[UIView gapLine]];
        
    [baseLayout addSubview:[self addressView]];
    
    [baseLayout addSubview:[UIView line]];
    
    [baseLayout addSubview:self.tableView];
}

- (MyBaseLayout *)goodInfoView {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    baseLayout.myHeight = 114;
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    
    UIImageView *imgView = [UIImageView new];
    imgView.size = CGSizeMake(85, 85);
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myLeft = 15;
    [imgView sd_setImageWithURL:[NSURL URLWithString:self.obj.goodObj.photo]];
    [baseLayout addSubview:imgView];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myLeft = 10;
    layout.size = CGSizeMake(SCREEN_WIDTH - 40 - imgView.width, imgView.height);
    layout.mySize = layout.size;
    layout.myCenterY = 0;
    [baseLayout addSubview:layout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont16];
    lbl.textColor = [UIColor moGreen];
    lbl.text = @"商品运输中";
    [lbl autoMyLayoutSize];
    [layout addSubview:lbl];
    [layout addSubview:[UIView placeholderVertView]];
    
    lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.font = [UIFont lightFont14];
    lbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    lbl.text = self.obj.goodObj.title;
    CGSize size = [lbl sizeThatFits:CGSizeMake(layout.width, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    lbl.myLeft = 0;
    lbl.myBottom = 0;
    [layout addSubview:lbl];
    
    return baseLayout;
}

- (MyBaseLayout *)logisticsView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor colorWithHexString:@"#F5F5FA"];
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = 35;
    layout.myBottom = 15;
    [layout setViewCornerRadius:2];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font14];
    lbl.textColor = [UIColor blackColor];
    lbl.text = StrF(@"[%@]: %@", self.obj.shipperObj.shipper_name, self.obj.shipperObj.logistic_code);
    lbl.myCenterY = 0;
    lbl.myLeft = 15;
    [lbl autoMyLayoutSize];
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView placeholderHorzView]];
    
    lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.size = CGSizeMake(44, 22);
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myRight = 15;
    [lbl setViewCornerRadius:lbl.height/2.0];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    lbl.backgroundColor = [UIColor whiteColor];
    lbl.layer.borderColor = [UIColor colorWithHexString:@"#AAAABB"].CGColor;
    lbl.layer.borderWidth = 1;
    lbl.text = @"复制";
    @weakify(lbl)
    [lbl addAction:^(UIView *view) {
        @strongify(lbl)
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = lbl.text;
        [NotifyHelper showMessageWithMakeText:@"复制成功"];
    }];
    [layout addSubview:lbl];
    
    return layout;
}

- (MyBaseLayout *)addressView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = MyLayoutSize.wrap;
    layout.myTop = 20;
    layout.myBottom = 20;
    
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"订单详情地址"];
    imgView.size = CGSizeMake(30, 30);
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myLeft = 0;
    imgView.myRight = 10;
    [layout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.font = [UIFont lightFont14];
    lbl.textColor = [UIColor blackColor];
    lbl.text = StrF(@"收货地址：%@", self.obj.userAddress);
    lbl.myCenterY = 0;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - imgView.width - 30 - 10, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    [layout addSubview:lbl];
    
    [layout layoutIfNeeded];
    return layout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.tableFooterView = [UIView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        Class cls = ShipperListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.weight = 1;
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
    }
    return _tableView;
}

- (NSArray *)dataSourceArr {
    if (!_dataSourceArr) {
        _dataSourceArr = self.obj.shipperObj.tracesArr;
    }
    return _dataSourceArr;
}

@end
