//
//  MyVC.m
//  BOB
//
//  Created by mac on 2020/9/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyVC.h"
#import "MyHeaderView.h"
#import "MyListCell.h"
#import "WalletAddressViewVC.h"
#import "MyServiceView.h"
#import "ManageAuthUserListObj.h"
#import "LcwlChat.h"
#import "MXChatManager.h"

@interface MyVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MyHeaderView *headerView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@end

@implementation MyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self.headerView configureView];
    [self.tableView reloadData];
    [self fetchData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Private Method
- (void)fetchData {
    @weakify(self)
    [UDetail get_userinfo:^(BOOL success, NSString *error) {
        @strongify(self)
        [self.tableView.mj_header endRefreshing];
        [self.headerView configureView];
    }];
    [self fetchOrderStatistics];
}


#pragma mark 获取钱包地址
- (void)fetchWalletAddress {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [self request:@"api/user/info/distributeAddress"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            WalletInfoObj *obj = [WalletInfoObj modelParseWithDict:object[@"data"][@"userInfo"]];
            if ([StringUtil isEmpty:obj.eth_address]) {
                [NotifyHelper showMessageWithMakeText:@"暂未开放"];
                return;
            }
            UDetail.user.eth_address = obj.eth_address?:@"";
            [WalletAddressViewVC showInViewController:[[MXRouter sharedInstance] getMallTopNavigationController]];
        }
    }];
}

#pragma mark 获取订单各个状态数量
- (void)fetchOrderStatistics {
    [self request:@"api/shop/order/getOrderStatistics"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSArray *dataArr = object[@"data"][@"orderStatisticsInfo"];
            NSMutableDictionary *MDic = [NSMutableDictionary dictionaryWithCapacity:dataArr.count];
            for (NSDictionary *dic in dataArr) {
                if (![dic isKindOfClass:NSDictionary.class]) {
                    continue;
                }
                if ([@"00" isEqualToString:[dic[@"order_status"] description]]) {
                    [MDic setValue:[dic[@"order_num"] description] forKey:@"待付款"];
                } else if ([@"02" isEqualToString:[dic[@"order_status"] description]]) {
                    [MDic setValue:[dic[@"order_num"] description] forKey:@"待发货"];
                } else if ([@"03" isEqualToString:[dic[@"order_status"] description]]) {
                    [MDic setValue:[dic[@"order_num"] description] forKey:@"待收货"];
                } else if ([@"04" isEqualToString:[dic[@"order_status"] description]]) {
                    [MDic setValue:[dic[@"order_num"] description] forKey:@"已取消"];
                } else if ([@"05" isEqualToString:[dic[@"order_status"] description]]) {
                    [MDic setValue:[dic[@"order_num"] description] forKey:@"退款"];
                } else if ([@"09" isEqualToString:[dic[@"order_status"] description]]) {
                    [MDic setValue:[dic[@"order_num"] description] forKey:@"已完成"];
                }
            }
            [self.headerView.orderView configureView:MDic];
        }
    }];
}

#pragma mark - Delegate
#pragma mark UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSourceMArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section >= self.dataSourceMArr.count) {
        return 0;
    }
    NSArray *tempArr = self.dataSourceMArr[section];
    if (![tempArr isKindOfClass:[NSArray class]]) {
        return 0;
    }
    return tempArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 15;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = CGRectMake(10, 5, SCREEN_WIDTH - 20, 10);
    [bgView setBorderWithCornerRadius:5 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 15);
    [view addSubview:bgView];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = CGRectMake(10, 0, SCREEN_WIDTH - 20, 10);
    [bgView setBorderWithCornerRadius:5 byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight];
    UIView *view = [UIView new];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, 15);
    [view addSubview:bgView];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = [MyListCell class];
    MyListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:[MyListCell class]]) {
        return;
    }
    NSArray *tempArr = self.dataSourceMArr[indexPath.section];
    if (![tempArr isKindOfClass:[NSArray class]]) {
        return;
    }
    if (indexPath.row >= tempArr.count) {
        return;
    }
    NSDictionary *obj = tempArr[indexPath.row];
    MyListCell *listCell = (MyListCell *)cell;
    [listCell configureView:obj];
    if (indexPath.row == tempArr.count - 1) {
        listCell.line.hidden = YES;
    } else {
        listCell.line.hidden = NO;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if (section >= self.dataSourceMArr.count) {
        return;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceMArr.count) {
        return;
    }
    NSArray *tempArr = self.dataSourceMArr[indexPath.section];
    if (![tempArr isKindOfClass:[NSArray class]]) {
        return;
    }
    if (indexPath.row >= tempArr.count) {
        return;
    }
    NSDictionary *obj = tempArr[indexPath.row];
    if ([@"意见反馈" isEqualToString:obj[@"title"]]) {
        MXRoute(@"FeedBackVC", nil);
        return;
    }
    if ([@"消息中心" isEqualToString:obj[@"title"]]) {
        MXRoute(@"MessageCenterVC", nil);
        return;
    }
    if ([@"余额记录" isEqualToString:obj[@"title"]]) {
        MXRoute(@"WalletRecordListVC", @{@"title":@"余额记录"});
        return;
    }
    if ([@"收货地址" isEqualToString:obj[@"title"]]) {        
        MXRoute(@"ShippingAddressVC", nil)
        return;
    }
    if ([@"钱包地址" isEqualToString:obj[@"title"]]) {
        [self fetchWalletAddress];
        return;
    }
    if ([@"联系客服" isEqualToString:obj[@"title"]]) {
        [self getMemberServiceList];
        return;
    }
    
    if ([@"我的消息" isEqualToString:obj[@"title"]]) {
        MXRoute(@"ServiceMomentsVC", nil);
        return;
    }
    [NotifyHelper showMessageWithMakeText:@"暂未开放"];
}

///获取客服
- (void)getMemberServiceList {
    NSString *url = @"api/user/info/getMemberServiceList";
    url = StrF(@"%@/%@", LcwlServerRoot2, url);
    NSDictionary *param = @{};
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url);
        net.params(param);
        net.finish(^(id data){
            [NotifyHelper hideHUDForView:self.view animated:YES];
            
            if ([data isSuccess]) {
                NSArray *dataArr = data[@"data"][@"serviceList"];
                dataArr = [ManageAuthUserListObj modelListParseWithArray:dataArr];
                NSArray *tempArr = [dataArr valueForKeyPath:@"@unionOfObjects.ID"];
                [MXCache setValue:tempArr forKey:kMXMemberServiceList];
                
                NSArray *fried = [[LcwlChat shareInstance].chatManager friends];
                ///未保存在本地的用户
                NSMutableArray *notFriendMArr = [NSMutableArray array];
                ManageAuthUserListObj *obj = [dataArr firstObject];
                
                if (![obj isKindOfClass:ManageAuthUserListObj.class]) {
                    return;
                }
                //如果是自己则不显示
                if ([obj.ID isEqualToString:UDetail.user.user_id]) {
                    return;;
                }
                FriendModel *model = [FriendModel new];
                model.userid = obj.ID;
                model.name = obj.nick_name;
                model.user_name = obj.nick_name;
                model.nick_name = obj.nick_name;
                model.smartName = obj.nick_name;
                model.head_photo = obj.head_photo;
                model.avatar = obj.head_photo;
                
                //判断是否本地好友  不是本地好友 写入本地
                NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"userid == '%@'", obj.ID)];
                NSArray *temp2Arr = [fried filteredArrayUsingPredicate:predicate];
                if (temp2Arr.count > 0) {
                    //把备注带上
                    FriendModel *tempObj = temp2Arr.firstObject;
                    if ([tempObj isKindOfClass:FriendModel.class]) {
                        model.remark = tempObj.remark;
                    }
                    return;;
                }
                model.followState = MoRelationshipTypeStranger;
                model.creatorRole = 10; //客服
                [notFriendMArr addObject:model];
                
                if (notFriendMArr.count > 0) {
                    ///写入数据库 聊天都需要从本地数据库取用户信息
                    [[MXChatManager sharedInstance] insertFriends:notFriendMArr];
                }
                
                [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":model.userid,@"type":@(eConversationTypeChat),@"creatorRole":@(10)}];
            } else {
                [NotifyHelper showMessageWithMakeText:data[@"msg"]];
            }
        }).failure(^(id error){
            [NotifyHelper hideHUDForView:self.view animated:YES];
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

#pragma mark - InitUI
- (void)createUI {
    [self.view addSubview:self.tableView];
    [self layoutUI];
    
}

- (void)layoutUI {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        AdjustTableBehavior(_tableView);
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.headerView;
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [MyListCell viewHeight];
        _tableView.sectionFooterHeight = 0.01;
        Class cls = [MyListCell class];
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        @weakify(self)
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self)
            [self fetchData];
        }];
    }
    return _tableView;
}

- (MyHeaderView *)headerView {
    if (!_headerView) {
        CGFloat height = [MyHeaderView viewHeight];
        _headerView = [[MyHeaderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, height)];
    }
    return _headerView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
        NSDictionary *kaibo = @{@"title":@"申请开播", @"leftImg":@"申请开播_my", @"rightImg":@"Arrow"};
        NSDictionary *wallet = @{@"title":@"钱包地址", @"leftImg":@"钱包地址", @"rightImg":@"Arrow"};
        NSDictionary *bill = @{@"title":@"余额记录", @"leftImg":@"余额记录", @"rightImg":@"Arrow"};
        NSDictionary *address = @{@"title":@"收货地址", @"leftImg":@"收货地址", @"rightImg":@"Arrow"};
        NSDictionary *server = @{@"title":@"联系客服", @"leftImg":@"联系客服", @"rightImg":@"Arrow"};
        NSDictionary *message = @{@"title":@"消息中心", @"leftImg":@"消息中心", @"rightImg":@"Arrow"};
        NSDictionary *feedback = @{@"title":@"意见反馈", @"leftImg":@"意见反馈", @"rightImg":@"Arrow"};
        
#ifdef Kefu_Server_dev
        if(UDetail.user.isSevice) {
            NSMutableArray *arr = [[self.dataSourceMArr lastObject] mutableCopy];
            if([arr count] <= 6) {
                NSDictionary *myMsgDic = @{@"title":@"我的消息", @"leftImg":@"我的消息", @"rightImg":@"Arrow"};
                [_dataSourceMArr addObject:@[kaibo, wallet, address, message, feedback, myMsgDic]];
            }
        } else {
            [_dataSourceMArr addObject:@[kaibo, wallet, address, server, message, feedback]];
        }
#else
        [_dataSourceMArr addObject:@[kaibo, wallet, address, server, message, feedback]];
#endif
        
    }
    return _dataSourceMArr;
}

@end
