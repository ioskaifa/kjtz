//
//  SLARechargeVC.m
//  BOB
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SLARechargeVC.h"
#import "SLARechargeCell.h"
#import "UserRechargeListObj.h"

@interface SLARechargeVC ()

@end

@implementation SLARechargeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNavBarTitle:@"SLA充值"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Override Super Class Method
- (HTTPRequestMethod)httpMethod {
    return HTTPRequestMethodPost;
}

- (NSDictionary *)requestArgument {
    return @{@"line_type":@"03"};
}

- (NSString *)requestUrl {
    return @"api/user/recharge/getUserRechargeList";
}

///请求时传入last id的key
- (NSString *)httpRequstLastIdKey {
    return @"last_id";
}

///上拉刷新时从数据源数组中最后一个元素取值的key
- (NSString *)getValueFromItemLastIdKey {
    return @"ID";
}

///后台返回数据中数组的keypath
- (NSString *)dataListKeyPath {
    return @"data.rechargeRecordList";
}

///数据源中的model,需要继承BaseObject
- (Class)model {
    return [UserRechargeListObj class];
}

///是否需要上拉刷新
- (BOOL)needPullUpRefresh {
    return YES;
}

///是否需要下拉刷新
- (BOOL)needPullDownRefresh {
    return YES;
}

#pragma mark - setup
///配置tableVeiw
- (void)configTableView:(UITableView *)tableView {
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellClass:[SLARechargeCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[SLARechargeCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
    
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [SLARechargeCell viewHeight];
}

@end
