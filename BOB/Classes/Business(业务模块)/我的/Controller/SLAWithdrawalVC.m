//
//  SLAWithdrawalVC.m
//  BOB
//
//  Created by mac on 2020/10/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SLAWithdrawalVC.h"
#import "SendPageInfoObj.h"
#import "YQPayKeyWordVC.h"
#import "NSDate+String.h"

@interface SLAWithdrawalVC ()<UITextFieldDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) SendPageInfoObj *obj;
///提币地址
@property (nonatomic, strong) UITextField *addressTF;
///提币数量
@property (nonatomic, strong) UITextField *numTF;
///手续费
@property (nonatomic, strong) UILabel *chargeLbl;
///实际到账
@property (nonatomic, strong) UILabel *actualLbl;

@property (nonatomic, strong) UILabel *commitLbl;

@end

@implementation SLAWithdrawalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)navBarRightBtnAction:(id)sender {
    MXRoute(@"UserSendLogListVC", @{@"title":@"提币记录"});
}

- (BOOL)valiParam {
    [self.view endEditing:YES];
    self.obj.address = [StringUtil trim:self.addressTF.text];
    if ([StringUtil isEmpty:self.obj.address]) {
        [NotifyHelper showMessageWithMakeText:@"提币地址不能为空"];
        return NO;
    }
    
    self.obj.number = [StringUtil trim:self.numTF.text];
    if (![StringUtil isNumberString:self.obj.number]) {
        [NotifyHelper showMessageWithMakeText:@"提币数量有误"];
        return NO;
    }
    if ([self.obj.number doubleValue] < self.obj.sendSlaMinNumber) {
        [NotifyHelper showMessageWithMakeText:@"提币数量不能少于最小提币数"];
        return NO;
    }
    if ([self.obj.number doubleValue] > self.obj.userInfo.sla_num) {
        [NotifyHelper showMessageWithMakeText:@"提币数量不能大于可提现数量"];
        return NO;
    }
    return YES;
}

- (void)textFieldTextChange {
    if (![StringUtil isNumberString:self.numTF.text]) {
        return;
    }
    double num = [self.numTF.text doubleValue];
    double sendSlaMinCahrgeNum = num * self.obj.sendSlaSingleCahrgeRate;
    if (sendSlaMinCahrgeNum < self.obj.sendSlaMinCahrgeNum) {
        sendSlaMinCahrgeNum = self.obj.sendSlaMinCahrgeNum;
    }
    self.chargeLbl.text = StrF(@"-%0.4f", sendSlaMinCahrgeNum);
    [self.chargeLbl sizeToFit];
    self.chargeLbl.mySize = self.chargeLbl.size;
    
    double diff = num - sendSlaMinCahrgeNum;
    self.actualLbl.text = StrF(@"%0.4f SLA", diff);
    [self.actualLbl sizeToFit];
    self.actualLbl.mySize = self.actualLbl.size;
}

- (void)commit {
    [[YQPayKeyWordVC alloc] showInViewController:[[MXRouter sharedInstance] getMallTopNavigationController]
                                            type:NormalPayType
                                        dataDict:@{@"title":@"支付密码"}
                                           block:^(NSString *password) {
        [NotifyHelper showHUDAddedTo:self.view animated:YES];
        [self request:@"api/wallet/send/userApplySend"
                param:@{@"timestamp":[NSDate getCurrentTimeTimestampByFormatter:@"YYYY-MM-dd HH:mm:ss"],
                        @"send_type":@"01",
                        @"address":self.obj.address?:@"",
                        @"num":self.obj.number?:@"",
                        @"pay_password":password?:@""}
           completion:^(BOOL success, id object, NSString *error) {
            if (success) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [NotifyHelper showMessageWithMakeText:error];
            }
        }];
    }];
}

- (void)fetchData {
    [self request:@"api/wallet/send/getSendPageInfo"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            self.obj = [SendPageInfoObj modelParseWithDict:object[@"data"]];
            self.tableView.tableHeaderView = [self headerView];
        }
    }];
}

- (void)createUI {
    [self setNavBarTitle:@"SLA提现"];
    [self setNavBarRightBtnWithTitle:@"提现记录" andImageName:nil];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    
    [layout addSubview:self.tableView];
}

- (MyBaseLayout *)headerView {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 10;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myHeight = 40;
    [layout setViewCornerRadius:5];
    layout.backgroundColor = [UIColor colorWithHexString:@"#F2F4F7"];
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#45454D"];
    lbl.font = [UIFont font15];
    lbl.text = @"可提现SLA";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 10;
    [layout addSubview:lbl];
    [layout addSubview:[UIView placeholderHorzView]];
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#45454D"];
    lbl.font = [UIFont font15];
    lbl.text = StrF(@"%0.4f", self.obj.userInfo.sla_num);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myRight = 10;
    [layout addSubview:lbl];
    [baseLayout addSubview:layout];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#45454D"];
    lbl.font = [UIFont font15];
    lbl.text = @"提币地址";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 20;
    lbl.myLeft = 20;
    [baseLayout addSubview:lbl];
    
    UITextField *tf = [UITextField new];
    tf.myLeft = 20;
    tf.myRight = 20;
    tf.myHeight = 30;
    tf.myTop = 15;
    
    NSArray *txtArr = @[@"请输入提币地址"];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#DADAE6"]];
    NSArray *fontArr = @[[UIFont font15]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    tf.attributedPlaceholder = att;
    tf.font = [UIFont font15];
#ifdef DEBUG
    tf.text = @"0x10884a73ef5975031b1404b371a082735b9fe644";
#endif
    self.addressTF = tf;
    [baseLayout addSubview:tf];
    
    [baseLayout addSubview:[self.class factoryLine]];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#45454D"];
    lbl.font = [UIFont font15];
    lbl.text = @"数量";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 20;
    lbl.myLeft = 20;
    [baseLayout addSubview:lbl];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myHeight = 40;
    
    tf = [UITextField new];
    tf.myLeft = 20;
    tf.myRight = 20;
    tf.myHeight = 30;
    tf.weight = 1;
    tf.myCenterY = 0;
    tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    
    txtArr = @[StrF(@"最小提币数量%0.4f", self.obj.sendSlaMinNumber)];
    colorArr = @[[UIColor colorWithHexString:@"#DADAE6"]];
    fontArr = @[[UIFont font15]];
    att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    tf.attributedPlaceholder = att;
    tf.font = [UIFont font15];
    [tf addTarget:self action:@selector(textFieldTextChange) forControlEvents:UIControlEventEditingChanged];
    self.numTF = tf;
    [subLayout addSubview:tf];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#45454D"];
    lbl.font = [UIFont boldFont14];
    lbl.text = @"SLA";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myRight = 20;
    [subLayout addSubview:lbl];
    [baseLayout addSubview:subLayout];
    [baseLayout addSubview:[self.class factoryLine]];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#45454D"];
    lbl.font = [UIFont font15];
    lbl.text = @"手续费";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 20;
    lbl.myLeft = 20;
    [baseLayout addSubview:lbl];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myHeight = 40;
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor moGreen];
    lbl.font = [UIFont boldFont15];
    lbl.text = @"-0.0000";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myRight = 20;
    lbl.myLeft = 20;
    lbl.myHeight = 20;
    self.chargeLbl = lbl;
    [subLayout addSubview:lbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#45454D"];
    lbl.font = [UIFont boldFont14];
    lbl.text = @"SLA";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myRight = 20;
    [subLayout addSubview:lbl];
    [baseLayout addSubview:subLayout];
    [baseLayout addSubview:[self.class factoryLine]];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myHeight = 40;
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#151419"];
    lbl.font = [UIFont font15];
    lbl.text = @"实际到账";
    lbl.myCenterY = 0;
    lbl.myLeft = 20;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    [subLayout addSubview:lbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#151419"];
    lbl.font = [UIFont boldFont15];
    lbl.text = @"0.000 SLA";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myRight = 20;
    self.actualLbl = lbl;
    [subLayout addSubview:lbl];
    [baseLayout addSubview:subLayout];
    
    [baseLayout addSubview:self.commitLbl];
    
    SPButton *btn = [SPButton new];
    btn.userInteractionEnabled = NO;
    btn.imagePosition = SPButtonImagePositionLeft;
    btn.imageTitleSpace = 10;
    [btn setImage:[UIImage imageNamed:@"温馨提示"] forState:UIControlStateNormal];
    [btn setTitle:@"温馨提示：" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont font12];
    [btn setTitleColor:[UIColor colorWithHexString:@"#FF4757"] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.mySize = btn.size;
    btn.myLeft = 20;
    [baseLayout addSubview:btn];
    
    lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.myLeft = 20;
    lbl.myTop = 10;
    CGFloat cahrgeRate = self.obj.sendSlaSingleCahrgeRate * 100;
    NSString *tip1 = StrF(@"1）手续费率：%0.0f%%；单笔最小手续费：%0.2f", cahrgeRate, self.obj.sendSlaMinCahrgeNum);
    NSString *tip2 = StrF(@"\n2）最小提币数量：%0.4f", self.obj.sendSlaMinNumber);
    NSString *tip3 = @"\n3）手续费提现额度内扣除，例如：申请提现 100，手续费1%，手续费 5.00，实际到账 95.00";
    txtArr = @[tip1, tip2, tip3];
    colorArr = @[[UIColor colorWithHexString:@"#AAAABB"], [UIColor colorWithHexString:@"#AAAABB"], [UIColor colorWithHexString:@"#AAAABB"]];
    fontArr = @[[UIFont font12], [UIFont font12], [UIFont font12]];
    att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 5; // 调整行间距
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 40, MAXFLOAT)];
    lbl.mySize = size;
    [baseLayout addSubview:lbl];
    
    baseLayout.myWidth = SCREEN_WIDTH;
    [baseLayout layoutIfNeeded];
    return baseLayout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.myBottom = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

+ (UIView *)factoryLine {
    UIView *line = [UIView line:[UIColor colorWithHexString:@"#E6E6E6"]];
    line.myLeft = 20;
    line.myRight = 20;
    line.myTop = 5;
    return line;
}

- (UILabel *)commitLbl {
    if (!_commitLbl) {
        _commitLbl = ({
            UILabel *object = [UILabel new];
            object.backgroundColor = [UIColor moGreen];
            object.font = [UIFont boldFont16];
            object.textAlignment = NSTextAlignmentCenter;
            object.textColor = [UIColor whiteColor];
            [object setViewCornerRadius:5];
            object.text = @"确认提现";
            object.myLeft = 20;
            object.myRight = 20;
            object.myHeight = 45;
            object.myTop = 50;
            object.myBottom = 20;
            @weakify(self)
            [object addAction:^(UIView *view) {
                @strongify(self)
                if ([self valiParam]) {
                    [self commit];
                }
            }];
            object;
        });
    }
    return _commitLbl;
}

@end
