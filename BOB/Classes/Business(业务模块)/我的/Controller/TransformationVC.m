//
//  TransformationVC.m
//  BOB
//
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "TransformationVC.h"
#import "YQPayKeyWordVC.h"

@interface TransformationVC ()

@property (nonatomic, strong) UITableView *tableView;
///充值数量
@property (nonatomic, strong) UITextField *tf;
///消耗积分
@property (nonatomic, strong) UILabel *consumeLbl;
///积分总额
@property (nonatomic, assign) double score_num;
///积分兑换余额最小数量
@property (nonatomic, assign) CGFloat scoreToBalanceMinNumber;
///积分兑换余额比例 1积分 = scoreToBalanceRate  余额
@property (nonatomic, assign) CGFloat scoreToBalanceRate;

@property (nonatomic, strong) UIButton *submitBtn;

@end

@implementation TransformationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)navBarRightBtnAction:(id)sender {
    MXRoute(@"TransListVC", @{@"title":@"转换记录"})
}

- (void)fetchData {
    [self request:@"api/wallet/exchange/getExchangePageInfo"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *userInfo = object[@"data"][@"userInfo"];
            if (userInfo[@"score_num"]) {
                self.score_num = [userInfo[@"score_num"] doubleValue];
            }
            NSDictionary *userExchangeInfo = object[@"data"][@"userExchangeInfo"];
            if (userExchangeInfo[@"scoreToBalanceMinNumber"]) {
                self.scoreToBalanceMinNumber = [userExchangeInfo[@"scoreToBalanceMinNumber"] floatValue];
            }
            if (userExchangeInfo[@"scoreToBalanceRate"]) {
                self.scoreToBalanceRate = [userExchangeInfo[@"scoreToBalanceRate"] floatValue];
            }
            [self initUI];
        }
    }];
}

- (void)changedTextField:(UITextField *)sender {
    if (sender == self.tf) {
        CGFloat num = [sender.text floatValue];
        CGFloat integral = num * self.scoreToBalanceRate;
        self.consumeLbl.text = StrF(@"%0.2f", integral);
        [self.consumeLbl sizeToFit];
        self.consumeLbl.mySize = self.consumeLbl.size;
    }
}

- (BOOL)valiPara {
    [self.tf resignFirstResponder];
    NSString *value = [StringUtil trim:self.tf.text];
    if ([StringUtil isEmpty:value]) {
        [NotifyHelper showMessageWithMakeText:@"请输入余额数量"];
        return NO;
    }
    if ([self.consumeLbl.text floatValue] < self.scoreToBalanceMinNumber) {
        [NotifyHelper showMessageWithMakeText:StrF(@"余额数量不能小于%0.0f", self.scoreToBalanceMinNumber)];
        return NO;
    }
    if ([self.consumeLbl.text floatValue] > self.score_num) {
        [NotifyHelper showMessageWithMakeText:@"消耗积分超过积分总额"];
        return NO;
    }
    return YES;
}

- (void)commit {
    [[YQPayKeyWordVC alloc] showInViewController:[[MXRouter sharedInstance] getTopNavigationController]
                                            type:NormalPayType
                                        dataDict:@{@"title":@"支付密码"}
                                           block:^(NSString * password) {
         [self request:@"/api/wallet/exchange/userApplyExchange"
                 param:@{@"exchange_type":@"01",
                         @"exchange_num":self.tf.text?:@"1",
                         @"pay_password":password
                 }
            completion:^(BOOL success, id object, NSString *error) {
             if (success) {
                 [self.navigationController popViewControllerAnimated:YES];
             } else {
                 [NotifyHelper showMessageWithMakeText:object[@"msg"]];
             }
         }];
    }];
}

- (void)createUI {
    [self setNavBarTitle:@"转换"];
    [self setNavBarRightBtnWithTitle:@"转换记录" andImageName:nil];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (void)initUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myWidth = SCREEN_WIDTH;
    baseLayout.myHeight = MyLayoutSize.wrap;
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [baseLayout addSubview:layout];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 0;
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myHeight = 40;
    subLayout.backgroundColor = [UIColor colorWithHexString:@"#F3F2F7"];
    [layout addSubview:subLayout];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"#45454D"];
    lbl.text = @"积分总额：";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 20;
    [subLayout addSubview:lbl];
    
    [subLayout addSubview:[UIView placeholderHorzView]];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"45454D"];
    lbl.text = StrF(@"%0.2f", self.score_num);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myRight = 20;
    [subLayout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor colorWithHexString:@"#5C5C5C"];
    lbl.text = @"余额数量";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 20;
    lbl.myLeft = 20;
    [layout addSubview:lbl];
    
    [layout addSubview:self.tf];
    UIView *line = [UIView line];
    line.myTop = 15;
    line.myLeft = 20;
    line.myRight = 20;
    [layout addSubview:line];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 0;
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myHeight = 40;
    [layout addSubview:subLayout];
    lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"#5C5C5C"];
    lbl.text = @"需消耗积分：";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 20;
    [subLayout addSubview:lbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"#EF0F00"];
    lbl.text = @"0.00";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myRight = 20;
    self.consumeLbl = lbl;
    [subLayout addSubview:lbl];
    
    [layout layoutSubviews];
    
    [baseLayout addSubview:self.submitBtn];
    [baseLayout layoutSubviews];
    self.tableView.tableHeaderView = baseLayout;
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = ({
            UITableView *object = [UITableView new];
            object.tableFooterView = [UIView new];
            object.backgroundColor = [UIColor moBackground];
            object;
        });
    }
    return _tableView;
}

- (UITextField *)tf {
    if (!_tf) {
        _tf = [UITextField new];
        _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
        _tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _tf.myLeft = 20;
        _tf.myRight = 20;
        _tf.myTop = 10;
        _tf.height = 30;
        _tf.myHeight = _tf.height;
        _tf.font = [UIFont font20];
        NSArray *txtArr = @[StrF(@"最低转换数量%@", @(self.scoreToBalanceMinNumber))];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#DBDBDB"]];
        NSArray *fontArr = @[[UIFont font20]];
        NSAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        _tf.attributedPlaceholder = att;
        [_tf addTarget:self action:@selector(changedTextField:) forControlEvents:UIControlEventEditingChanged];
    }
    return _tf;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"确认转换" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myLeft = 15;
            object.myRight = 15;
            object.myTop = 50;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                if ([self valiPara]) {
                    [self commit];
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

@end
