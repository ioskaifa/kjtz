//
//  InvitationVC.m
//  BOB
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "InvitationVC.h"
#import "UIImage+Utils.h"

@interface InvitationVC ()

@property (nonatomic, strong) UIButton *backBtn;

@property (nonatomic, strong) UIImageView *qrCode;

@end

@implementation InvitationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)createUI {
    UIImageView *bgImgView = [self bgImgView];
    [self.view addSubview:bgImgView];
    [bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"享超大福利"];
    imgView.myTop = 50 + safeAreaInset().top;
    imgView.myCenterX = 0;
    [layout addSubview:imgView];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    [subLayout setViewCornerRadius:5];
    subLayout.backgroundColor = [UIColor whiteColor];
    subLayout.myLeft = 40;
    subLayout.myRight = 40;
    subLayout.myTop = 30;
    subLayout.myHeight = MyLayoutSize.wrap;
    imgView = [UIImageView autoLayoutImgView:@"我的邀请码bg"];
    imgView.myCenterX = 0;
    [subLayout addSubview:imgView];
    self.qrCode.myTop = -(imgView.height/2.0);
    [subLayout addSubview:self.qrCode];
        
    UILabel *lbl = [UILabel new];
    lbl.size = CGSizeMake(180, 35);
    [lbl setViewCornerRadius:2];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor moGreen];
    lbl.font = [UIFont font24];
    lbl.backgroundColor = [UIColor blackColor];
    lbl.text = UDetail.user.user_uid;
    lbl.myCenterX = 0;
    lbl.myBottom = 10;
    lbl.myTop = 20;
    [subLayout addSubview:lbl];
    lbl = [UILabel new];
    lbl.font = [UIFont font16];
    lbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    lbl.text = @"我的邀请码";
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myTop = 5;
    lbl.myBottom = 10;
    [subLayout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont boldFont16];
    lbl.textColor = [UIColor whiteColor];
    lbl.backgroundColor = [UIColor moGreen];
    lbl.myLeft = 0;
    lbl.myRight = 0;
    lbl.myHeight = 50;
    lbl.text = @"复制邀请链接";
    [subLayout addSubview:lbl];
    [lbl addAction:^(UIView *view) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = UDetail.user.qr_code_url;
        [NotifyHelper showMessageWithMakeText:@"复制成功"];
    }];
    
    [layout addSubview:subLayout];
    
    [self.view addSubview:self.backBtn];
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = ({
            UIButton *object = [UIButton new];
            object.x = 17;
            object.y = safeAreaInset().top + 20;
            object.size = CGSizeMake(40, 40);
            [object setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                [self.navigationController popViewControllerAnimated:YES];
            }];
            object;
        });
    }
    return _backBtn;
}

- (UIImageView *)bgImgView {
    UIImageView *imgView = [UIImageView new];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"推广中心bg" ofType:@"png"];
    UIImage *img = [UIImage imageWithContentsOfFile:path];
    imgView.image = img;
    return imgView;
}

- (UIImageView *)qrCode {
    if (!_qrCode) {
        _qrCode = [UIImageView new];
        UIImage *qrImg = [UIImage encodeQRImageWithContent:UDetail.user.qr_code_url?:@"" size:CGSizeMake(180, 180)];
        _qrCode.image = qrImg;
        _qrCode.size = CGSizeMake(180, 180);
        _qrCode.mySize = _qrCode.size;
        _qrCode.myCenterX = 0;
        _qrCode.myTop = 50;
    }
    return _qrCode;
}

@end
