//
//  MyOrderSectionHeaderView.m
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyOrderSectionHeaderView.h"

@interface MyOrderSectionHeaderView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *statusLbl;

@end

@implementation MyOrderSectionHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 50;
}

- (void)configureView:(MyOrderListObj *)obj {
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, obj.photo);
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    self.titleLbl.text = obj.name;
    [self.titleLbl sizeToFit];
    self.titleLbl.mySize = self.titleLbl.size;
    
    if (obj.orderStatus == OrderStatusToPay ||
        obj.orderStatus == OrderStatusToDelive) {
        self.statusLbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
    } else if (obj.orderStatus == OrderStatusToComplete) {
        self.statusLbl.textColor = [UIColor moBlack];
    } else if (obj.orderStatus == OrderStatusToCancel) {
        self.statusLbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    } else {
        self.statusLbl.textColor = [UIColor moGreen];
    }
    self.statusLbl.text = obj.formatStatus;
    [self.statusLbl sizeToFit];
    self.statusLbl.mySize = self.statusLbl.size;
}

- (void)createUI {
    UIView *superView = self.contentView;
    superView.backgroundColor = [UIColor moBackground];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.size = CGSizeMake(SCREEN_WIDTH - 20, [self.class viewHeight] - 10);
    layout.backgroundColor = [UIColor whiteColor];
    [layout setBorderWithCornerRadius:5 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    layout.myTop = 10;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    [superView addSubview:layout];
    
    //[layout addSubview:self.imgView];
    [layout addSubview:self.titleLbl];
    [layout addSubview:[UIView placeholderHorzView]];
    [layout addSubview:self.statusLbl];
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            object.size = CGSizeMake(30, 30);
            [object setViewCornerRadius:object.height/2.0];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 10;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor moBlack];
            object.myCenterY = 0;
            object.myLeft = 5;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)statusLbl {
    if (!_statusLbl) {
        _statusLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font14];
            object.myCenterY = 0;
            object.myRight = 10;
            object;
        });
    }
    return _statusLbl;
}

@end
