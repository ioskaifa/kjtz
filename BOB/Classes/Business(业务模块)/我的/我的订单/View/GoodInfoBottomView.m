//
//  GoodInfoBottomView.m
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodInfoBottomView.h"
#import "UIImage+Color.h"
#import "AxcAE_TabBarBadge.h"

@interface GoodInfoBottomView ()


@end

@implementation GoodInfoBottomView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60 + safeAreaInsetBottom();
}
///购物车数量
- (void)configureView:(NSInteger)num {
    AxcAE_TabBarBadge *badge = [self.cartBtn viewWithTag:1];
    if (![badge isKindOfClass:AxcAE_TabBarBadge.class]) {
        return;;
    }
    [badge setBadgeText:StrF(@"%@", @(num))];
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = safeAreaInsetBottom();
    layout.gravity = MyGravity_Horz_Around;
    [layout addSubview:self.homeBtn];
    [layout addSubview:self.serverBtn];
    [layout addSubview:self.cartBtn];
    [layout addSubview:self.joinCartBtn];
    [layout addSubview:self.buyBtn];
    
    [self addSubview:layout];
}

- (SPButton *)homeBtn {
    if (!_homeBtn) {
        _homeBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionTop;
            object.imageTitleSpace = 5;
            object.titleLabel.font = [UIFont font12];
            object.titleLabel.textAlignment = NSTextAlignmentCenter;
            [object setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"商品详情_首页"] forState:UIControlStateNormal];
            [object setTitle:@"首页" forState:UIControlStateNormal];
            [object sizeToFit];
            CGSize size = object.size;
            object.size = CGSizeMake(size.width + 10, size.height);
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _homeBtn;
}

- (SPButton *)serverBtn {
    if (!_serverBtn) {
        _serverBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionTop;
            object.imageTitleSpace = 5;
            object.titleLabel.font = [UIFont font12];
            object.titleLabel.textAlignment = NSTextAlignmentCenter;
            [object setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"商品详情客服"] forState:UIControlStateNormal];
            [object setTitle:@"客服" forState:UIControlStateNormal];
            [object sizeToFit];
            CGSize size = object.size;
            object.size = CGSizeMake(size.width + 10, size.height);
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _serverBtn;
}

- (SPButton *)cartBtn {
    if (!_cartBtn) {
        _cartBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionTop;
            object.imageTitleSpace = 5;
            object.titleLabel.font = [UIFont font12];
            object.titleLabel.textAlignment = NSTextAlignmentCenter;
            [object setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"商品详情购物车"] forState:UIControlStateNormal];
            [object setTitle:@"购物车" forState:UIControlStateNormal];
            [object sizeToFit];
            CGSize size = object.size;
            object.size = CGSizeMake(size.width + 20, size.height);
            object.mySize = object.size;
            AxcAE_TabBarBadge *badge = [AxcAE_TabBarBadge new];
            badge.tag = 1;
            badge.automaticHidden = YES;
            badge.frame = CGRectMake(object.width - 20, -5, badge.width, badge.height);
            [object addSubview:badge];
            object.myCenterY = 0;
            object;
        });
    }
    return _cartBtn;
}

- (UIButton *)joinCartBtn {
    if (!_joinCartBtn) {
        _joinCartBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(105, 42);
            [object setViewCornerRadius:5];
            object.titleLabel.font = [UIFont boldFont15];
            object.layer.borderColor = [UIColor colorWithHexString:@"#DADAE6"].CGColor;
            object.layer.borderWidth = 1;
            object.titleLabel.textAlignment = NSTextAlignmentCenter;
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setTitleColor:[UIColor colorWithHexString:@"#737380"] forState:UIControlStateNormal];;
            [object setTitle:@"加入购物车" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _joinCartBtn;
}

- (UIButton *)buyBtn {
    if (!_buyBtn) {
        _buyBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(105, 42);
            [object setViewCornerRadius:5];
            object.titleLabel.font = [UIFont boldFont15];
            object.titleLabel.textAlignment = NSTextAlignmentCenter;
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setBackgroundColor:[UIColor moGreen]];
            [object setTitle:@"立即购买" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _buyBtn;
}

@end
