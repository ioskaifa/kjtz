//
//  ConfirmOrderBottomView.m
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ConfirmOrderBottomView.h"
#import "UIImage+Color.h"

@interface ConfirmOrderBottomView ()

@property (nonatomic, strong) UILabel *lbl;
///立即付款
@property (nonatomic, strong) UIButton *payBtn;

@end

@implementation ConfirmOrderBottomView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60 + safeAreaInsetBottom();
}

- (void)configureView:(CGFloat)totalNum discontNum:(CGFloat)discontNum {
    self.lbl.size = CGSizeZero;
    self.lbl.mySize = self.lbl.size;
//    NSString *discount = StrF(@"会员折扣：无");
//    if (discontNum > 0) {
//        discount = StrF(@"  会员折扣：-￥%0.2f", discontNum);
//    }
    totalNum = totalNum - discontNum;
    NSArray *txtArr = @[@"合计：",
                        StrF(@"￥%0.2f", totalNum),
                        @"\n(含运费）"];
    NSArray *colorArr = @[[UIColor moBlack],
                          [UIColor blackColor],
                          [UIColor colorWithHexString:@"#A8ACB3"]];
    NSArray *fontArr = @[[UIFont font15],
                         [UIFont boldFont18],
                         [UIFont font11]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                        colors:colorArr
                                                                         fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 5; // 调整行间距
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    self.lbl.attributedText = att;
    [self.lbl sizeToFit];
    self.lbl.mySize = self.lbl.size;
}

- (void)configurePayBtnState:(BOOL)entable {
    if (entable) {
        self.payBtn.backgroundColor = [UIColor moGreen];
    } else {
        self.payBtn.backgroundColor = [UIColor colorWithHexString:@"#D0D0DB"];
    }
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = safeAreaInsetBottom();
    [self addSubview:layout];
    
    [layout addSubview:self.lbl];
    [layout addSubview:[UIView placeholderHorzView]];
    [layout addSubview:self.payBtn];
    [self addTopSingleLine:[UIColor moBackground]];
}

- (UILabel *)lbl {
    if (!_lbl) {
        _lbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 0;
            object.myCenterY = 0;
            object.myLeft = 10;
            object;
        });
    }
    return _lbl;
}

- (UIButton *)payBtn {
    if (!_payBtn) {
        _payBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(116, 42);
            [object setViewCornerRadius:5];
            [object setBackgroundColor:[UIColor moGreen]];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont15];
            [object setTitle:@"立即付款" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [btn.nextResponder routerEventWithName:@"ConfirmOrderBottomViewPay" userInfo:nil];
            }];
            object;
        });
    }
    return _payBtn;
}

@end
