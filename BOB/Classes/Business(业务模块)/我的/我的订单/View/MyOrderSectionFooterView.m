//
//  MyOrderSectionFooterView.m
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyOrderSectionFooterView.h"

@interface MyOrderSectionFooterView ()
///会员折扣
@property (nonatomic, strong) UILabel *discountLbl;

@property (nonatomic, strong) UILabel *priceLbl;
///取消订单
@property (nonatomic, strong) UIButton *cancelBtn;
///去支付
@property (nonatomic, strong) UIButton *payBtn;
///确认收货
@property (nonatomic, strong) UIButton *receiveBtn;
///删除记录
@property (nonatomic, strong) UIButton *delBtn;
///再次购买
@property (nonatomic, strong) UIButton *buyBtn;

@property (nonatomic, assign) NSInteger section;

@end

@implementation MyOrderSectionFooterView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 70;
}

- (void)configureView:(MyOrderListObj *)obj section:(NSInteger)section {
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    self.section = section;
    NSString *discount = StrF(@"-￥%0.2f", obj.discount_cash_num);
    UIColor *color = [UIColor moRed];
    if (obj.discount_cash_num == 0) {
        discount = @"无";
        color = [UIColor colorWithHexString:@"#A8A8A8"];
    }
    NSArray *txtArr = @[@"会员折扣：", discount];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#A8A8A8"], color];
    NSArray *fontArr = @[[UIFont font13], [UIFont font13]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    self.discountLbl.attributedText = att;
    [self.discountLbl sizeToFit];
    self.discountLbl.mySize = self.discountLbl.size;
    self.discountLbl.visibility = MyVisibility_Invisible;
    txtArr = @[StrF(@"共%ld件商品，合计：", (long)obj.sumNumber), StrF(@"￥%0.2f", obj.total_cash_num)];
    colorArr = @[[UIColor colorWithHexString:@"#A8A8A8"], [UIColor moBlack]];
    fontArr = @[[UIFont font13], [UIFont boldFont15]];
    att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    self.priceLbl.attributedText = att;
    [self.priceLbl sizeToFit];
    self.priceLbl.mySize = self.priceLbl.size;
    
    self.cancelBtn.visibility = MyVisibility_Gone;
    self.payBtn.visibility = MyVisibility_Gone;
    self.receiveBtn.visibility = MyVisibility_Gone;
    self.delBtn.visibility = MyVisibility_Gone;
    self.buyBtn.visibility = MyVisibility_Gone;
        
    switch (obj.orderStatus) {
        case OrderStatusToPay: {
            self.cancelBtn.visibility = MyVisibility_Visible;
            self.payBtn.visibility = MyVisibility_Visible;
            break;
        }
        case OrderStatusToDelive: {            
            break;
        }
        case OrderStatusToReceive: {
            self.receiveBtn.visibility = MyVisibility_Visible;
            break;
        }
        case OrderStatusToComplete: {            
            break;
        }
        case OrderStatusToCancel: {
            self.delBtn.visibility = MyVisibility_Visible;
            break;
        }
        default:
            break;
    }
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    layout.size = CGSizeMake(SCREEN_WIDTH - 20, [self.class viewHeight]);
    layout.backgroundColor = [UIColor whiteColor];
    [layout setBorderWithCornerRadius:5 byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight];
    [self.contentView addSubview:layout];
    self.contentView.backgroundColor = [UIColor moBackground];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myTop = 5;
    subLayout.myHeight = MyLayoutSize.wrap;
    [subLayout addSubview:self.discountLbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    [subLayout addSubview:self.priceLbl];
    [layout addSubview:subLayout];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myTop = 5;
    subLayout.weight = 1;
    [subLayout addSubview:[UIView placeholderHorzView]];
    [subLayout addSubview:self.cancelBtn];
    [subLayout addSubview:self.payBtn];
    [subLayout addSubview:self.receiveBtn];
    [subLayout addSubview:self.delBtn];
    [subLayout addSubview:self.buyBtn];
    [layout addSubview:subLayout];
}

#pragma mark - Init
- (UILabel *)discountLbl {
    if (!_discountLbl) {
        _discountLbl = ({
            UILabel *object = [UILabel new];
            object.myLeft = 10;
            object.myCenterY = 0;
            object;
        });
    }
    return _discountLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = ({
            UILabel *object = [UILabel new];
            object.myRight = 10;
            object.myCenterY = 0;
            object;
        });
    }
    return _priceLbl;
}

- (UIButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(85, 26);
            [object setViewCornerRadius:2];
            [object setTitleColor:[UIColor colorWithHexString:@"#46474D"] forState:UIControlStateNormal];
            object.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
            object.layer.borderWidth = 1;
            [object setTitle:@"取消订单" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myTop = 5;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [btn.nextResponder routerEventWithName:@"MyOrderSectionFooterViewCancel" userInfo:@{@"section":@(self.section)}];
            }];
            object;
        });
    }
    return _cancelBtn;
}

- (UIButton *)payBtn {
    if (!_payBtn) {
        _payBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(70, 26);
            [object setViewCornerRadius:2];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            [object setTitle:@"去支付" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myTop = 5;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [btn.nextResponder routerEventWithName:@"MyOrderSectionFooterViewPay" userInfo:@{@"section":@(self.section)}];
            }];
            object;
        });
    }
    return _payBtn;
}

- (UIButton *)receiveBtn {
    if (!_receiveBtn) {
        _receiveBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(85, 26);
            [object setViewCornerRadius:2];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            [object setTitle:@"确认收货" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myTop = 5;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [btn.nextResponder routerEventWithName:@"MyOrderSectionFooterViewReceive" userInfo:@{@"section":@(self.section)}];
            }];
            object;
        });
    }
    return _receiveBtn;
}

- (UIButton *)delBtn {
    if (!_delBtn) {
        _delBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(85, 26);
            [object setViewCornerRadius:2];
            [object setTitleColor:[UIColor colorWithHexString:@"#AAAABB"] forState:UIControlStateNormal];
            object.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
            object.layer.borderWidth = 1;
            [object setTitle:@"删除记录" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myTop = 5;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [btn.nextResponder routerEventWithName:@"MyOrderSectionFooterViewDel" userInfo:@{@"section":@(self.section)}];
            }];
            object;
        });
    }
    return _delBtn;
}

- (UIButton *)buyBtn {
    if (!_buyBtn) {
        _buyBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(85, 26);
            [object setViewCornerRadius:object.height/2.0];
            [object setTitleColor:[UIColor colorWithHexString:@"#6B6B6B"] forState:UIControlStateNormal];
            object.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
            object.layer.borderWidth = 1;
            [object setTitle:@"再次购买" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myTop = 5;
            object.myRight = 10;
            object;
        });
    }
    return _buyBtn;
}

@end
