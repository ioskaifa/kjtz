//
//  GoodInfoHeaderView.m
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodInfoHeaderView.h"
#import "GKCycleScrollView.h"
#import "GoodInfoHeaderSubView.h"
#import "GoodsCollectionAPI.h"

@interface GoodInfoHeaderView ()<GKCycleScrollViewDataSource, GKCycleScrollViewDelegate>

@property (nonatomic, strong) MyBaseLayout *baseLayout;

///轮播图
@property (nonatomic, strong) GKCycleScrollView *cycleScrollView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UIPageControl *pageControl;
///发货地址
@property (nonatomic, strong) GoodInfoHeaderSubView *addressView;
///邮费
@property (nonatomic, strong) GoodInfoHeaderSubView *expressView;

@property (nonatomic, strong) GoodsInfoObj *goodObj;

@property (nonatomic, strong) UILabel *stockLbl;
///收藏按钮
@property (nonatomic, strong) UIButton *collectionBtn;

@end

@implementation GoodInfoHeaderView

- (instancetype)initWithGoodsInfoObj:(GoodsInfoObj *)obj {
    if (self = [super init]) {
        self.goodObj = obj;
        [self createUI];
    }
    return self;
}

- (CGFloat)viewHeight {
    [self.baseLayout layoutIfNeeded];
    return self.baseLayout.height;
}

- (void)configureView:(GoodsInfoObj *)obj {
    if (![obj isKindOfClass:GoodsInfoObj.class]) {
        return;
    }
    [self.dataSourceMArr removeAllObjects];
    [self.dataSourceMArr addObjectsFromArray:obj.goods_navigationArr];
    [self.cycleScrollView reloadData];
}

- (void)configureStock {
    self.stockLbl.text = StrF(@"库存 %ld", self.goodObj.goods_stock_num);
}

#pragma mark - Delegate
#pragma mark GKCycleScrollViewDataSource
- (NSInteger)numberOfCellsInCycleScrollView:(GKCycleScrollView *)cycleScrollView {
    return self.dataSourceMArr.count;
}

- (GKCycleScrollViewCell *)cycleScrollView:(GKCycleScrollView *)cycleScrollView cellForViewAtIndex:(NSInteger)index {
    GKCycleScrollViewCell *cell = [cycleScrollView dequeueReusableCell];
    if (!cell) {
        cell = [GKCycleScrollViewCell new];
    }
    if (index < self.dataSourceMArr.count) {
        NSString *img_url = self.dataSourceMArr[index];
//        img_url = StrF(@"%@/%@", UDetail.user.qiniu_domain, img_url);
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
        }];
    }
    cell.imageView.contentMode = UIViewContentModeScaleToFill;
    return cell;
}

- (void)cycleScrollView:(GKCycleScrollView *)cycleScrollView didSelectCellAtCell:(GKCycleScrollViewCell *)cell
                atIndex:(NSInteger)index {
    if (index >= self.dataSourceMArr.count) {
        return;
    }
}

- (void)collectionBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    sender.userInteractionEnabled = NO;
    [GoodsCollectionAPI createUserGoodsCollect:self.goodObj.ID oper_type:sender.selected completion:^(BOOL success, id object, NSString *error) {
        sender.userInteractionEnabled = YES;
    }];
}

- (void)getUserGoodsCollectInfo {
    [GoodsCollectionAPI getUserGoodsCollectInfoByGoodsId:self.goodObj.ID completion:^(BOOL success, id object, NSString *error) {
        if (object[@"data"][@"shopCollectInfo"]) {
            self.collectionBtn.selected = YES;
        } else {
            self.collectionBtn.selected = NO;
        }
    }];
}

- (void)createUI {
    [self addSubview:self.baseLayout];
    [self.baseLayout addSubview:self.cycleScrollView];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.myTop = 10;
    subLayout.myLeft = 15;
    subLayout.myRight = 15;
    subLayout.myBottom = 10;
    subLayout.myHeight = MyLayoutSize.wrap;
    [self.baseLayout addSubview:subLayout];
    
    MyLinearLayout *disLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    disLayout.myLeft = 0;
    disLayout.myRight = 0;
    disLayout.myHeight = MyLayoutSize.wrap;
    disLayout.myBottom = 5;
    [subLayout addSubview:disLayout];
    CGFloat disPrice = self.goodObj.cash_min;
    //等级大于0 显示折扣价
//    if (UDetail.user.walletInfoObj.user_grade > 0) {
//        disPrice = self.goodObj.cash_min * UDetail.user.walletInfoObj.mall_discount_rate;
//    }
    NSString *disPriceString = StrF(@"%0.2f", disPrice);
    NSArray *tempArr = [disPriceString componentsSeparatedByString:@"."];
    NSString *integer = tempArr.firstObject;
    NSString *decimal = StrF(@".%@", tempArr.lastObject);
    NSArray *txtArr = @[@"￥", integer, decimal];
    NSArray *colorArr = @[[UIColor blackColor], [UIColor blackColor], [UIColor blackColor]];
    NSArray *fontArr = @[[UIFont boldFont15], [UIFont boldSystemFontOfSize:30], [UIFont boldFont15]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    UILabel *lbl = [UILabel new];
    lbl.attributedText = att;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myRight = 15;
    [disLayout addSubview:lbl];
    
    if (self.goodObj.only_sla_pay) {
        lbl = [UILabel new];
        lbl.size = CGSizeMake(70, 15);
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = @"仅支持SLA支付";
        lbl.font = [UIFont systemFontOfSize:8];
        lbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
        lbl.layer.borderWidth = 0.5;
        lbl.layer.borderColor = [UIColor colorWithHexString:@"#FF4757"].CGColor;
        lbl.mySize = lbl.size;
        lbl.myLeft = 0;
        lbl.myCenterY = 0;
        [disLayout addSubview:lbl];
    }
    
    [disLayout addSubview:[UIView placeholderHorzView]];
    SPButton *btn = [SPButton new];
    btn.imageTitleSpace = 10;
    btn.imagePosition = SPButtonImagePositionTop;
    btn.titleLabel.font = [UIFont font13];
    [btn setTitle:@"未收藏" forState:UIControlStateNormal];
    [btn setTitle:@"已收藏" forState:UIControlStateSelected];
    
    [btn setImage:[UIImage imageNamed:@"未收藏"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"已收藏"] forState:UIControlStateSelected];
    [btn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateSelected];
    self.collectionBtn = btn;
    if (!([@"01" isEqualToString:self.goodObj.mall_type] || [@"02" isEqualToString:self.goodObj.mall_type])) {
        btn.visibility = MyVisibility_Invisible;
    }
    @weakify(self)
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self collectionBtnClick:btn];
    }];
    [btn sizeToFit];
    btn.size = CGSizeMake(btn.width + 25, btn.height);
    btn.mySize = btn.size;
    btn.myCenterY = 0;
    btn.myRight = 0;
    [disLayout addSubview:btn];
    
    //显示折扣价 需要显示市场价
    if (self.goodObj.underline_cash > 0.0) {
        lbl = [UILabel new];
        txtArr = @[StrF(@"￥%0.2f", self.goodObj.underline_cash)];
        colorArr = @[[UIColor colorWithHexString:@"#A8A8A8"]];
        fontArr = @[[UIFont font12]];
        att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        //中划线
        NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSRange range = [att.string rangeOfString:att.string];
        [att addAttributes:attribtDic range:range];
        lbl.attributedText = att;
        [lbl sizeToFit];
        lbl.mySize = lbl.size;
        [subLayout addSubview:lbl];
    }
    
//    if (UDetail.user.walletInfoObj.user_grade > 0) {
//        lbl = [UILabel new];
//        lbl.textAlignment = NSTextAlignmentCenter;
//        lbl.size = CGSizeMake(46, 20);
//        [lbl setViewCornerRadius:3];
//        lbl.mySize = lbl.size;
//        lbl.font = [UIFont font12];
//        lbl.textColor = [UIColor colorWithHexString:@"#FF293B"];
//        lbl.text = @"折扣价";
//        lbl.backgroundColor = [UIColor colorWithHexString:@"#F9E5E6"];
//        lbl.myCenterY = 0;
//        [disLayout addSubview:lbl];
//
//    }
    
    lbl = [UILabel new];
    lbl.font = [UIFont font16];
    lbl.textColor = [UIColor colorWithHexString:@"#151419"];
    lbl.numberOfLines = 0;
    NSString *name = self.goodObj.goods_name;
    if (self.goodObj.isFree) {
        name = StrF(@"1       %@", name);
    }
    lbl.text = name;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    lbl.myLeft = 15;
    lbl.myRight = 15;
    lbl.myBottom = 10;
    if (self.goodObj.isFree) {
        UILabel *tipLbl = [UILabel new];
        tipLbl.size = CGSizeMake(34, 18);
        tipLbl.text = @"免单";
        [tipLbl setViewCornerRadius:1];
        tipLbl.backgroundColor = [UIColor colorWithHexString:@"#D3F5F2"];
        tipLbl.textColor = [UIColor colorWithHexString:@"#00A99B"];
        tipLbl.font = [UIFont font11];
        tipLbl.textAlignment = NSTextAlignmentCenter;
        [lbl addSubview:tipLbl];
        [tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(tipLbl.size);
            make.left.mas_equalTo(tipLbl.mas_left);
            make.top.mas_equalTo(tipLbl.mas_top);
        }];
    }
    [self.baseLayout addSubview:lbl];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 15;
    subLayout.myRight = 15;
    subLayout.myHeight = MyLayoutSize.wrap;
    subLayout.myBottom = 10;
    [self.baseLayout addSubview:subLayout];
    lbl = [UILabel new];
    lbl.font = [UIFont font12];
    lbl.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
    lbl.text = StrF(@"已售 %ld", (long)self.goodObj.goods_sales_num);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 0;
    lbl.myCenterY = 0;
    [subLayout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font12];
    lbl.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
    lbl.text = StrF(@"库存 %ld", (long)self.goodObj.goods_stock_num);
    self.stockLbl = lbl;
    lbl.weight = 1;
    lbl.textAlignment = NSTextAlignmentRight;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myRight = 0;
    lbl.myCenterY = 0;
    [subLayout addSubview:lbl];
    
    [self.baseLayout addSubview:[UIView gapLine]];
    [self.baseLayout addSubview:self.specView];
    if (self.goodObj.isSelfSupport && !self.goodObj.isLive) {
        [self.baseLayout addSubview:self.addressView];
    }
    if (self.goodObj.isSelfSupport) {
        [self.baseLayout addSubview:self.expressView];
    }
    
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = 45;
    layout.backgroundColor = [UIColor moBackground];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"商品详情";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 15;
    lbl.myCenterY = 0;
    [layout addSubview:lbl];
    
    [self.baseLayout addSubview:layout];
    [self configureView:self.goodObj];
    [self getUserGoodsCollectInfo];
}

#pragma mark - Init
- (MyBaseLayout *)baseLayout {
    if (!_baseLayout) {
        _baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _baseLayout.myTop = 0;
        _baseLayout.myLeft = 0;
        _baseLayout.myRight = 0;
        _baseLayout.myHeight = MyLayoutSize.wrap;
        _baseLayout.backgroundColor = [UIColor whiteColor];
    }
    return _baseLayout;
}

#pragma mark - 轮播图
- (GKCycleScrollView *)cycleScrollView {
    if (!_cycleScrollView) {
        _cycleScrollView = [[GKCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 350)];
        _cycleScrollView.pageControl = self.pageControl;
        _cycleScrollView.isAutoScroll = YES;
        _cycleScrollView.dataSource = self;
        _cycleScrollView.delegate = self;
        _cycleScrollView.backgroundColor = [UIColor moBackground];
        _cycleScrollView.myTop = 0;
        _cycleScrollView.myLeft = 0;
        _cycleScrollView.myRight = 0;
        _cycleScrollView.myBottom = 0;
        _cycleScrollView.myHeight = _cycleScrollView.height;
        _cycleScrollView.isAutoScroll = NO;
        [_cycleScrollView addSubview:self.pageControl];
        _cycleScrollView.backgroundColor = [UIColor moBackground];
    }
    return _cycleScrollView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [UIPageControl new];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.numberOfPages = self.dataSourceMArr.count;
        _pageControl.frame = CGRectMake(20, self.cycleScrollView.height - 20, SCREEN_WIDTH - 40 - 30, 20);
    }
    return _pageControl;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (GoodInfoHeaderSubView *)specView {
    if (!_specView) {
        NSArray *txtArr = @[@"选择规格"];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#5C5C66"]];
        NSArray *fontArr = @[[UIFont font13]];
        NSMutableAttributedString *leftAtt = [NSMutableAttributedString initWithTitles:txtArr
                                                                                colors:colorArr
                                                                                 fonts:fontArr];
        txtArr = @[@"请选择规格"];
        colorArr = @[[UIColor blackColor]];
        fontArr = @[[UIFont font13]];
        NSMutableAttributedString *rightAtt = [NSMutableAttributedString initWithTitles:txtArr
                                                                                 colors:colorArr
                                                                                  fonts:fontArr];
        _specView = [[GoodInfoHeaderSubView alloc] initWithLeftAtt:leftAtt
                                                         centerAtt:rightAtt
                                                          rightImg:@"Arrow"];
        _specView.myTop = 0;
        _specView.myLeft = 0;
        _specView.myRight = 0;
        _specView.myHeight = [_specView viewHeight];
    }
    return _specView;
}

- (GoodInfoHeaderSubView *)addressView {
    if (!_addressView) {
        NSArray *txtArr = @[@"发货地址"];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#5C5C66"]];
        NSArray *fontArr = @[[UIFont font13]];
        NSMutableAttributedString *leftAtt = [NSMutableAttributedString initWithTitles:txtArr
                                                                                colors:colorArr
                                                                                 fonts:fontArr];
        txtArr = @[self.goodObj.send_address];
        colorArr = @[[UIColor blackColor]];
        fontArr = @[[UIFont font13]];
        NSMutableAttributedString *rightAtt = [NSMutableAttributedString initWithTitles:txtArr
                                                                                 colors:colorArr
                                                                                  fonts:fontArr];
        _addressView = [[GoodInfoHeaderSubView alloc] initWithLeftAtt:leftAtt
                                                            centerAtt:rightAtt
                                                             rightImg:@""];
        _addressView.myTop = 0;
        _addressView.myLeft = 0;
        _addressView.myRight = 0;
        _addressView.myHeight = [_addressView viewHeight];
    }
    return _addressView;
}

- (GoodInfoHeaderSubView *)expressView {
    if (!_expressView) {
        NSArray *txtArr = @[@"物流运费"];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#5C5C66"]];
        NSArray *fontArr = @[[UIFont font13]];
        NSMutableAttributedString *leftAtt = [NSMutableAttributedString initWithTitles:txtArr
                                                                                colors:colorArr
                                                                                 fonts:fontArr];
        NSString *txt = @"免运费";
        if (self.goodObj.express_cash != 0) {
            txt = StrF(@"￥%0.2f", self.goodObj.express_cash);
        }
        txtArr = @[txt];
        colorArr = @[[UIColor blackColor]];
        fontArr = @[[UIFont font13]];
        NSMutableAttributedString *rightAtt = [NSMutableAttributedString initWithTitles:txtArr
                                                                                 colors:colorArr
                                                                                  fonts:fontArr];
        _expressView = [[GoodInfoHeaderSubView alloc] initWithLeftAtt:leftAtt
                                                            centerAtt:rightAtt
                                                             rightImg:@""];
        _expressView.myTop = 0;
        _expressView.myLeft = 0;
        _expressView.myRight = 0;
        _expressView.myHeight = [_expressView viewHeight];
    }
    return _expressView;
}

@end
