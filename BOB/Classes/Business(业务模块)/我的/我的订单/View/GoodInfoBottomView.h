//
//  GoodInfoBottomView.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodInfoBottomView : UIView

///首页
@property (nonatomic, strong) SPButton *homeBtn;
///客服
@property (nonatomic, strong) SPButton *serverBtn;
///购物车
@property (nonatomic, strong) SPButton *cartBtn;
///加入购物车
@property (nonatomic, strong) UIButton *joinCartBtn;
///立即购买
@property (nonatomic, strong) UIButton *buyBtn;

+ (CGFloat)viewHeight;
///购物车数量
- (void)configureView:(NSInteger)num;

@end

NS_ASSUME_NONNULL_END
