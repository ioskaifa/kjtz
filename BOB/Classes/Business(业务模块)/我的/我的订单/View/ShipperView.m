//
//  ShipperView.m
//  BOB
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ShipperView.h"

@interface ShipperView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *IDLbl;

@property (nonatomic, strong) UILabel *scopyLbl;

@property (nonatomic, strong) UIImageView *arrowImgView;

@end

@implementation ShipperView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 95;
}

- (void)configureView:(TracesInfoObj *)obj {
    if (!([obj isKindOfClass:TracesInfoObj.class] || [obj isKindOfClass:TracesOtherInfoObj.class])) {
        return;
    }
    
    self.nameLbl.text = obj.AcceptStation;
    CGSize size = [self.nameLbl sizeThatFits:CGSizeMake(self.nameLbl.width, MAXFLOAT)];
    self.nameLbl.size = size;
    self.nameLbl.mySize = self.nameLbl.size;
    
    self.IDLbl.text = obj.AcceptTime;
    [self.IDLbl autoMyLayoutSize];
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    layout.backgroundColor = [UIColor whiteColor];
    [self addSubview:layout];
    
    [layout setViewCornerRadius:10];
    [layout addSubview:self.imgView];
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];;
    subLayout.myLeft = 10;
    subLayout.myRight = 10;
    subLayout.myCenterY = 0;
    subLayout.weight = 1;
    subLayout.myHeight = MyLayoutSize.wrap;
    [layout addSubview:subLayout];
    [subLayout addSubview:self.nameLbl];
    [subLayout addSubview:self.IDLbl];
    [layout addSubview:self.arrowImgView];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"物流"];
            object.myCenterY = 0;
            object.myLeft = 15;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.numberOfLines = 2;
            object.textColor = [UIColor moGreen];
            object.width = SCREEN_WIDTH - 70 - self.imgView.width - self.arrowImgView.width;
            object.myWidth = object.width;
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)IDLbl {
    if (!_IDLbl) {
        _IDLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont lightFont13];
            object.textColor = [UIColor colorWithHexString:@"#737380"];
            object.myTop = 10;
            object;
        });
    }
    return _IDLbl;
}

- (UILabel *)scopyLbl {
    if (!_scopyLbl) {
        _scopyLbl = ({
            UILabel *object = [UILabel new];
            object.textAlignment = NSTextAlignmentCenter;
            object.size = CGSizeMake(44, 22);
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myRight = 15;
            [object setViewCornerRadius:object.height/2.0];
            object.font = [UIFont font13];
            object.textColor = [UIColor moGreen];
            object.backgroundColor = [UIColor whiteColor];
            object.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
            object.layer.borderWidth = 1;
            object.text = @"复制";
            @weakify(self)
            [object addAction:^(UIView *view) {
                @strongify(self)
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = self.IDLbl.text;
                [NotifyHelper showMessageWithMakeText:@"复制成功"];
            }];
            object;
        });
    }
    return _scopyLbl;
}

- (UIImageView *)arrowImgView {
    if (!_arrowImgView) {
        _arrowImgView = [UIImageView autoLayoutImgView:@"Arrow"];
        _arrowImgView.myCenterY = 0;
        _arrowImgView.myRight = 15;
    }
    return _arrowImgView;
}

@end
