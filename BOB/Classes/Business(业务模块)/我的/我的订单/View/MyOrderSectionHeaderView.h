//
//  MyOrderSectionHeaderView.h
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyOrderListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyOrderSectionHeaderView : UITableViewHeaderFooterView

+ (CGFloat)viewHeight;

- (void)configureView:(MyOrderListObj *)obj;

@end

NS_ASSUME_NONNULL_END
