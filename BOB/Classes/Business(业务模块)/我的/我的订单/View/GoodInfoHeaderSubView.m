//
//  GoodInfoHeaderSubView.m
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodInfoHeaderSubView.h"

@interface GoodInfoHeaderSubView ()

@property (nonatomic, strong) MyBaseLayout *baseLayout;

@property (nonatomic, strong) UILabel *leftLbl;

@property (nonatomic, strong) UILabel *centerLbl;

@property (nonatomic, strong) UIImageView *rightImgView;

@property (nonatomic, copy) NSAttributedString *leftAtt;

@property (nonatomic, copy) NSAttributedString *centerAtt;

@property (nonatomic, copy) NSString *rightImg;

@end

@implementation GoodInfoHeaderSubView

- (instancetype)initWithLeftAtt:(NSAttributedString *)leftAtt
                      centerAtt:(NSAttributedString *)centerAtt
                       rightImg:(NSString *)rightImg {
    self = [super init];
    if (self) {
        self.leftAtt = leftAtt;
        self.centerAtt = centerAtt;
        self.rightImg = rightImg;
        [self createUI];
    }
    return self;
}

- (CGFloat)viewHeight {
    [self.baseLayout layoutIfNeeded];
    return self.baseLayout.height;
}

- (void)configureCenterAtt:(NSAttributedString *)att {
    if (![att isKindOfClass:NSAttributedString.class]) {
        return;
    }
    self.centerLbl.attributedText = att;
    CGFloat width = SCREEN_WIDTH - self.leftLbl.width - self.rightImgView.width - 60;
    CGSize size = [self.centerLbl sizeThatFits:CGSizeMake(width, MAXFRAG)];
    self.centerLbl.mySize = size;    
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self addSubview:layout];
    [layout addSubview:self.leftLbl];
    [layout addSubview:self.centerLbl];
    [layout addSubview:self.rightImgView];
    if (CGSizeEqualToSize(self.rightImgView.size, CGSizeZero)) {
        self.rightImgView.visibility = MyVisibility_Gone;
    }
    CGFloat width = SCREEN_WIDTH - self.leftLbl.width - self.rightImgView.width - 60;
    CGSize size = [self.centerLbl sizeThatFits:CGSizeMake(width, MAXFRAG)];
    self.centerLbl.mySize = size;
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor moBackground];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(SINGLE_LINE_HEIGHT);
        make.left.mas_equalTo(self.centerLbl);
        make.right.mas_equalTo(self.mas_right).mas_offset(-15);
        make.bottom.mas_equalTo(self);
    }];
    self.baseLayout = layout;
}

- (UILabel *)leftLbl {
    if (!_leftLbl) {
        _leftLbl = ({
            UILabel *object = [UILabel new];
            object.myTop = 15;
            object.myLeft = 15;
            object.attributedText = self.leftAtt;
            [object sizeToFit];
            object.mySize = object.size;
            object;
        });
    }
    return _leftLbl;
}

- (UIImageView *)rightImgView {
    if (!_rightImgView) {
        _rightImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:self.rightImg];
            object.myTop = 15;
            object.myRight = 15;
            object;
        });
    }
    return _rightImgView;
}

- (UILabel *)centerLbl {
    if (!_centerLbl) {
        _centerLbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 0;
            object.myTop = 15;
            object.myLeft = 20;
            object.myRight = 15;
            object.myBottom = 15;
            object.weight = 1;
            object.attributedText = self.centerAtt;
            object;
        });
    }
    return _centerLbl;
}

@end
