//
//  ShipperListCell.m
//  BOB
//
//  Created by Colin on 2020/10/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ShipperListCell.h"

@interface ShipperListCell()

@property (nonatomic, strong) UILabel *lbl;

@property (nonatomic, strong) UIView *topLine;

@property (nonatomic, strong) UIView *bottomLine;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) MyFrameLayout *cycleView;

@end

@implementation ShipperListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

- (void)configureView:(TracesInfoObj *)obj {
    if (!([obj isKindOfClass:TracesInfoObj.class] || [obj isKindOfClass:TracesOtherInfoObj.class])) {
        return;
    }
    
    self.lbl.attributedText = obj.formatContentAtt;
    if (obj.isFirst) {
        self.topLine.visibility = MyVisibility_Invisible;
        self.imgView.visibility = MyVisibility_Visible;
        self.cycleView.visibility = MyVisibility_Invisible;
    } else {
        self.topLine.visibility = MyVisibility_Visible;
        self.imgView.visibility = MyVisibility_Invisible;
        self.cycleView.visibility = MyVisibility_Visible;
    }
    
    if (obj.isLast) {
        self.bottomLine.visibility = MyVisibility_Invisible;
    } else {
        self.bottomLine.visibility = MyVisibility_Visible;
    }
}

- (void)createUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.contentView addSubview:baseLayout];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myWidth = 15;
    layout.myLeft = 25;
    layout.myRight = 15;
    layout.myTop = 0;
    layout.myBottom = 0;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.topLine];
    MyFrameLayout *frameLayout = [MyFrameLayout new];
    frameLayout.mySize = CGSizeMake(15, 15);
    frameLayout.myCenterX = 0;
    [frameLayout addSubview:self.imgView];
    [frameLayout addSubview:self.cycleView];
    [layout addSubview:frameLayout];
    [layout addSubview:self.bottomLine];
    
    [baseLayout addSubview:self.lbl];
}

- (UIView *)topLine {
    if (!_topLine) {
        _topLine = [UIView new];
        _topLine.backgroundColor = [UIColor colorWithHexString:@"#DADAE6"];
        _topLine.myWidth = 1;
        _topLine.myCenterX = 0;
        _topLine.myHeight = 15;
    }
    return _topLine;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine = [UIView new];
        _bottomLine.backgroundColor = [UIColor colorWithHexString:@"#DADAE6"];
        _bottomLine.myWidth = 1;
        _bottomLine.myCenterX = 0;
        _bottomLine.weight = 1;
    }
    return _bottomLine;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.image = [UIImage imageNamed:@"圆点_绿色"];
        _imgView.size = CGSizeMake(15, 15);
        _imgView.mySize = _imgView.size;
        _imgView.myCenterX = 0;
    }
    return _imgView;
}

- (UILabel *)lbl {
    if (!_lbl) {
        _lbl = [UILabel new];
        _lbl.numberOfLines = 0;
        _lbl.myTop = 10;
        _lbl.myBottom = 10;
        _lbl.myRight = 15;
        _lbl.weight = 1;
    }
    return _lbl;
}

- (MyFrameLayout *)cycleView {
    if (!_cycleView) {
        _cycleView = [MyFrameLayout new];
        _cycleView.size = CGSizeMake(15, 15);
        _cycleView.mySize = _cycleView.size;
        _cycleView.backgroundColor = [UIColor whiteColor];
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor moBackground];
        view.size = CGSizeMake(6, 6);
        view.mySize = view.size;
        view.myCenter = CGPointZero;
        [view setViewCornerRadius:view.height/2.0];
        [_cycleView addSubview:view];
    }
    return _cycleView;
}

@end
 
