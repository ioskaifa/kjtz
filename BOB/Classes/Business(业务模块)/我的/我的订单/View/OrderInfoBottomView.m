//
//  OrderInfoBottomView.m
//  BOB
//
//  Created by mac on 2020/9/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "OrderInfoBottomView.h"
#import "UIImage+Color.h"

@interface OrderInfoBottomView ()
///取消订单
@property (nonatomic, strong) UIButton *cancelBtn;
///立即付款
@property (nonatomic, strong) UIButton *payBtn;
///确认收货
@property (nonatomic, strong) UIButton *receiveBtn;
///退款
@property (nonatomic, strong) UIButton *refundBtn;

@property (nonatomic, strong) MyOrderListObj *obj;

@end

@implementation OrderInfoBottomView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return safeAreaInsetBottom() + 55;
}

- (void)configureView:(MyOrderListObj *)obj {
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    self.cancelBtn.visibility = MyVisibility_Gone;
    self.payBtn.visibility = MyVisibility_Gone;
    self.receiveBtn.visibility = MyVisibility_Gone;
    
    switch (obj.orderStatus) {
        case OrderStatusToPay: {
            if (!obj.is_makeGroup) {
                self.cancelBtn.visibility = MyVisibility_Visible;
            }            
            self.payBtn.visibility = MyVisibility_Visible;
            break;
        }
        case OrderStatusToReceive: {
            self.receiveBtn.visibility = MyVisibility_Visible;
            break;
        }
        default:
            break;
    }
    if (obj.isRefund) {
        self.refundBtn.visibility = MyVisibility_Visible;
    } else {
        self.refundBtn.visibility = MyVisibility_Gone;
    }
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:[UIView placeholderHorzView]];
    [layout addSubview:self.refundBtn];
    [layout addSubview:self.cancelBtn];
    [layout addSubview:self.payBtn];
    [layout addSubview:self.receiveBtn];
    [self addTopSingleLine:[UIColor moBackground]];
}

- (UIButton *)cancelBtn {
    if (!_cancelBtn) {
        _cancelBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(116, 42);
            [object setViewCornerRadius:2];
            [object setTitleColor:[UIColor colorWithHexString:@"#46474D"] forState:UIControlStateNormal];
            object.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
            object.layer.borderWidth = 1;
            [object setTitle:@"取消订单" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font15];
            object.mySize = object.size;
            object.myTop = ([self.class viewHeight] - object.height)/2.0 - safeAreaInsetBottom()/2;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [btn.nextResponder routerEventWithName:@"OrderInfoBottomViewCancel" userInfo:nil];
            }];
            object;
        });
    }
    return _cancelBtn;
}

- (UIButton *)payBtn {
    if (!_payBtn) {
        _payBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(116, 42);
            [object setViewCornerRadius:2];
            [object setBackgroundColor:[UIColor moGreen]];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont15];
            [object setTitle:@"立即付款" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myTop = ([self.class viewHeight] - object.height)/2.0 - safeAreaInsetBottom()/2;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [btn.nextResponder routerEventWithName:@"OrderInfoBottomViewPay" userInfo:nil];
            }];
            object;
        });
    }
    return _payBtn;
}

- (UIButton *)receiveBtn {
    if (!_receiveBtn) {
        _receiveBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(116, 42);
            [object setViewCornerRadius:2];
            [object setBackgroundColor:[UIColor moGreen]];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont15];
            [object setTitle:@"确认收货" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myTop = ([self.class viewHeight] - object.height)/2.0 - safeAreaInsetBottom()/2;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [btn.nextResponder routerEventWithName:@"OrderInfoBottomViewConfirm" userInfo:nil];
            }];
            object;
        });
    }
    return _receiveBtn;
}

- (UIButton *)refundBtn {
    if (!_refundBtn) {
        _refundBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font13];
            object.size = CGSizeMake(116, 42);
            [object setViewCornerRadius:2];
            [object setTitleColor:[UIColor colorWithHexString:@"#46474D"] forState:UIControlStateNormal];
            object.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
            object.layer.borderWidth = 1;
            object.titleLabel.font = [UIFont boldFont15];
            [object setTitle:@"退款" forState:UIControlStateNormal];
            object.mySize = object.size;
            object.myTop = ([self.class viewHeight] - object.height)/2.0 - safeAreaInsetBottom()/2;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [btn.nextResponder routerEventWithName:@"OrderInfoBottomViewRefund" userInfo:nil];
            }];
            object;
        });
    }
    return _refundBtn;
}

@end
