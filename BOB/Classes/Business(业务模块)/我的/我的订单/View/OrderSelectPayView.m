//
//  OrderSelectPayView.m
//  BOB
//
//  Created by mac on 2020/9/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "OrderSelectPayView.h"
#import "STDTableCellView.h"
#import "ModifyPayPwdVC.h"

@interface OrderSelectPayView ()
///订单金额
@property (nonatomic, assign) CGFloat amount;
///折扣
@property (nonatomic, assign) double rate;
///支付宝
@property (nonatomic, strong) STDTableCellView *aliPayView;
///微信
@property (nonatomic, strong) STDTableCellView *wechatView;
///余额
@property (nonatomic, strong) MyBaseLayout *balanceView;
///
@property (nonatomic, strong) STDTableCellView *selectedView;

@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, strong) UILabel *amountLbl;
@property (nonatomic, strong) UILabel *SLAPayTipLbl;

@property (nonatomic, assign) BOOL noSLAPay;

@end

@implementation OrderSelectPayView

- (instancetype)initWithAmount:(CGFloat)amount {
    if (self = [super init]) {
        self.amount = amount;
        [self createUI];
    }
    return self;
}

- (instancetype)initWithAmount:(CGFloat)amount withRate:(CGFloat)rate withSlaPrice:(CGFloat)slaPrice noSLAPay:(BOOL)noSLAPay {
    if (self = [super init]) {
        self.amount = amount;
        self.rate = rate;
        _sla_price = slaPrice;
        self.noSLAPay = noSLAPay;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 550 + safeAreaInsetBottom();
}

+ (void)checkSetPayPassword:(FinishedBlock)complete {
    if (UDetail.user.isSetPayPassword) {
        if (complete) {
            complete(nil);
        }
    } else {
        [MXAlertViewHelper showAlertViewWithMessage:@"还未设置支付密码，立即设置?" completion:^(BOOL cancelled, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                ModifyPayPwdVC *vc = [ModifyPayPwdVC new];
                vc.complete = complete;
                vc.title = @"设置支付密码";
                [[[MXRouter sharedInstance] getTopNavigationController] pushViewController:vc animated:YES];
            }
        }];
    }
}

- (void)setSla_price:(CGFloat)sla_price {
    _sla_price = sla_price;
    [self configureAmountLbl];
}

- (void)configureAmountLbl {
    NSMutableAttributedString *att;
    if ([@"SLA" isEqualToString:[self selectPayType]]) {
        if (self.sla_price == 0) {
            [NotifyHelper showHUDAddedTo:self animated:YES];
            return;
        }
        [NotifyHelper hideHUDForView:self animated:YES];
        NSArray *txtArr = @[StrF(@"%0.4f", self.amount*self.rate/self.sla_price), @"SLA"];
        NSArray *colorArr = @[[UIColor moBlack], [UIColor moBlack]];
        NSArray *fontArr = @[[UIFont fontWithName:@"DINAlternate-Bold" size:42], [UIFont systemFontOfSize:22]];
        att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        self.SLAPayTipLbl.visibility = MyVisibility_Visible;
    } else {
        NSArray *txtArr = @[@"￥", StrF(@"%0.2f", self.amount)];
        NSArray *colorArr = @[[UIColor moBlack], [UIColor moBlack]];
        NSArray *fontArr = @[[UIFont systemFontOfSize:22], [UIFont fontWithName:@"DINAlternate-Bold" size:42]];
        att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        self.SLAPayTipLbl.visibility = MyVisibility_Invisible;
        
    }
    self.amountLbl.attributedText = att;
    [self.amountLbl sizeToFit];
    self.amountLbl.mySize = self.amountLbl.size;
}

#pragma mark 选择的充值类型
- (NSString *)selectPayType {
    if (self.selectedView == self.aliPayView) {
        return @"支付宝";
    } else if (self.selectedView == self.wechatView) {
        return @"微信";
    } else {
        return @"SLA";
    }
}

- (void)selectPayClick:(UIView *)sender {
    if (![sender isKindOfClass:STDTableCellView.class]) {
        return;
    }
    if (sender == self.selectedView) {
        return;
    }
    if (self.only_sla_pay) {
        if (sender != self.balanceView) {
            [NotifyHelper showMessageWithMakeText:@"当前商品仅支持SLA支付"];
            return;
        }
    }
    [self.selectedView configureRightImg:@"icon_red_unselect"];
    STDTableCellView *cellView = (STDTableCellView *)sender;
    [cellView configureRightImg:@"icon_green_select"];
    self.selectedView = cellView;
    [self configureAmountLbl];
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [layout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font18];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"支付订单";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 15;
    lbl.myBottom = 15;
    lbl.myCenterX = 0;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView fullLine]];
    
    lbl = [UILabel new];
    NSArray *txtArr = @[@"￥", StrF(@"%0.2f", self.amount)];
    NSArray *colorArr = @[[UIColor moBlack], [UIColor moBlack]];
    NSArray *fontArr = @[[UIFont boldFont20], [UIFont boldSystemFontOfSize:40]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    lbl.attributedText = att;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 20;
    lbl.myCenterX = 0;
    self.amountLbl = lbl;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
    lbl.text = StrF(@"≈￥%0.2f", self.amount * self.rate);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 10;
    lbl.myCenterX = 0;
    self.SLAPayTipLbl = lbl;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
    lbl.text = @"选择支付方式";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 30;
    lbl.myLeft = 20;
    [layout addSubview:lbl];
    
    UIView *line = [UIView gapLineWithHeight:SINGLE_LINE_HEIGHT];
    if (!self.noSLAPay) {
        [layout addSubview:self.balanceView];
        line.myLeft = 20;
        line.myRight = 20;
        [layout addSubview:line];
    } else {
        self.selectedView = self.aliPayView;
    }
    
    [layout addSubview:self.aliPayView];
    line = [UIView gapLineWithHeight:SINGLE_LINE_HEIGHT];
    line.myLeft = 20;
    line.myRight = 20;
    [layout addSubview:line];
    [layout addSubview:self.wechatView];
    line = [UIView gapLineWithHeight:SINGLE_LINE_HEIGHT];
    line.myLeft = 20;
    line.myRight = 20;
    [layout addSubview:line];
    
    [layout addSubview:[UIView placeholderVertView]];
    [layout addSubview:self.submitBtn];
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.closeImgView.size);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
    [self configureAmountLbl];
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

- (STDTableCellView *)aliPayView {
    if (!_aliPayView) {
        _aliPayView = ({
            NSArray *txtArr = @[@"支付宝"];
            NSArray *colorArr = @[[UIColor moBlack]];
            NSArray *fontArr = @[[UIFont font15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                                colors:colorArr
                                                                                 fonts:fontArr];
            NSAttributedString *leftAtt = att;
            NSAttributedString *rightAtt = [[NSAttributedString alloc] initWithString:@""];
            NSString *rightImg = @"icon_red_unselect";
            if (self.noSLAPay) {
                rightImg = @"icon_green_select";
            }
            STDTableCellView *object = [[STDTableCellView alloc] initWithLeftImg:@"支付宝"
                                                                         leftTxt:leftAtt
                                                                        rightTxt:rightAtt
                                                                        rightImg:rightImg];
            object.myLeft = 20;
            object.myRight = 20;
            object.myHeight = 50;
            @weakify(self)
            [object addAction:^(UIView *view) {
                @strongify(self)
                [self selectPayClick:view];
            }];
            object;
        });
    }
    return _aliPayView;
}

- (STDTableCellView *)wechatView {
    if (!_wechatView) {
        _wechatView = ({
            NSArray *txtArr = @[@"微信"];
            NSArray *colorArr = @[[UIColor moBlack]];
            NSArray *fontArr = @[[UIFont font15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                                colors:colorArr
                                                                                 fonts:fontArr];
            NSAttributedString *leftAtt = att;
            NSAttributedString *rightAtt = [[NSAttributedString alloc] initWithString:@""];
            STDTableCellView *object = [[STDTableCellView alloc] initWithLeftImg:@"微信"
                                                                         leftTxt:leftAtt
                                                                        rightTxt:rightAtt
                                                                        rightImg:@"icon_red_unselect"];
            object.myLeft = 20;
            object.myRight = 20;
            object.myHeight = 50;
            @weakify(self)
            [object addAction:^(UIView *view) {
                @strongify(self)
                [self selectPayClick:view];
            }];
            object;
        });
    }
    return _wechatView;
}

- (MyBaseLayout *)balanceView {
    if (!_balanceView) {
        _balanceView = ({
            NSArray *txtArr = @[@"SLA", StrF(@" (%0.2f)", UDetail.user.sla_num)];
            NSArray *colorArr = @[[UIColor moBlack], [UIColor colorWithHexString:@"#A8A8A8"]];
            NSArray *fontArr = @[[UIFont font15], [UIFont font15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                                colors:colorArr
                                                                                 fonts:fontArr];
            NSAttributedString *leftAtt = att;
            NSAttributedString *rightAtt = [[NSAttributedString alloc] initWithString:@""];
            NSString *rightImg = @"icon_green_select";
            if (self.noSLAPay) {
                rightImg = @"icon_red_unselect";
            }
            STDTableCellView *object = [[STDTableCellView alloc] initWithLeftImg:@"SLA_pay"
                                                                         leftTxt:leftAtt
                                                                        rightTxt:rightAtt
                                                                        rightImg:rightImg];
            object.myLeft = 20;
            object.myRight = 20;
            object.myHeight = 50;
            object.myTop = 0;
            if (!self.noSLAPay) {
                self.selectedView = object;
            }
            @weakify(self)
            [object addAction:^(UIView *view) {
                @strongify(self)
                [self selectPayClick:view];
            }];
            MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            layout.myLeft = 0;
            layout.myRight = 0;
            layout.myHeight = MyLayoutSize.wrap;
            layout.myTop = 25;
            [layout addSubview:object];
            if (self.rate > 0 && self.rate < 1) {
                UILabel *lbl = [UILabel new];
                lbl.textAlignment = NSTextAlignmentCenter;
                lbl.font = [UIFont font12];
                lbl.text = StrF(@"%0.1f折优惠", self.rate*10);
                lbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
                [lbl sizeToFit];
                lbl.size = CGSizeMake(lbl.width + 10, lbl.height + 5);
                lbl.mySize = lbl.size;
                lbl.myTop = -5;
                lbl.layer.borderWidth = 1;
                lbl.layer.borderColor = [UIColor colorWithHexString:@"#FF4757"].CGColor;
                lbl.myLeft = 57;
                lbl.myBottom = 15;
                [lbl setViewCornerRadius:3];
                [layout addSubview:lbl];
            }
            
            [layout layoutIfNeeded];
            layout;
        });
    }
    return _balanceView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"确认支付" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myLeft = 15;
            object.myRight = 15;
            object.myBottom = safeAreaInsetBottom() + 20;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                if (self.block) {
                    self.block([self selectPayType]);
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

@end
