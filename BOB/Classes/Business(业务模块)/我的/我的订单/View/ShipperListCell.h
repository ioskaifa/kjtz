//
//  ShipperListCell.h
//  BOB
//
//  Created by Colin on 2020/10/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShipperInfoObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShipperListCell : UITableViewCell

- (void)configureView:(TracesInfoObj *)obj;

@end

NS_ASSUME_NONNULL_END
