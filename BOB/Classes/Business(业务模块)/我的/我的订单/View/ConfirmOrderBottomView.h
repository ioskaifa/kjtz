//
//  ConfirmOrderBottomView.h
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConfirmOrderBottomView : UIView

+ (CGFloat)viewHeight;

///  填充数据
/// @param totalNum 合计金额
/// @param discontNum 折扣金额
- (void)configureView:(CGFloat)totalNum discontNum:(CGFloat)discontNum;

- (void)configurePayBtnState:(BOOL)entable;

@end

NS_ASSUME_NONNULL_END
