//
//  GoodSpeciSectionTitleView.m
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodSpeciSectionTitleView.h"

@interface GoodSpeciSectionTitleView ()

@property (nonatomic, strong) UILabel *titleLbl;

@end

@implementation GoodSpeciSectionTitleView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60;
}

- (void)configureView:(NSString *)title {
    self.titleLbl.text = title?:@"";
    [self.titleLbl sizeToFit];
    self.titleLbl.mySize = self.titleLbl.size;
}

- (void)createUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    [layout addSubview:self.titleLbl];
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor colorWithHexString:@"#403D4D"];
            object.font = [UIFont font13];
            object.myLeft = 15;
            object.myBottom = 15;
            object;
        });
    }
    return _titleLbl;
}


@end
