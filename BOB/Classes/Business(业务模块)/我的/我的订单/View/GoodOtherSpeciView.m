//
//  GoodOtherSpeciView.m
//  BOB
//
//  Created by mac on 2020/10/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodOtherSpeciView.h"
#import "GoodSpeciCell.h"
#import "GoodSpeciSectionTitleView.h"
#import "GoodSpeciSectionNumView.h"

@interface GoodOtherSpeciView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) GoodSKUObj *obj;

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) NSArray *dataSourceArr;
///已选择的规格 (多个)
@property (nonatomic, strong) NSMutableDictionary *selectedMDic;
///已选择组合规格 (一个)
@property (nonatomic, strong) NSMutableDictionary *selectedIDMDic;

@property (nonatomic, strong) UIButton *submitBtn;

@property (nonatomic, strong) GoodSpeciSectionNumView *sectionNumView;

@end

@implementation GoodOtherSpeciView

- (instancetype)initWithSpeci:(GoodSKUObj *)obj {
    if (self = [super init]) {
        self.obj = obj;
        [self createUI];
        
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 550 + safeAreaInsetBottom();
}

- (void)reload {
    [self.colView reloadData];
}

- (void)configureHeaderView {
    [self.selectedIDMDic removeAllObjects];
//    NSArray *characterIDArr = [self.selectedMDic keysSortedByValueUsingComparator:^NSComparisonResult(CharacterListItem *obj1, CharacterListItem *obj2) {
//        if ([obj1.character_id integerValue] > [obj2.character_id integerValue]) {
//            return NSOrderedDescending;
//        }else if ([obj1.character_id integerValue] < [obj2.character_id integerValue]){
//            return NSOrderedAscending;
//        } else {
//            return NSOrderedSame;
//        }
//    }];
    NSArray *tempArr = self.selectedMDic.allValues;
    NSArray *characterIDArr = [tempArr valueForKeyPath:@"@unionOfObjects.character_value_name"];
    //属性都选全了 更新头部的信息
    if (characterIDArr.count == self.dataSourceArr.count) {
        SkuListItem *item = [self.obj findSKUBy:characterIDArr];
        if (item) {
            NSString *imgUrl = item.imageUrl;
            CGFloat price = item.marketPrice;
            NSInteger stock = item.stock;
            NSString *name = item.describe;
            [self.headerView configureView:imgUrl marketPrice:price stock:stock chooseSpeci:name];
            [self.sectionNumView configureView:stock];
            [self.selectedIDMDic setValue:item.skuId forKey:@"character_value"];
        } else {
            [self.headerView configureViewDefaultValue];
        }
    } else {
        [self.headerView configureViewDefaultValue];
    }
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.dataSourceArr.count + 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section >= self.dataSourceArr.count) {
        return 0;
    }
    NSArray *tempArr = self.dataSourceArr[section];
    if (![tempArr isKindOfClass:NSArray.class]) {
        return 0;
    }
    return tempArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = GoodSpeciCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceArr.count) {
        return;
    }
    NSArray *tempArr = self.dataSourceArr[indexPath.section];
    if (![tempArr isKindOfClass:NSArray.class]) {
        return;
    }
    if (indexPath.row >= tempArr.count) {
        return;
    }
    CharacterListItem *obj = tempArr[indexPath.row];
    if (![obj isKindOfClass:[CharacterListItem class]]) {
        return;
    }
    if (![cell isKindOfClass:[GoodSpeciCell class]]) {
        return;
    }
    
    GoodSpeciCell *listCell = (GoodSpeciCell *)cell;
    [listCell configureView:obj];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == self.dataSourceArr.count) {
            return self.sectionNumView;
        } else {
            GoodSpeciSectionTitleView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:NSStringFromClass(GoodSpeciSectionTitleView.class) forIndexPath:indexPath];
            if (indexPath.section >= self.dataSourceArr.count) {
                return view;
            }
            NSArray *tempArr = self.dataSourceArr[indexPath.section];
            if (![tempArr isKindOfClass:NSArray.class]) {
                return view;
            }
            CharacterListItem *obj = tempArr.firstObject;
            if ([obj isKindOfClass:CharacterListItem.class]) {
                [view configureView:obj.character_name];
            }
            return view;
        }
    } else {
        return [UICollectionReusableView new];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceArr.count) {
        return;
    }
    NSArray *tempArr = self.dataSourceArr[indexPath.section];
    if (![tempArr isKindOfClass:NSArray.class]) {
        return;
    }
    if (indexPath.row >= tempArr.count) {
        return;
    }
    CharacterListItem *obj = tempArr[indexPath.row];
    if (obj.cellType == GoodSpeciCellTypeDisEnable) {
        return;
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cellType == 1"];
    NSArray *filterArr = [tempArr filteredArrayUsingPredicate:predicate];
    if (filterArr.count == 0) {
        obj.cellType = GoodSpeciCellTypeSelected;
        [collectionView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
        [self.selectedMDic setValue:obj forKey:obj.character_name];
        [self configureHeaderView];
        return;
    }
    if ([filterArr containsObject:obj]) {
        obj.cellType = GoodSpeciCellTypeNormal;
        [self.selectedMDic removeObjectForKey:obj.character_name];
        [self configureHeaderView];
        [collectionView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
        return;
    }
    obj.cellType = GoodSpeciCellTypeSelected;
    for (CharacterListItem *item in filterArr) {
        if (![item isKindOfClass:CharacterListItem.class]) {
            continue;
        }
        item.cellType = GoodSpeciCellTypeNormal;
    }
    [self.selectedMDic setValue:obj forKey:obj.character_name];
    [self configureHeaderView];
    [collectionView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceArr.count) {
        return CGSizeZero;
    }
    NSArray *tempArr = self.dataSourceArr[indexPath.section];
    if (![tempArr isKindOfClass:NSArray.class]) {
        return CGSizeZero;
    }
    if (indexPath.row >= tempArr.count) {
        return CGSizeZero;
    }
    CharacterListItem *obj = tempArr[indexPath.row];
    if (![obj isKindOfClass:[CharacterListItem class]]) {
        return CGSizeZero;
    }
    return obj.itemSize;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section == self.dataSourceArr.count) {
        return CGSizeMake(SCREEN_WIDTH, [GoodSpeciSectionNumView viewHeight]);
    } else {
        return CGSizeMake(SCREEN_WIDTH, [GoodSpeciSectionTitleView viewHeight]);
    }
}

#pragma mark - InitUI
- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [layout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    [layout addSubview:self.headerView];
    [layout addSubview:self.colView];
    [layout addSubview:self.submitBtn];
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.closeImgView.size);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
    [self.colView reloadData];
}

#pragma mark - Init
- (UICollectionView *)colView {
    if (!_colView) {
        _colView = ({
            UICollectionView *object = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
            object.backgroundColor = [UIColor whiteColor];
            object.delegate = self;
            object.dataSource = self;
            Class cls = GoodSpeciCell.class;
            [object registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
            object.weight = 1;
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            cls = GoodSpeciSectionNumView.class;
            [object registerClass:cls forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
              withReuseIdentifier:NSStringFromClass(cls)];
            cls = GoodSpeciSectionTitleView.class;
            [object registerClass:cls forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
              withReuseIdentifier:NSStringFromClass(cls)];
            object;
        });
    }
    return _colView;
}

- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = ({
            UICollectionViewFlowLayout *object = [UICollectionViewFlowLayout new];
            object.minimumLineSpacing = 15;
            object.minimumInteritemSpacing = 15;
            object.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15);
            //cell 靠左显示
            SEL sel = NSSelectorFromString(@"_setRowAlignmentsOptions:");
            if ([object respondsToSelector:sel]) {
                NSDictionary *dic =  @{@"UIFlowLayoutCommonRowHorizontalAlignmentKey":@(NSTextAlignmentLeft),
                                       @"UIFlowLayoutLastRowHorizontalAlignmentKey" : @(NSTextAlignmentLeft),
                                       @"UIFlowLayoutRowVerticalAlignmentKey" : @(NSTextAlignmentCenter)};
                ((void(*)(id,SEL,NSDictionary*))objc_msgSend)(object,sel,dic);
            }
            object;
        });
    }
    return _flowLayout;
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"确认" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myLeft = 15;
            object.myRight = 15;
            object.myBottom = safeAreaInsetBottom() + 20;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                if (self.selectedMDic.allKeys.count != self.dataSourceArr.count) {
                    [NotifyHelper showMessageWithMakeText:@"请选择商品属性"];
                    return;
                }
                //把商品数量回调回去
                [self.selectedIDMDic setValue:@(self.sectionNumView.value) forKey:@"goods_num"];
                if (self.block) {
                    self.block(self.selectedIDMDic);
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

- (GoodSpeciHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [GoodSpeciHeaderView new];
        _headerView.size = CGSizeMake(SCREEN_WIDTH, [GoodSpeciHeaderView viewHeight]);
        _headerView.mySize = _headerView.size;
    }
    return _headerView;
}

- (NSArray *)dataSourceArr {
    if (!_dataSourceArr) {
        _dataSourceArr = self.obj.characterSectionArr;
    }
    return _dataSourceArr;
}

- (GoodSpeciSectionNumView *)sectionNumView {
    if (!_sectionNumView) {
        _sectionNumView = [self.colView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass(GoodSpeciSectionNumView.class) forIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.dataSourceArr.count]];
    }
    return _sectionNumView;
}

- (NSMutableDictionary *)selectedMDic {
    if (!_selectedMDic) {
        _selectedMDic = [NSMutableDictionary dictionaryWithCapacity:self.dataSourceArr.count];
    }
    return _selectedMDic;
}

- (NSMutableDictionary *)selectedIDMDic {
    if (!_selectedIDMDic) {
        _selectedIDMDic = [NSMutableDictionary dictionaryWithCapacity:1];
    }
    return _selectedIDMDic;
}

@end
