//
//  GoodSpeciHeaderView.m
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodSpeciHeaderView.h"

@interface GoodSpeciHeaderView ()

@property (nonatomic, strong) UIImageView *imgView;
///当前价格
@property (nonatomic, strong) UILabel *priceLbl;
///折扣价 标签
@property (nonatomic, strong) UILabel *discountLbl;
///市场价
@property (nonatomic, strong) UILabel *marketPriceLbl;
///库存
@property (nonatomic, strong) UILabel *stockLbl;
///已选规格
@property (nonatomic, strong) UILabel *chooseLbl;

@end

@implementation GoodSpeciHeaderView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 130;
}

- (void)configureViewDefaultValue {
    NSInteger graded = UDetail.user.walletInfoObj.user_grade;
    CGFloat rate = UDetail.user.walletInfoObj.mall_discount_rate;
    CGFloat price = self.goodInfoObj.cash_min;
    NSInteger stock = self.goodInfoObj.goods_stock_num;
    if (!self.goodInfoObj.isSelfSupport) {
        price = 0.0;
        stock = 0;
    }
    [self configureView:self.goodInfoObj.goods_show
                 graded:graded
           discountRate:rate
            marketPrice:price
                  stock:stock
            chooseSpeci:@""];
}

- (void)configureView:(NSString *)imgUrl
               graded:(NSInteger)graded
         discountRate:(CGFloat)rate
          marketPrice:(CGFloat)marketPrice
                stock:(NSInteger)stock
          chooseSpeci:(NSString *)chooseSpeci {
    self.discountLbl.visibility = MyVisibility_Gone;
    self.marketPriceLbl.visibility = MyVisibility_Gone;
    
//    imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, imgUrl);
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    CGFloat price = marketPrice;
//    if (graded > 0) {
//        price = price * rate;
//    }
    self.priceLbl.text = StrF(@"￥%0.2f", price);
    [self.priceLbl sizeToFit];
    self.priceLbl.mySize = self.priceLbl.size;
    
    NSArray *txtArr = @[StrF(@"￥%0.2f", marketPrice)];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#A8A8A8"]];
    NSArray *fontArr = @[[UIFont font12]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttributes:attribtDic range:range];
    self.marketPriceLbl.attributedText = att;
    [self.marketPriceLbl sizeToFit];
    self.marketPriceLbl.mySize = self.marketPriceLbl.size;
    
    self.stockLbl.text = StrF(@"库存：%ld", (long)stock);
    [self.stockLbl sizeToFit];
    self.stockLbl.mySize = self.stockLbl.size;
    
    [self configureChooseSpeci:chooseSpeci];
//    if (graded > 0) {
//        self.discountLbl.visibility = MyVisibility_Visible;
//        self.marketPriceLbl.visibility = MyVisibility_Visible;
//    }
}

- (void)configureView:(NSString *)imgUrl
          marketPrice:(CGFloat)marketPrice
                stock:(NSInteger)stock
          chooseSpeci:(NSString *)chooseSpeci {
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    self.priceLbl.text = StrF(@"￥%0.2f", marketPrice);
    [self.priceLbl sizeToFit];
    self.priceLbl.mySize = self.priceLbl.size;
    
    self.stockLbl.text = StrF(@"库存：%ld", (long)stock);
    [self.stockLbl sizeToFit];
    self.stockLbl.mySize = self.stockLbl.size;
    
    [self configureChooseSpeci:chooseSpeci];
}

- (void)configureChooseSpeci:(NSString *)string {
    self.chooseLbl.text = StrF(@"已选：%@", string);
    [self.chooseLbl sizeToFit];
    CGFloat width = SCREEN_WIDTH - 45 - self.imgView.width;
    if (self.chooseLbl.width > width) {
        self.chooseLbl.width = width;
    }
    self.chooseLbl.mySize = self.chooseLbl.size;
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    [layout addSubview:self.imgView];
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.weight = 1;
    subLayout.myCenterY = 0;
    subLayout.myLeft = 0;
    subLayout.myRight = 15;
    subLayout.myHeight = self.imgView.height;
    [layout addSubview:subLayout];
    
    MyLinearLayout *disPriceLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    disPriceLayout.myTop = 0;
    disPriceLayout.myHeight = MyLayoutSize.wrap;
    disPriceLayout.myLeft = 0;
    disPriceLayout.myRight = 0;
    [subLayout addSubview:disPriceLayout];
    [disPriceLayout addSubview:self.priceLbl];
    [disPriceLayout addSubview:self.discountLbl];
    
    [subLayout addSubview:self.marketPriceLbl];
    [subLayout addSubview:self.stockLbl];
    [subLayout addSubview:self.chooseLbl];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.size = CGSizeMake(100, 100);
            [object setViewCornerRadius:5];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 15;
            object.myRight = 15;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor blackColor];
            object.font = [UIFont systemFontOfSize:23];
            object.myRight = 15;
            object;
        });
    }
    return _priceLbl;
}

- (UILabel *)discountLbl {
    if (!_discountLbl) {
        _discountLbl = ({
            UILabel *object = [UILabel new];
            object.textAlignment = NSTextAlignmentCenter;
            object.size = CGSizeMake(46, 20);
            [object setViewCornerRadius:3];
            object.mySize = object.size;
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#FF293B"];
            object.text = @"折扣价";
            object.backgroundColor = [UIColor colorWithHexString:@"#F9E5E6"];
            object.myCenterY = 0;
            object.visibility = MyVisibility_Gone;
            object;
        });
    }
    return _discountLbl;
}

- (UILabel *)marketPriceLbl {
    if (!_marketPriceLbl) {
        _marketPriceLbl = ({
            UILabel *object = [UILabel new];
            object.myTop = 5;
            object.visibility = MyVisibility_Gone;
            object;
        });
    }
    return _marketPriceLbl;
}

- (UILabel *)stockLbl {
    if (!_stockLbl) {
        _stockLbl = ({
            UILabel *object = [UILabel new];
            object.myTop = 10;
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
            object;
        });
    }
    return _stockLbl;
}

- (UILabel *)chooseLbl {
    if (!_chooseLbl) {
        _chooseLbl = ({
            UILabel *object = [UILabel new];
            object.myTop = 5;
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#45454D"];
            object.text = @"已选：";
            [object sizeToFit];
            object.mySize = object.size;
            object;
        });
    }
    return _chooseLbl;
}

@end
