//
//  ConfirmGoodInfoView.h
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderInfoTempObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConfirmGoodInfoView : UIView

@property (nonatomic, strong) MyBaseLayout *layout;

@property (nonatomic, strong) UIView *line;

- (void)configureView:(OrderGoodsListItem *)obj;

@end

NS_ASSUME_NONNULL_END
