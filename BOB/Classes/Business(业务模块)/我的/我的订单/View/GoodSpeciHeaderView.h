//
//  GoodSpeciHeaderView.h
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsInfoObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodSpeciHeaderView : UIView

+ (CGFloat)viewHeight;

@property (nonatomic, strong) GoodsInfoObj *goodInfoObj;

/// 设置默认值
- (void)configureViewDefaultValue;

/// configureView
/// @param imgUrl 商品图片
/// @param graded 会员等级
/// @param rate 会员折扣
/// @param marketPrice 市场价
/// @param stock 库存
- (void)configureView:(NSString *)imgUrl
               graded:(NSInteger)graded
         discountRate:(CGFloat)rate
          marketPrice:(CGFloat)marketPrice
                stock:(NSInteger)stock
          chooseSpeci:(NSString *)chooseSpeci;

/// configureView
/// @param imgUrl 商品图片
/// @param marketPrice 市场价
/// @param stock 库存
- (void)configureView:(NSString *)imgUrl
          marketPrice:(CGFloat)marketPrice
                stock:(NSInteger)stock
          chooseSpeci:(NSString *)chooseSpeci;

- (void)configureChooseSpeci:(NSString *)string;

@end

NS_ASSUME_NONNULL_END
