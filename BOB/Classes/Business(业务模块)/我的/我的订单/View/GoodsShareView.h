//
//  GoodsShareView.h
//  BOB
//
//  Created by colin on 2021/1/18.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsInfoObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsShareView : UIView

- (instancetype)initWithObj:(GoodsInfoObj *)obj;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
