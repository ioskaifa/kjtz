//
//  GoodSpeciCell.m
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodSpeciCell.h"

@interface GoodSpeciCell ()

@property (nonatomic, strong) UILabel *valueLbl;

@property (nonatomic, strong) CharacterListItem *obj;

@end

@implementation GoodSpeciCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)configureView:(CharacterListItem *)obj {
    if (![obj isKindOfClass:CharacterListItem.class]) {
        return;
    }
    self.obj = obj;
    self.valueLbl.text = obj.character_value_name;
    self.valueLbl.size = obj.itemSize;
    self.valueLbl.mySize = self.valueLbl.size;
    
    switch (self.obj.cellType) {
        case GoodSpeciCellTypeNormal:{
            [self configureValueNormal];
            break;
        }
        case GoodSpeciCellTypeSelected:{
            [self configureValueSelected];
            break;
        }
        case GoodSpeciCellTypeDisEnable:{
            [self configureValueDisEnable];
            break;
        }
        default:
            [self configureValueNormal];
            break;
    }
}

- (void)configureValueNormal {
    [self.valueLbl setViewCornerRadius:2];
    self.valueLbl.backgroundColor = [UIColor colorWithHexString:@"#F5F5FA"];
    self.valueLbl.textColor = [UIColor colorWithHexString:@"#5C5C66"];
    self.valueLbl.layer.borderColor = [UIColor colorWithHexString:@"#F5F5FA"].CGColor;
    self.valueLbl.layer.borderWidth = 1;
}

- (void)configureValueSelected {
    [self.valueLbl setViewCornerRadius:2];
    self.valueLbl.layer.borderColor = [UIColor blackColor].CGColor;
    self.valueLbl.layer.borderWidth = 1;
    self.valueLbl.backgroundColor = [UIColor whiteColor];
    self.valueLbl.textColor = [UIColor blackColor];
}

- (void)configureValueDisEnable {
    self.valueLbl.textColor = [UIColor colorWithHexString:@"#C2C2CC"];
    self.valueLbl.backgroundColor = [UIColor whiteColor];
    [self borderFillDotted];
}

#pragma mark - 填充虚线
- (void)borderFillDotted {
    CAShapeLayer *border = [CAShapeLayer layer];
    //虚线的颜色
    border.strokeColor = [UIColor redColor].CGColor;
    //填充的颜色
    border.fillColor = [UIColor whiteColor].CGColor;
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.valueLbl.bounds
                                                    cornerRadius:self.valueLbl.height/2.0];
    
    //设置路径
    border.path = path.CGPath;
    
    border.frame = self.valueLbl.bounds;
    //虚线的宽度
    border.lineWidth = 1.f;

    //设置线条的样式
    //    border.lineCap = @"square";
    //虚线的间隔
    border.lineDashPattern = @[@4, @2];
    [self.valueLbl setViewCornerRadius:2];
    [self.valueLbl.layer addSublayer:border];
}

- (void)createUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.valueLbl];
}

- (UILabel *)valueLbl {
    if (!_valueLbl) {
        _valueLbl = [UILabel new];
        _valueLbl.font = [UIFont font12];
        _valueLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _valueLbl;
}

@end
