//
//  MyOrderGoodInfoView.m
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyOrderGoodInfoView.h"

@interface MyOrderGoodInfoView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *specsLbl;

@property (nonatomic, strong) UILabel *priceLbl;

@end

@implementation MyOrderGoodInfoView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)configureView:(BuyGoodsListObj *)obj {
    if (![obj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    
    NSString *imgUrl = obj.photo;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    self.priceLbl.size = CGSizeZero;
    self.priceLbl.mySize = self.priceLbl.size;
    NSArray *txtArr = @[StrF(@"￥%0.2f", obj.price), StrF(@"\nx%ld", (long)obj.number)];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#151419"], [UIColor colorWithHexString:@"#AAAABB"]];
    NSArray *fontArr = @[[UIFont boldFont14], [UIFont font12]];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 6; // 调整行间距
    paragraphStyle.alignment = NSTextAlignmentRight;
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    self.priceLbl.attributedText = att;
    [self.priceLbl sizeToFit];
    self.priceLbl.mySize = self.priceLbl.size;
    
    self.titleLbl.text = obj.title;
    self.specsLbl.text = obj.specs;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = self.titleLbl.width;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;

    size = [self.specsLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    self.specsLbl.size = size;
    self.specsLbl.mySize = self.specsLbl.size;
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:self.imgView];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.myLeft = 10;
    subLayout.myRight = 10;
    subLayout.weight = 1;
    subLayout.myTop = 10;
    subLayout.myBottom = 10;
    [layout addSubview:subLayout];
    
    [subLayout addSubview:self.titleLbl];
    [subLayout addSubview:self.specsLbl];
    
    [layout addSubview:self.priceLbl];
    
    [self addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(SINGLE_LINE_HEIGHT);
        make.left.mas_equalTo(subLayout.mas_left);
        make.right.mas_equalTo(self.priceLbl.mas_right);
        make.bottom.mas_equalTo(self.mas_bottom);
    }];
    self.layout = layout;
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            [object setViewCornerRadius:5];
            object.myTop = 10;
            object.myBottom = 10;
            object.myLeft = 10;
            object.widthSize.equalTo(object.heightSize);
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont lightFont14];
            object.numberOfLines = 2;
            object.textColor = [UIColor colorWithHexString:@"#151419"];
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)specsLbl {
    if (!_specsLbl) {
        _specsLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font11];
            object.numberOfLines = 0;
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myTop = 5;
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _specsLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.numberOfLines = 0;
        _priceLbl.myRight = 10;
        _priceLbl.myTop = 10;
    }
    return _priceLbl;
}

- (UIView *)line {
    if (!_line) {
        _line = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor moBackground];
            object;
        });
    }
    return _line;
}

@end
