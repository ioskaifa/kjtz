//
//  GoodOtherSpeciView.h
//  BOB
//
//  Created by mac on 2020/10/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodSpeciHeaderView.h"
#import "GoodSKUObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodOtherSpeciView : UIView

@property (nonatomic, strong) GoodSpeciHeaderView *headerView;

@property (nonatomic, strong) UIImageView *closeImgView;

@property (nonatomic, copy) FinishedBlock block;

+ (CGFloat)viewHeight;

- (instancetype)initWithSpeci:(GoodSKUObj *)obj;

- (void)reload;

@end

NS_ASSUME_NONNULL_END
