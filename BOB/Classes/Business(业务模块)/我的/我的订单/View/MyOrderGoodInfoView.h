//
//  MyOrderGoodInfoView.h
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuyGoodsListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyOrderGoodInfoView : UIView

@property (nonatomic, strong) MyBaseLayout *layout;

@property (nonatomic, strong) UIView *line;

- (void)configureView:(BuyGoodsListObj *)obj;

@end

NS_ASSUME_NONNULL_END
