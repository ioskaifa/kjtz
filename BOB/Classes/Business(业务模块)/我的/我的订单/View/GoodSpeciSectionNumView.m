//
//  GoodSpeciSectionNumView.m
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodSpeciSectionNumView.h"
#import "PPNumberButton.h"

@interface GoodSpeciSectionNumView ()

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) PPNumberButton *numberBtn;

@property (nonatomic, assign) NSInteger number;

@end

@implementation GoodSpeciSectionNumView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (NSInteger)value {
    return self.numberBtn.currentNumber;
}

+ (CGFloat)viewHeight {
    return 60;
}

- (void)configureView:(NSInteger)maxNum {
    self.numberBtn.maxValue = maxNum;
}

- (void)createUI {
    MyFrameLayout *baseLayout = [MyFrameLayout new];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self addSubview:baseLayout];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myBottom = 15;
    layout.myHeight = MyLayoutSize.wrap;
    layout.myLeft = 15;
    layout.myRight = 15;
    [baseLayout addSubview:layout];
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor moBlack];
    lbl.font = [UIFont boldFont15];
    lbl.text = @"数量";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    [layout addSubview:lbl];
    [layout addSubview:[UIView placeholderHorzView]];
    [layout addSubview:self.numberBtn];
}

- (PPNumberButton *)numberBtn {
    if (!_numberBtn) {
        _numberBtn = ({
            PPNumberButton *object = [PPNumberButton numberButtonWithFrame:CGRectZero];
            object.minValue = 1;
            object.increaseTitle = @"＋";
            object.decreaseTitle = @"－";
            object.mySize = CGSizeMake(70, 30);
            object.myCenterY = 0;
            object;
        });
    }
    return _numberBtn;
}

@end
