//
//  OrderInfoGoodsView.m
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "OrderInfoGoodsView.h"
#import "MyOrderGoodInfoView.h"

@interface OrderInfoGoodsView ()

@property (nonatomic, strong) MyBaseLayout *baseLayout;

@property (nonatomic, strong) MyOrderListObj *obj;

@property (nonatomic, strong) MyBaseLayout *freightLayout;

@property (nonatomic, strong) MyBaseLayout *totalLayout;

@end

@implementation OrderInfoGoodsView

- (instancetype)initWithOrderObj:(MyOrderListObj *)obj {
    if (self = [super init]) {
        self.obj = obj;
        [self createUI];
    }
    return self;
}

- (CGFloat)viewHeight {
    [self.baseLayout layoutSubviews];
    return self.baseLayout.height;
}

- (void)configureFreightLayout {
    UILabel *lbl = [self.freightLayout viewWithTag:1];
    if (![lbl isKindOfClass:UILabel.class]) {
        return;
    }
    //邮费
    NSString *string = StrF(@"￥%0.2f", self.obj.postage);
    if (self.obj.postage == 0) {
        string = @"免运费";
    }
    lbl.text = string;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
}

- (void)configureTotalLayout {
    UILabel *lbl = [self.totalLayout viewWithTag:1];
    if (![lbl isKindOfClass:UILabel.class]) {
        return;
    }
    //订单总价
    NSString *string = StrF(@"￥%0.2f", self.obj.total_cash_num);
    lbl.text = string;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
}

- (void)createUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myTop = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self addSubview:layout];
    self.baseLayout = layout;
    [layout addSubview:[self contentLayout]];
}

- (MyBaseLayout *)contentLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    [layout setViewCornerRadius:10];
    layout.myTop = 0;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myHeight = MyLayoutSize.wrap;
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 10;
    subLayout.myTop = 10;
    subLayout.myHeight = 30;
    [layout addSubview:subLayout];
    NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, self.obj.photo);
    UIImageView *imgView = [UIImageView new];
    imgView.backgroundColor = [UIColor moBackground];
    imgView.size = CGSizeMake(30, 30);
    [imgView setCircleView];
    [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    imgView.visibility = MyVisibility_Gone;
    [subLayout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor moBlack];
    lbl.myCenterY = 0;
    lbl.myLeft = 5;
    lbl.text = self.obj.name;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    [subLayout addSubview:lbl];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.myLeft = 10;
    subLayout.myRight = 10;
    subLayout.myTop = 10;
    subLayout.myHeight = MyLayoutSize.wrap;
    subLayout.backgroundColor = [UIColor colorWithHexString:@"#FAFAFC"];
    [subLayout setViewCornerRadius:10];
    [layout addSubview:subLayout];
    for (NSInteger i = 0; i < self.obj.orderList.count; i++) {
        BuyGoodsListObj *tempObj = self.obj.orderList[i];
        if (![tempObj isKindOfClass:BuyGoodsListObj.class]) {
            continue;
        }
        MyOrderGoodInfoView *infoView = [MyOrderGoodInfoView new];
        infoView.layout.backgroundColor = [UIColor colorWithHexString:@"#FAFAFC"];
        infoView.myLeft = 0;
        infoView.size = CGSizeMake((SCREEN_WIDTH - 40), 105);
        infoView.mySize = infoView.size;
        [infoView configureView:tempObj];
        if (i == self.obj.orderList.count - 1) {
            infoView.line.hidden = YES;
        }
        [subLayout addSubview:infoView];
    }
    [subLayout layoutSubviews];
    //商品总价
    MyBaseLayout *factoryLayout = [self.class factoryLayout:@"商品总价"
                                               leftTxtColor:[UIColor colorWithHexString:@"#AAAABB"]
                                                leftTxtFont:[UIFont font13]
                                                 rightTitle:StrF(@"￥%0.2f", self.obj.cash_num)
                                              rightTxtColor:[UIColor moBlack]
                                               rightTxtFont:[UIFont font13]];
    factoryLayout.myTop = 5;
    [layout addSubview:factoryLayout];
    //邮费
    NSString *string = StrF(@"￥%0.2f", self.obj.postage);
    if (self.obj.postage == 0) {
        string = @"免运费";
    }
    self.freightLayout = [self.class factoryLayout:@"物流运费"
                                      leftTxtColor:[UIColor colorWithHexString:@"#AAAABB"]
                                       leftTxtFont:[UIFont font13]
                                        rightTitle:string
                                     rightTxtColor:[UIColor moBlack]
                                      rightTxtFont:[UIFont font13]];
    [layout addSubview:self.freightLayout];
    //会员折扣
    string = StrF(@"-￥%0.2f", self.obj.discount_cash_num);
    if (self.obj.discount_cash_num == 0) {
        string = @"无";
    }
    NSArray *txtArr = @[string];
    NSArray *fontArr = @[[UIFont font13]];
    NSArray *colorArr = @[[UIColor moRed]];
    NSAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                 colors:colorArr
                                                                  fonts:fontArr];
    
    if ([@"04" isEqualToString:self.obj.order_pay_type]) {
        if (self.obj.discount_cash_num != 0 &&
            self.obj.pay_sla_price != 0) {
            txtArr = @[StrF(@"-%0.4f SLA", self.obj.discount_cash_num/self.obj.pay_sla_price), StrF(@"(折合:￥%0.2f)", self.obj.discount_cash_num)];
            fontArr = @[[UIFont font13], [UIFont font11]];
            colorArr = @[[UIColor colorWithHexString:@"#FF4757"], [UIColor colorWithHexString:@"#AAAABB"]];
            att = [NSMutableAttributedString initWithTitles:txtArr
                                                     colors:colorArr
                                                      fonts:fontArr];
        }
    }
            
    [layout addSubview:[self.class factoryLayout:@"会员折扣"
                                    leftTxtColor:[UIColor colorWithHexString:@"#AAAABB"]
                                     leftTxtFont:[UIFont font13]
                                        rightAtt:att]];
    
    if (self.obj.is_makeGroup) {
        txtArr = @[StrF(@"-%0.2f", self.obj.spell_cash_coupon)];
        fontArr = @[[UIFont font13]];
        colorArr = @[[UIColor colorWithHexString:@"#FF4757"]];
        att = [NSMutableAttributedString initWithTitles:txtArr
                                                 colors:colorArr
                                                  fonts:fontArr];
        [layout addSubview:[self.class factoryLayout:@"折扣券"
                                        leftTxtColor:[UIColor colorWithHexString:@"#AAAABB"]
                                         leftTxtFont:[UIFont font13]
                                            rightAtt:att]];
    }
    
    string = StrF(@"￥%0.2f", self.obj.total_cash_num);
    txtArr = @[string];
    fontArr = @[[UIFont font12]];
    colorArr = @[[UIColor moRed]];
    att = [NSMutableAttributedString initWithTitles:txtArr
                                             colors:colorArr
                                              fonts:fontArr];
    if ([@"04" isEqualToString:self.obj.order_pay_type]) {
        if (self.obj.total_cash_num != 0 &&
            self.obj.pay_sla_price != 0) {
            txtArr = @[StrF(@"%0.4f SLA", self.obj.total_cash_num/self.obj.pay_sla_price), StrF(@"(折合:￥%0.2f)", self.obj.total_cash_num)];
            fontArr = @[[UIFont font13], [UIFont font11]];
            colorArr = @[[UIColor colorWithHexString:@"#FF4757"], [UIColor colorWithHexString:@"#AAAABB"]];
            att = [NSMutableAttributedString initWithTitles:txtArr
                                                     colors:colorArr
                                                      fonts:fontArr];
        }
    }
    
    //订单总价
    self.totalLayout = [self.class factoryLayout:@"订单总价"
                                    leftTxtColor:[UIColor colorWithHexString:@"#151419"]
                                     leftTxtFont:[UIFont font13]
                                        rightAtt:att];
    factoryLayout = self.totalLayout;
    factoryLayout.myBottom = 5;
    [layout addSubview:factoryLayout];
    [layout layoutIfNeeded];
    return layout;
}

+ (MyBaseLayout *)factoryLayout:(NSString *)leftTxt
                   leftTxtColor:(UIColor *)leftColor
                    leftTxtFont:(UIFont *)leftFont
                     rightTitle:(NSString *)rightTxt
                  rightTxtColor:(UIColor *)rightColor
                   rightTxtFont:(UIFont *)rightFont {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myTop = 0;
    layout.myHeight = 30;
    UILabel *lbl = [UILabel new];
    lbl.textColor = leftColor;
    lbl.font = leftFont;
    lbl.text = leftTxt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView placeholderHorzView]];
    
    lbl = [UILabel new];
    lbl.textColor = rightColor;
    lbl.font = rightFont;
    lbl.text = rightTxt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.tag = 1;
    [layout addSubview:lbl];
    return layout;
}

+ (MyBaseLayout *)factoryLayout:(NSString *)leftTxt
                   leftTxtColor:(UIColor *)leftColor
                    leftTxtFont:(UIFont *)leftFont
                       rightAtt:(NSAttributedString *)rightAtt {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myTop = 0;
    layout.myHeight = 30;
    UILabel *lbl = [UILabel new];
    lbl.textColor = leftColor;
    lbl.font = leftFont;
    lbl.text = leftTxt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView placeholderHorzView]];
    
    lbl = [UILabel new];
    lbl.attributedText = rightAtt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.tag = 1;
    [layout addSubview:lbl];
    return layout;
}

@end
