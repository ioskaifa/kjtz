//
//  OrderSelectPayView.h
//  BOB
//
//  Created by mac on 2020/9/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderSelectPayView : UIView

@property (nonatomic, strong) UIImageView *closeImgView;

@property (nonatomic, copy) FinishedBlock block;
///只能SLA支付
@property (nonatomic, assign) BOOL only_sla_pay;
///sla 汇率
@property (nonatomic, assign) CGFloat sla_price;

+ (CGFloat)viewHeight;
///提示是否设置支付密码
+ (void)checkSetPayPassword:(FinishedBlock)complete;

- (instancetype)initWithAmount:(CGFloat)amount;

- (instancetype)initWithAmount:(CGFloat)amount withRate:(CGFloat)rate withSlaPrice:(CGFloat)slaPrice noSLAPay:(BOOL)noSLAPay;

@end

NS_ASSUME_NONNULL_END
