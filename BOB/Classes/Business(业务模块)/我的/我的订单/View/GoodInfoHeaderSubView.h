//
//  GoodInfoHeaderSubView.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodInfoHeaderSubView : UIView

- (instancetype)initWithLeftAtt:(NSAttributedString *)leftAtt
                      centerAtt:(NSAttributedString *)centerAtt
                       rightImg:(NSString *)rightImg;

- (CGFloat)viewHeight;

- (void)configureCenterAtt:(NSAttributedString *)att;

@end

NS_ASSUME_NONNULL_END
