//
//  MyOrderListCell.m
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyOrderListCell.h"

@interface MyOrderListCell ()

@end

@implementation MyOrderListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 130;
}

- (void)configureView:(BuyGoodsListObj *)obj {
    if (![obj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    
    [self.goodView configureView:obj];
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    [self.contentView addSubview:self.goodView];
    
    [self.goodView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 10, 0, 10));
    }];
}

- (MyOrderGoodInfoView *)goodView {
    if (!_goodView) {
        _goodView = [MyOrderGoodInfoView new];
    }
    return _goodView;
}

@end
