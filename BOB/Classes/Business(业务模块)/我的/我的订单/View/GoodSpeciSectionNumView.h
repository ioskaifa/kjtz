//
//  GoodSpeciSectionNumView.h
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodSpeciSectionNumView : UICollectionReusableView

@property (nonatomic, assign) NSInteger value;

+ (CGFloat)viewHeight;

- (void)configureView:(NSInteger)maxNum;

@end

NS_ASSUME_NONNULL_END
