//
//  ConfirmOrderAddressView.h
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserAddressListObj.h"
#import "OrderInfoTempObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface ConfirmOrderAddressView : UIView

- (CGFloat)viewHeight;

- (void)configureView:(UserAddressListObj *)obj;

@end

NS_ASSUME_NONNULL_END
