//
//  MyOrderNullView.m
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyOrderNullView.h"

@interface MyOrderNullView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *tipsLbl;

@end

@implementation MyOrderNullView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 230;
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    self.backgroundColor = [UIColor whiteColor];
    
    [layout addSubview:self.imgView];
    [layout addSubview:[UIView placeholderVertView]];
    [layout addSubview:self.tipsLbl];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"我的订单空"];
            object.myCenterX = 0;
            object.myTop = 30;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)tipsLbl {
    if (!_tipsLbl) {
        _tipsLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor colorWithHexString:@"#333333"];
            object.font = [UIFont font14];
            object.text = @"暂无订单";
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myBottom = 30;
            object;
        });
    }
    return _tipsLbl;
}

@end
