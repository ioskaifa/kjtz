//
//  ConfirmGoodInfoView.m
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ConfirmGoodInfoView.h"

@interface ConfirmGoodInfoView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) MyFrameLayout *specsLayout;
@property (nonatomic, strong) UILabel *specsLbl;

@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) UILabel *numLbl;

@end

@implementation ConfirmGoodInfoView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)configureView:(OrderGoodsListItem *)obj {
    if (![obj isKindOfClass:OrderGoodsListItem.class]) {
        return;
    }
    NSString *imgUrl = obj.goods_detail_show;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    self.titleLbl.text = obj.goods_name;
    self.specsLbl.text = obj.character_value;
    self.priceLbl.text = StrF(@"￥%0.2f",  obj.cash);
    [self.priceLbl sizeToFit];
    self.priceLbl.mySize = self.priceLbl.size;
    self.numLbl.text = StrF(@"x%ld", (long)obj.goods_num);
    [self.numLbl sizeToFit];
    self.numLbl.mySize = self.numLbl.size;
    
    CGFloat width = SCREEN_WIDTH - 50 - self.imgView.width - 20;
    [self.titleLbl sizeToFit];
    if (self.titleLbl.width > width) {
        CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
        self.titleLbl.size = size;
    }
    self.titleLbl.mySize = self.titleLbl.size;
    
    CGSize size = [self.specsLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    size = CGSizeMake(size.width + 20, size.height + 10);
    self.specsLayout.mySize = size;
//    self.specsLbl.mySize = self.specsLbl.size;
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:self.imgView];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.myLeft = 10;
    subLayout.myRight = 10;
    subLayout.weight = 1;
    subLayout.myTop = 10;
    subLayout.myBottom = 10;
    [layout addSubview:subLayout];
    
    [subLayout addSubview:self.titleLbl];
    [subLayout addSubview:self.specsLayout];
    [self.specsLayout addSubview:self.specsLbl];
    [subLayout addSubview:[UIView placeholderVertView]];
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myHeight = MyLayoutSize.wrap;
    bottomLayout.myLeft = 0;
    bottomLayout.myRight = 0;
    bottomLayout.myBottom = 0;
    [layout addSubview:bottomLayout];
    [bottomLayout addSubview:self.priceLbl];
    [bottomLayout addSubview:[UIView placeholderHorzView]];
    [bottomLayout addSubview:self.numLbl];
    [subLayout addSubview:bottomLayout];
    
    [self addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(SINGLE_LINE_HEIGHT);
        make.left.mas_equalTo(subLayout.mas_left);
        make.right.mas_equalTo(subLayout.mas_right);
        make.bottom.mas_equalTo(self.mas_bottom);
    }];
    self.layout = layout;
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            [object setViewCornerRadius:5];
            object.myTop = 10;
            object.size = CGSizeMake(100, 100);
            object.mySize = object.size;
            object.myLeft = 10;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font14];
            object.numberOfLines = 0;
            object.textColor = [UIColor colorWithHexString:@"#151419"];
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _titleLbl;
}

- (MyFrameLayout *)specsLayout {
    if (!_specsLayout) {
        _specsLayout = [MyFrameLayout new];
        _specsLayout.backgroundColor = [UIColor colorWithHexString:@"#F5F5FA"];
        [_specsLayout setViewCornerRadius:5];
        _specsLayout.myTop = 5;
        _specsLayout.myLeft = 0;
    }
    return _specsLayout;
}

- (UILabel *)specsLbl {
    if (!_specsLbl) {
        _specsLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font11];
            object.numberOfLines = 0;
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myTop = 5;
            object.myLeft = 5;
            object.myRight = 5;
            object.myBottom = 5;
            object;
        });
    }
    return _specsLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor blackColor];
            object.myCenterY = 0;
            object.myLeft = 0;
            object;
        });
    }
    return _priceLbl;
}

- (UILabel *)numLbl {
    if (!_numLbl) {
        _numLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor moBlack];
            object.myCenterY = 0;
            object.myRight = 0;
            object;
        });
    }
    return _numLbl;
}

- (UIView *)line {
    if (!_line) {
        _line = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor moBackground];
            object;
        });
    }
    return _line;
}

@end
