//
//  OrderInfoAddressView.m
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "OrderInfoAddressView.h"

@interface OrderInfoAddressView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *addressLbl;

@property (nonatomic, strong) UIImageView *arrowImgView;

@end

@implementation OrderInfoAddressView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 100;
}

- (void)configureView:(MyOrderListObj *)obj {
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    self.nameLbl.size = CGSizeZero;
    self.addressLbl.size = CGSizeZero;
    if ([StringUtil isEmpty:obj.userAddress]) {
        [self configureNullView];
    } else {
        self.nameLbl.text = StrF(@"%@ %@", obj.userName, [StringUtil telNumberFormat344:obj.userTel]);
        [self.nameLbl sizeToFit];
        self.nameLbl.mySize = self.nameLbl.size;
        
        self.addressLbl.text = obj.userAddress;
        CGSize size = [self.addressLbl sizeThatFits:CGSizeMake(self.addressLbl.width, MAXFLOAT)];
        self.addressLbl.size = size;
        self.addressLbl.mySize = self.addressLbl.size;
        
        if (obj.orderStatus == OrderStatusToPay) {
            self.arrowImgView.visibility = MyVisibility_Visible;
        } else {
            self.arrowImgView.visibility = MyVisibility_Gone;
        }
    }
}

- (void)configureNullView {
    self.nameLbl.text = @"收货地址";
    [self.nameLbl autoMyLayoutSize];
    
    self.addressLbl.text = @"请选择收货地址";
    [self.addressLbl autoMyLayoutSize];
    
    self.arrowImgView.visibility = MyVisibility_Visible;
}

- (void)createUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myTop = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:[self contentLayout]];
}

- (MyBaseLayout *)contentLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    [layout setViewCornerRadius:10];
    layout.myTop = 0;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    [layout addSubview:self.imgView];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.myLeft = 10;
    subLayout.myRight = 15;
    subLayout.weight = 1;
    subLayout.myHeight = MyLayoutSize.wrap;
    subLayout.myCenterY = 0;
    [layout addSubview:subLayout];
    
    [subLayout addSubview:self.nameLbl];
    [subLayout addSubview:self.addressLbl];
    
    [layout addSubview:self.arrowImgView];
    return layout;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"订单详情地址"];
            object.myCenterY = 0;
            object.myLeft = 15;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor moBlack];
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)addressLbl {
    if (!_addressLbl) {
        _addressLbl = ({
            UILabel *object = [UILabel new];
            object.width = SCREEN_WIDTH - 60 - self.imgView.width - self.arrowImgView.width;
            object.numberOfLines = 0;
            object.myTop = 10;
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#737380"];
            object;
        });
    }
    return _addressLbl;
}

- (UIImageView *)arrowImgView {
    if (!_arrowImgView) {
        _arrowImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"Arrow"];
            object.myCenterY = 0;
            object.myRight = 15;
            object;
        });
    }
    return _arrowImgView;
}

@end
