//
//  GoodSpeciCell.h
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodSpeciObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodSpeciCell : UICollectionViewCell

- (void)configureView:(CharacterListItem *)obj;

@end

NS_ASSUME_NONNULL_END
