//
//  ConfirmOrderAddressView.m
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ConfirmOrderAddressView.h"

@interface ConfirmOrderAddressView ()

@property (nonatomic, strong) MyBaseLayout *baseLayout;
///省市区
@property (nonatomic, strong) UILabel *firstLbl;
///省市区之外的地址
@property (nonatomic, strong) UILabel *centerLbl;
///姓名 电话
@property (nonatomic, strong) UILabel *lastLbl;

@property (nonatomic, strong) UIImageView *leftImgView;

@property (nonatomic, strong) UIImageView *arrowImgView;

@property (nonatomic, strong) UILabel *tipLbl;

@end

@implementation ConfirmOrderAddressView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (CGFloat)viewHeight {
    [self.baseLayout layoutIfNeeded];
    if (self.tipLbl.visibility == MyVisibility_Gone) {
        return self.baseLayout.height + 20;
    } else {
        return self.baseLayout.height + 20 + 30;
    }
}

- (void)configureView:(UserAddressListObj *)obj {
    if (![obj isKindOfClass:UserAddressListObj.class]) {
        self.centerLbl.visibility = MyVisibility_Gone;
        self.lastLbl.visibility = MyVisibility_Gone;
        self.firstLbl.text = @"请选择收货地址";
        [self configureLbl:self.firstLbl];
        [self configureLbl:self.centerLbl];
        [self configureLbl:self.lastLbl];
        self.tipLbl.visibility = MyVisibility_Gone;
        return;
    }
    if ([StringUtil isEmpty:obj.address]) {
        self.centerLbl.visibility = MyVisibility_Gone;
        self.lastLbl.visibility = MyVisibility_Gone;
        self.firstLbl.text = @"请选择收货地址";
    } else {
        self.centerLbl.visibility = MyVisibility_Visible;
        self.lastLbl.visibility = MyVisibility_Visible;
        self.firstLbl.text = obj.address_area;
        self.centerLbl.text = obj.address_des;
        self.lastLbl.text = StrF(@"%@ %@", obj.name, obj.tel);
    }
    [self configureLbl:self.firstLbl];
    [self configureLbl:self.centerLbl];
    [self configureLbl:self.lastLbl];
    if ([StringUtil isEmpty:obj.tips]) {
        self.tipLbl.visibility = MyVisibility_Gone;
    } else {
        self.tipLbl.visibility = MyVisibility_Visible;
        self.tipLbl.text = StrF(@"*%@", obj.tips);
    }
}

- (void)configureLbl:(UILabel *)lbl {
    if (![lbl isKindOfClass:UILabel.class]) {
        return;
    }
    if (lbl.visibility == MyVisibility_Gone) {
        return;
    }
    CGFloat width = SCREEN_WIDTH - 50 - self.leftImgView.width - self.arrowImgView.width;
    CGSize size = [lbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
}

- (void)createUI {
    MyLinearLayout *frameLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    frameLayout.myLeft = 0;
    frameLayout.myRight = 0;
    frameLayout.myTop = 0;
    frameLayout.myBottom = 0;
    
    [self addSubview:frameLayout];
    [frameLayout addSubview:self.baseLayout];
    [self.baseLayout addSubview:self.leftImgView];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myCenterY = 0;
    layout.weight = 1;
    layout.myHeight = MyLayoutSize.wrap;
    
    [layout addSubview:self.firstLbl];
    [layout addSubview:self.centerLbl];
    [layout addSubview:self.lastLbl];
    
    [self.baseLayout addSubview:layout];
    
    [self.baseLayout addSubview:self.arrowImgView];
    
    [frameLayout addSubview:self.tipLbl];
}

- (MyBaseLayout *)baseLayout {
    if (!_baseLayout) {
        _baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
        _baseLayout.myTop = 10;
        _baseLayout.myLeft = 0;
        _baseLayout.myRight = 0;
        _baseLayout.myHeight = MyLayoutSize.wrap;
    }
    return _baseLayout;
}

- (UILabel *)firstLbl {
    if (!_firstLbl) {
        _firstLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor blackColor];
            object.numberOfLines = 0;
            object.myLeft = 0;
            object.myRight = 0;            
            object;
        });
    }
    return _firstLbl;
}

- (UILabel *)centerLbl {
    if (!_centerLbl) {
        _centerLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font13];
            object.textColor = [UIColor moBlack];
            object.numberOfLines = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.myTop = 5;
            object;
        });
    }
    return _centerLbl;
}

- (UILabel *)lastLbl {
    if (!_lastLbl) {
        _lastLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor moBlack];
            object.numberOfLines = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.myTop = 10;
            object;
        });
    }
    return _lastLbl;
}

- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        _leftImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"购物车地址"];
            object.myLeft = 15;
            object.myCenterY = 0;
            object;
        });
    }
    return _leftImgView;
}

- (UIImageView *)arrowImgView {
    if (!_arrowImgView) {
        _arrowImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"Arrow"];
            object.myRight = 15;
            object.myCenterY = 0;
            object;
        });
    }
    return _arrowImgView;
}

- (UILabel *)tipLbl {
    if (!_tipLbl) {
        _tipLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#FF4757"];
            object.myLeft = 10;
            object.myRight = 10;
            object.myTop = 10;
            object.myHeight = 20;
            object.myBottom = 10;
            object;
        });
    }
    return _tipLbl;
}

@end
