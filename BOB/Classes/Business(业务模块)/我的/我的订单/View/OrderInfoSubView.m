//
//  OrderInfoSubView.m
//  BOB
//
//  Created by mac on 2020/9/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "OrderInfoSubView.h"

@interface OrderInfoSubView ()

@property (nonatomic, strong) MyOrderListObj *obj;

@end

@implementation OrderInfoSubView

- (instancetype)initWithOrderObj:(MyOrderListObj *)obj {
    if (self = [super init]) {
        self.obj = obj;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 290;
}

- (void)createUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myTop = 0;
    layout.myBottom = 0;
    
    [self addSubview:layout];
    [layout addSubview:[self contentLayout]];
}

- (MyBaseLayout *)contentLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    [layout setViewCornerRadius:10];
    layout.myTop = 10;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 10;
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 10;
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myHeight = 20;
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor moGreen];
    view.size = CGSizeMake(4, 15);
    view.mySize = view.size;
    view.myCenterY = 0;
    [subLayout addSubview:view];
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    lbl.font = [UIFont font13];
    lbl.text = @"订单信息";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 10;
    [subLayout addSubview:lbl];
    
    [layout addSubview:subLayout];
    
    NSString *string = self.obj.remarks;
    if ([StringUtil isEmpty:string]) {
        string = @"无";
    }
    [layout addSubview:[self.class factoryLayout:@"订单备注:" rightTitle:string]];
    if (self.obj.isFree) {
        [layout addSubview:[self.class factoryLayout:@"是否免单:" rightTitle:self.obj.is_free?@"是":@"否"]];
        if (self.obj.is_free) {
            MyBaseLayout *subLayout = [self.class factoryLayout:@"免单状态:" rightTitle:self.obj.format_free_status];
            UILabel *lbl = [subLayout viewWithTag:1];
            lbl.textColor = [UIColor moGreen];
            [layout addSubview:subLayout];
        }
    }
    [layout addSubview:[self.class factoryLayout:@"支付方式:" rightTitle:self.obj.format_order_pay_type?:@"- -"]];
    [layout addSubview:[self.class factoryLayout:@"订单编号:" rightTitle:self.obj.order_id?:@"- -"]];
    [layout addSubview:[self.class factoryLayout:@"创建时间:" rightTitle:self.obj.formatCreateTime]];
    [layout addSubview:[self.class factoryLayout:@"付款时间:" rightTitle:self.obj.formatPayTime?:@"- -"]];
    [layout addSubview:[self.class factoryLayout:@"成交时间:" rightTitle:self.obj.formatReceiveTime?:@"- -"]];
    if (self.obj.orderStatus == OrderStatusToRefund) {
        [layout addSubview:[self.class factoryLayout:@"退款状态:" rightTitle:self.obj.format_refund_status?:@"- -"]];
    }
    
    return layout;
}

+ (MyBaseLayout *)factoryLayout:(NSString *)leftTxt
                     rightTitle:(NSString *)rightTxt {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 15;
    layout.myRight = 10;
    layout.myTop = 10;
    layout.myHeight = 20;
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#151419"];
    lbl.font = [UIFont font13];
    lbl.text = leftTxt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#151419"];
    lbl.font = [UIFont font13];
    lbl.text = rightTxt;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 30;
    lbl.weight = 1;
    lbl.tag = 1;
    [layout addSubview:lbl];
    return layout;
}

@end
