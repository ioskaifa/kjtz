//
//  GoodInfoHeaderView.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsInfoObj.h"
#import "GoodInfoHeaderSubView.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodInfoHeaderView : UIView

///规格选择
@property (nonatomic, strong) GoodInfoHeaderSubView *specView;

- (instancetype)initWithGoodsInfoObj:(GoodsInfoObj *)obj;

- (CGFloat)viewHeight;
///更新库存
- (void)configureStock;

@end

NS_ASSUME_NONNULL_END
