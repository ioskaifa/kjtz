//
//  ShipperView.h
//  BOB
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShipperInfoObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShipperView : UIView

+ (CGFloat)viewHeight;

- (void)configureView:(TracesInfoObj *)obj;

@end

NS_ASSUME_NONNULL_END
