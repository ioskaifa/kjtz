//
//  GoodsShareView.m
//  BOB
//
//  Created by colin on 2021/1/18.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "GoodsShareView.h"

@interface GoodsShareView ()

@property (nonatomic, strong) GoodsInfoObj *obj;

@property (nonatomic, strong) UIImageView *qrCode;

@property (nonatomic, strong) UIView *contentView;

@end

@implementation GoodsShareView

- (instancetype)initWithObj:(GoodsInfoObj *)obj {
    if (self = [super init]) {
        self.obj = obj;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return SCREEN_HEIGHT;
}

- (void)btnClick:(UIButton *)sender {
    if (![sender isKindOfClass:UIButton.class]) {
        return;
    }
    if ([@"保存图片" isEqualToString:sender.currentTitle]) {
        [self saveImageToPhotosAlbum:self.contentView];
        
    } else {
        [NotifyHelper showMessageWithMakeText:@"暂未开放"];
    }
}

#pragma mark - 保存View到本地相册
- (void)saveImageToPhotosAlbum:(UIView *)view {
    // 设置绘制图片的大小
    UIGraphicsBeginImageContextWithOptions(view.size, NO, 0.0);
    // 绘制图片
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // 保存图片到相册   如果需要获取保存成功的事件第二和第三个参数需要设置响应对象和方法，该方法为固定格式。
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

#pragma mark - Deleaget
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if (!error) {
        [NotifyHelper showMessageWithMakeText:@"保存成功!"];
    }
    [self routerEventWithName:@"MXCustomAlertVCCancel" userInfo:nil];
}

- (void)createUI {
    MyLinearLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    [baseLayout addSubview:[UIView placeholderVertView]];
    self.contentView = [self contentLayout];
    [baseLayout addSubview:self.contentView];
    [baseLayout addSubview:[self bottomLayout]];
}

- (MyBaseLayout *)contentLayout {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myHeight = MyLayoutSize.wrap;
    baseLayout.myLeft = 50;
    baseLayout.myRight = 50;
    baseLayout.myBottom = 20;
    baseLayout.backgroundColor = [UIColor whiteColor];
    [baseLayout setViewCornerRadius:10];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor moBackground];
    layout.myCenterX = 0;
    layout.height = 25;
    layout.myHeight = layout.height;
    layout.myWidth = MyLayoutSize.wrap;
    [layout setViewCornerRadius:layout.height/2.0];
    layout.myTop = 10;
    layout.myBottom = 10;
    [baseLayout addSubview:layout];
    
    UIImageView *imgView = [UIImageView new];
    imgView.size = CGSizeMake(20, 20);
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myLeft = 10;
    [imgView setViewCornerRadius:imgView.height/2.0];
    [imgView sd_setImageWithURL:[NSURL URLWithString:StrF(@"%@/%@", UDetail.user.qiniu_domain, UDetail.user.head_photo)]];
    [layout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor blackColor];
    lbl.text = StrF(@"%@ %@", UDetail.user.nickname?:@"", @" 分享一个好物给您");
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    lbl.myLeft = 10;
    lbl.myRight = 15;
    [layout addSubview:lbl];
    
    imgView = [UIImageView new];
    imgView.size = CGSizeMake(SCREEN_WIDTH - 100, SCREEN_WIDTH - 100);
    imgView.mySize = imgView.size;
    imgView.myTop = 0;
    imgView.myLeft = 0;
    [imgView sd_setImageWithURL:[NSURL URLWithString:self.obj.goods_show]];
    [baseLayout addSubview:imgView];
    
    NSString *disPriceString = StrF(@"%0.2f", self.obj.cash_min);
    NSArray *tempArr = [disPriceString componentsSeparatedByString:@"."];
    NSString *integer = tempArr.firstObject;
    NSString *decimal = StrF(@".%@", tempArr.lastObject);
    NSArray *txtArr = @[@"￥", integer, decimal];
    NSArray *colorArr = @[[UIColor blackColor], [UIColor blackColor], [UIColor blackColor]];
    NSArray *fontArr = @[[UIFont boldFont15], [UIFont boldSystemFontOfSize:30], [UIFont boldFont15]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    lbl = [UILabel new];
    lbl.attributedText = att;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 10;
    lbl.myLeft = 15;
    [baseLayout addSubview:lbl];
    
    //显示折扣价 需要显示市场价
    if (self.obj.underline_cash > 0.0) {
        lbl = [UILabel new];
        txtArr = @[StrF(@"￥%0.2f", self.obj.underline_cash)];
        colorArr = @[[UIColor colorWithHexString:@"#A8A8A8"]];
        fontArr = @[[UIFont font12]];
        att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        //中划线
        NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSRange range = [att.string rangeOfString:att.string];
        [att addAttributes:attribtDic range:range];
        lbl.attributedText = att;
        [lbl sizeToFit];
        lbl.myTop = 5;
        lbl.mySize = lbl.size;
        [baseLayout addSubview:lbl];
    }
    
    lbl = [UILabel new];
    lbl.font = [UIFont font16];
    lbl.textColor = [UIColor colorWithHexString:@"#151419"];
    lbl.numberOfLines = 0;
    NSString *name = self.obj.goods_name;
    if (self.obj.isFree) {
        name = StrF(@"1       %@", name);
    }
    lbl.text = name;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30 - 100, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    lbl.myLeft = 15;
    lbl.myRight = 15;
    lbl.myBottom = 10;
    lbl.myTop = 10;
    [baseLayout addSubview:lbl];
    
    if (self.obj.isFree) {
        UILabel *tipLbl = [UILabel new];
        tipLbl.size = CGSizeMake(34, 18);
        tipLbl.text = @"免单";
        [tipLbl setViewCornerRadius:1];
        tipLbl.backgroundColor = [UIColor colorWithHexString:@"#D3F5F2"];
        tipLbl.textColor = [UIColor colorWithHexString:@"#00A99B"];
        tipLbl.font = [UIFont font11];
        tipLbl.textAlignment = NSTextAlignmentCenter;
        [lbl addSubview:tipLbl];
        [tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(tipLbl.size);
            make.left.mas_equalTo(tipLbl.mas_left);
            make.top.mas_equalTo(tipLbl.mas_top);
        }];
    }
    
    [baseLayout addSubview:self.qrCode];
    lbl = [UILabel new];
    lbl.font = [UIFont font12];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"长按或扫描打开";
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myTop = 5;
    lbl.myBottom = 10;
    [baseLayout addSubview:lbl];
    [baseLayout layoutIfNeeded];
    
    return baseLayout;
}

- (MyLinearLayout *)bottomLayout {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.size = CGSizeMake(SCREEN_WIDTH, 160 + safeAreaInsetBottom());
    baseLayout.mySize = baseLayout.size;
    baseLayout.myLeft = 0;
    baseLayout.myBottom = 0;
    [baseLayout setBorderWithCornerRadius:20 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont font14];
    lbl.text = @"分享当前图片到";
    [lbl autoMyLayoutSize];
    lbl.myTop = 15;
    lbl.myCenterX = 0;
    lbl.myBottom = 15;
    [baseLayout addSubview:lbl];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.weight = 1;
    layout.gravity = MyGravity_Horz_Among;
    //@"QQ好友", @"微信好友", @"朋友圈",
    NSArray *dataArr = @[@"保存图片"];
    for (NSString *string in dataArr) {
        SPButton *btn = [SPButton new];
        btn.imagePosition = SPButtonImagePositionTop;
        btn.imageTitleSpace = 10;
        btn.titleLabel.font = [UIFont font12];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:string] forState:UIControlStateNormal];
        [btn setTitle:string forState:UIControlStateNormal];
        [btn sizeToFit];
        btn.size = CGSizeMake(btn.width + 20, btn.height);
        btn.mySize = btn.size;
        @weakify(self)
        [btn addAction:^(UIButton *btn) {
            @strongify(self)
            [self btnClick:btn];
        }];
        [layout addSubview:btn];
    }
    [baseLayout addSubview:layout];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font16];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"取消";
    [lbl addAction:^(UIView *view) {
        [self routerEventWithName:@"MXCustomAlertVCCancel" userInfo:nil];
    }];
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myBottom = safeAreaInsetBottom() + 10;
    [baseLayout addSubview:lbl];
    return baseLayout;
}

- (UIImageView *)qrCode {
    if (!_qrCode) {
        _qrCode = [UIImageView new];
        CGSize size = CGSizeMake(80, 80);
        UIImage *qrImg = [UIImage encodeQRImageWithContent:UDetail.user.qr_code_url?:@"" size:size];
        _qrCode.image = qrImg;
        _qrCode.size = size;
        _qrCode.mySize = _qrCode.size;
        _qrCode.myCenterX = 0;
        _qrCode.myTop = 0;
        
        UIImageView *imgView = [UIImageView autoLayoutImgView:@"logo_sla"];
        imgView.size = CGSizeMake(15, 15);
        [_qrCode addSubview:imgView];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(imgView.size);
            make.center.mas_equalTo(_qrCode);
        }];
    }
    return _qrCode;
}

@end
