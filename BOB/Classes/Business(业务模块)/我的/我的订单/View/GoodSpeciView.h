//
//  GoodSpeciView.h
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodSpeciObj.h"
#import "GoodSpeciHeaderView.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodSpeciView : UIView

@property (nonatomic, strong) GoodSpeciHeaderView *headerView;

@property (nonatomic, strong) UIImageView *closeImgView;

@property (nonatomic, copy) FinishedBlock block;

+ (CGFloat)viewHeight;

- (instancetype)initWithSpeci:(GoodSpeciObj *)obj;

- (void)reload;

@end

NS_ASSUME_NONNULL_END
