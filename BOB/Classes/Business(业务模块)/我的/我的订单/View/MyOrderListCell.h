//
//  MyOrderListCell.h
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuyGoodsListObj.h"
#import "MyOrderGoodInfoView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyOrderListCell : UITableViewCell

@property (nonatomic, strong) MyOrderGoodInfoView *goodView;

+ (CGFloat)viewHeight;

- (void)configureView:(BuyGoodsListObj *)obj;

@end

NS_ASSUME_NONNULL_END
