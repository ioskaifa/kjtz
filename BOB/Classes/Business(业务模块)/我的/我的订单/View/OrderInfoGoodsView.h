//
//  OrderInfoGoodsView.h
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyOrderListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderInfoGoodsView : UIView

- (instancetype)initWithOrderObj:(MyOrderListObj *)obj;

- (CGFloat)viewHeight;

- (void)configureFreightLayout;

- (void)configureTotalLayout;

@end

NS_ASSUME_NONNULL_END
