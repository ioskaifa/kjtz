//
//  GoodsCollectionAPI.m
//  BOB
//
//  Created by colin on 2021/1/15.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "GoodsCollectionAPI.h"

@implementation GoodsCollectionAPI

+ (void)createUserGoodsCollect:(NSString *)goods_id oper_type:(BOOL)oper_type completion:(RequestResultObjectCallBack)completion {
    if ([StringUtil isEmpty:goods_id]) {
        if (completion) {
            completion(NO,nil, nil);
        }
        return;
    }
    NSString *url = @"api/shop/collect/createUserGoodsCollect";
    NSDictionary *param = @{
        @"goods_id":goods_id,
        @"oper_type":oper_type?@"1":@"0"
    };
    [self request:url param:param completion:^(BOOL success, id object, NSString *error) {
        if (completion) {
            completion(success, object, error);
        }
    }];
}

+ (void)getUserGoodsCollectInfoByGoodsId:(NSString *)goodsId completion:(RequestResultObjectCallBack)completion {
    NSString *url = @"api/shop/collect/getUserGoodsCollectInfoByGoodsId";
    NSDictionary *param = @{
        @"goods_id":goodsId
    };
    [self request:url param:param completion:^(BOOL success, id object, NSString *error) {
        if (completion) {
            completion(success, object, error);
        }
    }];
}

+ (void)getUserGoodsCollectInfoList:(NSString *)mall_type last_id:(NSString *)last_id completion:(RequestResultObjectCallBack)completion {
    NSString *url = @"api/shop/collect/getUserGoodsCollectInfoList";
    NSDictionary *param = @{
        @"mall_type":mall_type,
        @"last_id":last_id
    };
    [self request:url param:param completion:^(BOOL success, id object, NSString *error) {
        if (completion) {
            completion(success, object, error);
        }
    }];
}

+ (void)delUserGoodsCollectGoods:(NSArray *)goodsIds completion:(RequestResultObjectCallBack)completion {
    if (![goodsIds isKindOfClass:NSArray.class]) {
        if (completion) {
            completion(NO,nil, nil);
        }
        return;
    }
    if (goodsIds.count == 0) {
        if (completion) {
            completion(NO,nil, nil);
        }
        return;
    }
    NSString *url = @"api/shop/collect/delUserGoodsCollectGoods";
    NSDictionary *param = @{
        @"collet_id_list":[goodsIds componentsJoinedByString:@","]
    };
    [self request:url param:param completion:^(BOOL success, id object, NSString *error) {
        if (completion) {
            completion(success, object, error);
        }
    }];
}

@end
