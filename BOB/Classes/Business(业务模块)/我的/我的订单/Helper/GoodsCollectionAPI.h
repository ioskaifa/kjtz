//
//  GoodsCollectionAPI.h
//  BOB
//
//  Created by colin on 2021/1/15.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsCollectionAPI : NSObject

///商品收藏和取消收藏接口
// oper_type 0-取消收藏 1-收藏
+ (void)createUserGoodsCollect:(NSString *)goods_id oper_type:(BOOL)oper_type completion:(RequestResultObjectCallBack)completion;

///查询是否收藏该商品接口
+ (void)getUserGoodsCollectInfoByGoodsId:(NSString *)goodsId completion:(RequestResultObjectCallBack)completion;

///查询用户收藏夹列表接口
//mall_type ""全部 01—数字商城 02—免单商城
+ (void)getUserGoodsCollectInfoList:(NSString *)mall_type last_id:(NSString *)last_id completion:(RequestResultObjectCallBack)completion;

///删除收藏夹商品接口
+ (void)delUserGoodsCollectGoods:(NSArray *)goodsIds completion:(RequestResultObjectCallBack)completion;

@end

NS_ASSUME_NONNULL_END
