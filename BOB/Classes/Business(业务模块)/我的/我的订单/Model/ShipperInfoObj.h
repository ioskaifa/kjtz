//
//  ShipperInfoObj.h
//  BOB
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface TracesInfoObj : BaseObject

@property (nonatomic , copy) NSString              * Action;
@property (nonatomic , copy) NSString              * AcceptStation;
@property (nonatomic , copy) NSString              * AcceptTime;
@property (nonatomic , copy) NSString              * Location;
@property (nonatomic , copy) NSMutableAttributedString    * formatContentAtt;
///是否是第一个
@property (nonatomic , assign) BOOL    isFirst;
///是否是最后一个
@property (nonatomic , assign) BOOL    isLast;
@property (nonatomic , assign) CGFloat viewHeight;

@end

///供应链物流
@interface TracesOtherInfoObj : BaseObject

@property (nonatomic , copy) NSString              * AcceptStation;
@property (nonatomic , copy) NSString              * AcceptTime;
@property (nonatomic , copy) NSMutableAttributedString    * formatContentAtt;
///是否是第一个
@property (nonatomic , assign) BOOL    isFirst;
///是否是最后一个
@property (nonatomic , assign) BOOL    isLast;
@property (nonatomic , assign) CGFloat viewHeight;

@end

@interface ShipperInfoObj : BaseObject

@property (nonatomic , copy) NSString              * receiver_city_name;
@property (nonatomic , copy) NSString              * reason;
@property (nonatomic , copy) NSString              * e_business_id;
@property (nonatomic , copy) NSString              * sender_name;
@property (nonatomic , copy) NSString              * remark;
@property (nonatomic , copy) NSString              * sender_mobile;
@property (nonatomic , copy) NSString              * create_by;
@property (nonatomic , copy) NSString              * receiver_province_name;
@property (nonatomic , copy) NSString              * logistic_code;
@property (nonatomic , copy) NSString              * update_time;
@property (nonatomic , copy) NSString              * up_date;
@property (nonatomic , copy) NSString              * estimated_delivery_time;
@property (nonatomic , copy) NSString              * sender_exp_area_name;
@property (nonatomic , copy) NSString              * receiver_name;
@property (nonatomic , copy) NSString              * sender_city_name;
///物流编号
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * shipper_code;
@property (nonatomic , copy) NSString              * state;
@property (nonatomic , copy) NSString              * update_by;
@property (nonatomic , copy) NSString              * receiver_exp_area_name;
@property (nonatomic , copy) NSString              * traces;
@property (nonatomic , strong) NSArray              * tracesArr;
@property (nonatomic , copy) NSString              * sender_province_name;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , copy) NSString              * receiver_mobile;
@property (nonatomic , copy) NSString              * sender_address;
@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , copy) NSString              * state_ex;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * receiver_address;
@property (nonatomic , copy) NSString              * callback;
@property (nonatomic , copy) NSString              * up_time;
@property (nonatomic , copy) NSString              * location;
@property (nonatomic , copy) NSString              * customer_name;
///快递公司名称
@property (nonatomic , copy) NSString              * shipper_name;
@property (nonatomic , copy) NSString              * is_subscribe;
@property (nonatomic , copy) NSString              * order_id;

@end

NS_ASSUME_NONNULL_END
