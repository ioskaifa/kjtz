//
//  GoodsInfoObj.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsInfoObj : BaseObject

///是否是免单商品
@property (nonatomic, assign) BOOL isFree;
///是否直播商品
@property (nonatomic, assign) BOOL isLive;
///是否是自营商品
@property (nonatomic, assign) BOOL isSelfSupport;
///轮播图
@property (nonatomic , copy) NSString              * goods_navigation;
///发货地址
@property (nonatomic , copy) NSString              * send_address;
///商品销量
@property (nonatomic , assign) NSInteger              goods_sales_num;
///联系电话
@property (nonatomic , copy) NSString              * contact_tel;
///商品库存
@property (nonatomic , assign) NSInteger              goods_stock_num;
///删除状态 0—未删除 1—已删除
@property (nonatomic , assign) BOOL del_status;
///商品市场价
@property (nonatomic , assign) CGFloat              goods_market_cash;
///分类编号
@property (nonatomic , copy) NSString              *category_id;
///商品编号
@property (nonatomic , copy) NSString              *ID;
///是否天天特卖状态 0-否  1-是
@property (nonatomic , copy) NSString              * day_sale_status;
///供应商名称
@property (nonatomic , copy) NSString              * supplier_name;
///商品状态 00-待上架 08-已下架 09-已上架
@property (nonatomic , copy) NSString              * goods_status;
///商品名称
@property (nonatomic , copy) NSString              * goods_name;
///是否有好货状态 0-否  1-是
@property (nonatomic , copy) NSString              * have_good_status;
///商品详情图 多张图以英文逗号拼接的
@property (nonatomic , copy) NSString              * goods_describe;
///邮费
@property (nonatomic , assign) CGFloat              express_cash;
///是否聚划算状态 1-否  1-是
@property (nonatomic , copy) NSString              * bargain_status;
///最小金额
@property (nonatomic , assign) CGFloat              cash_min;
///是否品牌购状态 0-否  1-是
@property (nonatomic , copy) NSString              * brand_buy_status;
///是否热销美妆状态 0-否  1-是
@property (nonatomic , copy) NSString              * is_new_product_status;
///01：自营商品 02：供应链商品
@property (nonatomic , copy) NSString              * goods_type;
///商品展示图
@property (nonatomic , copy) NSString              * goods_show;
///供应商logo
@property (nonatomic , copy) NSString              * supplier_logo;
///商品详情图 多张图以英文逗号拼接的
@property (nonatomic , strong) NSArray              * goods_describeArr;
///轮播图
@property (nonatomic , strong) NSArray              * goods_navigationArr;
///SLA支付折扣比例
@property (nonatomic , assign) CGFloat sla_discount_rate;
/// 01数字商城 02免单商城
@property (nonatomic, copy) NSString *mall_type;

/*--------------供应链商品字段--------*/
///供应链商品编号
@property (nonatomic , copy) NSString *chain_goods_id;
///商品卖点
@property (nonatomic , copy) NSString *sellingPoint;
///规格类型1：单规格 2：多规格
@property (nonatomic , copy) NSString *skuType;
///品牌ID
@property (nonatomic , copy) NSString *brandId;
///品牌名称
@property (nonatomic , copy) NSString *brandName;
///商品名称
@property (nonatomic , copy) NSString *name;
///分类ID
@property (nonatomic , copy) NSString *categoryId;
///分类名称
@property (nonatomic , copy) NSString *categoryName;
///分类路径
@property (nonatomic , copy) NSString *categoryIds;
///封面图
@property (nonatomic , copy) NSString *coverImage;
///轮播图
@property (nonatomic , strong) NSArray *photos;
///市场价
@property (nonatomic , assign) CGFloat marketPrice;
///分销价
@property (nonatomic , assign) CGFloat agentPrice;
///详情图
@property (nonatomic , strong) NSArray *detailImages;

@property (nonatomic , strong) NSArray *skuList;
///商品是否收藏
@property (nonatomic, assign) BOOL isCollection;

///商品是否只能SLA支付
@property (nonatomic, assign) BOOL only_sla_pay;
///市场原价、划线价格
@property (nonatomic, assign) CGFloat underline_cash;

@end

NS_ASSUME_NONNULL_END
