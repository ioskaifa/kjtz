//
//  OrderInfoTempObj.h
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"
#import "UserAddressListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderGoodsListItem :BaseObject
///是否是自营商品
@property (nonatomic, assign) BOOL isSelfSupport;
///01：自营商品 02：供应链商品
@property (nonatomic , copy) NSString              * goods_type;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * order_id;
@property (nonatomic , copy) NSString              * goods_id;
@property (nonatomic , copy) NSString              * goods_detail_id;
@property (nonatomic , copy) NSString              * goods_name;
@property (nonatomic , copy) NSString              * goods_detail_show;
@property (nonatomic , copy) NSString              * character_value;
@property (nonatomic , assign) CGFloat             express_cash;
@property (nonatomic , assign) CGFloat             cash;
@property (nonatomic , assign) NSInteger           goods_num;
@property (nonatomic , assign) BOOL   only_sla_pay;

@end

@interface OrderSettleListItem :BaseObject

///是否是自营商品
@property (nonatomic, assign) BOOL isSelfSupport;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * order_id;
@property (nonatomic , copy) NSString              * order_type;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * supplier_id;
@property (nonatomic , copy) NSString              * supplier_logo;
@property (nonatomic , copy) NSString              * supplier_name;
@property (nonatomic , copy) NSString              * name;
@property (nonatomic , copy) NSString              * tel;
@property (nonatomic , copy) NSString              * address;
///省市
@property (nonatomic , copy) NSString              * address_first;
///除省市 之后的地址
@property (nonatomic , copy) NSString              * address_last;
@property (nonatomic , assign) NSInteger           goods_num;
///邮费
@property (nonatomic , assign) CGFloat             express_cash;
///供应链订单运费
@property (nonatomic , assign) CGFloat             freight_fee;
///订单金额
@property (nonatomic , assign) CGFloat              cash_num;
///订单总金额
@property (nonatomic , assign) CGFloat              total_cash_num;
///会员折扣
@property (nonatomic , assign) CGFloat              discount_cash_num;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , strong) NSArray <OrderGoodsListItem *>              * orderGoodsList;

@end

@interface OrderInfoTempObj : BaseObject

@property (nonatomic , strong) NSArray <OrderSettleListItem *> * orderSettleList;
///商品总件数
@property (nonatomic , assign) NSInteger            total_goods_num;
///订单总金额
@property (nonatomic , assign) CGFloat              total_cash_num;
///订单总金额合计 + 自营运费合计 + 供应链运费合计
@property (nonatomic , assign) CGFloat              total_total_cash_num;
///会员折扣
@property (nonatomic , assign) CGFloat              discount_cash_num;
///自营运费合计
@property (nonatomic , assign) CGFloat              total_express_cash;
///供应链运费合计
@property (nonatomic , assign) CGFloat              total_freight_fee;
///自营运费合计 + 供应链运费合计
@property (nonatomic , assign) CGFloat              total_express;
///SLA支付折扣比例
@property (nonatomic , assign) CGFloat              sla_discount_rate;
///是否包含供应链订单  -  需要单独获取运费
@property (nonatomic , assign) BOOL                 isBlend;

@property (nonatomic,  strong) UserAddressListObj *addressObj;
///所有订单号集合
@property (nonatomic, strong) NSArray *order_idArr;
///所有订单ID集合
@property (nonatomic, strong) NSArray *orderIDArr;
///所有供应链订单号集合
@property (nonatomic, strong) NSArray *order_id_other_Arr;

@property (nonatomic , assign) BOOL   only_sla_pay;

- (OrderSettleListItem *)findOrderSettleItemByOrderId:(NSString *)orderId;

@end

NS_ASSUME_NONNULL_END
