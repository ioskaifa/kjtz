//
//  GoodSKUObj.h
//  BOB
//
//  Created by mac on 2020/10/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"
#import "GoodSpeciObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface SpecListItem : BaseObject

@property (nonatomic , copy) NSString              * image;
@property (nonatomic , copy) NSString              * name;
@property (nonatomic , copy) NSString              * value;

- (CharacterListItem *)formatToCharacterListItem;

@end

@interface SkuListItem : BaseObject

@property (nonatomic , assign) CGFloat marketPrice;
@property (nonatomic , copy) NSString              * code;
@property (nonatomic , copy) NSString              * imageUrl;
@property (nonatomic , strong) NSArray <SpecListItem *> * specList;
@property (nonatomic , strong) NSArray <CharacterListItem *> * formatSpecList;
@property (nonatomic , copy) NSString              * weight;
@property (nonatomic , assign) CGFloat             agentPrice;
@property (nonatomic , assign) NSInteger             stock;
@property (nonatomic , copy) NSString              * skuId;
@property (nonatomic , strong) NSArray        *skuKeyArr;
@property (nonatomic , copy) NSString        *describe;

@end

@interface GoodSKUObj : BaseObject

@property (nonatomic , strong) NSArray <SkuListItem *> * specList;

///属性名 - CharacterListItem
@property (nonatomic , strong) NSDictionary *characterListDic;
///当前规格的所有属性, 不重复 属性1 属性2 属性3 属性4 属性5
@property (nonatomic , strong) NSArray <CharacterListItem *> *characterList;

@property (nonatomic , strong) NSArray *characterSectionArr;

//根据 选中 SpecListItem name 集合去遍历对应匹配属性
- (SkuListItem *)findSKUBy:(NSArray *)valueArr;

- (SkuListItem *)findSkuListItemBySkuId:(NSString *)skuId;

@end

NS_ASSUME_NONNULL_END
