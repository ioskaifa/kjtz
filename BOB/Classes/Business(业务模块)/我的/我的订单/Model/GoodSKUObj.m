//
//  GoodSKUObj.m
//  BOB
//
//  Created by mac on 2020/10/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodSKUObj.h"

@implementation SpecListItem

- (CharacterListItem *)formatToCharacterListItem {
    CharacterListItem *item = [CharacterListItem new];
    item.character_name = self.name;
    item.character_value_name = self.value;
    item.character_id = self.value;
    item.character_value_id = self.value;
    return item;
}

@end

@implementation SkuListItem

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"specList" : [SpecListItem class]};
}

- (NSString *)imageUrl {
    if ([StringUtil isEmpty:_imageUrl]) {
        SpecListItem *itme = self.specList.firstObject;
        if ([itme isKindOfClass:SpecListItem.class]) {
            _imageUrl = itme.image;
        }
    }
    return _imageUrl;
}

- (NSArray *)skuKeyArr {
    if (!_skuKeyArr) {
        _skuKeyArr = [self.specList valueForKeyPath:@"@distinctUnionOfObjects.value"];
    }
    return _skuKeyArr;
}

- (NSArray<CharacterListItem *> *)formatSpecList {
    if (!_formatSpecList) {
        NSMutableArray *MArr = [NSMutableArray arrayWithCapacity:self.specList.count];
        for (SpecListItem *item in self.specList) {
            if (![item isKindOfClass:SpecListItem.class]) {
                continue;
            }
            [MArr addObject:[item formatToCharacterListItem]];
        }
        _formatSpecList = MArr;
    }
    return _formatSpecList;
}

- (NSString *)describe {
    if (!_describe) {
        NSMutableArray *MArr = [NSMutableArray array];
        for (SpecListItem *item in self.specList) {
            if (![item isKindOfClass:SpecListItem.class]) {
                continue;
            }
            [MArr addObject:item.value];
        }
        _describe = [MArr componentsJoinedByString:@" "];
    }
    return _describe;
}

@end

@implementation GoodSKUObj

- (NSArray *)characterSectionArr {
    if (!_characterSectionArr) {
        NSArray *charactersArr = self.characterList;
        NSArray *valueArr = [charactersArr valueForKeyPath:@"@distinctUnionOfObjects.character_name"];
        NSMutableArray *MArr = [NSMutableArray arrayWithCapacity:valueArr.count];
        for (NSString *name in valueArr) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"character_name == '%@'", name)];
            NSArray *tempArr = [charactersArr filteredArrayUsingPredicate:predicate];
            if (tempArr.count > 0) {
                [MArr addObject:tempArr];
            }
        }
        _characterSectionArr = MArr;
    }
    return _characterSectionArr;
}

- (NSDictionary *)characterListDic {
    if (!_characterListDic) {
        NSArray *charactersArr = [self.specList valueForKeyPath:@"formatSpecList"];
        NSMutableArray *specListItemMArr = [NSMutableArray array];
        for (NSArray *tempArr in charactersArr) {
            if (![tempArr isKindOfClass:NSArray.class]) {
                continue;
            }
            [specListItemMArr addObjectsFromArray:tempArr];
        }
        NSArray *valueArr = [specListItemMArr valueForKeyPath:@"@distinctUnionOfObjects.character_value_name"];
        NSMutableDictionary *MDic = [NSMutableDictionary dictionaryWithCapacity:valueArr.count];
        for (NSString *name in valueArr) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"character_value_name == '%@'", name)];
            NSArray *tempArr = [specListItemMArr filteredArrayUsingPredicate:predicate];
            if (tempArr.count > 0) {
                [MDic setValue:tempArr.firstObject forKey:name];
            }
        }
        _characterListDic = MDic;
    }
    return _characterListDic;
}

- (NSArray<CharacterListItem *> *)characterList {
    if (!_characterList) {
        _characterList = self.characterListDic.allValues;
    }
    return _characterList;
}

- (SkuListItem *)findSKUBy:(NSArray *)valueArr {
    SkuListItem *findItem = nil;
    for (SkuListItem *itme in self.specList) {
        if (![itme isKindOfClass:SkuListItem.class]) {
            continue;
        }
        BOOL isFind = YES;
        for (NSString *value in valueArr) {
            if (![itme.skuKeyArr containsObject:value]) {
                isFind = NO;
            }
        }
        if (isFind) {
            findItem = itme;
            break;
        }
    }
    return findItem;
}

- (SkuListItem *)findSkuListItemBySkuId:(NSString *)skuId {
    if([StringUtil isEmpty:skuId]) {
        return [self.specList safeObjectAtIndex:0];
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"skuId == '%@'", skuId)];
    NSArray *tempArr = [self.specList filteredArrayUsingPredicate:predicate];    
    return [tempArr firstObject];
}

@end
