//
//  ShipperInfoObj.m
//  BOB
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ShipperInfoObj.h"
#import "MXJsonParser.h"

@implementation TracesInfoObj

- (NSMutableAttributedString *)formatContentAtt {
    if (!_formatContentAtt) {
        NSArray *txtArr = @[self.AcceptStation, StrF(@"\n%@", self.AcceptTime)];
        NSArray *colorArr = @[[UIColor moGreen], [UIColor moGreen]];
        NSArray *fontArr = @[[UIFont boldFont15], [UIFont font12]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        paragraphStyle.lineSpacing = 5; // 调整行间距
//        paragraphStyle.alignment = NSTextAlignmentRight;
        NSRange range = [att.string rangeOfString:att.string];
        [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
        _formatContentAtt = att;
    }
    return _formatContentAtt;
}

@end

@implementation TracesOtherInfoObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"AcceptStation" : @"context",
             @"AcceptTime":@"time"};
}

- (NSMutableAttributedString *)formatContentAtt {
    if (!_formatContentAtt) {
        NSArray *txtArr = @[self.AcceptStation, StrF(@"\n%@", self.AcceptTime)];
        NSArray *colorArr = @[[UIColor moGreen], [UIColor moGreen]];
        NSArray *fontArr = @[[UIFont boldFont15], [UIFont font12]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        paragraphStyle.lineSpacing = 5; // 调整行间距
//        paragraphStyle.alignment = NSTextAlignmentRight;
        NSRange range = [att.string rangeOfString:att.string];
        [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
        _formatContentAtt = att;
    }
    return _formatContentAtt;
}

@end

@interface ShipperInfoObj ()

///计算高度
@property (nonatomic, strong) UILabel *lbl;

@end

@implementation ShipperInfoObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

- (NSArray *)tracesArr {
    if (!_tracesArr) {
        NSArray *tempArr = [MXJsonParser jsonToDictionary:self.traces];
        if ([tempArr isKindOfClass:NSArray.class]) {
            NSMutableArray *MArr = [NSMutableArray arrayWithCapacity:tempArr.count];
            for (NSInteger i = tempArr.count - 1; i >= 0; i--) {
                NSDictionary *dic = tempArr[i];
                if (![dic isKindOfClass:NSDictionary.class]) {
                    continue;
                }
                TracesInfoObj *obj = [TracesInfoObj modelParseWithDict:dic];
                if ([StringUtil isEmpty:obj.AcceptStation] && [StringUtil isEmpty:obj.AcceptTime]) {
                    obj = [TracesOtherInfoObj modelParseWithDict:dic];
                }
                if (obj) {
                    NSString *string = obj.formatContentAtt.string;
                    NSRange range = [obj.formatContentAtt.string rangeOfString:string];
                    if (i == 0) {
                        obj.isLast = YES;
                    } else {
                        obj.isLast = NO;
                    }
                    if (i == tempArr.count - 1) {
                        obj.isFirst = YES;
                        [obj.formatContentAtt addAttribute:NSForegroundColorAttributeName
                                                     value:[UIColor moGreen]
                                                     range:range];
                        [obj.formatContentAtt addAttribute:NSFontAttributeName
                                                     value:[UIFont boldFont15]
                                                     range:range];
                    } else {
                        obj.isFirst = NO;
                        [obj.formatContentAtt addAttribute:NSForegroundColorAttributeName
                                                     value:[UIColor colorWithHexString:@"#7F7F8E"]
                                                     range:range];
                        [obj.formatContentAtt addAttribute:NSFontAttributeName
                                                     value:[UIFont lightFont15]
                                                     range:range];
                    }
                    self.lbl.attributedText = obj.formatContentAtt;
                    CGSize size = [self.lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 70, MAXFLOAT)];
                    obj.viewHeight = size.height + 35;
                    [MArr addObject:obj];
                }
            }
            _tracesArr = MArr;
        }
    }
    return _tracesArr;
}

- (UILabel *)lbl {
    if (!_lbl) {
        _lbl= [UILabel new];
        _lbl.numberOfLines = 0;
    }
    return _lbl;
}

@end
