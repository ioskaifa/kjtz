//
//  GoodsInfoObj.m
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodsInfoObj.h"

@implementation GoodsInfoObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id",
             @"is_new_product_status" : @"new_product_status"};
}

- (BOOL)isSelfSupport {
    if ([@"01" isEqualToString:self.goods_type] || self.isLive) {
        return YES;
    }
    return NO;
}

- (BOOL)isFree {
    if ([@"02" isEqualToString:self.mall_type]) {
        return YES;
    } else {
        return NO;
    }
}

- (NSString *)goods_show {
    if (self.isSelfSupport) {
        return StrF(@"%@/%@", UDetail.user.qiniu_domain, _goods_show);
    } else {
        return self.coverImage;
    }
}

- (NSString *)goods_name {
    if (self.isSelfSupport) {
        return _goods_name;
    } else {
        return self.name;
    }
}

- (NSInteger)goods_stock_num {
    if (self.isSelfSupport) {
        NSInteger num = 0;
        if ([self.skuList isKindOfClass:NSArray.class]) {
            num = [[self.skuList valueForKeyPath:@"@sum.goods_detail_stock_num"] integerValue];
        }
        return num;
    } else {
        NSInteger num = 0;
        if ([self.skuList isKindOfClass:NSArray.class]) {
            num = [[self.skuList valueForKeyPath:@"@sum.stock"] integerValue];
        }
        return num;
    }
}

- (NSArray *)goods_describeArr {
    if (!_goods_describeArr) {
        if (self.isSelfSupport) {
            NSArray *tempArr = [self.goods_describe componentsSeparatedByString:@","];
            NSMutableArray *dataMArr = [NSMutableArray arrayWithCapacity:tempArr.count];
            for (NSString *string in tempArr) {
                NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, string);
                [dataMArr addObject:imgUrl];
            }
            _goods_describeArr = dataMArr;
        } else {
            _goods_describeArr = self.detailImages;
        }
        
    }
    return _goods_describeArr;
}

- (NSArray *)goods_navigationArr {
    if (!_goods_navigationArr) {
        if (self.isSelfSupport) {
            NSArray *tempArr = [self.goods_navigation componentsSeparatedByString:@","];
            NSMutableArray *dataMArr = [NSMutableArray arrayWithCapacity:tempArr.count];
            for (NSString *string in tempArr) {
                NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, string);
                [dataMArr addObject:imgUrl];
            }
            _goods_navigationArr = dataMArr;
        } else {
            _goods_navigationArr = self.photos;
        }
        
    }
    return _goods_navigationArr;
}

- (CGFloat)cash_min {
    if (self.isSelfSupport) {
        return _cash_min;
    } else {
        return self.marketPrice;
    }
}

@end
