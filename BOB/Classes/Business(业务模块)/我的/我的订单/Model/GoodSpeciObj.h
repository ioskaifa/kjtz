//
//  GoodSpeciObj.h
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

typedef NS_ENUM(NSInteger, GoodSpeciCellType) {
    ///普通状态
    GoodSpeciCellTypeNormal    = 0,
    ///选中状态
    GoodSpeciCellTypeSelected  = 1,
    ///不可选状态
    GoodSpeciCellTypeDisEnable = 3,
};

NS_ASSUME_NONNULL_BEGIN

@interface GoodsDetailListItem : BaseObject
///商品明细库存
@property (nonatomic , assign) NSInteger           goods_detail_stock_num;
///商品编号
@property (nonatomic , assign) NSInteger           goods_id;
///当前明细对应属性信息 格式：1-1,2-11
@property (nonatomic , copy) NSString              *character_value;
///商品明细展示图
@property (nonatomic , copy) NSString              *goods_detail_show;
///商品明细编号
@property (nonatomic , copy) NSString              *goods_detail_id;
///商品明细价格
@property (nonatomic , assign) CGFloat             cash;
///组合规格描述
@property (nonatomic , copy) NSString              *goods_detail_describe;


@end

@interface CharacterListItem : BaseObject
///属性值名称
@property (nonatomic , copy) NSString           *character_value_name;
///属性名称
@property (nonatomic , copy) NSString           *character_name;
///属性编号
@property (nonatomic , copy) NSString           *character_id;
///属性值编号
@property (nonatomic , copy) NSString           *character_value_id;
///属性编号 - 属性值编号
@property (nonatomic , copy) NSString           *character_key_value;

@property (nonatomic, assign) GoodSpeciCellType cellType;

@property (nonatomic, assign) CGSize itemSize;

@end

@interface GoodSpeciObj : BaseObject

@property (nonatomic , strong) NSArray <GoodsDetailListItem *>  *goodsDetailList;
@property (nonatomic , strong) NSArray <CharacterListItem *>    *characterList;
///分类好的属性列表 二维数组 便于collectionView的展示
@property (nonatomic , strong) NSArray *characterSectionArr;
///以character_value为key CharacterListItem为value  对goodsDetailList转字典 方便查找
@property (nonatomic , strong) NSDictionary     *characterListDic;
///以character_id-character_value_id 为key CharacterListItem为value  对characterList转字段 方便查找
@property (nonatomic , strong) NSDictionary     *goodsDetailListDic;

- (NSString *)characterValueToDescribe:(NSString *)character_value;

@end

NS_ASSUME_NONNULL_END
