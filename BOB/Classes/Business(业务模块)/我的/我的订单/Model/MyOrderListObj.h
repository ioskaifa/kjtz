//
//  MyOrderListObj.h
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"
#import "BuyGoodsListObj.h"
#import "ShipperInfoObj.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, OrderStatus) {
    ///全部
    OrderStatusToAll      = 0,
    ///待付款
    OrderStatusToPay      = 1,
    ///待发货
    OrderStatusToDelive   = 2,
    ///待收货
    OrderStatusToReceive  = 3,
    ///已取消
    OrderStatusToCancel   = 4,
    ///退款
    OrderStatusToRefund   = 5,
    ///已完成
    OrderStatusToComplete = 6,
    ///已冻结
    OrderStatusToFrozen   = 7,
};

@interface MyOrderListObj : BaseObject

///本单是否拼团订单
@property (nonatomic, assign) BOOL is_makeGroup;
///本单是否被免单
@property (nonatomic, assign) BOOL is_free;
///免单状态 00-未免单 09-已免单
@property (nonatomic, copy) NSString *free_status;
@property (nonatomic, copy) NSString *format_free_status;
///是否是免单订单
@property (nonatomic, assign) BOOL isFree;
///是否直播商品
@property (nonatomic, assign) BOOL isLive;
@property (nonatomic, assign) BOOL isSelfSupport;
///订单编号
@property (nonatomic, copy) NSString *ID;
///订单编号
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, strong) NSArray *orderList;
@property (nonatomic, strong) BuyGoodsListObj *goodObj;
///店铺ID
@property (nonatomic, copy) NSString *supplier_id;
///店铺名称
@property (nonatomic, copy) NSString *name;
///店铺头像
@property (nonatomic, copy) NSString *photo;
///物流编号
@property (nonatomic, copy) NSString *shipper_id;
@property (nonatomic, strong) ShipperInfoObj *shipperObj;
///支付的商户订单号
@property (nonatomic, copy) NSString *out_trade_no;
///订单类型
@property (nonatomic, copy) NSString *order_type;
///会员折扣
@property (nonatomic, assign) CGFloat discount_cash_num;
///商品总数量
@property (nonatomic, assign) NSInteger sumNumber;
///商品总价
@property (nonatomic, assign) CGFloat cash_num;
///订单总价 (商品总价+运费-折扣)
@property (nonatomic, assign) CGFloat total_cash_num;
///自营邮费
@property (nonatomic, assign) CGFloat postage;
@property (nonatomic, assign) CGFloat express_cash;
///供应链运费
@property (nonatomic, assign) CGFloat freight_fee;
///备注
@property (nonatomic, copy) NSString *remarks;
///收货人姓名
@property (nonatomic, copy) NSString *userName;
///收货人电话
@property (nonatomic, copy) NSString *userTel;
///收货人地址
@property (nonatomic, copy) NSString *userAddress;
@property (nonatomic, copy) NSString *address_id;
///订单支付方式 01—支付宝 02—微信 03—余额 04—SLA（新增）
@property (nonatomic, copy) NSString *order_pay_type;
@property (nonatomic, copy) NSString *format_order_pay_type;
///支付人民币 注意：只有支付宝和微信支付才有
@property (nonatomic, assign) CGFloat pay_num;
///支付SLA价格
@property (nonatomic, assign) CGFloat pay_sla_price;
///支付余额 注意：只有余额支付的才有值
@property (nonatomic, assign) CGFloat pay_balance_num;
///订单状态
@property (nonatomic, copy) NSString *status;
///订单状态
@property (nonatomic, assign) OrderStatus orderStatus;

@property (nonatomic, copy) NSString *formatStatus;
///订单详情title
@property (nonatomic, copy) NSString *formatInfoTips;

///创建日期
@property (nonatomic, copy) NSString *cre_date;
///创建时间
@property (nonatomic, copy) NSString *cre_time;
///创建日期
@property (nonatomic, copy) NSString *formatCreateTime;

///支付日期
@property (nonatomic, copy) NSString *pay_date;
///支付时间
@property (nonatomic, copy) NSString *pay_time;
///支付日期
@property (nonatomic, copy) NSString *formatPayTime;

///成交日期
@property (nonatomic, copy) NSString *receive_date;
///成交时间
@property (nonatomic, copy) NSString *receive_time;
///成交日期
@property (nonatomic, copy) NSString *formatReceiveTime;
///退款状态 00-待审核 04-退款中 07-退款失败  08-审核失败  09退款成功
@property (nonatomic, copy) NSString *refund_status;
@property (nonatomic, copy) NSString *format_refund_status;
@property (nonatomic, assign) NSTimeInterval sys_time;

@property (nonatomic, assign) CGFloat sla_discount_rate;
///是否可以退款
@property (nonatomic, assign) BOOL isRefund;

@property (nonatomic, assign) CGFloat spell_cash_coupon;
///是否只能SLA支付
@property (nonatomic, assign) BOOL only_sla_pay;

+ (NSString *)orderStatusToString:(OrderStatus)status;

@end

NS_ASSUME_NONNULL_END
