//
//  GoodSpeciObj.m
//  BOB
//
//  Created by mac on 2020/9/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodSpeciObj.h"

@implementation GoodsDetailListItem

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"goods_detail_id" : @"id"};
}

@end

@implementation CharacterListItem

- (NSString *)character_key_value {
    if (!_character_key_value) {
        _character_key_value = StrF(@"%@-%@", self.character_id, self.character_value_id);
    }
    return _character_key_value;
}

@end

@implementation GoodSpeciObj

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"goodsDetailList" : [GoodsDetailListItem class],
             @"characterList" : [CharacterListItem class]};
}

- (NSArray *)characterSectionArr {
    if (!_characterSectionArr) {
        NSArray *charactersArr = [self.characterList valueForKeyPath:@"character_id"];
        ///去重
        NSSet *set = [NSSet setWithArray:charactersArr];
        charactersArr = set.allObjects;
        ///排序
        charactersArr = [charactersArr sortedArrayUsingSelector:@selector(compare:)];
        NSMutableArray *MArr = [NSMutableArray arrayWithCapacity:charactersArr.count];
        for (NSString *ID in charactersArr) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"character_id == '%@'", ID)];
            NSArray *tempArr = [self.characterList filteredArrayUsingPredicate:predicate];
            [MArr addObject:tempArr];
        }
        _characterSectionArr = MArr;
    }
    return _characterSectionArr;
}

- (NSDictionary *)characterListDic {
    if (!_characterListDic) {
        NSMutableDictionary *tempMDic = [NSMutableDictionary dictionaryWithCapacity:self.characterList.count];
        for (CharacterListItem *item in self.characterList) {
            if (![item isKindOfClass:CharacterListItem.class]) {
                continue;
            }
            [tempMDic setValue:item forKey:StrF(@"%@-%@", item.character_id, item.character_value_id)];
        }
        _characterListDic = tempMDic;
    }
    return _characterListDic;
}

- (NSDictionary *)goodsDetailListDic {
    if (!_goodsDetailListDic) {
        NSMutableDictionary *tempMDic = [NSMutableDictionary dictionaryWithCapacity:self.goodsDetailList.count];
        for (GoodsDetailListItem *item in self.goodsDetailList) {
            if (![item isKindOfClass:GoodsDetailListItem.class]) {
                continue;
            }
            [tempMDic setValue:item forKey:item.character_value];
        }
        _goodsDetailListDic = tempMDic;
    }
    return _goodsDetailListDic;
}

- (NSString *)characterValueToDescribe:(NSString *)character_value {
    if ([StringUtil isEmpty:character_value]) {
        return character_value;
    }
    NSArray *tempArr = [character_value componentsSeparatedByString:@","];
    NSMutableArray *tempMArr = [NSMutableArray arrayWithCapacity:tempArr.count];
    for (NSString *key in tempArr) {
        if (![key isKindOfClass:NSString.class]) {
            continue;
        }
        CharacterListItem *item = self.characterListDic[key];
        if (![item isKindOfClass:CharacterListItem.class]) {
            continue;
        }
        [tempMArr addObject:item.character_value_name];
    }
    return [tempMArr componentsJoinedByString:@" "];
}

@end
