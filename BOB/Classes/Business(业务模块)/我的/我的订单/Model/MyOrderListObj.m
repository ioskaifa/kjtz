//
//  MyOrderListObj.m
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyOrderListObj.h"

@implementation MyOrderListObj

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"orderList" : [BuyGoodsListObj class]};
}

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id",
             @"orderList" : @"orderGoodsList",
             @"name":@"supplier_name",
             @"photo":@"supplier_logo",
             @"sumNumber":@"goods_num",
             @"remarks":@"note",
             @"userName":@"name",
             @"userTel":@"tel",
             @"userAddress":@"address"};
}

- (BOOL)isSelfSupport {
    if ([@"02" isEqualToString:self.order_type]) {
        return NO;
    }
    return YES;
}

- (CGFloat)total_cash_num {
    return self.postage + self.cash_num - self.discount_cash_num - self.spell_cash_coupon;
}

- (CGFloat)postage {
    return self.express_cash + self.freight_fee;
}

- (OrderStatus)orderStatus {
    if ([@"00" isEqualToString:self.status]) {
        return OrderStatusToPay;
    } else if ([@"02" isEqualToString:self.status]) {
        return OrderStatusToDelive;
    } else if ([@"03" isEqualToString:self.status]) {
        return OrderStatusToReceive;
    } else if ([@"04" isEqualToString:self.status]) {
        return OrderStatusToCancel;
    } else if ([@"05" isEqualToString:self.status]) {
        return OrderStatusToRefund;
    } else if ([@"08" isEqualToString:self.status]) {
        return OrderStatusToFrozen;
    } else if ([@"09" isEqualToString:self.status]) {
        return OrderStatusToComplete;
    } else {
        return OrderStatusToCancel;
    }
}

+ (NSString *)orderStatusToString:(OrderStatus)status {
    NSString *string = @"";
    switch (status) {
        case OrderStatusToAll:{
            string = @"";
            break;
        }
        case OrderStatusToPay:{
            string = @"00";
            break;
        }
        case OrderStatusToDelive:{
            string = @"02";
            break;
        }
        case OrderStatusToReceive:{
            string = @"03";
            break;
        }
        case OrderStatusToComplete:{
            string = @"09";
            break;
        }
        case OrderStatusToCancel:{
            string = @"04";
            break;
        }
        case OrderStatusToRefund:{
            string = @"05";
            break;
        }
        default:
            string = @"";
            break;
    }
    return string;
}

- (NSString *)formatStatus {
    NSString *string = @"";
    switch (self.orderStatus) {
        case OrderStatusToDelive:{
            string = @"待发货";
            break;
        }
        case OrderStatusToPay:{
            string = @"待付款";
            break;
        }
        case OrderStatusToReceive:{
            string = @"待收货";
            break;
        }
        case OrderStatusToComplete:{
            string = @"已完成";
            break;
        }
        case OrderStatusToCancel:{
            string = @"已取消";
            break;
        }
        case OrderStatusToFrozen:{
            string = @"已冻结";
            break;
        }
        case OrderStatusToRefund:{
            string = self.format_refund_status;
            break;
        }
        default:
            string = @"已取消";
            break;
    }
    return string;
}

- (NSString *)formatInfoTips {
    NSString *string = @"";
    switch (self.orderStatus) {
        case OrderStatusToDelive:{
            string = @"等待卖家发货";
            break;
        }
        case OrderStatusToPay:{
            string = @"等待买家付款";
            break;
        }
        case OrderStatusToReceive:{
            string = @"卖家已发货";
            break;
        }
        case OrderStatusToComplete:{
            string = @"订单已完成";
            break;
        }
        case OrderStatusToCancel:{
            string = @"订单已取消";
            break;
        }
        case OrderStatusToFrozen:{
            string = @"订单已冻结";
            break;
        }
        case OrderStatusToRefund:{
            string = self.format_refund_status;
            break;
        }
        default:
            string = @"订单已取消";
            break;
    }
    return string;
}

- (NSString *)formatCreateTime {
    if (!_formatCreateTime) {
        _formatCreateTime = StrF(@"%@ %@",
                                 [StringUtil formatDayString:self.cre_date unit:@"-"],
                                 [StringUtil formatTimeStringmm:self.cre_time]);
    }
    return _formatCreateTime;
}

- (NSString *)formatPayTime {
    if (!_formatPayTime) {
        if ([StringUtil isEmpty:self.pay_date]) {
            return @"- -";
        }
        _formatPayTime = StrF(@"%@ %@",
                              [StringUtil formatDayString:self.pay_date unit:@"-"],
                              [StringUtil formatTimeStringmm:self.pay_time]);
    }
    return _formatPayTime;
}

- (NSString *)formatReceiveTime {
    if (!_formatReceiveTime) {
        if ([StringUtil isEmpty:self.receive_date]) {
            return @"- -";
        }
        _formatReceiveTime = StrF(@"%@ %@",
                                  [StringUtil formatDayString:self.receive_date unit:@"-"],
                                  [StringUtil formatTimeStringmm:self.receive_time]);
    }
    return _formatReceiveTime;
}

- (BuyGoodsListObj *)goodObj {
    if (!_goodObj) {
        _goodObj = self.orderList.firstObject;
    }
    return _goodObj;
}

- (NSString *)format_order_pay_type {
    NSString *string = @"- -";
    if ([@"01" isEqualToString:self.order_pay_type]) {
        string = @"支付宝";
    } else if ([@"02" isEqualToString:self.order_pay_type]) {
        string = @"微信";
    } else if ([@"03" isEqualToString:self.order_pay_type]) {
        string = @"余额";
    } else if ([@"04" isEqualToString:self.order_pay_type]) {
        string = @"SLA";
    }
    
    return string;
}

- (NSString *)name {
    if (self.isFree) {
        return @"免单商城";
    } else {
        return @"数字商城";
    }
}

- (NSString *)format_free_status {
    NSString *string = @"";
    if ([@"00" isEqualToString:self.free_status]) {
        string = @"未免单";
    } else if ([@"09" isEqualToString:self.free_status]) {
        string = @"已免单";
    }
    
    return string;
}

- (NSString *)format_refund_status {
    if (self.orderStatus != OrderStatusToRefund) {
        return @"- -";
    }
    NSString *string = @"- -";
    if ([@"00" isEqualToString:self.refund_status]) {
        string = @"退款待审核";
    } else if ([@"04" isEqualToString:self.refund_status]) {
        string = @"退款中";
    } else if ([@"07" isEqualToString:self.refund_status]) {
        string = @"退款失败";
    } else if ([@"08" isEqualToString:self.refund_status]) {
        string = @"退款拒绝";
    } else if ([@"09" isEqualToString:self.refund_status]) {
        string = @"退款成功";
    }
    return string;
}

///退款状态 00 07 08 && 订单状态代发货
- (BOOL)isRefund {
    if (self.orderStatus == OrderStatusToDelive) {
        return YES;
    }
    BOOL refundStatus = NO;
    if ([@"00" isEqualToString:self.refund_status] ||
        [@"07" isEqualToString:self.refund_status] ||
        [@"08" isEqualToString:self.refund_status]) {
        refundStatus = YES;
    }
    return (refundStatus && (self.orderStatus == OrderStatusToRefund));
}

@end
