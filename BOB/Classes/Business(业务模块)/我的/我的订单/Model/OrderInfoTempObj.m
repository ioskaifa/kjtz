//
//  OrderInfoTempObj.m
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "OrderInfoTempObj.h"

@implementation OrderGoodsListItem

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

- (BOOL)isSelfSupport {
    if ([@"01" isEqualToString:self.goods_type]) {
        return YES;
    }
    return NO;
}

- (NSString *)goods_detail_show {
    if (self.isSelfSupport) {
        return StrF(@"%@/%@", UDetail.user.qiniu_domain, _goods_detail_show);
    } else {
        return _goods_detail_show;;
    }
}

@end

@implementation OrderSettleListItem

- (BOOL)isSelfSupport {
    if ([@"01" isEqualToString:self.order_type]) {
        return YES;
    }
    return NO;
}

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"orderGoodsList" : [OrderGoodsListItem class]};
}

- (NSString *)address_first {
    if (!_address_first) {
        NSArray *tempArr = [self.address componentsSeparatedByString:@"·"];
        NSMutableArray *tempMArr = [NSMutableArray arrayWithArray:tempArr];
        [tempMArr removeObject:tempMArr.lastObject];
        _address_first = [tempMArr componentsJoinedByString:@" "];
    }
    return _address_first;
}

- (NSString *)address_last {
    if (!_address_last) {
        NSArray *tempArr = [self.address componentsSeparatedByString:@"·"];
        _address_last = tempArr.lastObject?:@"";
    }
    return _address_last;
}

@end

@implementation OrderInfoTempObj

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"orderSettleList" : [OrderSettleListItem class]};
}

- (NSInteger)total_goods_num {
    return [[self.orderSettleList valueForKeyPath:@"@sum.goods_num"] integerValue];
}

- (CGFloat)total_cash_num {
    return [[self.orderSettleList valueForKeyPath:@"@sum.cash_num"] floatValue];
}

- (CGFloat)discount_cash_num {
    return [[self.orderSettleList valueForKeyPath:@"@sum.discount_cash_num"] floatValue];
}

- (CGFloat)total_express_cash {
    return [[self.orderSettleList valueForKeyPath:@"@sum.express_cash"] floatValue];
}

- (CGFloat)total_freight_fee {
    return [[self.orderSettleList valueForKeyPath:@"@sum.freight_fee"] floatValue];
}

- (CGFloat)total_total_cash_num {
    return self.total_cash_num + self.total_freight_fee + self.total_express_cash;
}

- (CGFloat)total_express {
    return self.total_express_cash + self.total_freight_fee;;
}

- (BOOL)isBlend {
    BOOL find = NO;
    for (OrderSettleListItem *item in self.orderSettleList) {
        if (![item isKindOfClass:OrderSettleListItem.class]) {
            continue;
        }
        if (!item.isSelfSupport) {
            find = YES;
            break;
        }
    }
    return find;
}

- (NSArray *)order_idArr {
    if (!_order_idArr) {
        _order_idArr = [self.orderSettleList valueForKeyPath:@"@distinctUnionOfObjects.order_id"];
    }
    return _order_idArr;
}

- (NSArray *)orderIDArr {
    if (!_orderIDArr) {
        _orderIDArr = [self.orderSettleList valueForKeyPath:@"@distinctUnionOfObjects.ID"];
    }
    return _orderIDArr;
}

- (NSArray *)order_id_other_Arr {
    if (!_order_id_other_Arr) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"order_type == '02'"];
        NSArray *tempArr = [self.orderSettleList filteredArrayUsingPredicate:predicate];
        _order_id_other_Arr = [tempArr valueForKeyPath:@"@distinctUnionOfObjects.order_id"];
    }
    return _order_id_other_Arr;
}

- (OrderSettleListItem *)findOrderSettleItemByOrderId:(NSString *)orderId {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"order_id == '%@'", orderId)];
    NSArray *tempArr = [self.orderSettleList filteredArrayUsingPredicate:predicate];
    return tempArr.firstObject;
}

@end
