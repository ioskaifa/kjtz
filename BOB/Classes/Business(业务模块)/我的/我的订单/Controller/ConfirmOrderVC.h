//
//  ConfirmOrderVC.h
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, ConfirmOrderType) {
    ///来自商品详情立即购买
    ConfirmOrderTypeFromGoodInfoVC         = 0,
    ///来自购物车结算
    ConfirmOrderTypeFromShoppingCartVC     = 1,
};

NS_ASSUME_NONNULL_BEGIN

@interface ConfirmOrderVC : BaseViewController
///是否免单商品
@property (nonatomic, assign) BOOL isFree;
///是否直播商品
@property (nonatomic, assign) BOOL isLive;

@property (nonatomic, copy) NSString *order_id;
///订单来源
@property (nonatomic, assign) ConfirmOrderType fromType;

@end

NS_ASSUME_NONNULL_END
