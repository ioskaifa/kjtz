//
//  OrderInfoVC.h
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "MyOrderListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderInfoVC : BaseViewController

@property (nonatomic, strong) NSString *orderID;

@property (nonatomic, strong) MyOrderListObj *obj;
///是否免单商品
@property (nonatomic, assign) BOOL isFree;
///是否直播商品
@property (nonatomic, assign) BOOL isLive;

@end

NS_ASSUME_NONNULL_END
