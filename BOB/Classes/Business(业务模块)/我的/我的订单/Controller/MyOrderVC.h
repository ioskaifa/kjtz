//
//  MyOrderVC.h
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "MyOrderListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyOrderVC : BaseViewController
///是否免单
@property (nonatomic, assign) BOOL isFree;
///是否直播
@property (nonatomic, assign) BOOL isLive;

@property (nonatomic, assign) OrderStatus orderStatus;

@end

NS_ASSUME_NONNULL_END
