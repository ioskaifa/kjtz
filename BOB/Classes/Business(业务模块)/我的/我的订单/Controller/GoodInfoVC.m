//
//  GoodInfoVC.m
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodInfoVC.h"
#import "XHWebImageAutoSize.h"
#import "GoodsInfoObj.h"
#import "GoodSpeciObj.h"
///供应链的商品规格
#import "GoodSKUObj.h"
#import "GoodInfoHeaderView.h"
#import "GoodInfoBottomView.h"
#import "GoodSpeciView.h"
#import "GoodOtherSpeciView.h"
#import "HWPanModelVC.h"
#import "BuyFooterView.h"
#import "MyServiceView.h"
#import "GoodsCollectionAPI.h"
#import "GoodsShareView.h"
#import "ManageAuthUserListObj.h"
#import "MXChatManager.h"
#import "LcwlChat.h"

@interface GoodInfoVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) GoodInfoHeaderView *headerView;

@property (nonatomic, strong) GoodInfoBottomView *bottomView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) GoodsInfoObj *goodInfoObj;

@property (nonatomic, strong) GoodSpeciObj *goodSpeciObj;

@property (nonatomic, strong) GoodSKUObj *goodSKUObj;

@property (nonatomic, strong) UIImageView *backImgView;

@property (nonatomic, strong) UIImageView *homeImgView;

@property (nonatomic, strong) GoodSpeciView *speciView;

@property (nonatomic, strong) GoodOtherSpeciView *otherSpeciView;

@property (nonatomic, strong) HWPanModelVC *panVC;
///计算属性大小
@property (nonatomic, strong) UILabel *staticValueLbl;

///加入购物车  商品详情编号 带规格属性
@property (nonatomic, copy) NSString *goods_detail_id;
///加入购物车  商品数量
@property (nonatomic, copy) NSString *goods_num;

@property (nonatomic, strong) BuyFooterView *footerView;

@property (nonatomic, strong) SPButton *notExistBtn;

@end

@implementation GoodInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
    [self fetchCartNum];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self.view bringSubviewToFront:self.backImgView];
    [self.view bringSubviewToFront:self.homeImgView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)fetchData {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    NSString *api = @"api/shop/goods/getGoodsInfo";
    if (self.isFree) {
        api = @"api/freeshop/goods/getGoodsInfo";
    }
    if (self.isLive) {
        api = @"api/live/userLiveGoods/getLiveGoodsInfo";
    }
    [self request:api
            param:@{@"goods_id":self.goods_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideAllHUDsForView:self.view animated:YES];
        if (success) {
            self.goodInfoObj = [GoodsInfoObj modelParseWithDict:object[@"data"][@"goodsInfo"]];
            self.goodInfoObj.isFree = self.isFree;
            self.goodInfoObj.isLive = self.isLive;
            NSArray *arr = [SkuListItem modelListParseWithArray:object[@"data"][@"goodsInfo"][@"skuList"]];
            self.goodSKUObj.specList = arr;
            self.goodInfoObj.skuList = arr;
            for (CharacterListItem *itme in self.goodSKUObj.characterList) {
                if (![itme isKindOfClass:CharacterListItem.class]) {
                    continue;
                }
                self.staticValueLbl.size = CGSizeZero;
                self.staticValueLbl.text = itme.character_value_name;
                [self.staticValueLbl sizeToFit];
                CGSize size = self.staticValueLbl.size;
                itme.itemSize = CGSizeMake(size.width + 20, size.height + 10);
            }
            if (![self.goodInfoObj isKindOfClass:GoodsInfoObj.class]) {
                return;
            }
            [self initUI];
            [self.view bringSubviewToFront:self.backImgView];
            [self.view bringSubviewToFront:self.homeImgView];
            if (self.goodInfoObj.isSelfSupport) {
                [self fetchGoodsDetail];
            }
            [self fetchFooterViewData];
            if (self.goodInfoObj.del_status) {
                [self configureGoodsNotExist];
            } else if (![@"09" isEqualToString:self.goodInfoObj.goods_status]) {
                [self configureGoodsOffShelves];
            }
        }
    }];
        
}

- (void)fetchGoodsDetail {
    NSString *api = @"api/shop/goods/getGoodsDetail";
    if (self.isFree) {
        api = @"api/freeshop/goods/getGoodsDetail";
    }
    if (self.isLive) {
        api = @"api/live/userLiveGoods/getLiveGoodsDetail";
    }
    [self request:api
            param:@{@"goods_id":self.goods_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            self.goodSpeciObj = [GoodSpeciObj modelParseWithDict:object[@"data"]];
            self.goodInfoObj.skuList = self.goodSpeciObj.goodsDetailList;
            [self.headerView configureStock];
            for (CharacterListItem *itme in self.goodSpeciObj.characterList) {
                if (![itme isKindOfClass:CharacterListItem.class]) {
                    continue;
                }
                self.staticValueLbl.size = CGSizeZero;
                self.staticValueLbl.text = itme.character_value_name;
                [self.staticValueLbl sizeToFit];
                CGSize size = self.staticValueLbl.size;
                itme.itemSize = CGSizeMake(size.width + 20, size.height + 10);
            }
        }
    }];
}

#pragma mark - 获取为您推荐
- (void)fetchFooterViewData {
    [self.footerView fetchLastDataByCategory:self.goodInfoObj.category_id complete:^(CGFloat height) {
        if (self.footerView.height == height) {
            return;
        }
        self.footerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
        self.tableView.tableFooterView = self.footerView;
    }];
}

#pragma mark - 去购物车
- (void)gotoCart {
    NSString *clsString = @"ShoppingCartVC";
    if (self.isFree) {
        clsString = @"FreeShoppingCartVC";
    }
    NSArray *navArr = self.navigationController.viewControllers;
    //如果导航栏中有购物车直接返回, 没有的话跳转到购物车tab
    BaseViewController *findVC = nil;
    for (NSInteger i = navArr.count - 1; i >= 0; i--) {
        BaseViewController *vc = navArr[i];
        if (![vc isKindOfClass:BaseViewController.class]) {
            continue;
        }
        if ([vc isKindOfClass:NSClassFromString(clsString)]) {
            findVC = vc;
            break;
        }        
    }
    if (findVC) {
        [self.navigationController popToViewController:findVC animated:YES];
        return;
    }
    if (self.isFree) {
        MXRoute(@"FreeShoppingCartVC", nil)
    } else {
        MXRoute(@"ShoppingCartVC", nil)
    }    
}

#pragma mark - 添加购物车
- (void)joinCart {
    if ([StringUtil isEmpty:self.goods_detail_id]) {
        return;
    }
    NSString *api = @"api/shop/cart/createUserGoodsCart";
    if (self.isFree) {
        api = @"api/freeshop/cart/createUserGoodsCart";
    }
    [self request:api
            param:@{@"goods_detail_id":self.goods_detail_id?:@"",
                    @"goods_num":self.goods_num?:@"1",
                    @"goods_type":self.goodInfoObj.goods_type,
                    @"goods_id":self.goodInfoObj.ID}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            [NotifyHelper showMessageWithMakeText:@"加入购物车成功!"];
            [self fetchCartNum];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark - 立即购买
- (void)buyGood {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    NSString *api = @"api/shop/order/createTempOrder";
    if (self.isFree) {
        api = @"api/freeshop/order/createTempOrder";
    }
    if (self.isLive) {
        api = @"api/live/liveOrder/createLiveTempOrder";
    }
    [self request:api
            param:@{@"goods_detail_id":self.goods_detail_id?:@"",
                    @"goods_num":self.goods_num?:@"1",
                    @"goods_type":self.goodInfoObj.goods_type,
                    @"goods_id":self.goodInfoObj.ID,
                    @"live_id":self.live_id?:@"",
            }
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            NSString *orderId = object[@"data"][@"order_id"];
            if (self.isFree) {
                MXRoute(@"ConfirmOrderVC", (@{@"order_id":orderId?:@"", @"fromType":@(0), @"isFree":@(1)}))
            } else if (self.isLive) {
                MXRoute(@"ConfirmOrderVC", (@{@"order_id":orderId?:@"", @"fromType":@(0), @"isLive":@(1)}))
            } else {
                MXRoute(@"ConfirmOrderVC", (@{@"order_id":orderId?:@"", @"fromType":@(0)}))
            }
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark - 获取购物车数量
- (void)fetchCartNum {
    NSString *api = @"api/shop/cart/getUserCartTotalNum";
    if (self.isFree) {
        api = @"api/freeshop/cart/getUserCartTotalNum";
    }
    [self request:api
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSInteger cart_total_num = [object[@"data"][@"cart_total_num"] integerValue];
            [self.bottomView configureView:cart_total_num];
        }
    }];
}

- (void)configureSpeiView:(NSDictionary *)dataDic {
    NSString *txt = @"请选择规格";
    if ([dataDic isKindOfClass:NSDictionary.class]) {
        NSString *character_value = dataDic[@"character_value"]?:@"";
        self.goods_num = [dataDic[@"goods_num"]?:@"" description];
        txt = [self.goodSpeciObj characterValueToDescribe:character_value];
        GoodsDetailListItem *item = self.goodSpeciObj.goodsDetailListDic[character_value];
        if ([item isKindOfClass:GoodsDetailListItem.class]) {
            self.goods_detail_id = item.goods_detail_id;
        }
    }
    NSArray *txtArr = @[txt];
    NSArray *colorArr = @[[UIColor blackColor]];
    NSArray *fontArr = @[[UIFont font13]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                        colors:colorArr
                                                                         fonts:fontArr];
    [self.headerView.specView configureCenterAtt:att];
    [self.tableView beginUpdates];
    self.headerView.height = [self.headerView viewHeight];
    [self.tableView endUpdates];
}

- (void)configureOtherSpeiView:(NSDictionary *)dataDic {
    NSString *txt = @"请选择规格";
    if ([dataDic isKindOfClass:NSDictionary.class]) {
        NSString *character_value = dataDic[@"character_value"]?:@"";
        self.goods_num = [dataDic[@"goods_num"]?:@"" description];
        SkuListItem *item = [self.goodSKUObj findSkuListItemBySkuId:character_value];
        if ([item isKindOfClass:SkuListItem.class]) {
            self.goods_detail_id = item.skuId;
            txt = item.describe;
        }
    }
    NSArray *txtArr = @[txt];
    NSArray *colorArr = @[[UIColor blackColor]];
    NSArray *fontArr = @[[UIFont font13]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                        colors:colorArr
                                                                         fonts:fontArr];
    [self.headerView.specView configureCenterAtt:att];
    [self.tableView beginUpdates];
    self.headerView.height = [self.headerView viewHeight];
    [self.tableView endUpdates];
}

///商品已下架
- (void)configureGoodsOffShelves {
    self.bottomView.joinCartBtn.visibility = MyVisibility_Gone;
    self.bottomView.buyBtn.myWidth = self.bottomView.buyBtn.width * 2;
    [self.bottomView.buyBtn setTitle:@"该商品已经下架啦~" forState:UIControlStateNormal];
    self.bottomView.buyBtn.backgroundColor = [UIColor colorWithHexString:@"#666666"];
    self.bottomView.buyBtn.userInteractionEnabled = NO;
}

///商品不存在
- (void)configureGoodsNotExist {
    self.tableView.hidden = YES;
    self.bottomView.hidden = YES;
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.goodInfoObj.goods_describeArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *urlArr = self.goodInfoObj.goods_describeArr;
    if (indexPath.row >= urlArr.count) {
        return 0.01;
    }
    NSString *imgUrl = urlArr[indexPath.row];
    return [XHWebImageAutoSize imageHeightForURL:[NSURL URLWithString:imgUrl] layoutWidth:SCREEN_WIDTH estimateHeight:0.01];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class cls = UITableViewCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    UIImageView *imgView = [cell.contentView viewWithTag:1000];
    if (!imgView) {
        imgView = [UIImageView new];
        imgView.tag = 1000;
        [cell.contentView addSubview:imgView];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
           make.edges.equalTo(@(0));
        }];
    }
    NSArray *urls = self.goodInfoObj.goods_describeArr;
    if (indexPath.row < self.goodInfoObj.goods_describeArr.count) {
        NSString *imageURL = [urls safeObjectAtIndex:indexPath.row];
        //加载网络图片使用SDWebImage
        [imgView sd_setImageWithURL:[NSURL URLWithString:imageURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            /** 缓存image size */
            [XHWebImageAutoSize storeImageSize:image forURL:imageURL completed:^(BOOL result) {
                /** reload  */
                if(result) {
                    [tableView xh_reloadDataForURL:imageURL];
                };
            }];
        }];
    }
    return cell;
}

- (void)createUI {
    [self.view addSubview:self.backImgView];
    self.view.backgroundColor = [UIColor moBackground];
    [self.view addSubview:self.homeImgView];
}

- (void)initUI {
    self.tableView.tableHeaderView = self.headerView;
    [self.view addSubview:self.notExistBtn];
    [self.notExistBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.notExistBtn.size);
        make.centerX.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view.mas_top).mas_offset(150 + safeAreaInsetBottom());
    }];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, self.bottomView.height, 0));
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.bottomView.size);
        make.left.right.bottom.mas_equalTo(self.view);
    }];
}

///获取客服
- (void)getMemberServiceList {
    NSString *url = @"api/user/info/getMemberServiceList";
    url = StrF(@"%@/%@", LcwlServerRoot2, url);
    NSDictionary *param = @{};
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url);
        net.params(param);
        net.finish(^(id data){
            [NotifyHelper hideHUDForView:self.view animated:YES];
            
            if ([data isSuccess]) {
                NSArray *dataArr = data[@"data"][@"serviceList"];
                dataArr = [ManageAuthUserListObj modelListParseWithArray:dataArr];
                NSArray *tempArr = [dataArr valueForKeyPath:@"@unionOfObjects.ID"];
                [MXCache setValue:tempArr forKey:kMXMemberServiceList];
                
                NSArray *fried = [[LcwlChat shareInstance].chatManager friends];
                ///未保存在本地的用户
                NSMutableArray *notFriendMArr = [NSMutableArray array];
                ManageAuthUserListObj *obj = [dataArr firstObject];
                
                if (![obj isKindOfClass:ManageAuthUserListObj.class]) {
                    return;
                }
                //如果是自己则不显示
                if ([obj.ID isEqualToString:UDetail.user.user_id]) {
                    return;
                }
                FriendModel *model = [FriendModel new];
                model.userid = obj.ID;
                model.name = obj.nick_name;
                model.user_name = obj.nick_name;
                model.nick_name = obj.nick_name;
                model.smartName = obj.nick_name;
                model.head_photo = obj.head_photo;
                model.avatar = obj.head_photo;
                
                //判断是否本地好友  不是本地好友 写入本地
                NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"userid == '%@'", obj.ID)];
                NSArray *temp2Arr = [fried filteredArrayUsingPredicate:predicate];
                if (temp2Arr.count > 0) {
                    //把备注带上
                    FriendModel *tempObj = temp2Arr.firstObject;
                    if ([tempObj isKindOfClass:FriendModel.class]) {
                        model.remark = tempObj.remark;
                    }
                    //return;;
                }
                model.followState = MoRelationshipTypeStranger;
                model.creatorRole = 10; //客服
                [notFriendMArr addObject:model];
                
                if (notFriendMArr.count > 0) {
                    ///写入数据库 聊天都需要从本地数据库取用户信息
                    [[MXChatManager sharedInstance] insertFriends:notFriendMArr];
                }
                
                
                NSMutableDictionary *productInfo = [NSMutableDictionary dictionaryWithCapacity:1];
                [productInfo setValue:self.goodInfoObj.ID forKey:@"attr1"];
                [productInfo setValue:self.goodInfoObj.goods_show forKey:@"attr2"];
                [productInfo setValue:self.goodInfoObj.goods_name forKey:@"attr3"];
                [productInfo setValue:[@(self.goodInfoObj.cash_min) description] forKey:@"attr4"];
                
                [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":model.userid,@"type":@(eConversationTypeChat),@"creatorRole":@(10),@"productInfo":productInfo}];
            } else {
                [NotifyHelper showMessageWithMakeText:data[@"msg"]];
            }
        }).failure(^(id error){
            [NotifyHelper hideHUDForView:self.view animated:YES];
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = self.footerView;
        Class cls = UITableViewCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        AdjustTableBehavior(_tableView);
    }
    return _tableView;
}

- (UIImageView *)backImgView {
    if (!_backImgView) {
        _backImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"商品详情返回"];
            object.x = 20;
            object.y = safeAreaInset().top + 20;
            [object addAction:^(UIView *view) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            object;
        });
    }
    return _backImgView;
}

- (UIImageView *)homeImgView {
    if (!_homeImgView) {
        _homeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"直播分享"];
            object.x = SCREEN_WIDTH - object.width - 20;
            object.y = self.backImgView.y;
            [object addAction:^(UIView *view) {
                GoodsShareView *shareView = [[GoodsShareView alloc] initWithObj:self.goodInfoObj];
                shareView.size = CGSizeMake(SCREEN_WIDTH, [GoodsShareView viewHeight]);
                MXCustomAlertVC *alertVC = [MXCustomAlertVC showInViewController:[[MXRouter sharedInstance] getTopNavigationController] contentView:shareView];
            }];
            object;
        });
    }
    return _homeImgView;
}

- (GoodInfoBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [GoodInfoBottomView new];
        _bottomView.size = CGSizeMake(SCREEN_WIDTH, [GoodInfoBottomView viewHeight]);
        @weakify(self)
        [_bottomView.buyBtn addAction:^(UIButton *btn) {
            @strongify(self)
            self.speciView.block = ^(id data) {
                @strongify(self)
                [self.panVC dismiss];
                [self configureSpeiView:data];
                [self buyGood];
            };
            self.otherSpeciView.block = ^(id data) {
                @strongify(self)
                [self.panVC dismiss];
                [self configureOtherSpeiView:data];
                [self buyGood];
            };
            [self.panVC show];
        }];
        [_bottomView.joinCartBtn addAction:^(UIButton *btn) {
            @strongify(self)
            self.speciView.block = ^(id data) {
                @strongify(self)
                [self.panVC dismiss];
                [self configureSpeiView:data];
                [self joinCart];
            };
            self.otherSpeciView.block = ^(id data) {
                @strongify(self)
                [self.panVC dismiss];
                [self configureOtherSpeiView:data];
                [self joinCart];
            };
            [self.panVC show];
        }];
        [_bottomView.cartBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self gotoCart];
        }];
        [_bottomView.homeBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        [_bottomView.serverBtn addAction:^(UIButton *btn) {
#ifdef Kefu_Server_dev
    [self getMemberServiceList];
#else
    MyServiceView *view = [MyServiceView new];
    view.size = CGSizeMake(SCREEN_WIDTH - 40, [MyServiceView viewHeight]);
    [MXCustomAlertVC showInViewController:[[MXRouter sharedInstance] getTopNavigationController] contentView:view];
#endif
            //[self getMemberServiceList];
        }];
        if (self.isLive) {
            _bottomView.homeBtn.visibility = MyVisibility_Invisible;
            _bottomView.serverBtn.visibility = MyVisibility_Invisible;
            _bottomView.cartBtn.visibility = MyVisibility_Invisible;
            _bottomView.joinCartBtn.visibility = MyVisibility_Invisible;
        }
    }
    return _bottomView;
}

- (GoodInfoHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[GoodInfoHeaderView alloc] initWithGoodsInfoObj:self.goodInfoObj];
        _headerView.size = CGSizeMake(SCREEN_WIDTH, [_headerView viewHeight]);
        @weakify(self)
        [_headerView.specView addAction:^(UIView *view) {
            @strongify(self)
            self.speciView.block = ^(id data) {
                @strongify(self)
                [self.panVC dismiss];
                [self configureSpeiView:data];
            };
            
            self.otherSpeciView.block = ^(id data) {
                @strongify(self)
                [self.panVC dismiss];
                [self configureOtherSpeiView:data];
            };
            [self.panVC show];
        }];
    }
    return _headerView;
}

- (UILabel *)staticValueLbl {
    if (!_staticValueLbl) {
        _staticValueLbl = [UILabel new];
        _staticValueLbl.font = [UIFont font13];
    }
    return _staticValueLbl;
}

- (GoodSpeciView *)speciView {
    if (!_speciView) {
        _speciView = [[GoodSpeciView alloc] initWithSpeci:self.goodSpeciObj];
        _speciView.size = CGSizeMake(SCREEN_WIDTH, [GoodSpeciView viewHeight]);
        @weakify(self)
        [_speciView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.panVC dismiss];
        }];
        _speciView.headerView.goodInfoObj = self.goodInfoObj;
        [_speciView.headerView configureViewDefaultValue];
    }
    return _speciView;
}

- (GoodOtherSpeciView *)otherSpeciView {
    if (!_otherSpeciView) {
        _otherSpeciView = [[GoodOtherSpeciView alloc] initWithSpeci:self.goodSKUObj];
        _otherSpeciView.size = CGSizeMake(SCREEN_WIDTH, [GoodSpeciView viewHeight]);
        @weakify(self)
        [_otherSpeciView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.panVC dismiss];
        }];
        _otherSpeciView.headerView.goodInfoObj = self.goodInfoObj;
        [_otherSpeciView.headerView configureViewDefaultValue];
    }
    return _otherSpeciView;
}

- (HWPanModelVC *)panVC {
    if (!_panVC) {
        _panVC = ({
            HWPanModelVC *object = [HWPanModelVC showInVC:self
                                               customView:[self customViewSpeciView]];
            object;
        });
    }
    return _panVC;
}

- (UIView *)customViewSpeciView {
    if (self.goodInfoObj.isSelfSupport) {
        return self.speciView;
    } else {
        return self.otherSpeciView;
    }
}

- (BuyFooterView *)footerView {
    if (!_footerView) {
        _footerView = [BuyFooterView new];
    }
    return _footerView;
}

- (GoodSKUObj *)goodSKUObj {
    if (!_goodSKUObj) {
        _goodSKUObj = [GoodSKUObj new];
    }
    return _goodSKUObj;
}

- (SPButton *)notExistBtn {
    if (!_notExistBtn) {
        _notExistBtn = [SPButton new];
        _notExistBtn.userInteractionEnabled = NO;
        _notExistBtn.imagePosition = SPButtonImagePositionTop;
        _notExistBtn.imageTitleSpace = 10;
        _notExistBtn.titleLabel.font = [UIFont font13];
        [_notExistBtn setTitle:@"商品已失效不存在" forState:UIControlStateNormal];
        [_notExistBtn setImage:[UIImage imageNamed:@"商品不存在"] forState:UIControlStateNormal];
        [_notExistBtn setTitleColor:[UIColor colorWithHexString:@"#A6A4A4"] forState:UIControlStateNormal];
        [_notExistBtn sizeToFit];
    }
    return _notExistBtn;
}

@end
