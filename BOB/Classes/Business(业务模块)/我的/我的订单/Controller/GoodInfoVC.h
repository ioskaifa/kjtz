//
//  GoodInfoVC.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodInfoVC : BaseViewController

@property (nonatomic, copy) NSString *goods_id;
///免单商品
@property (nonatomic, assign) BOOL isFree;
///直播商品
@property (nonatomic, assign) BOOL isLive;
///直播间编号
@property (nonatomic, copy) NSString *live_id;

@end

NS_ASSUME_NONNULL_END
