//
//  MyOrderVC.m
//  BOB
//
//  Created by mac on 2020/9/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyOrderVC.h"
#import "MyOrderNullView.h"
#import "YJSliderView.h"
#import "MyOrderListCell.h"
#import "MyOrderSectionHeaderView.h"
#import "MyOrderSectionFooterView.h"

@interface MyOrderVC () <UITableViewDelegate, UITableViewDataSource, YJSliderViewDelegate>

@property (nonatomic, strong) MyOrderNullView *nullView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YJSliderView *sliderView;

@property (nonatomic, strong) NSArray *sliderDataSourceArr;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation MyOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.last_id = @"";
    [self fetchData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSArray *vcArr = self.navigationController.viewControllers;
    NSMutableArray *vcMArr = [NSMutableArray arrayWithCapacity:vcArr.count];
    for (UIViewController *vc in vcArr) {
        if ([vc isKindOfClass:NSClassFromString(@"ConfirmOrderVC")]) {
            continue;
        }
        [vcMArr addObject:vc];
    }
    if (vcMArr.count != vcArr.count) {
        self.navigationController.viewControllers = vcMArr;
    }
}

- (void)navBarRightBtnAction:(id)sender {
    MXRoute(@"MyFreeShopNameListVC", nil)
}

#pragma mark - Private Method
- (void)fetchData {
    NSString *api = @"api/shop/order/getOrderList";
    if (self.isFree) {
        api = @"api/freeshop/order/getOrderList";
    }
    if (self.isLive) {
        api = @"api/live/liveOrder/getLiveOrderList";
    }
    [self request:api
            param:@{@"last_id":self.last_id?:@"",
                    @"status":[MyOrderListObj orderStatusToString:self.orderStatus]
            }
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            if ([@"" isEqualToString:self.last_id]) {
                [self.dataSourceMArr removeAllObjects];
            }
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *dataArr = [MyOrderListObj modelListParseWithArray:dataDic[@"data"][@"orderList"]];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            MyOrderListObj *lastObj = [self.dataSourceMArr lastObject];
            if (lastObj) {
                self.last_id = lastObj.ID;
            }
            [self.tableView reloadData];
        }
        [self.tableView reloadData];
        if (self.dataSourceMArr.count == 0) {
            self.nullView.hidden = NO;
        } else {
            self.nullView.hidden = YES;
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    if ([@"MyOrderSectionFooterViewCancel" isEqualToString:eventName]) {
        [MXAlertViewHelper showAlertViewWithMessage:@"是否取消该订单？" completion:^(BOOL cancelled, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self cancelOrder:[userInfo[@"section"] integerValue]];
            }
        }];
    } else if ([@"MyOrderSectionFooterViewPay" isEqualToString:eventName]) {
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:[userInfo[@"section"] integerValue]]];
    } else if ([@"MyOrderSectionFooterViewDel" isEqualToString:eventName]) {
        [MXAlertViewHelper showAlertViewWithMessage:@"是否删除该订单？" completion:^(BOOL cancelled, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self delOrder:[userInfo[@"section"] integerValue]];
            }
        }];
    } else if ([@"MyOrderSectionFooterViewReceive" isEqualToString:eventName]) {
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:[userInfo[@"section"] integerValue]]];
    } else {
        [super routerEventWithName:eventName userInfo:userInfo];
    }
}

#pragma mark 取消订单
- (void)cancelOrder:(NSInteger)section {
    if (section >= self.dataSourceMArr.count) {
        return;
    }
    MyOrderListObj *obj = self.dataSourceMArr[section];
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    NSString *api = @"api/shop/order/cancelOrder";
    if (self.isFree) {
        api = @"api/freeshop/order/cancelOrder";
    }
    if (self.isLive) {
        api = @"api/live/liveOrder/cancelLiveOrder";
    }
    [self request:api
            param:@{@"order_id":obj.order_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            obj.status = @"04";
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark - 删除订单
- (void)delOrder:(NSInteger)section {
    if (section >= self.dataSourceMArr.count) {
        return;
    }
    MyOrderListObj *obj = self.dataSourceMArr[section];
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    NSString *api = @"api/shop/order/delOrder";
    if (self.isFree) {
        api = @"api/freeshop/order/delOrder";
    }
    if (self.isLive) {
        api = @"api/live/liveOrder/delLiveOrder";
    }
    [self request:api
            param:@{@"order_id":obj.order_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            [self tableViewDelSection:section];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)tableViewDelSection:(NSInteger)section {
    if (section >= self.dataSourceMArr.count) {
        return;
    }
    MyOrderListObj *obj = self.dataSourceMArr[section];
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    [self.dataSourceMArr removeObject:obj];
    [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSourceMArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section >= self.dataSourceMArr.count) {
        return 0;
    }
    MyOrderListObj *obj = self.dataSourceMArr[section];
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return 0;
    }
    NSArray *tempArr = obj.orderList;
    if (![tempArr isKindOfClass:NSArray.class]) {
        return 0;
    }
    return tempArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MyOrderListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceMArr.count) {
        return;
    }
    MyOrderListObj *obj = self.dataSourceMArr[indexPath.section];
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    NSArray *tempArr = obj.orderList;
    if (![tempArr isKindOfClass:NSArray.class]) {
        return;
    }
    if (indexPath.row >= tempArr.count) {
        return;
    }
    BuyGoodsListObj *listObj = tempArr[indexPath.row];
    if (![listObj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    if (![cell isKindOfClass:MyOrderListCell.class]) {
        return;
    }
    
    MyOrderListCell *listCell = (MyOrderListCell *)cell;
    if (indexPath.row == tempArr.count - 1) {
        listCell.goodView.line.hidden = YES;
    } else {
        listCell.goodView.line.hidden = NO;
    }
    [listCell configureView:listObj];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [MyOrderListCell viewHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.dataSourceMArr.count) {
        return;
    }
    MyOrderListObj *obj = self.dataSourceMArr[indexPath.section];
    if (![obj isKindOfClass:MyOrderListObj.class]) {
        return;
    }
    if (self.isFree) {
        MXRoute(@"OrderInfoVC", (@{@"orderID":obj.order_id,@"isFree":@(1)}))
    } else if (self.isLive) {
        MXRoute(@"OrderInfoVC", (@{@"orderID":obj.order_id,@"isLive":@(1)}))
    } else {
        MXRoute(@"OrderInfoVC", @{@"orderID":obj.order_id})
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [MyOrderSectionHeaderView viewHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return [MyOrderSectionFooterView viewHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    Class cls = MyOrderSectionHeaderView.class;
    MyOrderSectionHeaderView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass(cls)];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, [MyOrderSectionHeaderView viewHeight]);
    MyOrderListObj *obj = self.dataSourceMArr[section];
    if ([obj isKindOfClass:MyOrderListObj.class]) {
        obj.isFree = self.isFree;
    }
    if (section < self.dataSourceMArr.count) {
        [view configureView:obj];
    }
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    Class cls = MyOrderSectionFooterView.class;
    MyOrderSectionFooterView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass(cls)];
    view.frame = CGRectMake(0, 0, SCREEN_WIDTH, [MyOrderSectionFooterView viewHeight]);
    if (section < self.dataSourceMArr.count) {
        [view configureView:self.dataSourceMArr[section] section:section];
    }
    return view;
}

#pragma mark YJSliderViewDelegate
- (NSInteger)numberOfItemsInYJSliderView:(YJSliderView *)sliderView {
    return self.sliderDataSourceArr.count;
}

- (UIView *)yj_SliderView:(YJSliderView *)sliderView viewForItemAtIndex:(NSInteger)index {
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (NSString *)yj_SliderView:(YJSliderView *)sliderView titleForItemAtIndex:(NSInteger)index {
    if (index >= self.sliderDataSourceArr.count) {
        return @"";
    }
    return self.sliderDataSourceArr[index];
}

- (NSInteger)initialzeIndexFoYJSliderView:(YJSliderView *)sliderView {
    return self.orderStatus;
}

- (void)yj_SliderView:(YJSliderView *)sliderView didSelectItemAtIndex:(NSIndexPath *)indexPath {
    self.orderStatus = indexPath.row;
    if (indexPath.row == 4) {
        self.orderStatus = OrderStatusToComplete;
    }
    
    self.last_id = @"";
    [self fetchData];
}

#pragma mark - InitUI
- (void)createUI {
    if (self.isFree) {
        [self setNavBarTitle:@"免单区订单"];
        [self setNavBarRightBtnWithTitle:@"我的免单" andImageName:nil];
    } else {
        [self setNavBarTitle:@"我的订单"];
    }
    self.view.backgroundColor = [UIColor moBackground];
    self.last_id = @"";
    
    [self.view addSubview:self.sliderView];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.nullView];
        
    [self layoutUI];
}

- (void)layoutUI {
    [self.sliderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(45);
        make.top.mas_equalTo(self.view.mas_top);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sliderView.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    
    [self.nullView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sliderView.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.height.mas_equalTo(self.nullView.height);
    }];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        Class cls = MyOrderListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        cls = MyOrderSectionHeaderView.class;
        [_tableView registerClass:cls forHeaderFooterViewReuseIdentifier:NSStringFromClass(cls)];
        cls = MyOrderSectionFooterView.class;
        [_tableView registerClass:cls forHeaderFooterViewReuseIdentifier:NSStringFromClass(cls)];
        _tableView.tableFooterView = [UIView new];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (YJSliderView *)sliderView {
    if (!_sliderView) {
        _sliderView = [[YJSliderView alloc] initWithFrame:CGRectZero];
        _sliderView.delegate = self;
        _sliderView.themeColor = [UIColor blackColor];
        _sliderView.lineColor = [UIColor blackColor];
    }
    return _sliderView;
}

- (NSArray *)sliderDataSourceArr {
    if (!_sliderDataSourceArr) {
        _sliderDataSourceArr = @[@"全部", @"待付款", @"待发货", @"待收货", @"已完成"];
    }
    return _sliderDataSourceArr;
}

- (MyOrderNullView *)nullView {
    if (!_nullView) {
        _nullView = [MyOrderNullView new];
        _nullView.size = CGSizeMake(SCREEN_WIDTH, [MyOrderNullView viewHeight]);
        _nullView.hidden = YES;
    }
    return _nullView;
}

@end
