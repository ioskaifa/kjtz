//
//  ConfirmOrderVC.m
//  BOB
//
//  Created by mac on 2020/9/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ConfirmOrderVC.h"
#import "ConfirmOrderAddressView.h"
#import "OrderInfoTempObj.h"
#import "ConfirmGoodInfoView.h"
#import "ConfirmOrderBottomView.h"
#import "OrderSelectPayView.h"
#import "HWPanModelVC.h"
#import "YQPayKeyWordVC.h"
#import "PolymerPayManager.h"

@interface ConfirmOrderVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MyBaseLayout *baseLayout;
///地址信息
@property (nonatomic, strong) ConfirmOrderAddressView *addressView;

@property (nonatomic, strong) OrderInfoTempObj *obj;

@property (nonatomic, strong) ConfirmOrderBottomView *bottomView;

@property (nonatomic, strong) OrderSelectPayView *orderSelectPayView;

@property (nonatomic, strong) HWPanModelVC *panVC;

@property (nonatomic, strong) UITextField *tf;

@property (nonatomic, assign) CGFloat sla_price;
///sla支付开关 - 直正对免单商城
@property (nonatomic, assign) BOOL sla_pay_lock;

@end

@implementation ConfirmOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
    [self fetchSlaPrice];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self fetchAddressByAddressID:self.obj.addressObj.address_id];
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    if ([@"ConfirmOrderBottomViewPay" isEqualToString:eventName]) {
        if ([self valiParam]) {
            [self commitOrder];
        }
    } else {
        [super routerEventWithName:eventName userInfo:userInfo];
    }
}

- (BOOL)valiParam {
    if ([StringUtil isEmpty:self.obj.addressObj.address_id]) {
        [NotifyHelper showMessageWithMakeText:@"请选择收货地址"];
        return NO;
    }
    if (![StringUtil isEmpty:self.obj.addressObj.tips]) {
        [NotifyHelper showMessageWithMakeText:self.obj.addressObj.tips];
        return NO;
    }
    return YES;
}

#pragma mark - 获取临时订单详情
- (void)fetchData {
    [self request:[self apiUrl]
            param:@{@"order_id":self.order_id?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            self.obj = [OrderInfoTempObj modelParseWithDict:object[@"data"]];
            OrderSettleListItem *item = self.obj.orderSettleList.firstObject;
            if ([item isKindOfClass:OrderSettleListItem.class]) {
                //一次性提交支付的订单中只要有商品是only_sla_pay=1，就只能SLA支付
                for (OrderGoodsListItem *order in item.orderGoodsList) {
                    if (![order isKindOfClass:OrderGoodsListItem.class]) {
                        continue;
                    }
                    if (order.only_sla_pay) {
                        self.obj.only_sla_pay = order.only_sla_pay;
                        break;
                    }
                }
            }
            [self initUI];
            [self.bottomView configureView:self.obj.total_cash_num
                                discontNum:self.obj.discount_cash_num];
            [self fetchDefualtAddress];
        }
    }];
}

#pragma mark - 获取SLA汇率
- (void)fetchSlaPrice {
    NSString *url = @"api/shop/pay/getSlaPrice";
    if (self.isFree) {
        url = @"api/freeshop/pay/getSlaPrice";
    }
    if (self.isLive) {
        url = @"api/live/livePay/getSlaPrice";
    }
    [self request:url param:@{} completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object[@"data"];
            if (dataDic[@"sla_price"]) {
                self.sla_price = [dataDic[@"sla_price"] floatValue];
                self.orderSelectPayView.sla_price = self.sla_price;
                if (dataDic[@"sla_pay_lock"]) {
                    self.sla_pay_lock = [dataDic[@"sla_pay_lock"] boolValue];
                }
            }
        }
    }];
}

#pragma mark - 提交正式订单
- (void)commitOrder {
    switch (self.fromType) {
        case ConfirmOrderTypeFromGoodInfoVC: {
            [self commitBatchOrder];
            break;
        }
        case ConfirmOrderTypeFromShoppingCartVC: {
            [self commitBatchOrder];
            break;
        }
        default:
            break;
    }
}

#pragma mark - 提交正式订单 - 商品详情 立即购买
- (void)commitSingleOrder {
    NSString *note = [StringUtil trim:self.tf.text];
    if (note.length > 100) {
        [NotifyHelper showMessageWithMakeText:@"给商家留言最多100字"];
        return;
    }
    [OrderSelectPayView checkSetPayPassword:^(id data) {
        [NotifyHelper showHUDAddedTo:self.view animated:YES];
        NSString *api = @"api/shop/order/submitBatchOrder";
        if (self.isFree) {
            api = @"api/freeshop/order/submitBatchOrder";
        }
        if (self.isLive) {
            api = @"api/live/liveOrder/submitLiveSingleOrder";
        }
        [self request:api
                param:@{@"order_id":[self.obj.order_idArr componentsJoinedByString:@","],
                        @"note":note,
                        @"address_id":self.obj.addressObj.address_id?:@""}
           completion:^(BOOL success, id object, NSString *error) {
            [NotifyHelper hideAllHUDsForView:self.view animated:YES];
            if (success) {
                [NotifyHelper hideAllHUDsForView:self.view animated:YES];
                [self.panVC show];
            } else {
                [NotifyHelper showMessageWithMakeText:error];
            }
        }];
    }];
}

#pragma mark - 提交正式订单 - 购物车
- (void)commitBatchOrder {
    NSString *note = [StringUtil trim:self.tf.text];
    if (note.length > 100) {
        [NotifyHelper showMessageWithMakeText:@"给商家留言最多100字"];
        return;
    }
    if ([StringUtil isEmpty:note]) {
        note = @" ";
    }
    NSMutableArray *noteMArr = [NSMutableArray array];
    for (NSInteger i = 0; i < self.obj.orderSettleList.count; i++) {
        [noteMArr addObject:note];
    }
    
    [OrderSelectPayView checkSetPayPassword:^(id data) {
        [NotifyHelper showHUDAddedTo:self.view animated:YES];
        NSString *api = @"api/shop/order/submitBatchOrder";
        if (self.isFree) {
            api = @"api/freeshop/order/submitBatchOrder";
        }
        if (self.isLive) {
            api = @"api/live/liveOrder/submitLiveBatchOrder";
        }
        [self request:api
                param:@{@"order_id":[self.obj.order_idArr componentsJoinedByString:@","],
                        @"notes_str":[noteMArr componentsJoinedByString:@"&"],
                        @"address_id":self.obj.addressObj.address_id?:@""}
           completion:^(BOOL success, id object, NSString *error) {
            [NotifyHelper hideAllHUDsForView:self.view animated:YES];
            if (success) {
                [NotifyHelper hideAllHUDsForView:self.view animated:YES];
                [self.panVC show];
            } else {
                [NotifyHelper showMessageWithMakeText:error];
            }
        }];
    }];
}

#pragma mark - 查询默认地址
- (void)fetchDefualtAddress {
    [self request:@"api/user/address/getUserAddressInfo"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            UserAddressListObj *obj = [UserAddressListObj modelParseWithDict:object[@"data"][@"userAddress"]];
            [self updateAddress:obj];
        }
    }];
}

#pragma mark - 判断当前地址是否有效
- (void)fetchAddressByAddressID:(NSString *)addressID {
    if ([StringUtil isEmpty:addressID]) {
        //如果没有地址ID 拉取默认地址 看看是否有数据
        [self fetchDefualtAddress];
        return;
    }
    [self request:@"api/user/address/getUserAddressById"
            param:@{@"address_id":addressID}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            //如果拉的数据为空 则尝试拉取默认地址
            UserAddressListObj *obj = [UserAddressListObj modelParseWithDict:object[@"data"][@"userAddress"]];
            if ([obj isKindOfClass:UserAddressListObj.class]) {
                if (![StringUtil isEmpty:obj.address_id]) {
                    [self updateAddress:obj];
                    return;
                }
            }
            [self fetchDefualtAddress];
        }
    }];
}

- (void)updateAddress:(UserAddressListObj *)obj {
    if (![obj isKindOfClass:UserAddressListObj.class]) {
        self.obj.addressObj = obj;
        [self configureAddressView:obj];
        return;
    }
    self.obj.addressObj = obj;
    if (self.obj.isBlend) {
        [self fetchOrderFreightFee];
    }
    [self configureAddressView:obj];
}

- (void)configureAddressView:(UserAddressListObj *)obj {
    if ([StringUtil isEmpty:obj.tips]) {
        [self.bottomView configurePayBtnState:YES];
    } else {
        [self.bottomView configurePayBtnState:NO];
    }
    [self.addressView configureView:self.obj.addressObj];
    self.addressView.height = [self.addressView viewHeight];
    self.addressView.myHeight = self.addressView.height;
    [self.tableView beginUpdates];
    [self.baseLayout layoutIfNeeded];
    [self.tableView endUpdates];
}

#pragma mark - 拉取订单运费
- (void)fetchOrderFreightFee {
    NSString *api = @"api/shop/order/getOrderFreightFee";
    if (self.isFree) {
        api = @"api/freeshop/order/getOrderFreightFee";
    }
    [self request:api
            param:@{@"address_id":self.obj.addressObj.address_id?:@"",
                    @"order_id":[self.obj.order_id_other_Arr componentsJoinedByString:@","]
            }
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSArray *tempArr = object[@"data"][@"orderFreightFeeList"];
            for (NSDictionary *dic in tempArr) {
                if (![dic isKindOfClass:NSDictionary.class]) {
                    continue;
                }
                if (dic[@"order_id"]) {
                    OrderSettleListItem *item = [self.obj findOrderSettleItemByOrderId:[dic[@"order_id"] description]];
                    if ([item isKindOfClass:OrderSettleListItem.class]) {
                        item.freight_fee = [dic[@"freight_fee"] floatValue];
                    }
                }
            }
            self.obj.addressObj.tips = @"";
            [self configureAddressView:self.obj.addressObj];
        } else {
            self.obj.addressObj.tips = error;
            [self configureAddressView:self.obj.addressObj];
        }
    }];
}

- (NSString *)payOrderUrl {
    NSString *string;
    switch (self.fromType) {
        case ConfirmOrderTypeFromGoodInfoVC: {
            string = @"/api/shop/pay/paySingleOrderApp";
            if (self.isFree) {
                string = @"/api/freeshop/pay/paySingleOrderApp";
            }
            if (self.isLive) {
                string = @"/api/live/livePay/payLiveSingleOrderApp";
            }
            break;
        }
        case ConfirmOrderTypeFromShoppingCartVC: {
            string = @"/api/shop/pay/payBatchOrderApp";
            if (self.isFree) {
                string = @"/api/freeshop/pay/payBatchOrderApp";
            }
            if (self.isLive) {
                string = @"/api/live/livePay/payLiveSingleOrderApp";
            }
            break;
        }
        default:
            string = @"/api/shop/pay/paySingleOrderApp";
            if (self.isFree) {
                string = @"/api/freeshop/pay/paySingleOrderApp";
            }
            if (self.isLive) {
                string = @"/api/live/livePay/payLiveSingleOrderApp";
            }
            break;
    }
    return string;
}

- (void)payOrder:(NSString *)selectPayType {
    PolymerPayType payType = [self formatPayType:selectPayType];
    if (payType == PolymerPayTypeExchange) {
        [[YQPayKeyWordVC alloc] showInViewController:[[MXRouter sharedInstance] getMallTopNavigationController]
                                                type:NormalPayType
                                            dataDict:@{@"title":@"支付密码"}
                                               block:^(NSString * password) {
            [NotifyHelper showHUDAddedTo:self.view animated:YES];
            [self request:[self payOrderUrl]
                    param:@{@"order_id":[self.obj.order_idArr componentsJoinedByString:@","],
                            @"order_pay_type":@"04",
                            @"pay_password":password
                    }
               completion:^(BOOL success, id object, NSString *error) {
                [NotifyHelper hideHUDForView:self.view animated:YES];
                if (!success) {
                    [NotifyHelper showMessageWithMakeText:object[@"msg"]];
                } else {
                    OrderSettleListItem *items = self.obj.orderSettleList.firstObject;
                    NSString *productName = [[items.orderGoodsList firstObject] valueForKey:@"goods_name"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveRoomBuyGoodSuccess" object:@{@"total_cash":[NSNumber numberWithFloat:self.obj.total_total_cash_num],@"productName":productName ?: @""}];
                }

                [self gotoAboutOrderVC];
            }];
        }];
    } else {
        NSDictionary *param =@{@"order_id":[self.obj.order_idArr componentsJoinedByString:@","],
                               @"order_pay_type":[self formatPay_Type:selectPayType]};
        [[PolymerPayManager shared] pay:payType
                                    url:[self payOrderUrl]
                                  param:param viewController:self block:^(id  _Nullable data) {
            if(![data isSuccess]) {
                [NotifyHelper showMessageWithMakeText:[data valueForKey:@"msg"]];
            } else {
                OrderSettleListItem *items = self.obj.orderSettleList.firstObject;
                NSString *productName = [[items.orderGoodsList firstObject] valueForKey:@"goods_name"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LiveRoomBuyGoodSuccess" object:@{@"total_cash":[NSNumber numberWithFloat:self.obj.total_total_cash_num],@"productName":productName ?: @""}];
            }
            if (self.obj.orderIDArr.count == 1) {
                if (self.isFree) {
                    MXRoute(@"OrderInfoVC", (@{@"orderID":self.obj.order_idArr.firstObject,@"isFree":@(1)}))
                } else {
                    MXRoute(@"OrderInfoVC", @{@"orderID":self.obj.order_idArr.firstObject})
                }
            } else {
                if (self.isFree) {
                    MXRoute(@"MyOrderVC", @{@"isFree":@(1)})
                } else {
                    MXRoute(@"MyOrderVC", nil)
                }
            }
        }];
    }
}

- (PolymerPayType)formatPayType:(NSString *)selectPayType {
    PolymerPayType payType = PolymerPayTypeExchange;
    if ([@"支付宝" isEqualToString:selectPayType]) {
        payType = PolymerPayTypeAliPay;
    } else if ([@"微信" isEqualToString:selectPayType]) {
        payType = PolymerPayTypeWePay;
    }
    return payType;
}

- (NSString *)formatPay_Type:(NSString *)selectPayType {
    if ([@"支付宝" isEqualToString:selectPayType]) {
        return @"01";
    } else if ([@"微信" isEqualToString:selectPayType]) {
        return @"02";
    }
    return @"04";
}

- (NSString *)payType:(NSString *)selectPayType {
    if ([@"支付宝" isEqualToString:selectPayType]) {
        return @"01";
    } else if ([@"微信" isEqualToString:selectPayType]) {
        return @"02";
    }
    return @"04";
}

- (NSString *)apiUrl {
    NSString *string;
    switch (self.fromType) {
        case ConfirmOrderTypeFromGoodInfoVC:
            string = @"api/shop/order/getTempOrderSettleList";
            if (self.isFree) {
                string = @"api/freeshop/order/getTempOrderSettleList";
            }
            if (self.isLive) {
                string = @"/api/live/liveOrder/getLiveTempOrderSettleList";
            }
            break;
        case ConfirmOrderTypeFromShoppingCartVC:
            string = @"api/shop/order/getTempOrderSettleList";
            if (self.isFree) {
                string = @"api/freeshop/order/getTempOrderSettleList";
            }
            break;
        default:
            string = @"api/shop/order/getTempOrderSettleList";
            if (self.isFree) {
                string = @"api/freeshop/order/getTempOrderSettleList";
            }
            if (self.isLive) {
                string = @"/api/live/liveOrder/getLiveTempOrderSettleList";
            }
            break;
    }
    return string;
}

///跳转OrderInfoVC or MyOrderVC
- (void)gotoAboutOrderVC {
    if (self.obj.orderIDArr.count == 1) {
        if (self.isFree) {
            MXRoute(@"OrderInfoVC", (@{@"orderID":self.obj.order_idArr.firstObject,@"isFree":@(1)}))
        } else if (self.isLive) {
            MXRoute(@"OrderInfoVC", (@{@"orderID":self.obj.order_idArr.firstObject,@"isLive":@(1)}))
        }  else {
            MXRoute(@"OrderInfoVC", @{@"orderID":self.obj.order_idArr.firstObject})
        }
    } else {
        if (self.isFree) {
            MXRoute(@"MyOrderVC", @{@"isFree":@(1)})
        } else if (self.isLive) {
            MXRoute(@"MyOrderVC", @{@"isLive":@(1)})
        }  else {
            MXRoute(@"MyOrderVC", nil)
        }
    }
}

- (void)createUI {
    [self setNavBarTitle:@"确认订单"];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.tableView];
    [layout addSubview:self.bottomView];
    self.sla_pay_lock = NO;
}

- (void)initUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myHeight = MyLayoutSize.wrap;
    
    [baseLayout addSubview:self.addressView];
    
    UIImageView *imgView = [UIImageView new];
    imgView.image= [UIImage imageNamed:@"地址分割线"];
    imgView.myHeight = 3;
    imgView.myLeft = 0;
    imgView.myRight = 0;
    [baseLayout addSubview:imgView];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    [layout setViewCornerRadius:10];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 15;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myBottom = 0;
    [baseLayout addSubview:layout];
    
    for (OrderSettleListItem *item in self.obj.orderSettleList) {
        if (![item isKindOfClass:OrderSettleListItem.class]) {
            continue;
        }
        for (OrderGoodsListItem *goodsItem in item.orderGoodsList) {
            if (![goodsItem isKindOfClass:OrderGoodsListItem.class]) {
                continue;
            }
            ConfirmGoodInfoView *view = [ConfirmGoodInfoView new];
            view.myLeft = 0;
            view.myRight = 0;
            view.myHeight = 130;
            [view configureView:goodsItem];
            [layout addSubview:view];
        }
    }
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myHeight = 20;
    subLayout.myTop = 20;
    subLayout.myLeft = 15;
    subLayout.myRight = 15;
    
    UILabel *lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentRight;
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor colorWithHexString:@"#5B5B66"];
    lbl.mySize = CGSizeMake(100, 20);
    lbl.text = @"物流运费";
    [subLayout addSubview:lbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor blackColor];
    lbl.text = StrF(@"￥%0.2f", self.obj.total_express);
    if (self.obj.total_express == 0) {
        lbl.text = @"无";
    }
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    [subLayout addSubview:lbl];
    [layout addSubview:subLayout];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myHeight = 20;
    subLayout.myTop = 10;
    subLayout.myLeft = 15;
    subLayout.myRight = 15;
    lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentRight;
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor colorWithHexString:@"#5B5B66"];
    lbl.text = @"订单备注";
    lbl.mySize = CGSizeMake(100, 20);
    [subLayout addSubview:lbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    UITextField *tf = [UITextField new];
    tf.mySize = CGSizeMake(180, 20);
    NSArray *txtArr = @[@"选填，给商家留言最多100字"];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#AAAABB"]];
    NSArray *fontArr = @[[UIFont font13]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                        colors:colorArr
                                                                         fonts:fontArr];
    tf.attributedPlaceholder = att;
    tf.font = [UIFont font13];
    tf.textColor = [UIColor moBlack];
    [subLayout addSubview:tf];
    self.tf = tf;
    [layout addSubview:subLayout];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myHeight = 25;
    subLayout.myTop = 10;
    subLayout.myLeft = 15;
    subLayout.myRight = 15;
    subLayout.myBottom = 10;
    [subLayout addSubview:[UIView placeholderHorzView]];

    txtArr = @[StrF(@"共%ld件，", (long)self.obj.total_goods_num),
               @"小计：",
               StrF(@"￥%0.2f", self.obj.total_total_cash_num)];
    colorArr = @[[UIColor colorWithHexString:@"#A8A8A8"],
                 [UIColor blackColor],
                 [UIColor blackColor]];
    fontArr = @[[UIFont font13], [UIFont font13], [UIFont boldFont15]];
    att = [NSMutableAttributedString initWithTitles:txtArr
                                             colors:colorArr
                                              fonts:fontArr];
    lbl = [UILabel new];
    lbl.attributedText = att;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    [subLayout addSubview:lbl];
    [layout addSubview:subLayout];
    
    [baseLayout layoutIfNeeded];
    self.tableView.tableHeaderView = baseLayout;
    self.baseLayout = baseLayout;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.weight = 1;
        _tableView.myTop = 1;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
    }
    return _tableView;
}

- (ConfirmOrderAddressView *)addressView {
    if (!_addressView) {
        _addressView = ({
            ConfirmOrderAddressView *object = [ConfirmOrderAddressView new];
            object.backgroundColor = [UIColor whiteColor];
            [object configureView:self.obj.addressObj];
            object.height = [object viewHeight];
            object.myHeight = object.height;
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            @weakify(self)
            [object addAction:^(UIView *view) {
                void (^selectBlock)(id) = ^(UserAddressListObj *obj) {
                    @strongify(self)
                    if (![obj isKindOfClass:UserAddressListObj.class]) {
                        return;
                    }
                    [self updateAddress:obj];
                };
                MXRoute(@"ShippingAddressVC", @{@"block":selectBlock});
            }];
            object;
        });
        
    }
    return _addressView;
}

- (ConfirmOrderBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [ConfirmOrderBottomView new];
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.mySize = CGSizeMake(SCREEN_WIDTH, [ConfirmOrderBottomView viewHeight]);
        [_bottomView configureView:0 discontNum:0];
    }
    return _bottomView;
}

- (OrderSelectPayView *)orderSelectPayView {
    if (!_orderSelectPayView) {
        CGFloat amount = self.obj.total_total_cash_num;
        _orderSelectPayView = [[OrderSelectPayView alloc] initWithAmount:amount
                                                                withRate:self.obj.sla_discount_rate
                                                            withSlaPrice:self.sla_price noSLAPay:self.isFree?!self.sla_pay_lock:NO];
        _orderSelectPayView.only_sla_pay = self.obj.only_sla_pay;
        _orderSelectPayView.size = CGSizeMake(SCREEN_WIDTH, [OrderSelectPayView viewHeight]);
        @weakify(self)
        [_orderSelectPayView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.panVC dismiss];
            [self gotoAboutOrderVC];
        }];
        _orderSelectPayView.block = ^(id data) {
            @strongify(self)
            NSString *payType = data;
            MLog(@"选择支付方式....%@", payType);
            [self.panVC dismiss];
            [self payOrder:payType];
        };
    }
    return _orderSelectPayView;
}

- (HWPanModelVC *)panVC {
    if (!_panVC) {
        _panVC = ({
            HWPanModelVC *object = [HWPanModelVC showInVC:self
                                               customView:self.orderSelectPayView];
            object.didDismissblock = ^(id data) {
                //[self gotoAboutOrderVC];
            };
            object;
        });
    }
    return _panVC;
}

@end
