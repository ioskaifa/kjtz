//
//  MyCollectionListObj.m
//  BOB
//
//  Created by colin on 2021/1/16.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "MyCollectionListObj.h"

@implementation MyCollectionListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id",
    };
}

- (NSString *)goods_show {
    if ([_goods_show hasPrefix:@"http"]) {
        return _goods_show;
    } else {
        return StrF(@"%@/%@", UDetail.user.qiniu_domain, _goods_show);
    }
}

@end
