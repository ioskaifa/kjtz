//
//  WalletRecordDetailObj.h
//  BOB
//
//  Created by colin on 2020/12/9.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletRecordDetailObj : BaseObject

@property (nonatomic , copy) NSString *op_type;
///：编号
@property (nonatomic , copy) NSString *ID;
///：购物订单号
@property (nonatomic , copy) NSString *order_id;
///：订单类型（01：自营订单  02：供应链订单）
@property (nonatomic , copy) NSString *order_type;
///：用户编号
@property (nonatomic , copy) NSString *user_id;
///：商户订单号
@property (nonatomic , copy) NSString *out_trade_no;
///：商品数量
@property (nonatomic , copy) NSString *goods_num;
///：快递费
@property (nonatomic , copy) NSString *express_cash;
///：供应链运费
@property (nonatomic , copy) NSString *freight_fee;
///：商品金额
@property (nonatomic , copy) NSString *cash_num;
///：订单总金额
@property (nonatomic , copy) NSString *total_cash_num;
///：订单折扣金额
@property (nonatomic , copy) NSString *discount_cash_num;
///：支付类型（01：支付宝  02：微信   03：提现账户余额  04：助农基金  05：提货余额）
@property (nonatomic , copy) NSString *order_pay_type;
///：支付数量
@property (nonatomic , copy) NSString *pay_num;
///：支付余额
@property (nonatomic , copy) NSString *pay_balance_num;
///：支付SLA
@property (nonatomic , copy) NSString *pay_sla_num;
///：支付SLA数量
@property (nonatomic , copy) NSString *pay_sla_price;
///：SLA支付折扣比例
@property (nonatomic , copy) NSString *sla_discount_rate;
///：状态（00-待付款 02-待发货 03-待收货 04-已取消 05-退款 09-交易完成）
@property (nonatomic , copy) NSString *status;
///：创建时间
@property (nonatomic , copy) NSString *cre_time;
///：更新时间
@property (nonatomic , copy) NSString *up_date;
///：支付时间
@property (nonatomic , copy) NSString *pay_date;
///：发货时间
@property (nonatomic , copy) NSString *send_date;
///：收货时间
@property (nonatomic , copy) NSString *receive_date;
///：是否免单（0：否  1：是）
@property (nonatomic , copy) NSString *is_free;
///：免单状态（00：未免单  09：已免单）
@property (nonatomic , copy) NSString *free_status;
///：钱包类型（03：SLA）
@property (nonatomic , copy) NSString *purse_type;
///：拨款数量
@property (nonatomic , copy) NSString *num;
///：提币地址
@property (nonatomic , copy) NSString *address;
///：提币类型（01：交易所  02：CLS系统）
@property (nonatomic , copy) NSString *send_type;
///：提币数量
@property (nonatomic , copy) NSString *account;
///：手续费
@property (nonatomic , copy) NSString *charge;
///：手续费
@property (nonatomic , copy) NSString *rate;
///：HASH凭证
@property (nonatomic , copy) NSString *hashString;
///：奖励金额
@property (nonatomic , copy) NSString *benefit_money;
///：SLA价格
@property (nonatomic , copy) NSString *sla_price;
///：SLA数量
@property (nonatomic , copy) NSString *sla_num;
///：购物订单编号
@property (nonatomic , copy) NSString *buy_order_id;
///：购物订单号
@property (nonatomic , copy) NSString *buy_order_no;
///：订单号（流水号）
@property (nonatomic , copy) NSString *order_no;
///：充币数量
@property (nonatomic , copy) NSString *number;
@property (nonatomic , copy) NSString *user_tel;
@property (nonatomic , copy) NSString *nick_name;
///：备注
@property (nonatomic , copy) NSString *remark;

@property (nonatomic , copy) NSString *gift_name;
@property (nonatomic , copy) NSString *gift_sla_num;
@property (nonatomic , copy) NSString *gift_num;

@property (nonatomic , copy) NSString *refund_status;
@property (nonatomic , copy) NSString *refund_sla_num;
@property (nonatomic , copy) NSString *refund_ben_rate;
@property (nonatomic , copy) NSString *win_order_no;
@property (nonatomic , copy) NSString *spell_sla_num;
@property (nonatomic , copy) NSString *spell_peo_num;
@property (nonatomic , copy) NSString *spell_ben_rate;
@property (nonatomic , copy) NSString *spell_cash_coupon;
@property (nonatomic , copy) NSString *goods_market_cash;
@property (nonatomic , copy) NSString *coupon_status;
@property (nonatomic , copy) NSString *win_date;
@property (nonatomic , copy) NSString *refund_date;
@property (nonatomic , copy) NSString *is_win;
@property (nonatomic , copy) NSString *goods_name;
@property (nonatomic , copy) NSString *goods_no;
@property (nonatomic , copy) NSString *cre_date;

@end

NS_ASSUME_NONNULL_END
