//
//  UserSendLogListObj.h
//  BOB
//
//  Created by mac on 2020/10/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"
#import "IMXWalletRecord.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserSendLogListObj : BaseObject<IMXWalletRecord>

@property (nonatomic , copy) NSString              * address;
@property (nonatomic , copy) NSString              * charge;
@property (nonatomic , copy) NSString              * rate;
@property (nonatomic , assign) NSInteger              min_feet;
@property (nonatomic , copy) NSString              * send_type;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , assign) NSInteger              ID;
@property (nonatomic , copy) NSString              * order_id;
@property (nonatomic , copy) NSString              * account;
@property (nonatomic , copy) NSString              * hash;
///00-待处理  02-处理中 03-待审核 05-审核冻结 06-审核拒绝  08-提币失败 09-提币成功
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , copy) NSString              * formatStatus;

@end

NS_ASSUME_NONNULL_END
