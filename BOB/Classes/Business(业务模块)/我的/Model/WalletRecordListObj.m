//
//  WalletRecordListObj.m
//  BOB
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletRecordListObj.h"

@implementation WalletRecordListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

#pragma mark - 列表相关
///记录名称
- (NSString *)listTitle {
    return self.op_name;
}
///记录时间
- (NSString *)listTime {
    return StrF(@"时间：%@", self.cre_time);
}
///记录值
- (NSString *)listValue {
    return StrF(@"%0.4f", self.num);
}

- (NSString *)listAccountValue {
    return @"";
}
///流水号
- (NSString *)listSerialNum {
    return StrF(@"流水号：%@", self.order_id);
}

///手续费
- (NSString *)listChargeValue {
    return @"";
}

@end
