//
//  UserAddressListObj.m
//  BOB
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserAddressListObj.h"

@implementation UserAddressListObj

- (NSString *)formatAddress {
    if (!_formatAddress) {
        _formatAddress = [self.address stringByReplacingOccurrencesOfString:@"·" withString:@" "];
    }
    return _formatAddress;
}

- (NSString *)address_area {
    if (!_address_area) {
        NSRange range = [self.address rangeOfString:@"·" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            _address_area = [self.address substringToIndex:range.location];
        } else {
            _address_area = self.address;
        }
    }
    return _address_area;
}

- (NSString *)address_des {
    if (!_address_des) {
        NSRange range = [self.address rangeOfString:@"·" options:NSBackwardsSearch];
        if (range.location != NSNotFound) {
            _address_des = [self.address substringFromIndex:range.location+1];
        } else {
            _address_des = self.address;
        }
    }
    return _address_des;
}

@end
