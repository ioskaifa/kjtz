//
//  UserExchangeLogListObj.m
//  BOB
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserExchangeLogListObj.h"

@implementation UserExchangeLogListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

#pragma mark - 列表相关
///记录名称
- (NSString *)listTitle {
    return @"积分转换成余额";
}
///记录时间
- (NSString *)listTime {
    return StrF(@"时间：%@", self.cre_date);
}
///记录值
- (NSString *)listValue {
    return StrF(@"-%0.2f", self.exchange_num);
}

- (NSString *)listAccountValue {
    return @"";
}
///流水号
- (NSString *)listSerialNum {
    return StrF(@"流水号：%@", self.order_id);
}

@end
