//
//  WalletRecordDetailObj.m
//  BOB
//
//  Created by colin on 2020/12/9.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletRecordDetailObj.h"

@implementation WalletRecordDetailObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id",
             @"hashString" : @"hash",
    };
}

- (NSString *)order_type {
    NSString *order_type = @"--";
    if ([@"01" isEqualToString:_order_type]) {
        order_type = @"自营订单";
    } else if ([@"02" isEqualToString:_order_type]) {
        order_type = @"供应链订单";
    }
    return order_type;
}

- (NSString *)order_pay_type {
    NSString *order_pay_type = @"--";
    if ([@"01" isEqualToString:_order_pay_type]) {
        order_pay_type = @"支付宝";
    } else if ([@"02" isEqualToString:_order_pay_type]) {
        order_pay_type = @"微信";
    } else if ([@"03" isEqualToString:_order_pay_type]) {
        order_pay_type = @"提现账户余额";
    } else if ([@"04" isEqualToString:_order_pay_type]) {
        if ([@"28" isEqualToString:self.op_type] ||
            [@"34" isEqualToString:self.op_type]) {
            order_pay_type = @"SLA";
        } else {
            order_pay_type = @"助农基金";
        }
    } else if ([@"05" isEqualToString:_order_pay_type]) {
        order_pay_type = @"提货余额";
    }
    return order_pay_type;
}

- (NSString *)status {
    NSString *status = @"--";
    if ([@"13" isEqualToString:self.op_type] ||
        [@"14" isEqualToString:self.op_type] ||
        [@"28" isEqualToString:self.op_type]) {
        if ([@"00" isEqualToString:_status]) {
            status = @"待付款";
        } else if ([@"02" isEqualToString:_status]) {
            status = @"待发货";
        } else if ([@"03" isEqualToString:_status]) {
            status = @"待收货";
        } else if ([@"04" isEqualToString:_status]) {
            status = @"已取消";
        } else if ([@"05" isEqualToString:_status]) {
            status = @"退款";
        } else if ([@"08" isEqualToString:_status]) {
            status = @"已冻结";
        } else if ([@"09" isEqualToString:_status]) {
            status = @"交易完成";
        } else if ([@"10" isEqualToString:_status]) {
            status = @"订单失效";
        }
    } else if ([@"11" isEqualToString:self.op_type] ||
               [@"12" isEqualToString:self.op_type]) {
        if ([@"00" isEqualToString:_status]) {
            status = @"待处理";
        } else if ([@"02" isEqualToString:_status]) {
            status = @"处理中";
        } else if ([@"03" isEqualToString:_status]) {
            status = @"待审核";
        } else if ([@"05" isEqualToString:_status]) {
            status = @"审核冻结";
        } else if ([@"06" isEqualToString:_status]) {
            status = @"审核拒绝";
        } else if ([@"08" isEqualToString:_status]) {
            status = @"提币失败";
        } else if ([@"09" isEqualToString:_status]) {
            status = @"提币成功";
        }
    } else if ([@"31" isEqualToString:self.op_type]) {
        if ([@"00" isEqualToString:_status]) {
            status = @"待开奖";
        } else if ([@"02" isEqualToString:_status]) {
            status = @"正在开奖";
        } else if ([@"08" isEqualToString:_status]) {
            status = @"拼团失败";
        } else if ([@"09" isEqualToString:_status]) {
            status = @"已开奖";
        }
    }
    
    return status;
}

- (NSString *)refund_status {
    NSString *status = @"--";
    if ([@"00" isEqualToString:_status]) {
        status = @"奖金待返还";
    } else if ([@"02" isEqualToString:_status]) {
        status = @"奖金返还中";
    } else if ([@"08" isEqualToString:_status]) {
        status = @"奖金返还成功";
    }
    return status;
}

- (NSString *)pay_num {
    if ([StringUtil isEmpty:_pay_num]) {
        return @"--";
    } else {
        return _pay_num;
    }
}

- (NSString *)pay_balance_num {
    if ([StringUtil isEmpty:_pay_balance_num]) {
        return @"--";
    } else {
        return _pay_balance_num;
    }
}

- (NSString *)sla_discount_rate {
    if ([StringUtil isEmpty:_sla_discount_rate]) {
        return StrF(@"0.0%@", @"%");
    } else {
        CGFloat rate = [_sla_discount_rate floatValue];
        rate = rate * 100;
        return StrF(@"%0.2f%@", rate, @"%");
    }
}

- (NSString *)cre_time {
    if ([StringUtil isEmpty:_cre_time]) {
        return @"--";
    } else {
        return _cre_time;
    }
}

- (NSString *)up_date {
    if ([StringUtil isEmpty:_up_date]) {
        return @"--";
    } else {
        return _up_date;
    }
}

- (NSString *)pay_date {
    if ([StringUtil isEmpty:_pay_date]) {
        return @"--";
    } else {
        return _pay_date;
    }
}

- (NSString *)send_date {
    if ([StringUtil isEmpty:_send_date]) {
        return @"--";
    } else {
        return _send_date;
    }
}

- (NSString *)receive_date {
    if ([StringUtil isEmpty:_receive_date]) {
        return @"--";
    } else {
        return _receive_date;
    }
}

- (NSString *)is_free {
    NSString *is_free = @"--";
    if ([@"0" isEqualToString:_is_free]) {
        is_free = @"否";
    } else if ([@"1" isEqualToString:_is_free]) {
        is_free = @"是";
    }
    return is_free;
}

- (NSString *)free_status {
    NSString *free_status = @"--";
    if ([@"00" isEqualToString:_free_status]) {
        free_status = @"未免单";
    } else if ([@"09" isEqualToString:_free_status]) {
        free_status = @"已免单";
    }
    return free_status;
}

- (NSString *)purse_type {
    NSString *purse_type = @"--";
    if ([@"03" isEqualToString:_purse_type]) {
        purse_type = @"SLA";
    }
    return purse_type;
}

- (NSString *)send_type {
    NSString *string = @"--";
    if ([@"01" isEqualToString:_send_type]) {
        string = @"交易所";
    } else if ([@"02" isEqualToString:_send_type]) {
        string = @"CLS系统";
    }
    return string;
}

- (NSString *)rate {
    if (([@"21" isEqualToString:self.op_type] ||
         [@"22" isEqualToString:self.op_type])) {
        return StrF(@"%@%@", _rate, @"%");
    } else {
        CGFloat rate = [_rate floatValue];
        rate = rate * 100;
        return StrF(@"%0.2f%@", rate, @"%");
    }
}

- (NSString *)remark {
    if ([StringUtil isEmpty:_remark]) {
        return @"--";
    } else {
        return _remark;
    }
}

- (NSString *)is_win {
    NSString *string = @"--";
    if ([@"0" isEqualToString:_is_win]) {
        string = @"否";
    } else if ([@"1" isEqualToString:_is_win]) {
        string = @"是";
    }
    return string;
}

- (NSString *)coupon_status {
    NSString *string = @"--";
    if ([@"00" isEqualToString:_is_win]) {
        string = @"待使用";
    } else if ([@"04" isEqualToString:_is_win]) {
        string = @"已失效";
    } else if ([@"09" isEqualToString:_is_win]) {
        string = @"已使用";
    }
    return string;
}

@end
