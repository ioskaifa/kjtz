//
//  IMXWalletRecord.h
//  BOB
//  钱包相关 记录列表 记录详情 <余额记录, 积分记录, 充值记录, 转换记录>
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol IMXWalletRecord <NSObject>

@optional
#pragma mark - 列表相关
///记录名称
- (NSString *)listTitle;
///记录时间
- (NSString *)listTime;
///记录值
- (NSString *)listValue;
///地址 /账号   -  针对充值
- (NSString *)listAccountValue;
///流水号
- (NSString *)listSerialNum;
///手续费
- (NSString *)listChargeValue;
///状态
- (NSString *)listStatus;

#pragma mark - 详情相关
///详情名称
- (NSString *)detailsTitle;
///详情类型
- (NSString *)detailsType;
///详情充值方式
- (NSString *)detailsModel;
///详情账号key
- (NSString *)detailsAccountKey;
///详情账号value
- (NSString *)detailsAccountValue;
///详情充值时间
- (NSString *)detailsTime;
///详情流水号
- (NSString *)detailsSerialNum;

@end

NS_ASSUME_NONNULL_END
