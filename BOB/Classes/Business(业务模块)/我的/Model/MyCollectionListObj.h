//
//  MyCollectionListObj.h
//  BOB
//
//  Created by colin on 2021/1/16.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyCollectionListObj : BaseObject

@property (nonatomic , copy) NSString              * goods_status;
@property (nonatomic , copy) NSString              * goods_name;
@property (nonatomic , copy) NSString              * goods_id;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , assign) CGFloat              goods_market_cash;
@property (nonatomic , assign) CGFloat              cash_min;
@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , copy) NSString              * user_id;
///01—数字商城 02—免单商城
@property (nonatomic , copy) NSString              * mall_type;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * goods_type;
@property (nonatomic , copy) NSString              * goods_show;
@property (nonatomic , copy) NSString              * chain_goods_id;
///是否删除
@property (nonatomic , assign) BOOL del_status;
///是否只能用SLA支付
@property (nonatomic , assign) BOOL only_sla_pay;

@end

NS_ASSUME_NONNULL_END
