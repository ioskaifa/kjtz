//
//  SendPageInfoObj.h
//  BOB
//
//  Created by mac on 2020/10/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserInfo :BaseObject

@property (nonatomic , copy) NSString              *ID;
///SLA数量
@property (nonatomic , assign) double              sla_num;
///冻结SLA数量
@property (nonatomic , assign) double              freeze_sla_num;

@end

@interface SendPageInfoObj : BaseObject

@property (nonatomic , strong) UserInfo              * userInfo;
///SLA提币手续费率
@property (nonatomic , assign) double sendSlaSingleCahrgeRate;
///SLA提币单笔最低手续费
@property (nonatomic , assign) double              sendSlaMinCahrgeNum;
///最小提币数量
@property (nonatomic , assign) double              sendSlaMinNumber;
///提币地址
@property (nonatomic , copy) NSString              *address;
///提币数量
@property (nonatomic , copy) NSString              *number;

@end

NS_ASSUME_NONNULL_END
