//
//  UserRechargeListObj.m
//  BOB
//
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserRechargeListObj.h"

@implementation UserRechargeListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id",
             @"MDhash" : @"hash"
    };
}

- (NSString *)recharge_type_des {
    if ([@"01" isEqualToString:self.recharge_type]) {
        return @"支付宝";
    } else if ([@"02" isEqualToString:self.recharge_type]) {
        return @"微信";
    } else {
        return @"SLA";
    }
}

#pragma mark - 列表相关
///记录名称
- (NSString *)listTitle {
    return StrF(@"充值-来自%@", self.recharge_type_des);
}
///记录时间
- (NSString *)listTime {
    return StrF(@"时间：%@", self.cre_date);
}
///记录值
- (NSString *)listValue {
    return StrF(@"+%0.2f", self.recharge_num);
}

- (NSString *)listAccountValue {
    if ([@"01" isEqualToString:self.recharge_type]) {
        return @"";
    } else if ([@"02" isEqualToString:self.recharge_type]) {
        return @"";
    } else {
        return @"";
    }
}
///流水号
- (NSString *)listSerialNum {
    return StrF(@"流水号：%@", self.order_id);
}

#pragma mark - 详情相关

@end
