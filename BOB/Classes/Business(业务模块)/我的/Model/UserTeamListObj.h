//
//  UserTeamListObj.h
//  BOB
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserTeamListObj : BaseObject

@property (nonatomic , assign) NSInteger              referer_id;
@property (nonatomic , copy) NSString              * user_tel;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , copy) NSString              * sys_user_account;
@property (nonatomic , copy) NSString              * user_grade;
@property (nonatomic , assign) NSInteger              referer_num;
@property (nonatomic , copy) NSString              * uid;
@property (nonatomic , copy) NSString              * register_type;
@property (nonatomic , copy) NSString              * nick_name;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , assign) NSInteger              under_num;
@property (nonatomic , copy) NSString              * head_photo;
@property (nonatomic , copy) NSString              * status;

@property (nonatomic , copy) NSAttributedString              * nameTel;

@end

NS_ASSUME_NONNULL_END
