//
//  UserExchangeLogListObj.h
//  BOB
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"
#import "IMXWalletRecord.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserExchangeLogListObj : BaseObject<IMXWalletRecord>

@property (nonatomic , assign) CGFloat              exchange_to_num;
@property (nonatomic , assign) NSInteger              user_id;
@property (nonatomic , assign) CGFloat             exchange_num;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * exchange_type;
@property (nonatomic , copy) NSString              * order_id;
@property (nonatomic , assign) CGFloat             exchange_scale;

@end

NS_ASSUME_NONNULL_END
