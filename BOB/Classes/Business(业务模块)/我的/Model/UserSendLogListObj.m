//
//  UserSendLogListObj.m
//  BOB
//
//  Created by mac on 2020/10/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserSendLogListObj.h"

@implementation UserSendLogListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

- (NSString *)formatStatus {
    if (!_formatStatus) {
        if ([@"00" isEqualToString:self.status]) {
            _formatStatus = @"待处理";
        } else if ([@"02" isEqualToString:self.status]) {
            _formatStatus = @"处理中";
        } else if ([@"03" isEqualToString:self.status]) {
            _formatStatus = @"待审核";
        } else if ([@"05" isEqualToString:self.status]) {
            _formatStatus = @"审核冻结";
        } else if ([@"06" isEqualToString:self.status]) {
            _formatStatus = @"审核拒绝";
        } else if ([@"08" isEqualToString:self.status]) {
            _formatStatus = @"提币失败";
        } else if ([@"09" isEqualToString:self.status]) {
            _formatStatus = @"提币成功";
        } else {
            _formatStatus = @"--";
        }
    }
    return _formatStatus;
}

#pragma mark - 列表相关
///记录名称
- (NSString *)listTitle {
    return @"币种-SLA";
}
///记录时间
- (NSString *)listTime {
    return StrF(@"时间：%@", self.cre_date);
}
///记录值
- (NSString *)listValue {
    return StrF(@"-%@", self.account);
}

- (NSString *)listAccountValue {
    return StrF(@"地址：%@", self.address);
}
///流水号
- (NSString *)listSerialNum {
    return StrF(@"流水号：%@", self.order_id);
}

///手续费
- (NSString *)listChargeValue {
    return StrF(@"手续费：-%@", self.charge);
}

- (NSString *)listStatus {
    return StrF(@"提币状态：%@", self.formatStatus);
}

@end
