//
//  UserTeamListObj.m
//  BOB
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserTeamListObj.h"

@implementation UserTeamListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

- (NSAttributedString *)nameTel {
    if (!_nameTel) {
        NSArray *txtArr = @[self.nick_name, StrF(@"(%@)", self.user_tel)];
        NSArray *colorArr = @[[UIColor moBlack], [UIColor colorWithHexString:@"#DADAE6"]];
        NSArray *fontArr = @[[UIFont font15], [UIFont font13]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        _nameTel = att;
    }
    return _nameTel;
}

@end
