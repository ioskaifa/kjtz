//
//  UserAddressListObj.h
//  BOB
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

//"update_time": "20200912185951",
//"address": "北京市·市辖区·平谷区·北京市平谷区太和园甲六号楼五单元九号",
//"create_time": "20200912185734",
//"user_id": "104",
//"address_id": 1,
//"name": "测试",
//"tel": "1888888888",
//"isdefault": "0"

@interface UserAddressListObj : BaseObject

@property (nonatomic , copy) NSString              * update_time;
@property (nonatomic , copy) NSString              * address;
@property (nonatomic , copy) NSString              * create_time;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              *address_id;
@property (nonatomic , copy) NSString              * name;
@property (nonatomic , copy) NSString              * tel;
///是否默认 0—非默认 1—默认
@property (nonatomic , assign) BOOL                isdefault;
///把.替换成空格
@property (nonatomic , copy) NSString              * formatAddress;
///省市区
@property (nonatomic , copy) NSString              * address_area;
///省市区之后的地址
@property (nonatomic , copy) NSString              * address_des;
///错误提示
@property (nonatomic , copy) NSString              * tips;

@end

NS_ASSUME_NONNULL_END
