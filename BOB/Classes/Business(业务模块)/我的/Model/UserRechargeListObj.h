//
//  UserRechargeListObj.h
//  BOB
//
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"
#import "IMXWalletRecord.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserRechargeListObj : BaseObject<IMXWalletRecord>

@property (nonatomic , copy) NSString              * account_type;
@property (nonatomic , copy) NSString              * recharge_type;
@property (nonatomic , assign) NSInteger              user_id;
@property (nonatomic , copy) NSString              * recharge_voucher;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , assign) NSInteger              ID;
@property (nonatomic , copy) NSString              * order_id;
@property (nonatomic , assign) CGFloat              recharge_num;
@property (nonatomic , copy) NSString              * status;

@property (nonatomic , copy) NSString              * order_no;
///SLA数量
@property (nonatomic , copy) NSString              * sla_num;
///SLA兑换USDT价格
@property (nonatomic , copy) NSString              * price1;
///美元兑换人民币价格
@property (nonatomic , copy) NSString              * price2;
///兑换金额
@property (nonatomic , copy) NSString              * money;
///SLA充币编号
@property (nonatomic , copy) NSString              * accept_id;
///充币HASH值
@property (nonatomic , copy) NSString              * MDhash;

@property (nonatomic , copy) NSString              * recharge_type_des;

@end

NS_ASSUME_NONNULL_END
