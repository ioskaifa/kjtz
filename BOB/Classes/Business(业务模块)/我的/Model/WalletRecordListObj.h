//
//  WalletRecordListObj.h
//  BOB
//
//  Created by mac on 2020/9/24.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"
#import "IMXWalletRecord.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletRecordListObj : BaseObject<IMXWalletRecord>

@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , assign) NSInteger              user_id;
@property (nonatomic , copy) NSString              * after_num;
@property (nonatomic , assign) double             num;
@property (nonatomic , copy) NSString              * op_type_code;
@property (nonatomic , copy) NSString              * op_name;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * op_type;
@property (nonatomic , copy) NSString              * before_num;
@property (nonatomic , copy) NSString              * order_id;

@end

NS_ASSUME_NONNULL_END
