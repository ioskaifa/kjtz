//
//  SendPageInfoObj.m
//  BOB
//
//  Created by mac on 2020/10/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SendPageInfoObj.h"

@implementation UserInfo

@end

@implementation SendPageInfoObj

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"UserInfo" : [UserInfo class]};
}

@end
