//
//  MyCollectionCell.m
//  BOB
//
//  Created by colin on 2021/1/16.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "MyCollectionCell.h"

@interface MyCollectionCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *typeLbl;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) UILabel *saleLbl;

@property (nonatomic, strong) UILabel *slaLbl;

@end

@implementation MyCollectionCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 150;
}

- (void)configureView:(MyCollectionListObj *)obj {
    if (![obj isKindOfClass:MyCollectionListObj.class]) {
        return;
    }
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.goods_show]];
    self.titleLbl.text = obj.goods_name;
    CGFloat width = SCREEN_WIDTH - 40 - self.imgView.width;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    self.priceLbl.text = StrF(@"￥%0.2f", obj.cash_min);
    [self.priceLbl autoMyLayoutSize];
    [self configureTypeLbl:obj.mall_type];
    [self configureDelLbl:obj];
    [self configureSlaLbl:obj.only_sla_pay];
}

- (void)configureTypeLbl:(NSString *)mall_type {
    if ([@"01" isEqualToString:mall_type]) {
        self.typeLbl.text = @"数字商城";
        self.typeLbl.backgroundColor = [UIColor blackColor];
    } else {
        self.typeLbl.text = @"免单商城";
        self.typeLbl.backgroundColor = [UIColor moGreen];
    }
}

- (void)configureSlaLbl:(BOOL)only_sla_pay {
    if (only_sla_pay) {
        self.slaLbl.visibility = MyVisibility_Visible;
    } else {
        self.slaLbl.visibility = MyVisibility_Invisible;
    }
}

- (void)configureDelLbl:(MyCollectionListObj *)obj {
    if ([@"09" isEqualToString:obj.goods_status]) {
        self.saleLbl.visibility = MyVisibility_Gone;
    } else {
        self.saleLbl.text = @"已下架";
        self.saleLbl.visibility = MyVisibility_Visible;
    }
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    baseLayout.myTop = 10;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    baseLayout.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:baseLayout];
    
    [baseLayout addSubview:self.imgView];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myLeft = 10;
    layout.myRight = 15;
    layout.myTop = 10;
    layout.myBottom = 10;
    layout.weight = 1;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.typeLbl];
    [layout addSubview:self.titleLbl];
    [layout addSubview:self.saleLbl];
    [layout addSubview:[UIView placeholderVertView]];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myBottom = 0;
    subLayout.myHeight = MyLayoutSize.wrap;
    [layout addSubview:subLayout];
    
    [subLayout addSubview:self.priceLbl];
    [subLayout addSubview:self.slaLbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"免单购物车"];
    imgView.size = CGSizeMake(30, 30);
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myRight = 0;
    [subLayout addSubview:imgView];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        _imgView.myLeft = 15;
        _imgView.myCenterY = 0;
        _imgView.size = CGSizeMake(120, 120);
        _imgView.mySize = _imgView.size;
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.font = [UIFont lightFont15];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _titleLbl.numberOfLines = 2;
        _titleLbl.myLeft = 0;
        _titleLbl.myRight = 0;
        _titleLbl.myTop = 0;
    }
    return _titleLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.font = [UIFont boldFont18];
        _priceLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _priceLbl.myBottom = 5;
    }
    return _priceLbl;
}

- (UILabel *)saleLbl {
    if (!_saleLbl) {
        _saleLbl = [UILabel new];
        _saleLbl.font = [UIFont font12];
        _saleLbl.textColor = [UIColor colorWithHexString:@"#999999"];
        _saleLbl.myTop = 5;
        _saleLbl.myLeft = 0;
        _saleLbl.size = CGSizeMake(55, 20);
        _saleLbl.textAlignment = NSTextAlignmentCenter;
        _saleLbl.backgroundColor = [UIColor colorWithHexString:@"#EEEAEA"];
        [_saleLbl setViewCornerRadius:_saleLbl.height/2.0];
        _saleLbl.mySize = _saleLbl.size;
        _saleLbl.text = @"已下架";
    }
    return _saleLbl;
}

- (UILabel *)typeLbl {
    if (!_typeLbl) {
        _typeLbl = [UILabel new];
        _typeLbl.size = CGSizeMake(70, 25);
        _typeLbl.textColor = [UIColor whiteColor];
        [_typeLbl setViewCornerRadius:2];
        _typeLbl.textAlignment = NSTextAlignmentCenter;
        _typeLbl.text = @"数字商城";
        _typeLbl.backgroundColor = [UIColor blackColor];
        _typeLbl.font = [UIFont font15];
        _typeLbl.mySize = _typeLbl.size;
        _typeLbl.myBottom = 5;
    }
    return _typeLbl;
}

- (UILabel *)slaLbl {
    if (!_slaLbl) {
        _slaLbl = [UILabel new];
        _slaLbl.size = CGSizeMake(70, 15);
        _slaLbl.textAlignment = NSTextAlignmentCenter;
        _slaLbl.text = @"仅支持SLA支付";
        _slaLbl.font = [UIFont systemFontOfSize:8];
        _slaLbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
        _slaLbl.layer.borderWidth = 0.5;
        _slaLbl.layer.borderColor = [UIColor colorWithHexString:@"#FF4757"].CGColor;
        _slaLbl.mySize = _slaLbl.size;
        _slaLbl.myLeft = 5;
        _slaLbl.myCenterY = 0;
    }
    return _slaLbl;
}

@end
