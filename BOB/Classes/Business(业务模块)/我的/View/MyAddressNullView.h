//
//  MyAddressNullView.h
//  BOB
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyAddressNullView : UIView

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
