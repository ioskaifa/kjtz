//
//  STDTableCellView.h
//  BOB
//
//  Created by mac on 2020/9/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STDTableCellView : UIView

- (instancetype)initWithLeftImg:(NSString *)leftImg
                        leftTxt:(NSAttributedString *)leftTxt
                       rightTxt:(NSAttributedString *)rightTxt
                       rightImg:(NSString *)rightImg;

- (void)configureRightImg:(NSString *)rightImg;

@end

NS_ASSUME_NONNULL_END
