//
//  UserAddressListCell.m
//  BOB
//
//  Created by mac on 2020/9/21.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserAddressListCell.h"

@interface UserAddressListCell ()
///姓名+电话
@property (nonatomic, strong) UILabel *nameLbl;
///默认标记
@property (nonatomic, strong) UILabel *defaultLbl;
///地址
@property (nonatomic, strong) UILabel *addressLbl;

@property (nonatomic, strong) MyBaseLayout *layout;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UserAddressListObj *cellObj;

@end

@implementation UserAddressListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 100;
}

- (void)configureView:(UserAddressListObj *)obj {
    if (![obj isKindOfClass:UserAddressListObj.class]) {
        return;
    }
    self.cellObj = obj;
    NSString *txt = StrF(@"%@ %@", obj.name, [StringUtil telNumberFormat344:obj.tel]);
    self.nameLbl.text = txt;
    [self.nameLbl autoMyLayoutSize];
    
    self.addressLbl.text = obj.formatAddress;
    [self.addressLbl autoMyLayoutSize];
    if (obj.isdefault) {
        self.defaultLbl.visibility = MyVisibility_Visible;
    } else {
        self.defaultLbl.visibility = MyVisibility_Gone;
    }
    CGFloat width = SCREEN_WIDTH - 80 - self.imgView.width;
    if (self.addressLbl.width > width) {
        CGSize size = [self.addressLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
        self.addressLbl.size = size;
        self.addressLbl.mySize = self.addressLbl.size;
    }
}

- (void)createUI {
    self.backgroundColor = [UIColor moBackground];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    [layout setViewCornerRadius:10];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 5;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 5;
    [self.contentView addSubview:layout];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.myHeight = MyLayoutSize.wrap;
    subLayout.myLeft = 15;
    subLayout.myRight = 15;
    subLayout.weight = 1;
    subLayout.myCenterY = 0;
    [layout addSubview:subLayout];
    MyLinearLayout *topLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    topLayout.myTop = 0;
    topLayout.myLeft = 0;
    topLayout.myRight = 0;
    topLayout.myHeight = 22;
    [topLayout addSubview:self.nameLbl];
    [topLayout addSubview:[UIView placeholderHorzView]];
    [topLayout addSubview:self.defaultLbl];
    [subLayout addSubview:topLayout];
    [subLayout addSubview:self.addressLbl];
    self.layout = subLayout;
    
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
    line.myWidth = 1;
    line.myTop = 10;
    line.myBottom = 10;
    line.myRight = 15;
    [layout addSubview:line];
        
    [layout addSubview:self.imgView];
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor moBlack];
            object.myCenterY = 0;
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)defaultLbl {
    if (!_defaultLbl) {
        _defaultLbl = ({
            UILabel *object = [UILabel new];
            object.textAlignment = NSTextAlignmentCenter;
            object.size = CGSizeMake(35, 16);
            [object setViewCornerRadius:2];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myRight = 0;
            object.font = [UIFont font11];
            object.textColor = [UIColor whiteColor];
            object.backgroundColor = [UIColor moGreen];
            object.text = @"默认";
            object;
        });
    }
    return _defaultLbl;
}

- (UILabel *)addressLbl {
    if (!_addressLbl) {
        _addressLbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 0;
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#5C5C5C"];
            object.myTop = 10;
            object;
        });
    }
    return _addressLbl;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView autoLayoutImgView:@"编辑收货地址"];
        _imgView.myCenterY = 0;
        _imgView.myRight = 15;
        @weakify(self)
        [_imgView addAction:^(UIView *view) {
            @strongify(self)
            MXRoute(@"AddressManageVC", (@{@"type":@(1), @"obj":self.cellObj}))
        }];
    }
    return _imgView;
}

@end
