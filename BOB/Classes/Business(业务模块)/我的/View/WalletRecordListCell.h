//
//  WalletRecordListCell.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "STDTableViewCell.h"
#import "IMXWalletRecord.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletRecordListCell : STDTableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(id<IMXWalletRecord>)obj;

@end

NS_ASSUME_NONNULL_END
