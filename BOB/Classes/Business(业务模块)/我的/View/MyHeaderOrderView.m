//
//  MyHeaderOrderView.m
//  BOB
//
//  Created by mac on 2020/9/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyHeaderOrderView.h"
#import "AxcAE_TabBarBadge.h"

@interface MyHeaderOrderView ()

@property (nonatomic, strong) NSMutableArray *subViewMArr;

@end

@implementation MyHeaderOrderView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self creaetUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 110;
}

- (void)configureView:(NSDictionary *)dataDic {
    if (![dataDic isKindOfClass:NSDictionary.class]) {
        return;
    }
//    for (SPButton *btn in self.subViewMArr) {
//        if (![btn isKindOfClass:SPButton.class]) {
//            continue;
//        }
//        AxcAE_TabBarBadge *badge = [btn viewWithTag:1];
//        if (![badge isKindOfClass:AxcAE_TabBarBadge.class]) {
//            continue;
//        }
//        NSString *num = dataDic[btn.currentTitle];
//        [badge setBadgeText:num];
//    }
}

- (void)statusBtnClick:(UIButton *)sender {
    if (![sender isKindOfClass:UIButton.class]) {
        return;
    }
//    NSInteger index = 0;
//    if([@"待付款" isEqualToString:sender.currentTitle]) {
//        index = 1;
//    } else if ([@"待发货" isEqualToString:sender.currentTitle]) {
//        index = 2;
//    } else if ([@"待收货" isEqualToString:sender.currentTitle]) {
//        index = 3;
//    } else if ([@"已完成" isEqualToString:sender.currentTitle]) {
//        index = 4;
//    }
//    MXRoute(@"MyOrderVC", @{@"orderStatus":@(index)});
    
    if([@"数字订单" isEqualToString:sender.currentTitle]) {
        MXRoute(@"MyOrderVC", @{@"orderStatus":@(0)});
    } else if ([@"免单订单" isEqualToString:sender.currentTitle]) {
        MXRoute(@"MyOrderVC", @{@"isFree":@(1)})
    } else if ([@"拼团订单" isEqualToString:sender.currentTitle]) {
        MXRoute(@"MGGoodsOrderVC", nil)
    } else if ([@"直播订单" isEqualToString:sender.currentTitle]) {
        MXRoute(@"MyOrderVC", @{@"isLive":@(1)})
    }
}

- (void)creaetUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myTop = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:[self orderLayout]];
}

- (MyBaseLayout *)orderLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    [layout setViewCornerRadius:5];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 5;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    
    MyLinearLayout *topLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    topLayout.myTop = 10;
    topLayout.myLeft = 15;
    topLayout.myRight = 15;
    topLayout.myBottom = 15;
    topLayout.myHeight = MyLayoutSize.wrap;
    [layout addSubview:topLayout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    lbl.text = @"我的订单";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    [topLayout addSubview:lbl];
        
    [topLayout addSubview:[UIView placeholderHorzView]];
    
    SPButton *allBtn = [SPButton new];
    allBtn.imagePosition = SPButtonImagePositionRight;
    allBtn.imageTitleSpace = 5;
    allBtn.titleLabel.font = [UIFont font15];
    [allBtn setTitleColor:[UIColor colorWithHexString:@"#AAAABB"] forState:UIControlStateNormal];
    [allBtn setImage:[UIImage imageNamed:@"Arrow"] forState:UIControlStateNormal];
    [allBtn setTitle:@"全部" forState:UIControlStateNormal];
    [allBtn sizeToFit];
    allBtn.mySize = allBtn.size;
    allBtn.myCenterY = 0;
    allBtn.myRight = 0;
    allBtn.userInteractionEnabled = NO;
//    [topLayout addSubview:allBtn];
//    [topLayout addAction:^(UIView *view) {
//        MXRoute(@"MyOrderVC", @{@"orderStatus":@(0)});
//    }];
    
    MyLinearLayout *statusLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    statusLayout.weight = 1;
    statusLayout.myLeft = 0;
    statusLayout.myRight = 0;
    statusLayout.gravity = MyGravity_Horz_Around;
    [layout addSubview:statusLayout];
//    NSArray *statusArr = @[@"待付款", @"待发货", @"待收货", @"已完成"];
    NSArray *statusArr = @[@"数字订单", @"免单订单", @"拼团订单", @"直播订单"];
    for (NSString *string in statusArr) {
        SPButton *btn = [self.class factoryStatusBtn:string image:string];
        [statusLayout addSubview:btn];
        [self.subViewMArr addObject:btn];
        @weakify(self)
        [btn addAction:^(UIButton *btn) {
            @strongify(self)
            [self statusBtnClick:btn];
        }];
    }
    
    return layout;
}

+ (SPButton *)factoryStatusBtn:(NSString *)title image:(NSString *)img {
    SPButton *btn = [SPButton new];
    btn.imagePosition = SPButtonImagePositionTop;
    btn.imageTitleSpace = 10;
    btn.titleLabel.font = [UIFont font12];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btn setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    [btn sizeToFit];
    CGSize size = btn.size;
    btn.mySize = CGSizeMake(size.width + 30, size.height);
    
    AxcAE_TabBarBadge *badge = [AxcAE_TabBarBadge new];
    badge.tag = 1;
    badge.automaticHidden = YES;    
    badge.frame = CGRectMake(btn.width, -3, badge.width, badge.height);
    [btn addSubview:badge];
    return btn;
}

- (NSMutableArray *)subViewMArr {
    if (!_subViewMArr) {
        _subViewMArr = [NSMutableArray array];
    }
    return _subViewMArr;
}

@end
