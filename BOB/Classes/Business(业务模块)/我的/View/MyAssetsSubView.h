//
//  MyAssetsSubView.h
//  BOB
//
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MyAssetsSubViewType) {
    ///余额
    MyAssetsSubViewTypeBalance    = 0,
    ///积分
    MyAssetsSubViewTypeIntegral   = 1,
};

@interface MyAssetsSubView : UIView
///充值，转换
@property (nonatomic, strong) UIButton *inputBtn;
///进入记录列表
@property (nonatomic, strong) UIButton *listBtn;

- (instancetype)initWithViewType:(MyAssetsSubViewType)type;

- (void)configureView;

@end

NS_ASSUME_NONNULL_END
