//
//  MyHeaderView.h
//  BOB
//
//  Created by mac on 2020/9/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyHeaderOrderView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyHeaderView : UIView

///我的订单
@property (nonatomic, strong) MyHeaderOrderView *orderView;

+ (CGFloat)viewHeight;

- (void)configureView;

@end

NS_ASSUME_NONNULL_END
