//
//  MyToolsView.h
//  BOB
//
//  Created by mac on 2020/10/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyToolsView : UIView

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
