//
//  WalletRecordListView.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMXWalletRecord.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletRecordListView : UIView

- (void)configureView:(id<IMXWalletRecord>)obj;

@end

NS_ASSUME_NONNULL_END
