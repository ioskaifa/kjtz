//
//  MyToolsView.m
//  BOB
//
//  Created by mac on 2020/10/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyToolsView.h"

@implementation MyToolsView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self creaetUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 130;
}

- (void)statusBtnClick:(UIButton *)sender {
    if (![sender isKindOfClass:UIButton.class]) {
        return;
    }
    sender.userInteractionEnabled = NO;
    if ([@"钱包" isEqualToString:sender.currentTitle]) {
        MXRoute(@"SLAWalletVC", nil);
    }  else if ([@"免单订单" isEqualToString:sender.currentTitle]) {
        MXRoute(@"MyOrderVC", @{@"isFree":@(1)})
    } else if ([@"我的免单" isEqualToString:sender.currentTitle]) {
        MXRoute(@"MyFreeShopNameListVC", nil)
    } else if ([@"购物车" isEqualToString:sender.currentTitle]) {
        MXRoute(@"ShoppingCartVC", nil)
    } else if ([@"收藏夹" isEqualToString:sender.currentTitle]) {
        MXRoute(@"MyCollectionVC", nil)
    } else {
        [NotifyHelper showMessageWithMakeText:@"暂未开放"];
    }
    sender.userInteractionEnabled = YES;
}


- (void)creaetUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myTop = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:[self toolLayout]];
}

- (MyBaseLayout *)toolLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    [layout setViewCornerRadius:5];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    lbl.text = @"常用工具";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 15;
    lbl.myLeft = 15;
    lbl.myBottom = 15;
    [layout addSubview:lbl];
    
    MyLinearLayout *statusLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    statusLayout.weight = 1;
    statusLayout.myLeft = 0;
    statusLayout.myRight = 0;
    statusLayout.gravity = MyGravity_Horz_Around;
    [layout addSubview:statusLayout];
    NSArray *statusArr = @[@"钱包", @"收藏夹", @"购物车", @"我的免单"];
    for (NSString *string in statusArr) {
        SPButton *btn = [self.class factoryStatusBtn:string image:string];
        [statusLayout addSubview:btn];
        @weakify(self)
        [btn addAction:^(UIButton *btn) {
            @strongify(self)
            [self statusBtnClick:btn];
        }];
        if ([@"--" isEqualToString:string]) {
            btn.visibility = MyVisibility_Invisible;
        }
    }
    
    return layout;
}

+ (SPButton *)factoryStatusBtn:(NSString *)title image:(NSString *)img {
    SPButton *btn = [SPButton new];
    btn.imagePosition = SPButtonImagePositionTop;
    btn.imageTitleSpace = 10;
    btn.titleLabel.font = [UIFont font12];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btn setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    [btn sizeToFit];
    CGSize size = btn.size;
    btn.mySize = CGSizeMake(size.width + 20, size.height);
    return btn;
}

@end
