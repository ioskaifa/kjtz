//
//  MyCollectionCell.h
//  BOB
//
//  Created by colin on 2021/1/16.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCollectionListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyCollectionCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(MyCollectionListObj *)obj;

@end

NS_ASSUME_NONNULL_END
