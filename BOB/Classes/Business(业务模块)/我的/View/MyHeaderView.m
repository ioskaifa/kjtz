//
//  MyHeaderView.m
//  BOB
//
//  Created by mac on 2020/9/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyHeaderView.h"
#import "MyHeaderAssetsView.h"
#import "MyHeaderManageView.h"
#import "MyToolsView.h"

@interface MyHeaderView ()

@property (nonatomic, strong) MyBaseLayout *baseLayout;
///用户头像
@property (nonatomic, strong) UIImageView *headImgView;
///用户昵称
@property (nonatomic, strong) UILabel *nickNameLbl;
///用户手机
@property (nonatomic, strong) UILabel *telLbl;
///会员等级
@property (nonatomic, strong) UIImageView *levelImgView;
///我的资产
@property (nonatomic, strong) MyHeaderAssetsView *assetsView;
///我的管理
@property (nonatomic, strong) MyHeaderManageView *manageView;
///我的管理
@property (nonatomic, strong) MyToolsView *toolsView;

@property (nonatomic, assign) CGFloat nameMaxWidth;

@end

@implementation MyHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

#pragma mark - Public Method
+ (CGFloat)viewHeight {
    return 520 + safeAreaInset().top;
}

- (void)configureView {
    UserModel *user = UDetail.user;
    if (!user) {
        return;
    }
    
    NSString *headUrl = StrF(@"%@/%@", user.qiniu_domain, user.head_photo);
    [self.headImgView sd_setImageWithURL:[NSURL URLWithString:headUrl]];
    
    self.nickNameLbl.text = user.nickname;
    [self.nickNameLbl sizeToFit];
    if (self.nickNameLbl.width > self.nameMaxWidth) {
        self.nickNameLbl.width = self.nameMaxWidth;
    }
    self.nickNameLbl.mySize = self.nickNameLbl.size;
    
    self.telLbl.text = [StringUtil telNumberFormat344:user.user_tel];
    [self.telLbl sizeToFit];
    self.telLbl.mySize = self.telLbl.size;
        
    if ([@"0" isEqualToString:user.user_grade]) {
        self.levelImgView.visibility = MyVisibility_Gone;
    } else {
        self.levelImgView.visibility = MyVisibility_Visible;
    }
    
    [self.assetsView configureView];
}

#pragma mark - InitUI
- (void)createUI {
    [self addSubview:self.baseLayout];
    [self.baseLayout addSubview:[self bgImgView]];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 10;
    layout.myHeight = MyLayoutSize.wrap;
    [self.baseLayout addSubview:layout];
    
    [layout addSubview:[self userInfoLayout]];
    [layout addSubview:self.orderView];
//    [layout addSubview:self.assetsView];
    [layout addSubview:self.manageView];
    [layout addSubview:self.toolsView];
}

#pragma mark - Init
- (MyBaseLayout *)baseLayout {
    if (!_baseLayout) {
        _baseLayout = [MyFrameLayout new];
        _baseLayout.myTop = 0;
        _baseLayout.myLeft = 0;
        _baseLayout.myRight = 0;
        _baseLayout.myHeight = MyLayoutSize.wrap;
        _baseLayout.backgroundColor = [UIColor moBackground];
    }
    return _baseLayout;
}

#pragma mark - 头部背景图
- (UIView *)bgImgView {
    UIImageView *imgView = [UIImageView new];
    UIImage *img = [UIImage imageNamed:@"我的头部背景"];
    CGSize size = img.size;
    imgView.image = img;
    imgView.size = CGSizeMake(SCREEN_WIDTH, size.height*SCREEN_WIDTH/size.width);
    imgView.mySize = imgView.size;
    imgView.myTop = 0;
    imgView.myLeft = 0;
    return imgView;
}

#pragma mark - 用户信息
- (MyBaseLayout *)userInfoLayout {
    self.nameMaxWidth = SCREEN_WIDTH;
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = safeAreaInset().top + 30;
    layout.myLeft = 20;
    layout.myRight = 10;
    layout.myHeight = self.headImgView.height;
    [layout addSubview:self.headImgView];
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myCenterY = 0;
    rightLayout.myLeft = 10;
    rightLayout.myRight = 0;
    rightLayout.weight = 1;
    [layout addSubview:rightLayout];
    
    MyLinearLayout *nameLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    nameLayout.myLeft = 0;
    nameLayout.myRight = 0;
    nameLayout.myHeight = MyLayoutSize.wrap;
    nameLayout.myBottom = 10;
    [rightLayout addSubview:nameLayout];
    [nameLayout addSubview:self.nickNameLbl];
    [nameLayout addSubview:self.levelImgView];
        
    [nameLayout addSubview:[UIView placeholderHorzView]];
    
    UIButton *btn = [UIButton new];
    [btn setImage:[UIImage imageNamed:@"二维码"] forState:UIControlStateNormal];
    btn.size = CGSizeMake(30, 30);
    btn.myCenterY = 0;
    btn.myLeft = 10;
    [btn addAction:^(UIButton *btn) {
        MXRoute(@"InvitationExVC", nil)
    }];
    [nameLayout addSubview:btn];
        
    btn = [UIButton new];
    [btn setImage:[UIImage imageNamed:@"设置"] forState:UIControlStateNormal];
    btn.size = CGSizeMake(30, 30);
    btn.myCenterY = 0;
    btn.myRight = 0;
    [btn addAction:^(UIButton *btn) {
        MXRoute(@"SettingVC", nil)
    }];
    [nameLayout addSubview:btn];
    self.nameMaxWidth = SCREEN_WIDTH - self.headImgView.width - 30 - btn.width*2 - 25 - self.levelImgView.width;
    [rightLayout addSubview:self.telLbl];
    
    [layout layoutSubviews];
    return layout;
}

- (UIImageView *)headImgView {
    if (!_headImgView) {
        _headImgView = [UIImageView new];
        _headImgView.size = CGSizeMake(60, 60);
        [_headImgView setCircleView];
        _headImgView.backgroundColor = [UIColor whiteColor];
        _headImgView.mySize = _headImgView.size;
    }
    return _headImgView;
}

- (UIImageView *)levelImgView {
    if (!_levelImgView) {
        _levelImgView = [UIImageView autoLayoutImgView:@"会员"];
        _levelImgView.myCenterY = 0;
        _levelImgView.myLeft = 5;
    }
    return _levelImgView;
}

- (UILabel *)nickNameLbl {
    if (!_nickNameLbl) {
        _nickNameLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont18];
            object.textColor = [UIColor blackColor];
            object.myCenterY = 0;
            object;
        });
    }
    return _nickNameLbl;
}

- (UILabel *)telLbl {
    if (!_telLbl) {
        _telLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font14];
            object.textColor = [UIColor colorWithHexString:@"#5B5B66"];
            object.myCenterY = 0;
            object;
        });
    }
    return _telLbl;
}

- (MyHeaderOrderView *)orderView {
    if (!_orderView) {
        _orderView = [MyHeaderOrderView new];
        _orderView.myTop = 15;
        _orderView.myLeft = 0;
        _orderView.myRight = 0;
        _orderView.myHeight = [MyHeaderOrderView viewHeight];
    }
    return _orderView;
}

- (MyHeaderAssetsView *)assetsView {
    if (!_assetsView) {
        _assetsView = [MyHeaderAssetsView new];
        _assetsView.myTop = 10;
        _assetsView.myLeft = 0;
        _assetsView.myRight = 0;
        _assetsView.myHeight = [MyHeaderAssetsView viewHeight];
        [_assetsView addAction:^(UIView *view) {
            MXRoute(@"MyAssetsVC", nil);
        }];
    }
    return _assetsView;
}

- (MyHeaderManageView *)manageView {
    if (!_manageView) {
        _manageView = [MyHeaderManageView new];
        _manageView.myTop = 10;
        _manageView.myLeft = 0;
        _manageView.myRight = 0;
        _manageView.myHeight = [MyHeaderManageView viewHeight];
    }
    return _manageView;
}

- (MyToolsView *)toolsView {
    if (!_toolsView) {
        _toolsView = [MyToolsView new];
        _toolsView.myTop = 10;
        _toolsView.myLeft = 0;
        _toolsView.myRight = 0;
        _toolsView.myHeight = [MyToolsView viewHeight];
    }
    return _toolsView;
}

@end
