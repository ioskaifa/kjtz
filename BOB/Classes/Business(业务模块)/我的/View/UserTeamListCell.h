//
//  UserTeamListCell.h
//  BOB
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTeamListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserTeamListCell : UITableViewCell

@property (nonatomic, strong) UIView *line;

+ (CGFloat)viewHeight;

- (void)configureView:(UserTeamListObj *)obj;

@end

NS_ASSUME_NONNULL_END
