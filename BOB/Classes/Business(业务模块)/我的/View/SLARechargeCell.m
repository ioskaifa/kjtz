//
//  SLARechargeCell.m
//  BOB
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SLARechargeCell.h"

@interface SLARechargeCell ()

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *valueLbl;
///兑换汇率
@property (nonatomic, strong) UILabel *rateLbl;
///充值数量
@property (nonatomic, strong) UILabel *numLbl;
///时间
@property (nonatomic, strong) UILabel *timeLbl;
///流水号
@property (nonatomic, strong) UILabel *serLbl;
///hash值
@property (nonatomic, strong) UILabel *hashLbl;

@end

@implementation SLARechargeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addBottomSingleLine:[UIColor moBackground]];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 210;
}

- (void)configureView:(UserRechargeListObj *)obj {
    if (![obj isKindOfClass:UserRechargeListObj.class]) {
        return;
    }
    self.valueLbl.text = StrF(@"+%@", obj.money);
    [self.valueLbl autoMyLayoutSize];
    
    self.rateLbl.text = StrF(@"兑换汇率：1 USDT≈%@ SLA 1 USD≈%@ CNY", obj.price1, obj.price2);
    [self.rateLbl autoMyLayoutSize];
    
    self.numLbl.text = StrF(@"充值数量：%@", obj.sla_num);
    [self.numLbl autoMyLayoutSize];
    
    self.timeLbl.text = StrF(@"时间：%@", obj.cre_date);
    [self.timeLbl autoMyLayoutSize];
    
    self.serLbl.text = StrF(@"流水号：%@", obj.order_no);
    [self.serLbl autoMyLayoutSize];
    
    self.hashLbl.text = StrF(@"hash值：%@", obj.MDhash);
    CGSize size = [self.hashLbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    self.hashLbl.size = size;
    self.hashLbl.mySize = self.hashLbl.size;
}

- (void)loadContent {
    UserRechargeListObj *obj = self.data;
    [self configureView:obj];
}

- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 15;
    subLayout.myLeft = 15;
    subLayout.myRight = 15;
    subLayout.myBottom = 10;
    subLayout.myHeight = MyLayoutSize.wrap;
    [layout addSubview:subLayout];
    [subLayout addSubview:self.titleLbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    [subLayout addSubview:self.valueLbl];
    
    [layout addSubview:self.rateLbl];
    [layout addSubview:self.numLbl];
    [layout addSubview:self.timeLbl];
    [layout addSubview:self.serLbl];
    [layout addSubview:self.hashLbl];
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor moBlack];
            object.text = @"充值-来自SLA";
            [object sizeToFit];
            object.mySize = object.size;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)valueLbl {
    if (!_valueLbl) {
        _valueLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor moBlack];
            object;
        });
    }
    return _valueLbl;
}

- (UILabel *)rateLbl {
    if (!_rateLbl) {
        _rateLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
            object.myLeft = 15;
            object.myBottom = 6;
            object;
        });
    }
    return _rateLbl;
}

- (UILabel *)numLbl {
    if (!_numLbl) {
        _numLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
            object.myLeft = 15;
            object.myBottom = 6;
            object;
        });
    }
    return _numLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
            object.myLeft = 15;
            object.myBottom = 6;
            object;
        });
    }
    return _timeLbl;
}

- (UILabel *)serLbl {
    if (!_serLbl) {
        _serLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
            object.myLeft = 15;
            object.myBottom = 6;
            object;
        });
    }
    return _serLbl;
}

- (UILabel *)hashLbl {
    if (!_hashLbl) {
        _hashLbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 0;
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
            object.myLeft = 15;
            object;
        });
    }
    return _hashLbl;
}

@end
