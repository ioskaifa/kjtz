//
//  MyHeaderAssetsView.h
//  BOB
//
//  Created by mac on 2020/9/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyHeaderAssetsView : UIView

+ (CGFloat)viewHeight;

- (void)configureView;

@end

NS_ASSUME_NONNULL_END
