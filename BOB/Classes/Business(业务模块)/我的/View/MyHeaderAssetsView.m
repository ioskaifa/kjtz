//
//  MyHeaderAssetsView.m
//  BOB
//
//  Created by mac on 2020/9/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyHeaderAssetsView.h"

@interface MyHeaderAssetsView ()
///余额
@property (nonatomic, strong) UILabel *balanceLbl;
///积分
@property (nonatomic, strong) UILabel *integralLbl;

@end

@implementation MyHeaderAssetsView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self creaetUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 70;
}

- (void)configureView {
    UserModel *user = UDetail.user;
    if (!user) {
        return;
    }
    double balance = user.balance_num;
    NSArray *txtArr = @[StrF(@"%0.2f", balance), @"\n余额(￥)"];
    NSArray *colorArr = @[[UIColor blackColor], [UIColor colorWithHexString:@"#151419"]];
    NSArray *fontArr = @[[UIFont fontWithName:@"DINAlternate-Bold" size:15], [UIFont font12]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 5; // 调整行间距
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    self.balanceLbl.attributedText = att;
    [self.balanceLbl sizeToFit];
    self.balanceLbl.myHeight = self.balanceLbl.height;
    
    double integral = user.score_num;
    txtArr = @[StrF(@"%0.2f", integral), @"\n积分"];
    colorArr = @[[UIColor colorWithHexString:@"#1A1A1A"], [UIColor colorWithHexString:@"#151419"]];
    fontArr = @[[UIFont fontWithName:@"DINAlternate-Bold" size:15], [UIFont font12]];
    att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 5; // 调整行间距
    paragraphStyle.alignment = NSTextAlignmentCenter;
    range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    self.integralLbl.attributedText = att;
    [self.integralLbl sizeToFit];
    self.integralLbl.myHeight = self.balanceLbl.height;
}

- (void)creaetUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myTop = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:[self myAssets]];
}

- (MyBaseLayout *)myAssets {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    [layout setViewCornerRadius:5];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    
    MyLinearLayout *leftLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    leftLayout.myLeft = 0;
    leftLayout.myRight = 30;
    leftLayout.weight = 1;
    leftLayout.myHeight = MyLayoutSize.fill;
    leftLayout.gravity = MyGravity_Horz_Stretch;
    [layout addSubview:leftLayout];
    [leftLayout addSubview:self.balanceLbl];
    [leftLayout addSubview:self.integralLbl];
    
    SPButton *btn = [SPButton new];
    btn.userInteractionEnabled = NO;
    btn.imagePosition = SPButtonImagePositionTop;
    btn.imageTitleSpace = 5;
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    btn.titleLabel.font = [UIFont font12];
    [btn setTitleColor:[UIColor colorWithHexString:@"#151419"] forState:UIControlStateNormal];
    [btn setTitle:@"我的资产" forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"我的资产"] forState:UIControlStateNormal];
    [btn sizeToFit];
    CGSize size = btn.size;
    btn.myCenterY = 0;
    btn.size = CGSizeMake(size.width + 30, size.height);
    btn.mySize = btn.size;
    btn.myRight = 15;    
    [layout addSubview:btn];
    
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"我的资产分割线"];
    [imgView sizeToFit];
    [self addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(imgView.size);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.right.mas_equalTo(self.mas_right).offset(- (45 + btn.width));
    }];
    
    return layout;
}

- (UILabel *)balanceLbl {
    if (!_balanceLbl) {
        _balanceLbl = [UILabel new];
        _balanceLbl.numberOfLines = 0;
        _balanceLbl.textAlignment = NSTextAlignmentCenter;
        _balanceLbl.myCenterY = 0;
    }
    return _balanceLbl;
}

- (UILabel *)integralLbl {
    if (!_integralLbl) {
        _integralLbl = [UILabel new];
        _integralLbl.numberOfLines = 0;
        _integralLbl.textAlignment = NSTextAlignmentCenter;
        _integralLbl.myCenterY = 0;
    }
    return _integralLbl;
}

@end
