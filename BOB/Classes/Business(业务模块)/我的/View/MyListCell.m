//
//  MyListCell.m
//  BOB
//
//  Created by mac on 2020/9/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyListCell.h"
#import "JSBadgeView.h"
#import "LcwlChat.h"

@interface MyListCell ()

@property (nonatomic, strong) UIImageView *leftImgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UIImageView *rightImgView;

@property(nonatomic,strong) JSBadgeView *badgeView;

@end

@implementation MyListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self createUI];
    }
    return self;
}

#pragma mark - Public Method
- (void)configureView:(NSDictionary *)obj {
    if (![obj isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    if (obj[@"leftImg"]) {
        self.leftImgView.image = [UIImage imageNamed:obj[@"leftImg"]];
    }
    
    if (obj[@"title"]) {
        self.titleLbl.text = [NSString stringWithFormat:@"%@ ", obj[@"title"]];
    }
    
    if (obj[@"rightImg"]) {
        self.rightImgView.image = [UIImage imageNamed:obj[@"rightImg"]];
    }
    
    if ([obj[@"title"] isEqualToString:@"我的消息"]) {
        int num = [[LcwlChat shareInstance].chatManager getServiceAllUnReadNums];
        if(num > 0) {
            self.badgeView.badgeText = toStr(num);
            self.badgeView.hidden = NO;
        } else {
            self.badgeView.hidden = YES;
        }
        
    }
}

+ (CGFloat)viewHeight {
    return 50;
}

#pragma mark - InitUI
- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    MyLinearLayout *lineLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    lineLayout.myTop = 0;
    lineLayout.myLeft = 0;
    lineLayout.myRight = 0;
    lineLayout.myHeight = [self.class viewHeight];
    [layout addSubview:lineLayout];
    
    [lineLayout addSubview:self.leftImgView];
    [lineLayout addSubview:self.titleLbl];
    [lineLayout addSubview:self.rightImgView];
        
    [layout addSubview:self.line];
    
    [self.contentView addArrowIconWithRightOffset:-20];
}

#pragma mark - Init
- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        _leftImgView = [UIImageView new];
        _leftImgView.size = CGSizeMake(18, 18);
        _leftImgView.mySize = _leftImgView.size;
        _leftImgView.myLeft = 15;
        _leftImgView.myCenterY = 0;
        
        
    }
    return _leftImgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.font = [UIFont font15];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _titleLbl.myHeight = 15;
        _titleLbl.myWidth = MyLayoutSize.wrap;
        _titleLbl.myLeft = 10;
        _titleLbl.myCenterY = 0;
        
        [_titleLbl addSubview:self.badgeView];
    }
    return _titleLbl;
}

- (UIImageView *)rightImgView {
    if (!_rightImgView) {
        _rightImgView = [UIImageView new];
        //_rightImgView.mySize = CGSizeMake(10, 10);
//        _rightImgView.myCenterY = 0;
//        _rightImgView.myRight = 15;
        _rightImgView.useFrame = YES;
    }
    return _rightImgView;
}

- (UIView *)line {
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = [UIColor colorWithHexString:@"#F2F3FA"];
        _line.myTop = 0;
        _line.myLeft = 15;
        _line.myRight = 15;
        _line.myHeight = 1;
    }
    return _line;
}

-(JSBadgeView* )badgeView{
    if (_badgeView == nil) {
        _badgeView = [[JSBadgeView alloc] initWithParentView:self.titleLbl alignment:JSBadgeViewAlignmentTopRight frame:CGRectZero];
        _badgeView.hidden = YES;
        //_badgeView.badgeText = @"99";
        _badgeView.userInteractionEnabled = NO;
        [_badgeView setFrame:CGRectMake(0, -5, 10, 10)];
    }
    return _badgeView;
}
@end
