//
//  FeedBackTypeView.h
//  BOB
//
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeedBackTypeView : UIView

@property (nonatomic, strong) UIImageView *closeImgView;

@property (nonatomic, copy) FinishedBlock block;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
