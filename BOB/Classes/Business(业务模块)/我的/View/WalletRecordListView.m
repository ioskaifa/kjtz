//
//  WalletRecordListView.m
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletRecordListView.h"

@interface WalletRecordListView ()

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *timeLbl;

@property (nonatomic, strong) UILabel *valueLbl;
///流水号
@property (nonatomic, strong) UILabel *serialNumLbl;
///账号
@property (nonatomic, strong) UILabel *accountLbl;
///手续费
@property (nonatomic, strong) UILabel *serviceChargeLbl;
///状态
@property (nonatomic, strong) UILabel *statusLbl;

@end

@implementation WalletRecordListView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)configureView:(id<IMXWalletRecord>)obj {
    if (![obj conformsToProtocol:@protocol(IMXWalletRecord)]) {
        return;
    }
    self.titleLbl.text = [obj listTitle];
    [self.titleLbl sizeToFit];
    self.titleLbl.mySize = self.titleLbl.size;
    
    self.serviceChargeLbl.text = [obj listChargeValue];
    [self.serviceChargeLbl sizeToFit];
    self.serviceChargeLbl.mySize = self.serviceChargeLbl.size;
    
    self.timeLbl.text = [obj listTime];
    [self.timeLbl sizeToFit];
    self.timeLbl.mySize = self.timeLbl.size;
    
    self.valueLbl.text = [obj listValue];
    [self.valueLbl sizeToFit];
    self.valueLbl.mySize = self.valueLbl.size;
    
    self.accountLbl.text = [obj listAccountValue];
    CGSize size = [self.accountLbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    self.accountLbl.size = size;
    self.accountLbl.mySize = self.accountLbl.size;
    if (CGSizeEqualToSize(self.accountLbl.size, CGSizeZero)) {
        self.accountLbl.visibility = MyVisibility_Gone;
    }
    
    self.serialNumLbl.text = [obj listSerialNum];
    [self.serialNumLbl autoMyLayoutSize];
    if (CGSizeEqualToSize(self.serialNumLbl.size, CGSizeZero)) {
        self.serialNumLbl.visibility = MyVisibility_Gone;
    }
    
    if ([obj respondsToSelector:@selector(listStatus)]) {
        self.statusLbl.visibility = MyVisibility_Visible;
        self.statusLbl.text = [obj listStatus];
        [self.statusLbl autoMyLayoutSize];
        if (CGSizeEqualToSize(self.statusLbl.size, CGSizeZero)) {
            self.statusLbl.visibility = MyVisibility_Gone;
        }
    } else {
        self.statusLbl.visibility = MyVisibility_Gone;
    }
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myCenterY = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self addSubview:layout];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 15;
    subLayout.myRight = 15;
    subLayout.myBottom = 10;
    subLayout.myHeight = MyLayoutSize.wrap;
    [layout addSubview:subLayout];
    [subLayout addSubview:self.titleLbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    [subLayout addSubview:self.valueLbl];
    
    [layout addSubview:self.serviceChargeLbl];
    [layout addSubview:self.accountLbl];
    [layout addSubview:self.timeLbl];
    [layout addSubview:self.serialNumLbl];
    [layout addSubview:self.statusLbl];
}

#pragma mark - Init
- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor moBlack];
            object.myLeft = 0;
            object.myCenterY = 0;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myLeft = 15;
            object.myRight = 15;
            object.myHeight = 20;
            object.myBottom = 8;
            object;
        });
    }
    return _timeLbl;
}

- (UILabel *)accountLbl {
    if (!_accountLbl) {
        _accountLbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 0;
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myLeft = 15;
            object.myRight = 15;
            object.myHeight = 20;
            object.myBottom = 8;
            object;
        });
    }
    return _accountLbl;
}

- (UILabel *)serialNumLbl {
    if (!_serialNumLbl) {
        _serialNumLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myLeft = 15;
            object.myRight = 15;
            object.myHeight = 20;
            object.myBottom = 8;
            object;
        });
    }
    return _serialNumLbl;
}

- (UILabel *)valueLbl {
    if (!_valueLbl) {
        _valueLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldSystemFontOfSize:18];
            object.textColor = [UIColor moBlack];
            object.myLeft = 15;
            object;
        });
    }
    return _valueLbl;
}

- (UILabel *)serviceChargeLbl {
    if (!_serviceChargeLbl) {
        _serviceChargeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myLeft = 15;
            object.myRight = 15;
            object.myRight = 0;
            object.myHeight = 20;
            object.myBottom = 8;
            object;
        });
    }
    return _serviceChargeLbl;
}

- (UILabel *)statusLbl {
    if (!_statusLbl) {
        _statusLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font13];
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myLeft = 15;
            object.myRight = 15;
            object.myRight = 0;
            object.myHeight = 20;
            object.myBottom = 8;
            object;
        });
    }
    return _statusLbl;
}

@end
