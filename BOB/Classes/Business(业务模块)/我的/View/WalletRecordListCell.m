//
//  WalletRecordListCell.m
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletRecordListCell.h"
#import "WalletRecordListView.h"

@interface WalletRecordListCell ()

@property (nonatomic, strong) WalletRecordListView *listView;

@end

@implementation WalletRecordListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
        [self addBottomSingleLine:[UIColor moBackground]];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 125;
}

- (void)loadContent {
    id<IMXWalletRecord> obj = self.data;
    [self.listView configureView:obj];
}

- (void)configureView:(id<IMXWalletRecord>)obj {
    [self.listView configureView:obj];
}

- (void)createUI {
    [self.contentView addSubview:self.listView];
    [self.listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
}

- (WalletRecordListView *)listView {
    if (!_listView) {
        _listView = [WalletRecordListView new];
    }
    return _listView;
}
@end
