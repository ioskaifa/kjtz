//
//  MyHeaderManageView.m
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyHeaderManageView.h"

@implementation MyHeaderManageView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 150;
}

- (void)createUI {
    MyFrameLayout *baseLayout = [MyFrameLayout new];
    baseLayout.backgroundColor = [UIColor whiteColor];
    [baseLayout setViewCornerRadius:5];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 10;
    baseLayout.myRight = 10;
    baseLayout.myBottom = 0;
    [self addSubview:baseLayout];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 10;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 10;
    layout.gravity = MyGravity_Vert_Stretch;
    [baseLayout addSubview:layout];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 0;
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.gravity = MyGravity_Horz_Stretch;
    MyBaseLayout *subSubLayout = [self.class factoryLayout:@"数字系统" describe:@"数字系统" img:@"我的CSLA"];
    [subSubLayout addAction:^(UIView *view) {
        [NotifyHelper showMessageWithMakeText:@"暂未开放"];
//        MXRoute(@"MBWebVC", (@{@"url":@"https://ibd.qbccwebsites.com/", @"navTitle":@"数字系统"}));
    }];
    [subLayout addSubview:subSubLayout];
    subSubLayout = [self.class factoryLayout:@"直播广场" describe:@"更多直播等你来" img:@"直播广场"];
    [subSubLayout addAction:^(UIView *view) {
//        [NotifyHelper showMessageWithMakeText:@"暂未开放"];
//        MXRoute(@"LiveListVC",nil);
//        MXRoute(@"TCRoomListViewController",nil);
    }];
    [subLayout addSubview:subSubLayout];
    [layout addSubview:subLayout];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 0;
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.gravity = MyGravity_Horz_Stretch;
    MyBaseLayout *addressLayout = [self.class factoryLayout:@"我的社群" describe:@"查看社群好友" img:@"我的团队"];
    [addressLayout addAction:^(UIView *view) {
        MXRoute(@"MyTeamVC", nil)
    }];
    [subLayout addSubview:addressLayout];
    
    addressLayout = [self.class factoryLayout:@"推广中心" describe:@"邀请好友赚收益" img:@"推广中心"];
    [addressLayout addAction:^(UIView *view) {
        MXRoute(@"InvitationExVC", nil)
    }];
    [subLayout addSubview:addressLayout];
    [layout addSubview:subLayout];
    
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    [baseLayout addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.top.mas_equalTo(baseLayout.mas_top).mas_offset(10);
        make.bottom.mas_equalTo(baseLayout.mas_bottom).mas_offset(-10);
        make.centerX.mas_equalTo(baseLayout.mas_centerX);
    }];
    
    view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    [baseLayout addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(baseLayout.mas_left).mas_offset(10);
        make.right.mas_equalTo(baseLayout.mas_right).mas_offset(-10);
        make.centerY.mas_equalTo(baseLayout.mas_centerY);
    }];
}

+ (MyBaseLayout *)factoryLayout:(NSString *)title describe:(NSString *)des img:(NSString *)img {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myBottom = 0;
    layout.myLeft = 10;
    layout.myWidth = ((SCREEN_WIDTH - 30 - 10) / 2.0);    
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 0;
    NSArray *txtArr = @[title, StrF(@"\n%@", des)];
    NSArray *colorArr = @[[UIColor blackColor], [UIColor colorWithHexString:@"#AAAABB"]];
    NSArray *fontArr = @[[UIFont boldFont15], [UIFont font13]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 10; // 调整行间距
//    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
    [lbl sizeToFit];
    lbl.myHeight = lbl.height;
    lbl.weight = 1;
    lbl.myCenterY = 0;
    [layout addSubview:lbl];

    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:img];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myRight = 30;
    [layout addSubview:imgView];
    
    return layout;
}

@end
