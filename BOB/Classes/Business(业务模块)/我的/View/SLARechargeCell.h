//
//  SLARechargeCell.h
//  BOB
//
//  Created by mac on 2020/9/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "STDTableViewCell.h"
#import "UserRechargeListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface SLARechargeCell : STDTableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(UserRechargeListObj *)obj;

@end

NS_ASSUME_NONNULL_END
