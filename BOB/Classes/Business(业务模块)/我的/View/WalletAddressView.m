//
//  WalletAddressView.m
//  BOB
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "WalletAddressView.h"
#import "UIImage+Color.h"

@interface WalletAddressView ()

@property (nonatomic, strong) UIImageView *qrCode;

@property (nonatomic,strong) UIButton *submitBtn;

@end

@implementation WalletAddressView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 480;
}

- (void)createUI {
    MyFrameLayout *baseLayout = [MyFrameLayout new];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self addSubview:baseLayout];
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"logo圆"];
    imgView.myCenterX = 0;
    imgView.myTop = 0;
    
    UIImageView *closeImgView = [UIImageView autoLayoutImgView:@"钱包close"];
    closeImgView.myCenterX = 0;
    closeImgView.myBottom = 0;
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    [layout setViewCornerRadius:20];
    layout.myTop = imgView.height/2.0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = closeImgView.height + 20;
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
    lbl.font = [UIFont font13];
    lbl.text = @"我的钱包地址";
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myBottom = 10;
    lbl.myTop = imgView.height/2.0;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.textColor = [UIColor moBlack];
    lbl.font = [UIFont font13];
    lbl.text = UDetail.user.eth_address?:@"";
    
    CGSize size = [lbl sizeThatFits:CGSizeMake(200, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myBottom = 20;
    [layout addSubview:lbl];
    
    [layout addSubview:self.qrCode];
    
    [layout addSubview:[UIView placeholderVertView]];
    
    [layout addSubview:self.submitBtn];
    
    [baseLayout addSubview:layout];
    [baseLayout addSubview:imgView];
    [baseLayout addSubview:closeImgView];
    self.closeImgView = closeImgView;
}

- (UIImageView *)qrCode {
    if (!_qrCode) {
        _qrCode = [UIImageView new];
        UIImage *qrImg = [UIImage encodeQRImageWithContent:UDetail.user.eth_address?:@"" size:CGSizeMake(180, 180)];
        _qrCode.image = qrImg;
        _qrCode.size = CGSizeMake(180, 180);
        _qrCode.mySize = _qrCode.size;
        _qrCode.myCenterX = 0;
    }
    return _qrCode;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(180, 40);
            [object setViewCornerRadius:5];
            [object setBackgroundColor:[UIColor moGreen]];
            object.titleLabel.font = [UIFont font16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setTitle:Lang(@"复制钱包地址") forState:UIControlStateNormal];
            object.myCenterX = 0;
            object.mySize = object.size;
            object.myBottom = 20;
            [object addAction:^(UIButton *btn) {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = UDetail.user.eth_address?:@"";
                [NotifyHelper showMessageWithMakeText:@"复制成功"];
            }];
            object;
        });
    }
    return _submitBtn;
}

@end
