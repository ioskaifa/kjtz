//
//  MyListCell.h
//  BOB
//
//  Created by mac on 2020/9/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyListCell : UITableViewCell

@property (nonatomic, strong) UIView *line;

- (void)configureView:(NSDictionary *)obj;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
