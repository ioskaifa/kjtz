//
//  UserTeamListCell.m
//  BOB
//
//  Created by mac on 2020/9/25.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserTeamListCell.h"

@interface UserTeamListCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *timeLbl;

@property (nonatomic, strong) UILabel *levelLbl;

@end

@implementation UserTeamListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 75;
}

- (void)configureView:(UserTeamListObj *)obj {
    if (![obj isKindOfClass:UserTeamListObj.class]) {
        return;
    }
    NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, obj.head_photo);
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    self.titleLbl.attributedText = obj.nameTel;
    [self.titleLbl autoMyLayoutSize];
    
    self.timeLbl.text = obj.cre_date;
    [self.timeLbl autoMyLayoutSize];
    
    self.levelLbl.text = StrF(@"V%@", obj.user_grade);
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.imgView];
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.myLeft = 10;
    subLayout.myRight = 10;
    subLayout.myCenterY = 0;
    subLayout.weight = 1;
    subLayout.myHeight = MyLayoutSize.wrap;
    [layout addSubview:subLayout];
    [subLayout addSubview:self.titleLbl];
    [subLayout addSubview:self.timeLbl];
    
    [layout addSubview:self.levelLbl];
    
    [self.contentView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.titleLbl.mas_left);
        make.right.mas_equalTo(layout.mas_right);
    }];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            object.size = CGSizeMake(40, 40);
            [object setViewCornerRadius:object.height/2.0];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 15;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor moBlack];
            object.font = [UIFont font15];
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.myBottom = 10;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
            object.font = [UIFont font13];
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _timeLbl;
}

- (UILabel *)levelLbl {
    if (!_levelLbl) {
        _levelLbl = ({
            UILabel *object = [UILabel new];
            object.size = CGSizeMake(35, 16);
            [object setViewCornerRadius:2];
            object.textAlignment = NSTextAlignmentCenter;
            object.textColor = [UIColor moBlack];
            object.font = [UIFont font12];
            object.backgroundColor = [UIColor colorWithHexString:@"#E3E3EB"];
            object.myRight = 15;
            object.myCenterY = 0;
            object.visibility = MyVisibility_Invisible;
            object;
        });
    }
    return _levelLbl;
}

- (UIView *)line {
    if (!_line) {
        _line = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor moBackground];
            object;
        });
    }
    return _line;
}

@end
