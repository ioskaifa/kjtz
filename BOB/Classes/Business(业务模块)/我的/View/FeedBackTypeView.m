//
//  FeedBackTypeView.m
//  BOB
//
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FeedBackTypeView.h"

@interface FeedBackTypeView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@end

@implementation FeedBackTypeView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 450;
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = UITableViewCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UILabel *lbl = [cell.contentView viewWithTag:1000];
    if (!lbl) {
        lbl = [UILabel new];
        lbl.tag = 1000;
        lbl.textColor = [UIColor moBlack];
        lbl.font = [UIFont boldFont15];
        [cell.contentView addSubview:lbl];
        UIView *superView = cell.contentView;
        [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(superView.mas_left).offset(20);
            make.right.mas_equalTo(superView.mas_right).offset(-20);
            make.height.mas_equalTo(20);
            make.centerY.mas_equalTo(superView);
        }];
        [cell addBottomSingleLineAndLeftRightOffset:20 color:[UIColor moBackground]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    UILabel *lbl = [cell.contentView viewWithTag:1000];
    if (!lbl) {
        return;
    }
    NSString *txt = self.dataSourceArr[indexPath.row];
    lbl.text = txt;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    if (self.block) {
        self.block(self.dataSourceArr[indexPath.row]);
    }
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [layout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font18];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"支付订单";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 15;
    lbl.myBottom = 15;
    lbl.myCenterX = 0;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView fullLine]];
    [layout addSubview:self.tableView];
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.closeImgView.size);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = ({
            UITableView *object = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
            object.separatorStyle = UITableViewCellSeparatorStyleNone;
            object.delegate = self;
            object.dataSource = self;
            object.showsVerticalScrollIndicator = NO;
            object.showsHorizontalScrollIndicator = NO;
            Class cls = UITableViewCell.class;
            [object registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
            object.rowHeight = 50;
            object.myLeft = 0;
            object.myRight = 0;
            object.weight = 1;
            object.myBottom = safeAreaInsetBottom() + 20;
            object;
        });
    }
    return _tableView;
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

- (NSArray *)dataSourceArr {
    if (!_dataSourceArr) {
        _dataSourceArr = @[@"闪退/卡顿",@"支付/收益",@"重复扣款",
                           @"账户安全",@"服务投诉",@"新功能建议",@"其他"];
    }
    return _dataSourceArr;
}

@end
