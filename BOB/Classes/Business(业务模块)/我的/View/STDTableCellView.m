//
//  STDTableCellView.m
//  BOB
//
//  Created by mac on 2020/9/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "STDTableCellView.h"

@interface STDTableCellView ()

@property (nonatomic, strong) UIImageView *leftImgView;

@property (nonatomic, strong) UILabel *leftLbl;

@property (nonatomic, strong) UILabel *rightLbl;

@property (nonatomic, strong) UIImageView *rightImgView;

@property (nonatomic, copy) NSString *leftImg;

@property (nonatomic, copy) NSAttributedString *leftTxt;

@property (nonatomic, copy) NSAttributedString *rightTxt;

@property (nonatomic, copy) NSString *rightImg;

@end

@implementation STDTableCellView

- (instancetype)initWithLeftImg:(NSString *)leftImg
                        leftTxt:(NSAttributedString *)leftTxt
                       rightTxt:(NSAttributedString *)rightTxt
                       rightImg:(NSString *)rightImg {
    if (self = [super init]) {
        self.leftImg = leftImg;
        self.leftTxt = leftTxt;
        self.rightTxt = rightTxt;
        self.rightImg = rightImg;
        [self createUI];
    }
    return self;
}

- (void)configureRightImg:(NSString *)rightImg {
    self.rightImgView.image = [UIImage imageNamed:rightImg];
    [self.rightImgView sizeToFit];
    self.rightImgView.mySize = self.rightImgView.size;
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    [layout addSubview:self.leftImgView];
    [layout addSubview:self.leftLbl];
    [layout addSubview:[UIView placeholderHorzView]];
    [layout addSubview:self.rightLbl];
    [layout addSubview:self.rightImgView];        
}

- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        _leftImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:self.leftImg];
            object.myLeft = 0;
            object.myCenterY = 0;
            object;
        });
    }
    return _leftImgView;
}

- (UILabel *)leftLbl {
    if (!_leftLbl) {
        _leftLbl = ({
            UILabel *object = [UILabel new];
            object.attributedText = self.leftTxt;
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 15;
            object;
        });
    }
    return _leftLbl;
}

- (UIImageView *)rightImgView {
    if (!_rightImgView) {
        _rightImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:self.rightImg];
            object.myRight = 0;
            object.myCenterY = 0;
            object;
        });
    }
    return _rightImgView;
}

- (UILabel *)rightLbl {
    if (!_rightLbl) {
        _rightLbl = ({
            UILabel *object = [UILabel new];
            object.attributedText = self.rightTxt;
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myRight = 15;
            object;
        });
    }
    return _rightLbl;
}

@end
