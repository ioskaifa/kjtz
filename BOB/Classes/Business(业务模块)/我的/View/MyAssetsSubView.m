//
//  MyAssetsSubView.m
//  BOB
//
//  Created by mac on 2020/9/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyAssetsSubView.h"

@interface MyAssetsSubView ()

@property (nonatomic, strong) UILabel *valueLbl;

@property (nonatomic, assign) MyAssetsSubViewType viewType;

@end

@implementation MyAssetsSubView

- (instancetype)initWithViewType:(MyAssetsSubViewType)type {
    if (self = [super init]) {
        self.viewType = type;
        [self createUI];
    }
    return self;
}

- (void)configureView {
    self.valueLbl.text = StrF(@"%0.2f", [self amount]);
    [self.valueLbl sizeToFit];
    self.valueLbl.mySize = self.valueLbl.size;
}

///标识图片
- (NSString *)tagImage {
    NSString *string = @"";
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            string = @"我的余额";
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            string = @"我的积分";
            break;
        }
        default:
            string = @"我的余额";
            break;
    }
    return string;
}

///金额
- (double)amount {
    double amount = 0;
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            amount = UDetail.user.balance_num;
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            amount = UDetail.user.score_num;
            break;
        }
        default:
            amount = UDetail.user.balance_num;
            break;
    }
    return amount;
}

- (NSString *)unit {
    NSString *string = @"";
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            string = @"余额（元）";
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            string = @"积分";
            break;
        }
        default:
            string = @"余额（元）";
            break;
    }
    return string;
}

- (NSString *)btnTitle {
    NSString *string = @"";
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            string = @"充值";
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            string = @"转换";
            break;
        }
        default:
            string = @"充值";
            break;
    }
    return string;
}

- (UIColor *)btnBorderColor {
    UIColor *color = nil;
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            color = [UIColor moGreen];
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            color = [UIColor colorWithHexString:@"#E3E3EB"];
            break;
        }
        default:
            color = [UIColor moGreen];
            break;
    }
    return color;
}

- (UIColor *)btnTitleColor {
    UIColor *color = nil;
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            color = [UIColor whiteColor];
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            color = [UIColor colorWithHexString:@"#46474D"];
            break;
        }
        default:
            color = [UIColor moGreen];
            break;
    }
    return color;
}

- (UIColor *)btnBGColor {
    UIColor *color = nil;
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            color = [UIColor moGreen];
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            color = [UIColor whiteColor];
            break;
        }
        default:
            color = [UIColor moGreen];
            break;
    }
    return color;
}

- (NSString *)listTitle {
    NSString *string = @"";
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            string = @"余额记录";
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            string = @"积分记录";
            break;
        }
        default:
            string = @"余额记录";
            break;
    }
    return string;
}

- (UIImage *)listImage {
    UIImage *image = nil;
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            image = [UIImage imageNamed:@"余额记录_arrow"];
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            image = [UIImage imageNamed:@"积分记录_arrow"];
            break;
        }
        default:
            image = [UIImage imageNamed:@"余额记录_arrow"];
            break;
    }
    return image;
}

- (UIColor *)listColor {
    UIColor *color = nil;
    switch (self.viewType) {
        case MyAssetsSubViewTypeBalance:{
            color = [UIColor moGreen];
            break;
        }
        case MyAssetsSubViewTypeIntegral:{
            color = [UIColor colorWithHexString:@"#46474D"];
            break;
        }
        default:
            color = [UIColor moGreen];
            break;
    }
    return color;
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    [layout setViewCornerRadius:10];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont fontWithName:@"DINAlternate-Bold" size:40];
    lbl.textColor = [UIColor moBlack];
    lbl.text = StrF(@"%0.2f", [self amount]);
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myTop = 35;
    self.valueLbl = lbl;
    [layout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor moBlack];
    lbl.text = [self unit];
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myTop = 10;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView placeholderVertView]];
    
    UIButton *btn = [UIButton new];
    btn.size = CGSizeMake(155, 42);
    [btn setViewCornerRadius:btn.height/2.0];
    [btn setTitleColor:[self btnTitleColor] forState:UIControlStateNormal];
    btn.layer.borderColor = [self btnBorderColor].CGColor;
    btn.layer.borderWidth = 1;
    [btn setBackgroundColor:[self btnBGColor]];
    [btn setTitle:[self btnTitle] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldFont15];
    btn.myCenterX = 0;
    btn.myBottom = 10;
    self.inputBtn = btn;
    [layout addSubview:btn];
    
    SPButton *listBtn = [SPButton new];
    listBtn.imagePosition = SPButtonImagePositionRight;
    listBtn.imageTitleSpace = 10;
    [listBtn setImage:[self listImage] forState:UIControlStateNormal];
    [listBtn setTitle:[self listTitle] forState:UIControlStateNormal];
    [listBtn setTitleColor:[self listColor] forState:UIControlStateNormal];
    listBtn.titleLabel.font = [UIFont font15];
    [listBtn sizeToFit];
    listBtn.mySize = listBtn.size;
    listBtn.myCenterX = 0;
    listBtn.myBottom = 30;
    self.listBtn = listBtn;
    [layout addSubview:listBtn];
    
    UIImageView *imgView = [UIImageView autoLayoutImgView:[self tagImage]];
    [self addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(imgView.size);
        make.top.mas_equalTo(self.mas_top).offset(10);
        make.right.mas_equalTo(self.mas_right).offset(-10);
    }];
}

@end
