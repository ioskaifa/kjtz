//
//  ChooseContactAtVC.h
//  BOB
//
//  Created by mac on 2020/8/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^SelectedBlock)(NSArray * _Nullable datasArr);

NS_ASSUME_NONNULL_BEGIN

@interface ChooseContactAtVC : BaseViewController

///是否展示@所有人选项
@property (nonatomic, assign) BOOL isShowAllAt;

@property (nonatomic, strong) NSArray *dataSourceArr;

@property (nonatomic, copy) SelectedBlock selectedBlock;

@end

NS_ASSUME_NONNULL_END
