//
//  ChooseContactAtVC.m
//  BOB
//
//  Created by mac on 2020/8/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChooseContactAtVC.h"
#import "ChooseContactAtCell.h"

static const CGFloat sectionHeaderHeight = 25;

@interface ChooseContactAtVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UITableView *searchTableView;

@property (nonatomic, strong) NSMutableArray *searchDataMArr;

@property(nonatomic, strong)  UIView *searchView;

@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation ChooseContactAtVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)configCurrentVC {
    
}

#pragma mark - Private Method
- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        if ([StringUtil isEmpty:textField.text]) {
            self.tableView.visibility = MyVisibility_Visible;
            self.searchTableView.visibility = MyVisibility_Gone;
        } else {
            self.tableView.visibility = MyVisibility_Gone;
            self.searchTableView.visibility = MyVisibility_Visible;
        }
        [self filterDataByKey:textField.text];
    }
}

- (void)filterDataByKey:(NSString *)key {
    if ([StringUtil isEmpty:key]) {
        return;
    }
    [self.searchDataMArr removeAllObjects];
    NSString *name;
    NSString *remark;
    for (NSArray *tempArr in self.dataSourceArr) {
        if (![tempArr isKindOfClass:NSArray.class]) {
            continue;
        }
        if (tempArr.count == 0) {
            continue;
        }
        for (FriendModel *obj in tempArr) {
            if (![obj isKindOfClass:FriendModel.class]) {
                continue;
            }
            name = obj.name?:@"";
            remark = obj.remark?:@"";
            if ([name containsString:key] ||
                [remark containsString:key]) {
                [self.searchDataMArr addObject:obj];
            }
        }
    }
    [self.searchTableView reloadData];
}

#pragma mark - Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.tableView) {
        return ALPHA2.length;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        if (section >= self.dataSourceArr.count) {
            return 0;
        }
        NSArray *tempArr = self.dataSourceArr[section];
        if (![tempArr isKindOfClass:NSArray.class]) {
            return 0;
        }
        return tempArr.count;
    } else {
        return self.searchDataMArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class cls = ChooseContactAtCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ChooseContactAtCell viewHeight];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        if (indexPath.section >= self.dataSourceArr.count) {
            return;
        }
        NSArray *tempArr = self.dataSourceArr[indexPath.section];
        if (![tempArr isKindOfClass:NSArray.class]) {
            return;
        }
        if (indexPath.row >= tempArr.count) {
            return;
        }
        if (![cell isKindOfClass:ChooseContactAtCell.class]) {
            return;
        }
        FriendModel *obj = tempArr[indexPath.row];
        ChooseContactAtCell *listCell = (ChooseContactAtCell *)cell;
        [listCell configureView:obj];
        return;
    }
    
    if (tableView == self.searchTableView) {
        if (indexPath.row >= self.searchDataMArr.count) {
            return;
        }
        FriendModel *obj = self.searchDataMArr[indexPath.row];
        ChooseContactAtCell *listCell = (ChooseContactAtCell *)cell;
        [listCell configureView:obj];
        return;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        if (indexPath.section >= self.dataSourceArr.count) {
            return;
        }
        NSArray *tempArr = self.dataSourceArr[indexPath.section];
        if (![tempArr isKindOfClass:NSArray.class]) {
            return;
        }
        if (indexPath.row >= tempArr.count) {
            return;
        }
        FriendModel *obj = tempArr[indexPath.row];
        if (![obj isKindOfClass:FriendModel.class]) {
            return;
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        if (self.selectedBlock) {
            self.selectedBlock(@[obj]);
        }
        return;
    }
    
    if (tableView == self.searchTableView) {
        if (indexPath.row >= self.searchDataMArr.count) {
            return;
        }
        FriendModel *obj = self.searchDataMArr[indexPath.row];
        if (![obj isKindOfClass:FriendModel.class]) {
            return;
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        if (self.selectedBlock) {
            self.selectedBlock(@[obj]);
        }
        return;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        NSString *title = [self sectionTitleInSection:section];
        if (!title) {
            return 0.01;
        }
        return sectionHeaderHeight;
    }
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *title = [self sectionTitleInSection:section];
    if (!title) {
        return [UIView new];
    }
    
    UILabel *label = [UILabel new];
    label.frame = CGRectMake(15, (sectionHeaderHeight-20)/2, 20, 20);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont font14];
    label.text = title;
        
    UIView *view = [UIView new];
    view.size = CGSizeMake(SCREEN_WIDTH, sectionHeaderHeight);
    [view setBackgroundColor:[UIColor moBackground]];
    [view addSubview:label];
    return view;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == self.tableView) {
        NSMutableArray *indices = [NSMutableArray arrayWithObject:UITableViewIndexSearch];
        for (int i = 0; i < ALPHA2.length; i++){
            if (i >= self.dataSourceArr.count) {
                continue;
            }
            NSArray *tempArr = self.dataSourceArr[i];
            if (![tempArr isKindOfClass:NSArray.class]) {
                continue;
            }
            if (tempArr.count == 0) {
                continue;
            }
            [indices safeAddObj:[[ALPHA2 substringFromIndex:i] substringToIndex:1]];
        }
        return indices;
    }
    return @[];
}

-(NSString*)sectionTitleInSection:(NSInteger)section{
    if (section >= self.dataSourceArr.count) {
        return nil;
    }
    NSArray *tempArr = self.dataSourceArr[section];
    if (![tempArr isKindOfClass:NSArray.class]) {
        return nil;
    }
    if (tempArr.count == 0) {
        return nil;
    }
    
    NSString *str = [NSString stringWithFormat:@"%@", [[ALPHA2 substringFromIndex:section] substringToIndex:1]];
    return str;
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"选择提醒的人"];
    [self initNavLeftItem];
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [layout addSubview:self.searchView];
    [layout addSubview:self.tableView];
    [layout addSubview:self.searchTableView];
    [self.view addSubview:layout];
    
    [layout addSubview:self.tableView];
}

- (void)initNavLeftItem {
    UIButton *btn = [UIButton new];
    @weakify(self)
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            if (self.selectedBlock) {
                self.selectedBlock(@[]);
            }
        }];
    }];
    btn.titleLabel.font = [UIFont font17];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitle:@"取消" forState:UIControlStateNormal];
    [btn sizeToFit];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = item;
}

#pragma mark - Init
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.sectionIndexColor = [UIColor lightGrayColor];
        Class cls = ChooseContactAtCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.myTop = self.searchView.height;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.myBottom = 0;
        AdjustTableBehavior(_tableView);
    }
    return _tableView;
}

- (UITableView *)searchTableView{
    if (!_searchTableView) {
        _searchTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _searchTableView.delegate = self;
        _searchTableView.dataSource = self;
        _searchTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _searchTableView.tableFooterView = [UIView new];
        _searchTableView.backgroundColor = [UIColor whiteColor];
        _searchTableView.sectionIndexColor = [UIColor lightGrayColor];
        Class cls = ChooseContactAtCell.class;
        [_searchTableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _searchTableView.myTop = self.searchView.height;
        _searchTableView.myLeft = 0;
        _searchTableView.myRight = 0;
        _searchTableView.myBottom = 0;
        _searchTableView.visibility = MyVisibility_Gone;
        AdjustTableBehavior(_searchTableView);
    }
    return _searchTableView;
}

- (UIView *)searchView {
    if (!_searchView) {
        _searchView = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor colorWithHexString:@"#EDEDED"];
            MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            layout.myTop = 0;
            layout.myLeft = 0;
            layout.size = CGSizeMake(SCREEN_WIDTH, 54);
            layout.mySize = layout.size;
            [layout addSubview:self.searchTF];
            [object addSubview:layout];
            object.size = layout.size;
            object;
        });
    }
    return _searchView;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = ({
            UITextField *object = [UITextField new];
            object.delegate = self;
            [object addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
            [object setViewCornerRadius:15];
            object.font = [UIFont font15];
            object.backgroundColor = [UIColor whiteColor];
            UIView *leftView = [UIView new];
            leftView.size = CGSizeMake(40, 30);
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"im_search_icon"] forState:UIControlStateNormal];
            [leftBtn sizeToFit];
            leftBtn.x = 10;
            leftBtn.y = 5;
            [leftView addSubview:leftBtn];
            object.leftView = leftView;
            object.returnKeyType = UIReturnKeySearch;
            object.leftViewMode = UITextFieldViewModeAlways;
            object.clearButtonMode = UITextFieldViewModeWhileEditing;
            NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索"] colors:@[[UIColor moBlack]] fonts:@[[UIFont font14]]];
            object.attributedPlaceholder = att;
            object.myLeft = 5;
            object.myWidth = SCREEN_WIDTH - 10;
            object.myHeight = 35;
            object.myCenterY = 0;
            [object setViewCornerRadius:17];
            object;
        });
    }
    return _searchTF;
}

- (NSMutableArray *)searchDataMArr {
    if (!_searchDataMArr) {
        _searchDataMArr = [NSMutableArray array];
    }
    return _searchDataMArr;
}

@end
