//
//  MXPointAtManager.h
//  BOB
//  @功能管理类
//  Created by mac on 2020/8/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXInputTextView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MXPointAtManager : NSObject
///群ID 获取群成员
@property (nonatomic, copy) NSString *groupId;
///@数据  key userid value 昵称
@property (nonatomic, strong) NSMutableDictionary *pointAtDataMDic;
///@文本Range  key userId value range 辅助@删除
@property (nonatomic, strong) NSMutableDictionary *pointAtRangeMDic;

+ (instancetype)sharedManager;

///插入@
- (void)pointAtFriend:(MXInputTextView *)textView inRange:(NSRange)range;
///删除@
- (BOOL)delPointAt:(MXInputTextView *)textView withRange:(NSRange)range ;
///清空@信息
- (void)clearAtMessage;

@end

NS_ASSUME_NONNULL_END
