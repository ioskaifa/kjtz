//
//  MXPointAtManager.m
//  BOB
//  
//  Created by mac on 2020/8/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MXPointAtManager.h"
#import "LcwlChat.h"
#import "ContactHelper.h"

@implementation MXPointAtManager

+ (instancetype)sharedManager {
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

#pragma mark - Public Method
- (void)pointAtFriend:(MXInputTextView *)textView inRange:(NSRange)range {
    [textView resignFirstResponder];
    //选择完后回调
    void (^selectBlock)(NSArray *) = ^(NSArray *friendsArr) {
        if (![friendsArr isKindOfClass:NSArray.class]) {
            return;
        }
        FriendModel *obj = friendsArr.firstObject;
        if (![obj isKindOfClass:FriendModel.class]) {
            [self addPointAt:textView inRange:range withName:@"" andUserID:@""];
            return;
        }
        NSString *name = obj.groupNickname;
        if ([StringUtil isEmpty:name]) {
            name = obj.name;
        }
        [self addPointAt:textView inRange:range withName:name andUserID:obj.userid];
    };
    NSArray *tempArr = [[LcwlChat shareInstance].chatManager loadGroupMembers:self.groupId];
    //移除自己
    NSMutableArray *tempMArr = [NSMutableArray arrayWithCapacity:tempArr.count];
    for (FriendModel *obj in tempArr) {
        if (![obj isKindOfClass:FriendModel.class]) {
            continue;
        }
        if ([obj.userid isEqualToString:[LcwlChat shareInstance].user.user_id]) {
            continue;
        }
        [tempMArr addObject:obj];
    }
    
    //排序
    tempArr = [ContactHelper indexFriendVCModel:tempMArr];
    NSDictionary *param = @{@"dataSourceArr":tempArr,
                            @"selectedBlock":selectBlock};
    MXRoute(@"ChooseContactAtVC", param);
}

#pragma mark - Private Method
- (void)addPointAt:(MXInputTextView *)textView
           inRange:(NSRange)range
          withName:(NSString *)nickname
         andUserID:(NSString *)userID {
    NSString *text = textView.text;
    NSString *frontText = [text substringToIndex:range.location+1];
    NSString *behindText = @"";
    if (text.length >= range.location + 1) {
        behindText = [text substringFromIndex:range.location + 1];
    }
    frontText = StrF(@"%@%@ ", frontText, nickname);
    if ([StringUtil isEmpty:nickname]) {
        frontText = StrF(@"%@%@", frontText, nickname);
    }
    NSString *atName = StrF(@"%@%@ ", @"@", nickname);
    if ([StringUtil isEmpty:nickname]) {
        atName = StrF(@"%@%@", @"@", nickname);
    }
    NSRange inRange = [frontText rangeOfString:atName options:NSBackwardsSearch];
    if (inRange.location == NSNotFound) {
        return;
    }
    textView.text = StrF(@"%@%@", frontText, behindText);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //改变光标的位置
        NSRange range = NSMakeRange(inRange.location + inRange.length, 0);
        textView.selectedRange = range;
        [textView becomeFirstResponder];
    });
    if ([StringUtil isEmpty:userID]) {
        return;
    }
    //pointAtDataMDic pointAtRangeMDic 保存着同一个key
    [self.pointAtDataMDic setValue:nickname?:@"" forKey:userID];
    [self.pointAtRangeMDic setValue:userID forKey:NSStringFromRange(inRange)];
}

- (BOOL)delPointAt:(MXInputTextView *)textView withRange:(NSRange)range {
    if (textView.text.length != (range.location + 1)) {
        return YES;
    }
    NSString *subText = [textView.text substringToIndex:range.location + 1];
    //删除的字符是否是 ' '
    if (!([subText characterAtIndex:subText.length-1] == ' ')) {
        return YES;
    }
    NSInteger index = -1;
    for(NSInteger i = subText.length - 1; i >= 0; i--){
        if ([subText characterAtIndex:i] == '@' ) {
            index = i;
            break;
        }
    }
    if (index == -1) {
        return YES;
    }
    NSRange rang = NSMakeRange(index, subText.length - index);
    NSString *rangeString = NSStringFromRange(rang);
    if (![self.pointAtRangeMDic.allKeys containsObject:rangeString]) {
        return YES;
    }
    NSString *userID = self.pointAtRangeMDic[rangeString];
    [self.pointAtDataMDic removeObjectForKey:userID];
    [self.pointAtRangeMDic removeObjectForKey:rangeString];
    NSString *text = [textView.text substringToIndex:rang.location];
    textView.text = text;
    return NO;
}

- (void)clearAtMessage {
    [self.pointAtDataMDic removeAllObjects];
    [self.pointAtRangeMDic removeAllObjects];
}

#pragma mark - Init
- (NSMutableDictionary *)pointAtDataMDic {
    if (!_pointAtDataMDic) {
        _pointAtDataMDic = [NSMutableDictionary dictionary];
    }
    return _pointAtDataMDic;
}

- (NSMutableDictionary *)pointAtRangeMDic {
    if (!_pointAtRangeMDic) {
        _pointAtRangeMDic = [NSMutableDictionary dictionary];
    }
    return _pointAtRangeMDic;
}

@end
