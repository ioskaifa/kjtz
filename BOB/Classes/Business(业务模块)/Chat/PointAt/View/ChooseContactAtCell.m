//
//  ChooseContactAtCell.m
//  BOB
//
//  Created by mac on 2020/8/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChooseContactAtCell.h"

@interface ChooseContactAtCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *contentLbl;

@end

@implementation ChooseContactAtCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60;
}

- (void)configureView:(FriendModel *)obj {
    if (![obj isKindOfClass:FriendModel.class]) {
        return;
    }
    NSString *avatar = StrF(@"%@?imageView2/1/w/60/h/60", obj.avatar);
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:avatar]
                    placeholderImage:[UIImage defaultAvatar]];
    NSString *name = obj.remark;
    if ([StringUtil isEmpty:name]) {
        name = obj.name;
    }
    if ([StringUtil isEmpty:name]) {
        name = obj.nick_name;
    }
    self.contentLbl.text = name;
}

#pragma mark - InitUI
- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.imgView];
    [layout addSubview:self.contentLbl];
    
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor moBackground];
    [self.contentView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0.5);
        make.left.mas_equalTo(self.contentView.mas_left);
        make.right.mas_equalTo(self.contentView.mas_right);
        make.bottom.mas_equalTo(self.contentView.mas_bottom);
    }];
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.size = CGSizeMake(40, 40);
            [object setViewCornerRadius:object.height/2.0];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 20;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)contentLbl {
    if (!_contentLbl) {
        _contentLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor blackColor];
            object.myHeight = 20;
            object.weight = 1;
            object.myCenterY = 0;
            object.myLeft = 15;
            object.myRight = 15;
            object;
        });
    }
    return _contentLbl;
}

@end
