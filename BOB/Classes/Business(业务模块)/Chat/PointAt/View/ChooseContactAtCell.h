//
//  ChooseContactAtCell.h
//  BOB
//
//  Created by mac on 2020/8/15.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChooseContactAtCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(FriendModel *)obj;

@end

NS_ASSUME_NONNULL_END
