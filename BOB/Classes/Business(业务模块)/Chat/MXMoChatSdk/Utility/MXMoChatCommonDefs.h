//
//  MXMoChatCommonDefs.h
//  MXMoChat
//
//  聊天sdk相关的一此公共常量
//  Created by aken on 15/12/24.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#ifndef MXMoChatCommonDefs_h
#define MXMoChatCommonDefs_h

// 登录infos
static NSString* const kMxLastLoginLoginUserName = @"k_lastlogin_username";
static NSString* const kMxLastLoginLoginToken = @"k_lastlogin_user_token";


#endif /* MXMoChatCommonDefs_h */
