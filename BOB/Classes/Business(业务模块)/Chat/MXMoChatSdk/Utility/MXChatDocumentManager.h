//
//  MXChatDocumentManager.h
//  MXMoChat
//
//  Created by aken on 15/12/23.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @class
 @brief 聊天相关的文件处理类
 */
@interface MXChatDocumentManager : NSObject


/*!
 @method
 @brief 创建语音/图片缓放的文件夹根目录
 */
+ (NSString *)cacheDocDirectory;

/*!
 @method
 @brief 存放语音/图片
 @discussion
 @param tempImageString 保存的文件名
 @param userId 要保存的对应目录
 @result 图片对象
 */
+ (NSString*)fileSavePath:(NSString*)tempImageString withUserId:(NSString*)userId;

@end
