//
//  MXChatDocumentManager.m
//  MXMoChat
//
//  Created by aken on 15/12/23.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import "MXChatDocumentManager.h"

@implementation MXChatDocumentManager


+ (NSString *)cacheDocDirectory {
    
    NSString *tempPath=nil;
    
    if (tempPath==nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        tempPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Lcwl/chatbuffer"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL isDir = FALSE;
        BOOL isDirExist = [fileManager fileExistsAtPath:tempPath isDirectory:&isDir];
        if(!(isDirExist && isDir)){
            
            [[NSFileManager defaultManager] createDirectoryAtPath:tempPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
    }
    return tempPath;
}

#pragma mark - 临时图片文件存放路径
+ (NSString*)fileSavePath:(NSString*)tempImageString withUserId:(NSString*)userId {
    
    NSString* filePath = [self cacheDocDirectory];
    
    NSString* userCachesPath = [filePath stringByAppendingPathComponent:userId];

    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:userCachesPath isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) )
    {
        
        [fileManager createDirectoryAtPath:userCachesPath withIntermediateDirectories:YES attributes:nil error:nil];
        // 作日志用
        MLog(@"创建用户文件夹成功");
    }
    
    [fileManager changeCurrentDirectoryPath:[[self cacheDocDirectory] stringByExpandingTildeInPath]];
    
    NSString *path = [userCachesPath stringByAppendingPathComponent:tempImageString];
    path = [NSString stringWithFormat:@"%@.jpg",path];
    return path;
}

@end
