//
//  IDeviceMediaManager.h
//  MoPal_Developer
//
//  Created by aken on 15/12/28.
//  Copyright © 2015年 MXMoChat. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @protocol
 @brief 录制语音,语音播放协议集合
 @discussion
 */
@protocol IDeviceMediaManager<NSObject>

@optional

#pragma mark - 播放
/*!
 @method
 @brief 播放语音
 @param filePath wav/amr音频所在路径
 @param completon  回调函数
 */
- (void)playAudioWithPath:(NSString *)filePath
               completion:(void(^)(NSError *error))completon;

/*!
 @method
 @brief 播放语音
 @param filePath wav/amr音频所在路径
 @param enabled 是否打开扬声器 YES/NO
 @param completon  回调函数
 */
- (void)playAudioWithPath:(NSString *)filePath
           speakerEnabled:(BOOL)enabled
               completion:(void(^)(NSError *error))completon;

/*!
 @method
 @brief 当前是否正在播放
 */
- (BOOL)isPlaying;

/*!
 @method
 @brief 停止播放
 */
- (void)stopPlaying;

#pragma mark -  录制

/*!
 @@property
 @brief 获取录制音频时的音量(0~1)
 @result
 */
- (double)peekRecorderVoiceMeter;

/*!
 @method
 @brief 开始录音
 @param completon  回调函数
 */
- (void)startRecordAudio:(void(^)(NSError *error))completion;

/*!
 @method
 @brief 停止录音
 @param completon  回调函数
 @result 返回:语音路径、语音长度
 */
-(void)stopRecordAudioWithCompletion:(void(^)(NSString *recordPath,
                                              NSInteger aDuration,
                                              NSError *error))completion;

/*!
 @method
 @brief 停止录音
 */
- (void)cancelCurrentRecording;

/*!
 @method
 @brief 当前是否正在录音
 */
- (BOOL)isRecording;

@end
