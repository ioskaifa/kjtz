//
//  MXDeviceManagerPlayer.h
//  MXMoChat
//
//  语音播放实现类
//  Created by aken on 15/12/24.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @class
 @brief 语音播放实现类
 @discussion 默认是使用扬声器播放
 */
@interface MXDeviceManagerPlayer : NSObject


/*!
 @method
 @brief 当前是否正在播放
 */
+ (BOOL)isPlaying;

/*!
 @method
 @brief 停止播放
 */
+ (void)stopPlaying;

/*!
 @method
 @brief 播放语音
 @param filePath wav/amr音频所在路径
 @param completon  回调函数
 @discussion 默认是使用扬声器播放
 */
+ (void)playAudioWithPath:(NSString *)filePath
               completion:(void(^)(NSError *error))completon;

/*!
 @method
 @brief 播放语音
 @param filePath wav/amr音频所在路径
 @param enabled 是否打开扬声器 YES/NO
 @param completon  回调函数
 */
+ (void)playAudioWithPath:(NSString *)filePath
               speakerEnabled:(BOOL)enabled
               completion:(void(^)(NSError *error))completon;

@end
