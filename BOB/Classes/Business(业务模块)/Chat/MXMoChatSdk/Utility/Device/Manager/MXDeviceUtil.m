//
//  MXDeviceUtil.m
//  MXMoChat
//
//  Created by aken on 15/12/24.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import "MXDeviceUtil.h"
#import <AVFoundation/AVFoundation.h>

@interface MXDeviceUtil()
{
    NSString    *_currCategory;
    BOOL        _currActive;
}

@end

@implementation MXDeviceUtil

+ (MXDeviceUtil*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (NSError *)setupAudioSessionCategory:(MXAudioSession)session
                             isActive:(BOOL)isActive {
    BOOL isNeedActive = NO;
    if (isActive != _currActive) {
        isNeedActive = YES;
        _currActive = isActive;
    }
    NSError *error = nil;
    NSString *audioSessionCategory = nil;
    switch (session) {
        case MX_Audio_Player:
            audioSessionCategory = AVAudioSessionCategoryPlayback;
            break;
        case MX_Audio_Recorder:
            audioSessionCategory = AVAudioSessionCategoryRecord;
            break;
        default:
            audioSessionCategory = AVAudioSessionCategoryAmbient;
            break;
    }
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    // 如果当前category等于要设置的，不需要再设置
    if (![_currCategory isEqualToString:audioSessionCategory]) {
        [audioSession setCategory:audioSessionCategory error:nil];
    }
    if (isNeedActive) {
        BOOL success = [audioSession setActive:isActive
                                   withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation
                                         error:&error];
        if(!success || error){
            error = [NSError errorWithDomain:@"初始化AVAudioPlayer失败!"
                                        code:3
                                    userInfo:nil];
            return error;
        }
    }
    _currCategory = audioSessionCategory;
    
    return error;
}


@end
