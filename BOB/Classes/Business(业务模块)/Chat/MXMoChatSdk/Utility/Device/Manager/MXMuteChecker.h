//
//  MXMuteChecker.h
//  MoPal_Developer
//
//  通过播放caf文件来判断手机静音
//  Created by aken on 17/3/9.
//  Copyright © 2017年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

/*
  Note: Up to iOS 7 there is no simple API to check whether the user switched on the mute button(silent switch) or not…
 */

typedef void (^MuteCheckCompletionHandler)(NSTimeInterval lapse, BOOL muted);

@interface MXMuteChecker : NSObject

// this class must use with a MuteChecker.caf (a 0.2 sec mute sound) in Bundle

@property (nonatomic , assign) SystemSoundID soundId;
@property (nonatomic , strong) MuteCheckCompletionHandler completionBlk;

- (instancetype)initWithCompletionBlk:(MuteCheckCompletionHandler)completionBlk;

- (void)check;

@end
