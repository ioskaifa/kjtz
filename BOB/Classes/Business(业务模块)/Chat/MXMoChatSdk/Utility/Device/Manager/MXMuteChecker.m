//
//  MXMuteChecker.m
//  MoPal_Developer
//
//  Created by aken on 17/3/9.
//  Copyright © 2017年 MoXian. All rights reserved.
//

#import "MXMuteChecker.h"

void MuteCheckCompletionProc(SystemSoundID ssID, void* clientData);

@interface MXMuteChecker ()

@property (nonatomic, strong) NSDate *startTime;

-(void)completed;

@end

void MuteCheckCompletionProc(SystemSoundID ssID, void* clientData){
    MXMuteChecker *obj = (__bridge MXMuteChecker *)clientData;
    [obj completed];
}

@implementation MXMuteChecker

- (void)playMuteSound {
    
    self.startTime = [NSDate date];
    AudioServicesPlaySystemSound(self.soundId);
}

- (void)completed {
    
    NSDate *now = [NSDate date];
    NSTimeInterval t = [now timeIntervalSinceDate:self.startTime];
    
    BOOL muted = (t > 0.1)? NO : YES;
    
    self.completionBlk(t, muted);
}

- (void)check {
    
    if (self.startTime == nil) {
        
        [self playMuteSound];
        
    } else {
        
        NSDate *now = [NSDate date];
        NSTimeInterval lastCheck = [now timeIntervalSinceDate:self.startTime];
        if (lastCheck > 1) {	//prevent checking interval shorter then the sound length
            
            [self playMuteSound];
        }
    }
}


- (instancetype)initWithCompletionBlk:(MuteCheckCompletionHandler)completionBlk {
    
    self = [self init];
    
    if (self) {
        
        NSURL* url = [[NSBundle mainBundle] URLForResource:@"MuteChecker" withExtension:@"caf"];
        if (AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &_soundId) == kAudioServicesNoError){
            AudioServicesAddSystemSoundCompletion(self.soundId, CFRunLoopGetMain(), kCFRunLoopDefaultMode, MuteCheckCompletionProc,(__bridge void *)(self));
            UInt32 yes = 1;
            AudioServicesSetProperty(kAudioServicesPropertyIsUISound, sizeof(_soundId),&_soundId,sizeof(yes), &yes);
            self.completionBlk = completionBlk;
        } else {
            MLog(@"error setting up Sound ID");
        }
    }
    return self;
}


- (void)dealloc {
    
    if (self.soundId != -1){
        
        AudioServicesRemoveSystemSoundCompletion(self.soundId);
        AudioServicesDisposeSystemSoundID(self.soundId);
    }
}

@end
