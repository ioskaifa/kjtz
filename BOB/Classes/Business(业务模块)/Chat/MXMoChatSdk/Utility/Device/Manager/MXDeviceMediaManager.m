//
//  MXDeviceMediaManager.m
//  MXMoChat
//
//  Created by aken on 15/12/24.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import "MXDeviceMediaManager.h"
#import "MXDeviceMediaUtil.h"

@interface MXDeviceMediaManager()<IDeviceMediaManager>

@end

@implementation MXDeviceMediaManager

+ (MXDeviceMediaManager*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}


-(id)deviceMedia{
    
    return [MXDeviceMediaUtil sharedInstance];
}


@end
