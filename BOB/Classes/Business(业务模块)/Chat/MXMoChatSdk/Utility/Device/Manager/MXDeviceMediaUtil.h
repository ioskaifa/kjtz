//
//  MXDeviceMediaUtil.h
//  MoPal_Developer
//
//  播放,录制整理合
//  Created by aken on 15/12/28.
//  Copyright © 2015年 MXMoChat. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @method
 @brief 播放,录制整合
 */
@interface MXDeviceMediaUtil : NSObject

/*!
 @method
 @brief 播放,录制整合
 @result MXDeviceMediaUtil实例对象
 */
+ (MXDeviceMediaUtil *)sharedInstance;


@end
