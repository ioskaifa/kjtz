//
//  MXDeviceMediaManager.h
//  MXMoChat
//
//  获取语音实现类的单实例
//  Created by aken on 15/12/24.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDeviceMediaManager.h"

@interface MXDeviceMediaManager : NSObject

/*!
 @method
 @brief 获取语音实现类的单实例
 @result MXDeviceMediaManager实例对象
 */
+ (MXDeviceMediaManager *)sharedInstance;

/*!
 @property
 @brief 得到播放语音,录制语音对象
 */
@property (nonatomic, readonly, strong) id<IDeviceMediaManager> deviceMedia;


@end
