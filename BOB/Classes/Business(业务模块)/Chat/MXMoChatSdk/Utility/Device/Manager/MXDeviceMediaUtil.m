//
//  MXDeviceMediaUtil.m
//  MoPal_Developer
//
//  Created by aken on 15/12/28.
//  Copyright © 2015年 MXMoChat. All rights reserved.
//

#import "MXDeviceMediaUtil.h"
#import "IDeviceMediaManager.h"
#import "MXDeviceManagerPlayer.h"
#import "MXDeviceManagerRecorder.h"

@interface MXDeviceMediaUtil()<IDeviceMediaManager>

@end

@implementation MXDeviceMediaUtil

+ (MXDeviceMediaUtil*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}


#pragma mark - 播放
/// 当前是否正在播放
- (BOOL)isPlaying {
    return [MXDeviceManagerPlayer isPlaying];
}

/// 停止播放
- (void)stopPlaying {
    [MXDeviceManagerPlayer stopPlaying];
}

/// wav/amr音频所在路径
- (void)playAudioWithPath:(NSString *)filePath
               completion:(void(^)(NSError *error))completon {
    [MXDeviceManagerPlayer playAudioWithPath:filePath completion:completon];
}

/// wav/amr音频所在路径 是否打开扬声器 YES/NO
- (void)playAudioWithPath:(NSString *)filePath
           speakerEnabled:(BOOL)enabled
               completion:(void(^)(NSError *error))completon {
    [MXDeviceManagerPlayer playAudioWithPath:filePath speakerEnabled:enabled completion:completon];
}

#pragma mark - 录制

/// 获取录制音频时的音量(0~1)
- (double)peekRecorderVoiceMeter {
    return [MXDeviceManagerRecorder peekRecorderVoiceMeter];
}

/// 开始录音
- (void)startRecordAudio:(void(^)(NSError *error))completion {
    [MXDeviceManagerRecorder startRecordAudio:completion];
}

/// 停止录音
- (void)stopRecordAudioWithCompletion:(void(^)(NSString *recordPath,
                                               NSInteger aDuration,
                                               NSError *error))completion {
    [MXDeviceManagerRecorder stopRecordAudioWithCompletion:completion];
}

/// 停止录音
- (void)cancelCurrentRecording {
    [MXDeviceManagerRecorder cancelCurrentRecording];
}

/// 当前是否正在录音
- (BOOL)isRecording {
    return [MXDeviceManagerRecorder isRecording];
}

@end
