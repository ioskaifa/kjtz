//
//  MXDeviceManagerPlayer.m
//  MXMoChat
//
//  Created by aken on 15/12/24.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import "MXDeviceManagerPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import "MXDeviceUtil.h"
#import "VoiceConverter.h"
#import "LcwlChat.h"

typedef void (^DevicePlayFinish)(NSError *error);

@interface MXDeviceManagerPlayer () <AVAudioPlayerDelegate> {
    
    
}

// 重要属性
@property (nonatomic, strong) AVAudioPlayer *player;
@property (nonatomic, strong) DevicePlayFinish playFinish;

// 获取语音播放实现类的单实例
+ (MXDeviceManagerPlayer *)sharedInstance;

@end

@implementation MXDeviceManagerPlayer

+ (MXDeviceManagerPlayer*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (void)dealloc {
    if (_player) {
        _player.delegate = nil;
        [_player stop];
        _player = nil;
    }
    _playFinish = nil;
}

#pragma mark - public
+ (void)playAudioWithPath:(NSString *)filePath
               completion:(void(^)(NSError *error))completon {
    [[self sharedInstance] playAudioWithPath:filePath completion:completon];
}

+ (void)playAudioWithPath:(NSString *)filePath
           speakerEnabled:(BOOL)enabled
               completion:(void(^)(NSError *error))completon {
    
    // 调用默认的播放
    [[self sharedInstance] playAudioWithPath:filePath speakerEnabled:enabled completion:completon];
}

+ (BOOL)isPlaying {
    return [[self sharedInstance] isPlaying];
}


+ (void)stopPlaying {
    [[self sharedInstance] stopCurrentPlaying];
}

#pragma mark - private
// 播放音频
- (void)playAudioWithPath:(NSString *)filePath
               completion:(void(^)(NSError *error))completon {
    
    [self playAudioWithPath:filePath speakerEnabled:YES completion:completon];
    
}

- (void)playAudioWithPath:(NSString *)filePath
           speakerEnabled:(BOOL)enabled
               completion:(void(^)(NSError *error))completon {
    
    BOOL isNeedSetActive = YES;
    // 如果正在播放音频，停止当前播放。
    if([self isPlaying]){
        [self stopCurrentPlaying];
        isNeedSetActive = NO;
    }
    
    if (isNeedSetActive) {
         UserModel* model = [LcwlChat shareInstance].user;
        //wg 0224
         NSString* voicePlayModel = [MXCache valueForKey:[NSString stringWithFormat:@"ControlSpeak_%@",[model valueForKey:@"user_id"]]];
        if ([voicePlayModel isEqualToString:@"1"]) {
            [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
        }else{
            [[UIDevice currentDevice] setProximityMonitoringEnabled:YES];
        }
        
        // 设置播放时需要的category
//        [[MXDeviceUtil sharedInstance] setupAudioSessionCategory:MX_Audio_Player
//                                                        isActive:YES];
    }
    
    NSString *wavFilePath = [[filePath stringByDeletingPathExtension] stringByAppendingPathExtension:@"wav"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //如果转换后的wav文件不存在, 则去转换一下
    if (![fileManager fileExistsAtPath:wavFilePath]) {
        NSString *wavPath=[VoiceConverter amrToWav:filePath];
        if (!wavPath) {
            if (completon) {
                completon([NSError errorWithDomain:@"格式化音频文件出错!"
                                              code:7
                                          userInfo:nil]);
            }
            return ;
        }
    }
    
    [self setSpeakerEnabled:enabled];
    
    [self asyncPlayingWithPath:wavFilePath
                    completion:^(NSError *error) {
                        
                        [[MXDeviceUtil sharedInstance]  setupAudioSessionCategory:MX_DeviceRecord_Default
                                                                         isActive:NO];
                        if (completon) {
                            completon(error);
                        }
                    }];
}

// 当前是否正在播放
- (BOOL)isPlaying {
    return !!_player;
}

// 得到当前播放音频路径
- (NSString *)playingFilePath {
    
    NSString *path = nil;
    if (_player && _player.isPlaying) {
        path = _player.url.path;
    }
    
    return path;
}

- (void)asyncPlayingWithPath:(NSString *)aFilePath
                  completion:(void(^)(NSError *error))completon{

    
    _playFinish = completon;
    NSError *error = nil;
    NSFileManager *fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:aFilePath]) {
        error = [NSError errorWithDomain:@"播放文件不存在!"
                                    code:5
                                userInfo:nil];
        if (_playFinish) {
            _playFinish(error);
        }
        _playFinish = nil;
        
        return;
    }
    
    NSURL *wavUrl = [[NSURL alloc] initFileURLWithPath:aFilePath];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:wavUrl error:&error];
    _player.volume = 0.99;
    
    UInt32 audioRoute = kAudioSessionOverrideAudioRoute_Speaker;
    
    AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRoute), &audioRoute);

    if (error || !_player) {
        _player = nil;
        error = [NSError errorWithDomain:@"初始化AVAudioPlayer失败!"
                                    code:6
                                userInfo:nil];
        if (_playFinish) {
            _playFinish(error);
        }
        _playFinish = nil;
        return;
    }
    
    _player.delegate = self;
    [_player prepareToPlay];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    [_player play];
    
}



// 停止当前播放
- (void)stopCurrentPlaying {
    if(_player){
        _player.delegate = nil;
        [_player stop];
        _player = nil;
    }
    if (_playFinish) {
        _playFinish = nil;
    }
}

- (void)setSpeakerEnabled:(BOOL)enabled {
    
    
    AVAudioSession *session =   [AVAudioSession sharedInstance];
    NSError *error;
    
    [session setActive:NO error:&error];
//    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:&error];
    [session setMode:AVAudioSessionModeVoiceChat error:&error];
    
    
    if (![self isHeadsetPluggedIn]) {
        
        if (enabled) {
            
            [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        }
        else {  // 关闭speaker
            [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:&error];
        }
    }
    
    [session setActive:YES error:&error];
    
}

// 添加语音耳机监控
- (BOOL)isHeadsetPluggedIn {
    
    AVAudioSessionRouteDescription* route = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription* desc in [route outputs]) {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
            return YES;
    }
    return NO;
}

#pragma mark - AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player
                       successfully:(BOOL)flag {
    if (_playFinish) {
        _playFinish(nil);
    }
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    //  播放完以后，置空
    if (_player) {
        _player.delegate = nil;
        _player = nil;
    }
    
    _playFinish = nil;
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player
                                 error:(NSError *)error {
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    if (_playFinish) {
        NSError *error = [NSError errorWithDomain:@"播放失败!"
                                             code:7
                                         userInfo:nil];
        _playFinish(error);
    }
    if (_player) {
        _player.delegate = nil;
        _player = nil;
    }
}

@end
