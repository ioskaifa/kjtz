//
//  MXDeviceManagerRecorder.m
//  MXMoChat
//
//  Created by aken on 15/12/24.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import "MXDeviceManagerRecorder.h"
#import "ChatCacheFileUtil.h"
#import "VoiceConverter.h"
#import "MXDeviceUtil.h"

// 需要修改
//#import "MXMoChat.h"
#import "LcwlChat.h"
#import "TalkConfig.h"

#import "MXMoChatCommonDefs.h"

typedef void(^DeviceRecordFinish)(NSString *recordPath);


static NSInteger const DeviceRecordMinDuration = 1.0;


@interface MXDeviceManagerRecorder()<AVAudioRecorderDelegate>
{
    NSDate      *_startDate;
    NSDate      *_endDate;
    NSDate      *_recorderStartDate;
    NSDate      *_recorderEndDate;
}

// 重要属性
@property (nonatomic, strong) AVAudioRecorder *recorder;
@property (nonatomic, strong) NSDictionary *recordSetting;
@property (nonatomic, strong) DeviceRecordFinish recordFinish;

// 获取语音录制实现类的单实例
+ (MXDeviceManagerRecorder *)sharedInstance;

@end

@implementation MXDeviceManagerRecorder

+ (MXDeviceManagerRecorder*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

-(void)dealloc{
    if (_recorder) {
        _recorder.delegate = nil;
        [_recorder stop];
        [_recorder deleteRecording];
        _recorder = nil;
    }
    _recordFinish = nil;
}

#pragma mark - public 

/*!
 @method
 @brief 获取录制音频时的音量(0~1)
 @result
 */
+ (double)peekRecorderVoiceMeter {
    return [[self sharedInstance] peekRecorderVoiceMeter];
}

/*!
 @method
 @brief 开始录音
 @param completon  回调函数
 */
+ (void)startRecordAudio:(void(^)(NSError *error))completion {
    [[self sharedInstance] startRecordAudio:completion];
}

/*!
 @method
 @brief 停止录音
 @param completon  回调函数
 @result 返回:语音路径、语音长度
 */
+ (void)stopRecordAudioWithCompletion:(void(^)(NSString *recordPath,
                                               NSInteger aDuration,
                                               NSError *error))completion {
    [[self sharedInstance] stopRecordAudioWithCompletion:completion];
}

/*!
 @method
 @brief 停止录音
 */
+ (void)cancelCurrentRecording {
    [[self sharedInstance] cancelCurrentRecording];
}

/*!
 @method
 @brief 当前是否正在录音
 */
+ (BOOL)isRecording {
    
    return [[self sharedInstance] isRecording];
}

#pragma mark - private
/// 开始录制语音
- (void)startRecordAudio:(void(^)(NSError *error))completion {
    NSError *error = nil;
    
    // 判断当前是否是录音状态
    if ([self isRecording]) {
        if (completion) {
            error = [NSError errorWithDomain:@"还没完成录制语音，请稍后!"
                                        code:2
                                    userInfo:nil];
            completion(error);
        }
        return ;
    }

    if ([self isRecording]) {
        [self cancelCurrentRecording];
    }
    
    [[MXDeviceUtil sharedInstance] setupAudioSessionCategory:MX_Audio_Recorder
                           isActive:YES];
    
    [self startToPrepareRecording:completion];
}

// 停止录音
-(void)stopRecordAudioWithCompletion:(void(^)(NSString *recordPath,
                                                 NSInteger aDuration,
                                                 NSError *error))completion {
    NSError *error = nil;
    // 当前是否在录音
    if(![self isRecording]){
        if (completion) {
            error = [NSError errorWithDomain:@"录制还没开始!"
                                        code:3
                                    userInfo:nil];
            completion(nil,0,error);
            return;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    _recorderEndDate = [NSDate date];
    
    if([_recorderEndDate timeIntervalSinceDate:_recorderStartDate] < DeviceRecordMinDuration){
        if (completion) {
            error = [NSError errorWithDomain:@"录制语音过短!"
                                        code:4
                                    userInfo:nil];
            completion(nil,0,error);
        }
        
        // 如果录音时间较短，延迟1秒停止录音 不允许用户循序重新录音
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(DeviceRecordMinDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf stopRecordingWithCompletion:^(NSString *recordPath) {
                [[MXDeviceUtil sharedInstance] setupAudioSessionCategory:MX_DeviceRecord_Default isActive:NO];
            }];
        });
        
        return ;
    }
    
    [self stopRecordingWithCompletion:^(NSString *recordPath) {
        if (completion) {
            if (recordPath) {
                //录音格式转换，从wav转为amr
                NSString* amrPath = [VoiceConverter wavToAmr:recordPath];
                
                if (amrPath) {
                    // 删除录的wav
                    [[ChatCacheFileUtil sharedInstance] deleteWithContentPath:recordPath];
                }
                
                double timeInterval = ceil([self->_recorderEndDate timeIntervalSinceDate:self->_recorderStartDate]);
      
                completion(amrPath,(int)timeInterval,nil);
            }
            [[MXDeviceUtil sharedInstance] setupAudioSessionCategory:MX_DeviceRecord_Default isActive:NO];
        }
    }];
}

- (double)peekRecorderVoiceMeter {
    
    double ret = 0.0;
    if ([self recorder].isRecording) {
        [[self recorder] updateMeters];
        //获取音量的平均值  [recorder averagePowerForChannel:0];
        //音量的最大值  [recorder peakPowerForChannel:0];
        double lowPassResults = pow(10, (0.05 * [[self recorder] peakPowerForChannel:0]));
        ret = lowPassResults;
    }
    
    return ret;
}


- (void)startToPrepareRecording:(void (^)(NSError *))completion {
    
    NSError *error = nil;
    NSString *wavFilePath = [self audioFilePath];
    
    NSURL *wavUrl = [[NSURL alloc] initFileURLWithPath:wavFilePath];
    _recorder = [[AVAudioRecorder alloc] initWithURL:wavUrl
                                            settings:self.recordSetting
                                               error:&error];
    if(!_recorder || error) {
        _recorder = nil;
        if (completion) {
            error = [NSError errorWithDomain:@"初始化AVAudioRecorder失败"
                                        code:1
                                    userInfo:nil];
            completion(error);
        }
        return ;
    }
    _recorderStartDate = [NSDate date];
    

    _startDate = [NSDate date];
    _recorder.meteringEnabled = YES;
    _recorder.delegate = self;
    
    [_recorder record];
    if (completion) {
        completion(error);
    }
    
}

- (void)stopRecordingWithCompletion:(void (^)(NSString *))completion {
    
    _recordFinish = completion;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self->_recorder stop];
    });
}

-(NSString*)audioFilePath {
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyyMMddHHmmss"];

    // 需要修改
    UserModel *loginInfo=[[LcwlChat shareInstance] user];
    NSString *fullPath = [[[ChatCacheFileUtil sharedInstance] userDocPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"rec_%@_%@.wav",loginInfo.chatUser_id,[dateFormater stringFromDate:now]]];
    return fullPath;
}

/// 当前是否正在录音
- (BOOL)isRecording {
    
    return !!_recorder;
}

/// 取消录音
- (void)cancelCurrentRecording {
    _recorder.delegate = nil;
    if (_recorder.recording) {
        [_recorder stop];
    }
    _recorder = nil;
    _recordFinish = nil;
}

#pragma mark - AVAudioRecorderDelegate
- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder
                           successfully:(BOOL)flag {
    NSString *recordPath = [[_recorder url] path];
    if (_recordFinish) {
        if (!flag) {
            recordPath = nil;
        }
        _recordFinish(recordPath);
    }
    _recorder = nil;
    _recordFinish = nil;
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder
                                   error:(NSError *)error{
    MLog(@"error:%@",error);
}

#pragma mark - Getter and Setter
- (NSDictionary *)recordSetting {
    
    if (_recordSetting==nil) {
        _recordSetting = [[NSDictionary alloc] initWithObjectsAndKeys:
                          [NSNumber numberWithFloat: 8000.0],AVSampleRateKey, //采样率
                          [NSNumber numberWithInt: kAudioFormatLinearPCM],AVFormatIDKey,
                          [NSNumber numberWithInt:16],AVLinearPCMBitDepthKey,//采样位数 默认 16
                          [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,//通道的数目
                          nil];
    }
    
    return _recordSetting;
}

+(int)recordLength{
    MXDeviceManagerRecorder* recorder = [self sharedInstance];
    double timeInterval = round([[NSDate date] timeIntervalSinceDate:recorder->_recorderStartDate]);
    return timeInterval;
}
@end
