//
//  MXDeviceUtil.h
//  MXMoChat
//
//  设备相关的公共类
//  Created by aken on 15/12/24.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, MXAudioSession){
    MX_DeviceRecord_Default = 0,
    MX_Audio_Player,
    MX_Audio_Recorder
};


/*!
 @class
 @brief 设备相关的公共类
 @discussion
 */
@interface MXDeviceUtil : NSObject

/*!
 @method
 @brief 获取语音播放实现类的单实例
 @result MXDeviceUtil实例对象
 */
+ (MXDeviceUtil *)sharedInstance;



/*!
 @method
 @brief 设置AVAudioSession相关属性
 @param session AudioSession类型
 @param isActive 设置Session开或关
 @result 如果设置出错，则返回错误
 */
- (NSError *)setupAudioSessionCategory:(MXAudioSession)session
                              isActive:(BOOL)isActive;

@end
