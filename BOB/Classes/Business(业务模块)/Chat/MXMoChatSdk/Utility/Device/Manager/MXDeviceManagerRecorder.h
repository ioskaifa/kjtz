//
//  MXDeviceManagerRecorder.h
//  MXMoChat
//
//  语音录制实现类
//  Created by aken on 15/12/24.
//  Copyright © 2015年 MoChatSdk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>


/*!
 @class
 @brief 语音录制实现类
 @discussion
 */
@interface MXDeviceManagerRecorder : NSObject

/*!
 @method
 @brief 获取录制音频时的音量(0~1)
 @result
 */
+ (double)peekRecorderVoiceMeter;

/*!
 @method
 @brief 开始录音
 @param completon  回调函数
 */
+ (void)startRecordAudio:(void(^)(NSError *error))completion;

/*!
 @method
 @brief 停止录音
 @param completon  回调函数
 @result 返回:语音路径、语音长度
 */
+ (void)stopRecordAudioWithCompletion:(void(^)(NSString *recordPath,
                                              NSInteger aDuration,
                                              NSError *error))completion;

/*!
 @method
 @brief 停止录音
 */
+ (void)cancelCurrentRecording;

/*!
 @method
 @brief 当前是否正在录音
 */
+ (BOOL)isRecording;

+(int)recordLength;

@end
