//
//  MXEmotion.h
//  ChatToolBar
//
//  表情对象
//  Created by aken on 16/5/12.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, MXEmotionType) {
    MXEmotionDefault = 0, // 默认表情
    MXEmotionPng,         // PNG
    MXEmotionGif          // 大表情或者Gif表情
};


@interface MXEmotion : NSObject

/*!
 @property
 @brief 表情类型
 */
@property (nonatomic, assign) MXEmotionType emotionType;

/*!
 @property
 @brief 表情文字
 */
@property (nonatomic, copy) NSString *emotionTitle;

/*!
 @property
 @brief 表情名字
 */
@property (nonatomic, copy) NSString *emotionName;

/*!
 @property
 @brief 表情Id
 */
@property (nonatomic, copy) NSString *emotionId;

/*!
 @property
 @brief 表情本地缩略图地址
 */
@property (nonatomic, copy) NSString *emotionThumbnail;

/*!
 @property
 @brief 表情本地原始图片地址
 */
@property (nonatomic, copy) NSString *emotionOriginal;

/*!
 @property
 @brief 表情本地原始网络地址
 */
@property (nonatomic, copy) NSString *emotionOriginalURL;

/*!
 @method
 @brief 初始化表情对象
 @param emotionTitle 表情标题
 @param emotionName  表情名字
 @param emotionId   id
 @param emotionThumbnail 表情本地缩略图地址
 @param emotionOriginal  表情本地原始图片地址
 @param emotionOriginalURL 表情本地原始网络地址
 @param emotionType     表情类型
 @result 表情对象
 */
- (id)initWithTitle:(NSString*)emotionTitle
       emotionName:(NSString*)emotionName
         emotionId:(NSString*)emotionId
  emotionThumbnail:(NSString*)emotionThumbnail
   emotionOriginal:(NSString*)emotionOriginal
emotionOriginalURL:(NSString*)emotionOriginalURL
       emotionType:(MXEmotionType)emotionType;

@end
