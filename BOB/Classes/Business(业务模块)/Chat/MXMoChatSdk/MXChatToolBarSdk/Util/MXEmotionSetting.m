//
//  MXEmotionSetting.m
//  MXChatToolBarSdk
//
//  Created by aken on 16/6/7.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionSetting.h"

@implementation MXEmotionSetting

- (id)initWithType:(MXEmotionType)Type
        emotionRow:(NSInteger)emotionRow
        emotionCol:(NSInteger)emotionCol
          emotions:(NSArray*)emotions
          tagImage:(id)tagImage

{
    self = [super init];
    if (self) {
        _emotionType = Type;
        _emotionRow = emotionRow;
        _emotionCol = emotionCol;
        _emotions = emotions;
        _tagImage = tagImage;
    }
    return self;
}

@end
