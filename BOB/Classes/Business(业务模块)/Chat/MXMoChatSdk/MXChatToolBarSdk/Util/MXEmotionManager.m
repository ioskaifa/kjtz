//
//  MXEmotionManager.m
//  ChatToolBar
//
//  Created by aken on 16/4/18.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionManager.h"

@interface MXEmotionManager()

@property (nonatomic, strong, readwrite) NSArray *defaultEmoji;
@property (nonatomic, strong, readwrite) NSArray *defaultGifs;

// 默认
@property (nonatomic, strong) NSMutableArray *emojis;
@property (nonatomic, strong) NSMutableArray *gifEmotions;

@end

@implementation MXEmotionManager

+ (MXEmotionManager*)manager {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (id)init {
    
    self = [super init];
    
    if (self) {
        _emojis = [NSMutableArray array];
        _gifEmotions = [NSMutableArray array];
    }
    
    return self;
}

#pragma mark - Public
- (NSArray *)defaultEmoji {
    
    if (_emojis.count > 0) {
        
        return _emojis;
    }
    
    NSArray *tmpArray = [self emojiForDefault];
    if (tmpArray && tmpArray.count > 0) {
        [_emojis addObjectsFromArray:tmpArray];
    }
    return _emojis;
}

- (NSArray *)defaultGifs {
    
    if (_gifEmotions.count > 0) {
        
        return _gifEmotions;
    }
    
    NSArray *tmpGifArray = [self loadGifEmotions];
    if (tmpGifArray && tmpGifArray.count > 0) {
        [_gifEmotions addObjectsFromArray:tmpGifArray];
    }
    return _gifEmotions;
}

- (void)clearAllEmotions {
    
    if (_emojis) {
        [_emojis removeAllObjects];
    }
    
    if (_gifEmotions) {
        [_gifEmotions removeAllObjects];
    }
}

- (NSString* )emojiPath {
    
    NSString* rootPath = [self chatToolBarBundlePath];
    return [NSString stringWithFormat:@"%@/Emotion/emoji/",rootPath];
}


#pragma mark - Private

- (NSArray*)emojiForDefault {
    
    NSString *bundlePath = [self chatToolBarBundlePath];
    
    NSString *facePath = [[NSBundle bundleWithPath:bundlePath] pathForResource:@"face_default" ofType:@"plist" inDirectory:@"Emotion/plist"];
    
    NSArray *faceArray = [NSArray arrayWithContentsOfFile:facePath];
    
    
    return faceArray;
}

- (NSArray*)loadGifEmotions {
    
    NSString *bundlePath = [self chatToolBarBundlePath];
    
    NSString *facePath = [[NSBundle bundleWithPath:bundlePath] pathForResource:@"moya" ofType:@"plist" inDirectory:@"Emotion/plist"];
    
    NSArray *faceArray = [NSArray arrayWithContentsOfFile:facePath];
    
    
    return faceArray;
}

- (NSString*)chatToolBarBundlePath {
    return [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"MXChatToolBar.bundle"];
}

- (NSString*)deleteEmotionWithInputText:(NSString*)string {

    if([string length]<=0)
        return @"";
    NSInteger n=-1;
    
    if( [string characterAtIndex:[string length]-1] == ']'){
        for(NSInteger i=[string length]-1;i>=0;i--){
            if( [string characterAtIndex:i] == '[' ){
                n = i;
                break;
            }
        }
    }
    
    if (n >= 0) {
        
        NSRange range;
        range.location = n;
        range.length = [string length] - n;
        NSString* emojiKey = [string substringWithRange:NSMakeRange(n,(string.length-n))];
        
        BOOL isExit =  [self stringContainsEmotion:emojiKey];
        
        if (isExit) {
            
            return  [string substringWithRange:NSMakeRange(0,n)];
            
        }else{
            
            return  [string substringToIndex:[string length]-1];
        }
    }else{
        return  [string substringToIndex:[string length]-1];
    }
    
    return nil;
}

- (BOOL)stringContainsEmotion:(NSString *)string {
    
    __block BOOL isContains = NO;
    
    NSArray *emArray = _emojis;
    
    [emArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *dic = (NSDictionary*)obj;
            if ([string isEqualToString:dic[@"faceName"]]) {
                
                isContains = YES;
                *stop = YES;
            }
            
        }
        
    }];
    
    return isContains;
}


@end
