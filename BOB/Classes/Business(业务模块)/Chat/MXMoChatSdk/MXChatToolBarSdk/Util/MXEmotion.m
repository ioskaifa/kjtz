//
//  MXEmotion.m
//  ChatToolBar
//
//  Created by aken on 16/5/12.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotion.h"

@implementation MXEmotion

- (id)initWithTitle:(NSString*)emotionTitle
         emotionName:(NSString*)emotionName
         emotionId:(NSString*)emotionId
  emotionThumbnail:(NSString*)emotionThumbnail
   emotionOriginal:(NSString*)emotionOriginal
emotionOriginalURL:(NSString*)emotionOriginalURL
       emotionType:(MXEmotionType)emotionType
{
    self = [super init];
    if (self) {
        _emotionTitle = emotionTitle;
        _emotionName = emotionName;
        _emotionId = emotionId;
        _emotionThumbnail = emotionThumbnail;
        _emotionOriginal = emotionOriginal;
        _emotionOriginalURL = emotionOriginalURL;
        _emotionType = emotionType;
    }
    return self;
}


@end