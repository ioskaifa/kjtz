//
//  MXEmotionSetting.h
//  MXChatToolBarSdk
//
//  表情控制排版管理类
//  Created by aken on 16/6/7.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MXEmotion.h"

#define MX_EMOTION_DEFAULT_EXT @"em_emotion"

@interface MXEmotionSetting : NSObject

/*!
 @property
 @brief MXEmotion数组
 */
@property (nonatomic, strong) NSArray *emotions;

/*!
 @property
 @brief 表情控件显示行数
 */
@property (nonatomic, assign) NSInteger emotionRow;

/*!
 @property
 @brief 表情控件显示列数
 */
@property (nonatomic, assign) NSInteger emotionCol;

/*!
 @property
 @brief 表情类型
 */
@property (nonatomic, assign) MXEmotionType emotionType;

/*!
 @property
 @brief 表情控件底部按钮表情
 */
@property (nonatomic, strong) id tagImage;

/*!
 @method
 @brief 表情对象管理
 @param Type 表情类型
 @param emotionRow   行
 @param emotionCol   列
 @param emotions     表情数组
 @param tagImage     表情底部表情封面
 @result 对象管理
 */
- (id)initWithType:(MXEmotionType)Type
        emotionRow:(NSInteger)emotionRow
        emotionCol:(NSInteger)emotionCol
          emotions:(NSArray*)emotions
          tagImage:(id)tagImage;


@end
