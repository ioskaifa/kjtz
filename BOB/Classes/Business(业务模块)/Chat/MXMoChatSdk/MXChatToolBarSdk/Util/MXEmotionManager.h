//
//  MXEmotionManager.h
//  ChatToolBar
//
//  表情加载管理类
//  Created by aken on 16/4/18.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface MXEmotionManager : NSObject


+ (MXEmotionManager*)manager;

/*!
 @property
 @brief 获取默认表态表情数组
 */
@property (nonatomic, strong, readonly) NSArray *defaultEmoji;

/*!
 @property
 @brief 获取动态Gif表情数组(分段)组成
 */
@property (nonatomic, strong, readonly) NSArray *defaultGifs;

/*!
 @method
 @brief 从内存中清除所有表情
 */
- (void)clearAllEmotions;

/*!
 @method
 @brief 删除字符串中的表情(用于点击键盘上的删除按钮时)
 @param string 表情字符串
 */
- (NSString*)deleteEmotionWithInputText:(NSString*)string;

/*!
 @method
 @brief 默认表态表情数组是否存在有该表情
 @param string 表情字符串
 */
- (BOOL)stringContainsEmotion:(NSString *)string;

/*!
 @property
 @brief 获取默认表态路径
 */
- (NSString* )emojiPath;

@end
