//
//  MXChatToolBarCommon.h
//  ChatToolBar
//
//  Created by aken on 16/4/19.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#ifndef MXChatToolBarCommon_h
#define MXChatToolBarCommon_h

#define MXChatToolBarBundle(c)  [NSString stringWithFormat:@"MXChatToolBar.bundle/%@",c]

#define MXChatBarBottomViewColor [UIColor colorWithRed:246.0 / 255.0 green:246.0 / 255.0 blue:248.0 / 255.0 alpha:1.0];

typedef NS_ENUM(NSInteger,MXChatToolBarViewTag) {
    MXChatToolBarViewTagForCellContentView = 1000000000,
    MXChatToolBarStyleTagForBarBackground  = 1000000001,
    MXChatToolBarStyleTagForBottomView     = 1000000002
};


#endif /* MXChatToolBarCommon_h */
