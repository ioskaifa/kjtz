//
//  MXMoreItemCollectionCell.m
//  ChatToolBar
//
//  Created by aken on 16/5/19.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXMoreItemCollectionCell.h"
#import "MXEmotion.h"
#import "MXChatBarMoreItem.h"

#import "MXEmotionCollectionViewCell.h"
#import "MXEmotionManager.h"
#import "MXChatToolBarCommon.h"

@implementation MXMoreItemCollectionCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.imageButton];
    }
    return self;
}

- (void)setFrame:(CGRect)frame {

     _imageButton.frame = self.bounds;
    
    [super setFrame:frame];
}

- (void)setEmotion:(MXEmotion *)emotion {
    _emotion = emotion;
    if ([emotion isKindOfClass:[MXEmotion class]]) {

        [_imageButton setImage:[UIImage imageNamed:emotion.emotionOriginal] forState:UIControlStateNormal];
        _imageButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [_imageButton setTitle:emotion.emotionTitle forState:UIControlStateNormal];

        _imageButton.tag = [emotion.emotionId intValue];
        
        [_imageButton addTarget:self action:@selector(sendEmotion:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
}

- (void)changeImageButtonSize:(CGSize)buttonSize {
    
    CGRect rect = _imageButton.frame;
    rect.origin.x = self.bounds.size.width/2 - buttonSize.width/2;
    rect.origin.y = self.bounds.size.height/2 - buttonSize.height/2;
    rect.size = buttonSize;
    _imageButton.frame = rect;
}

- (void)sendEmotion:(UIButton*)sender {
    
    if (_delegate) {
        if ([_emotion isKindOfClass:[MXEmotion class]]) {
            if ([_delegate respondsToSelector:@selector(didSelectMoreItemAt:)]) {
                [_delegate didSelectMoreItemAt:_emotion];
            }
        }
    }
}

- (MXChatBarMoreItem*)imageButton {
    if (!_imageButton) {
        _imageButton = [MXChatBarMoreItem buttonWithType:UIButtonTypeCustom];
        _imageButton.frame = self.bounds;
        _imageButton.userInteractionEnabled = YES;
        _imageButton.backgroundColor = [UIColor clearColor];
        [_imageButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _imageButton.titleLabel.font=[UIFont systemFontOfSize:12.0f];
    }
    return _imageButton;
}

@end
