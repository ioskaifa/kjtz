//
//  MXChatBarMoreView.h
//  ChatToolBar
//
//  更多面板：相册、拍照、地理位置
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXToolBarBottomView.h"

@class MXEmotion;

@protocol MXChatBarMoreViewDelegate<NSObject>

@optional

/*!
 @method
 @brief 点击更多面板上的Item时触发
 @param emotion 选中项
 */
- (void)didSelectMoreItemAt:(MXEmotion*)emotion;

@end

@interface MXChatBarMoreView : MXToolBarBottomView

@property (nonatomic, weak) id<MXChatBarMoreViewDelegate> delegate;

/*!
 @property
 @brief items
 */
@property (nonatomic, strong, readonly) NSArray *items;

/*!
 @method
 @brief 加载更多面板
 @param emotionManagers 数据源
 */
- (void)loadMorePanelManagers:(NSArray *)emotionManagers;

/*!
 @method
 @brief 是否滚动
 @param enabled YES/NO
 */
- (void)setScrollEnabled:(BOOL)enabled;

@end
