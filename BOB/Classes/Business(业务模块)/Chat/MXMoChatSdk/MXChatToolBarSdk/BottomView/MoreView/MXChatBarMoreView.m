//
//  MXChatBarMoreView.m
//  ChatToolBar
//
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXChatBarMoreView.h"
#import "MXMoreItemCollectionCell.h"

#import "MXEmotionSetting.h"
#import "MXChatToolBarCommon.h"

@interface MXChatBarMoreView()<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,MXMoreItemCollectionCellDelegate>

@property (nonatomic, strong, readwrite) NSArray        *emotionManagers;

@property (nonatomic, strong) NSMutableArray            *emotions;

@property (nonatomic, strong, readwrite) NSArray *items;

@end

@implementation MXChatBarMoreView
// 此处为了解决编译器编译时的警告
//Auto property synthesis will not synthesize property 'name' because it is 'readwrite' but it will be synthesized 'readonly' via another property
@synthesize emotionManagers = _emotionManagers;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.backgroundColor = MXChatBarBottomViewColor;
        [self.collectionView registerClass:[MXMoreItemCollectionCell class] forCellWithReuseIdentifier:@"moreCollectionCell"];
//        [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
        
        CGFloat height = frame.size.height - 20;
        self.collectionView.frame =  CGRectMake(0, 0, frame.size.width, height);
        self.pageControl.frame = CGRectMake(0, CGRectGetMaxY(self.collectionView.frame), frame.size.width, 20);
        
        [self setupDefaultPanel];
        
    }
    return self;
}

- (void)dealloc {
    if (_emotions) {
        
        [_emotions removeAllObjects];
        _emotions = nil;
    }
}

#pragma mark - Public
- (void)loadMorePanelManagers:(NSArray *)emotionManagers {
    
    if (self.emotions.count > 0) {
        [self.emotions removeAllObjects];
    }
    [self.emotions addObjectsFromArray:emotionManagers];
    
    [self.collectionView reloadData];
    
    [self caculateSectionPageCount];
    
    [self.collectionView layoutIfNeeded];
}

- (void)setScrollEnabled:(BOOL)enabled {
    self.collectionView.scrollEnabled = enabled;
}

#pragma mark - Private

- (void)setupDefaultPanel {
    
    NSMutableArray *tmpEmotions = [NSMutableArray array];
    NSMutableArray *emotionManagerArray = [NSMutableArray array];
    
    
//    nim_message_plus_album
    //nim_message_plus_take
    //nim_message_plus_location
    
    NSString* path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_album@3x" ofType:@"png"];
    MXEmotion *emotion = [[MXEmotion alloc] initWithTitle:@"相册" emotionName:@"相册" emotionId:@"1" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
    [tmpEmotions addObject:emotion];
    
    path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_take@3x" ofType:@"png"];
    emotion = [[MXEmotion alloc] initWithTitle:@"拍摄" emotionName:@"拍摄" emotionId:@"2" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
    [tmpEmotions addObject:emotion];
    
//    path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_voice@3x" ofType:@"png"];
//    emotion = [[MXEmotion alloc] initWithTitle:@"音频通话" emotionName:@"音频通话" emotionId:@"10" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
//    [tmpEmotions addObject:emotion];
    
    path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_location@3x" ofType:@"png"];
    emotion = [[MXEmotion alloc] initWithTitle:@"位置" emotionName:@"位置" emotionId:@"3" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
    [tmpEmotions addObject:emotion];
    
    path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_file@3x" ofType:@"png"];
    emotion = [[MXEmotion alloc] initWithTitle:@"文件" emotionName:@"文件" emotionId:@"11" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
    [tmpEmotions addObject:emotion];
    
//    path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_redpacket@3x" ofType:@"png"];
//    emotion = [[MXEmotion alloc] initWithTitle:@"红包" emotionName:@"红包" emotionId:@"4" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
//    [tmpEmotions addObject:emotion];
    
//    path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_transfer@3x" ofType:@"png"];
//    emotion = [[MXEmotion alloc] initWithTitle:@"转账" emotionName:@"转账" emotionId:@"5" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
//    [tmpEmotions addObject:emotion];
    
//    path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_deal@3x" ofType:@"png"];
//    emotion = [[MXEmotion alloc] initWithTitle:@"快捷交易" emotionName:@"快捷交易" emotionId:@"6" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
//    [tmpEmotions addObject:emotion];
    
    UIViewController *chatVC = [[MXRouter sharedInstance] getTopViewController];
    if(chatVC && [chatVC isKindOfClass:NSClassFromString(@"ChatViewVC")] && [[chatVC valueForKey:@"creatorRole"] integerValue] > 0) {
        //do nothing
    } else {
        path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_contact@3x" ofType:@"png"];
        emotion = [[MXEmotion alloc] initWithTitle:@"个人名片" emotionName:@"个人名片" emotionId:@"8" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
        [tmpEmotions addObject:emotion];
    }
    
//    path = [[NSBundle mainBundle]pathForResource:@"nim_message_plus_favorite@3x" ofType:@"png"];
//    emotion = [[MXEmotion alloc] initWithTitle:@"收藏" emotionName:@"收藏" emotionId:@"7" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
//    [tmpEmotions addObject:emotion];
    
    if([UDetail.user.is_group_send integerValue] == 1) {
        path = [[NSBundle mainBundle]pathForResource:@"自动转发@3x" ofType:@"png"];
        emotion = [[MXEmotion alloc] initWithTitle:@"自动转发" emotionName:@"自动转发" emotionId:@"6" emotionThumbnail:@"" emotionOriginal:path emotionOriginalURL:@"" emotionType:MXEmotionDefault];
        [tmpEmotions addObject:emotion];
    }
    
    MXEmotionSetting *managerForDefault = [[MXEmotionSetting alloc] initWithType:MXEmotionDefault emotionRow:2 emotionCol:4 emotions:tmpEmotions tagImage:nil];
    
    
    [emotionManagerArray addObject:managerForDefault];

    
    if (self.emotions.count > 0) {
        [self.emotions removeAllObjects];
    }
    [self.emotions addObjectsFromArray:emotionManagerArray];
    
    [self.collectionView reloadData];
    
  
    [self caculateSectionPageCount];
}

- (void)caculateSectionPageCount {

    if (!self.emotionManagers) {
        return;
    }
    // 目前emotionManagers只有一个section
    MXEmotionSetting *emotionManager = [self.emotionManagers firstObject];
    
    // 每页条数
    NSInteger pageSize = emotionManager.emotionRow*emotionManager.emotionCol;
    
    NSInteger count = emotionManager.emotions.count;
    
    NSInteger p1 = count / pageSize;
    NSInteger p2 = count % pageSize;
    NSInteger totalPage = 0;
    
    if(count <= pageSize ){
        totalPage = 1;
    }else if(p2==0){
        totalPage = p1;
    }else if(p2!=0){
        totalPage = p1+1;
    }
    
    self.pageControl.numberOfPages = totalPage;
    
    if (totalPage == 1) {
        self.pageControl.hidden = YES;
    }else {
        self.pageControl.hidden = NO;
    }

}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    MXEmotionSetting *emotionManager = [self.emotionManagers objectAtIndex:section];
    return [emotionManager.emotions count];

}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString* identify = @"moreCollectionCell";
    MXMoreItemCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    
    [cell sizeToFit];
    MXEmotionSetting *emotionManager = [self.emotionManagers objectAtIndex:indexPath.section];
    MXEmotion *emotion = [emotionManager.emotions objectAtIndex:indexPath.row];
    cell.emotion = emotion;
    cell.userInteractionEnabled = YES;
    cell.delegate = self;
    
    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MXEmotionSetting *emotionManager = [self.emotionManagers objectAtIndex:indexPath.section];
    NSInteger maxRow = emotionManager.emotionRow;
    NSInteger maxCol = emotionManager.emotionCol;
    CGFloat itemWidth = self.frame.size.width / maxCol;
    CGFloat itemHeight = (collectionView.frame.size.height) / maxRow;
    
    return CGSizeMake(itemWidth, itemHeight);
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGFloat contentOffsetX =  self.collectionView.contentOffset.x;
    CGFloat pageWidth = self.collectionView.frame.size.width;
    
    NSInteger pageIndex = (contentOffsetX + pageWidth / 2) / pageWidth;
    
    self.pageControl.currentPage = pageIndex;
}

#pragma mark - MXMoreItemCollectionCellDelegate
- (void)didSelectMoreItemAt:(MXEmotion *)emotion {
    
    if (_delegate) {
        if ([emotion isKindOfClass:[MXEmotion class]]) {
            if ([_delegate respondsToSelector:@selector(didSelectMoreItemAt:)]) {
                [_delegate didSelectMoreItemAt:emotion];
            }
        }
    }
}

#pragma mark - Getters
- (NSMutableArray *)emotions {
    if (!_emotions) {
        _emotions = [NSMutableArray array];
    }
    return _emotions;
}

- (NSArray*)emotionManagers {
    return self.emotions;
}

- (NSArray *)items {
    
    // visibleCells 如果想在viewdidload时获取，则要调用[self.collectionView layoutIfNeeded];否则要return 0;
    NSArray *cellsArray = [self.collectionView visibleCells];
    NSMutableArray *tmpArray = [NSMutableArray array];
    
    for (MXMoreItemCollectionCell *cell in cellsArray) {
        [tmpArray addObject:cell.imageButton];
    }
    
    return tmpArray;
}

@end
