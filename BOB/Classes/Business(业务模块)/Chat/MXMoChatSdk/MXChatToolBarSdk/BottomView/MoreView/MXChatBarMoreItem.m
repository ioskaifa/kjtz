//
//  MXChatBarMoreItem.m
//  ChatToolBar
//
//  Created by aken on 16/5/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXChatBarMoreItem.h"

@implementation MXChatBarMoreItem

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    
    UIImageView *imageView = [self imageView];
    CGRect imageFrame = imageView.frame;
    imageFrame.size.width=54;
    imageFrame.size.height=54;
    imageFrame.origin.y=23;
    imageFrame.origin.x = self.frame.size.width/2 - imageFrame.size.width/2;
    imageView.frame = imageFrame;
    
    UILabel *titleLabel = [self titleLabel];
    CGRect titleFrame = titleLabel.frame;
    titleFrame.size.width=self.frame.size.width;
    titleFrame.size.height=14;
    titleFrame.origin.x = ((self.frame.size.width - titleFrame.size.width)/ 2);
    titleFrame.origin.y=CGRectGetMaxY(imageView.frame) + 8 ;
    titleLabel.frame = titleFrame;
    titleLabel.textAlignment = NSTextAlignmentCenter;
}

@end
