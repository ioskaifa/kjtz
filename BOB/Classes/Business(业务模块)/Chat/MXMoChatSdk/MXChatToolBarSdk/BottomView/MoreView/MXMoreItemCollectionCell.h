//
//  MXMoreItemCollectionCell.h
//  ChatToolBar
//
//  Created by aken on 16/5/19.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MXEmotion;
@class MXChatBarMoreItem;

@protocol MXMoreItemCollectionCellDelegate<NSObject>

@optional

- (void)didSelectMoreItemAt:(MXEmotion*)emotion;

@end

@interface MXMoreItemCollectionCell : UICollectionViewCell

@property (nonatomic, weak) id<MXMoreItemCollectionCellDelegate> delegate;
@property (nonatomic, strong) MXChatBarMoreItem *imageButton;
@property (nonatomic, strong) MXEmotion *emotion;

@end
