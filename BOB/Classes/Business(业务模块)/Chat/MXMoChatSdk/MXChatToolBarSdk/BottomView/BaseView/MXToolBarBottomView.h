//
//  MXToolBarBottomView.h
//  ChatToolBar
//
//  聊天底部面板列表项
//  Created by aken on 16/5/19.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MXToolBarBottomView : UIView

/*!
 @property
 @brief 页码
 */
@property (nonatomic, strong) UIPageControl     *pageControl;

/*!
 @property
 @brief 列表
 */
@property (nonatomic, strong) UICollectionView  *collectionView;

/*!
 @property
 @brief 表情数组
 */
@property (nonatomic, strong, readonly) NSArray         *emotionManagers;

@end
