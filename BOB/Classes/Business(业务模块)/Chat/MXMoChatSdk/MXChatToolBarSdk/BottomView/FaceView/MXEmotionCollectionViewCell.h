//
//  MXEmotionCollectionViewCell.h
//  ChatToolBar
//
//  Created by aken on 16/4/22.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 静态表情size
static const NSInteger kMXFacialPanelViewEmojiWidth = 30;
/// 动态表情size
static const NSInteger kMXFacialPanelViewGifWidth = 66;

@class MXEmotion;
@class MXEmotionItemButton;

@protocol MXEmotionCollectionViewCellDelegate

@optional

- (void)didSendEmotion:(MXEmotion*)emotion;

@end

@interface MXEmotionCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<MXEmotionCollectionViewCellDelegate> delegate;
@property (nonatomic, strong) MXEmotionItemButton *imageButton;
@property (nonatomic, strong) MXEmotion *emotion;

@end
