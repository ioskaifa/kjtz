//
//  MXEmotionHighlightedView.m
//  MXChatToolBarSdk
//
//  Created by aken on 16/11/1.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionHighlightedView.h"

@implementation MXEmotionHighlightedView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {

        self.backgroundColor = [UIColor colorWithRed:227.0/255.0 green:226.0/255.0 blue:231.0/255.0 alpha:0.9];
        self.layer.cornerRadius = 4.0;
    }
    
    return self;
}

@end
