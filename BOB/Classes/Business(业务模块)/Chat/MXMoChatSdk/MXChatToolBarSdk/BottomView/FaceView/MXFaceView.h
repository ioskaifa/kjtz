//
//  MXFaceView.h
//  ChatToolBar
//
//  表情框
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMXFaceViewDelegate.h"

@class MXEmotion;

@interface MXFaceView : UIView

@property (nonatomic, weak) id<IMXFaceViewDelegate> delegate;

/*!
 @property
 @brief 表情数组
 */
@property (nonatomic, strong, readonly) NSArray *emotions;

/*!
 @method
 @brief 加载默认表情
 @param emotionManagers 数据源
 */
- (void)loadDefaultEmotionManagers:(NSArray*)emotionManagers;

/*!
 @method
 @brief 加载更多表情
 @param emotionManagers 数据源
 */
- (void)loadMoreEmotionManagers:(NSArray*)emotionManagers;

/*!
 @method
 @brief 隐藏添加表情入口
 */
- (void)hideAddEmotionEntrance;

/*!
 @method
 @brief 隐藏发送表情
 */
- (void)hideSendEmotion;

/*!
 @method
 @brief 禁止/开启发送按钮
 @param enabled YES/NO
 */
- (void)enabledSendButton:(BOOL)enabled;

@end
