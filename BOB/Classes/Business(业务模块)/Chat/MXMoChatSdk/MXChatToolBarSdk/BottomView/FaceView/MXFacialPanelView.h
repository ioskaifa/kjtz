//
//  MXFacialPanelView.h
//  ChatToolBar
//
//  显示表情
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXToolBarBottomView.h"

@class MXEmotion;

@protocol MXFacialPanelViewDelegate <NSObject>

@optional

/*!
 @method
 @brief 表情类型tab变化时触发
 @param index 下标
 */
- (void)didFacialPanelViewEmotionTypeChange:(NSInteger)index;

/*!
 @method
 @brief 选中表情
 @param str 表情字符串
 */
-(void)didSelectedFacialView:(NSString*)str;

/*!
 @method
 @brief 点击表情面板上的删除按钮
 @param str 表情字符串
 */
-(void)didDeleteSelectedEmotion:(NSString *)str;

/*!
 @method
 @brief 点击gif表情
 @param emotion 表情对象
 */
-(void)didSendFace:(MXEmotion *)emotion;

@end


@interface MXFacialPanelView : MXToolBarBottomView

/*!
 @property
 @brief 是否显示表情预览
 */
@property (nonatomic, assign) BOOL showPopPreView;

/*!
 @property
 @brief 代理
 */
@property (nonatomic, weak) id<MXFacialPanelViewDelegate> delegate;

/*!
 @method
 @brief 加载所有表情
 @param emotionManagers 表情数组
 */
-(void)loadFacialView:(NSArray*)emotionManagers;

/*!
 @method
 @brief 根据表情页，加载表情
 @param page 页码
 */
-(void)loadFacialViewWithPage:(NSInteger)page;

@end
