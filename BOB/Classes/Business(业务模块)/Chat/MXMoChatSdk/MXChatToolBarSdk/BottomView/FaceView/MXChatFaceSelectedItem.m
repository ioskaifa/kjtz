//
//  MXChatFaceSelectedItem.m
//  ChatToolBar
//
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXChatFaceSelectedItem.h"
#import "MXEmotionSetting.h"

//#if __has_include(<AFNetworking/AFNetworking.h>)
//#import <AFNetworking/AFNetworking.h>
//#else
//#import "AFNetworking.h"
//#endif

#define kButtonMagin 5
#define kButtonItemTag 100

@interface MXChatFaceSelectedItem()


@property (nonatomic, strong) UIView *bgView;

@property (assign, nonatomic) NSUInteger buttonItemCount;

@end

@implementation MXChatFaceSelectedItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.bgView=[[UIView alloc] initWithFrame:self.bounds];
        [self addSubview:self.bgView];
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    
    [super setFrame:frame];
    
    self.bgView.frame = self.bounds;
    
}

#pragma mark - Public

-(void)reloadDataSource:(NSArray *)dataSource {
    
    [self removeAllViews];
    
    if (!dataSource) {
        return;
    }
    
    self.buttonItemCount = [dataSource count];
    
    self.selectedButtonIndex = 0;
    CGFloat buttonWidth = kChatFaceSelectedItemWidth + kButtonMagin;
    
    @autoreleasepool {
        
        for (NSInteger i = 0; i < dataSource.count; i++) {
            
            UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(i*buttonWidth,0,buttonWidth,buttonWidth);
            [button setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
            button.imageView.contentMode = UIViewContentModeScaleAspectFit;
            id item=[dataSource objectAtIndex:i];
            
            if ([item isKindOfClass:[MXEmotionSetting class]]) {

                MXEmotionSetting *emotionManager = [dataSource objectAtIndex:i];
                if ([emotionManager.tagImage isKindOfClass:[UIImage class]]) {
                
                    [button setImage:emotionManager.tagImage forState:UIControlStateNormal];
                    
                }else if ([emotionManager.tagImage isKindOfClass:[NSString class]]) {
                    
                    NSString * encodingString = [emotionManager.tagImage stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
                    NSURL *url = [NSURL URLWithString:encodingString];

                     // 此处会卡线程，如果不想用sdwebimage，过多的依赖，需另外维护一个缓存图片的方法
                    // 方法1
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        // 耗时的操作
                        
                        __block UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                             [button setImage:image forState:UIControlStateNormal];
                        });  
                    });
                    // 方法2
//                    [button sd_setImageWithURL:url forState:UIControlStateNormal];
                }
            }
            
            if (i == 0) {
                
                if (self.selectedColor) {
                    button.backgroundColor = self.selectedColor;
                }
                
            }else {

                button.backgroundColor = [UIColor whiteColor];
            }
            
            button.titleLabel.font = [UIFont boldSystemFontOfSize:12];
            button.tag = kButtonItemTag+i;
            [button addTarget:self action:@selector(didSelectButtonIndex:) forControlEvents:UIControlEventTouchUpInside];

            [self.bgView addSubview:button];
            
            UIView *vLineView = [[UIView alloc] initWithFrame:CGRectMake(buttonWidth-1,
                                                                         buttonWidth/2 - 16,
                                                                         0.5,32)];
            vLineView.backgroundColor = [UIColor colorWithRed:.9f green:.9f blue:.9f alpha:1];
            [button addSubview:vLineView];
        }
    }
    
}

- (UIButton*)selectedItem {
    
    UIButton *tmpButton = nil;
    
    for (UIView *v in self.bgView.subviews) {
        if (v) {
            
            if ([v isKindOfClass:[UIButton class]]) {
                UIButton *tempBtn=(UIButton*)v;
                if (tempBtn.tag==_selectedButtonIndex + kButtonItemTag) {
                    tmpButton = tempBtn;
                    break;
                }
                
            }
        }
    }
    
    return tmpButton;
}

#pragma mark - 按钮事件
-(void)selectedAtIndex:(id)sender{
    
    UIButton* btn = (UIButton*)sender;
    NSInteger tag=btn.tag;
    
    if (tag !=_selectedButtonIndex+kButtonItemTag) {
        
        UIButton *tempBtn=(UIButton*)[self viewWithTag:_selectedButtonIndex+kButtonItemTag];
        tempBtn.selected=NO;
        _selectedButtonIndex=btn.tag - kButtonItemTag;
        tempBtn.layer.borderColor=[UIColor grayColor].CGColor;
        tempBtn.backgroundColor = [UIColor whiteColor];
    }
    
    btn.selected = YES;
    btn.backgroundColor = self.selectedColor;
}

- (void)removeAllViews {
    
    for (UIView *v in self.bgView.subviews) {
        if (v) {
            [v removeFromSuperview];
        }
    }
}

-(void)didSelectButtonIndex:(id)sender{
    
    [self selectedAtIndex:sender];
    
    if (self.didSelectedCallBack) {
        self.didSelectedCallBack(self.selectedButtonIndex);
    }
}

- (void)setSelectedButtonIndex:(NSInteger)selectedButtonIndex {
    
    NSUInteger tmpIndex = MIN(MAX(0, selectedButtonIndex), self.buttonItemCount-1);
    if (_selectedButtonIndex == tmpIndex) {
        return;
    }
    _selectedButtonIndex = tmpIndex;
    
    @autoreleasepool {
        for (UIView *v in self.bgView.subviews) {
            if (v) {
                
                if ([v isKindOfClass:[UIButton class]]) {
                    UIButton *tempBtn=(UIButton*)v;
                    if (tempBtn.tag==_selectedButtonIndex + kButtonItemTag) {
                        tempBtn.selected=YES;
                        tempBtn.backgroundColor = self.selectedColor;
                    }else{
                        tempBtn.selected=NO;
                        tempBtn.backgroundColor = [UIColor whiteColor];
                    }
                }
            }
        }
    }
}


@end
