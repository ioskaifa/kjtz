//
//  MXFacialPanelView.m
//  ChatToolBar
//
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXFacialPanelView.h"
#import "MXEmotionCollectionViewCell.h"
#import "MXFaceView.h"
#import "MXEmotionSetting.h"
#import "MXChatToolBarCommon.h"
#import "MXPopPreviewView.h"
#import "MXEmotionItemButton.h"
#import "MXEmotionHighlightedView.h"

#import "HorizontalCollectionViewLayout.h"

/// 分页控件高度
static const NSInteger MXFacialPanelViewPangeControlHeight = 8;
static const NSInteger MXFacialPanelViewPangeControlBotttomHeight = 27;
static const NSInteger MXFacialPanelViewPanelOutOfMinY = 14.0;

@interface MXFacialPanelView () <UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,MXEmotionCollectionViewCellDelegate>
{
    CGFloat     _itemWidth;
    CGFloat     _itemHeight;
}

@property (nonatomic)         NSInteger                 currentEmotionTypePage;

// 每个section页数
@property (nonatomic, strong) NSArray                   *sectionPages;

@property (nonatomic, strong, readwrite) NSArray        *emotionManagers;

@property (nonatomic, strong) NSMutableArray            *emotions;

@property (nonatomic, assign) BOOL isShowPopPreView;

@end

@implementation MXFacialPanelView
@synthesize emotionManagers = _emotionManagers;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
//        self.backgroundColor = [UIColor brownColor];
//        self.collectionView.backgroundColor = [UIColor darkGrayColor];
        [self setupUI];
        
        CGFloat height = frame.size.height - MXFacialPanelViewPangeControlBotttomHeight;
        self.collectionView.frame = CGRectMake(0, 0, frame.size.width, height);
        
        self.pageControl.frame = CGRectMake(0, frame.size.height - 2*MXFacialPanelViewPangeControlHeight, frame.size.width, MXFacialPanelViewPangeControlHeight);
    }
    return self;
}

- (void)setupUI {
    
    self.currentEmotionTypePage = 0;
    
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];

    [self.collectionView registerClass:[MXEmotionCollectionViewCell class] forCellWithReuseIdentifier:@"collectionCell"];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self action:@selector(longPressGestureRecognized:)];
//    longPress.minimumPressDuration = 0.5;
    [self.collectionView addGestureRecognizer:longPress];
    
//    longPress.cancelsTouchesInView = NO;

    
    [self.pageControl addObserver:self forKeyPath:@"numberOfPages" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)dealloc {
    
    [self.pageControl removeObserver:self forKeyPath:@"numberOfPages"];
    
    if (_emotions) {
        
        [_emotions removeAllObjects];
        _emotions = nil;
    }
    
    if (_sectionPages) {
        
        _sectionPages = nil;
    }
    
    self.collectionView.delegate = nil;
    self.collectionView.dataSource = nil;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    MXEmotionSetting *emotionManager = [self.emotionManagers objectAtIndex:section];
    return [emotionManager.emotions count];

}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    if (self.emotionManagers == nil || [self.emotionManagers count] == 0) {
        return 1;
    }
    return [self.emotionManagers count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString* identify = @"collectionCell";
    MXEmotionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    
    [cell sizeToFit];
    MXEmotionSetting *emotionManager = [self.emotionManagers objectAtIndex:indexPath.section];
    MXEmotion *emotion = [emotionManager.emotions objectAtIndex:indexPath.row];
    cell.emotion = emotion;
    cell.userInteractionEnabled = YES;
    cell.delegate = self;

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    
    MXEmotionSetting *emotionManager = [self.emotionManagers objectAtIndex:section];
    
    if (!emotionManager) {
        return CGSizeMake(0, 0);
    }
    
    CGFloat itemWidth = self.frame.size.width / emotionManager.emotionCol;
    NSInteger pageSize = emotionManager.emotionRow*emotionManager.emotionCol;
    NSInteger lastPage = (pageSize - [emotionManager.emotions count] % pageSize);
    
    if (lastPage < emotionManager.emotionRow ||[emotionManager.emotions count]%pageSize == 0) {
        return CGSizeMake(0, 0);
    } else{
        NSInteger size = lastPage/emotionManager.emotionRow;
        return CGSizeMake(size*itemWidth, self.frame.size.height);
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionReusableView *reusableview = nil;
    
    // 用于占位
    if (kind == UICollectionElementKindSectionFooter){
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        reusableview = footerview;
        reusableview.hidden = YES;
    }
    return reusableview;
}

#pragma mark --UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MXEmotionSetting *emotionManager = [self.emotionManagers objectAtIndex:indexPath.section];
    NSInteger maxRow = emotionManager.emotionRow;
    NSInteger maxCol = emotionManager.emotionCol;
    CGFloat itemWidth = self.frame.size.width / maxCol;
    CGFloat itemHeight = (collectionView.frame.size.height) / maxRow;
    
    return CGSizeMake(itemWidth, itemHeight);
}

#pragma makr - MXEmotionCollectionViewCellDelegate
- (void)didSendEmotion:(MXEmotion *)emotion {

    if (emotion) {
        // 默认表情
        if (emotion.emotionType == MXEmotionDefault) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedFacialView:)]) {
                [self.delegate didSelectedFacialView:emotion.emotionTitle];
            }
        } else { // gif
            
            if (emotion.emotionTitle.length > 0) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(didSendFace:)]) {
                    [self.delegate didSendFace:emotion];
                }
            }
        }
        
    }else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didDeleteSelectedEmotion:)]) {
            [self.delegate didDeleteSelectedEmotion:nil];
        }
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
   
    CGRect visibleRect = (CGRect){.origin = self.collectionView.contentOffset, .size = self.collectionView.bounds.size};
    CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
    NSIndexPath *visibleIndexPath = [self.collectionView indexPathForItemAtPoint:visiblePoint];
    
    if (visibleIndexPath) {
        
        self.currentEmotionTypePage = visibleIndexPath.section;
    }
    
    [self changePageCountrolPage];
    
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    
    if ([keyPath isEqualToString:@"numberOfPages"]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didFacialPanelViewEmotionTypeChange:)]) {
            
            [self.delegate didFacialPanelViewEmotionTypeChange:self.currentEmotionTypePage];
        }
    }
}

#pragma mark - Private
- (void)caculateSectionPageCount {
    
    
    NSMutableArray *tempPageArray = [NSMutableArray array];
    
    @autoreleasepool {
        for (MXEmotionSetting *emotionManager in self.emotionManagers) {
            
            // 每页条数
            NSInteger pageSize = emotionManager.emotionRow * emotionManager.emotionCol;
            
            NSInteger count = emotionManager.emotions.count;
            
            NSInteger p1 = count / pageSize;
            NSInteger p2 = count % pageSize;
            NSInteger totalPage = 0;
            
            if(count <= pageSize ){
                totalPage = 1;
            }else if(p2==0){
                totalPage = p1;
            }else if(p2!=0){
                totalPage = p1+1;
            }
            
            [tempPageArray addObject:@(totalPage)];
            
        }
    }
    
    self.sectionPages = tempPageArray;
}

- (void)changePageCountrolPage {
  
    NSInteger currentTotalPage = [self.sectionPages[self.currentEmotionTypePage] intValue];
    NSInteger curPage = 0;
    CGFloat contentOffsetX =  self.collectionView.contentOffset.x;
    CGFloat pageWidth = self.collectionView.frame.size.width;
    
    NSInteger pageIndex = (contentOffsetX + pageWidth / 2) / pageWidth;
    // 第一个section时
    if (self.currentEmotionTypePage == 0) {
        
        curPage = (contentOffsetX + pageWidth / 2) / pageWidth;
        
    }else if (self.currentEmotionTypePage == [self.emotionManagers count] - 1) { //  最后一个section
        
        // 剩下的页数
        CGFloat currentSectionTotalWidth = currentTotalPage * pageWidth;
        NSInteger preTotalPage = (self.collectionView.contentSize.width - currentSectionTotalWidth) / pageWidth;
        
        curPage = pageIndex - preTotalPage;
        
    }else { // 两者之间

        // 之前页数之和
        NSInteger tempPreTotalPage = 0;
        for (NSInteger i=0; i< self.currentEmotionTypePage; i++) {
            tempPreTotalPage += [self.sectionPages[i] intValue];
        }
        
        curPage = pageIndex - tempPreTotalPage;
    }

    self.pageControl.numberOfPages = currentTotalPage;
    self.pageControl.currentPage = curPage;
    
    [self.collectionView reloadData];
}

- (void)longPressGestureRecognized:(id)sender {

    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;

    CGPoint location = [longPress locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
    
    MXEmotionCollectionViewCell *cell = (MXEmotionCollectionViewCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
   
    // 如果是默认的静态表情，则不显示预览
    if ([cell.emotion isKindOfClass:[NSString class]]) {
        return;
    }
    if (!cell.emotion) {
        return;
    }
    if (cell.emotion.emotionType == MXEmotionDefault) {
        return;
    }
    
    static   MXPopPreviewView *snapshotView = nil;
    CGRect outRect = self.collectionView.bounds;
    outRect.origin.y = outRect.origin.y - MXFacialPanelViewPanelOutOfMinY;

    static MXEmotionHighlightedView *highlightedView = nil;
    
    if (cell.emotion.emotionType == MXEmotionGif) {
        if ([cell.emotion.emotionTitle isEqualToString:@""] || [cell.emotion.emotionName isEqualToString:@""]) {
            [snapshotView removeFromSuperview];
            [highlightedView removeFromSuperview];
            return;
        }
    }
    
    if (longPress.state == UIGestureRecognizerStateBegan) {
        
        
        snapshotView = [self customSnapshotFromView:cell];
        [self addSubview:snapshotView];
        [snapshotView reloadData:cell.emotion];
        
        CGPoint converCenter = [self changeSnapViewCenter:snapshotView fromView:cell];
        snapshotView.center = converCenter;
        
        highlightedView = [self customHighlightedView:cell];
        
        [self addSubview:highlightedView];
        [self addSubview:highlightedView];
        
        CGPoint hightedCenter = [self changeHighlightedViewCenter:highlightedView fromView:cell];
        highlightedView.center = hightedCenter;
        
        [self sendSubviewToBack:highlightedView];
        
    }else if (longPress.state == UIGestureRecognizerStateChanged){
        
        
        CGPoint converCenter = [self changeSnapViewCenter:snapshotView fromView:cell];
        
        // 如果当前点不在snapshotView的区域里，则更新snapshotView的位置
        if (!CGPointEqualToPoint(converCenter, snapshotView.center)) {
            [snapshotView reloadData:cell.emotion];
            
            CGRect rect = [self.collectionView convertRect:cell.frame toView:self];
            [snapshotView changePreView:self fromRect:rect];
            [snapshotView setNeedsDisplay];
            
            CGPoint converCenter = [self changeSnapViewCenter:snapshotView fromView:cell];
            snapshotView.center = converCenter;
            
        }
        
        CGPoint orginhightedCenter = [self changeHighlightedViewCenter:highlightedView fromView:cell];
        // 当触摸点不在highlightedView的区域里时，更新highlightedView的位置
        if (!CGPointEqualToPoint(orginhightedCenter, highlightedView.center)) {
            CGPoint changehightedCenter = [self changeHighlightedViewCenter:highlightedView fromView:cell];
            highlightedView.center = changehightedCenter;
            
        }

        // 超出范围后，则删除掉
        
        CGFloat contentOffsetX =  self.collectionView.contentOffset.x;
        
        if (!CGRectContainsPoint(outRect, location)) {
            [snapshotView removeFromSuperview];
            [highlightedView removeFromSuperview];
        }
        
        // y
        if (location.y < MXFacialPanelViewPanelOutOfMinY) {
            [snapshotView removeFromSuperview];
            [highlightedView removeFromSuperview];
        }
        
        // x
        CGFloat offX = location.x - contentOffsetX;
        if (offX < MXFacialPanelViewPanelOutOfMinY) {
            [snapshotView removeFromSuperview];
            [highlightedView removeFromSuperview];
        }else if (offX > self.bounds.size.width - MXFacialPanelViewPanelOutOfMinY) {
            [snapshotView removeFromSuperview];
            [highlightedView removeFromSuperview];
        }
        
    }else if (longPress.state == UIGestureRecognizerStateEnded) {
        
        [snapshotView removeFromSuperview];
        [highlightedView removeFromSuperview];
        
    }else {
        [snapshotView removeFromSuperview];
        [highlightedView removeFromSuperview];
    }
    
}

- (CGPoint)changeSnapViewCenter:(MXPopPreviewView*)snapshotView fromView:(MXEmotionCollectionViewCell*)cell {
    
    CGSize contentSize = snapshotView.frame.size;
    
    CGFloat outerWidth = self.bounds.size.width;
    
    CGPoint converCenter = [self.collectionView convertPoint:cell.center toView:self];
    CGPoint center = CGPointMake(converCenter.x,
                                 converCenter.y - cell.imageButton.frame.size.height);
    
    if (center.x < 50) {
        
        center.x = kMXFacialPanelViewGifWidth + MXPopPreviewViewMarginX;
        
    }else if ((center.x + contentSize.width/2 + MXPopPreviewViewMarginX) > outerWidth) {
        
        center.x = outerWidth - kMXFacialPanelViewGifWidth - MXPopPreviewViewMarginX; //center.x - 10;
    }
    
    center.y = center.y - kMXFacialPanelViewGifWidth/2 - MXPopPreviewViewMarginY;
    
    return center;
    
}

- (MXPopPreviewView *)customSnapshotFromView:(MXEmotionCollectionViewCell *)inputView {
    
    
    CGRect rect = [self.collectionView convertRect:inputView.frame toView:self];
    MXPopPreviewView *popView = [[MXPopPreviewView alloc] init];
    [popView showPreviewInView:self fromRect:rect];
    
    return popView;
    
}

- (MXEmotionHighlightedView*)customHighlightedView:(MXEmotionCollectionViewCell *)inputView {
    
    CGRect lightedRect = CGRectMake(0, 0, kMXFacialPanelViewGifWidth + 10, kMXFacialPanelViewGifWidth + 10);
    MXEmotionHighlightedView *lightedView = [[MXEmotionHighlightedView alloc] initWithFrame:lightedRect];
    
    return lightedView;
}

- (CGPoint)changeHighlightedViewCenter:(MXEmotionHighlightedView*)lightedView fromView:(MXEmotionCollectionViewCell*)cell {
    
   
    CGPoint converCenter = [self.collectionView convertPoint:cell.center toView:self];
    CGPoint center = CGPointMake(converCenter.x,
                                 converCenter.y);
    
    return center;
    
}

#pragma mark - Getters
- (NSMutableArray *)emotions {
    if (!_emotions) {
        _emotions = [NSMutableArray array];
    }
    return _emotions;
}

#pragma mark - Public 
-(void)loadFacialView:(NSArray*)emotionManagers {
    
    if (self.emotions.count > 0) {
        [self.emotions removeAllObjects];
    }
    [self.emotions addObjectsFromArray:emotionManagers];
    
    [self caculateSectionPageCount];
    
    [self changePageCountrolPage];
    [self.collectionView reloadData];
}

-(void)loadFacialViewWithPage:(NSInteger)page {
    
    [self.collectionView reloadData];
    
    self.currentEmotionTypePage = page;
    
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:page]
                            atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                    animated:NO];
    CGPoint offSet = self.collectionView.contentOffset;
    if (page == 0) {
        [self.collectionView setContentOffset:CGPointMake(0, 0) animated:NO];
    } else {
        [self.collectionView setContentOffset:CGPointMake(CGRectGetWidth(self.frame)*((int)(offSet.x/CGRectGetWidth(self.frame))+1), 0) animated:NO];
    }
    [self changePageCountrolPage];
    
}

- (NSArray*)emotionManagers {
    return _emotions;
}

- (void)setShowPopPreView:(BOOL)showPopPreView {
    self.isShowPopPreView = showPopPreView;
}

@end
