//
//  MXPopPreviewView.m
//  ChatToolBar
//
//  Created by aken on 16/5/6.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXPopPreviewView.h"
#import "FLAnimatedImage.h"

#import "MXChatToolBarCommon.h"
#import "MXEmotionSetting.h"


CGFloat const MXPopPreviewViewBackgroundWidth = 140.f;
CGFloat const MXPopPreviewViewBackgroundHeight = 128.f;

@interface MXPopPreviewView()

@end

@interface MXPopPreviewView()
{
    CGFloat  _arrowPosition;
}

// 表情背景
@property (nonatomic, strong) UIView *contentView;

// Toolbar表情
@property (nonatomic, strong) MXEmotion  *emotion;


@end

@implementation MXPopPreviewView

- (instancetype)initWithPreviewFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if(self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        self.layer.shadowOpacity = 0.4;
        self.layer.shadowOffset = CGSizeMake(2, 2);
        self.layer.shadowRadius = 10.0;
        
        
        [self addSubview:self.contentView];
        [self.contentView addSubview:self.faceImageView];
        
        self.contentView.frame = self.bounds;
        self.faceImageView.frame = self.contentView.bounds;
        
        self.faceImageView.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizer:)];
        [self.contentView addGestureRecognizer:tap];
        
        
        [self arrowDefaultPosition];
    }
    
    return self;
}

- (instancetype)init {
    
    self = [self initWithPreviewFrame:CGRectZero];
    
    if(self) {
        
        self.contentView.frame = CGRectMake(0, 0, MXPopPreviewViewBackgroundWidth, MXPopPreviewViewBackgroundHeight);
        self.faceImageView.frame = self.contentView.bounds;
    }
    
    return self;
}

- (void)arrowDefaultPosition {
    
    CGSize contentSize = _contentView.frame.size;
    
    // 箭头的起始点
//    CGFloat arrowHeight = contentSize.height + MXPopPreviewViewArrowSize;
    CGFloat arrowWidthHalf = contentSize.width * 0.5f;
    
    CGFloat rectX = contentSize.width * 0.5f;
//    CGFloat rectY = contentSize.height + MXPopPreviewViewArrowSize;

    CGPoint point = (CGPoint){
        rectX - arrowWidthHalf,
        0
    };
    
    _arrowPosition = rectX - point.x;
    
    // 背景
    _contentView.frame = (CGRect){CGPointZero, contentSize};
    _faceImageView.frame = CGRectInset(_contentView.bounds, 5, 5);
    
    self.frame = (CGRect) {
        point,
        contentSize.width,
        contentSize.height + MXPopPreviewViewArrowSize
    };
    
    _faceImageView.center = _contentView.center;
}

- (void)tapGestureRecognizer:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapPopPreviewEmotion:)]) {
        [self.delegate tapPopPreviewEmotion:self.emotion];
    }
}


#pragma mark - Public

- (void)showPreviewInView:(UIView *)view
              fromRect:(CGRect)fromRect {
    
    [view addSubview:self];
    
    [self changePreView:view fromRect:fromRect];
}

- (void)changePreView:(UIView *)view
             fromRect:(CGRect)fromRect {
    
    CGSize contentSize = _contentView.frame.size;
    CGFloat outerWidth = view.bounds.size.width;
    
    
    CGFloat rectX = fromRect.origin.x + fromRect.size.width * 0.5f;
    CGFloat rectY = fromRect.origin.y;
    
    // 箭头的起始点
    CGFloat arrowHeight = contentSize.height + MXPopPreviewViewArrowSize;
    CGFloat arrowWidthHalf = contentSize.width * 0.5f;
    
    CGPoint point = (CGPoint){
        rectX - arrowWidthHalf,
        rectY - arrowHeight
    };
    
    
    if (point.x < MXPopPreviewViewMarginX) {
        point.x = MXPopPreviewViewMarginX;
    }
    
    if ((point.x + contentSize.width + MXPopPreviewViewMarginX) > outerWidth) {
        point.x = outerWidth - contentSize.width - MXPopPreviewViewMarginX;
    }
    
    _arrowPosition = rectX - point.x;
    _contentView.frame = (CGRect){CGPointZero, contentSize};
    _faceImageView.frame = CGRectInset(_contentView.bounds, 5, 5);
    
    self.frame = (CGRect) {
        point,
        contentSize.width,
        contentSize.height + MXPopPreviewViewArrowSize
    };

    _faceImageView.center = _contentView.center;
}

- (void)reloadData:(id)model {
    
    MXEmotion *tmpModel = (MXEmotion*)model;
    
    if ([_emotion.emotionOriginal isEqualToString:tmpModel.emotionOriginal]) {
        return;
    }
    
    _emotion = tmpModel;
    
    
//    NSString *urlString = _emotion.emotionOriginal;
//    NSString *encodingString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    
//    if ([urlString hasPrefix:@"http"] ||
//        [urlString hasPrefix:@"https"]) {
//
//        NSURL *url = [NSURL URLWithString:encodingString];
//
//
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            // 耗时的操作
//
//            __block NSData *data = [NSData dataWithContentsOfURL:url];
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
//                _faceImageView.animatedImage = image;
//            });
//        });
//
//
//    }else {
    NSString* path = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"MXChatToolBar.bundle"] ;
    NSString* imagePath = [NSString stringWithFormat:@"%@/Emotion/gif/%@",path,_emotion.emotionId];
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfFile:imagePath]];
    _faceImageView.animatedImage = image;
//    }
    
//    [UIView animateWithDuration:.3 animations:^{
//        _faceImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.3, 1.3);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:.2 animations:^{
//            _faceImageView.transform = CGAffineTransformIdentity;
//        }];
//    }];

}

- (void)showData:(id)model {
    
    if (!model) {
        return;
    }
    
    MXEmotion *tmpModel = (MXEmotion*)model;
    _emotion = tmpModel;
    
    NSString *urlString = _emotion.emotionOriginal;
    NSString *encodingString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfFile:encodingString]];
    _faceImageView.animatedImage = image;
}

- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];
    
    CGRect frame = self.bounds;
    
    CGFloat y = frame.origin.y + frame.size.height;
    
    // 画一个三角
    UIBezierPath *arrowPath = [UIBezierPath bezierPath];
    
    CGFloat arrowX = _arrowPosition;
    CGFloat arrowX0 = arrowX - MXPopPreviewViewArrowSize;
    CGFloat arrowX1 = arrowX + MXPopPreviewViewArrowSize;
    CGFloat arrowY0 = y - MXPopPreviewViewArrowSize - 0.44;
    CGFloat arrowY1 = y;
    
    [arrowPath moveToPoint:    (CGPoint){arrowX, arrowY1}];
    [arrowPath addLineToPoint: (CGPoint){arrowX1, arrowY0}];
    [arrowPath addLineToPoint: (CGPoint){arrowX0, arrowY0}];
    [arrowPath addLineToPoint: (CGPoint){arrowX, arrowY1}];
    
//    [[UIColor redColor] setStroke];
//
    
    [arrowPath closePath];
    
    [[UIColor colorWithRed:1 green:1 blue:1 alpha:0.92] set];
    
    [arrowPath fill];
    
//    arrowPath.lineWidth = 0;
//    
//    // 设置画笔颜色
//    UIColor *strokeColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.92];
//    [strokeColor set];
//    
//    // 根据我们设置的各个点连线
//    [arrowPath stroke];

}

#pragma mark - Getters
- (UIView*)contentView {
    
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
        _contentView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.92];
        _contentView.layer.cornerRadius = 8.0;
    }
    
    return _contentView;
}

- (FLAnimatedImageView*)faceImageView {
    if (!_faceImageView) {
        _faceImageView = [[FLAnimatedImageView alloc] init];
    }
    return _faceImageView;
}

@end
