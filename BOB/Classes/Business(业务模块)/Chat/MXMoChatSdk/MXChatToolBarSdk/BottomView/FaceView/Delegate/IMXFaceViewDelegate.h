//
//  IMXFaceViewDelegate.h
//  MXChatToolBarSdk
//
//  MXFaceView 协议
//  Created by aken on 16/6/2.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MXEmotion;

@protocol IMXFaceViewDelegate <NSObject>

@required

/*!
 @method
 @brief 静态表情
 @param emotionStr 表情字符串
 @param isDelete   是否是删除
 */
- (void)didSelectedFacialView:(NSString *)emotionStr isDelete:(BOOL)isDelete;

/*!
 @method
 @brief  Gif动态表情
 @param emotion 表情
 */
- (void)didSendFaceWithEmotion:(MXEmotion *)emotion;

/*!
 @method
 @brief  表情面板上的发送按钮
 */
- (void)didSendFace;

/*!
 @method
 @brief  添加表情
 */
- (void)didAddEmotion;

@end