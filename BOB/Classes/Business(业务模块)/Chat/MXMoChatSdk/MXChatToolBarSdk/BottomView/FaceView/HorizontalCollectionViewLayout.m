//
//  HorizontalCollectionViewLayout.m
//  ChatToolBar
//
//  Created by aken on 16/5/16.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "HorizontalCollectionViewLayout.h"

#import "MXToolBarBottomView.h"
#import "MXEmotionSetting.h"

@implementation HorizontalCollectionViewLayout

- (id)init {
    
    self = [super init];
    if (self) {
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.minimumLineSpacing = 0;
        self.minimumInteritemSpacing = 0;
        
    }
    return self;
}

- (CGSize)collectionViewContentSize {
    
    CGSize size = [super collectionViewContentSize];
    
    CGFloat collectionViewWidth = self.collectionView.frame.size.width;
    
    NSInteger columnCount = (int)ceil((size.width / collectionViewWidth));
    
    CGSize newSize = CGSizeMake((columnCount) * collectionViewWidth, size.height);
    
    return newSize;
}


- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    id dataSource = self.collectionView.dataSource;
    
    if (!dataSource) {
        return nil;
    }
    
    MXEmotionSetting *emotionManager = nil;
    if ([dataSource isKindOfClass:[MXToolBarBottomView class]]) {
        emotionManager = [((MXToolBarBottomView*)dataSource).emotionManagers objectAtIndex:indexPath.section];
    }
    
    NSInteger maxRow = emotionManager.emotionRow;
    NSInteger maxCol = emotionManager.emotionCol;
    
    if (maxRow == 0 || maxCol == 0) {
        return nil;
    }
    
//    NSInteger maxCol1 = (int)(self.collectionView.frame.size.width / self.itemSize.width);
//    NSInteger maxRow1 = (int)(self.collectionView.frame.size.height / self.itemSize.height);
    
    NSInteger itemPage = (int)indexPath.row/(maxCol * maxRow);
    NSInteger rowPosition = indexPath.row - (itemPage * maxCol * maxRow);
    
    NSInteger rowPer = (int)(rowPosition / maxCol);
    NSInteger colPer = rowPosition % maxCol;
    
    NSInteger item = rowPer + colPer * maxRow + itemPage * maxCol * maxRow;
    
    NSIndexPath *fakeIndexPath = [NSIndexPath indexPathForItem:item inSection:indexPath.section];
    UICollectionViewLayoutAttributes *attributes = [super layoutAttributesForItemAtIndexPath:fakeIndexPath];
    
    return attributes;

    
}

- (NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    
    CGFloat newX = MIN(0, rect.origin.x - rect.size.width/2);
    CGFloat newWidth = rect.size.width*2 + (rect.origin.x - newX);
    
    CGRect newRect = CGRectMake(newX, rect.origin.y, newWidth, rect.size.height);

    NSArray *allAttributesInRect = [[NSArray alloc] initWithArray:[super layoutAttributesForElementsInRect:newRect] copyItems:YES];
    
    for (UICollectionViewLayoutAttributes *attr in allAttributesInRect) {
        UICollectionViewLayoutAttributes *newAttr = [self layoutAttributesForItemAtIndexPath:attr.indexPath];
        
        attr.frame = newAttr.frame;
        attr.center = newAttr.center;
        attr.bounds = newAttr.bounds;
        attr.hidden = newAttr.hidden;
        attr.size = newAttr.size;
    }
    
    return allAttributesInRect;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}


@end

