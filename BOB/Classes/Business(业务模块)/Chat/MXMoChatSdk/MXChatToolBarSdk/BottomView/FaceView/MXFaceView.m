//
//  MXFaceView.m
//  ChatToolBar
//
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXFaceView.h"
#import "MXChatToolBarCommon.h"
#import "MXChatFaceSelectedItem.h"
#import "MXEmotionSetting.h"
#import "MXEmotionManager.h"
#import "MXFacialPanelView.h"

static const NSInteger FaceSelectedItemMargin = 10;
static const NSInteger EmotionForGifOnePageTotalCount = 8;
//static const NSInteger MXFaceViewBottomTabViewItemWidth = 46;
static const NSInteger MXFaceViewBottomTabViewItemHeight = 36;
static const NSInteger MXFacialPanelViewDefaultHeight = 222; // 222 + 26

@interface MXFaceView()<MXFacialPanelViewDelegate>

// 底部表情面板封面tab背景view
@property (nonatomic, strong) UIView            *bottomView;
@property (nonatomic, strong) UIScrollView      *bottomScrollView;
@property (nonatomic, strong) UIButton          *addButton;
@property (nonatomic, strong) UIButton          *sendButton;
@property (nonatomic, strong) MXChatFaceSelectedItem *faceSelectedItem;

@property (nonatomic, strong) MXFacialPanelView *facialPanelView;
@property (nonatomic, strong) UIView            *vLineView;
@property (nonatomic, strong) UIView            *vSendLineView;


@property (nonatomic, strong) NSArray           *emotionManagers;

@property (nonatomic, strong, readwrite) NSArray *emotions;

@end

@implementation MXFaceView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
  
        [self setupUI];

        [self setupDefaultEmotion];
        
    }
    return self;
}

- (void)dealloc {
    
    if (_emotionManagers) {
        _emotionManagers = nil;
    }
    
    [_facialPanelView removeFromSuperview];
    _facialPanelView.delegate=nil;
    _facialPanelView=nil;
}

//- (void)willMoveToSuperview:(UIView *)newSuperview {
//    if (newSuperview) {
//        [self reloadEmotionData];
//    }
//}


#pragma mark - Private
- (void)setupUI {
    
    self.backgroundColor = MXChatBarBottomViewColor;
    
    [self addSubview:self.facialPanelView];
    [self addSubview:self.bottomView];
    [self.bottomView addSubview:self.addButton];
    [self.bottomView addSubview:self.sendButton];
    [self.bottomView addSubview:self.bottomScrollView];
    [self.bottomScrollView addSubview:self.faceSelectedItem];
    [self.bottomView addSubview:self.vLineView];
    [self.bottomView addSubview:self.vSendLineView];
    
    [self updateUIFrame];
}

- (void)updateUIFrame {
    
    // 底部
    self.bottomView.frame = CGRectMake(0, CGRectGetHeight(self.frame) - MXFaceViewBottomTabViewItemHeight, CGRectGetWidth(self.frame), MXFaceViewBottomTabViewItemHeight);
   
    // 添加表情按钮
//    self.addButton.frame = CGRectMake(0, 0, MXFaceViewBottomTabViewItemHeight, CGRectGetHeight(self.bottomView.frame));
    self.addButton.frame = CGRectMake(0, 0, 0, CGRectGetHeight(self.bottomView.frame));
    // 发送表情按钮
    self.sendButton.frame = CGRectMake(CGRectGetWidth(self.bottomView.frame) - MXFaceViewBottomTabViewItemHeight - 10, 0,
                                       MXFaceViewBottomTabViewItemHeight + 10, CGRectGetHeight(self.bottomView.frame));
    
    //表情封面tab
    self.bottomScrollView.frame = CGRectMake(CGRectGetMaxX(self.addButton.frame), 0,
                                             CGRectGetWidth(self.frame) - MXFaceViewBottomTabViewItemHeight * 2, CGRectGetHeight(self.bottomView.frame));
    
    self.faceSelectedItem.frame = self.bottomScrollView.bounds;
    
    // 分割线
    self.vLineView.frame = CGRectMake(CGRectGetMaxX(self.addButton.frame),
                                      MXFaceViewBottomTabViewItemHeight/2 - 32/2,
                                      0.5, 32);
    self.vSendLineView.frame = CGRectMake(CGRectGetMinX(self.sendButton.frame),
                                      0,
                                      0.5, MXFaceViewBottomTabViewItemHeight);
    
    CALayer *shadowLayer = [[CALayer alloc]init];
    shadowLayer.frame = CGRectMake(0, 1, 0.4, CGRectGetHeight(self.vSendLineView.frame) - 1);
    shadowLayer.backgroundColor = self.vSendLineView.backgroundColor.CGColor;
    shadowLayer.shadowColor = [UIColor blackColor].CGColor;
    shadowLayer.shadowOffset = CGSizeMake(-1.0, 0.0);
    shadowLayer.shadowOpacity = 0.6;
    shadowLayer.shadowRadius = 2.0;

    [self.vSendLineView.layer addSublayer:shadowLayer];

}


// 默认有四种表情：静态表情和3种gif表情
- (void)setupDefaultEmotion {
    
    NSMutableArray *tmpEmotions = [NSMutableArray array];
    
    NSArray *tmpArray = [MXEmotionManager manager].defaultEmoji;
    
    @autoreleasepool {
        for (NSDictionary *dicitonary in tmpArray) {
            
            MXEmotion *emotion = [[MXEmotion alloc] initWithTitle:dicitonary[@"faceName"] emotionName:dicitonary[@"faceName"]emotionId:dicitonary[@"faceId"] emotionThumbnail:dicitonary[@"faceImageName"] emotionOriginal:dicitonary[@"faceImageName"] emotionOriginalURL:@"" emotionType:MXEmotionDefault];
            [tmpEmotions addObject:emotion];
        }
    }

    MXEmotionSetting *managerForDefault = [[MXEmotionSetting alloc] initWithType:MXEmotionDefault emotionRow:3 emotionCol:7 emotions:tmpEmotions tagImage:[UIImage imageNamed:MXChatToolBarBundle(@"ToolBar/Motalk_toolbar_default_system")]];
    NSString* path = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"MXChatToolBar.bundle"] ;
    
    NSMutableArray *tmpGifEmotions = [NSMutableArray array];
    
    NSArray *tmpGifArray = [MXEmotionManager manager].defaultGifs;
    @autoreleasepool {
        for (NSDictionary *dicitonary in tmpGifArray) {
            MXEmotion *emotion = [[MXEmotion alloc] initWithTitle:dicitonary[@"faceName"] emotionName:dicitonary[@"faceName"]emotionId:dicitonary[@"faceId"] emotionThumbnail:[NSString stringWithFormat:@"%@/Emotion/gif/%@",path,dicitonary[@"faceImageName"]] emotionOriginal:[NSString stringWithFormat:@"%@/Emotion/gif/mt_anger.gif",path] emotionOriginalURL:@"" emotionType:MXEmotionGif];
            [tmpGifEmotions addObject:emotion];
        }
    }
    
    MXEmotionSetting *managerMoya = [[MXEmotionSetting alloc] initWithType:MXEmotionGif emotionRow:2 emotionCol:3 emotions:tmpGifEmotions tagImage:[UIImage imageNamed:@"zhunian"]];
    ///隐藏第三方表情包
    self.emotionManagers = @[managerForDefault];
    
    self.emotions = self.emotionManagers;
    

    
    
    [self loadDefaultEmotionManagers:self.emotionManagers];
    
    
}

- (void)animationToHideSendButton:(BOOL)hidden {
    
    if (hidden) {
        
        [UIView animateWithDuration:0.35f animations:^{
            
            CGFloat bHeight = CGRectGetHeight(self.bottomView.frame);
            self.sendButton.frame = CGRectMake(CGRectGetWidth(self.bottomView.frame), 0, bHeight + 10, CGRectGetHeight(self.bottomView.frame));
            self.vSendLineView.frame = CGRectMake(CGRectGetMinX(self.sendButton.frame),
                                                  0,
                                                  0.5, 40);
            
        } completion:nil];
        
    }else {
        
        [UIView animateWithDuration:0.35f animations:^{
            
            CGFloat bHeight = CGRectGetHeight(self.bottomView.frame);
            self.sendButton.frame = CGRectMake(CGRectGetWidth(self.bottomView.frame) - bHeight - 10, 0, bHeight + 10, CGRectGetHeight(self.bottomView.frame));
            self.vSendLineView.frame = CGRectMake(CGRectGetMinX(self.sendButton.frame),
                                                  0,
                                                  0.5, 40);
            
        } completion:nil];
    }
}

#pragma mark - 事件
- (void)sendButtonClick {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSendFace)]) {
        [self.delegate didSendFace];
    }
}

- (void)addButtonClick {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didAddEmotion)]) {
        [self.delegate didAddEmotion];
    }
}

#pragma mark - Delegate 
- (void)didFacialPanelViewEmotionTypeChange:(NSInteger)index {
   
    self.faceSelectedItem.selectedButtonIndex = index;
    
    if (index == 0) { // 静态表情才有发送按钮
        
        [self animationToHideSendButton:NO];
        self.vSendLineView.hidden = NO;
        
    }else {
        [self animationToHideSendButton:YES];
        self.vSendLineView.hidden = YES;
    }

    CGFloat itemX = self.faceSelectedItem.selectedItem.frame.origin.x;
    CGFloat scrollX = self.bottomScrollView.contentOffset.x;
    
    CGFloat addWidth = CGRectGetWidth(self.addButton.frame);
    CGFloat sendWidth = CGRectGetWidth(self.sendButton.frame);
    
    
    if(itemX - scrollX > self.bounds.size.width - self.faceSelectedItem.selectedItem.frame.size.width - addWidth - sendWidth) {
        
        CGFloat pargin = itemX - self.bottomScrollView.frame.size.width + sendWidth;
        [self.bottomScrollView setContentOffset:CGPointMake(pargin, 0) animated:YES];
    }
    
    if(itemX - scrollX < FaceSelectedItemMargin) {
        if (itemX - FaceSelectedItemMargin < 0) {
            [self.bottomScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }else {
            [self.bottomScrollView setContentOffset:CGPointMake(itemX - FaceSelectedItemMargin, 0) animated:YES];
        }
    }
}

/// 选中表情
-(void)didSelectedFacialView:(NSString*)str {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedFacialView:isDelete:)]) {
        [self.delegate didSelectedFacialView:str isDelete:NO];
    }
}

/// 点击表情面板上的删除按钮
-(void)didDeleteSelectedEmotion:(NSString *)str {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedFacialView:isDelete:)]) {
        [self.delegate didSelectedFacialView:str isDelete:YES];
    }
}

/// gif表情
-(void)didSendFace:(MXEmotion *)emotion {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSendFaceWithEmotion:)]) {
        [self.delegate didSendFaceWithEmotion:emotion];
    }
}

#pragma mark - Public
- (void)loadDefaultEmotionManagers:(NSArray*)emotionManagers {

    if (emotionManagers.count == 0) {
        return;
    }
    // 如果是默认的静态表情，在每页的数组后面添加“删除”按钮
    @autoreleasepool {
        
        for (MXEmotionSetting *emotionManager in emotionManagers) {
            if (emotionManager.emotionType != MXEmotionGif) {
                NSMutableArray *array = [NSMutableArray arrayWithArray:emotionManager.emotions];
                NSInteger maxRow = emotionManager.emotionRow;
                NSInteger maxCol = emotionManager.emotionCol;
                NSInteger count = 1;
                
                while (1) {
                    
                    NSInteger index = maxRow * maxCol * count - 1;
                    if (index >= [array count]) {
                        [array addObject:@""];
                        break;
                    } else {
                        [array insertObject:@"" atIndex:index];
                    }
                    count++;
                }
                emotionManager.emotions = array;
            }
        }
        
    }

    
    
    [self loadMoreEmotionManagers:emotionManagers];
    
}

- (void)loadMoreEmotionManagers:(NSArray *)emotionManagers {
    
    for (MXEmotionSetting *emotionManager in emotionManagers) {
        
        if (emotionManager.emotionType == MXEmotionGif) {
            
            NSMutableArray *array = [NSMutableArray arrayWithArray:emotionManager.emotions];

            NSInteger totalCount = emotionManager.emotions.count;

            // 每页条数
            NSInteger pageSize = emotionManager.emotionRow * emotionManager.emotionCol;
            
            if (pageSize == EmotionForGifOnePageTotalCount) {
             
                NSInteger p1 = totalCount / pageSize;
                NSInteger p2 = totalCount % pageSize;
                NSInteger totalPage = 0;
                
                if(totalCount <= pageSize ){
                    totalPage = 1;
                }else if(p2==0){
                    totalPage = p1;
                }else if(p2!=0){
                    totalPage = p1+1;
                }
                
                // 按每页8条，共有x页，总数为8*x
                NSInteger fullCount = totalPage * EmotionForGifOnePageTotalCount;
                
                // 不够总数，则补
                if (totalCount < fullCount) {
                    
                    NSInteger leftCount = fullCount - totalCount;
                    
                    for (NSInteger i=0; i < leftCount; i++) {
                        
                        MXEmotion *moyaGif = [[MXEmotion alloc] initWithTitle:@"" emotionName:@"" emotionId:@"" emotionThumbnail:@"" emotionOriginal:@"" emotionOriginalURL:@"" emotionType:MXEmotionGif];
                        [array addObject:moyaGif];
                    }
                }
                
            }
            
            emotionManager.emotions = array;
        }
    }

    self.emotionManagers = emotionManagers;
    
    [self.faceSelectedItem reloadDataSource:emotionManagers];
    
    self.bottomScrollView.contentSize = CGSizeMake(_emotionManagers.count * (kChatFaceSelectedItemWidth+5), CGRectGetHeight(self.bottomScrollView.frame));
    
    CGRect selectedItemRect = self.faceSelectedItem.frame;
    selectedItemRect.size.width = self.bottomScrollView.contentSize.width;
    self.faceSelectedItem.frame = selectedItemRect;
    
    [self.facialPanelView loadFacialView:emotionManagers];
}

- (void)hideAddEmotionEntrance {
    
    self.addButton.hidden = YES;
    self.vLineView.hidden = YES;
    
    self.bottomScrollView.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame) - 50 * 2, CGRectGetHeight(self.bottomView.frame));
}

- (void)hideSendEmotion {
    
    self.sendButton.hidden = YES;
    self.vSendLineView.hidden = YES;
}

- (void)enabledSendButton:(BOOL)enabled {
    
    if (enabled) {
        
        self.sendButton.backgroundColor = [UIColor colorWithRed:0 green:125.0/255.0 blue:1.0 alpha:1];
        [self.sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        CGFloat bHeight = CGRectGetHeight(self.bottomView.frame);
        
        if (!self.addButton.hidden) {
            self.bottomScrollView.frame = CGRectMake(CGRectGetMaxX(self.addButton.frame), 0, CGRectGetWidth(self.frame) - bHeight * 2, CGRectGetHeight(self.bottomView.frame));
        }
        
    }else {
        
        self.sendButton.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:250.0/255.0 alpha:1];
        [self.sendButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        
        if (!self.addButton.hidden) {
            self.bottomScrollView.frame = CGRectMake(CGRectGetMaxX(self.addButton.frame), 0, CGRectGetWidth(self.frame) - CGRectGetHeight(self.bottomView.frame), CGRectGetHeight(self.bottomView.frame));
        }
    }
}

#pragma mark - Getters

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.tag = MXChatToolBarStyleTagForBottomView;
        
    }
    return _bottomView;
}

- (MXFacialPanelView*)facialPanelView {
    if (!_facialPanelView) {
        _facialPanelView = [[MXFacialPanelView alloc] initWithFrame:CGRectMake(0, 0,
                                                                               self.frame.size.width,
                                                                               MXFacialPanelViewDefaultHeight)];
        _facialPanelView.delegate = self;
    }
    return _facialPanelView;
}

- (UIView *)vLineView {
    if (!_vLineView) {
        _vLineView = [[UIView alloc] init];
        _vLineView.backgroundColor = [UIColor colorWithRed:.9f green:.9f blue:.9f alpha:1];
        
    }
    return _vLineView;
}

- (UIView *)vSendLineView {
    if (!_vSendLineView) {
        _vSendLineView = [[UIView alloc] init];
        _vSendLineView.backgroundColor = [UIColor colorWithRed:.9f green:.9f blue:.9f alpha:1];
    }
    return _vSendLineView;
}


- (UIScrollView*)bottomScrollView {
    if (!_bottomScrollView) {
        _bottomScrollView = [[UIScrollView alloc] init];
        _bottomScrollView.showsHorizontalScrollIndicator = NO;
    }
    return _bottomScrollView;
}

- (UIButton*)addButton {
    if (!_addButton) {
        
        _addButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addButton setImage:[UIImage imageNamed:MXChatToolBarBundle(@"ToolBar/Motalk_EmotionsAdd")] forState:UIControlStateNormal];
        [_addButton addTarget:self action:@selector(addButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addButton;
}


- (UIButton*)sendButton {
    if (!_sendButton) {
        
        _sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        // 默认为灰色，不可点
         _sendButton.backgroundColor = [UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:250.0/255.0 alpha:1];
        [_sendButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_sendButton setTitle:@"发送" forState:UIControlStateNormal];
        _sendButton.showsTouchWhenHighlighted = YES;
//        _sendButton.enabled = NO;
        [_sendButton addTarget:self action:@selector(sendButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendButton;
}

- (MXChatFaceSelectedItem*)faceSelectedItem {
    if (!_faceSelectedItem) {

        _faceSelectedItem=[[MXChatFaceSelectedItem alloc] init];
        _faceSelectedItem.selectedColor = self.backgroundColor;
        _faceSelectedItem.selectedButtonIndex = 0;
        
        __weak typeof(self) weakSelf = self;
        _faceSelectedItem.didSelectedCallBack=^(NSInteger index) {
            __strong typeof(self) strongkSelf = weakSelf;
            [strongkSelf.facialPanelView loadFacialViewWithPage:index];
        };
    }
    return _faceSelectedItem;
}

- (NSArray*)emotions {
    if (!_emotions) {
        _emotions = [NSArray array];
    }
    return _emotions;
}
@end
