//
//  MXEmotionItemButton.m
//  MXChatToolBarSdk
//
//  Created by aken on 16/10/31.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionItemButton.h"

@implementation MXEmotionItemButton

-(void)initCustomConfig {
    
    CGSize titleSize = self.titleLabel.frame.size;
    CGSize imageSize = self.imageView.frame.size;
    
    CGFloat titleLength=titleSize.width;
    
    
    self.imageEdgeInsets = UIEdgeInsetsMake(0,
                                            0.0,
                                            0.0,
                                            - titleSize.width - imageSize.width - 30);
    self.titleEdgeInsets = UIEdgeInsetsMake(0.0,
                                            - (imageSize.width + titleLength/2),
                                            0,
                                            0.0);
    
}


- (void)layoutSubviews
{
    
    [super layoutSubviews];
    
    [self initCustomConfig];
    
    UIImageView *imageView = [self imageView];
    CGRect imageFrame = imageView.frame;
//    imageFrame.size.width = 66;
//    imageFrame.size.height = 66;
    imageFrame.origin.x = self.frame.size.width/2 - imageFrame.size.width/2;
    
    imageView.frame = imageFrame;
//
    UILabel *titleLabel = [self titleLabel];
    CGRect titleFrame = titleLabel.frame;
    titleFrame.size.width=self.frame.size.width;
    titleFrame.origin.x = ((self.frame.size.width - titleFrame.size.width)/ 2);
    titleFrame.origin.y = CGRectGetMaxY(imageView.frame) + 2;
    titleLabel.frame = titleFrame;
    titleLabel.textAlignment = NSTextAlignmentCenter;

}

@end
