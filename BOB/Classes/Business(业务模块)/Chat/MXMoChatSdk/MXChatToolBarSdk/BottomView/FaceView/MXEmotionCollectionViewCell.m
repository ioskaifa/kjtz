//
//  MXEmotionCollectionViewCell.m
//  ChatToolBar
//
//  Created by aken on 16/4/22.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionCollectionViewCell.h"
#import "MXEmotionSetting.h"
#import "MXChatToolBarCommon.h"
#import "MXEmotionItemButton.h"

#import "FLAnimatedImage.h"

@implementation MXEmotionCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor brownColor];
        [self.contentView addSubview:self.imageButton];
        // 此处的作用就是为了点击空隙时toolbar收起来
        self.contentView.tag = MXChatToolBarViewTagForCellContentView;
    }
    return self;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    _imageButton.frame = self.bounds;
}

- (void)setEmotion:(MXEmotion *)emotion {
    _emotion = emotion;
    if ([emotion isKindOfClass:[MXEmotion class]]) {
        
        if (emotion.emotionType == MXEmotionGif) {
            
            // 分本地和远程
            NSString *urlString = emotion.emotionThumbnail;
            if ([urlString hasPrefix:@"http"] ||
                [urlString hasPrefix:@"https"]) {
                
                NSString * encodingString = [emotion.emotionOriginalURL stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
                NSURL *url = [NSURL URLWithString:encodingString];
                
                FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfURL:url]];
                [self.imageButton setImage:image.posterImage forState:UIControlStateNormal];
                
                
            }else {
                
                FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfFile:urlString]];
                [self.imageButton setImage:image.posterImage forState:UIControlStateNormal];
                [self.imageButton setTitle:emotion.emotionName forState:UIControlStateNormal];
            }
 
            
            [self changeImageButtonSize:CGSizeMake(kMXFacialPanelViewGifWidth,kMXFacialPanelViewGifWidth)];
            
        } else if (emotion.emotionType == MXEmotionPng) {
            
            // 分本地和远程
            
            [self.imageButton setImage:[UIImage imageNamed:emotion.emotionThumbnail] forState:UIControlStateNormal];
            self.imageButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
        } else {
            
            NSString *imagePath =[NSString stringWithFormat:@"Emotion/emoji/%@",emotion.emotionThumbnail];
            [self.imageButton setImage:[UIImage imageNamed:MXChatToolBarBundle(imagePath)] forState:UIControlStateNormal];
            self.imageButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.imageButton setTitle:nil forState:UIControlStateNormal];
            
            [self changeImageButtonSize:CGSizeMake(kMXFacialPanelViewEmojiWidth,kMXFacialPanelViewEmojiWidth)];
        }
        
        [self.imageButton addTarget:self action:@selector(sendEmotion:) forControlEvents:UIControlEventTouchUpInside];
        
    } else {
        
        
        [self.imageButton setTitle:nil forState:UIControlStateNormal];
        [self.imageButton setImage:[UIImage imageNamed:MXChatToolBarBundle(@"ToolBar/Motalk_toolbar_faceDelete")] forState:UIControlStateNormal];
        [self.imageButton addTarget:self action:@selector(sendEmotion:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
}

- (void)changeImageButtonSize:(CGSize)buttonSize {
    
    CGRect rect = _imageButton.frame;
    rect.origin.x = self.bounds.size.width/2 - buttonSize.width/2;
    rect.origin.y = self.bounds.size.height/2 - buttonSize.height/2;
    rect.size = buttonSize;
    _imageButton.frame = rect;
}

- (void)sendEmotion:(id)sender {
    if (_delegate) {
        if ([_emotion isKindOfClass:[MXEmotion class]]) {
            [_delegate didSendEmotion:_emotion];
        } else {
            [_delegate didSendEmotion:nil];
        }
    }
}

- (MXEmotionItemButton*)imageButton {
    if (!_imageButton) {
        _imageButton = [MXEmotionItemButton buttonWithType:UIButtonTypeCustom];
        _imageButton.frame = self.bounds;
        _imageButton.userInteractionEnabled = YES;
        _imageButton.backgroundColor = [UIColor clearColor];
        [_imageButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [_imageButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
        _imageButton.frame = CGRectMake(0, 0, kMXFacialPanelViewGifWidth, kMXFacialPanelViewGifWidth);

        _imageButton.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_imageButton setTitleColor:[UIColor colorWithRed:128.0/255.0 green:128.0/255.0 blue:128.0/255.0 alpha:1] forState:UIControlStateNormal];
//        _imageButton.adjustsImageWhenHighlighted = YES;
    }
    return _imageButton;
}

@end
