//
//  MXEmotionHighlightedView.h
//  MXChatToolBarSdk
//
//  点击表情高亮背景
//  Created by aken on 16/11/1.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MXEmotionHighlightedView : UIView

@end
