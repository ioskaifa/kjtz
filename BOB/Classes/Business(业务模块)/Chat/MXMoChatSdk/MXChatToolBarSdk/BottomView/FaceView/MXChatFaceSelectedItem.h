//
//  MXChatFaceSelectedItem.h
//  ChatToolBar
//
//  表情选中的View
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>

static const NSInteger kChatFaceSelectedItemWidth = 35;

typedef void(^ChatFaceSelectedItemCallBack)(NSInteger index);

@interface MXChatFaceSelectedItem : UIView

@property (nonatomic, strong) ChatFaceSelectedItemCallBack didSelectedCallBack;

// 选中下标
@property (nonatomic) NSInteger selectedButtonIndex;

// 选中背景色
@property (nonatomic, strong) UIColor *selectedColor;

// 选项项
@property (nonatomic, strong) UIButton *selectedItem;

// 加载数据
-(void)reloadDataSource:(NSArray *)dataSource;

@end
