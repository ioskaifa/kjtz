//
//  MXPopPreviewView.h
//  ChatToolBar
//
//  长按表情预览
//  Created by aken on 16/5/6.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat MXPopPreviewViewMarginX = 13.0f;
static const CGFloat MXPopPreviewViewMarginY = 12.0f;
static const CGFloat MXPopPreviewViewArrowSize = 12.f;

@class FLAnimatedImageView;

@protocol MXPopPreviewViewDelegate <NSObject>

@optional
- (void)tapPopPreviewEmotion:(id)emotion;

@end

@interface MXPopPreviewView : UIView

// 展示face表情的
@property (nonatomic, strong) FLAnimatedImageView *faceImageView;

// 代理
@property (nonatomic, weak) id<MXPopPreviewViewDelegate> delegate;

/*!
 @method
 @brief 初始化预览表情界面
 *@param frame frame
 */
- (instancetype)initWithPreviewFrame:(CGRect)frame;
    

/*!
 @method
 @brief 加载数据(本地或是网络)
 *@param model 数据模型
 */
- (void)reloadData:(id)model;

/*!
 @method
 @brief 显示本地表情信息
 *@param model 数据模型
 */
- (void)showData:(id)model;

/*!
 @method
 @brief 显示长按表情预览
 *@param view 显示在哪个view上
 *@param fromRect 起始位置
 */
- (void)showPreviewInView:(UIView *)view
              fromRect:(CGRect)fromRect;

/*!
 @method
 @brief 改变长按表情预览页面位置
 *@param view 显示在哪个view上
 *@param fromRect 起始位置
 */
- (void)changePreView:(UIView *)view
              fromRect:(CGRect)fromRect;

@end

extern const CGFloat MXPopPreviewViewBackgroundWidth;
extern const CGFloat MXPopPreviewViewBackgroundHeight;