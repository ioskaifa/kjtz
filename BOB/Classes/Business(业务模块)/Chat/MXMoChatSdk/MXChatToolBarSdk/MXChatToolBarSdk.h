//
//  MXChatToolBarSdk.h
//  MXChatToolBarSdk
//
//  Created by aken on 16/5/27.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

//#import <UIKit/UIKit.h>
//
//#import <MXChatToolBarSdk/FLAnimatedImage.h>
//
//#import <MXChatToolBarSdk/MXChatToolBar.h>
//#import <MXChatToolBarSdk/MXChatBarMoreView.h>
//#import <MXChatToolBarSdk/MXFaceView.h>
//#import <MXChatToolBarSdk/MXPopPreviewView.h>
//#import <MXChatToolBarSdk/MXFacialPanelView.h>
//#import <MXChatToolBarSdk/MXChatToolBarCommon.h>
//#import <MXChatToolBarSdk/MXEmotionSetting.h>
//#import <MXChatToolBarSdk/MXEmotionManager.h>
//#import <MXChatToolBarSdk/MXInputTextView.h>
//
//// delegate
//#import <MXChatToolBarSdk/IMXFaceViewDelegate.h>
//#import <MXChatToolBarSdk/IMXChatToolBarDataSource.h>
//#import <MXChatToolBarSdk/IMXChatToolBarDelegate.h>

#import "FLAnimatedImage.h"

#import "MXChatToolBar.h"
#import "MXChatBarMoreView.h"
#import "MXFaceView.h"
#import "MXPopPreviewView.h"
#import "MXFacialPanelView.h"
#import "MXChatToolBarCommon.h"
#import "MXEmotionSetting.h"
#import "MXEmotionManager.h"
#import "MXInputTextView.h"

// delegate
#import "IMXFaceViewDelegate.h"
#import "IMXChatToolBarDataSource.h"
#import "IMXChatToolBarDelegate.h"



