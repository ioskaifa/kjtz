//
//  MXChatToolbarItem.m
//  ChatToolBar
//
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXChatToolbarItem.h"

@implementation MXChatToolbarItem

- (instancetype)initWithButton:(UIButton *)button
                      withView:(UIView *)bottomView {
    
    self = [super init];
    if (self) {
        _button = button;
        _bottomView = bottomView;
    }
    
    return self;
}

@end
