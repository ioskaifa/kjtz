//
//  IMXChatToolBarDataSource.h
//  ChatToolBar
//
//  数据源代理
//  Created by aken on 16/5/17.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IMXChatToolBarDataSource <NSObject>

@optional
/*!
 @method
 @brief 获取表情列表,可以作自定义扩展
 @param viewController 当前消息视图
 @result 更多表情数据
 */
- (NSArray*)emotionFormessageViewController:(id)viewController;

/*!
 @method
 @brief 获取扩展面板列表,可以作自定义扩展
 @param viewController 当前消息视图
 @result 更多面板数据
 */
- (NSArray*)morePanelItemFormessageViewController:(id)viewController;

@end
