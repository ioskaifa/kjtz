//
//  IMXChatToolBarDelegate.h
//  ChatToolBar
//
//  聊天框代理协议集合
//  Created by aken on 16/5/17.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IMXChatToolBarDelegate <NSObject>

@required
/*!
 @method
 @brief 高度变到toHeight
 @param toHeight 变化
 */
- (void)chatToolbarDidChangeFrameToHeight:(CGFloat)toHeight;

@optional
/************************发送表情*****************************/
/*!
 @method
 @brief 发送文字消息或默认表情
 @param text 文字消息
 */
- (void)didSendText:(NSString *)text;

/*!
 @method
 @brief 发送文字消息或默认gif表情
 @param text 文字消息
 @param ext 扩展消息
 */
- (void)didSendText:(NSString *)text withExt:(NSDictionary *)ext;

/*!
 @method
 @brief 发送第三方表情，不会添加到文字输入框中
 @param faceLocalPath 选中的表情的本地路径
 */
- (void)didSendFace:(NSString *)faceLocalPath;

/*!
 @method
 @brief 点击添加按钮，弹出表情列表
 */
- (void)didAddEmotion;

/*!
 @method
 @brief 点击选择静态表情，回调给外部使用
 @param emotionStr 表情字符串
 @param isDelete   是否是删除
 */
- (void)didSelectedEmoji:(NSString *)emotionStr isDelete:(BOOL)isDelete;

/************************录制语音*****************************/

/*!
 @method
 @brief 按下录音按钮开始录音
 */
- (void)didStartRecordingVoiceAction:(NSString *)error;

/*!
 @method
 @brief 手指向上滑动取消录音
 */
- (void)didCancelRecordingVoiceAction;

/*!
 @method
 @brief 松开手指完成录音
 */
- (void)didFinishRecoingVoiceAction;

/*!
 @method
 @brief 当手指离开按钮的范围内时，主要为了通知外部的HUD
 */
- (void)didDragOutsideAction;

/*!
 @method
 @brief 当手指再次进入按钮的范围内时，主要也是为了通知外部的HUD
 */
- (void)didDragInsideAction;

/**************************更多面板***************************/

/*!
 @method
 @brief 面板显示
 @param itmes 面板items
 */
- (void)chartMoreViewDidLoad:(NSArray *)itmes;
/*!
 @method
 @brief 点击面板item
 */
- (void)didSelectMoreItemAt:(id)emotion;

/**************************自定义面板高度***************************/
/*!
 @method
 @brief 点击面板item
 */
- (CGFloat)chatToolBarBottomPancelForMoreViewHeight;

/**************************匹配表情时触发预览***************************/
/*!
 @method
 @brief 在输入框里输入文字时，匹配表情，如果匹配上，则显示
 @param emotionString 表情字符串
 */
- (void)chatToolBarShouldChangeText:(NSString *)emotionString;

/**************************输入框字符变化回调***************************/
/*!
 @method
 @brief 在输入框里文本发生变化
 */
- (BOOL)chatToolBarShouldDelTextInRange:(NSRange)range replacementText:(NSString *)text;

@end
