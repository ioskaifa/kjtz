//
//  MXChatToolbarItem.h
//  ChatToolBar
//
//  聊天工具栏项
//  Created by aken on 16/4/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MXChatToolbarItem : NSObject

/*!
 @property
 @brief ToolBar左右项
 */
@property (nonatomic, strong, readonly) UIButton *button;

/*!
 @property
 @brief 点击按钮之后在toolbar下方延伸出的页面
 */
@property (nonatomic, strong) UIView *bottomView;

/*!
 @method
 @brief 根据不同的枚举创建不同样式的bar
 @param button     ToolBar左或右按钮项
 @param bottomView 底部view
 */
- (instancetype)initWithButton:(UIButton *)button
                      withView:(UIView *)bottomView;

@end
