//
//  MXChatToolBar.m
//  ChatToolBar
//
//  Created by aken on 16/4/18.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXChatToolBar.h"
#import "MXChatToolBarCommon.h"
#import "MXChatToolbarItem.h"
#import "MXEmotionManager.h"
#import "MXEmotionSetting.h"
#import "MXInputTextView.h"

#import <AVFoundation/AVFoundation.h>

// 默认英文键盘高度258 中文283.5
NSInteger const MXToolBarPanelDefaultHeight = 260;

static CGFloat const kMXFaceViewAnimateWithDuration = 0.35f;
//
static CGFloat const kMXFaceViewInputextHeight = 47;

#define kInputTextViewMinHeight 47
#define kInputTextViewMaxHeight 89
#define kHorizontalPadding 8
#define kVerticalPadding 5

@interface MXChatToolBar () <UITextViewDelegate, IMXFaceViewDelegate,
MXChatBarMoreViewDelegate, YYTextViewDelegate> {
    
    CGFloat _previousTextViewContentHeight;
}

@property(nonatomic, strong) NSMutableArray *leftItems;
@property(nonatomic, strong) NSMutableArray *rightItems;

// 底部扩展页面
@property(nonatomic) BOOL isShowButtomView;

@property(nonatomic, strong) UIView *topLineView;
@property(nonatomic, strong) UIView *bottomLineView;

// 按钮、toolbarView
@property(nonatomic, strong) UIView *toolbarView;
@property(nonatomic, strong) UIButton *changeRecordButton; // 来用切换录制按钮
@property(nonatomic, strong) UIButton *recordButton;
@property(nonatomic, strong) UIButton *moreButton;
@property(nonatomic, strong) UIButton *faceButton;

// 父类view的高度
@property(nonatomic) CGFloat superViewHeight;
@property(nonatomic, assign) CGRect keyboardFrame;

// 当前Bar的style
@property(nonatomic) MXChatToolBarStyle toolBarStyle;

@property(nonatomic, readwrite) BOOL isPop;

// 用于判断当前Toolbar是否处于可见/或被销毁
//@property(nonatomic) BOOL isToolbarActive;

@end

@implementation MXChatToolBar

- (id)initWithFrame:(CGRect)frame {
    
    return [self initWithFrame:frame style:MXChatToolBarStyleDefault];
}

- (id)initWithFrame:(CGRect)frame style:(MXChatToolBarStyle)barStyle {
    
    if (frame.size.height < (kVerticalPadding * 2 + kInputTextViewMinHeight)) {
        frame.size.height = kVerticalPadding * 2 + kInputTextViewMinHeight;
    }
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.isPop = NO;
        self.toolBarStyle = barStyle;
        // 检验barStyle不能越出自定义的范围
        if (barStyle < MXChatToolBarStyleDefault ||
            barStyle > MXChatToolBarStyleOnlyFaceEmotion) {
            
            self.toolBarStyle = MXChatToolBarStyleDefault;
        }
        
        _leftItems = [NSMutableArray array];
        _rightItems = [NSMutableArray array];
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(chatKeyboardWillChangeFrame:)
         name:UIKeyboardWillChangeFrameNotification
         object:nil];
        
        self.superViewHeight = [UIScreen mainScreen].bounds.size.height -([self viewController].navigationController.navigationBar.isTranslucent
                                                                          ? 0
                                                                          : 64);
        //        self.superViewHeight = [UIScreen mainScreen].bounds.size.height;
        
        [self setupSubviews:self.toolBarStyle];
    }
    
    return self;
}

- (void)enableKeyboardNotification:(BOOL)enable {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if(enable) {
        [[NSNotificationCenter defaultCenter]
        addObserver:self
        selector:@selector(chatKeyboardWillChangeFrame:)
        name:UIKeyboardWillChangeFrameNotification
        object:nil];
    }
}


- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIKeyboardWillChangeFrameNotification
     object:nil];
    
    _delegate = nil;
    _inputTextView.delegate = nil;
    _inputTextView = nil;
    
    //
    [_leftItems removeAllObjects];
    _leftItems = nil;
    
    [_rightItems removeAllObjects];
    _rightItems = nil;
    
    if (_activityButtomView) {
        [_activityButtomView removeFromSuperview];
        _activityButtomView = nil;
    }
    
    [[MXEmotionManager manager] clearAllEmotions];
}

- (void)setupSubviews:(MXChatToolBarStyle)barStyle {
    
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.toolbarView];
    
    if (barStyle == MXChatToolBarStyleDefault) {
        
        [self.toolbarView addSubview:self.changeRecordButton];
      
        [self.toolbarView addSubview:self.faceButton];
        [self.toolbarView addSubview:self.moreButton];
        [self.toolbarView addSubview:self.inputTextView];
          [self.toolbarView addSubview:self.recordButton];
        [self updateUIFrame];
        // item
        MXChatToolbarItem *styleItem =
        [[MXChatToolbarItem alloc] initWithButton:self.changeRecordButton
                                         withView:nil];
        [self setInputViewLeftItems:@[ styleItem ]];
        
        MXChatToolbarItem *faceItem =
        [[MXChatToolbarItem alloc] initWithButton:self.faceButton
                                         withView:self.faceView];
        MXChatToolbarItem *moreItem =
        [[MXChatToolbarItem alloc] initWithButton:self.moreButton
                                         withView:self.moreView];
        
        [self setInputViewRightItems:@[ faceItem, moreItem ]];
        
    } else if (barStyle == MXChatToolBarStyleSimple) {
        
        [self.toolbarView addSubview:self.inputTextView];
        
    } else if (barStyle == MXChatToolBarStyleStaticEmotion) {
        
        [self.toolbarView addSubview:self.faceButton];
        [self.toolbarView addSubview:self.inputTextView];
        
        [self updateUIFrame];
        
        MXChatToolbarItem *faceItem =
        [[MXChatToolbarItem alloc] initWithButton:self.faceButton
                                         withView:self.faceView];
        [self setInputViewRightItems:@[ faceItem ]];
        [(MXFaceView *)self.faceView hideAddEmotionEntrance];
        
    }else if (barStyle == MXChatToolBarStyleOnlyFaceEmotion) {
        
        [self.toolbarView addSubview:self.faceButton];
        
        [self updateUIFrame];
        
        MXChatToolbarItem *faceItem =[[MXChatToolbarItem alloc] initWithButton:self.faceButton withView:self.faceView];
        [self setInputViewLeftItems:@[ faceItem ]];
    }
    
    [self.toolbarView addSubview:self.topLineView];
    [self.toolbarView addSubview:self.bottomLineView];
    
}

- (void)updateUIFrame {
    
    self.maxTextInputViewHeight = kInputTextViewMaxHeight;
    
    self.topLineView.frame = CGRectMake(0, 0, self.bounds.size.width, 0.5);
    self.bottomLineView.frame = CGRectMake(0, CGRectGetHeight(self.frame) - 1,
                                           self.bounds.size.width, 0.5);
    
    CGRect changeRect = self.changeRecordButton.frame;
    changeRect.size = CGSizeMake(21, 21);
    self.changeRecordButton.frame = changeRect;
    
    CGRect faceRect = self.faceButton.frame;
    faceRect.size = CGSizeMake(25, 25);
    self.faceButton.frame = faceRect;
    
    CGRect moreRect = self.moreButton.frame;
    moreRect.size = CGSizeMake(25, 25);
    self.moreButton.frame = moreRect;
    
    _previousTextViewContentHeight =[self getTextViewContentH:self.inputTextView];
}

#pragma mark - Public

+ (CGFloat)defaultHeight {
    
    return kMXFaceViewInputextHeight + 10;
}

- (void)setMaxTextInputViewHeight:(CGFloat)maxTextInputViewHeight {
    if (maxTextInputViewHeight > kInputTextViewMaxHeight) {
        maxTextInputViewHeight = kInputTextViewMaxHeight;
    }
    _maxTextInputViewHeight = maxTextInputViewHeight;
}

// 停止编辑
- (BOOL)endEditing:(BOOL)force {
    
    BOOL result = [super endEditing:force];
    
    for (MXChatToolbarItem *item in self.rightItems) {
        item.button.selected = NO;
    }
    
    self.isPop = NO;
    // 还原到初始位置
    if (_activityButtomView) {
        [UIView animateWithDuration:kMXFaceViewAnimateWithDuration
                         animations:^{
                             [self.activityButtomView
                              setFrame:CGRectMake(0, self.superViewHeight,
                                                  self.frame.size.width,
                                                  MXToolBarPanelDefaultHeight)];
                             [self setCureenFrame:0];
                         }
                         completion:^(BOOL finished) {
                             [self.activityButtomView removeFromSuperview];
                             self.activityButtomView = nil;
                         }];
    } else {
        if (force) {
            [self setCureenFrame:0];
        }
    }
    
    return result;
}

- (void)reloadData {
    
    if (self.toolBarStyle == MXChatToolBarStyleDefault) {
        
        __weak __typeof(self) weakSelf = self;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            __strong __typeof(self) strongSelf = weakSelf;
            // 加载表情
            if ([strongSelf.dataSource
                 respondsToSelector:@selector(emotionFormessageViewController:)]) {
                
                NSArray *emotionManagers =
                [strongSelf.dataSource emotionFormessageViewController:strongSelf];
                
                if (emotionManagers && emotionManagers.count > 0) {
                    
                    [(MXFaceView *)strongSelf.faceView
                     loadMoreEmotionManagers:emotionManagers];
                }
            }
            
            // 加载更多面板
            if ([strongSelf.dataSource
                 respondsToSelector:@selector(
                                              morePanelItemFormessageViewController:)]) {
                     
                     NSArray *emotionManagers =
                     [self.dataSource morePanelItemFormessageViewController:strongSelf];
                     [(MXChatBarMoreView *)strongSelf.moreView
                      loadMorePanelManagers:emotionManagers];
                 }
            
        });
    }
}

- (void)resetRecordState {
    
    [self.recordButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    [self.recordButton cancelTrackingWithEvent:UIEventTypeTouches];
}



#pragma mark - 事件
// 录音
- (void)recordSwitchClick:(UIButton *)sender {
    
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    
    if (button.selected) {
        for (MXChatToolbarItem *item in self.rightItems) {
            item.button.selected = NO;
        }
        
        for (MXChatToolbarItem *item in self.leftItems) {
            if (item.button != button) {
                item.button.selected = NO;
            }
        }
        
        //录音状态下，不显示底部扩展页面
        [self endEditing:YES];
        
        //将inputTextView内容置空，以使toolbarView回到最小高度
        self.inputTextView.text = @"";
        [self textViewDidChange:self.inputTextView];
        [self.inputTextView resignFirstResponder];
    } else {
        //键盘也算一种底部扩展页面
        [self.inputTextView becomeFirstResponder];
    }
    
    [UIView animateWithDuration:kMXFaceViewAnimateWithDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.recordButton.hidden = !button.selected;
                         self.inputTextView.hidden = button.selected;
                     }
                     completion:nil];
}

// 表情
- (void)faceButtonClick:(UIButton *)sender {
    
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    
    self.isShowButtomView = button.selected;
    
    
    MXChatToolbarItem *faceItem = nil;
    
    if (self.toolBarStyle < MXChatToolBarStyleOnlyFaceEmotion) {
        
        for (MXChatToolbarItem *item in self.rightItems) {
            if (item.button == button) {
                faceItem = item;
                continue;
            }
            
            item.button.selected = NO;
        }
        
        for (MXChatToolbarItem *item in self.leftItems) {
            item.button.selected = NO;
        }
        
        if (button.selected) {
            //如果处于文字输入状态，使文字输入框失去焦点
            [self.inputTextView resignFirstResponder];
            
            self.recordButton.hidden = button.selected;
            self.inputTextView.hidden = !button.selected;
        } else {
            
            [self.inputTextView becomeFirstResponder];
        }
        
    }else  {
        
        for (MXChatToolbarItem *item in self.leftItems) {
            if (item.button == button) {
                faceItem = item;
                continue;
            }
            
            item.button.selected = NO;
        }
        
        if (button.selected) {
            
            [self.exTextView resignFirstResponder];
            
        } else {
            
            [self.exTextView becomeFirstResponder];
        }
        
    }
    
    
    [self willShowBottomView:faceItem.bottomView show:button.selected];
}

// 更多面板
- (void)showMoreButtonClick:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    
    self.isShowButtomView = button.selected;
    
    MXChatToolbarItem *moreItem = nil;
    for (MXChatToolbarItem *item in self.rightItems) {
        if (item.button == button) {
            moreItem = item;
            continue;
        }
        
        item.button.selected = NO;
    }
    
    for (MXChatToolbarItem *item in self.leftItems) {
        item.button.selected = NO;
    }
    
    if (button.selected) {
        //如果处于文字输入状态，使文字输入框失去焦点
        [self.inputTextView resignFirstResponder];
        
        [UIView animateWithDuration:kMXFaceViewAnimateWithDuration
                              delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.recordButton.hidden = button.selected;
                             self.inputTextView.hidden = !button.selected;
                         }
                         completion:nil];
    } else {
        [self.inputTextView becomeFirstResponder];
    }
    
//    if (self.delegate &&
//        [self.delegate
//         respondsToSelector:@selector(
//                                      chatToolBarBottomPancelForMoreViewHeight)]) {
//             CGFloat height = [self.delegate chatToolBarBottomPancelForMoreViewHeight];
//             CGRect bottomRect = moreItem.bottomView.frame;
//             bottomRect.size.height = height;
//             moreItem.bottomView.frame = bottomRect;
//         }
    
    [self willShowBottomView:moreItem.bottomView show:button.selected];
  
}

- (void)recordButtonTouchDown {
    @weakify(self)
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    if ([audioSession respondsToSelector:@selector(requestRecordPermission:)]) {
        [audioSession performSelector:@selector(requestRecordPermission:)
                           withObject:^(BOOL granted) {
            @strongify(self)
            Block_Exec_Main_Async_Safe(^{
                if (granted) {
                    if (self.recordButton.state == UIControlStateNormal) {
                        return;
                    }
                    if (self.delegate &&
                        [self.delegate respondsToSelector:@selector(didStartRecordingVoiceAction:)]) {
                            [self.delegate didStartRecordingVoiceAction:nil];
                        }
                } else {
                    if (self.delegate &&
                        [self.delegate respondsToSelector:@selector(didStartRecordingVoiceAction:)]) {
                            [self.delegate didStartRecordingVoiceAction:
                             @"App需要访问您的麦克风。\n"
                             @"请启用麦克风-设置/隐私/"
                             @"麦克风"];
                        }
                }
            });
         }];
    }
}

- (void)recordButtonTouchUpOutside {
    
    if (self.delegate &&
        [self.delegate
         respondsToSelector:@selector(didCancelRecordingVoiceAction)]) {
            [self.delegate didCancelRecordingVoiceAction];
        }
}

- (void)recordButtonTouchUpInside {
    
    if ([self.delegate
         respondsToSelector:@selector(didFinishRecoingVoiceAction)]) {
        [self.delegate didFinishRecoingVoiceAction];
    }
}

- (void)recordDragOutside {
    
    if ([self.delegate respondsToSelector:@selector(didDragOutsideAction)]) {
        [self.delegate didDragOutsideAction];
    }
}

- (void)recordDragInside {
    
    if ([self.delegate respondsToSelector:@selector(didDragInsideAction)]) {
        [self.delegate didDragInsideAction];
    }
}

#pragma mark - UITextViewDelegate for 文本框

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    for (MXChatToolbarItem *item in self.leftItems) {
        item.button.selected = NO;
    }
    
    for (MXChatToolbarItem *item in self.rightItems) {
        item.button.selected = NO;
    }
    self.isPop = YES;
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    [textView becomeFirstResponder];
    self.isShowButtomView = NO;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        [(MXFaceView *)(self.faceView) enabledSendButton:NO];
        
        if ([self.delegate respondsToSelector:@selector(didSendText:)]) {
            
            [self.delegate didSendText:textView.text];
            self.inputTextView.text = @"";
            [self willShowInputTextViewToHeight:[self getTextViewContentH:self.inputTextView]];
        }
        
        return NO;
        
    } else {
        
        // 删除表情时，重新赋值
        if ([text isEqualToString:@""]) {
            if (textView.text.length == (range.location + 1)) {
                //删除表情
                NSString *tmpEmotionStr = [[MXEmotionManager manager] deleteEmotionWithInputText:textView.text];
                NSString *currentText = [textView.text substringToIndex:range.location];
                if (![tmpEmotionStr isEqualToString:currentText]) {
                    self.inputTextView.text = tmpEmotionStr;
                    [self textViewDidChange:self.inputTextView];
                    return NO;
                }
            }
        }
    }
    //删除@相关
    if (self.delegate &&[self.delegate respondsToSelector:@selector(chatToolBarShouldDelTextInRange:replacementText:)]) {
        return [self.delegate chatToolBarShouldDelTextInRange:range replacementText:text];
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    
    if (textView.scrollEnabled) {
        [textView scrollRangeToVisible:NSMakeRange(textView.text.length - 2, 1)];
    }
    
    if (textView.text.length > 0) {
        
        [(MXFaceView *)(self.faceView) enabledSendButton:YES];
        
    } else {
        [(MXFaceView *)(self.faceView) enabledSendButton:NO];
    }
    
    

    
    [self willShowInputTextViewToHeight:[self getTextViewContentH:textView]];
    
    // 输出每个文字发生变化
    if (self.delegate &&[self.delegate respondsToSelector:@selector(chatToolBarShouldChangeText:)]) {
        
        [self.delegate chatToolBarShouldChangeText:textView.text];
    }
}

#pragma mark - MXFaceViewDelegate for 发送表情

// 静态表情
- (void)didSelectedFacialView:(NSString *)emotionStr isDelete:(BOOL)isDelete {
    
    if (self.toolBarStyle == MXChatToolBarStyleOnlyFaceEmotion) {
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedEmoji:isDelete:)]) {
            [self.delegate didSelectedEmoji:emotionStr isDelete:isDelete];
        }
        
    }else {
        
        NSString *chatText = self.inputTextView.text;
        
        // 点击静态表情
        if (!isDelete && emotionStr.length > 0) {
            
            self.inputTextView.text = [NSString stringWithFormat:@"%@%@", chatText, emotionStr];
            
        } else { // 静态表情上面的删除按钮
            
            NSString *tmpEmotionStr = [[MXEmotionManager manager] deleteEmotionWithInputText:chatText];
            self.inputTextView.text = tmpEmotionStr;
        }
        
        [self textViewDidChange:self.inputTextView];
    }
}

// Gif动态表情
- (void)didSendFaceWithEmotion:(MXEmotion *)emotion {
    
    if (emotion) {
        
        if ([self.delegate respondsToSelector:@selector(didSendText:withExt:)]) {
            
            [self.delegate didSendText:emotion.emotionId
                               withExt:@{MX_EMOTION_DEFAULT_EXT : emotion}];
        }
    }
}

// 表情面板上的发送按钮
- (void)didSendFace {
    
    NSString *chatText = self.inputTextView.text;
    if (chatText.length > 0) {
        
        [(MXFaceView *)(self.faceView) enabledSendButton:NO];
        
        if ([self.delegate respondsToSelector:@selector(didSendText:)]) {
            
            if (![_inputTextView.text isEqualToString:@""]) {
                
                [self.delegate didSendText:chatText];
                self.inputTextView.text = @"";
                [self willShowInputTextViewToHeight:
                 [self getTextViewContentH:self.inputTextView]];
            }
        }
    }
}

// 添加表情
- (void)didAddEmotion {
    if ([self.delegate respondsToSelector:@selector(didAddEmotion)]) {
        [self.delegate didAddEmotion];
    }
}

#pragma mark - MXChatBarMoreViewDelegate for 更多面板
- (void)didSelectMoreItemAt:(MXEmotion *)emotion {
    
    if ([self.delegate respondsToSelector:@selector(didSelectMoreItemAt:)]) {
        [self.delegate didSelectMoreItemAt:emotion];
    }
}

#pragma mark - UIKeyboardNotification

//获取当前屏幕显示的viewcontroller
- (UIViewController *)getCurrentVC
{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
    
    return currentVC;
}

- (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
    UIViewController *currentVC;
    
    if ([rootVC presentedViewController]) {
        // 视图是被presented出来的
        
        rootVC = [rootVC presentedViewController];
    }
    
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
        
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
        
    } else {
        // 根视图为非导航类
        
        currentVC = rootVC;
    }
    
    return currentVC;
}

- (void)chatKeyboardWillChangeFrame:(NSNotification *)notification {
    
//     if (self.isToolbarActive) {
        UIViewController* vc = [self getCurrentVC];
    if (![vc isKindOfClass:NSClassFromString(@"ChatViewVC")]) {
        return;
    }
        self.keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        
        NSDictionary *userInfo = notification.userInfo;
        CGRect endFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        CGRect beginFrame = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        endFrame.size.height-=SafeAreaBottomHeight;
        if (beginFrame.origin.y == [[UIScreen mainScreen] bounds].size.height) {
            
            [self setCureenFrame:endFrame.size.height];
            
            
            if (self.activityButtomView) {
                
                [UIView animateWithDuration:kMXFaceViewAnimateWithDuration
                                 animations:^{
                                     //self.superViewHeight
                                     [self.activityButtomView
                                      setFrame:CGRectMake(
                                                          0, CGRectGetMaxY(self.inputTextView.frame)+CGRectGetMinY(self.frame), self.frame.size.width,
                                                          self.activityButtomView.frame.size.height)];
                                 }
                                 completion:^(BOOL finished) {
                                     [self.activityButtomView removeFromSuperview];
                                     self.activityButtomView = nil;
                                 }];
            }
            
            if (self.exTextView) {
                for (MXChatToolbarItem *item in self.leftItems) {
                    item.button.selected = NO;
                }
                
                
            }
            
            
        } else if (endFrame.origin.y == [[UIScreen mainScreen] bounds].size.height) {
            
            if (!self.exTextView) {
                // 键盘收起来
                [self setCureenFrame:endFrame.size.height];
                if (!self.isShowButtomView) {
                    // 键盘失去焦点，可能是点击发表情或是点击更多面板
                    if ([self.inputTextView resignFirstResponder]) {
                        
                        [self endEditing:YES];
                    }
                    
                }
            }
            
        } else {
            
            [self setCureenFrame:endFrame.size.height];
        }
//    }
}

#pragma mark - Private input view for 根据文字行数来改变textview高度

- (CGFloat)getTextViewContentH:(YYTextView *)textView {
    if ([StringUtil isEmpty:textView.text]) {
        return kInputTextViewMinHeight;
    }
    int height = textView.textLayout.textBoundingSize.height + textView.textContainerInset.top + textView.textContainerInset.bottom;
    return height;
}

- (void)willShowInputTextViewToHeight:(CGFloat)toHeight {
    
    if (toHeight < kInputTextViewMinHeight) {
        toHeight = kInputTextViewMinHeight;
    }
    if (toHeight > self.maxTextInputViewHeight) {
        toHeight = self.maxTextInputViewHeight;
    }
    
    if (toHeight == _previousTextViewContentHeight) {
        //        self.inputTextView.contentOffset =
        //        CGPointMake(0, self.inputTextView.contentSize.height -
        //                    self.inputTextView.frame.size.height);
        [self.inputTextView
         scrollRangeToVisible:NSMakeRange([self.inputTextView.text length], 1)];
        return;
        
    } else {
        
        CGFloat changeHeight = toHeight - _previousTextViewContentHeight;
        
        CGRect rect = self.frame;
        rect.size.height += changeHeight;
        rect.origin.y -= changeHeight;
        self.frame = rect;
        
        rect = self.toolbarView.frame;
        rect.size.height += changeHeight;
        self.toolbarView.frame = rect;
        
        self.bottomLineView.frame =
        CGRectMake(0, rect.size.height - 1, self.bounds.size.width, 0.5);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
            [self.inputTextView setContentOffset:CGPointMake(0.0f,
                                                             (self.inputTextView.contentSize.height -
                                                              self.inputTextView.frame.size.height) /
                                                             2)
                                        animated:YES];
        }
        _previousTextViewContentHeight = toHeight;
        
        if (_delegate &&[_delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
            
            [_delegate chatToolbarDidChangeFrameToHeight:self.frame.size.height];
        }
    }
}

#pragma mark - Private bottom view

// 重新设置toolbar
- (void)setCureenFrame:(CGFloat)bottomHeight {
    
    CGRect fromFrame = self.frame;
    CGFloat toHeight = self.toolbarView.frame.size.height + bottomHeight;
    CGRect toFrame = CGRectMake(fromFrame.origin.x,
                                fromFrame.origin.y + (fromFrame.size.height - toHeight),
                                fromFrame.size.width, toHeight);
    if (!CGRectEqualToRect(self.frame, toFrame)) {
        self.frame = toFrame;
    }
    if (_delegate &&[_delegate respondsToSelector:@selector(chatToolbarDidChangeFrameToHeight:)]) {
        [_delegate chatToolbarDidChangeFrameToHeight:toHeight];
    }
}

- (void)willShowBottomView:(UIView *)bottomView show:(BOOL)show {
    
    if (![self.activityButtomView isEqual:bottomView]) {
        
        if (self.activityButtomView) {
            [self.activityButtomView removeFromSuperview];
        }
        self.activityButtomView = bottomView;
    }
    
    CGFloat bottomHeight = bottomView ? bottomView.frame.size.height : 0;
    if (show) {
        
        [self setCureenFrame:bottomHeight];
        
        if (bottomView) {
            
            self.isPop = YES;
            [self.superview addSubview:bottomView];
            
            __block CGFloat bottomViewY =
            CGRectGetMinY(self.frame) + CGRectGetHeight(self.toolbarView.frame);//[self.class defaultHeight];
            
            [UIView animateWithDuration:kMXFaceViewAnimateWithDuration
                             animations:^{
                                 [bottomView setFrame:CGRectMake(0, bottomViewY,
                                                                 self.frame.size.width,
                                                                 bottomHeight)];
                             }
                             completion:nil];
        }
        
    } else {
        
        if (bottomView) {
            [UIView animateWithDuration:kMXFaceViewAnimateWithDuration
                             animations:^{
                                 [bottomView
                                  setFrame:CGRectMake(0, self.superViewHeight,
                                                      self.frame.size.width, bottomHeight)];
                             }
                             completion:^(BOOL finished) {
                                 [bottomView removeFromSuperview];
                             }];
        }
    }
}

#pragma mark - Getters

- (UIView *)toolbarView {
    if (!_toolbarView) {
        _toolbarView = [[UIView alloc] initWithFrame:self.bounds];
        _toolbarView.backgroundColor = [UIColor colorWithHexString:@"#F6F7F6"];
        _toolbarView.tag = MXChatToolBarStyleTagForBarBackground;
    }
    return _toolbarView;
}

- (UIView *)topLineView {
    if (!_topLineView) {
        _topLineView = [[UIView alloc] init];
        _topLineView.backgroundColor =
        [UIColor colorWithRed:.9f green:.9f blue:.9f alpha:1];
    }
    return _topLineView;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor =
        [UIColor colorWithRed:.9f green:.9f blue:.9f alpha:1];
    }
    return _bottomLineView;
}

- (UIButton *)changeRecordButton {
    if (!_changeRecordButton) {
        _changeRecordButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _changeRecordButton.showsTouchWhenHighlighted = YES;
        
        [_changeRecordButton
         setImage:[UIImage
                   imageNamed:MXChatToolBarBundle(
                                                  @"ToolBar/Motalk_message_bottom_voice")]
         forState:UIControlStateNormal];
        [_changeRecordButton
         setImage:[UIImage
                   imageNamed:MXChatToolBarBundle(
                                                  @"ToolBar/Motalk_message_bottom_keyboard")]
         forState:UIControlStateSelected];
        _changeRecordButton.selected = NO;
        [_changeRecordButton addTarget:self
                                action:@selector(recordSwitchClick:)
                      forControlEvents:UIControlEventTouchUpInside];
    }
    return _changeRecordButton;
}

- (UIButton *)faceButton {
    if (!_faceButton) {
        
        _faceButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [_faceButton
         setImage:[UIImage
                   imageNamed:MXChatToolBarBundle(
                                                  @"ToolBar/Motalk_message_bottom_emotion")]
         forState:UIControlStateNormal];
        [_faceButton
         setImage:[UIImage
                   imageNamed:MXChatToolBarBundle(
                                                  @"ToolBar/Motalk_message_bottom_keyboard")]
         forState:UIControlStateSelected];
        [_faceButton addTarget:self
                        action:@selector(faceButtonClick:)
              forControlEvents:UIControlEventTouchUpInside];
        _faceButton.showsTouchWhenHighlighted = YES;
        _faceButton.selected = NO;
    }
    return _faceButton;
}

- (UIButton *)recordButton {
    if (!_recordButton) {
        
        _recordButton = [[UIButton alloc] initWithFrame:self.inputTextView.frame];
        _recordButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
        [_recordButton setTitleColor:[UIColor darkGrayColor]
                            forState:UIControlStateNormal];
        
        [_recordButton
         setBackgroundImage:
         [[UIImage imageNamed:MXChatToolBarBundle(
                                                  @"ToolBar/Motalk_message_bottom_record")]
          stretchableImageWithLeftCapWidth:10
          topCapHeight:10]
         forState:UIControlStateNormal];
        [_recordButton
         setBackgroundImage:
         [[UIImage
           imageNamed:MXChatToolBarBundle(
                                          @"ToolBar/Motalk_message_bottom_record_hl")]
          stretchableImageWithLeftCapWidth:10
          topCapHeight:10]
         forState:UIControlStateHighlighted];
        [_recordButton setTitle:@"按住  说话" forState:UIControlStateNormal];
        [_recordButton setTitle:@"松开  结束" forState:UIControlStateHighlighted];
        _recordButton.hidden = YES;
        
        [_recordButton addTarget:self
                          action:@selector(recordButtonTouchDown)
                forControlEvents:UIControlEventTouchDown];
        [_recordButton addTarget:self
                          action:@selector(recordButtonTouchUpOutside)
                forControlEvents:UIControlEventTouchUpOutside];
        [_recordButton addTarget:self
                          action:@selector(recordButtonTouchUpInside)
                forControlEvents:UIControlEventTouchUpInside];
        [_recordButton addTarget:self
                          action:@selector(recordDragOutside)
                forControlEvents:UIControlEventTouchDragExit];
        [_recordButton addTarget:self
                          action:@selector(recordDragInside)
                forControlEvents:UIControlEventTouchDragEnter];
        _recordButton.hidden = YES;
    }
    return _recordButton;
}

- (UIButton *)moreButton {
    if (!_moreButton) {
        
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _moreButton.showsTouchWhenHighlighted = YES;
        [_moreButton
         setImage:[UIImage imageNamed:MXChatToolBarBundle(
                                                          @"ToolBar/Motalk_message_bottom_plus")]
         forState:UIControlStateNormal];
        [_moreButton addTarget:self
                        action:@selector(showMoreButtonClick:)
              forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreButton;
}

- (MXInputTextView *)inputTextView {
    if (!_inputTextView) {
        _inputTextView = [[MXInputTextView alloc]
                          initWithFrame:CGRectMake(8, 7.5, self.frame.size.width - 16, 34)];
        _inputTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _inputTextView.scrollEnabled = YES;
        _inputTextView.returnKeyType = UIReturnKeySend;
        // UITextView内部判断send按钮是否可以用
        _inputTextView.enablesReturnKeyAutomatically = YES;
        _inputTextView.delegate = self;
        _inputTextView.backgroundColor = [UIColor whiteColor];
        _inputTextView.layer.cornerRadius = 6.0f;
    }
    return _inputTextView;
}

- (UIView *)faceView {
    if (!_faceView) {
        _faceView = [[MXFaceView alloc]
                     initWithFrame:CGRectMake(0, self.superViewHeight, self.frame.size.width,
                                              MXToolBarPanelDefaultHeight)];
        [(MXFaceView *)_faceView setDelegate:self];
        _faceView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    }
    
    return _faceView;
}

- (UIView *)moreView {
    if (!_moreView) {
        _moreView = [[MXChatBarMoreView alloc]
                     initWithFrame:CGRectMake(0, self.superViewHeight, self.frame.size.width,
                                              MXToolBarPanelDefaultHeight)];
        _moreView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [(MXChatBarMoreView *)_moreView setDelegate:self];
    }
    
    return _moreView;
}

- (CGFloat)bottomHeight {
    
    if (self.faceView.superview || self.moreView.superview) {
        return MAX(
                   self.keyboardFrame.size.height,
                   MAX(self.faceView.frame.size.height, self.moreView.frame.size.height));
    } else {
        return MAX(self.keyboardFrame.size.height, CGFLOAT_MIN);
    }
}

- (UIViewController *)viewController {
    UIResponder *responder = self;
    while ((responder = [responder nextResponder]))
        if ([responder isKindOfClass:[UIViewController class]])
            return (UIViewController *)responder;
    return nil;
}

#pragma mark - Setters

- (void)setSuperViewHeight:(CGFloat)superViewHeight {
    _superViewHeight = superViewHeight;
}

- (void)setInputViewLeftItems:(NSArray *)inputViewLeftItems {
    
    for (MXChatToolbarItem *item in self.leftItems) {
        [item.button removeFromSuperview];
        [item.bottomView removeFromSuperview];
    }
    [self.leftItems removeAllObjects];
    
    CGFloat oX = 8;
    CGFloat itemHeight = self.toolbarView.frame.size.height - 10;
    for (id item in inputViewLeftItems) {
        if ([item isKindOfClass:[MXChatToolbarItem class]]) {
            MXChatToolbarItem *chatItem = (MXChatToolbarItem *)item;
            if (chatItem.button) {
                CGRect itemFrame = chatItem.button.frame;
                if (itemFrame.size.height == 0) {
                    itemFrame.size.height = itemHeight;
                }
                
                if (itemFrame.size.width == 0) {
                    itemFrame.size.width = itemFrame.size.height;
                }
                
                itemFrame.origin.x = oX;
                itemFrame.origin.y =
                (self.toolbarView.frame.size.height - itemFrame.size.height) / 2;
                chatItem.button.frame = itemFrame;
                oX += (itemFrame.size.width + 8);
                
                [self.toolbarView addSubview:chatItem.button];
                [self.leftItems addObject:chatItem];
            }
        }
    }
    
    CGRect inputFrame = self.inputTextView.frame;
    CGFloat value = inputFrame.origin.x - oX;
    inputFrame.origin.x = oX;
    inputFrame.size.width += value;
    inputFrame.size.width += value;
    self.inputTextView.frame = inputFrame;
    
    CGRect recordFrame = self.recordButton.frame;
    recordFrame.origin.x = inputFrame.origin.x;
    recordFrame.size.width = inputFrame.size.width;
    self.recordButton.frame = recordFrame;
}

- (void)setInputViewRightItems:(NSArray *)inputViewRightItems {
    
    for (MXChatToolbarItem *item in self.rightItems) {
        [item.button removeFromSuperview];
        [item.bottomView removeFromSuperview];
    }
    [self.rightItems removeAllObjects];
    
    CGFloat oMaxX = self.toolbarView.frame.size.width - 8;
    CGFloat itemHeight = self.toolbarView.frame.size.height;
    if ([inputViewRightItems count] > 0) {
        for (NSInteger i = (inputViewRightItems.count - 1); i >= 0; i--) {
            id item = [inputViewRightItems objectAtIndex:i];
            if ([item isKindOfClass:[MXChatToolbarItem class]]) {
                MXChatToolbarItem *chatItem = (MXChatToolbarItem *)item;
                if (chatItem.button) {
                    CGRect itemFrame = chatItem.button.frame;
                    if (itemFrame.size.height == 0) {
                        itemFrame.size.height = itemHeight;
                    }
                    
                    if (itemFrame.size.width == 0) {
                        itemFrame.size.width = itemFrame.size.height;
                    }
                    
                    oMaxX -= itemFrame.size.width;
                    itemFrame.origin.x = oMaxX;
                    itemFrame.origin.y = (self.toolbarView.frame.size.height - itemFrame.size.height) / 2;
                    chatItem.button.frame = itemFrame;
                    
                    oMaxX -= 8;
                    
                    [self.toolbarView addSubview:chatItem.button];
                    [self.rightItems addObject:item];
                }
            }
        }
    }
    
    CGRect inputFrame = self.inputTextView.frame;
    CGFloat value = oMaxX - CGRectGetMaxX(inputFrame);
    inputFrame.size.width += value;
    self.inputTextView.frame = inputFrame;
    
    CGRect recordFrame = self.recordButton.frame;
    recordFrame.origin.x = inputFrame.origin.x;
    recordFrame.size.width = inputFrame.size.width;
    self.recordButton.frame = recordFrame;
}

- (void)setDataSource:(id<IMXChatToolBarDataSource>)dataSource {
    
    _dataSource = dataSource;
}

- (BOOL)isClickInKeyboard:(UIView *)touchView {
    if (touchView.tag == MXChatToolBarViewTagForCellContentView ||
        touchView.tag == MXChatToolBarStyleTagForBarBackground ||
        [touchView isKindOfClass:NSClassFromString(@"YYTextContainerView")] ||
        [touchView isKindOfClass:[MXFaceView class]] ||
        [touchView isKindOfClass:[MXChatToolBar class]] ||
        [touchView isKindOfClass:NSClassFromString(@"MXFacialPanelView")] ||
        touchView.tag == MXChatToolBarStyleTagForBottomView) {
        
        return YES;
    }
    return NO;
}
@end
