//
//  MXChatToolBar.h
//  ChatToolBar
//
//  聊天底部toolBar，包括文字输入、语音录制、表情、自定义面板
//  Created by aken on 16/4/18.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXChatBarMoreView.h"
#import "MXFaceView.h"
#import "MXInputTextView.h"
#import <UIKit/UIKit.h>

#import "IMXChatToolBarDataSource.h"
#import "IMXChatToolBarDelegate.h"

typedef NS_ENUM(NSInteger, MXChatToolBarStyle) {
    MXChatToolBarStyleDefault = 0,    // 标准的聊天bar
    MXChatToolBarStyleSimple,         // 只有一个文本框
    MXChatToolBarStyleStaticEmotion,  // 带有一套静态表情和文本框
    MXChatToolBarStyleOnlyFaceEmotion // 只有带有一套静态表情
};

@interface MXChatToolBar : UIView

/*!
 @property
 @brief 代理
 */
@property(nonatomic, weak) id<IMXChatToolBarDelegate> delegate;

/*!
 @property
 @brief 数据源
 */
@property(nonatomic, weak) id<IMXChatToolBarDataSource> dataSource;

/*!
 @property
 @brief ToolBar是否弹了起来
 */
@property(nonatomic, readonly) BOOL isPop;

/*!
 @property
 @brief 当前活跃的底部扩展页面
 */
@property(nonatomic, strong) UIView *activityButtomView;

/*!
 @property
 @brief 用于输入文本消息的输入框
 */
@property(nonatomic, strong) MXInputTextView *inputTextView;

/*!
 @property
 @brief 用于输入文本消息的输入框
 */
@property(nonatomic, strong) UITextView *exTextView;


/*!
 @property
 @brief 表情的附加页面
 */
@property(nonatomic, strong) UIView *faceView;

/*!
 @property
 @brief 更多的附加页面
 */
@property(nonatomic, strong) UIView *moreView;

/*!
 @property
 @brief 最大输入文本框高度
 */
@property(nonatomic) CGFloat maxTextInputViewHeight;

/*!
 @method
 @brief 根据不同的枚举创建不同样式的bar
 @param frame    位置
 @param barStyle 样式
 */
- (id)initWithFrame:(CGRect)frame style:(MXChatToolBarStyle)barStyle;

/*!
 @method
 @brief 重置录制按钮状态
 */
- (void)resetRecordState;

/*!
 @method
 @brief 默认toolbar高度
 */
+ (CGFloat)defaultHeight;

/*!
 @method
 @brief 刷新数据
 */
- (void)reloadData;

- (BOOL)isClickInKeyboard:(UIView *)touchView;

- (void)enableKeyboardNotification:(BOOL)enable;
@end

// 整个toolbar框的高度
extern NSInteger const MXToolBarPanelDefaultHeight;
