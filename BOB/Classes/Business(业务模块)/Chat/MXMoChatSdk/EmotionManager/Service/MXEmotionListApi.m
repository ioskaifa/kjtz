//
//  MXEmotionListApi.m
//  ChatToolBar
//
//  Created by aken on 16/5/5.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionListApi.h"
#import "MXEmotionSection.h"

@implementation MXEmotionListApi

+ (void)fetchEmotionCategory:(void(^)(NSArray *array))completion {
    
    NSString *jsonString = @"{\"data_list\":[{\"author\":\"moxian\",\"catalogs\":[{\"id\":\"mt_page0\",\"name\":\"3D魔牙\",\"description\":\"您收到一个重磅炸弹，请谨慎下载~\",\"fileUrl\":\"moya.zip\",\"hot\":\"1\",\"createTime\":\"1468307238\",\"catalogIcon\":\"moya.png\"},{\"id\":\"mt_page1\",\"name\":\"小熊\",\"description\":\"不只2，还有爱\",\"fileUrl\":\"bear.zip\",\"hot\":\"1\",\"createTime\":\"1468307438\",\"catalogIcon\":\"bear.png\"},{\"id\":\"mt_page2\",\"name\":\"变色龙\",\"description\":\"你有一个表情包，请注意下载~\",\"fileUrl\":\"rabit.zip\",\"hot\":\"1\",\"createTime\":\"1468307638\",\"catalogIcon\":\"rabit.png\"},{\"id\":\"mt_page3\",\"name\":\"魔小牙的世界\",\"description\":\"魔小牙的世界\",\"fileUrl\":\"moxiaoya.zip\",\"hot\":\"1\",\"createTime\":\"1468307638\",\"catalogIcon\":\"moxiaoya.png\"}]}]}";
    
    id obj=nil;
    NSError *error=nil;
    
        
    obj =[NSJSONSerialization JSONObjectWithData: [jsonString dataUsingEncoding:NSUTF8StringEncoding]
                                             options: NSJSONReadingMutableContainers
                                               error: &error];
    
    if ([obj isKindOfClass:[NSDictionary class]]) {
    
        NSArray *array =  ((NSDictionary*)obj)[@"data_list"];
        
        if ([array isKindOfClass:[NSArray class]]) {
            
            NSMutableArray *tmpAray = [NSMutableArray array];
            
            for (NSDictionary *dic in array) {
                
                MXEmotionSection *emotion = [[MXEmotionSection alloc] init];
                [emotion dictionaryToModel:dic];
                [tmpAray addObject:emotion];
            }

            completion(tmpAray);
        }
        
    }
    
}

- (NSString *)requestUrl {
    
    return [NSString stringWithFormat:@"http://file.dev2.moxian.com:80/mo_common_fileuploadservice/m2/image/getImagePacketList"];
}

- (MXRequestMethod)requestMXMethod {
    return MXRequestMethodGet;
}

- (MXRequestSerializerType)requestMXSerializerType {
    return MXRequestSerializerTypeHTTP;
}

@end
