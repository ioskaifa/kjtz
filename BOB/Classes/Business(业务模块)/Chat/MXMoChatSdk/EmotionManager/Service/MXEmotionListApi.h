//
//  MXEmotionListApi.h
//  ChatToolBar
//
//  获取表情包列表
//  Created by aken on 16/5/5.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXRequest.h"

@interface MXEmotionListApi : MXRequest


+ (void)fetchEmotionCategory:(void(^)(NSArray *array))completion;

@end
