//
//  EmotionManagerConfig.h
//  MXChatToolBarDemo
//
//  Created by aken on 16/7/20.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#ifndef EmotionManagerConfig_h
#define EmotionManagerConfig_h

static const NSString *kEmotionPackageRemoteRootPath = @"moxian_im_gif";
static const NSString *kEmotionPackageOldRemoteUrl = @"http://image.moxian.com/moxian_im_gif";
#define kEmotionPackageRemoteUrl  MX_HTTP_URL_FORMAT(@"image",@"") //

//static const NSString *kEmotionPackageRemoteUrl = @"http://image.dev2.moxian.com";

#endif /* EmotionManagerConfig_h */

