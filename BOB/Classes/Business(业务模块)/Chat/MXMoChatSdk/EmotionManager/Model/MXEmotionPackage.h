//
//  MXEmotionPackage.h
//  ChatToolBar
//
//  表情包列表消息对象
//  Created by aken on 16/5/3.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MXEmotionPackage : NSObject

/**
 *  表情包ID
 */
@property (nonatomic, copy) NSString *packageId;

/**
 *  表情包广告
 */
@property (nonatomic, copy) NSString *packageBanner;

/**
 *  表情包介绍
 */
@property (nonatomic, copy) NSString *packageIntro;

/**
 *  表情版权
 */
@property (nonatomic, copy) NSString *packageCopyright;

/**
 *  表情作者
 */
@property (nonatomic, copy) NSString *packageAuthor;

/**
 *  表情名称
 */
@property (nonatomic, copy) NSString *emotionName;

/**
 *  创建日期
 */
@property (nonatomic, copy) NSString *createTime;

/**
 *  表情封面图
 */
@property (nonatomic, copy) NSString *packageCover;

/**
 *  是否促销
 */
@property (nonatomic) NSInteger promotion;

/**
 *  聊天bar的封面图
 */
@property (nonatomic, copy) NSString *chatBarIcon;

/**
 *  表情类型
 */
@property (nonatomic, copy) NSString *type;

/**
 *  表情数组
 */
@property (nonatomic, strong) NSArray *emotions;

/**
 *  是否正在下载
 */
@property (nonatomic, assign, getter=isDownloading) BOOL downloading;

/**
 *  是否下载了
 */
@property (nonatomic, assign, getter=isCompleted) BOOL completed;

@property   (nonatomic) float progress;

/**
 *  表情包下载地址
 */
@property (nonatomic, copy) NSString *fileUrl;

/*!
 @method
 @brief 将字典转化成model (用于网络请求中解析)
 @param dictionary 字典
 */
- (void)dictionaryToModel:(NSDictionary*)dictionary;

/*!
 @method
 @brief 将字典转化成model (用于网络请求中解析)
 @param dictionary 字典
 */
- (void)dictionaryToAdModel:(NSDictionary*)dictionary;

/*!
 @method
 @brief 将字典转化成model (解析本地数据字典)
 @param dictionary 字典
 */
- (void)changeDictionaryToModel:(NSDictionary*)dictionary;

@end
