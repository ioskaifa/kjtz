//
//  MXEmotionSection.m
//  ChatToolBar
//
//  Created by aken on 16/5/5.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionSection.h"
#import "MXEmotionPackage.h"

@implementation MXEmotionSection

- (void)dictionaryToModel:(NSDictionary*)dictionary {
    
    if (!dictionary) {
        return;
    }
    
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        self.sectionId = [NSString stringWithFormat:@"%@",dictionary[@"id"]];
        
        self.sectionName = @"热门表情";//[NSString stringWithFormat:@"%@",dictionary[@"category_name"]];
        
        NSArray *tmpPackages = dictionary[@"catalogs"];
        
        if (tmpPackages) {
            
            NSMutableArray *aray = [NSMutableArray array];
            
            for (NSDictionary *dic in tmpPackages) {
                
                MXEmotionPackage *package = [[MXEmotionPackage alloc] init];
                [package dictionaryToModel:dic];
                
                [aray addObject:package];
            }
            
            self.emoticionPackages = [aray mutableCopy];
        }
    }

    
}

@end
