//
//  MXEmotionSection.h
//  ChatToolBar
//
//  类别：推荐和更多
//  Created by aken on 16/5/5.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MXEmotionSection : NSObject

/**
 *  类别ID
 */
@property (nonatomic, copy) NSString *sectionId;

/**
 *  类别名称
 */
@property (nonatomic, copy) NSString *sectionName;

/**
 *  表情数组
 */
@property (nonatomic, strong) NSArray *emoticionPackages;

- (void)dictionaryToModel:(NSDictionary*)dictionary;

@end
