//
//  MXEmotionDetails.h
//  ChatToolBar
//
//  表情包对应包的表情详情
//  Created by aken on 16/5/6.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MXEmotionDetails : NSObject

/**
 *  表情包ID
 */
@property (nonatomic, copy) NSString *packageId;

/**
 *  表情ID
 */
@property (nonatomic, copy) NSString *emotionId;

/**
 *  表情描述
 */
@property (nonatomic, copy) NSString *emotionName;

/**
 *  表情内码
 */
@property (nonatomic, copy) NSString *emotionCode;

/**
 *  表情展示图
 */
@property (nonatomic, copy) NSString *emotionThumbail;

/**
 *  表情动态图
 */
@property (nonatomic, copy) NSString *emotionImagePath;

- (void)dictionaryToModel:(NSDictionary*)dictionary;

@end
