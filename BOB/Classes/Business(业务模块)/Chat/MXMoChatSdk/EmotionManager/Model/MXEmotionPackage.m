//
//  MXEmotionPackage.m
//  ChatToolBar
//
//  Created by aken on 16/5/3.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionPackage.h"
#import "MXEmotionDetails.h"

@implementation MXEmotionPackage

- (void)dictionaryToModel:(NSDictionary*)dictionary {
    
    if (!dictionary) {
        return;
    }
    
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        
        self.packageId = [NSString stringWithFormat:@"%@",dictionary[@"id"]];
        self.packageIntro = [NSString stringWithFormat:@"%@",dictionary[@"description"]];
        self.emotionName = [NSString stringWithFormat:@"%@",dictionary[@"name"]];
        self.packageCover = [NSString stringWithFormat:@"%@",dictionary[@"catalogIcon"]];
        self.createTime = [NSString stringWithFormat:@"%@",dictionary[@"createTime"]];
        self.fileUrl = [NSString stringWithFormat:@"%@",dictionary[@"fileUrl"]];
        
//        self.packageId = [NSString stringWithFormat:@"%@",dictionary[@"id"]];
//        self.packageIntro = [NSString stringWithFormat:@"%@",dictionary[@"description"]];
//        self.emotionName = [NSString stringWithFormat:@"%@",dictionary[@"name"]];
//        self.packageCover = [NSString stringWithFormat:@"%@",dictionary[@"catalogIcon"]];
//        self.createTime = [NSString stringWithFormat:@"%@",dictionary[@"createTime"]];
//        self.fileUrl = [NSString stringWithFormat:@"%@",dictionary[@"fileUrl"]];
    }
    
}

- (void)dictionaryToAdModel:(NSDictionary*)dictionary {
    
    self.packageId = [NSString stringWithFormat:@"%@",dictionary[@"guid"]];
    
    self.packageBanner = [NSString stringWithFormat:@"%@",dictionary[@"banner"]];
    
    self.packageIntro = [NSString stringWithFormat:@"%@",dictionary[@"intro"]];
    
    self.packageCopyright = [NSString stringWithFormat:@"%@",dictionary[@"copyright"]];
    
    self.packageAuthor = [NSString stringWithFormat:@"%@",dictionary[@"author"]];
    
    self.emotionName = [NSString stringWithFormat:@"%@",dictionary[@"name"]];
    
    self.createTime = [NSString stringWithFormat:@"%@",dictionary[@"createtime"]];
    
    self.packageCover = [NSString stringWithFormat:@"%@",dictionary[@"cover"]];
    
    self.promotion = [[NSString stringWithFormat:@"%@",dictionary[@"promotion"]] intValue];
    
    self.chatBarIcon = [NSString stringWithFormat:@"%@",dictionary[@"chat_icon"]];
    
    self.type = [NSString stringWithFormat:@"%@",dictionary[@"type"]];
    

}

- (void)changeDictionaryToModel:(NSDictionary*)dictionary {
    
    self.packageAuthor = dictionary[@"author"];
    NSDictionary *tmpDic = dictionary[@"catalog"];
    
    if ([tmpDic isKindOfClass:[NSDictionary class]]) {
        
        self.packageId = [NSString stringWithFormat:@"%@",tmpDic[@"id"]];
        self.packageIntro = [NSString stringWithFormat:@"%@",tmpDic[@"description"]];
        self.emotionName = [NSString stringWithFormat:@"%@",tmpDic[@"name"]];
        self.packageCover = [NSString stringWithFormat:@"%@",tmpDic[@"catalogIcon"]];
        self.createTime = [NSString stringWithFormat:@"%@",tmpDic[@"createTime"]];
        self.fileUrl = [NSString stringWithFormat:@"%@",tmpDic[@"fileUrl"]];
        self.chatBarIcon = self.packageCover;
    }

    NSArray *tmpPackages = dictionary[@"emoticions"];
    
    if (tmpPackages) {
        
        NSMutableArray *aray = [NSMutableArray array];
        
        @autoreleasepool {
            
            for (NSDictionary *dic in tmpPackages) {
                
                MXEmotionDetails *package = [[MXEmotionDetails alloc] init];
                [package dictionaryToModel:dic];
                
                [aray addObject:package];
            }

            
        }
        self.emotions = [aray mutableCopy];
    }

}

@end
