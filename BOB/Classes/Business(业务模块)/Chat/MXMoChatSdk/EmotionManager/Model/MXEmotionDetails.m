//
//  MXEmotionDetails.m
//  ChatToolBar
//
//  Created by aken on 16/5/6.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionDetails.h"

@implementation MXEmotionDetails

- (void)dictionaryToModel:(NSDictionary*)dictionary {
    
    if (!dictionary) {
        return;
    }
    
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        
//        self.packageId = [NSString stringWithFormat:@"%@",dictionary[@"package_id"]];
//        self.emotionId = [NSString stringWithFormat:@"%@",dictionary[@"guid"]];
//        self.emotionThumbail = [NSString stringWithFormat:@"%@",dictionary[@"thumbail"]];
//        self.emotionCode = [NSString stringWithFormat:@"%@",dictionary[@"emo_code"]];
        self.emotionName = [NSString stringWithFormat:@"%@",dictionary[@"emoName"]];
        self.emotionImagePath = [NSString stringWithFormat:@"%@",dictionary[@"emoText"]];
    }
    
}

@end
