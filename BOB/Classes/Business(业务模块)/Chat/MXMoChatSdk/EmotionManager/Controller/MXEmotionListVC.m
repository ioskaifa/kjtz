//
//  MXEmotionListVC.m
//  ChatToolBar
//
//  Created by aken on 16/4/28.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionListVC.h"
#import "MXEmotionListView.h"

#import "MXEmotionListApi.h"
#import "MXEmotionExManager.h"
#import "MXEmotionPackage.h"
#import "MXEmotionSection.h"
#import "MXEmotionDownload.h"
#import "MXLoadingErrorView.h"

@interface MXEmotionListVC ()

@property (nonatomic, strong) MXEmotionListView  *listView;
@property (nonatomic, strong) MXLoadingErrorView *errorView;
@property (nonatomic, strong) MXLoadingErrorView *noEmotionView;

@end

@implementation MXEmotionListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"表情商店";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.listView];
    [self.view addSubview:self.errorView];
    [self.view addSubview:self.noEmotionView];
    
    [self initData];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [[MXEmotionDownload sharedInstance] removeAllDownloadDelegate];
}

#pragma mark - initData
- (void)initData {
    
    @weakify(self)
    
    MXEmotionListApi *api = [[MXEmotionListApi alloc] init];
    api.ignoreCache = YES;
    
     [NotifyHelper showMXLoadingHUDAddedTo:self.view animated:YES];
    
    [api startWithCompletionBlockWithSuccess:^(MXRequest *request) {
        
        @strongify(self)
        [NotifyHelper hideHUDForView:self.view animated:YES];
        NSDictionary *diction = (NSDictionary*)request.responseJSONObject;
        
        
        if (diction && [diction isKindOfClass:[NSDictionary class]]) {
            
            BOOL result = [diction[@"result"] boolValue];
            if (result) {
                
                NSDictionary *dict = diction[@"data"];
                
                NSArray *arry = dict[@"catalogs"];
                if (arry && [arry isKindOfClass:[NSArray class]] &&
                    arry.count > 0) {
                    
                    NSMutableArray *emotionArray = [NSMutableArray array];
                    
                    MXEmotionSection *emotion = [[MXEmotionSection alloc] init];
                    [emotion dictionaryToModel:dict];
                    [emotionArray addObject:emotion];
                    [self dealEmotionData:emotionArray];
                    
                    self.errorView.hidden = YES;
                    self.noEmotionView.hidden = YES;
                    
                }else {
                    
                    self.noEmotionView.hidden = NO;
                    self.errorView.hidden = YES;
                }
                
                
                
            }else {
                self.noEmotionView.hidden = NO;
                self.errorView.hidden = YES;
            }
        }
        
    } failure:^(MXRequest *request) {
        
        @strongify(self)
        [NotifyHelper hideHUDForView:self.view animated:YES];
        
        self.errorView.hidden = NO;
        MLog(@"failed");
    }];
    
}

- (void)dealEmotionData:(NSArray*)array {
    
    NSArray *tmpArray = [[MXEmotionExManager manager] loadEmotionsPackages];

    if (tmpArray && tmpArray.count > 0) {

        for (MXEmotionPackage *package in tmpArray) {
            
            for(MXEmotionSection *section in array) {
                
                for (MXEmotionPackage *finalPackage in section.emoticionPackages) {
                    
                    if ([package.fileUrl isEqualToString:finalPackage.fileUrl]) {
                        finalPackage.completed = YES;
                        finalPackage.downloading = NO;
                    }else if ([package.packageId isEqualToString:finalPackage.packageId]) {
                        finalPackage.completed = YES;
                        finalPackage.downloading = NO;
                    }
                }
            }
        }
    }
     [self.listView reloadData:array];
}

- (void)clickBtnEvent:(id)sender {
    
    [self initData];
}

- (MXEmotionListView*)listView {
    if (!_listView) {
        _listView = [[MXEmotionListView alloc] initWithFrame:self.view.bounds];
        
         __weak __typeof(self)weakSelf = self;
        _listView.downloadedBlick = ^ {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(didFinishDownloadEmotionPackage)]) {
                [strongSelf.delegate didFinishDownloadEmotionPackage];
            }

        };
    }
    
    return _listView;
}

- (MXLoadingErrorView *)errorView {
    
    if (!_errorView) {
        _errorView = [[MXLoadingErrorView alloc] initWithFrame:self.view.bounds];
        [_errorView setImageWithName:@"shopping_noMerchant" withText:@"加载失败，点击重新加载~"];
        [_errorView setHidden:YES];
        
        UIButton *clickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        clickBtn.frame = _errorView.bounds;
        [_errorView addSubview:clickBtn];
        [clickBtn addTarget:self action:@selector(clickBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _errorView;
}

- (MXLoadingErrorView *)noEmotionView {
    
    if (!_noEmotionView) {
        _noEmotionView = [[MXLoadingErrorView alloc] initWithFrame:self.view.bounds];
        [_noEmotionView setImageWithName:@"talk_emotion_moya_nor" withText:@"表情商店努力建设中，敬请期待~"];
        [_noEmotionView setHidden:YES];
    }
    return _noEmotionView;
}



@end
