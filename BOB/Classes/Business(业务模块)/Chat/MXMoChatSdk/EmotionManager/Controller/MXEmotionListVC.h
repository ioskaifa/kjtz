//
//  MXEmotionListVC.h
//  ChatToolBar
//
//  Created by aken on 16/4/28.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MXEmotionListVCDelegate <NSObject>

@optional

- (void)didFinishDownloadEmotionPackage;

@end

@interface MXEmotionListVC : BaseViewController

@property (nonatomic, weak) id<MXEmotionListVCDelegate> delegate;

@end
