//
//  MXEmotionExManager.m
//  MXChatToolBarDemo
//
//  Created by aken on 16/5/30.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionExManager.h"
#import "MXEmotionDownload.h"

#import "MXEmotionPackage.h"

static NSString * const kMXEmotionManagerPackageName = @"packages.plist";
static NSString * const kMXEmotionManagerPackageNamePathExtension = @"txt";

NSString * const kMXEmotionExManagerEmotionArrayKey = @"MXEmotionExManagerEmotionArray";
NSString * const kMXEmotionExManagerEmotionFileNameKey = @"MXEmotionExManagerEmotionFileName";
//
NSString * const kMXEmotionExManagerEmotionGifPathKey = @"MXEmotionExManagerEmotionGifPathKey";
NSString * const kMXEmotionExManagerEmotionGifNameKey = @"MXEmotionExManagerEmotionGifNameKey";


@implementation MXEmotionExManager {
    
    NSMutableArray *_cacheGifArray;
}

+ (MXEmotionExManager*)manager {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (id)init {
    
    self = [super init];
    
    if (self) {
        _cacheGifArray = [NSMutableArray array];
    }
    
    return self;
}

+ (void)saveEmotionPackageWithPath:(NSString*)url coverIcon:(NSString*)coverUrl {
    
    NSString *downloadPath = [[MXEmotionDownload sharedInstance] packagePath];
    NSString *filepath=[downloadPath stringByAppendingPathComponent:kMXEmotionManagerPackageName];
    
    NSMutableArray *faceArray = [[NSMutableArray alloc]initWithContentsOfFile:filepath];
    
    if (!faceArray) {
        faceArray = [NSMutableArray array];
    }
    
    BOOL isExit = NO;
    
    for (NSDictionary *dictionary in faceArray) {
        
        NSString *fileUrl = [dictionary[@"path"] lastPathComponent];
        if ([fileUrl isEqualToString:[url lastPathComponent]]) {
            isExit = YES;
            break;
        }
    }
    
    if (isExit) {
        return;
    }
    
    NSString *configName = [url lastPathComponent];
    NSString *fileName = [self stringByDeletingExtension:configName];
    
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    NSDictionary *dic =@{@"createTime":@(now),
                         @"path":configName,
                         @"zipPath":fileName,
                         MXEmotionDownloadEmotionCover:coverUrl};
    
    
    
    [faceArray addObject:dic];
    
    [faceArray writeToFile:filepath atomically:YES];
}

+ (NSArray*)loadSimpleEmotionsPackageConfigs {
    
    NSString *downloadPath = [[MXEmotionDownload sharedInstance] packagePath];
    NSString *filepath=[downloadPath stringByAppendingPathComponent:kMXEmotionManagerPackageName];
    
    // 所下载文件列表
    NSArray *tmpFaceArray = [[NSArray alloc]initWithContentsOfFile:filepath];
    
    return tmpFaceArray;
}

- (NSArray*)loadEmotionsPackages {
    
    // 所下载文件本地配置列表
    NSArray *tmpFaceArray = [self.class loadSimpleEmotionsPackageConfigs];
    // tmpFaceArray 和 _cacheGifArray 数量相同，刚本地配置没有更新，如果有更新，则重新加载
    if (_cacheGifArray.count > 0 && tmpFaceArray.count == _cacheGifArray.count) {
        
        return _cacheGifArray;
    }
    
    if (tmpFaceArray.count > 0 && _cacheGifArray.count > 0) {
        
        // 本地配置列表有更新
        if (tmpFaceArray.count > _cacheGifArray.count) {
            [_cacheGifArray removeAllObjects];
        }
    }
    
    NSArray *faceArray = [[tmpFaceArray reverseObjectEnumerator] allObjects];
    NSString *path = [[MXEmotionDownload sharedInstance] unZipPath];
    
    if (faceArray) {
        
//        NSMutableArray *tmpAray = [NSMutableArray array];
        @autoreleasepool {
            
            for (NSDictionary *dictionary in faceArray) {
                
                NSString *configName = dictionary[@"path"];
                NSString *unzipPath = nil;
                NSString *fileName = [self.class stringByDeletingExtension:configName];
                

                // 如果本地存在了老版本
                if ([configName isEqualToString:@"moya.txt"]) {
                     // 配置更新的表情配置
                    NSString *bundlePath = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"MXChatToolBar.bundle"];
                    NSString *facePath = [[NSBundle bundleWithPath:bundlePath] pathForResource:@"moya" ofType:@"txt" inDirectory:@"Emotion/plist"];
                    unzipPath = facePath;
                }else {
                    unzipPath = [NSString stringWithFormat:@"%@/%@/%@",path,fileName,configName];
                    
                    // 解压完后，要判断zip包有可能是在mac下面压缩，所以解压出来会多一层包的文件夹目录
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    
                    BOOL isDirExist = [fileManager fileExistsAtPath:unzipPath];
                    if(!isDirExist){
                        
                        unzipPath = [NSString stringWithFormat:@"%@/%@/%@/%@",path,fileName,fileName,configName];
                    }
                }
                
                
                
                NSData *textData = [NSData dataWithContentsOfFile:unzipPath];
                NSString *jsonSgtring = [[NSString alloc] initWithData:textData encoding:NSUTF8StringEncoding];
           
                NSError *error=nil;
     
                id obj =[NSJSONSerialization JSONObjectWithData: [jsonSgtring dataUsingEncoding:NSUTF8StringEncoding]
                                                         options: NSJSONReadingMutableContainers
                                                           error: &error];
                if (!error && obj) {
                    
                    NSDictionary *tmpDic = (NSDictionary*)obj;
                    MXEmotionPackage *emotion = [[MXEmotionPackage alloc] init];
                    [emotion changeDictionaryToModel:tmpDic];
                    emotion.packageCover = dictionary[MXEmotionDownloadEmotionCover];
                    [_cacheGifArray addObject:emotion];
                }
                
            }
        }
        
        return _cacheGifArray;
    }
    
    return nil;
    
}


+ (NSDictionary*)loadUnZipEmotions {
    
    NSString *path = [[MXEmotionDownload sharedInstance] unZipPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir = FALSE;
    BOOL isDirExist = [fileManager fileExistsAtPath:path isDirectory:&isDir];
    if(!(isDirExist && isDir)){
        
        return nil;
    }
    
    
    NSURL *bundleURL = [NSURL fileURLWithPath:path];

    
    NSDirectoryEnumerator *enumerator = [fileManager enumeratorAtURL:bundleURL
                                          includingPropertiesForKeys:@[NSURLNameKey, NSURLIsDirectoryKey]
                                                             options:NSDirectoryEnumerationSkipsHiddenFiles
                                                        errorHandler:^BOOL(NSURL *url, NSError *error)
    {
        if (error) {
            MLog(@"[错误] %@ (%@)", error, url);
            return NO;
        }
        
        return YES;
    }];
    
    // 表情路径
    NSMutableArray *fileUrl = [NSMutableArray array];
    // 表情对应文件夹
    NSMutableArray *fileNames = [NSMutableArray array];
    
    @autoreleasepool {
        
        for (NSURL *fileURL in enumerator) {
            NSString *filename;
            [fileURL getResourceValue:&filename forKey:NSURLNameKey error:nil];
            
            
            
            NSNumber *isDirectory;
            [fileURL getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:nil];
            
            // zip解压包括了__MACOSX
            if ([filename hasPrefix:@"_"] && [isDirectory boolValue]) {
                [enumerator skipDescendants];
                continue;
            }
            
            // 里面有txt文本配置

            if (![isDirectory boolValue]) {
                
                if ([filename hasSuffix:kMXEmotionManagerPackageNamePathExtension]) {
                    continue;
                }
                
                NSDictionary *dic = @{kMXEmotionExManagerEmotionGifPathKey:fileURL.path,
                                      kMXEmotionExManagerEmotionGifNameKey:filename};
                [fileUrl addObject:dic];
                
            }else {
                
                if (filename) {
                    
//                    NSString *lastName = fileURL.path.stringByDeletingLastPathComponent;
                    
//                    if ([lastName hasSuffix:kMXEmotionDownloadUnZipPath]) {
//                        continue;
//                    }
                    
                    [fileNames addObject:[NSString stringWithFormat:@"%@/%@",fileURL.path.stringByDeletingLastPathComponent,filename]];
                }
            }
        }
    }
    
    NSDictionary *dic = @{kMXEmotionExManagerEmotionArrayKey:fileUrl,
                          kMXEmotionExManagerEmotionFileNameKey:fileNames};

    
    return dic;
    
}

#pragma mark - Private

+ (NSString*)stringByDeletingExtension:(NSString*)filePath {
    
    NSString *fileName = nil;
    
    if (filePath) {
        
        if ([filePath rangeOfString:@"."].location !=NSNotFound) {
            
            NSArray *tmpArray = [filePath componentsSeparatedByString:@"."];
            fileName = tmpArray[0];
        }
    }
    
    return fileName;
}

@end
