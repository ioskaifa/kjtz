//
//  MXEmotionDownload.h
//  ChatToolBar
//
//  表情下载
//  Created by aken on 16/5/10.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol IMXEmotionDownloadDelegate;

@interface MXEmotionDownload : NSObject

/*!
 @method
 @brief 单例
 */
+ (MXEmotionDownload*)sharedInstance;

/*!
 @method
 @brief 下载表情包
 @param path 表情包网络路径
 @param userInfos 额外参数
 @param progress 下载进度回调的代理
 */

- (void)downloadEmotionDownWithUrl:(NSString*)path userInfos:(id)userInfos
            progress:(id<IMXEmotionDownloadDelegate>)progress;

/*!
 @method
 @brief 下载相关信息
 */
- (NSArray*)downloadArray;

/*!
 @method
 @brief 保存未下载完的url到本地
 */
- (void)saveCacheUnDownloaded;

/*!
 @method
 @brief 继续下载未完成的表情包
 */
- (void)autoDownloadUnCompleteEmotion;

/*!
 @method
 @brief 添加下载代理
 @param delegate 代理
 */
- (void)addDownloadDelegate:(id<IMXEmotionDownloadDelegate>)delegate;

/*!
 @method
 @brief 删除下载代理
 @param delegate 代理
 */
- (void)removeDownloadDelegate:(id<IMXEmotionDownloadDelegate>)delegate;

/*!
 @method
 @brief 删除所有下载代理
 */
- (void)removeAllDownloadDelegate;

/*!
 @method
 @brief 表情下载路径
 */
- (NSString*)packagePath;

/*!
 @method
 @brief 表情解压路径
 @param name 文件名
 */
- (NSString*)unZipPath;

/*!
 @method
 @brief 停止所有下载
 */
- (void)cancelAllDownload;

@end


/*!
 @protocol
 @brief 下载进度显示
 */
@protocol IMXEmotionDownloadDelegate <NSObject>

@optional

/*!
 @method
 @brief 进度
 @param progress 值域为0到1.0的浮点数
 @param index 表情下标
 */
- (void)setProgress:(float)progress downloadIndex:(NSInteger)index;

/*!
 @method
 @brief 下载成功回调
 @param success YES/NO
 @param index 表情下标
 */
- (void)finishDownloadWithSuccess:(BOOL)success downloadIndex:(NSInteger)index;

@end