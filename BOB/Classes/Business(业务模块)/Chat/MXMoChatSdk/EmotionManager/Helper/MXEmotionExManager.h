//
//  MXEmotionExManager.h
//  MXChatToolBarDemo
//
//  表情工具类
//  Created by aken on 16/5/30.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#if __has_include(<MXChatToolBarSdk/MXChatToolBarSdk.h>)
#import <MXChatToolBarSdk/MXChatToolBarSdk.h>
#else
#import "MXChatToolBarSdk.h"
#endif

static NSString *const MXEmotionDownloadEmotionCover = @"cover";

@interface MXEmotionExManager : NSObject

+ (MXEmotionExManager*)manager;

/*!
 @method
 @brief 获取解压后所有gif数据配置
 @result 返回表情列表
 */
+ (NSArray*)loadSimpleEmotionsPackageConfigs;

/*!
 @method
 @brief 获取解压后所有gif数据
 @result 返回表情列表和表情所在路径数组
 */
- (NSArray*)loadEmotionsPackages;

/*!
 @method
 @brief 根据url保存该表情本地配置
 @param url  本地url
 @param coverUrl 表情封面
 */
+ (void)saveEmotionPackageWithPath:(NSString*)url coverIcon:(NSString*)coverUrl;

@end
