//
//  MXEmotionDownload.m
//  ChatToolBar
//
//  Created by aken on 16/5/10.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionDownload.h"
#import "MXChatDocumentManager.h"
#import "MXEmotionExManager.h"
#import "SSZipArchive.h"
#if __has_include(<AFNetworking/AFNetworking.h>)
#import <AFNetworking/AFNetworking.h>
#else
#import "AFNetworking.h"
#endif

const NSString *kMXEmotionDownloadFilePath = @"moxian_im_gif";
NSString * const kMXEmotionDownloadFileIndexKey = @"index";
NSString * const kMXEmotionDownloadFractionCompleted  = @"fractionCompleted";
NSString * const kMXEmotionDownloadFileUnComplete  = @"unCompleted";
NSString * const MXEmotionDownloadUnZipPath = @"Gif";

@interface MXEmotionDownload()

// Session管理器
@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

// 下载代理
@property (nonatomic, weak) id<IMXEmotionDownloadDelegate> donwloadDelegate;

// 下载相关信息
@property (nonatomic, strong) NSMutableDictionary *allDownloaderDictionary;
@property (nonatomic, strong) NSMutableArray *downloadDelegates;

@end

@implementation MXEmotionDownload

+ (MXEmotionDownload*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (id)init {
    
    self = [super init];
    
    if (self) {
        
        _sessionManager = [AFHTTPSessionManager manager];
        _allDownloaderDictionary = [NSMutableDictionary dictionary];
        _downloadDelegates = [NSMutableArray array];
    }
    
    return self;
}

#pragma mark - Public
- (void)downloadEmotionDownWithUrl:(NSString*)path userInfos:(id)userInfos
                          progress:(id<IMXEmotionDownloadDelegate>)progress {
    
    if (_allDownloaderDictionary.count > 0) {
        
        BOOL success = [_allDownloaderDictionary.allKeys containsObject:path];
        
        if (success) {
            return;
        }
        
    }
    
    if (progress) {
        [self addDownloadDelegate:progress];
    }
    
    __block NSDictionary *dictionary;
    NSInteger index = 0;
    if ([userInfos isKindOfClass:[NSDictionary class]]) {
        
        dictionary = (NSDictionary*)userInfos;
        index = [dictionary[@"index"] intValue];
    }
    
    NSProgress *recProgress;
    
    NSURL *url = [NSURL URLWithString:path];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSessionDownloadTask *downloadTask = [_sessionManager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        
        if (_downloadDelegates && _downloadDelegates.count > 0) {
            
            for (id<IMXEmotionDownloadDelegate> delegate in _downloadDelegates) {
                
                if (delegate && [delegate respondsToSelector:@selector(setProgress:downloadIndex:)]) {
                    
                    [delegate setProgress:downloadProgress.fractionCompleted downloadIndex:index];
                }
            }
        }
        
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        
        NSURL *documentsDirectoryPath = [NSURL fileURLWithPath:[self packagePath]];
        return [documentsDirectoryPath URLByAppendingPathComponent:[response suggestedFilename]];
        
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        
        NSString *pathString = filePath.path;
        NSString *absZipString = [[MXEmotionDownload sharedInstance] unZipPath];
        
        // 文件名
        NSString *name = [self stringByDeletingExtension:[filePath lastPathComponent]];
        
        // 此处两个路径：zip包要window下压缩的，在mac下面压缩的zip包，在解压时会多一个包的名字，然后再在这名字目录下解压，如：包名叫abc.zip，解压出来是：abc/abc；同时会多一个__MACOSX
        NSString *fullZipString = [NSString stringWithFormat:@"%@/%@",absZipString,name];
        NSString *configString = [NSString stringWithFormat:@"%@/%@.txt",fullZipString,name];

        // 进行解压
        BOOL success = [SSZipArchive unzipFileAtPath:pathString toDestination:fullZipString];
        
        if (success) {
            
            if (name) {
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                BOOL isDirExist = [fileManager fileExistsAtPath:configString];
                if(isDirExist){
                    
                    [MXEmotionExManager saveEmotionPackageWithPath:configString coverIcon:dictionary[MXEmotionDownloadEmotionCover]];
                    
                    [self donwloadSuccess:YES downloadIndex:index];
                }
                // 解压完后，要判断zip包有可能是在mac下面压缩，所以解压出来会多一层包的文件夹目录
                NSString *nextLevelConfigString = [NSString stringWithFormat:@"%@/%@/%@.txt",fullZipString,name,name];
                BOOL isNextLevelDirExist = [fileManager fileExistsAtPath:nextLevelConfigString];
                if(isNextLevelDirExist){
                    
                    [MXEmotionExManager saveEmotionPackageWithPath:nextLevelConfigString coverIcon:dictionary[MXEmotionDownloadEmotionCover]];
                    
                    [self donwloadSuccess:YES downloadIndex:index];
                }
            }
            
        }else {
            MLog(@"下载失败");
            [self donwloadSuccess:NO downloadIndex:index];
        }
        
        [self removeCacheDownloadUrl:path];
        [_allDownloaderDictionary removeObjectForKey:path];
        [recProgress removeObserver:self
                         forKeyPath:kMXEmotionDownloadFractionCompleted
                            context:NULL];
        
    }]; 

    [_allDownloaderDictionary setValue:@(index) forKey:path];
    
    [downloadTask resume];
    
    [recProgress setUserInfoObject:@(index) forKey:kMXEmotionDownloadFileIndexKey];
    
}

- (NSArray*)downloadArray {
    
    return  [_allDownloaderDictionary allKeys];
}

- (void)addDownloadDelegate:(id<IMXEmotionDownloadDelegate>)delegate {
    
    if (_downloadDelegates && _downloadDelegates.count > 0) {
        if ([_downloadDelegates containsObject:delegate]) {
            return;
        }
    }
    [_downloadDelegates addObject:delegate];
}

- (void)removeDownloadDelegate:(id<IMXEmotionDownloadDelegate>)delegate {
    
    if (_downloadDelegates && _downloadDelegates.count > 0) {
        [_downloadDelegates removeObject:delegate];
    }
}

- (void)removeAllDownloadDelegate {
    
    if (_downloadDelegates) {
        [_downloadDelegates removeAllObjects];
    }
}

- (void)saveCacheUnDownloaded {
    
    if ([self downloadArray].count > 0) {
        [[NSUserDefaults standardUserDefaults] setValue:_allDownloaderDictionary forKey:kMXEmotionDownloadFileUnComplete];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)autoDownloadUnCompleteEmotion {
    
    NSDictionary *dictionary = [[NSUserDefaults standardUserDefaults] objectForKey:kMXEmotionDownloadFileUnComplete];
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        
        if (dictionary && dictionary.count > 0) {
            
            NSArray *cacheDownloadArray = [dictionary allKeys];
            
            for (NSString *url in cacheDownloadArray) {
                
                NSString *indexStr = dictionary[url];
                if (indexStr) {
                    
                    [self downloadEmotionDownWithUrl:url userInfos:@{@"index":@([indexStr intValue])} progress:nil];
                }
            }
            
            //            NSLog(@"cacheDownloadArray:%@",cacheDownloadArray);
        }
    }
    //
}

- (NSString*)packagePath {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@",[MXChatDocumentManager cacheDocDirectory],kMXEmotionDownloadFilePath];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir = FALSE;
    BOOL isDirExist = [fileManager fileExistsAtPath:path isDirectory:&isDir];
    if(!(isDirExist && isDir)){
        
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return path;
}

- (NSString*)unZipPath {
    
    NSString *zipString = [NSString stringWithFormat:@"%@/%@",[[MXEmotionDownload sharedInstance] packagePath],MXEmotionDownloadUnZipPath];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir = FALSE;
    BOOL isDirExist = [fileManager fileExistsAtPath:zipString isDirectory:&isDir];
    if(!(isDirExist && isDir)){
        
        [[NSFileManager defaultManager] createDirectoryAtPath:zipString withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    
    return zipString;
}

- (void)cancelAllDownload {
    
    NSArray *array = _sessionManager.downloadTasks;
    if (array && array.count > 0) {
        
        for (NSURLSessionDownloadTask *task in array) {
            [task cancel];
        }
    }
}

#pragma mark - Private
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//
//    if ([keyPath isEqualToString:kMXEmotionDownloadFractionCompleted] && [object isKindOfClass:[NSProgress class]]) {
//
//        NSProgress *progress = (NSProgress *)object;
//
//        if (_downloadDelegates && _downloadDelegates.count > 0) {
//
//            for (id<IMXEmotionDownloadDelegate> delegate in _downloadDelegates) {
//
//                if (delegate && [delegate respondsToSelector:@selector(setProgress:downloadIndex:)]) {
//
//                    NSDictionary *dic = progress.userInfo;
//                    NSInteger index = [dic[kMXEmotionDownloadFileIndexKey] intValue];
//
//                    [delegate setProgress:progress.fractionCompleted downloadIndex:index];
//                }
//            }
//        }
//
//    }
//}

- (NSString*)stringByDeletingExtension:(NSString*)filePath {
    
    NSString *fileName = nil;
    
    if (filePath) {
        
        if ([filePath rangeOfString:@"."].location !=NSNotFound) {
            
            NSArray *tmpArray = [filePath componentsSeparatedByString:@"."];
            fileName = tmpArray[0];
        }
    }
    
    return fileName;
}

- (void)donwloadSuccess:(BOOL)success downloadIndex:(NSInteger)index {
    
    if (_downloadDelegates && _downloadDelegates.count > 0) {
        
        for (id<IMXEmotionDownloadDelegate> delegate in _downloadDelegates) {
            
            if (delegate && [delegate respondsToSelector:@selector(finishDownloadWithSuccess:downloadIndex:)]) {
                [delegate finishDownloadWithSuccess:success downloadIndex:index];
            }
        }
    }
    
}

- (void)removeCacheDownloadUrl:(NSString*)url {
    
    NSDictionary *dictionary = [[NSUserDefaults standardUserDefaults] objectForKey:kMXEmotionDownloadFileUnComplete];
    
    if ([dictionary isKindOfClass:[NSDictionary class]]) {
        
        if (dictionary && dictionary.count > 0) {
            
            NSMutableDictionary *tmpDictionary = [NSMutableDictionary dictionaryWithDictionary:dictionary];
            [tmpDictionary removeObjectForKey:url];
            
            @synchronized (self) {
                
                [[NSUserDefaults standardUserDefaults] setValue:tmpDictionary forKey:kMXEmotionDownloadFileUnComplete];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }
}


@end
