//
//  MXEmotionListView.h
//  ChatToolBar
//
//  Created by aken on 16/5/5.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^MXEmotionListViewDownloadedBlock)(void);

@interface MXEmotionListView : UIView

@property (nonatomic, strong) MXEmotionListViewDownloadedBlock downloadedBlick;


// 刷新列表数据
- (void)reloadData:(NSArray*)array;

@end
