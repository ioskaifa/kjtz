//
//  MXEmotionListCell.h
//  ChatToolBar
//
//  Created by aken on 16/5/5.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmotionManagerConfig.h"

@class MXEmotionPackage;

@protocol MXEmotionListCellDelegate <NSObject>

@optional

- (void)startToDownloadEmotionPackageWithPackage:(MXEmotionPackage*)model indexRowAt:(NSInteger)index;

@end

@interface MXEmotionListCell : UITableViewCell

@property (nonatomic, strong) MXEmotionPackage *model;

@property (nonatomic, weak) id<MXEmotionListCellDelegate> delegate;

- (void)reloadProgress:(float)progress;

@end
