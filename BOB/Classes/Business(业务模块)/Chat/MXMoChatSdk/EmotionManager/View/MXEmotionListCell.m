//
//  MXEmotionListCell.m
//  ChatToolBar
//
//  Created by aken on 16/5/5.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionListCell.h"
#import "MXEmotionPackage.h"
#import "MXEmotionDownload.h"

#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MXNoImageView.h"


@interface MXEmotionListCell()<IMXEmotionDownloadDelegate>

/// 表情封面
@property (nonatomic, strong) UIImageView *emotionImageView;

/// 表情名字
@property (nonatomic, strong) UILabel *emotionLabel;

/// 表情介绍
@property (nonatomic, strong) UILabel *emotionInfoLabel;

/// 分割线
@property (nonatomic, strong) UIView *topLineView;

/// 下载
@property (nonatomic, strong) UIButton *downButton;

/// 下载进度条
@property (nonatomic, strong) UIProgressView *progressView;


@end

@implementation MXEmotionListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self setupUI];
        [self addUIContrains];
    }
    
    return self;
}

- (void)setModel:(MXEmotionPackage *)model {
    
    _model = model;
    NSString * encodingString = [model.packageCover stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
    
    NSURL *url = @"";//[NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",kEmotionPackageRemoteUrl,encodingString]];
    
//    [self.emotionImageView sd_setImageWithURL:url];
//    [self.emotionImageView setLoadOriginalImage:YES];
//    [self.emotionImageView mx_setImageWithURL:url placeholderImage:[MXNoImageView imageSize:CGSizeMake(60, 60)]];
    self.emotionLabel.text = model.emotionName;
    self.emotionInfoLabel.text = model.packageIntro;
    
    [self setupDownConfigs:_model];
    
}

- (void)reloadProgress:(float)progress {
    
    [self.progressView setProgress:progress animated:YES] ;
    
}

- (void)setupDownConfigs:(MXEmotionPackage *)model {
    
    if (model.downloading) {
        
        self.downButton.hidden = YES;
        self.progressView.hidden = NO;
        
        self.progressView.progress = model.progress;
    }
    
    if (model.completed) {
        [self setDownloadBuddonDisabled:YES];
    }else {
        [self setDownloadBuddonDisabled:NO];
    }
    
}

#pragma mark - Private
- (void)setupUI {

    [self.contentView addSubview:self.progressView];
    [self.contentView addSubview:self.emotionImageView];
    [self.contentView addSubview:self.downButton];
    [self.contentView addSubview:self.emotionLabel];
    [self.contentView addSubview:self.emotionInfoLabel];
    [self.contentView addSubview:self.topLineView];
}

- (void)addUIContrains {
    
    __weak MXEmotionListCell *superView = self;
    
    [self.emotionImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(superView);
        make.left.equalTo(superView).offset(10);
        make.width.and.height.equalTo(@60);
    }];
    
    [self.downButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(superView);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.width.equalTo(@60);
        make.height.equalTo(@30);
    }];
    
    [self.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(superView);
        make.right.equalTo(superView.mas_right).offset(-10);
        make.width.equalTo(@60);
    }];
    
    [self.emotionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.emotionImageView.mas_top).offset(3);
        make.left.equalTo(superView.emotionImageView.mas_right).offset(5);
        make.right.equalTo(superView.downButton.mas_left).offset(-5);
    }];
    
    [self.emotionInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superView.emotionLabel.mas_bottom).offset(3);
        make.left.equalTo(superView.emotionImageView.mas_right).offset(5);
        make.right.equalTo(superView.downButton.mas_left).offset(-5);
    }];
    
    [self.topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(superView.mas_bottom);
        make.left.equalTo(superView).offset(10);
        make.right.equalTo(superView);
        make.height.equalTo(@0.5);
    }];
}

- (void)setDownloadBuddonDisabled:(BOOL)disabled {
    
    if (disabled) {
        self.downButton.hidden = NO;
        self.progressView.hidden = YES;
        [_downButton setTitle:@"已下载" forState:UIControlStateNormal];
        _downButton.backgroundColor = [UIColor lightGrayColor];
        [_downButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        _downButton.backgroundColor = [UIColor whiteColor];
        _downButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
    }else {
        
        [_downButton setTitle:@"下载" forState:UIControlStateNormal];
        [_downButton setTitleColor:[UIColor colorWithRed:137.0/255.0 green:99.0/255.0 blue:200.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        _downButton.backgroundColor = [UIColor whiteColor];
        _downButton.layer.borderColor = [UIColor colorWithRed:137.0/255.0 green:99.0/255.0 blue:200.0/255.0 alpha:1.0].CGColor;
        _downButton.layer.borderWidth = 0.5;
        _downButton.layer.cornerRadius = 4.0;
        self.downButton.hidden = NO;
        self.progressView.hidden = YES;
    }
}

- (void)downButtonClick {
    
    if (_model.isCompleted) {
        return;
    }
    self.progressView.hidden = NO;
    self.downButton.hidden = YES;
    
    if (_delegate && [_delegate respondsToSelector:@selector(startToDownloadEmotionPackageWithPackage:indexRowAt:)]) {
        [_delegate startToDownloadEmotionPackageWithPackage:_model indexRowAt:self.tag];
    }
}

#pragma mark - Getters
- (UIImageView*)emotionImageView {
    
    if (!_emotionImageView) {
        
        _emotionImageView = [[UIImageView alloc] init];
    }
    
    return _emotionImageView;
}

- (UILabel*)emotionLabel {
    
    if (!_emotionLabel) {
        
        _emotionLabel = [[UILabel alloc] init];
        _emotionLabel.backgroundColor = [UIColor clearColor];
        _emotionLabel.textColor = [UIColor blackColor];
        _emotionLabel.numberOfLines = 1;
        _emotionLabel.font = [UIFont systemFontOfSize:16.0];
    }
    
    return _emotionLabel;
}

- (UILabel*)emotionInfoLabel {
    
    if (!_emotionInfoLabel) {
        
        _emotionInfoLabel = [[UILabel alloc] init];
        _emotionInfoLabel.backgroundColor = [UIColor clearColor];
        _emotionInfoLabel.textColor = [UIColor grayColor];
        _emotionInfoLabel.numberOfLines = 2;
        _emotionInfoLabel.font = [UIFont systemFontOfSize:14.0];
    }
    
    return _emotionInfoLabel;
}

- (UIView*)topLineView {
    
    if (!_topLineView) {
        _topLineView = [[UIView alloc] init];
        _topLineView.backgroundColor = [UIColor colorWithRed:204.0/255 green:204.0/255 blue:204.0/255 alpha:1];
    }
    
    return _topLineView;
    
}

- (UIButton*)downButton {
    
    if (!_downButton) {
        
        _downButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _downButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_downButton setTitle:@"下载" forState:UIControlStateNormal];
        [_downButton setTitleColor:[UIColor colorWithRed:137.0/255.0 green:99.0/255.0 blue:200.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        _downButton.backgroundColor = [UIColor whiteColor];
        _downButton.layer.borderColor = [UIColor colorWithRed:137.0/255.0 green:99.0/255.0 blue:200.0/255.0 alpha:1.0].CGColor;
        _downButton.layer.borderWidth = 0.5;
        _downButton.layer.cornerRadius = 4.0;
        
        [_downButton addTarget:self action:@selector(downButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _downButton;
}

- (UIProgressView*)progressView {
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] init];
        _progressView.progress = 0.0f;
        _progressView.progressTintColor = [UIColor colorWithRed:137.0/255.0 green:99.0/255.0 blue:200.0/255.0 alpha:1.0];
        _progressView.hidden = YES;
    }
    return _progressView;
}

@end
