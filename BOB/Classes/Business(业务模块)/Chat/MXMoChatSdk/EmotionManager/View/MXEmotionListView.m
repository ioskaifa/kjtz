//
//  MXEmotionListView.m
//  ChatToolBar
//
//  Created by aken on 16/5/5.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXEmotionListView.h"
#import "MXEmotionSection.h"
#import "MXEmotionPackage.h"
#import "MXEmotionDownload.h"
#import "MXEmotionExManager.h"

#import "MXEmotionListCell.h"
#import "SSZipArchive.h"

@interface MXEmotionListView()<UITableViewDelegate,UITableViewDataSource,MXEmotionListCellDelegate,IMXEmotionDownloadDelegate>

// 列表
@property (nonatomic, strong) UITableView            *tableView;
@property (nonatomic, strong) NSArray                *dataSource;
@property (nonatomic)   NSInteger clickIndex;

@end

@implementation MXEmotionListView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI {
    
    [self addSubview:self.tableView];
    
}

- (void)dealloc {
    [[MXEmotionDownload sharedInstance] removeDownloadDelegate:self];
}

#pragma mark - Public 
- (void)reloadData:(NSArray*)array {
    
    NSArray *downArray = [[MXEmotionDownload sharedInstance] downloadArray];
    
    if (downArray && downArray.count > 0) {
        
        // background有下载，则重新添加回调下载进度
        [[MXEmotionDownload sharedInstance] addDownloadDelegate:self];
        
        // 暂时只支持一个section
        for (MXEmotionSection *section in array) {
    
            NSMutableArray *packageArray = [NSMutableArray arrayWithArray:section.emoticionPackages];

            for (NSString *url in downArray) {
                
                for (MXEmotionPackage * model in packageArray) {
                    if ([model.fileUrl isEqualToString:[url lastPathComponent]]) {
                        model.downloading = YES;
                    }
                }
            }
        
            
            section.emoticionPackages = packageArray;
        }
        
    }
   
    self.dataSource = array;
    
    [self.tableView reloadData];
}

#pragma mark - Private
- (void)didDownloadEmotion:(MXEmotionPackage *)model indexRowAt:(NSInteger)index {
    
    NSString *path = @"";//[NSString stringWithFormat:@"%@/%@",kEmotionPackageRemoteUrl,model.fileUrl];
    
    id<IMXEmotionDownloadDelegate> progress = self;
    
    [[MXEmotionDownload sharedInstance] downloadEmotionDownWithUrl:path
           userInfos:@{@"index":@(index),
                       MXEmotionDownloadEmotionCover:model.packageCover} progress:progress];
}

- (NSString*)stringByDeletingExtension:(NSString*)filePath {
    
    NSString *fileName = nil;
    
    if (filePath) {
        
        if ([filePath rangeOfString:@"."].location !=NSNotFound) {
            
            NSArray *tmpArray = [filePath componentsSeparatedByString:@"."];
            fileName = tmpArray[0];
        }
    }
    
    return fileName;
}

- (void)setProgress:(float)progress downloadIndex:(NSInteger)index {
    
    MLog(@"progress_index_%@:%f",@(index),progress);
    
    MXEmotionSection *supModel = self.dataSource[0];
    MXEmotionPackage *model = supModel.emoticionPackages[index];
    model.downloading = YES;
    model.progress = progress;
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        MXEmotionListCell *cell = (MXEmotionListCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        [cell reloadProgress:progress];
    });
}
- (void)finishDownloadWithSuccess:(BOOL)success downloadIndex:(NSInteger)index {
    
    if (success) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            MXEmotionSection *supModel = self.dataSource[0];
            MXEmotionPackage *model = supModel.emoticionPackages[index];
            model.downloading = NO;
            model.completed = YES;
            model.progress = 1.0;
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        });

        [self didFinishDownloadEmotionPackage];
        
        
    }else {
        
         dispatch_async(dispatch_get_main_queue(), ^{
            MXEmotionSection *supModel = self.dataSource[0];
            MXEmotionPackage *model = supModel.emoticionPackages[index];
            model.downloading = NO;
            model.completed = NO;
            model.progress = 0.0;
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
            
            NSString *emotionName = [NSString stringWithFormat:@"未能成功下载“%@”，请重试",model.emotionName];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:emotionName delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
         });
    }
}

#pragma mark - tableview delegate

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    UIView *bgView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 34)];
//    bgView.backgroundColor=[UIColor colorWithRed:247.0/255 green:247.0/255 blue:247.0/255 alpha:1];
//    
//    UILabel *countLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, 7, 200, 20)];
//    countLabel.backgroundColor=[UIColor clearColor];
//    countLabel.font=[UIFont systemFontOfSize:16.0];
//    countLabel.textColor=[UIColor darkGrayColor];
//    [bgView addSubview:countLabel];
//    
//    if (self.dataSource) {
//        
//        MXEmotionSection *emSection = self.dataSource[section];
//        countLabel.text =  emSection.sectionName;
//    }
//    
//    
//    return bgView;
//    
//}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 30;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section < self.dataSource.count) {
    
        MXEmotionSection *subPackages = self.dataSource[section];
        
        return subPackages.emoticionPackages.count;
    }

    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString* leftIdentifier = @"MXEmotionListCell";

    MXEmotionListCell *cell = (MXEmotionListCell *)[tableView dequeueReusableCellWithIdentifier:leftIdentifier];
    if (cell==nil){

        cell = [[MXEmotionListCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:leftIdentifier];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    if (indexPath.section < self.dataSource.count) {
        
        MXEmotionSection *supModel = self.dataSource[indexPath.section];
        MXEmotionPackage *model = supModel.emoticionPackages[indexPath.row];
        
        cell.model = model;
        cell.tag = indexPath.row;
    }
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    //  暂时不跳详情
//    MXEmotionSection *supModel = self.dataSource[indexPath.section];
//    MXEmotionPackage *model = supModel.emoticionPackages[indexPath.row];
//    
//    MXEmotionDetailVC *detailVC = [[MXEmotionDetailVC alloc] init];
//    detailVC.model = model;
//    [[self viewController].navigationController pushViewController:detailVC animated:YES];
    
}

#pragma mark - 代理
- (void)startToDownloadEmotionPackageWithPackage:(MXEmotionPackage *)model indexRowAt:(NSInteger)index {
    
    [self didDownloadEmotion:model indexRowAt:index];
}

- (void)didFinishDownloadEmotionPackage {
    
    if (self.downloadedBlick) {
        self.downloadedBlick();
    }
}

#pragma mark - Getters

- (UITableView*)tableView {
    if (!_tableView) {
        
        _tableView = [[UITableView alloc]initWithFrame:self.bounds];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return _tableView;
}

@end
