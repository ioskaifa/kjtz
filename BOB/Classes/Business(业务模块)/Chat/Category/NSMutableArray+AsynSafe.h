//
//  NSArray+AsynSafe.h
//  MoPal_Developer
//
//  Created by 王 刚 on 16/4/20.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (AsynSafe)
typedef NSObject*(^query)(NSObject *);

-(void)insertObject:(id)anObject queue:(dispatch_queue_t)queue;
-(void)removeObject:(id)anObject queue:(dispatch_queue_t)queue;
-(void)removeAllObjectsInqueue:(dispatch_queue_t)queue;
@end
