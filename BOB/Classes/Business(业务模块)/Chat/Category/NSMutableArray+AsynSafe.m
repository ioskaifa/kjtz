//
//  NSArray+AsynSafe.m
//  MoPal_Developer
//
//  Created by 王 刚 on 16/4/20.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "NSMutableArray+AsynSafe.h"

@implementation NSMutableArray (AsynSafe)
-(void)insertObject:(id)anObject queue:(dispatch_queue_t)queue{
    dispatch_barrier_async(queue, ^(){
        [self addObject:anObject];
    });
}
-(void)removeObject:(id)anObject queue:(dispatch_queue_t)queue{
    dispatch_barrier_async(queue, ^(){
        if (anObject) {
            [self removeObject:anObject];
        }

    });
}

-(void)removeAllObjectsInqueue:(dispatch_queue_t)queue{
    dispatch_barrier_async(queue, ^(){
        [self removeAllObjects];
    });
}


@end
