//
//  ManageAuthUserListCell.m
//  BOB
//
//  Created by mac on 2020/8/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ManageAuthUserListCell.h"

@interface ManageAuthUserListCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *telLbl;

@end

@implementation ManageAuthUserListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 135;
}

- (void)configureView:(ManageAuthUserListObj *)obj {
    if (![obj isKindOfClass:ManageAuthUserListObj.class]) {
        return;
    }
    
    self.nameLbl.text = obj.nick_name;
    self.telLbl.text = obj.user_tel;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.head_photo] placeholderImage:[UIImage defaultAvatar]];
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    [layout setViewCornerRadius:5];
    layout.myTop = 10;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 0;
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myHeight = 40;
    subLayout.myBottom = 0;
    [layout addSubview:subLayout];
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.font = [UIFont font15];
    lbl.text = @"个人名片";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 15;
    [subLayout addSubview:lbl];
    
    UIView *view = [UIView new];
    view.myHeight = 1;
    view.weight = 1;
    [subLayout addSubview:view];
    
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"icon_message"];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myRight = 15;
    [subLayout addSubview:imgView];
    
    [layout addSubview:[UIView line]];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.weight = 1;
    subLayout.myTop = 0;
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myBottom = 0;
    [layout addSubview:subLayout];
    
    [subLayout addSubview:self.imgView];
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.weight = 1;
    rightLayout.myLeft = 15;
    rightLayout.myRight = 15;
    rightLayout.myCenterY = 0;
    rightLayout.myHeight = MyLayoutSize.wrap;
    [rightLayout addSubview:self.nameLbl];
    [rightLayout addSubview:self.telLbl];
    
    [subLayout addSubview:rightLayout];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.size = CGSizeMake(46, 46);
            object.mySize = object.size;
            [object setCircleView];
            object.myCenterY = 0;
            object.myLeft = 15;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor blackColor];
            object.font = [UIFont font15];
            object.myHeight = 20;
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)telLbl {
    if (!_telLbl) {
        _telLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor blackColor];
            object.font = [UIFont font15];
            object.myTop = 10;
            object.myHeight = 20;
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _telLbl;
}

@end
