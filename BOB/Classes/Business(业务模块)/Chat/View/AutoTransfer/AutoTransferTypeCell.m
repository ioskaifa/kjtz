//
//  AutoTransferTypeCell.m
//  BOB
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "AutoTransferTypeCell.h"
#import "MXChatManager.h"

@interface AutoTransferTypeCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *textBnt;
@property (weak, nonatomic) IBOutlet UIButton *voiceBnt;
@property (weak, nonatomic) IBOutlet UIButton *pictureBnt;
@property (weak, nonatomic) IBOutlet UIButton *fileBnt;
@property (weak, nonatomic) IBOutlet UIButton *emojiBnt;

@property(nonatomic, assign) TransferType type;
@end

@implementation AutoTransferTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.top.equalTo(@(18));
    }];
    
    CGFloat width = (SCREEN_WIDTH-30)/3.0;
    [self.textBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.top.equalTo(@(52));
    }];
    
    [self.voiceBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15+width));
        make.top.equalTo(@(52));
    }];
    
    [self.pictureBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15+width*2));
        make.top.equalTo(@(52));
    }];
    
    [self.fileBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.top.equalTo(self.textBnt.mas_bottom).offset(15);
    }];
    
    [self.emojiBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15+width));
        make.top.equalTo(self.textBnt.mas_bottom).offset(15);
    }];
    
    [self.textBnt addTarget:self action:@selector(bntClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.voiceBnt addTarget:self action:@selector(bntClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.pictureBnt addTarget:self action:@selector(bntClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.fileBnt addTarget:self action:@selector(bntClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.emojiBnt addTarget:self action:@selector(bntClick:) forControlEvents:UIControlEventTouchUpInside];
    
    self.textBnt.tag = TransferTypeText;
    self.voiceBnt.tag = TransferTypeVoice;
    self.pictureBnt.tag = TransferTypePicture;
    self.fileBnt.tag = TransferTypeFiles;
    self.emojiBnt.tag = TransferTypeTextEmoji;
    
    self.type = [MXChatManager sharedInstance].autoTransferModel.transferType;
    self.textBnt.selected = self.type & TransferTypeText;
    self.voiceBnt.selected = self.type & TransferTypeVoice;
    self.pictureBnt.selected = self.type & TransferTypePicture;
    self.fileBnt.selected = self.type & TransferTypeFiles;
    self.emojiBnt.selected = self.type & TransferTypeTextEmoji;
}

- (void)bntClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if(sender.selected) {
        self.type |= sender.tag;
    } else {
        self.type ^= sender.tag;
    }
    
    Block_Exec(self.block,@(self.type));
}
@end
