//
//  AutoTransferStatusCell.m
//  BOB
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "AutoTransferStatusCell.h"
#import "MXChatManager.h"

@interface AutoTransferStatusCell ()
@property (weak, nonatomic) IBOutlet UISwitch *switchBnt;

@end

@implementation AutoTransferStatusCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.contentView insertSubview:self.switchBnt atIndex:0];
    [self.switchBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-15));
        make.centerY.equalTo(self.contentView.mas_centerY);
        //make.width.equalTo(@(37));
        //make.height.equalTo(@(20));
    }];
    
    self.switchBnt.transform = CGAffineTransformMakeScale(0.75, 0.75);
    self.switchBnt.onTintColor = [UIColor themeColor];
    
    self.textLabel.text = Lang(@"转发状态");
    self.textLabel.font = [UIFont systemFontOfSize:15];
    
    [self.switchBnt setOn:[MXChatManager sharedInstance].autoTransferModel.transferStatus];
}

- (IBAction)switchBncClick:(id)sender {
    Block_Exec(self.block,@(self.switchBnt.isOn));
}

@end
