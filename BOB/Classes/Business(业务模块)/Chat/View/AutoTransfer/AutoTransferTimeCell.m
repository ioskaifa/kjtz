//
//  AutoTransferTimeCell.m
//  BOB
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "AutoTransferTimeCell.h"
#import "MXChatManager.h"

@interface AutoTransferTimeCell ()
@property (weak, nonatomic) IBOutlet UIButton *allDayBnt;
@property (weak, nonatomic) IBOutlet UIButton *customBnt;
@end

@implementation AutoTransferTimeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.textLabel.text = Lang(@"转发时间");
    self.textLabel.font = [UIFont systemFontOfSize:15];
    self.allDayBnt.tag = 1000;
    self.customBnt.tag = 1001;
    
    [self.customBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-15));
        make.centerY.equalTo(self.contentView);
    }];
    
    [self.allDayBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.customBnt.mas_left).offset(-30);
        make.centerY.equalTo(self.contentView);
    }];
    
    [self.customBnt addTarget:self action:@selector(bntClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.allDayBnt addTarget:self action:@selector(bntClick:) forControlEvents:UIControlEventTouchUpInside];
    
    if([MXChatManager sharedInstance].autoTransferModel.transferTime) {
        self.customBnt.selected = YES;
        self.allDayBnt.selected = NO;
    } else {
        self.customBnt.selected = NO;
        self.allDayBnt.selected = YES;
    }
}

- (void)bntClick:(UIButton *)sender {
    self.customBnt.selected = NO;
    self.allDayBnt.selected = NO;
    sender.selected = YES;
    Block_Exec(self.block,@(sender.tag-1000));
}

- (void)hideAllBnt {
    self.customBnt.hidden = YES;
    self.allDayBnt.hidden = YES;
}
@end
