//
//  AutoTransferNormalCell.h
//  BOB
//
//  Created by mac on 2019/8/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AutoTransferNormalCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

- (void)reloadUI:(NSString *)url title:(NSString *)title;
@end

NS_ASSUME_NONNULL_END
