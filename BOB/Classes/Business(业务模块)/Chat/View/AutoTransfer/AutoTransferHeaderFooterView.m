//
//  AutoTransferHeaderFooterView.m
//  BOB
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "AutoTransferHeaderFooterView.h"

@implementation AutoTransferHeaderFooterView

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if(self = [super initWithReuseIdentifier:reuseIdentifier]) {
        self.textLbl = [[UILabel alloc] init];
        self.textLbl.frame = CGRectMake(15, 0, SCREEN_WIDTH-15, 38);
        self.textLbl.font = [UIFont systemFontOfSize:14];
        self.textLbl.textColor = [UIColor colorWithHexString:@"#323333"];
        [self.contentView addSubview:self.textLbl];
        
        self.detailBnt = [[UIButton alloc] init];
        self.detailBnt.frame = CGRectMake(0, 0, SCREEN_WIDTH-15, 38);
        [self.detailBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        self.detailBnt.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.detailBnt setTitleColor:[UIColor colorWithHexString:@"#1DAAAC"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.detailBnt];
        [self.detailBnt addTarget:self action:@selector(bntClick) forControlEvents:UIControlEventTouchUpInside];
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)bntClick {
    Block_Exec(self.block,self.detailBnt);
}
@end
