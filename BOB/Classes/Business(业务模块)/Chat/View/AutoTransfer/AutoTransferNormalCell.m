//
//  AutoTransferNormalCell.m
//  BOB
//
//  Created by mac on 2019/8/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "AutoTransferNormalCell.h"
#import "XJGroupChatPhoto.h"

@implementation AutoTransferNormalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.centerY.equalTo(self.contentView);
        make.width.height.equalTo(@(40));
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imgView.mas_right).offset(15);
        make.centerY.equalTo(self.contentView);
        make.height.equalTo(@(40));
    }];
}

- (void)reloadUI:(NSString *)url title:(NSString *)title {
    self.titleLbl.text = title;
    if(url == nil) {
        self.imgView.image = nil;
        return;
    }
    NSArray *urls = [url componentsSeparatedByString:@","];
    if(urls.count >= 3) {
        [XJGroupChatPhoto createGroupChatPhotoWithObjs:urls complete:^(UIImage *image) {
            self.imgView.image = image;
        }];
    } else {
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:url] completed:nil];
    }
}
@end
