//
//  AutoTransferStatusCell.h
//  BOB
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AutoTransferStatusCell : UITableViewCell
@property(nonatomic, copy) FinishedBlock block;
@end

NS_ASSUME_NONNULL_END
