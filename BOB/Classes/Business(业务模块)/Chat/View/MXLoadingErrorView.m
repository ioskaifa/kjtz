//
//  MXLoadingErrorView.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/5/5.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "MXLoadingErrorView.h"

@interface MXLoadingErrorView ()

@end

@implementation MXLoadingErrorView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        self.backgroundColor = [UIColor moBackground];
    }
    return self;
}

- (void)setImageWithName:(NSString*)image withText:(NSString*)text
{
    if (image) {
        [_logoImgView setImage:[UIImage imageNamed:image]];
    }
    
    _messageLbl.text = text;
}

- (void)show {
    self.alpha = 0.0;
    [UIView animateWithDuration:0.25 animations:^{
        self.hidden = NO;
        self.alpha = 1.0;
    }];
}

#pragma mark - 布局
- (void)layoutView
{
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_top).offset(150);
    }];
    
    [self.messageLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(10);
        make.right.equalTo(self.mas_right).offset(-1);
        make.top.equalTo(self.logoImgView.mas_bottom).offset(10);
    }];
}

#pragma mark - UI
- (void)initUI
{
    [self addSubview:self.logoImgView];
    [self addSubview:self.messageLbl];
    [self layoutView];
}

- (UIImageView *)logoImgView
{
    if (!_logoImgView) {
        _logoImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"register_infos_tips"]];
    }
    
    return _logoImgView;
}

- (UILabel *)messageLbl
{
    if (!_messageLbl) {
        _messageLbl = [[UILabel alloc] init];
        [_messageLbl setTextAlignment:NSTextAlignmentCenter];
        _messageLbl.text = @"";
        _messageLbl.numberOfLines = 0;
        //yangjiale
        _messageLbl.font = [UIFont font14];
        [_messageLbl setTextColor:[UIColor moDarkGray]];
        //end
    }
    
    return _messageLbl;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    [self.logoImgView setCenter:CGPointMake(self.center.x, 150)];
    [self.messageLbl setFrame:CGRectMake(10, CGRectGetMaxY(self.logoImgView.frame) + 5, SCREEN_WIDTH - 20, CGRectGetHeight(self.messageLbl.frame))];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.touchView) {
        self.touchView();
    }
}

@end
