//
//  MoreSeachCell.m
//  Lcwl
//
//  Created by mac on 2018/12/10.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "MoreSeachView.h"
static int avatarWidthHeight = 40;
@interface MoreSeachView ()
@property (nonatomic, strong) UIImageView *logoImgView;
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UILabel *descLbl;
@property (nonatomic, strong) UIImageView *sexImgView;
@property (nonatomic, strong) MXSeparatorLine* line;
@end

@implementation MoreSeachView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self layoutIfNeeded];
        [self updateConstraintsIfNeeded];
    }
    return self;
}
#pragma mark - UI
- (void)initUI
{
    self.backgroundColor = [UIColor whiteColor];
    _logoImgView.layer.masksToBounds = YES;
    _logoImgView.layer.cornerRadius =  5;
    
    [self addSubview:self.logoImgView];
    [self addSubview:self.sexImgView];
    [self addSubview:self.nameLbl];
    [self addSubview:self.descLbl];
  
    _line =[MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self addSubview:_line];
    @weakify(self)
    [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.nameLbl.mas_left);
        make.right.equalTo(self.mas_right);
        make.bottom.equalTo(self.mas_bottom).offset(-1);
        make.height.equalTo(@(1));
        
    }];
    [self layoutView];
}
- (UIImageView *)logoImgView
{
    if (!_logoImgView) {
        _logoImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
    }
    
    return _logoImgView;
}

- (UIImageView *)sexImgView
{
    if (!_sexImgView) {
        _sexImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _sexImgView.image = [UIImage imageNamed:@"ic_female"];
    }
    
    return _sexImgView;
}
- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLbl.text = @"主标题";
    }
    
    return _nameLbl;
}
- (UILabel *)descLbl
{
    if (!_descLbl) {
        _descLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _descLbl.font = [UIFont font12];
        _descLbl.text = @"子标题";
    }
    
    return _descLbl;
}
- (void)layoutView
{
   
    @weakify(self)
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(10);
        make.centerY.equalTo(self.mas_centerY);
        make.width.height.equalTo(@(avatarWidthHeight));
    }];

}


-(void)reloadModel:(FriendModel *)model{
    [self.logoImgView sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    self.nameLbl.text = model.name;
    self.descLbl.text = model.remark;
    if ([StringUtil isEmpty:self.descLbl.text]) {
        @weakify(self)
        CGSize size = [self.nameLbl sizeThatFits:CGSizeMake(200, 30)];
        
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.logoImgView.mas_right).offset(10);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(size.width));
            make.height.equalTo(@(20));
        }];
        self.descLbl.hidden = YES;
    }else{
        CGSize size = [self.nameLbl sizeThatFits:CGSizeMake(200, 30)];

        @weakify(self)
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.logoImgView.mas_right).offset(10);
            make.centerY.equalTo(self.mas_centerY).offset(-10);
            make.width.equalTo(@(size.width+5));
            make.height.equalTo(@(20));
            
        }];
       
        [self.descLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.nameLbl.mas_left);;
            make.right.equalTo(self.mas_right);;
            make.centerY.equalTo(self.mas_centerY).offset(10);
            make.height.equalTo(@(20));
        }];
        [self.sexImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.nameLbl.mas_right).offset(5);
            make.centerY.equalTo(self.nameLbl.mas_centerY);
            make.width.equalTo(@(15));
            make.height.equalTo(@(15));
        }];
         self.descLbl.hidden = NO;
    }
    if (model.gender != 0) {
        self.sexImgView.hidden = YES;
    }else{
         self.sexImgView.hidden = NO;
    }
}
@end
