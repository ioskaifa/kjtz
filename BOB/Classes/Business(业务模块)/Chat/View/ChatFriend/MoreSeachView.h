//
//  MoreSeachCell.h
//  Lcwl
//
//  Created by mac on 2018/12/10.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MoreSeachView : UIView
-(void)reloadModel:(FriendModel *)model;
@end

NS_ASSUME_NONNULL_END
