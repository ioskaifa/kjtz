//
//  FriendListView.h
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^SelectRowBlock)(UIView* view);
typedef NS_ENUM(NSInteger, RowStyle) {
    RowStyleDefault = 0,
    RowStyleSubtitle = 1,
    RowStyleButton = 2,
    RowStyleRightLabel

};

typedef NS_ENUM(NSInteger, SelectGroupType) {
    SelectGroupTypeNone = 0,
    SelectGroupTypeSingle = 1,
    SelectGroupTypeMulti = 2,
};

@interface FriendListView : UIView
@property(nonatomic , strong) SelectRowBlock selectCallback;
@property(nonatomic , strong) SelectRowBlock rightBtnClickCallback;
@property (nonatomic, strong) UIImageView *logoImgView;
@property (nonatomic, strong) UILabel *nameLbl;
@property (nonatomic, strong) UILabel *descLbl;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, strong) UILabel *rightLabel;
@property (nonatomic, assign) SelectGroupType selectGroupType;
@property (nonatomic, strong) UIButton *selectBnt;

-(instancetype)initRowStyleDefault;

-(void)style:(RowStyle)style;

-(void)setShowDownLine:(BOOL)isShow;

-(void)setShowArrow:(BOOL)isShow;

-(void)reloadLogo:(NSString *)logo name:(NSString* )name;

-(void)reloadLogo:(NSString *)logo name:(NSString* )name desc:(NSString* )desc;

-(void)reloadLogo:(NSString *)logo attrName:(NSMutableAttributedString* )name ;

-(void)reloadLogo:(NSString *)logo attrName:(NSMutableAttributedString* )name attrDesc:(NSMutableAttributedString* )desc;

-(void)reloadLogo:(NSString *)logo attrName:(NSMutableAttributedString* )name attrDesc:(NSMutableAttributedString* )desc rightTip:(NSString* )tip;

-(void)reloadLogo:(NSString *)logo name:(NSString* )name desc:(NSString* )desc btntitle:(NSString *)btntitle;

-(void)reloadLocalLogo:(NSString *)logo name:(NSString* )name;

-(void)reloadLocalLogo:(NSString *)logo name:(NSString* )name desc:(NSString* )desc;

-(void)reloadLocalLogo:(NSString *)logo attrDesc:(NSString* )name;

@end

NS_ASSUME_NONNULL_END
