//
//  MoAttentionStatusView.m
//  MoPal_Developer
//
//  Created by Fly on 15/10/17.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "MoAttentionStatusView.h"
#import "UIButton+ActionBlock.h"
#import "UIView+Utils.h"

@interface MoAttentionStatusView()

@property (nonatomic, strong) UIButton *attentionButton;
@property (nonatomic, strong) UILabel  *statusLabel;
@property (nonatomic, readwrite, assign) MoRelationshipType currentStatus;

@end

@implementation MoAttentionStatusView

- (void)setupSubViews{
    [self addSubview:self.attentionButton];
    [self addSubview:self.statusLabel];
    self.layer.borderColor = RGB(138, 100, 204).CGColor;
    self.layer.borderWidth = 0.5f;
    [self setViewCornerRadius:4.0f];
    self.currentStatus = MoRelationshipTypeNone;

//    [self.attentionButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(@(UIEdgeInsetsZero));
//    }];
//    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(@(UIEdgeInsetsZero));
//    }];
}


- (void)refreshAttentionStatusView:(MoRelationshipType)status{
    self.currentStatus = status;
}

- (void)setCurrentStatus:(MoRelationshipType)currentStatus{
    if (_currentStatus == currentStatus) {
        return;
    }
    _currentStatus = currentStatus;
    BOOL isEnableAttention = NO;
    NSString *statusString = @"";
    UIColor *showColor = RGB(205, 205, 205);
    switch (currentStatus) {
        case MoRelationshipTypeNone:
            self.hidden = YES;
            return;
            break;
        case MoRelationshipTypeMyFriend:
            statusString = MXLang(@"TopicLikePeople_FollowEachOther", @"互相关注");
            break;
//        case MoRelationshipTypeMyFans:
//        case MoRelationshipTypeStranger:
//            showColor = RGB(138, 100, 204);
//            isEnableAttention = YES;
//            statusString = MXLang(@"TopicLikePeople_ToFollow", @"+ 关注");
//            break;
//        case MoRelationshipTypeMyFollowing:
//            statusString = MXLang(@"TopicLikePeople_Followed", @"已关注");
//            break;
        default:
            self.hidden = YES;
            break;
    }
    self.hidden = NO;
    self.statusLabel.text = statusString;
    self.layer.borderColor = showColor.CGColor;
    self.statusLabel.textColor = showColor;
    self.attentionButton.hidden = !isEnableAttention;
}

#pragma makr - Custom Accessors
- (UIButton *)attentionButton{
    if (!_attentionButton) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        @weakify(self)
        [button addAction:^(UIButton *btn) {
            @strongify(self)
            if (self.attentionBlock) {
                self.attentionBlock(self.currentStatus);
            }
        }];
        _attentionButton = button;
    }
    
    return _attentionButton;
}

- (UILabel *)statusLabel
{
    if (!_statusLabel) {
        UILabel* label = [[UILabel alloc]init];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor blackColor];
        label.font = [UIFont font14];
        label.textAlignment = NSTextAlignmentCenter;
        
        _statusLabel = label;
    }
    return _statusLabel;
}

@end
