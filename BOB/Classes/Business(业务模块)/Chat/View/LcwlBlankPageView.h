//
//  LcwlBlankPageView.h
//  Lcwl
//
//  Created by mac on 2018/12/10.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, LcwlBlankPageType){
    LcwlBlankPageMomentsVCType,               //无聊天记录
    LcwlBlankPageMobileContactVCType,
    LcwlBlankPageNewFriendVCVCType,
    LcwlBlankRecentlyGroupListVCType,
    LcwlBlankRecentlyHDTZVCype
    
};
@interface LcwlBlankPageView : UIView
+ (void)addBlankPageView:(LcwlBlankPageType)type
           withSuperView:(UIView*)superView
            touchedBlock:(void(^)(void))blankViewTouchedBlock;
+ (void)removeFromSuperView:(UIView*)superView;
@end

NS_ASSUME_NONNULL_END
