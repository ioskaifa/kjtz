//
//  AddFriendTopView.m
//  Lcwl
//
//  Created by mac on 2018/11/22.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "AddFriendTopView.h"
#import "MXSeparatorLine.h"
#import "SPButton.h"

@interface AddFriendTopView ()
///搜索框
@property (nonatomic, strong) MyLinearLayout *searchLayout;
///无网提示
@property (nonatomic, strong) MyLinearLayout *notReachableLayout;

@end

@implementation AddFriendTopView

- (instancetype)init {
    if (self = [super init]) {
        [self creaetUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 54;
}

- (void)configureStatus:(NSInteger)status {
    if (status == 1) {
        self.searchLayout.visibility = MyVisibility_Visible;
        self.notReachableLayout.visibility = MyVisibility_Invisible;
    } else {
        self.searchLayout.visibility = MyVisibility_Invisible;
        self.notReachableLayout.visibility = MyVisibility_Visible;
    }
}

- (void)creaetUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    [layout addSubview:self.searchLayout];
    [layout addSubview:self.notReachableLayout];
    
    [self configureStatus:1];
}

- (UILabel *)placeholderLbl {
    if (!_placeholderLbl) {
        _placeholderLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font14];
            object.textColor = [UIColor colorWithHexString:@"#999999"];
            object.text = @"搜索昵称/手机号";
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 10;
            object;
        });
    }
    return _placeholderLbl;
}

- (MyLinearLayout *)searchLayout {
    if (!_searchLayout) {
        _searchLayout = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            object.backgroundColor = [UIColor whiteColor];
            [object setViewCornerRadius:17];
            object.myCenterY = 0;
            object.myLeft = 5;
            object.myRight = 5;
            object.myHeight = 34;
            
            UIImageView *imgView = [UIImageView new];
            imgView.image = [UIImage imageNamed:@"im_search_icon"];
            [imgView sizeToFit];
            imgView.mySize = imgView.size;
            imgView.myCenterY = 0;
            imgView.myLeft = 20;
            [object addSubview:imgView];
            [object addSubview:self.placeholderLbl];
                        
            object;
        });
    }
    return _searchLayout;
}

- (MyLinearLayout *)notReachableLayout {
    if (!_notReachableLayout) {
        _notReachableLayout = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            object.backgroundColor = [UIColor colorWithHexString:@"#FD6E81"];
            [object setViewCornerRadius:5];
            object.myCenterY = 0;
            object.myLeft = 5;
            object.myRight = 5;
            object.myHeight = 34;
            
            UIImageView *imgView = [UIImageView new];
            imgView.image = [UIImage imageNamed:@"nim_g_ic_failed_small"];
            [imgView sizeToFit];
            imgView.mySize = imgView.size;
            imgView.myCenterY = 0;
            imgView.myLeft = 10;
            [object addSubview:imgView];
            
            UILabel *lbl = [UILabel new];
            lbl.textColor = [UIColor whiteColor];
            lbl.font = [UIFont font15];
            lbl.text = @"当前网络不可用,请检查你的网络设置";
            [lbl sizeToFit];
            lbl.mySize = lbl.size;
            lbl.myCenterY = 0;
            lbl.myLeft = 10;
            [object addSubview:lbl];
                        
            object;
        });
    }
    return _notReachableLayout;
}

@end
