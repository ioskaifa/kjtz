//
//  MoAttentionStatusView.h
//  MoPal_Developer
//
//  Created by Fly on 15/10/17.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "MPBaseView.h"

typedef void(^AttentionButtonBlock)(MoRelationshipType status);

@interface MoAttentionStatusView : MPBaseView

@property(nonatomic, readonly, assign) MoRelationshipType currentStatus;

@property (nonatomic, copy  ) AttentionButtonBlock  attentionBlock;

- (void)refreshAttentionStatusView:(MoRelationshipType)status;

@end
