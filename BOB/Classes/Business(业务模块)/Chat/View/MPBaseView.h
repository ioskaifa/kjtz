//
//  MPBaseView.h
//  MoPal_Developer
//
//  Created by Fly on 15/8/12.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Frame.h"
#import "NSObject+CallBackBlock.h"

@interface MPBaseView : UIView

@property (weak, nonatomic) UIViewController* dependVC;

+ (instancetype)viewWithNibIndex:(NSInteger)index;

//init 和 initWithFrame会自动调用
- (void)setupSubViews;
- (void)configViewWithModel:(id)obj;

+ (CGFloat)getViewHeight;
- (CGFloat)getViewHeight;

@end
