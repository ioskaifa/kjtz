//
//  ManageAuthUserListCell.h
//  BOB
//
//  Created by mac on 2020/8/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ManageAuthUserListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface ManageAuthUserListCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(ManageAuthUserListObj *)obj;

@end

NS_ASSUME_NONNULL_END
