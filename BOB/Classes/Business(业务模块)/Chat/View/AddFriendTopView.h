//
//  AddFriendTopView.h
//  Lcwl
//
//  Created by mac on 2018/11/22.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^ClickCallback)(void) ;

@interface AddFriendTopView : UIView

@property (nonatomic, strong) UILabel *placeholderLbl;

@property (nonatomic, copy) ClickCallback  callback;

+ (CGFloat)viewHeight;

- (void)configureStatus:(NSInteger)status;

@end

NS_ASSUME_NONNULL_END
