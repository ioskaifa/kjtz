//
//  MXLoadingErrorView.h
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/5/5.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TouchView)(void);

@interface MXLoadingErrorView : UIView

@property (nonatomic, strong) UIImageView *logoImgView;
@property (nonatomic, strong) UILabel     *messageLbl;
@property (nonatomic, strong) TouchView   touchView;

- (void)initUI;
- (void)layoutView;
- (void)setImageWithName:(NSString *)image withText:(NSString*)text;
- (void)show;

@end
