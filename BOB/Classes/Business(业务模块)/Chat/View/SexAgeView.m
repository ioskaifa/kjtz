//
//  SexAgeView.m
//  MoPal_Developer
//
//  Created by Fly on 15/10/13.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "SexAgeView.h"
#import "UIView+Utils.h"

#define SexAgeMagin  4

@interface SexAgeView()

@property (strong, nonatomic) UILabel       *ageLabel;
@property (strong, nonatomic) UIImageView   *sexImgView;

@end

@implementation SexAgeView

- (void)setupSubViews{
    [self addSubview:self.sexImgView];
    [self addSubview:self.ageLabel];
    self.bounds = CGRectMake(0, 0, 34, 14);
    self.sexImgView.frame = CGRectMake(SexAgeMagin, 2, 10, 10);
    self.ageLabel.frame = CGRectMake(self.sexImgView.right + 2, 0, 0, 0);
    [self setViewCornerRadius:3.0f];
}

- (void)setGenderType:(MoGenderType)genderType
          withUserAge:(NSString*)userAge
        withIsShowAge:(BOOL)isShow{
    //sex的数据值需要修改
    switch (genderType) {
        case MoGenderTypeMale: {
            self.sexImgView.image = [UIImage imageNamed:@"personCenter_infoBoy"];
            self.backgroundColor = RGB(57, 172, 228);
            break;
        }
        case MoGenderTypeFemale: {
            self.sexImgView.image = [UIImage imageNamed:@"personCenter_infoGirl"];
            self.backgroundColor = RGB(247, 118, 161);
            break;
        }
        default:{
            self.sexImgView.image = [UIImage imageNamed:@"personCenter_infoGirl"];
            self.backgroundColor = RGB(247, 118, 161);
            break;
        }
    }
    if (!isShow || !userAge || [StringUtil isEmpty:userAge]) {
        self.width = self.sexImgView.right + SexAgeMagin;
        _ageLabel.hidden = YES;//不要使用self. 有时候不需要生成新的对象
        return;
    }
    self.ageLabel.text = userAge;
    self.ageLabel.hidden = NO;
    [self.ageLabel sizeToFit];
    self.ageLabel.top = (self.height - self.ageLabel.height)/2.0f;
    self.width = self.ageLabel.right + SexAgeMagin;
}

- (void)refreshGenderType:(MoGenderType)genderType{
    //sex的数据值需要修改
    switch (genderType) {
        case MoGenderTypeMale: {
            self.sexImgView.image = [UIImage imageNamed:@"personCenter_infoBoy"];
            self.backgroundColor = RGB(57, 172, 228);
            break;
        }
        case MoGenderTypeFemale: {
            self.sexImgView.image = [UIImage imageNamed:@"personCenter_infoGirl"];
            self.backgroundColor = RGB(247, 118, 161);
            break;
        }
        default:{
            self.sexImgView.image = [UIImage imageNamed:@"personCenter_infoGirl"];
            self.backgroundColor = RGB(247, 118, 161);
            break;
        }
    }
}

- (UILabel *)ageLabel{
    if (!_ageLabel) {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.font = [UIFont font12];
        label.textColor = [UIColor whiteColor];
        
        _ageLabel = label;
    }
    return _ageLabel;
}

- (UIImageView *)sexImgView{
    if (!_sexImgView) {
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        
        _sexImgView = imgView;
    }
    return _sexImgView;
}

@end
