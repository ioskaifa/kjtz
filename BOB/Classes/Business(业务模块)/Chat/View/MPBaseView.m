//
//  MPBaseView.m
//  MoPal_Developer
//
//  Created by Fly on 15/8/12.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "MPBaseView.h"

@implementation MPBaseView

- (void)dealloc{
    MLog(@"dealloc-->%@",NSStringFromClass([self class]));
}

+ (instancetype)viewWithNibIndex:(NSInteger)index{
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    if ([nibs count] <= index) {
        return nil;
    }
    id view = [nibs safeObjectAtIndex:index];
    if (view) {
        [view setupSubViews];
    }
    return view;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupSubViews];
    }
    return self;
}

- (void)setupSubViews{
    MLog(@"%@ setupSubViews",NSStringFromClass([self class]));
}

- (void)configViewWithModel:(id)obj
{
    
}

+ (CGFloat)getViewHeight{
    return [[self viewWithNibIndex:0] getViewHeight];
}
- (CGFloat)getViewHeight{
    return CGRectGetHeight(self.bounds);
}


@end
