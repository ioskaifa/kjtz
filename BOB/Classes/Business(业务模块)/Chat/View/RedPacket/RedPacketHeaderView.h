//
//  RedPacketHeaderView.h
//  Lcwl
//
//  Created by mac on 2019/1/7.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedPacketHeaderView : UIView

-(void)reloadData:(NSDictionary*)dataDict  type:(NSString* )type;

@end

NS_ASSUME_NONNULL_END
