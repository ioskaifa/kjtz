//
//  RedPacketView.m
//  Lcwl
//
//  Created by mac on 2019/1/3.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "RedPacketHeaderView.h"
#import "UserModel.h"
#import "NSDate+Category.h"
#import "LcwlChat.h"

static int padding = 15;
static int logoWidth = 60;
@interface RedPacketHeaderView ()

@property (strong, nonatomic) UIImageView   *logoImg;

@property (strong, nonatomic) UILabel       *nameLbl;

@property (strong, nonatomic) UILabel       *moneyLbl;

@property (strong, nonatomic) UILabel       *numLbl;

@property (strong, nonatomic) UILabel       *numTitleLbl;

@property (strong, nonatomic) UILabel       *luckNumLbl;

@property (strong, nonatomic) UILabel       *luckTitleLbl;

@property (strong, nonatomic) UILabel       *sendTitleLbl;

@end

@implementation RedPacketHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self layout];
    }
    return self;
}

-(void)initUI{
    [self addSubview:self.logoImg];
    [self addSubview:self.nameLbl];
    [self addSubview:self.moneyLbl];
    [self addSubview:self.numLbl];
    [self addSubview:self.numTitleLbl];
    [self addSubview:self.luckNumLbl];
    [self addSubview:self.luckTitleLbl];
    [self addSubview:self.sendTitleLbl];
    self.sendTitleLbl.hidden = YES;
    self.numLbl.hidden       = YES;
    self.numTitleLbl.hidden  = YES;
    self.luckNumLbl.hidden   = YES;
    self.luckTitleLbl.hidden = YES;
    
}

-(void)layout{
    @weakify(self)
    [self.logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.mas_top).offset(45);
        make.centerX.equalTo(self.mas_centerX);
        make.width.and.height.equalTo(@(logoWidth));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.top.equalTo(self.logoImg.mas_bottom).offset(20);
        make.height.equalTo(@(20));
    }];
    [self.moneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.nameLbl.mas_bottom).offset(30);
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.height.equalTo(@(40));
    }];
    [self.numLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.moneyLbl.mas_bottom).offset(20);
        make.centerX.equalTo(self.mas_centerX).multipliedBy(0.5);
        make.height.equalTo(@(25));
        make.width.equalTo(@(100));
    }];
    [self.numTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.numLbl.mas_bottom);
        make.centerX.equalTo(self.numLbl.mas_centerX);
        make.height.equalTo(@(25));
        make.width.equalTo(@(100));
    }];
    [self.luckNumLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.moneyLbl.mas_bottom).offset(20);
        make.centerX.equalTo(self.mas_centerX).multipliedBy(1.5);
        make.height.equalTo(@(25));
        make.width.equalTo(@(100));
    }];
    [self.luckTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.luckNumLbl.mas_bottom);
        make.centerX.equalTo(self.luckNumLbl.mas_centerX);
        make.height.equalTo(@(25));
        make.width.equalTo(@(100));
    }];
    [self.sendTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.moneyLbl.mas_bottom).offset(20);
        make.left.right.equalTo(self);
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@(25));
    }];
    
}

- (UIImageView *)logoImg
{
    if (!_logoImg) {
        _logoImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImg.image = [UIImage imageNamed:@"avatar_default"];
        _logoImg.layer.masksToBounds = YES;
        _logoImg.layer.cornerRadius = 4;
    }
    
    return _logoImg;
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLbl.textColor = [UIColor blackColor];
        _nameLbl.font = [UIFont font19];
        _nameLbl.textAlignment = NSTextAlignmentCenter;
        _nameLbl.text = @"";
    }
    return _nameLbl;
}

- (UILabel *)moneyLbl
{
    if (!_moneyLbl) {
        _moneyLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _moneyLbl.textColor = [UIColor blackColor];
        _moneyLbl.font = [UIFont boldSystemFontOfSize:40];
        _moneyLbl.text = @" ";
        _moneyLbl.textAlignment = NSTextAlignmentCenter;
        _moneyLbl.adjustsFontSizeToFitWidth = YES;
    }
    return _moneyLbl;
}

- (UILabel *)numLbl
{
    if (!_numLbl) {
        _numLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _numLbl.textColor = [UIColor colorWithHexString:@"#949594"];
        _numLbl.font = [UIFont systemFontOfSize:28];
        _numLbl.text = @"18";
         _numLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _numLbl;
}

- (UILabel *)numTitleLbl
{
    if (!_numTitleLbl) {
        _numTitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _numTitleLbl.textColor = [UIColor colorWithHexString:@"#949594"];
        _numTitleLbl.font = [UIFont font14];
        _numTitleLbl.text = @"收到红包";
        _numTitleLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _numTitleLbl;
}

- (UILabel *)luckNumLbl
{
    if (!_luckNumLbl) {
        _luckNumLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _luckNumLbl.textColor = [UIColor colorWithHexString:@"#949594"];
        _luckNumLbl.font =  [UIFont systemFontOfSize:28];
        _luckNumLbl.text = @"2";
        _luckNumLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _luckNumLbl;
}

- (UILabel *)luckTitleLbl
{
    if (!_luckTitleLbl) {
        _luckTitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _luckTitleLbl.textColor = [UIColor colorWithHexString:@"#949594"];
        _luckTitleLbl.font = [UIFont font14];
        _luckTitleLbl.text = @"手气最佳";
        _luckTitleLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _luckTitleLbl;
}

- (UILabel *)sendTitleLbl
{
    if (!_sendTitleLbl) {
        _sendTitleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _sendTitleLbl.textColor = [UIColor lightGrayColor];
        _sendTitleLbl.font = [UIFont font17];
        _sendTitleLbl.text = @"发出红包28个";
        _sendTitleLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _sendTitleLbl;
}

-(void)reloadData:(NSDictionary*)dataDict type:(NSString* )type{
    if ([type isEqualToString:@"1"]) {
        self.sendTitleLbl.hidden = YES;
        self.numLbl.hidden       = NO;
        self.numTitleLbl.hidden  = NO;
        self.luckNumLbl.hidden   = NO;
        self.luckTitleLbl.hidden = NO;
        NSString* luckNum = [NSString stringWithFormat:@"%@",dataDict[@"top"]];
        NSString* totalNum = [NSString stringWithFormat:@"%@",dataDict[@"count"]];
        NSString* totalMoney = [NSString stringWithFormat:@"%@",dataDict[@"total"]];
        if ([StringUtil isEmpty:luckNum]) {
            luckNum = @"0";
        }
        if ([StringUtil isEmpty:totalNum]) {
            totalNum = @"0";
        }
        if ([StringUtil isEmpty:totalMoney]) {
            totalMoney = @"0";
        }
        self.moneyLbl.text = [NSString stringWithFormat:@"%@元",totalMoney];
        self.numLbl.text = totalNum;
        self.luckNumLbl.text = luckNum;
        UserModel* model = [LcwlChat shareInstance].user;
        self.nameLbl.text = [NSString stringWithFormat:@"%@共收到",model.smartName];
        [self.logoImg sd_setImageWithURL:[NSURL URLWithString:model.head_photo] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    }else if([type isEqualToString:@"2"]){
        self.sendTitleLbl.hidden = NO;
        self.numLbl.hidden       = YES;
        self.numTitleLbl.hidden  = YES;
        self.luckNumLbl.hidden   = YES;
        self.luckTitleLbl.hidden = YES;
        NSString* totalMoney = [NSString stringWithFormat:@"%@",dataDict[@"total"]];
        NSString* totalNum = [NSString stringWithFormat:@"%@",dataDict[@"count"]];
        if ([StringUtil isEmpty:totalNum]) {
            totalNum = @"0";
        }
        if ([StringUtil isEmpty:totalMoney]) {
            totalMoney = @"0";
        }
        self.moneyLbl.text = [NSString stringWithFormat:@"%@元",totalMoney];
        self.sendTitleLbl.text = [NSString stringWithFormat:@"发出红包%@个",totalNum];
    }

}
@end
