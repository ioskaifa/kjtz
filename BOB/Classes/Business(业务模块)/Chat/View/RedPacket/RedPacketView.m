//
//  RedPacketView.m
//  Lcwl
//
//  Created by mac on 2019/1/3.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "RedPacketView.h"

#import "NSDate+Category.h"
static int padding = 15;
static int logoWidth = 40;
@interface RedPacketView ()

@property (strong, nonatomic) UILabel       *nameLbl;

@property (strong, nonatomic) UILabel       *timeLbl;

@property (strong, nonatomic) UILabel       *moneyLbl;

@property (strong, nonatomic) UIImageView   *logoImg;

@property (strong, nonatomic) MXSeparatorLine* line;

@property (strong, nonatomic) UILabel       *luckLbl;

@end

@implementation RedPacketView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self layout];
    }
    return self;
}

-(void)initUI{
    [self addSubview:self.logoImg];
    [self addSubview:self.nameLbl];
    [self addSubview:self.timeLbl];
    [self addSubview:self.moneyLbl];
    [self addSubview:self.luckLbl];
    self.line = [MXSeparatorLine initHorizontalLineWidth:SCREEN_WIDTH orginX:0 orginY:0];
    [self addSubview:self.line];
    self.line.hidden = YES;
}

-(void)layout{
    @weakify(self)
    [self.logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(padding);
        make.centerY.equalTo(self.mas_centerY);
        make.width.and.height.equalTo(@(logoWidth));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.logoImg.mas_right).offset(padding);
        make.right.equalTo(self.moneyLbl.mas_left);
        make.top.equalTo(self.logoImg.mas_top);
        make.height.equalTo(@(20));
        
    }];
    [self.moneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        //make.width.equalTo(@(80));
        make.height.equalTo(@(20));
        make.top.equalTo(self.logoImg.mas_top);
        make.right.equalTo(self.mas_right).offset(-padding);
    }];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.nameLbl);
        make.bottom.equalTo(self.logoImg.mas_bottom);
        make.height.equalTo(@(18));
    }];
    [self.luckLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.width.equalTo(@(80));
        make.height.equalTo(@(20));
        make.top.equalTo(self.timeLbl.mas_top);
        make.right.equalTo(self.mas_right).offset(-padding);
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.bottom.equalTo(self.mas_bottom).offset(-1);
        make.height.equalTo(@(1));
    }];
    
  
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLbl.textColor = [UIColor blackColor];
        _nameLbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
        _nameLbl.text = @"沙漠狼";
    }
    return _nameLbl;
}

- (UILabel *)moneyLbl
{
    if (!_moneyLbl) {
        _moneyLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _moneyLbl.textColor = [UIColor blackColor];
        _moneyLbl.font = [UIFont font14];
        _moneyLbl.text = @"0.01";
        _moneyLbl.textAlignment = NSTextAlignmentRight;
    }
    return _moneyLbl;
}

- (UILabel *)timeLbl
{
    if (!_timeLbl) {
        _timeLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _timeLbl.textColor = [UIColor lightGrayColor];
        _timeLbl.font = [UIFont font12];
        _timeLbl.text = @"20:02";
    }
    return _timeLbl;
}

- (UILabel *)luckLbl
{
    if (!_luckLbl) {
        _luckLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _luckLbl.textColor = [UIColor colorWithHexString:@"ECB33F"];
        //ECB33F
        _luckLbl.textAlignment = NSTextAlignmentRight;
        _luckLbl.font = [UIFont font12];
        _luckLbl.text = @"手气最佳";
    }
    return _luckLbl;
}

- (UIImageView *)logoImg
{
    if (!_logoImg) {
        _logoImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImg.image = [UIImage imageNamed:@"im_search_icon"];
        _logoImg.layer.masksToBounds = YES;
        _logoImg.layer.cornerRadius = 4;
        _logoImg.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    return _logoImg;
}

-(void)reloadUserData:(RedPacketUserModel*)model{
    [self.logoImg sd_setImageWithURL:[NSURL URLWithString:model.randomUserAvatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    self.nameLbl.text = model.randomUserName;
    
    NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[model.randomSendTime doubleValue]];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    NSString * dateNow = [formatter stringFromDate:msgDate];
    self.timeLbl.text = dateNow;
    if ([model.randomLuck isEqualToString:@"1"]) {
        self.luckLbl.hidden = NO;
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:@"ic_redpacket_best.png"];
        attachment.bounds = CGRectMake(0, -2, 15, 14);
        NSAttributedString *attributed = [NSAttributedString attributedStringWithAttachment:attachment];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:@"手气最佳"];
        [attString insertAttributedString:attributed atIndex:0];
        self.luckLbl.attributedText = attString;
    }else{
        self.luckLbl.hidden =YES;
    }
    
    self.moneyLbl.text = [NSString stringWithFormat:@"%@%@",model.randomMoney,model.type];
}

-(void)reloadSenderData:(RedPacketSenderModel*)model opType:(NSString* )opType{
    if ([opType isEqualToString:@"1"]) {
        self.nameLbl.text = model.sendUserName;
        NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[model.sendTime doubleValue]];
        NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString * dateNow = [formatter stringFromDate:msgDate];
        self.timeLbl.text = dateNow;
        
        self.moneyLbl.text = [NSString stringWithFormat:@"%@%@",model.randomMoney,model.type];
        if ([model.randomLuck isEqualToString:@"1"]) {
            self.luckLbl.hidden = NO;
        }else{
            self.luckLbl.hidden =YES;
        }
        
        
        @weakify(self)
        [self.logoImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.mas_left).offset(padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.and.height.equalTo(@(0));
        }];
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.logoImg.mas_right);
            make.right.equalTo(self.moneyLbl.mas_left);
            make.centerY.equalTo(self.mas_centerY).offset(-10);
            make.height.equalTo(@(20));
            
        }];
        [self.moneyLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.width.equalTo(@(220));
            make.height.equalTo(@(20));
            make.centerY.equalTo(self.nameLbl);
            make.right.equalTo(self.mas_right).offset(-padding);
        }];
        [self.timeLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.right.equalTo(self.nameLbl);
            make.centerY.equalTo(self.mas_centerY).offset(10);
            make.height.equalTo(@(20));
        }];
        [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.right.equalTo(self);
            make.bottom.equalTo(self.mas_bottom).offset(-1);
            make.height.equalTo(@(1));
        }];
        [self.luckLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.width.equalTo(@(80));
            make.height.equalTo(@(20));
            make.top.equalTo(self.timeLbl.mas_top);
            make.right.equalTo(self.mas_right).offset(-padding);
        }];
    }else if([opType isEqualToString:@"2"]){
        if ([model.redPacketType isEqualToString:@"1"]) {
            self.nameLbl.text = @"拼手气红包";
        }else if([model.redPacketType isEqualToString:@"0"]){
            self.nameLbl.text = @"普通红包";
        }
        
        NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[model.sendTime doubleValue]];
        NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        NSString * dateNow = [formatter stringFromDate:msgDate];
        self.timeLbl.text = dateNow;
        
        self.moneyLbl.text = [NSString stringWithFormat:@"%@%@",model.redPacketMoney,model.type];
        
        self.luckLbl.hidden = YES;
        self.luckLbl.textColor = [UIColor lightGrayColor];
        //self.luckLbl.text = [NSString stringWithFormat:@"%@/%@",model.robbedNum,model.redPacketNum];
        
        
        @weakify(self)
        [self.logoImg mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.mas_left).offset(padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.and.height.equalTo(@(0));
        }];
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.logoImg.mas_right);
            make.right.equalTo(self.moneyLbl.mas_left);
            make.centerY.equalTo(self.mas_centerY).offset(-10);
            make.height.equalTo(@(20));
            
        }];
        [self.moneyLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            //make.width.equalTo(@(80));
            make.height.equalTo(@(20));
            make.centerY.equalTo(self.nameLbl);
            make.right.equalTo(self.mas_right).offset(-padding);
        }];
        [self.timeLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.right.equalTo(self.nameLbl);
            make.centerY.equalTo(self.mas_centerY).offset(10);
            make.height.equalTo(@(20));
        }];
        [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.right.equalTo(self);
            make.bottom.equalTo(self.mas_bottom).offset(-1);
            make.height.equalTo(@(1));
        }];
        [self.luckLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.width.equalTo(@(80));
            make.height.equalTo(@(20));
            make.top.equalTo(self.timeLbl.mas_top);
            make.right.equalTo(self.mas_right).offset(-padding);
        }];
    }
   
}

@end
