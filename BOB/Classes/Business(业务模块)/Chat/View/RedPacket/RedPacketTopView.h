//
//  RedPacketTopView.h
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RedPacketModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RedPacketTopView : UIView

- (void)reloadData:(RedPacketModel *)model;

- (void)stretchTopBG:(CGFloat)contentOffset;

-(void)height:(int)height;
@end

NS_ASSUME_NONNULL_END
