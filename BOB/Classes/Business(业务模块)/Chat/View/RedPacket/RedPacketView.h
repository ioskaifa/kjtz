//
//  RedPacketView.h
//  Lcwl
//
//  Created by mac on 2019/1/3.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RedPacketUserModel.h"
#import "RedPacketSenderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RedPacketView : UIView

-(void)reloadUserData:(RedPacketUserModel*)model;

-(void)reloadSenderData:(RedPacketSenderModel*)model opType:(NSString* )opType;

@end

NS_ASSUME_NONNULL_END
