//
//  RedPacketTopView.m
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "RedPacketTopView.h"
#import "NSMutableAttributedString+Attributes.h"

static int padding = 15;

static int cellPadding = 10;

@interface RedPacketTopView()

@property (nonatomic, strong) UIImageView *topBackImg;

@property (nonatomic, strong) UIImageView *avatarImg;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *descLbl;

@property (nonatomic, strong) UILabel *numLbl;

@property (nonatomic, strong) UILabel *tipLbl;

@property (nonatomic, strong) UILabel *moneyLbl;

@end

@implementation RedPacketTopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self layoutView];
    }
    return self;
}

- (UIImageView *)topBackImg
{
    if (!_topBackImg) {
        _topBackImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        _topBackImg.image =  [UIImage imageNamed:@"redPacket_top"];
        _topBackImg.frame = CGRectMake(0, -1, SCREEN_WIDTH, 270);
        
    }
    return _topBackImg;
}

- (UIImageView *)avatarImg
{
    if (!_avatarImg) {
        _avatarImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        _avatarImg.image = [UIImage imageNamed:@"avatar_default"];
        _avatarImg.contentMode = UIViewContentModeScaleAspectFit;
        _avatarImg.layer.cornerRadius = 4;
        _avatarImg.clipsToBounds = YES;
    }
    return _avatarImg;
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLbl.text = @"";
        _nameLbl.textColor = [UIColor moGolden];
        _nameLbl.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
        _nameLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _nameLbl;
}

- (UILabel *)numLbl
{
    if (!_numLbl) {
        _numLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _numLbl.text = @"";
        _numLbl.textColor = [UIColor blackColor];
        _numLbl.font = [UIFont boldSystemFontOfSize:28];
        _numLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _numLbl;
}

- (UILabel *)tipLbl
{
    if (!_tipLbl) {
        _tipLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _tipLbl.text = @"已存入美元，可用于发红包";
        _tipLbl.textColor = [UIColor blackColor];
        _tipLbl.font = [UIFont font14];
        _tipLbl.textAlignment = NSTextAlignmentCenter;
        _tipLbl.hidden = YES;
    }
    return _tipLbl;
}

- (UILabel *)moneyLbl
{
    if (!_moneyLbl) {
        _moneyLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        //_moneyLbl.text = @"美元";
        _moneyLbl.textColor = [UIColor colorWithHexString:@"#CEB056"];
        _moneyLbl.font = [UIFont systemFontOfSize:16];
        _moneyLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _moneyLbl;
}

- (UILabel *)descLbl
{
    if (!_descLbl) {
        _descLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _descLbl.text = @"";
        _descLbl.textColor = [UIColor moGolden];
        _descLbl.font = [UIFont systemFontOfSize:15];
         _descLbl.textAlignment = NSTextAlignmentCenter;
        
    }
    return _descLbl;
}

-(void)initUI{
    //self.clipsToBounds = YES;
    [self addSubview:self.topBackImg];
    [self addSubview:self.avatarImg];
    [self addSubview:self.nameLbl];
    [self addSubview:self.descLbl];
    [self addSubview:self.numLbl];
    [self addSubview:self.tipLbl];
    [self addSubview:self.moneyLbl];
}

-(void)layoutView{
    @weakify(self)
//    [self.topBackImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self)
//        make.left.right.equalTo(self);
//        make.top.equalTo(@(0));
//        make.height.equalTo(@(200));
//    }];
    [self.avatarImg mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.nameLbl.mas_left).offset(-10);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        make.height.width.equalTo(@(24));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.topBackImg.mas_centerX).offset(20);
        make.top.equalTo(@(40));
        make.height.equalTo(@(24));
    }];
    
    [self.descLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.mas_centerX);
        make.left.right.equalTo(self.topBackImg);
        make.top.equalTo(self.avatarImg.mas_bottom).offset(cellPadding);
        make.height.equalTo(@(15));
    }];
    
    [self.numLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.topBackImg.mas_centerX);
        make.left.right.equalTo(self.topBackImg);
        make.top.equalTo(self.descLbl.mas_bottom).offset(cellPadding);
        make.height.equalTo(@(30));
    }];
    
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.topBackImg.mas_centerX);
        make.left.right.equalTo(self.topBackImg);
        make.top.equalTo(self.numLbl.mas_bottom).offset(cellPadding);
        make.height.equalTo(@(20));
    }];
    
    [self.moneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.topBackImg.mas_centerX);
        //make.left.right.equalTo(self.topBackImg);
        make.top.equalTo(self.descLbl.mas_bottom).offset(50);
        //make.height.equalTo(@(20));
    }];
}

-(void)reloadData:(RedPacketModel *)model{
    [self.avatarImg sd_setImageWithURL:[NSURL URLWithString:model.sendUserAvatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    self.nameLbl.text = [NSString stringWithFormat:@"%@的红包",model.sendUserName];
    self.descLbl.text = model.remark;
//    if ([StringUtil isEmpty:model.randomMoney]) {
//        self.numLbl.text =@"";
//    }else{
//        self.numLbl.text = model.randomMoney;
//    }
   
    if(![StringUtil isEmpty:model.randomMoney]) {
        self.moneyLbl.attributedText = [NSMutableAttributedString initWithTitles:@[model.randomMoney ?: @"",model.type] colors:@[[UIColor blackColor],[UIColor blackColor]] fonts:@[[UIFont boldSystemFontOfSize:55],[UIFont systemFontOfSize:16]]];
    }
}

- (void)stretchTopBG:(CGFloat)contentOffset {
    self.topBackImg.frame = CGRectMake(0, contentOffset, SCREEN_WIDTH, 270-contentOffset);
}


-(void)height:(int)height{
    CGRect rect = self.frame;
    rect.size.height = height;
    self.frame = rect;
    @weakify(self)
    [self.topBackImg mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.top.equalTo(self.mas_top);
        make.height.equalTo(@(60));
    }];
    
    
}
@end

