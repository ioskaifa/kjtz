//
//  PlaceHolderImageManager.h
//  MoPal_Developer
//
//  Created by yangjiale on 24/9/15.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, PlaceHolderImageType){
    PlaceHolderImageTypeUserAvatar  = 0,//用户头像
    PlaceHolderImageTypeGroupAvatar = 1,//群组头像
    PlaceHolderImageTypeBusiAvatar  = 2,//商家头像
    PlaceHolderImageTypeChatImageDownloading = 3,//下载中默认图片
    PlaceHolderImageTypeChatImageDownFailure = 4,//图片下载失败
    PlaceHolderImageTypeMoYaImage =5//魔牙小人图片
};


@interface PlaceHolderImageManager : NSObject

//用户登录头像
+ (UIImage *)getUserLoginAvatarImage:(BOOL)isDefaultAvatar;

+ (UIImage *)imageWithPlaceHolder:(PlaceHolderImageType)PlaceHolderType;

@end
