//
//  MyServiceView.m
//  BOB
//
//  Created by colin on 2020/11/19.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyServiceView.h"
#import "ManageAuthUserListObj.h"
#import "MXChatManager.h"
#import "LcwlChat.h"

@interface MyServiceView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@end

@implementation MyServiceView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
        [self fetchData];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 300;
}

- (void)fetchData {
    NSString *url = @"api/user/info/getMemberServiceList";
    url = StrF(@"%@/%@", LcwlServerRoot2, url);
    NSDictionary *param = @{};
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url);
        net.params(param);
        net.finish(^(id data){
            if ([data isSuccess]) {
                NSArray *dataArr = data[@"data"][@"serviceList"];
                dataArr = [ManageAuthUserListObj modelListParseWithArray:dataArr];
                NSArray *tempArr = [dataArr valueForKeyPath:@"@unionOfObjects.ID"];
                [MXCache setValue:tempArr forKey:kMXMemberServiceList];
                [self.dataSourceMArr removeAllObjects];
                NSArray *fried = [[LcwlChat shareInstance].chatManager friends];
                ///未保存在本地的用户
                NSMutableArray *notFriendMArr = [NSMutableArray array];
                for (ManageAuthUserListObj *obj in dataArr) {
                    if (![obj isKindOfClass:ManageAuthUserListObj.class]) {
                        continue;
                    }
                    //如果是自己则不显示
                    if ([obj.ID isEqualToString:UDetail.user.user_id]) {
                        continue;
                    }
                    FriendModel *model = [FriendModel new];
                    model.userid = obj.ID;
                    model.name = obj.nick_name;
                    model.user_name = obj.nick_name;
                    model.nick_name = obj.nick_name;
                    model.smartName = obj.nick_name;
                    model.head_photo = obj.head_photo;
                    model.avatar = obj.head_photo;
                    [self.dataSourceMArr addObject:model];
                    //判断是否本地好友  不是本地好友 写入本地
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"userid == '%@'", obj.ID)];
                    NSArray *tempArr = [fried filteredArrayUsingPredicate:predicate];
                    if (tempArr.count > 0) {
                        //把备注带上
                        FriendModel *tempObj = tempArr.firstObject;
                        if ([tempObj isKindOfClass:FriendModel.class]) {
                            model.remark = tempObj.remark;
                        }
                        continue;
                    }
                    model.followState = MoRelationshipTypeStranger;
                    [notFriendMArr addObject:model];
                }
                if (notFriendMArr.count > 0) {
                    ///写入数据库 聊天都需要从本地数据库取用户信息
                    [[MXChatManager sharedInstance] insertFriends:notFriendMArr];
                }
                [self.tableView reloadData];
            } else {
                [NotifyHelper showMessageWithMakeText:data[@"msg"]];
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = [UITableViewCell class];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell addBottomSingleLine:[UIColor moBackground]];
    UIImageView *imgView = [cell.contentView viewWithTag:1000];
    if (!imgView) {
        imgView = [UIImageView new];
        imgView.tag = 1000;
        imgView.size = CGSizeMake(40, 40);
        [imgView setViewCornerRadius:imgView.height/2.0];
        [cell.contentView addSubview:imgView];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(imgView.size);
            make.left.mas_equalTo(cell.contentView.mas_left).mas_offset(15);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
        }];
    }
    
    UILabel *lbl = [cell.contentView viewWithTag:1001];
    if (!lbl) {
        lbl = [UILabel new];
        lbl.tag = 1001;
        lbl.font = [UIFont font15];
        lbl.textColor = [UIColor blackColor];
        [cell.contentView addSubview:lbl];
        [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.left.mas_equalTo(imgView.mas_right).mas_offset(15);
            make.right.mas_equalTo(cell.contentView.mas_right).mas_offset(-15);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
        }];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    FriendModel *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:FriendModel.class]) {
        return;
    }
    UIImageView *imgView = [cell.contentView viewWithTag:1000];
    if (imgView) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:StrF(@"%@/%@", UDetail.user.qiniu_domain, obj.head_photo)]];
    }
    
    UILabel *lbl = [cell.contentView viewWithTag:1001];
    if (lbl) {
        lbl.text = obj.nick_name;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    FriendModel *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:FriendModel.class]) {
        return;
    }
    [tableView routerEventWithName:@"MXCustomAlertVCCancel" userInfo:nil];
    [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":obj.userid,@"type":@(eConversationTypeChat),@"creatorRole":@(10)}];
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    [baseLayout setViewCornerRadius:5];
    baseLayout.backgroundColor = [UIColor whiteColor];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"联系客服";
    [lbl autoMyLayoutSize];
    lbl.myTop = 10;
    lbl.myBottom = 10;
    lbl.myCenterX = 0;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:[UIView fullLine]];
    
    [baseLayout addSubview:self.tableView];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        Class cls = [UITableViewCell class];
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.rowHeight = 60;
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

@end
