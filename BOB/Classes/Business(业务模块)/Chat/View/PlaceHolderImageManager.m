//
//  PlaceHolderImageManager.m
//  MoPal_Developer
//
//  Created by yangjiale on 24/9/15.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "PlaceHolderImageManager.h"

@implementation PlaceHolderImageManager


+ (UIImage *)imageWithPlaceHolder:(PlaceHolderImageType)PlaceHolderType
{
    UIImage *image=nil;
    switch (PlaceHolderType) {
        case PlaceHolderImageTypeUserAvatar:
            image = [UIImage imageNamed:@"login_default"];
            break;
        case PlaceHolderImageTypeGroupAvatar:
            image = [UIImage imageNamed:@"group_default"];
            break;
        case PlaceHolderImageTypeBusiAvatar:
            image = [UIImage imageNamed:@""];
            break;
        case PlaceHolderImageTypeChatImageDownloading:
            image = [UIImage imageNamed:@"Motalk_downloading"];
            break;
        case PlaceHolderImageTypeChatImageDownFailure:
            image = [UIImage imageNamed:@"Motalk_downloadFailure"];
            break;
        case PlaceHolderImageTypeMoYaImage:
            image = [UIImage imageNamed:@"login_no_default"];
            break;
        default:
            image = [UIImage imageNamed:@"login_no_default"];
            break;
    }
    
    return image;
}

+ (UIImage *)getUserLoginAvatarImage:(BOOL)isDefaultAvatar
{
    if(isDefaultAvatar){
        return [UIImage imageNamed:@"login_no_default"];
    }else{
        return [UIImage imageNamed:@"login_default"];
    }
}

@end
