//
//  ChatFriendCell.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/4/2.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXSeparatorLine.h"
typedef void(^AvatarClickCallback)(MoYouModel* moyou);
@interface ChatFriendCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (strong, nonatomic) IBOutlet UIView *rightView;
@property (strong, nonatomic) IBOutlet UIImageView *chatIcon;
@property (weak, nonatomic) IBOutlet UIButton *headImgBtn;
@property (strong, nonatomic) MXSeparatorLine* line;

@property (strong, nonatomic) MoYouModel* friend;

@property (nonatomic, strong) AvatarClickCallback callback;

- (IBAction)onClick:(id)sender;
-(void)reloadChatFriend:(MoYouModel*) chatFriend;
+(CGFloat)getHeight;
@end
