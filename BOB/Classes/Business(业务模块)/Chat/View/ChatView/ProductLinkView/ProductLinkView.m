//
//  ProductLinkView.m
//  BOB
//
//  Created by AlphaGo on 2021/1/21.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "ProductLinkView.h"

#import "MXJsonParser.h"
#define VoucherWidth 200
static NSInteger imagewidth = 80;
static NSInteger padding = 10;

@interface ProductLinkView ()
//对话背景
@property (nonatomic, strong) UIImageView *logoImageview;

@property (nonatomic, strong) UILabel      *titleLbl;

@property (nonatomic, strong) UILabel      *subtitleLbl;

@end

@implementation ProductLinkView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

+(CGFloat)height
{
    return 153;
}

#pragma mark - setter

- (void)setParam:(NSDictionary *)param {
    _param = param;
    
    NSDictionary* dict = param;
    NSString *avatar = dict[@"attr2"];
    if (avatar) {
        //avatar = StrF(@"%@/%@", UDetail.user.qiniu_domain, avatar);
        [self.logoImageview sd_setImageWithURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    }
    NSString *name = dict[@"attr3"];
    if (name) {
        self.titleLbl.text = name;
        CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(self.titleLbl.width, MAXFLOAT)];
        self.titleLbl.myHeight = size.height;
    }
    
    NSString *ID = dict[@"attr4"];
    if (ID) {
        self.subtitleLbl.text = [NSString stringWithFormat:@"¥%.2f",[ID doubleValue]];
        CGSize size = [self.subtitleLbl sizeThatFits:CGSizeMake(self.subtitleLbl.width, MAXFLOAT)];
        self.subtitleLbl.myHeight = size.height;
    }
}


- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 10;
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myTop = 0;
    bottomLayout.myLeft = 0;
    bottomLayout.myRight = 0;
    bottomLayout.myHeight = 100;
    [layout addSubview:bottomLayout];
    
    [bottomLayout addSubview:self.logoImageview];
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myLeft = 5;
    rightLayout.myRight = 5;
    rightLayout.weight = 1;
    rightLayout.myHeight = MyLayoutSize.wrap;
    rightLayout.myCenterY = 0;
    [bottomLayout addSubview:rightLayout];
    [rightLayout addSubview:self.titleLbl];
    [rightLayout addSubview:self.subtitleLbl];
    
    UIView *line = [UIView fullLine];
    line.myTop = 0;
    [layout addSubview:line];
    
    UIButton *bnt = [UIButton new];
    bnt.titleLabel.font = [UIFont font15];
    bnt.backgroundColor = [UIColor colorWithHexString:@"#FF4757"];
    [bnt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bnt setTitle:@"发送链接" forState:UIControlStateNormal];
    [bnt setViewCornerRadius:5];
    bnt.mySize = CGSizeMake(110, 33);
    bnt.myTop = 10;
    bnt.myCenterX = 0;
    bnt.myCenterY = 0;
    [layout addSubview:bnt];
    
    [bnt addAction:^(UIButton *btn) {
        Block_Exec(self.sendBlock,self.param);
    }];
    
    UIButton *closeBnt = [UIButton new];
    closeBnt.frame = CGRectMake(SCREEN_WIDTH-20-12*2-5, 5, 20, 20);
    [closeBnt setImage:[UIImage imageNamed:@"Close"] forState:UIControlStateNormal];
    closeBnt.useFrame = YES;
    closeBnt.showsTouchWhenHighlighted = YES;
    [layout addSubview:closeBnt];
    [closeBnt addAction:^(UIButton *btn) {
        [self removeFromSuperview];
    }];;
    
}

-(UIImageView* )logoImageview{
    if (_logoImageview == nil) {
        _logoImageview = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImageview.backgroundColor = [UIColor clearColor];
        [_logoImageview setViewCornerRadius:6];
        _logoImageview.size = CGSizeMake(70, 70);
        _logoImageview.mySize = _logoImageview.size;
        _logoImageview.myCenterY = 0;
        _logoImageview.myLeft = 5;
    }
    return _logoImageview;
}

- (UILabel*)titleLbl {
    
    if (_titleLbl == nil) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _titleLbl.font = [UIFont font14];
        _titleLbl.numberOfLines = 3;
        _titleLbl.backgroundColor = [UIColor clearColor];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _titleLbl.width = SCREEN_WIDTH - self.logoImageview.width - 12*2-10;
        _titleLbl.myWidth = _titleLbl.width;
    }
    
    return _titleLbl;
}

- (UILabel*)subtitleLbl {
    
    if (_subtitleLbl == nil) {
        _subtitleLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _subtitleLbl.font = [UIFont boldSystemFontOfSize:15];
        _subtitleLbl.numberOfLines = 1;
        _subtitleLbl.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
        _subtitleLbl.width = _titleLbl.width;
        _subtitleLbl.myWidth = _subtitleLbl.width;
        _subtitleLbl.myTop = 6;
    }
    
    return _subtitleLbl;
}


@end
