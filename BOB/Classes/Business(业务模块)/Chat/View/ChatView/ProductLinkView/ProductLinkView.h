//
//  ProductLinkView.h
//  BOB
//
//  Created by AlphaGo on 2021/1/21.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductLinkView : UIView
@property(nonatomic,strong) NSDictionary *param;
@property(nonatomic,copy) FinishedBlock sendBlock;
@end

NS_ASSUME_NONNULL_END
