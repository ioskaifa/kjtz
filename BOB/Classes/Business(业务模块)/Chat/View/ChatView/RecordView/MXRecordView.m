//
//  MXRecordView.m
//  ChatToolBar
//
//  Created by aken on 16/5/25.
//  Copyright © 2016年 ChatToolBar. All rights reserved.
//

#import "MXRecordView.h"
#import "MXDeviceMediaManager.h"
#import "MXDeviceManagerRecorder.h"

//#import <MXChatToolBarSdk/MXChatToolBarSdk.h>
#import "MXChatToolBarSdk.h"

#define kMaxRecordChangeWormingTime 50.0f
#define kTimeInterval 1.0//0.05f
#define kTimeIntervalCount 1.0
#define kMaxRecordTime 60

@interface MXRecordView ()
{
    NSTimer *_timer;
    
    // 显示动画的ImageView
    UIImageView *_recordAnimationView;
    
    // 提示文字
    UILabel *_textLabel;
    
    // 时间
    UILabel *_timeLabel;
    
    // 当前计数,初始为0
    //    CGFloat currentCount;
    NSInteger currentCount;
}

@property (nonatomic, assign) BOOL isCancel;

@end

@implementation MXRecordView

- (void)dealloc {
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIView *bgView = [[UIView alloc] initWithFrame:self.bounds];
        //        bgView.backgroundColor = [UIColor grayColor];
        bgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        bgView.layer.cornerRadius = 5;
        bgView.layer.masksToBounds = YES;
        //        bgView.alpha = 0.8;
        [self addSubview:bgView];
        
        // 录制时长
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width/2 - 50,
                                                               10,
                                                               100,
                                                               21)];
        _timeLabel.font = [UIFont systemFontOfSize:14.0];
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.textAlignment=NSTextAlignmentCenter;
        _timeLabel.text = @"0";
        [self addSubview:_timeLabel];
        
        
        // 声音图标图片
        _recordAnimationView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width/2 - 175/2,
                                                                             self.bounds.size.height/2 - 33,
                                                                             175,
                                                                             66)];
        _recordAnimationView.image = [UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback2")];
        [self addSubview:_recordAnimationView];
        
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(5,
                                                               self.bounds.size.height - 30,
                                                               self.bounds.size.width - 10,
                                                               25)];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.text = @"手指上滑 取消发送";
        [self addSubview:_textLabel];
        _textLabel.font = [UIFont systemFontOfSize:14.0];
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.layer.cornerRadius = 5;
        _textLabel.layer.borderColor = [[UIColor redColor] colorWithAlphaComponent:0.5].CGColor;
        _textLabel.layer.masksToBounds = YES;
    }
    return self;
}

// 录音按钮按下
-(void)recordButtonTouchDown
{
    
    // 需要根据声音大小切换recordView动画
    _textLabel.text =  @"手指上滑 取消发送";
    _textLabel.backgroundColor = [UIColor clearColor];
    
    [self startTimer];
}
// 手指在录音按钮内部时离开(确定发语音)
-(void)recordButtonTouchUpInside
{
    _recordAnimationView.frame = CGRectMake(self.bounds.size.width/2 - 175/2,
                                            self.bounds.size.height/2 - 33,
                                            175,
                                            66);
    _textLabel.textColor = [UIColor whiteColor];
    [self disabelTimer];
}
// 手指在录音按钮外部时离开(取消录制)
-(void)recordButtonTouchUpOutside
{
    _recordAnimationView.frame = CGRectMake(self.bounds.size.width/2 - 175/2,
                                            self.bounds.size.height/2 - 33,
                                            175,
                                            66);
    
    [_recordAnimationView setImage:[UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback2")]];
    _textLabel.textColor = [UIColor whiteColor];
    [self disabelTimer];
    
}
// 手指移动到录音按钮内部
-(void)recordButtonDragInside
{
    _textLabel.text = @"手指上滑 取消发送";
    _textLabel.backgroundColor = [UIColor clearColor];
    _textLabel.textColor = [UIColor whiteColor];
    
    _recordAnimationView.frame = CGRectMake(self.bounds.size.width/2 - 175/2,
                                            self.bounds.size.height/2 - 33,
                                            175,
                                            66);
    [_recordAnimationView setImage:[UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback1")]];
    
    
    self.isCancel = NO;
    _timeLabel.hidden = NO;
    
}

// 手指移动到录音按钮外部
-(void)recordButtonDragOutside
{
    _textLabel.text =  @"取消发送";
    _textLabel.textColor=[UIColor redColor];
    
    
    _recordAnimationView.frame = CGRectMake(self.bounds.size.width/2 - 59/2,
                                            self.bounds.size.height/2 - 71/2,
                                            59,
                                            71);
    [_recordAnimationView setImage:nil];
    [_recordAnimationView setImage:[UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_cancel_small_voice")]];
    
    self.isCancel = YES;
    _timeLabel.hidden = YES;
    // 停留在view上,继续录制
    //    [self pauseTimer];
}

// start timer
- (void)startTimer {
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:kTimeInterval
                                              target:self
                                            selector:@selector(setVoiceImage)
                                            userInfo:nil
                                             repeats:YES];
    self.isCancel = NO;
    _timeLabel.hidden = NO;
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

// 停止timer
-(void)disabelTimer {
    [_recordAnimationView setImage:[UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback1")]];
    
    [self pauseTimer];
    currentCount=1;
    self.isCancel = NO;
    
    _timeLabel.text = @"0";
    _timeLabel.hidden = NO;
}

// 暂停
- (void)pauseTimer {
    currentCount=0;
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
}

-(void)setVoiceImage {
    
    
    double voiceSound = 0 ;
    voiceSound = [[[MXDeviceMediaManager sharedInstance] deviceMedia] peekRecorderVoiceMeter];
    
    if (!self.isCancel) {
        _timeLabel.hidden = NO;
        _recordAnimationView.image = [UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback2")];
        if (0 < voiceSound <= 0.05) {
            [_recordAnimationView setImage:[UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback")]];
        }else if (0.05<voiceSound<=0.20) {
            [_recordAnimationView setImage:[UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback1")]];
        }else if (0.20<voiceSound<=0.25) {
            [_recordAnimationView setImage:[UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback2")]];
        }else if (0.85<voiceSound<=0.90) {
            [_recordAnimationView setImage:[UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback3")]];
        }else {
            [_recordAnimationView setImage:[UIImage imageNamed:MXChatToolBarBundle(@"Record/Motalk_message_bottom_voice_feedback4")]];
        }
        
        
        //        _timeLabel.text=[NSString stringWithFormat:@"%0.lf",currentCount];
        if (currentCount >=kMaxRecordChangeWormingTime) {
            _timeLabel.textColor = [UIColor redColor];
            _textLabel.text = @"语音录制不得超过60S哟";
            _textLabel.textColor = [UIColor redColor];
        }else{
            
            
            _timeLabel.textColor = [UIColor whiteColor];
            _textLabel.textColor = [UIColor whiteColor];
        }
    }else {
        _timeLabel.hidden = YES;
    }
    
    
    // 计算时间
    currentCount = [MXDeviceManagerRecorder recordLength];
    int p = currentCount;
    printf("切换失败! %d",p);
    _timeLabel.text=[NSString stringWithFormat:@"%ld",(long)currentCount];
    
    // 当前录音时长大于最大时长时，则停止录音
    if (currentCount>=kMaxRecordTime) {
        _timeLabel.text=[NSString stringWithFormat:@"%d",kMaxRecordTime];
        [[NSNotificationCenter defaultCenter] postNotificationName:kRecordNotificationMaxWorming object:nil];
        _timeLabel.textColor = [UIColor whiteColor];
        _textLabel.textColor = [UIColor whiteColor];
        [self pauseTimer];
    }
}

@end
