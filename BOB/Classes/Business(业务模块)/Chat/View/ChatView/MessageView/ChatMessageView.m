//
//  ChatMessageView.m
//  Moxian
//
//  Created by litiankun on 14-10-11.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import "ChatMessageView.h"
#import "ChatCacheFileUtil.h"
#import "VoiceConverter.h"
#import "ChatViewCell.h"
#import "TalkManager.h"
#import "LcwlChat.h"
#import "MXChatDBUtil.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "FBKVOController.h"
#import "GroupSystemMsgCell.h"
#import "ChatMessageTimeCell.h"
#import "MXConversation.h"
#import "NSObject+Additions.h"
#import "ChatFocusCell.h"
#import "MXDeviceManager.h"
#import "SDWebImageManager.h"
#import "MesFavoriteApi.h"
#import "MXJsonParser.h"
#import "ChatMessageWithdrawCell.h"

#define  minCellHeight 66


@interface ChatMessageView ()<UITableViewDataSource,UITableViewDelegate>
{
    // 复制
    UIMenuItem *_copyMenuItem;
    // 转发
    UIMenuItem *_forwardMenuItem;
    // 删除
    UIMenuItem *_deleteMenuItem;
    // 保存相册
    UIMenuItem *_saveImageMenuItem;
    // 复制图片
    UIMenuItem *_copyImageMenuItem;
    // 收藏
    UIMenuItem *_collectionMenuItem;
    
    // 使用听筒
    UIMenuItem *_earpieceMenuItem;
    
    NSIndexPath *_longPressIndexPath;
    
    //识别二维码
    UIMenuItem *_recognitionQRCode;
    
    ///撤回
    UIMenuItem *_withdrawMenuItem;
}
@property (nonatomic, copy) NSString* voicePlayModel;//空未设置过模式 0 扬声器 1 听筒

@end

@implementation ChatMessageView

- (instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.msgRecordTable];
        
        //添加监听
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(sensorStateChange:)name:@"UIDeviceProximityStateDidChangeNotification"
                                                   object:nil];
        self.voicePlayModel = [MXCache valueForKey:[NSString stringWithFormat:@"ControlSpeak_%@",UDetail.user.chatUser_id]];
    }
    return self;
}


- (void)applicationWillResignActive:(NSNotification *)notification
{
}
- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIDeviceProximityStateDidChangeNotification" object:nil];
    _msgRecordTable.delegate=nil;
    _msgRecordTable.dataSource=nil;
    _msgRecordTable=nil;
}

- (UITableView *)msgRecordTable {
    if (_msgRecordTable==nil) {
        _msgRecordTable=[[UITableView alloc] initWithFrame:self.bounds];
        _msgRecordTable.backgroundColor = [UIColor moBackground];
        _msgRecordTable.backgroundView=nil;
        [_msgRecordTable setBackgroundView:nil];
        [_msgRecordTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _msgRecordTable.delegate=self;
        _msgRecordTable.dataSource=self;
        Class cls = ChatMessageWithdrawCell.class;
        [_msgRecordTable registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        lpgr.minimumPressDuration = .5;
        [_msgRecordTable addGestureRecognizer:lpgr];
    }
    return _msgRecordTable;
}


#pragma mark   ---------tableView协议----------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.recordsArray.count;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
    
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView; {

    if (self.delegate && [self.delegate respondsToSelector:@selector(chatViewDidScroll)]) {
        [self.delegate chatViewDidScroll];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessageModel *msg=[self.viewModel.recordsArray safeObjectAtIndex:indexPath.row];
    if (![msg isKindOfClass:[MessageModel class]]) {//时间
        static NSString *timeCellIdentifier = @"timeCellIdentifier";
        ChatMessageTimeCell *timeCell = [tableView dequeueReusableCellWithIdentifier:timeCellIdentifier];
        if(!timeCell){
            timeCell=[[ChatMessageTimeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:timeCellIdentifier];
        }
        [timeCell reloadTime:(NSString*)msg];
        return timeCell;
        
    }else if(msg.type == MessageTypeNormal ||
             msg.type == MessageTypeGroupchat ||
             msg.type == MessageTypeSs ||
             msg.type == MessageTypeRich){
        if (msg.subtype == kMxmessageTypeReceiveRedpacket) {
            NSString * identifier= @"group_system_msg";
            UITableViewCell*cell =[tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell ==nil){
                cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                GroupSystemMsgCell* systemCell = [[GroupSystemMsgCell alloc]initWithFrame:CGRectZero];
                systemCell.tag = 101;
                [cell.contentView addSubview:systemCell];
                cell.backgroundColor = [UIColor clearColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            GroupSystemMsgCell* view = (GroupSystemMsgCell*)[cell.contentView viewWithTag:101];
            [view reloadData:msg];
            return cell;
        } else if (msg.subtype == kMXMessageTypeWithdraw) {
            Class cls = ChatMessageWithdrawCell.class;
            ChatMessageWithdrawCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls)];
            return cell;
        }
        NSString *cellIdentifier = [TalkManager cellIdentifierForMessageModel:msg];
        ChatViewCell *cell = (ChatViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[ChatViewCell alloc] initWithMessageModel:msg reuseIdentifier:cellIdentifier];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.messageModel = msg;
        return cell;
    }else if(msg.type == MessageTypeSgroup || msg.type == MessageTypeS){
        NSString * identifier= @"group_system_msg";
        UITableViewCell*cell =[tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell ==nil){
            cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            GroupSystemMsgCell* systemCell = [[GroupSystemMsgCell alloc]initWithFrame:CGRectZero];
            systemCell.tag = 101;
            [cell.contentView addSubview:systemCell];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        GroupSystemMsgCell* view = (GroupSystemMsgCell*)[cell.contentView viewWithTag:101];
        [view reloadData:msg];
          return cell;
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessageModel *msg=[self.viewModel.recordsArray safeObjectAtIndex:indexPath.row];
    if ([msg isKindOfClass:[MessageModel class]]) {
        if (msg.type == MessageTypeGroupchat ||
            msg.type == MessageTypeNormal ||
            msg.type == MessageTypeRich) {
            CGFloat height = [ChatViewCell tableView:tableView heightForRowAtIndexPath:indexPath withObject:msg];
            if (height > 10) {
                return height;
            } else {
                return 150;
            }
        }else if(msg.type == MessageTypeSgroup || msg.type == MessageTypeS ){
            return [GroupSystemMsgCell getHeight:msg]+10;
        }else if(msg.type == MessageTypeSs){
            CGFloat height = [ChatViewCell tableView:tableView heightForRowAtIndexPath:indexPath withObject:msg];
            return height;
        }
    } else {
        return 44;
    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:ChatMessageWithdrawCell.class]) {
        if (indexPath.row >= self.viewModel.recordsArray.count) {
            return;
        }
        
        MessageModel *msg = self.viewModel.recordsArray[indexPath.row];
        if (![msg isKindOfClass:MessageModel.class]) {
            return;
        }
        if (msg.subtype != kMXMessageTypeWithdraw) {
            return;
        }
        ChatMessageWithdrawCell *withDrawCell = (ChatMessageWithdrawCell *)cell;
        [withDrawCell configureView:msg];
    }
}

- (void)handleLongPress:(UILongPressGestureRecognizer*)gestureRecognize{
    
    if (gestureRecognize.state == UIGestureRecognizerStateBegan && [self.viewModel.recordsArray count] > 0) {
        
        CGPoint location = [gestureRecognize locationInView:self.msgRecordTable];
        NSIndexPath * indexPath = [self.msgRecordTable indexPathForRowAtPoint:location];
        id object = [self.viewModel.recordsArray safeObjectAtIndex:indexPath.row];
        if ([object isKindOfClass:[MessageModel class]]) {
            id cell = [self.msgRecordTable cellForRowAtIndexPath:indexPath];

            _longPressIndexPath = indexPath;
            MessageModel *model = [self.viewModel.recordsArray safeObjectAtIndex:_longPressIndexPath.row];
            if ([cell isKindOfClass:[ChatViewCell class]]) {
                ChatViewCell* tmpCell = (ChatViewCell*)cell;
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(longPressCellBubble:)]) {
                    [self.delegate longPressCellBubble:tmpCell];
                }
                [self showMenuViewController:tmpCell.headImageView messageModel:model];
            }
           
        }
    }
}

#pragma mark - 快捷菜单
- (void)showMenuViewController:(UIView *)showInView  messageModel:(MessageModel*)model
{
    self.operateModel = model;
    if (_menuController == nil) {
        _menuController = [UIMenuController sharedMenuController];
        [_menuController setMenuItems:@[]];
    }
    // 复制
    if (_copyMenuItem == nil) {
        _copyMenuItem = [[UIMenuItem alloc] initWithTitle:MXLang(@"MoTalk_copy", @"复制") action:@selector(copyMenuAction:)];
    }
    // 删除
    if (_deleteMenuItem == nil) {
        _deleteMenuItem = [[UIMenuItem alloc] initWithTitle:MXLang(@"MoTalk_delete", @"删除") action:@selector(deleteMenuAction:)];
    }
    // 转发
    if (_forwardMenuItem == nil) {
        _forwardMenuItem = [[UIMenuItem alloc] initWithTitle:MXLang(@"MoMessage_DynamicManager_forward_title_1", @"转发") action:@selector(forwordMenuAction:)];
    }
    //识别二维码
    if (_recognitionQRCode == nil) {
        _recognitionQRCode = [[UIMenuItem alloc] initWithTitle:MXLang(@"MoMessage_DynamicManager_forward_title_1", @"识别二维码") action:@selector(qrcodeMenuAction:)];
    }
    //收藏
    if (_collectionMenuItem == nil) {
        _collectionMenuItem = [[UIMenuItem alloc] initWithTitle:Lang(@"收藏") action:@selector(collectionMenuAction:)];
    }
    
    if (!_withdrawMenuItem) {
        _withdrawMenuItem = [[UIMenuItem alloc] initWithTitle:Lang(@"撤回") action:@selector(withdrawMenuAction:)];
    }
    
    if (model.type == MessageTypeNormal || model.type == MessageTypeGroupchat) {
        if (model.subtype == kMXMessageTypeText) {
            [_menuController setMenuItems:@[_copyMenuItem,_forwardMenuItem, _collectionMenuItem,_deleteMenuItem]];
        } else if (model.subtype == kMXMessageTypeVoice){
            if (_earpieceMenuItem == nil) {
                _earpieceMenuItem = [[UIMenuItem alloc] initWithTitle:@"听筒播放" action:@selector(earpieceMenuAction:)];
            }
            if ([StringUtil isEmpty:self.voicePlayModel]) {
                _earpieceMenuItem.title = @"听筒播放";
            }else if ([self.voicePlayModel isEqualToString:@"0"]) {
                _earpieceMenuItem.title = @"听筒播放";
            }else{
                _earpieceMenuItem.title = @"扬声器播放";
            }
            [_menuController setMenuItems:@[_earpieceMenuItem,_collectionMenuItem,_deleteMenuItem]];
        }else if (model.subtype == kMXMessageTypeImage){
            [_menuController setMenuItems:@[_forwardMenuItem, _collectionMenuItem,_deleteMenuItem,_recognitionQRCode]];
        }else if (model.subtype == kMXMessageTypeLocation ||  // 定位/gif
                  model.subtype == kMXMessageTypeGif){
            [_menuController setMenuItems:@[_forwardMenuItem, _collectionMenuItem, _deleteMenuItem]];
            
        }else if(model.subtype == kMXMessageTypeCard){
            [_menuController setMenuItems:@[_deleteMenuItem]];
        }else if(model.subtype == kMxmessageTypeRedPack){
            [_menuController setMenuItems:@[_deleteMenuItem]];
        }else if(model.subtype == kMXMessageTypeVoiceChat){
            [_menuController setMenuItems:@[_deleteMenuItem]];
        } else if (model.subtype == kMXMessageTypeFile) {
            [_menuController setMenuItems:@[_forwardMenuItem,_collectionMenuItem, _deleteMenuItem]];
        }
        else if (model.subtype == kMxmessageTypeVideo) {
            [_menuController setMenuItems:@[_collectionMenuItem, _deleteMenuItem]];
        }
    } else if (model.type == MessageTypeRich){
        [_menuController setMenuItems:@[_deleteMenuItem]];
    }
    NSMutableArray *menuItemsMArr = [NSMutableArray arrayWithArray:_menuController.menuItems];
    ///发送方撤回消息
    if (model.msg_direction) {
        //判断是否超出5分钟,超出则不显示撤回
        if (![model isOutOfWithdrawTimeLimit]) {
            if (![menuItemsMArr containsObject:_withdrawMenuItem]) {
                [menuItemsMArr insertObject:_withdrawMenuItem atIndex:0];
                [_menuController setMenuItems:menuItemsMArr];
            }
        }
    }
    if (![menuItemsMArr containsObject:_deleteMenuItem]) {
        [menuItemsMArr addObject:_deleteMenuItem];
        [_menuController setMenuItems:menuItemsMArr];
    }
    CGRect rect=showInView.frame;
    rect.origin.y=rect.origin.y + 5;
    if (model.msg_direction) {
        rect.origin.x=rect.origin.x - rect.size.width - 10;
    }else{
        rect.origin.x=rect.origin.x + rect.size.width + 10;
    }
    [_menuController setTargetRect:rect inView:showInView.superview];
    [_menuController setMenuVisible:YES animated:YES];
    
}

#pragma mark - MenuItem actions
#pragma mark 复制
- (void)copyMenuAction:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if (_longPressIndexPath.row > 0) {
        MessageModel *model = [self.viewModel.recordsArray safeObjectAtIndex:_longPressIndexPath.row];
        pasteboard.string = model.content;
    }
    
    _longPressIndexPath = nil;
}

#pragma mark 删除
- (void)deleteMenuAction:(id)sender
{
    if (_longPressIndexPath && _longPressIndexPath.row > 0) {
        MessageModel *model = [self.viewModel.recordsArray safeObjectAtIndex:_longPressIndexPath.row];
        NSMutableIndexSet *indexs = [NSMutableIndexSet indexSetWithIndex:_longPressIndexPath.row];
        
        [[MXChatDBUtil sharedDataBase] deleteMsgByMessageId:model.messageId chatId:model.chat_with];
        //判断是否是最后一条
        if ((self.viewModel.recordsArray.count - 1) == _longPressIndexPath.row){
            NSArray *tmpArray = [[MXChatDBUtil sharedDataBase]lastMessage:model.chat_with messageId:@""];
            //是否还存在信息
            if (tmpArray.count == 0) {
                MXConversation* conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:model.chat_with];
                conversation.lastClearTime = @"";
                [[MXChatDBUtil sharedDataBase] updateClearTime:model.chat_with
                                                          time:@""
                                                         msgID:@""];
            }else{
                MXConversation* conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:model.chat_with];
                [conversation.messages addObjectsFromArray:tmpArray];
                MessageModel* lastModel = tmpArray.lastObject;
                conversation.lastMessageId = lastModel.messageId;
                [[MXChatDBUtil sharedDataBase] updateClearTime:model.chat_with
                                                          time:conversation.latestMessage.sendTime
                                                         msgID:lastModel.messageId];
            }
        }
        
        NSMutableArray *indexPaths = [NSMutableArray arrayWithObjects:_longPressIndexPath, nil];;
        if (_longPressIndexPath.row - 1 >= 0) {
            id nextMessage = nil;
            id prevMessage = [self.viewModel.recordsArray safeObjectAtIndex:(_longPressIndexPath.row - 1)];
            if (_longPressIndexPath.row + 1 < [self.viewModel.recordsArray count]) {
                nextMessage = [self.viewModel.recordsArray safeObjectAtIndex:(_longPressIndexPath.row + 1)];
            }
            if ((!nextMessage || [nextMessage isKindOfClass:[NSString class]]) && [prevMessage isKindOfClass:[NSString class]]) {
                [indexs addIndex:_longPressIndexPath.row - 1];
                [indexPaths safeAddObj:[NSIndexPath indexPathForRow:(_longPressIndexPath.row - 1) inSection:0]];
            }
        }
        [self.viewModel.recordsArray removeObjectsAtIndexes:indexs];
        
        [self.msgRecordTable beginUpdates];
        [self.msgRecordTable deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.msgRecordTable endUpdates];
        if ([self.delegate respondsToSelector:@selector(showErrorView)]) {
            [self.delegate showErrorView];
        }
    }
    
    _longPressIndexPath = nil;
}

#pragma mark 转发
- (void)forwordMenuAction:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(forwardMessage:)]) {
        
        MessageModel *model = [self.viewModel.recordsArray safeObjectAtIndex:_longPressIndexPath.row];
        [self.delegate forwardMessage:@[[model copy]]];
        _longPressIndexPath = nil;
    }
}

#pragma mark 识别二维码
- (void)qrcodeMenuAction:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(qrcodeMenuAction:)]) {
        
        MessageModel *model = [self.viewModel.recordsArray safeObjectAtIndex:_longPressIndexPath.row];
        [self.delegate qrcodeMenuAction:@[[model copy]]];
        _longPressIndexPath = nil;
    }
}

#pragma mark 使用听筒
- (void)earpieceMenuAction:(id)sender {
    if ([StringUtil isEmpty:self.voicePlayModel]) {
        self.voicePlayModel = @"1";
    }else if([self.voicePlayModel isEqualToString:@"0"]){
        self.voicePlayModel = @"1";
    }else{
        self.voicePlayModel = @"0";
    }
    [self setSpeakerEnabledString:self.voicePlayModel];
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(audioPlayAction:)]) {
        [self.delegate audioPlayAction:self.operateModel];
    }
}

#pragma mark 消息收藏
- (void)collectionMenuAction:(id)sender {
    MLog(@"收藏")
    MessageModel *model = [self.viewModel.recordsArray safeObjectAtIndex:_longPressIndexPath.row];
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:model.body];
    NSDictionary *param = @{@"type":@(model.subtype),
                            @"attr1":bodyDic[@"attr1"]?:@"",
                            @"attr2":bodyDic[@"attr2"]?:@"",
                            @"attr3":bodyDic[@"attr3"]?:@"",
                            @"attr4":bodyDic[@"attr4"]?:@"",
                            @"attr5":bodyDic[@"attr5"]?:@"",
                            @"send_type":(model.chatType==eConversationTypeChat?@"1":@"2"),
                            @"send_id":model.fromID?:@"",
                            @"send_name":model.smartName?:@"",
                            @"send_time":model.sendTime
    };
    [NotifyHelper showHUDAddedTo:self animated:YES];
    [MesFavoriteApi addMesFavorite:param complete:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self animated:YES];
        if (success) {
            [NotifyHelper showMessageWithMakeText:@"收藏成功"];
        }
    }];
}

- (void)setSpeakerEnabledString:(NSString*)enabled {
    UserModel* model = [LcwlChat shareInstance].user;
    [MXCache setValue:enabled forKey:[NSString stringWithFormat:@"ControlSpeak_%@",model.chatUser_id]];
}

// 复制图片
- (void)copyImageMenuAction:(id)sender {
    
    [UIPasteboard removePasteboardWithName:@"CopyImage"];
    
    MessageModel *model = [self.viewModel.recordsArray safeObjectAtIndex:_longPressIndexPath.row];
    
    [NSIndexPath indexPathForRow:(_longPressIndexPath.row ) inSection:0];
    
    NSString *fileUrl = model.locFileUrl;
//    UIImage *image = [UIImage imageWithContentsOfFile:fileUrl];
    UIImage *image = nil;
    if (model.msg_direction) {
        if ([fileUrl hasSuffix:@".jpg"]) {
            image = [UIImage imageWithContentsOfFile:fileUrl];
        }else {
            NSString *imageUrl = [self appendImgPath:model.imageRemoteUrl];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
            image = [UIImage imageWithData:imageData];
        }
    }else {
        
        NSString *imageUrl = [self appendImgPath:model.imageRemoteUrl];
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
        image = [UIImage imageWithData:imageData];
    }
    if (!image) {
        MLog(@"不存在图片 %@",fileUrl);
        return ;
    }
    UIPasteboard *pasteBoard = [UIPasteboard pasteboardWithName:@"CopyImage" create:YES];
    pasteBoard.persistent = YES;
    pasteBoard.image = image;
}

// 保存相册
- (void)saveMenuAction:(id)sender {
    
    MessageModel *model = [self.viewModel.recordsArray safeObjectAtIndex:_longPressIndexPath.row];
    NSString *fileUrl = [model.imageRemoteUrl appendImgPath:model.imageRemoteUrl];
    UIImage *image = [[[SDWebImageManager sharedManager] imageCache] imageFromDiskCacheForKey:fileUrl];
    
    if (!image) {
        MLog(@"不存在图片 %@",fileUrl);
        return ;
    }
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    
}

#pragma mark 消息撤回
- (void)withdrawMenuAction:(id)sender {
    MLog(@"消息撤回")
    [MXAlertViewHelper showAlertViewWithMessage:@"是否撤回该消息" completion:^(BOOL cancelled, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            if (!self->_longPressIndexPath) {
                    return;
            }
            if (self->_longPressIndexPath.row <= 0) {
                return;
            }
            if (self->_longPressIndexPath.row >= self.viewModel.recordsArray.count) {
                return;
            }
            NSInteger selectIndex = self->_longPressIndexPath.row;
            MessageModel *model = self.viewModel.recordsArray[selectIndex];
            if (![model isKindOfClass:MessageModel.class]) {
                return;
            }
            model.subtype = kMXMessageTypeWithdraw;
            NSDictionary *bodyDic = @{@"attr1":model.messageId};
            model.body = [MXJsonParser dictionaryToJsonString:bodyDic];
            [[LcwlChat shareInstance].chatManager sendMessage:model
                                                   completion:nil];
            self.withdrawMessageID = model.messageId;
            ///等回执回来后再处理
            [NotifyHelper showHUDAddedTo:self animated:YES];
        }
        self->_longPressIndexPath = nil;
    }];
}

// 指定回调方法
- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    if(error != NULL){
        NSLog(@"保存图片失败");
    }else{
        [NotifyHelper showMessageWithMakeText:MXLang(@"Talk_controller_ChatMessageView_saveSuccess_1", @"保存成功")];
        _longPressIndexPath = nil;
    }
}



- (BOOL)canBecomeFirstResponder{
    return YES;
}

- (void)scrollViewToBottom:(BOOL)animated
{
    self.msgRecordTable.frame=self.bounds;
    if (self.msgRecordTable.contentSize.height > self.msgRecordTable.frame.size.height)
    {
        CGPoint offset = CGPointMake(0, self.msgRecordTable.contentSize.height - self.msgRecordTable.frame.size.height);
        [self.msgRecordTable setContentOffset:offset animated:YES];
    }
}

#pragma mark - EMDeviceManagerProximitySensorDelegate
//处理监听触发事件
-(void)sensorStateChange:(NSNotificationCenter *)notification;
{
    //如果此时手机靠近面部放在耳朵旁，那么声音将通过听筒输出，并将屏幕变暗（省电啊）
    if ([[UIDevice currentDevice] proximityState] == YES)
    {
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    }
    else
    {
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"123");
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    NSLog(@"345");
    return YES;
}


- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    MLog(@"");
    return [super hitTest:point withEvent:event];
}
@end
