//
//  ChatMessageView.h
//  Moxian
//
//  Created by litiankun on 14-10-11.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"
#import "ChatViewVM.h"

#import <AVFoundation/AVFoundation.h>

@protocol ChatMessageViewDelegate <NSObject>

@optional

// 信息重发
-(void)chatMessageReSend:(MessageModel*)model cellIndex:(NSInteger)cellRow;
// 点击头像
-(void)userHeaderToClick:(MessageModel*)model;

// 转发
-(void)forwardMessage:(NSArray*)model;

-(void)qrcodeMenuAction:(NSArray*)model;


// 长按cell时触发
-(void)longPressCellBubble:(id)cell;

// 听筒或语音
-(void)audioPlayAction:(MessageModel *)model ;


-(void)chatViewDidScroll;

//显示无数据view
-(void)showErrorView;
@end

@interface ChatMessageView : UIView//<LXActionSheetDelegate>
{

}

//
@property (nonatomic,weak) id<ChatMessageViewDelegate> delegate;

@property (nonatomic, weak) ChatViewVM* viewModel;

// 用户头像url
@property (nonatomic,strong) NSURL *myHeadImageUrl;

// 好友头像url
@property (nonatomic,strong) NSURL *fUserHeadImageUrl ;

// 数据列表
@property (nonatomic,strong) UITableView *msgRecordTable;

// 快捷菜单
@property (nonatomic,strong) UIMenuController *menuController;


@property (nonatomic, strong) MessageModel* operateModel;
///正在撤回的messageID
@property (nonatomic, copy) NSString *withdrawMessageID;

- (void)scrollViewToBottom:(BOOL)animated;

//- (void)stopAllPlay;

@end
