//
//  PasteboardShowImage.m
//  MoPal_Developer
//
//  Created by aken on 15/4/10.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "PasteboardShowImage.h"

#import "MXSeparatorLine.h"

#define ACTIONSHEET_BACKGROUNDCOLOR             [UIColor whiteColor]
#define WINDOW_COLOR                            [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]

#define ANIMATE_DURATION                        0.25f

#define PasteboardShowImageViewImageTag 1000022211

@protocol PasteboardShowImageViewDelegate <NSObject>

- (void)didSendImage:(UIImage*)image;

@end

@interface PasteboardShowImageView :UIView

@property (nonatomic,strong) UIView *backGroundView;
@property (nonatomic,strong) UIButton *okButton;
@property (nonatomic,strong) UIButton *cancelButton;
@property (nonatomic,strong) UIImageView *imageView;

@property (nonatomic,weak) id<PasteboardShowImageViewDelegate>delegate;


@end



@implementation PasteboardShowImageView
@synthesize imageView;

- (id)initWithData:(UIImage*)imgageData
{
    self = [super init];
    if (self) {
        
        self.tag = PasteboardShowImageViewImageTag;
        // 初始化背景视图，添加手势
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        self.backgroundColor = WINDOW_COLOR;
        self.userInteractionEnabled = YES;
        
        
        self.backGroundView = [[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 150,
                                                                       [UIScreen mainScreen].bounds.size.height/2 - 150,
                                                                       300,
                                                                       300)];
        self.backGroundView.backgroundColor =ACTIONSHEET_BACKGROUNDCOLOR;
        [self addSubview:self.backGroundView];

        
        
        imageView=[[UIImageView alloc] init];
        imageView.frame=CGRectMake(self.backGroundView.frame.size.width/2 - 140,
                                   10,
                                   280,
                                   236);
//        imageView.backgroundColor=[UIColor brownColor];
        imageView.image=imgageData;
        [self.backGroundView addSubview:imageView];
        
        
        
        UIView *line=[MXSeparatorLine initHorizontalLineWidth:self.backGroundView.frame.size.width orginX:0 orginY:CGRectGetMaxY(imageView.frame) + 10];
        [self.backGroundView addSubview: line];
        
        
        self.cancelButton=[UIButton buttonWithType:UIButtonTypeCustom];
        self.cancelButton.frame=CGRectMake(0, CGRectGetMaxY(line.frame), 150, 44);
        [self.cancelButton setTitle:MXLang(@"Public_Cancel", @"取消") forState:UIControlStateNormal];
        self.cancelButton.titleLabel.font=[UIFont font14];
        [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.backGroundView addSubview:self.cancelButton];
        [self.cancelButton addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        UIView *line1=[MXSeparatorLine initVerticalLineHeight:44 orginX:CGRectGetMaxX(self.cancelButton.frame) orginY:CGRectGetMaxY(line.frame)];
        [self.backGroundView addSubview: line1];
        
        self.okButton=[UIButton buttonWithType:UIButtonTypeCustom];
        self.okButton.frame=CGRectMake(CGRectGetMaxX(self.cancelButton.frame), CGRectGetMaxY(line.frame), 150, 44);
        [self.okButton setTitle:MXLang(@"Public_Send", @"发送") forState:UIControlStateNormal];
        self.okButton.titleLabel.font=[UIFont font14];
        [self.okButton setTitleColor:[UIColor moPurple] forState:UIControlStateNormal];
        [self.backGroundView addSubview:self.okButton];
        [self.okButton addTarget:self action:@selector(didSendImage) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
    }
    
    return self;
}


#pragma mark - 显示到ViewController
- (void)showInView
{
//    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    [[UIApplication sharedApplication].delegate.window  addSubview:self];
}

#pragma mark - 隐藏View
- (void)dismissView
{
    [UIView animateWithDuration:ANIMATE_DURATION animations:^{
//        [self.backGroundView setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, 0)];
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

#pragma mark - 发送
- (void)didSendImage {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSendImage:)]) {
        [self.delegate didSendImage:imageView.image];
        
        [self dismissView];
    }
}

@end

@interface PasteboardShowImage()<PasteboardShowImageViewDelegate>

@property (strong,nonatomic) PasteboardDidFinishSendCallBack callBack;

@end


@implementation PasteboardShowImage

+ (PasteboardShowImage*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (void)showToView:(UIImage*)imageData completion:(PasteboardDidFinishSendCallBack)completion {
 
    UIWindow *window =  [UIApplication sharedApplication].delegate.window;
    UIView *v = [window viewWithTag:PasteboardShowImageViewImageTag];
    if (v) {
        [v removeFromSuperview];
    }
    
    
    self.callBack=completion;
    
    PasteboardShowImageView *autoView=[[PasteboardShowImageView alloc] initWithData:imageData];
    autoView.delegate=self;
    [autoView showInView];
    
}

- (void)didSendImage:(UIImage *)image {
    if (self.callBack) {
        self.callBack(image);
    }
    
}

+ (void)showToView:(UIImage*)imageData completion:(PasteboardDidFinishSendCallBack)completion {
    
    [[self sharedInstance] showToView:imageData completion:completion];
}

@end
