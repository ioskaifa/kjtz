//
//  PasteboardShowImage.h
//  MoPal_Developer
//
//  Created by aken on 15/4/10.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^PasteboardDidFinishSendCallBack)(UIImage *image);

@interface PasteboardShowImage : NSObject

+ (void)showToView:(UIImage*)imageData completion:(PasteboardDidFinishSendCallBack)completion;

@end
