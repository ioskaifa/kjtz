//
//  ChatNewFriendCell.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/9/21.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SexAgeView.h"
#import "MoAttentionStatusView.h"

@protocol NewFriendAvatarPressDelegate<NSObject>

- (void)clickFriendAvatar:(NSString*)userid;
- (void)attentionFriendClick:(NSDictionary*)dict;

@end
@interface ChatNewFriendCell : UITableViewCell
@property (nonatomic, strong) UIImageView* imageviewAvatar;
@property (nonatomic, strong) UILabel* labelNickname;
@property (nonatomic, strong) SexAgeView* sexAndAgeView;
@property (nonatomic, strong) UILabel* labelTip;
@property (nonatomic, strong) UILabel* labelDate;
@property (nonatomic, weak) id<NewFriendAvatarPressDelegate> delegate;
- (id)initWithFriendModel:(NSDictionary *)dictionay reuseIdentifier:(NSString *)reuseIdentifier;
-(void)setNewFriendModel:(NSDictionary*) model;
+(NSInteger)getHeight;
@end
