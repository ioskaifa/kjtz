//
//  ChatViewCell.m
//  MoPal_Developer
//
//  Created by aken on 15/3/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatViewCell.h"
#import "LcwlChat.h"
#import "UIResponder+Router.h"
#import "FriendModel.h"
#import "ChatTextBubbleView.h"
#import "ChatLocationBubbleView.h"
#import "ChatRedPacketBubbleView.h"
#import "ChatVoiceChatBubbleView.h"
#import "ChatCardBubbleView.h"
#import "ChatProductLinkBubbleView.h"
#import "GroupSystemMsgCell.h"
#import "ChatVideoBubbleView.h"
#import "ChatFileBubbleView.h"
#import "ChatMessageWithdrawCell.h"

@implementation ChatViewCell
@synthesize proView;
- (id)initWithMessageModel:(MessageModel *)model reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithMessageModel:model reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.headImageView.clipsToBounds = YES;
        self.headImageView.layer.cornerRadius = 4.0;
    }
    return self;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

-(void)layoutCell{
    CGRect bubbleFrame = self.bubbleView.frame;
    if (self.messageModel.chatType == eConversationTypeGroupChat) {
        if (self.messageModel.type == MessageTypeGroupchat) {
            bubbleFrame.origin.y = CGRectGetMaxY(self.nameLabel.frame)+ChatAvatarPadding;
        }
        self.nameLabel.hidden = NO;
    }else{
        bubbleFrame.origin.y = self.headImageView.frame.origin.y;
        self.nameLabel.hidden = YES;
    }
    
    bubbleFrame.origin.x = CGRectGetMinX(self.headImageView.frame) - bubbleFrame.size.width-ChatAvatarPadding ;
    self.bubbleView.frame = bubbleFrame;
    
    CGRect frame = self.activityView.frame;
    frame.origin.x = bubbleFrame.origin.x - frame.size.width - ACTIVTIYVIEW_BUBBLE_PADDING;
    frame.origin.y = self.bubbleView.center.y - frame.size.height / 2;
    self.activityView.frame = frame;
    
    if(self.messageModel.msg_direction){
        _activityView.hidden = NO;
        _stateButton.hidden=NO;
    }else{
        _activityView.hidden = YES;
        _stateButton.hidden = YES;
    }
    if (self.messageModel.msg_direction) {
        [_activtiy stopAnimating];
        switch (self.messageModel.state) {
            case kMXMessageStateSending:{
                [_activtiy setHidden:NO];
                [_activtiy startAnimating];
                 _stateButton.hidden = YES;
                [_activtiy startAnimating];
            }
                break;
            case kMXMessageState_Failure:{
                [_activtiy setHidden:YES];
                [_activtiy stopAnimating];
                _stateButton.hidden = NO;
                [_stateButton setBackgroundImage:[UIImage imageNamed:@"sendFail"] forState:UIControlStateNormal];
            }
                break;
            default:
            {
                _activtiy.hidden = YES;
                [_activtiy stopAnimating];
                _stateButton.hidden = YES;
            }
                break;
        }
    }else{
        bubbleFrame.origin.x = CGRectGetMaxX(self.headImageView.frame)+ChatAvatarPadding;
        self.bubbleView.frame = bubbleFrame;
        [_activityView setHidden:YES];
        [_activtiy setHidden:YES];
        
    }
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    [self layoutCell];
}

- (void)setMessageModel:(MessageModel *)model
{
    [super setMessageModel:model];
    self.bubbleView.model = self.messageModel;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - action

// 重发按钮事件
-(void)retryButtonPressed:(UIButton *)sender
{
    [self routerEventWithName:kResendButtonTapEventName
                     userInfo:@{kShouldResendCell:self,@"message":self.messageModel}];
}

#pragma mark - private

- (void)setupSubviewsForMessageModel:(MessageModel *)messageModel
{
    [super setupSubviewsForMessageModel:messageModel];
    
    if (messageModel.msg_direction) {
        // 发送进度显示view
        _activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SEND_STATUS_SIZE, SEND_STATUS_SIZE)];
       
        [_activityView setHidden:YES];
        [self.contentView addSubview:_activityView];
        
        // 菊花
        _activtiy = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activtiy.backgroundColor = [UIColor clearColor];
        [_activityView addSubview:_activtiy];
        
        // 状态
        _stateButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _stateButton.frame = CGRectMake(0, 0, 18, 18);
        [_stateButton addTarget:self action:@selector(retryButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [_activityView addSubview:_stateButton];
        _activityView.hidden = NO;
        _stateButton.hidden=NO;
    }else{
        _activtiy.hidden = YES;
        _stateButton.hidden = YES;
    }
    
    self.bubbleView = [self bubbleViewForMessageModel:messageModel];
    [self.contentView addSubview:self.bubbleView];
}

- (ChatBaseBubbleView *)bubbleViewForMessageModel:(MessageModel *)messageModel
{
    if (messageModel.type == MessageTypeNormal || messageModel.type == MessageTypeGroupchat ) {
        switch (messageModel.subtype) {
            case kMXMessageTypeText:
            {
                return [[ChatTextBubbleView alloc] init];
            }
                break;
            case kMXMessageTypeImage:
            {
                return [[ChatImageBubbleView alloc] init];
            }
                break;
            case kMXMessageTypeLocation:
            {
                return [[ChatLocationBubbleView alloc] init];
            }
                break;
            case kMxmessageTypeVideo:
            {
                return [[ChatVideoBubbleView alloc] init];
            }
                break;
            case kMXMessageTypeGif:
            {
                return [[ChatGifBubbleView alloc] init];
            }
                break;
            case kMXMessageTypeVoice:
            {
                return [[ChatAudioBubbleView alloc] init];
            }
                break;
            case kMXMessageTypeCard:
            {
                return [[ChatCardBubbleView alloc] init];
            }
                break;
            case kMxmessageTypeRedPack:
            {
                return [[ChatRedPacketBubbleView alloc] init];
            }
                break;
            case kMXMessageTypeVoiceChat:
            {
                return [[ChatVoiceChatBubbleView alloc] init];
            }
                break;
            case kMXMessageTypeFile:
            {
                return [[ChatFileBubbleView alloc] init];
            }
                break;
            case kMXMessageTypeProductLink:
            {
                return [[ChatProductLinkBubbleView alloc] init];
            }
                break;
            default:
                break;
        }
    }else if(messageModel.type == MessageTypeSs){
        return [[ChatInviteBubbleView alloc] init];
    }else if(messageModel.type == MessageTypeRich){
        
    }
    
    
    return nil;
}

+ (CGFloat)bubbleViewHeightForMessageModel:(MessageModel *)messageModel
{
    if (messageModel.type == MessageTypeNormal || messageModel.type == MessageTypeGroupchat ) {
        switch (messageModel.subtype) {
            case kMXMessageTypeText:
            {
                return [ChatTextBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMXMessageTypeImage:
            {
                return [ChatImageBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMXMessageTypeGif:
            {
                return [ChatGifBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMXMessageTypeLocation:
            {
                return [ChatLocationBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMxmessageTypeVideo:
            {
                return [ChatVideoBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMXMessageTypeVoice:
            {
                return [ChatAudioBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMXMessageTypeCard:
            {
                return [ChatCardBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMxmessageTypeRedPack:
            {
                return [ChatRedPacketBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMXMessageTypeVoiceChat:
            {
                return [ChatVoiceChatBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMXMessageTypeFile:
            {
                return [ChatFileBubbleView heightForBubbleWithObject:messageModel];
            }
                break;
            case kMXMessageTypeProductLink:
            {
                return [ChatProductLinkBubbleView heightForBubbleWithObject:messageModel];
            }
            default:
                break;
        }
        
    }else if(messageModel.type == MessageTypeSs){
        return  [ChatInviteBubbleView heightForBubbleWithObject:nil];
    }else if(messageModel.type == MessageTypeRich){
        
    }
    return ChatAvatarWidth;
}

#pragma mark - public

+ (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath withObject:(MessageModel *)model
{
    
    if (model.subtype == kMxmessageTypeReceiveRedpacket) {
        return [GroupSystemMsgCell getHeight:model];
    } else if (model.subtype == kMXMessageTypeWithdraw) {
        return [ChatMessageWithdrawCell viewHeight];
    }
    NSInteger bubbleHeight = [self bubbleViewHeightForMessageModel:model];
    NSInteger headHeight = (ChatAvatarPadding * 2 + ChatAvatarWidth);
    if (model.type == MessageTypeGroupchat && !model.msg_direction) {
        bubbleHeight += NAME_LABEL_HEIGHT;
        bubbleHeight += ChatAvatarPadding;
    }
    return MAX(headHeight, bubbleHeight) + CELLPADDING;
}



@end
