//
//  ChatViewCell.h
//  MoPal_Developer
//
//  Created by aken on 15/3/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatViewBaseCell.h"
#import "ChatImageBubbleView.h"
#import "ChatGifBubbleView.h"
#import "ChatAudioBubbleView.h"
#import "ChatInviteBubbleView.h"

#define SEND_STATUS_SIZE 20 // 发送状态View的Size
#define ACTIVTIYVIEW_BUBBLE_PADDING 5 // 菊花和bubbleView之间的间距

@interface ChatViewCell : ChatViewBaseCell

//sender
@property (nonatomic, strong) UIActivityIndicatorView *activtiy;

@property (nonatomic, strong) UIView *activityView;
//
@property (nonatomic, strong) UIButton *stateButton;

@property(nonatomic, retain)  UIProgressView *proView;
@end
