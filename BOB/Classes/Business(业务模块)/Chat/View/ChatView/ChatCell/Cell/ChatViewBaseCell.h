//
//  ChatViewBaseCell.h
//  MoPal_Developer
//
//  消息展现的基本cell
//  Created by aken on 15/3/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"
#import "ChatBaseBubbleView.h"

#import "UIResponder+Router.h"

#define CELLPADDING 20 // Cell之间间距

#define NAME_LABEL_WIDTH 180 // nameLabel宽度
#define NAME_LABEL_HEIGHT 15 // nameLabel 高度
#define NAME_HEAD_PADDING 10 // nameLabel间距
#define NAME_LABEL_FONT_SIZE 14 // 字体

//extern NSString *const kRouterEventChatHeadImageTapEventName;

@interface ChatViewBaseCell : UITableViewCell
{
    
}

@property (nonatomic, strong) MessageModel *messageModel;

@property (nonatomic, strong) UIImageView *headImageView;       //头像
@property (nonatomic, strong) UILabel *nameLabel;               //姓名（暂时不支持显示）
@property (nonatomic, strong) ChatBaseBubbleView *bubbleView;
- (id)initWithMessageModel:(MessageModel *)model reuseIdentifier:(NSString *)reuseIdentifier;

- (void)setupSubviewsForMessageModel:(MessageModel *)model;

+ (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath withObject:(MessageModel *)model;

@end