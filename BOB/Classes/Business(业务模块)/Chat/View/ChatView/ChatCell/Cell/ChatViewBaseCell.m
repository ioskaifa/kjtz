//
//  ChatViewBaseCell.m
//  MoPal_Developer
//
//  Created by aken on 15/3/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatViewBaseCell.h"
#import "UIImageView+WebCache.h"
#import "LcwlChat.h"
#import "FriendModel.h"
//#import "GroupManager.h"
#import "NSObject+Additions.h"
#import "MXJsonParser.h"
//NSString *const kRouterEventChatHeadImageTapEventName = @"kRouterEventChatHeadImageTapEventName";

@interface ChatViewBaseCell()

@end

@implementation ChatViewBaseCell

- (id)initWithMessageModel:(MessageModel *)model reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headImagePressed:)];
        _headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ChatAvatarPadding, ChatAvatarPadding, ChatAvatarWidth, ChatAvatarWidth)];
        [_headImageView addGestureRecognizer:tap];
        _headImageView.userInteractionEnabled = YES;
        _headImageView.multipleTouchEnabled = YES;
        [self.contentView addSubview:_headImageView];
        
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.textColor = [UIColor grayColor];
        _nameLabel.font = [UIFont font12];
        [self.contentView addSubview:_nameLabel];
        
        [self setupSubviewsForMessageModel:model];
    }
    
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    CGRect frame = _headImageView.frame;
    frame.origin.x = _messageModel.msg_direction ? (self.bounds.size.width - _headImageView.frame.size.width - ChatAvatarPadding) : ChatAvatarPadding;
    _headImageView.frame = frame;
        if (_messageModel.type == MessageTypeGroupchat || _messageModel.type == MessageTypeNormal ||_messageModel.type == MessageTypeSs) {
            if (_messageModel.msg_direction) {
                _nameLabel.textAlignment = NSTextAlignmentRight;
                _nameLabel.frame = CGRectZero;
                _nameLabel.hidden = YES;
            }else{
                _nameLabel.textAlignment = NSTextAlignmentLeft;
                _nameLabel.frame = CGRectMake(CGRectGetMaxX(_headImageView.frame)+NAME_HEAD_PADDING, 0, NAME_LABEL_WIDTH, NAME_LABEL_HEIGHT);
                _nameLabel.hidden = NO;
            }
        }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - setter

- (void)setMessageModel:(MessageModel *)messageModel
{
    _messageModel = messageModel;
    if (messageModel.chatType == eConversationTypeChat) {
        UserModel* friend = [LcwlChat shareInstance].user;
        if ([friend.chatUser_id isEqualToString:_messageModel.fromID]){
            NSString* name = friend.user_name;
            _nameLabel.text = name;
             NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/120/h/120",friend.head_photo];
            [_headImageView sd_setImageWithURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
        }else{
            FriendModel *friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:messageModel.chat_with];
            if ([StringUtil isEmpty:friend.remark]) {
                _nameLabel.text = friend.name;
            }else{
                _nameLabel.text = friend.remark;
            }
             NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/120/h/120",friend.avatar];
            [_headImageView sd_setImageWithURL:[NSURL URLWithString:avatar]  placeholderImage:[UIImage imageNamed:@"avatar_default"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                
            }];
        }
    }else if(messageModel.chatType == eConversationTypeGroupChat){
        _nameLabel.text = messageModel.smartName;
        
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/120/h/120",messageModel.avatar];
        [_headImageView sd_setImageWithURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    }
}

#pragma mark - private

-(void)headImagePressed:(id)sender
{
    [super routerEventWithName:kRouterEventAvatarBubbleTapEventName userInfo:@{@"message":self.messageModel}];
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo
{
    [super routerEventWithName:eventName userInfo:userInfo];
}

#pragma mark - public

- (void)setupSubviewsForMessageModel:(MessageModel *)model
{
    if (model.msg_direction) {
        self.headImageView.frame = CGRectMake(self.bounds.size.width - ChatAvatarWidth - ChatAvatarPadding, ChatAvatarPadding, ChatAvatarWidth, ChatAvatarWidth);
    }
    else{
        self.headImageView.frame = CGRectMake(0, ChatAvatarPadding, ChatAvatarWidth, ChatAvatarWidth);
    }
}

+ (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath withObject:(MessageModel *)model
{
    return ChatAvatarWidth + CELLPADDING;
}

@end
