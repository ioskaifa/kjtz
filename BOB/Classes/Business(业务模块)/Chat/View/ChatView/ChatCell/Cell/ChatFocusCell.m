//
//  ChatFocusCell.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/11/23.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "ChatFocusCell.h"
//#import "GroupManager.h"
#import "LcwlChat.h"
#import "FriendModel.h"
#import "ChatTextBubbleView.h"
#import "TalkManager.h"
#import "NSObject+Additions.h"

@implementation ChatFocusCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (NSString *)cellIdentifierForMessageModel:(MessageModel *)model
{
    NSString *identifier = @"FocusMessage";
    if (model.msg_direction) {
        identifier = [identifier stringByAppendingString:@"i"];
    }else{
        identifier = [identifier stringByAppendingString:@"you"];
    }
    return identifier;
}
- (id)initWithMessageModel:(MessageModel *)model reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}

-(void)headImagePressed:(id)sender
{
    [super routerEventWithName:kRouterEventAvatarBubbleTapEventName userInfo:@{@"message":_message}];
}


-(void)reloadData:(MessageModel*)msg{
    _message = msg;
}

+(NSInteger)getHeight:(MessageModel*)msg{
    return 0;
}
@end
