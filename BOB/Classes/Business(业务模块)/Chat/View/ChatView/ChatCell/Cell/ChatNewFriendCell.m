//
//  ChatNewFriendCell.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/9/21.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatNewFriendCell.h"
#import "UIImageView+WebCache.h"
//#import "GroupManager.h"
//#import "TalkManager.h"
//#import "MXChat.h"
#import "NSObject+Additions.h"

static NSInteger avatarWidth = 40;
static NSInteger leftmargin = 10;

@interface ChatNewFriendCell()
@property (nonatomic) NSDictionary* dict;
@property (nonatomic, strong) MoAttentionStatusView *attentionView;

@end
@implementation ChatNewFriendCell
- (id)initWithFriendModel:(NSDictionary *)model reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initUI];
        [self initLayout];
    }
    
    return self;
}

-(void)initUI{
    // Initialization code
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headImagePressed:)];
    _imageviewAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, avatarWidth, avatarWidth)];
    _imageviewAvatar.layer.masksToBounds = YES;
    _imageviewAvatar.layer.cornerRadius = avatarWidth/2;
    [_imageviewAvatar addGestureRecognizer:tap];
    _imageviewAvatar.userInteractionEnabled = YES;
    [_imageviewAvatar addGestureRecognizer:tap];
    [self.contentView addSubview:_imageviewAvatar];
    
    _labelNickname = [[UILabel alloc] init];
    _labelNickname.backgroundColor = [UIColor clearColor];
    _labelNickname.textColor = [UIColor blackColor];
    _labelNickname.font = [UIFont font14];
    [self.contentView addSubview:_labelNickname];
    
    
    _sexAndAgeView = [[SexAgeView alloc] initWithFrame:CGRectZero];
    [self addSubview:_sexAndAgeView];
    
    _labelTip = [[UILabel alloc] init];
    _labelTip.backgroundColor = [UIColor clearColor];
    _labelTip.textColor = [UIColor grayColor];
    _labelTip.font = [UIFont font12];
    [self.contentView addSubview:_labelTip];
    
    _labelDate = [[UILabel alloc] init];
    _labelDate.backgroundColor = [UIColor clearColor];
    _labelDate.textColor = [UIColor grayColor];
    _labelDate.font = [UIFont font12];
    _labelDate.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_labelDate];
    
    [self.contentView addSubview:self.attentionView];
    @weakify(self)
    self.attentionView.attentionBlock = ^(MoRelationshipType status) {
        @strongify(self)
        if ([self.delegate respondsToSelector:@selector(attentionFriendClick:)]) {
            [self.delegate attentionFriendClick:self.dict];
        }
    };
}

- (void)headImagePressed:(UITapGestureRecognizer*)gesture{
    if ([self.delegate respondsToSelector:@selector(clickFriendAvatar:)]) {
        [self.delegate clickFriendAvatar:self.dict[@"userid"]];
    }
}

- (void)initLayout {
    @weakify(self)
//    [_imageviewAvatar mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self)
//        make.centerY.equalTo(self.mas_centerY);
//        make.left.equalTo(self.mas_left).offset(leftmargin);
//        make.width.equalTo(avatarWidth);
//        make.height.equalTo(avatarWidth);
//    }];
//
//    [_labelNickname mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_imageviewAvatar.mas_right).offset(leftmargin);
//        make.top.equalTo(_imageviewAvatar.mas_top).offset(-2);
//        make.right.lessThanOrEqualTo(@(-110));
//        make.height.equalTo(15);
//    }];
//
//    [_sexAndAgeView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_labelNickname.mas_left);
//        make.top.equalTo(_labelNickname.mas_bottom).offset(5);
//        make.width.equalTo(@(_sexAndAgeView.width));
//        make.height.equalTo(@(_sexAndAgeView.height));
//    }];
//
//    [_labelTip mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(_sexAndAgeView.mas_left);
//        make.top.equalTo(_sexAndAgeView.mas_bottom).offset(5);
//        make.width.equalTo(100);
//        make.height.equalTo(15);
//    }];
//
//    [_labelDate mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self)
//        make.right.equalTo(self.mas_right).offset(-leftmargin);
//        make.top.equalTo(_labelNickname.mas_top);
//        make.width.equalTo(150);
//        make.height.equalTo(15);
//    }];
//
//    [self.attentionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(@(-10));
//        make.bottom.equalTo(@(-12));
//        make.size.equalTo(CGSizeMake(64, 28));
//    }];
}

-(void)setNewFriendModel:(NSDictionary*) dictionay{
    self.dict = dictionay;
    
//    [_imageviewAvatar mx_setImageWithURL:[NSURL URLWithString:[self appendImgPath:dictionay[@"avatar"]]] placeholderImage:[UIImage imageNamed:[GroupManager getGroupDefaultPic:2]]];
//    _labelNickname.text = dictionay[@"name"];
//    BOOL isShowAge = [dictionay[@"age"] isEqualToString:@"0"];
//    [_sexAndAgeView setGenderType:[dictionay[@"gender"] integerValue] withUserAge:dictionay[@"age"] withIsShowAge:!isShowAge];
//    [_sexAndAgeView mas_updateConstraints:^(MASConstraintMaker *make) {
//        make.width.equalTo(@(_sexAndAgeView.width));
//    }];
//    _labelTip.text = MXLang(@"Talk_msg_chat_send_focus_84", @"给你打了招呼哟");
//    _labelDate.text = [TalkManager timeDataString:dictionay[@"time"]];
//    MoYouModel* friend = [[MXChat sharedInstance].chatManager loadFriendByChatId:dictionay[@"userid"]];
//    int followState = friend.followState;
//    if (followState > 0) {
//        [self.attentionView refreshAttentionStatusView:followState];
//    }else{
//        [self.attentionView refreshAttentionStatusView:0];
//    }
}
+(NSInteger)getHeight{
    return leftmargin*2+avatarWidth;
}

- (MoAttentionStatusView *)attentionView
{
    if (!_attentionView) {
        _attentionView = [[MoAttentionStatusView alloc] initWithFrame:CGRectZero];
    }
    return _attentionView;
}


@end
