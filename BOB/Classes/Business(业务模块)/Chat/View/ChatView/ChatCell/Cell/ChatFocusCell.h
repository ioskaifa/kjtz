//
//  ChatFocusCell.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/11/23.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatTextBubbleView.h"
@interface ChatFocusCell : UITableViewCell

+ (NSString *)cellIdentifierForMessageModel:(MessageModel *)model;
- (id)initWithMessageModel:(MessageModel *)model reuseIdentifier:(NSString *)reuseIdentifier;
@property (strong, nonatomic) ChatTextBubbleView* bubbleview;
//@property (strong, nonatomic) GroupSystemMsgCell* systemView;
@property (strong, nonatomic) UIImageView* avatarImageView;
@property (strong, nonatomic) MessageModel* message;
+(NSInteger)getHeight:(MessageModel*)msg;
-(void)reloadData:(MessageModel*)msg;
@end
