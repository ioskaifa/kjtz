//
//  ChatVideoBubbleView.h
//  BOB
//
//  Created by mac on 2020/1/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChatBaseBubbleView.h"

NS_ASSUME_NONNULL_BEGIN

#define MAX_SIZE 120 //　图片最大显示大小
#define MAX_IMAGE_WIDTH 110.0
#define MAX_IMAGE_HEIGHT 150.0

@interface ChatVideoBubbleView : ChatBaseBubbleView
@property (nonatomic, strong) UIImageView *imageView;
@end

NS_ASSUME_NONNULL_END
