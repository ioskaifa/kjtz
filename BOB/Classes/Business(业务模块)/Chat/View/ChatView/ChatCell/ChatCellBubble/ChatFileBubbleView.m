//
//  ChatFileBubbleView.m
//  BOB
//
//  Created by mac on 2020/7/29.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChatFileBubbleView.h"

@interface ChatFileBubbleView ()

@property (nonatomic, strong) MyLinearLayout *layout;

@property (nonatomic, strong) MyLinearLayout *titleLayout;

@property (nonatomic, strong) UIView *placeholderView;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *sizeLbl;

@property (nonatomic, assign) CGFloat titleLblWidth;

@end

static CGFloat const ChatFileBubbleViewWidth = 250;

@implementation ChatFileBubbleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)heightForBubbleWithObject:(MessageModel *)object {
    return 80;
}

-(void)bubbleViewPressed:(id)sender {
    [self routerEventWithName:kRouterEventFileBubbleTapEventName
                     userInfo:@{KMESSAGEKEY:self.model}];
}

///"attr1" : url,"attr2" : "doc", "attr3" : "size", "attr4" : "name",
- (void)setModel:(MessageModel *)model {
    [super setModel:model];
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:model.body];
    self.imgView.image = [UIImage imageNamed:[self.class iconWith:model]];
    
    NSString *title = StrF(@"%@.%@", bodyDic[@"attr4"], bodyDic[@"attr2"]);
    if ([StringUtil isEmpty:bodyDic[@"attr4"]] || [StringUtil isEmpty:bodyDic[@"attr2"]]) {
        title = StrF(@"%@.%@", model.fileName, model.fileSubType);
    }
    self.titleLbl.text = title;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(self.titleLblWidth, MAXFLOAT)];
    self.titleLbl.mySize = size;
    
    NSString *sizeDes = bodyDic[@"attr3"];
    if ([StringUtil isEmpty:sizeDes]) {
        sizeDes = [StringUtil convertFileSize:model.fileSize];
    }
    self.sizeLbl.text = sizeDes;
    [self.sizeLbl sizeToFit];
    self.sizeLbl.mySize = self.sizeLbl.size;    
    BOOL isReceiver = !model.msg_direction;
    NSString *imageName = isReceiver ? BUBBLE_LEFT_IMAGE_NAME : BUBBLE_FILE_RIGHT_IMAGE_NAME;
    self.backImageView.image = [UIImage resizeRightBubbleWithImage:[UIImage imageNamed:imageName]];
}

+ (NSString *)iconWith:(MessageModel *)model {
    NSString *icon = @"icon_msg_unknow";
    switch (model.fileType) {
        case kMXFileTypePDF:{
            icon = @"icon_msg_pdf";
            break;
        }
        case kMXFileTypeWord:{
            icon = @"icon_msg_word";
            break;
        }
        case kMXFileTypeExcel:{
            icon = @"icon_msg_excel";
            break;
        }
        default:
            break;
    }
    return icon;
}

- (CGSize)sizeThatFits:(CGSize)size {
    return CGSizeMake(ChatFileBubbleViewWidth, [self.class heightForBubbleWithObject:nil]);
}

- (void)createUI {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [self.layout addSubview:self.titleLayout];
    [self.layout addSubview:self.placeholderView];
    [self.layout addSubview:self.imgView];
    [layout addSubview:self.layout];
}

#pragma mark - Init
- (MyLinearLayout *)layout {
    if (!_layout) {
        _layout = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            object.myTop = 10;
            object.myLeft = 10;
            object.myRight = 10;
            object.myBottom = 10;
            object;
        });
    }
    return _layout;
}

- (MyLinearLayout *)titleLayout {
    if (!_titleLayout) {
        _titleLayout = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            object.myTop = 0;
            object.myLeft = 0;
            object.weight = 1;
            object.myHeight = MyLayoutSize.wrap;
            [object addSubview:self.titleLbl];
            [object addSubview:self.sizeLbl];
            object;
        });
    }
    return _titleLayout;
}

- (UIView *)placeholderView {
    if (!_placeholderView) {
        _placeholderView = ({
            UIView *object = [UIView new];
            object.size = CGSizeMake(10, 1);
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _placeholderView;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.image = [UIImage imageNamed:@"icon_msg_unknow"];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.lineBreakMode = NSLineBreakByTruncatingMiddle;
            object.font = [UIFont font15];
            object.textColor = [UIColor moBlack];
            object.numberOfLines = 2;
            CGFloat viewWidth = ChatFileBubbleViewWidth;
            CGFloat width = viewWidth - self.imgView.width - self.placeholderView.width;
            self.titleLblWidth = width - 20;
            object.width = self.titleLblWidth;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)sizeLbl {
    if (!_sizeLbl) {
        _sizeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font13];
            object.textColor = [UIColor grayColor];
            object.myTop = 5;
            object;
        });
    }
    return _sizeLbl;
}

@end
