//
//  ChatAudioBubbleView.h
//  MoPal_Developer
//
//  Created by aken on 15/8/27.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatBaseBubbleView.h"

#define ANIMATION_IMAGEVIEW_SIZE 15 // 小喇叭图片尺寸
#define ANIMATION_IMAGEVIEW_SPEED 1 // 小喇叭动画播放速度


#define ANIMATION_TIME_IMAGEVIEW_PADDING 5 // 时间与动画间距


#define ANIMATION_TIME_LABEL_WIDHT 30 // 时间宽度
#define ANIMATION_TIME_LABEL_HEIGHT 15 // 时间高度
#define ANIMATION_TIME_LABEL_FONT_SIZE 14 // 时间字体

// 发送
#define SENDER_ANIMATION_IMAGEVIEW_IMAGE_DEFAULT @"VoiceNodePlaying" // 小喇叭默认图片
#define SENDER_ANIMATION_IMAGEVIEW_IMAGE_01 @"VoiceNodePlaying-1" // 小喇叭动画第一帧
#define SENDER_ANIMATION_IMAGEVIEW_IMAGE_02 @"VoiceNodePlaying-2" // 小喇叭动画第二帧
#define SENDER_ANIMATION_IMAGEVIEW_IMAGE_03 @"VoiceNodePlaying-3" // 小喇叭动画第三帧


// 接收
#define RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_DEFAULT @"VoiceNodePlaying" // 小喇叭默认图片
#define RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_01 @"VoiceNodePlaying-1" // 小喇叭动画第一帧
#define RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_02 @"VoiceNodePlaying-2" // 小喇叭动画第二帧
#define RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_03 @"VoiceNodePlaying-3" // 小喇叭动画第二帧

// 接收声音动画
#define RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_Voice_1 @"Motalk_message_bg_other" 
#define RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_Voice_2 @"Motalk_message_gray_bg_other"


@interface ChatAudioBubbleView : ChatBaseBubbleView
{
    UIImageView *_animationImageView; // 动画的ImageView
    UILabel *_timeLabel; // 时间label
}

- (void)startAudioAnimation;
- (void)stopAudioAnimation;

- (void)startDownloadAudioAnimation;
- (void)stopDownloadAudioAnimation;

@end
