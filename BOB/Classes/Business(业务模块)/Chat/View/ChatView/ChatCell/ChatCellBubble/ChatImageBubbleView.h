//
//  ChatImageBubbleView.h
//  MoPal_Developer
//
//  Created by aken on 15/3/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatBaseBubbleView.h"

#define MAX_SIZE 120 //　图片最大显示大小
#define MAX_IMAGE_WIDTH 110.0
#define MAX_IMAGE_HEIGHT 150.0

@interface ChatImageBubbleView : ChatBaseBubbleView
@property (nonatomic, strong) UIImageView *imageView;

@end
