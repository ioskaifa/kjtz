//
//  ChatVoiceChatBubbleView.m
//  BOB
//
//  Created by mac on 2020/7/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChatVoiceChatBubbleView.h"

@interface ChatVoiceChatBubbleView ()

@property (nonatomic, strong) SPButton *contentBtn;

@end

@implementation ChatVoiceChatBubbleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)heightForBubbleWithObject:(MessageModel *)object {
    return 45;
}

-(void)bubbleViewPressed:(id)sender {
    [self routerEventWithName:kRouterEventVoiceChatBubbleTapEventName
                     userInfo:@{KMESSAGEKEY:self.model}];
}

- (void)setModel:(MessageModel *)model {
    if (model.msg_direction) {
        self.contentBtn.imagePosition = SPButtonImagePositionRight;
        [self.contentBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else {
        self.contentBtn.imagePosition = SPButtonImagePositionLeft;
        [self.contentBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:model.body];
    NSString *content = bodyDic[@"attr1"];
    [self.contentBtn setTitle:content forState:UIControlStateNormal];
    [self.contentBtn sizeToFit];
    if (CGSizeEqualToSize(model.size, CGSizeZero) ||
        model.size.width == 0 ||
        model.size.height == 0) {
        CGSize size = self.contentBtn.size;
        size = CGSizeMake(size.width + 20, size.height);
        self.contentBtn.mySize = size;
        model.size = size;
    } else {
        self.contentBtn.mySize = model.size;
    }
    [super setModel:model];
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGSize contentSize = self.model.size;
    contentSize = CGSizeMake(contentSize.width, [self.class heightForBubbleWithObject:nil]);
    return contentSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:self.contentBtn];
}

- (SPButton *)contentBtn {
    if (!_contentBtn) {
        _contentBtn = [SPButton new];
        _contentBtn.imageTitleSpace = 10;
        _contentBtn.titleLabel.font = [UIFont font15];
        [_contentBtn setImage:[UIImage imageNamed:@"icon_voicChat"] forState:UIControlStateNormal];
        _contentBtn.myCenterY = 0;
        _contentBtn.userInteractionEnabled = NO;
    }
    return _contentBtn;
}

@end
