//
//  ChatTextBubbleView.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/7/30.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatBaseBubbleView.h"
#define TEXTLABEL_MAX_WIDTH [UIScreen mainScreen].bounds.size.width*0.7 // textLaebl 最大宽度
#define LABEL_LINESPACE 0       // 行间距
@interface ChatTextBubbleView : ChatBaseBubbleView

+(CGSize)getTextSizeByContent:(NSString*)content;
@end
