//
//  ChatAudioBubbleView.m
//  MoPal_Developer
//
//  Created by aken on 15/8/27.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatAudioBubbleView.h"
#define SOUND_BUBBLE_WIDTH (200.f)
@interface ChatAudioBubbleView ()
{
    NSMutableArray *_senderAnimationImages;
    NSMutableArray *_recevierAnimationImages;
    UIImageView    *_isReadView;
    
    // 下载语音动画
    UIImageView *_downloadAnimationImageView;
    NSMutableArray *_downloadAnimationImages;
}

@end

@implementation ChatAudioBubbleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _animationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ANIMATION_IMAGEVIEW_SIZE, ANIMATION_IMAGEVIEW_SIZE)];
        _animationImageView.animationDuration = ANIMATION_IMAGEVIEW_SPEED;
        [self addSubview:_animationImageView];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ANIMATION_TIME_LABEL_WIDHT, ANIMATION_TIME_LABEL_HEIGHT)];
        _timeLabel.font = [UIFont boldSystemFontOfSize:ANIMATION_TIME_LABEL_FONT_SIZE];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_timeLabel];
        
        _isReadView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 8, 8)];
        _isReadView.layer.cornerRadius = 4;
        [_isReadView setClipsToBounds:YES];
        [_isReadView setBackgroundColor:[UIColor redColor]];
        [self addSubview:_isReadView];
        
        _senderAnimationImages = [[NSMutableArray alloc] initWithObjects: [UIImage imageNamed:SENDER_ANIMATION_IMAGEVIEW_IMAGE_01], [UIImage imageNamed:SENDER_ANIMATION_IMAGEVIEW_IMAGE_02], [UIImage imageNamed:SENDER_ANIMATION_IMAGEVIEW_IMAGE_03],nil];
        _recevierAnimationImages = [[NSMutableArray alloc] initWithObjects: [UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_01], [UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_02],[UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_03], nil];
        
        // 下载语音动画
        NSInteger leftCapWidth = BUBBLE_LEFT_LEFT_CAP_WIDTH;
        NSInteger topCapHeight = BUBBLE_LEFT_TOP_CAP_HEIGHT;
        
        UIImage *downLoadImage_1=[UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_Voice_1];
        UIImage *downLoadImage_2=[UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_Voice_2];
        
        _downloadAnimationImages=[[NSMutableArray alloc] initWithObjects: downLoadImage_1,downLoadImage_2,nil];
        _downloadAnimationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _downloadAnimationImageView.animationDuration=1.8;
        [self addSubview:_downloadAnimationImageView];
    }
    return self;
}

-(CGSize)sizeThatFits:(CGSize)size
{
    float voiceLen = [self.model.voiceLen floatValue]/60.0;
    int w = ((SOUND_BUBBLE_WIDTH*voiceLen+70)>SOUND_BUBBLE_WIDTH?SOUND_BUBBLE_WIDTH:(SOUND_BUBBLE_WIDTH*voiceLen+70));
    CGFloat width = w;
    return CGSizeMake(width, ChatAvatarWidth);
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = _animationImageView.frame;
    
    if (self.model.msg_direction) {
        
        frame.origin.x = self.frame.size.width - BUBBLE_ARROW_WIDTH - frame.size.width - BUBBLE_VIEW_PADDING;
        frame.origin.y = self.frame.size.height / 2 - frame.size.height / 2;
        _animationImageView.frame = frame;
        frame = _timeLabel.frame;
        frame.origin.x = BUBBLE_VIEW_PADDING;
        
        frame.origin.y = _animationImageView.center.y - frame.size.height / 2;
        _timeLabel.frame = frame;
        
    }else {
        _animationImageView.image = [UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_DEFAULT];
        
        frame.origin.x = BUBBLE_ARROW_WIDTH + BUBBLE_VIEW_PADDING;
        frame.origin.y = self.frame.size.height / 2 - frame.size.height / 2;
        _animationImageView.frame = frame;
        float voiceLen = [self.model.voiceLen floatValue]/60.0;
        int w = ((SOUND_BUBBLE_WIDTH*voiceLen+70)>SOUND_BUBBLE_WIDTH?SOUND_BUBBLE_WIDTH:(SOUND_BUBBLE_WIDTH*voiceLen+70));
        
        frame = _timeLabel.frame;
        frame.origin.x = (w - ANIMATION_TIME_LABEL_WIDHT - BUBBLE_VIEW_PADDING);

        frame.origin.y = _animationImageView.center.y - frame.size.height / 2;
        _timeLabel.frame = frame;
        frame.origin.x += (frame.size.width - _isReadView.frame.size.width / 2+15);
        frame.origin.y = - _isReadView.frame.size.height / 2;
        frame.size = _isReadView.frame.size;
        _isReadView.frame = frame;
        
        _downloadAnimationImageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    }
}

#pragma mark - setter

- (void)setModel:(MessageModel *)model
{
    [super setModel:model];
    
    // length
    _timeLabel.text = [NSString stringWithFormat:@"%@''",self.model.voiceLen];
    
    if (self.model.msg_direction) {
        [_isReadView setHidden:YES];
        _animationImageView.image = [UIImage imageNamed:SENDER_ANIMATION_IMAGEVIEW_IMAGE_DEFAULT];
        _animationImageView.animationImages = _senderAnimationImages;
        _timeLabel.textColor = [UIColor whiteColor];
        if (model.isPlaying) {
            [self startAudioAnimation];
        }else {
            [self stopAudioAnimation];
        }

    }
    else{
        //if (model.isPlayed) {
        _timeLabel.textColor = [UIColor blackColor];
        _animationImageView.image = [UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_DEFAULT];
        _animationImageView.animationImages = _recevierAnimationImages;
        // 下载语音动画
        _downloadAnimationImageView.animationImages=_downloadAnimationImages;
        
        // 先判断是否下载完
        if ([StringUtil isEmpty:model.locFileUrl] ) {
            [self startDownloadAudioAnimation];
        }else{
            if (model.isPlaying) {
                [self startAudioAnimation];
            }else {
                [self stopAudioAnimation];
            }
            [self stopDownloadAudioAnimation];
        }
        if(![StringUtil isEmpty:model.locFileUrl]){
            if (model.isPlay) {
                [_isReadView setHidden:YES];
            }else{
                [_isReadView setHidden:NO];
            }
        }
    }
}

#pragma mark - public

-(void)bubbleViewPressed:(id)sender
{
    [self routerEventWithName:kRouterEventAudioBubbleTapEventName userInfo:@{KMESSAGEKEY:self.model}];
}


+(CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    return 2 * BUBBLE_VIEW_PADDING + ANIMATION_IMAGEVIEW_SIZE ;
}

-(void)startAudioAnimation
{
    [_animationImageView startAnimating];
}

-(void)stopAudioAnimation
{
    [_animationImageView stopAnimating];
}

- (void)startDownloadAudioAnimation {
    _isReadView.hidden=YES;
    _downloadAnimationImageView.hidden=NO;
    [_downloadAnimationImageView startAnimating];
}

- (void)stopDownloadAudioAnimation {
    _isReadView.hidden=NO;
    _downloadAnimationImageView.hidden=YES;
    [_downloadAnimationImageView stopAnimating];
}

@end
