//
//  ChatBaseBubbleView.m
//  MoPal_Developer
//
//  Created by aken on 15/3/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatBaseBubbleView.h"
#import "UIImage+Utils.h"

@interface ChatBaseBubbleView ()

@end

@implementation ChatBaseBubbleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        _backImageView = [[UIImageView alloc] init];
        _backImageView.userInteractionEnabled = YES;
        _backImageView.multipleTouchEnabled = YES;
        _backImageView.clipsToBounds = YES;
        _backImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self addSubview:_backImageView];
        _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bubbleViewPressed:)];
        [self addGestureRecognizer:_tap];
        
        self.backgroundColor = [UIColor clearColor];
        
    }
    
    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
}

#pragma mark - setter

- (void)setModel:(MessageModel *)model
{
    _model = model;
    
    BOOL isReceiver = !_model.msg_direction;
    NSString *imageName = isReceiver ? BUBBLE_LEFT_IMAGE_NAME : BUBBLE_RIGHT_IMAGE_NAME;
    if (model.subtype == kMXMessageTypeCard) {
        self.backImageView.image = [UIImage imageNamed:@"card_bg"];
    }else if(model.subtype == kMxmessageTypeRedPack){
        NSString *imageName = isReceiver ? @"redpacket_left" : @"redpacket_right";
        self.backImageView.image = isReceiver ? [UIImage resizeLeftBubbleWithImage:[UIImage imageNamed:imageName]] : [UIImage resizeRightBubbleWithImage:[UIImage imageNamed:imageName]];
        if (model.isPlay == 1 || model.isPlay == 2) {
            self.backImageView.alpha = 0.5;
        }else{
            self.backImageView.alpha = 1.0;
        }
    } else {
        self.backImageView.image = [UIImage resizeRightBubbleWithImage:[UIImage imageNamed:imageName]];
    }
   
    [self sizeToFit];
}

#pragma mark - public

+ (CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    return 30;
}

- (void)bubbleViewPressed:(id)sender
{
    [self routerEventWithName:kRouterEventChatCellBubbleTapEventName userInfo:@{KMESSAGEKEY:self.model}];
}

- (void)progress:(CGFloat)progress
{
    [_progressView setProgress:progress animated:YES];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    MLog(@"");
    return [super hitTest:point withEvent:event];
}

@end
