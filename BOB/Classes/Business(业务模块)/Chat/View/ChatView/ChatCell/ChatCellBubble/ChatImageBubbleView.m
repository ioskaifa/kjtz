//
//  ChatImageBubbleView.m
//  MoPal_Developer
//
//  Created by aken on 15/3/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatImageBubbleView.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "NSObject+Additions.h"
#import "ChatCacheFileUtil.h"
#import "UIImageView+WebCache.h"
#import "MXJsonParser.h"
#import "DocumentManager.h"
// 接收图片动画
#define RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_1 @"Motalk_loading_point_1"
#define RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_2 @"Motalk_loading_point_2"
#define RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_3 @"Motalk_loading_point_3"

@interface ChatImageBubbleView ()
{
    UIImageView *_downloadAnimationImageView;
    NSMutableArray *_downloadAnimationImages;
    //
    UILabel *_tipsLabel;
}

@property (nonatomic, strong) UIImageView *loadingView;

@end

@implementation ChatImageBubbleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backImageView.hidden = YES;
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, MAX_IMAGE_WIDTH, MAX_IMAGE_HEIGHT)];
//        [_imageView setLoadOriginalImage:YES];
//
//        [_imageView setImageScaleType:@"t"];
        _imageView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        [_imageView setClipsToBounds:YES];
        [_imageView.layer setCornerRadius:3.0];
        [self addSubview:_imageView];
//        _progressLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//        _progressLabel.textColor = [UIColor whiteColor];
//        _progressLabel.backgroundColor = [UIColor clearColor];
//        _progressLabel.font = [UIFont font14];
//        _loadingView=[[UIImageView alloc] init];
//        [self addSubview:_loadingView];
//        _tipsLabel=[[UILabel alloc] init];
//        _tipsLabel.font=[UIFont systemFontOfSize:10.0];
//        _tipsLabel.text=MXLang(@"Talk_msg_group_detail_title_18", @"加载中");
//        [_loadingView addSubview:_tipsLabel];

//        _downloadAnimationImages=[[NSMutableArray alloc] initWithObjects: [UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_1],[UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_2],[UIImage imageNamed:RECEIVER_ANIMATION_IMAGEVIEW_IMAGE_3],nil];
//
//        _downloadAnimationImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
//        _downloadAnimationImageView.animationDuration=1.8;
//        _downloadAnimationImageView.animationImages=_downloadAnimationImages;
//        [_loadingView addSubview:_downloadAnimationImageView];

//        [self addSubview:_progressLabel];
        
        
    }
    
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize retSize = self.model.size;
    
    if (retSize.width == 0 || retSize.height == 0) {
        retSize.width = MAX_IMAGE_WIDTH;
        retSize.height = MAX_IMAGE_HEIGHT;
    }else if (retSize.width > MAX_IMAGE_WIDTH) {
        CGFloat height =  MAX_IMAGE_WIDTH / retSize.width  *  retSize.height;
        retSize.height = height;
        retSize.width = MAX_IMAGE_WIDTH;
        if (retSize.height > MAX_IMAGE_HEIGHT) {
            CGFloat width = MAX_IMAGE_HEIGHT / retSize.height * retSize.width;
            retSize.width = width;
            retSize.height = MAX_IMAGE_HEIGHT;
        }
    }else {
        CGFloat width = MAX_IMAGE_HEIGHT / retSize.height * retSize.width;
        retSize.width = width;
        retSize.height = MAX_IMAGE_HEIGHT;
        if (retSize.width > MAX_IMAGE_WIDTH) {
            CGFloat height =  MAX_IMAGE_WIDTH / retSize.width  *  retSize.height;
            retSize.height = height;
            retSize.width = MAX_IMAGE_WIDTH;
        }
    }
    return CGSizeMake(retSize.width + 2*BUBBLE_VIEW_PADDING + BUBBLE_ARROW_WIDTH , 2 * BUBBLE_VIEW_PADDING + retSize.height);
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.bounds;
    frame.size.width-=BUBBLE_ARROW_WIDTH;
    CGRect imageRect = CGRectInset(frame, BUBBLE_VIEW_PADDING, BUBBLE_VIEW_PADDING);
    if (!self.model.msg_direction) {
        imageRect.origin.x += BUBBLE_ARROW_WIDTH;
    }
    [self.imageView setFrame:imageRect];
    
    if (!self.model.msg_direction) {
        _loadingView.frame = imageRect;
        _tipsLabel.frame=CGRectMake(_loadingView.frame.size.width/2 - 15 - 5, _loadingView.frame.size.height/2 - 10, 30, 20);
        _downloadAnimationImageView.frame=CGRectMake(CGRectGetMaxX(_tipsLabel.frame), CGRectGetMinY(_tipsLabel.frame) + 8, 18, 4);
    }
    
}

#pragma mark - setter

- (void)setModel:(MessageModel *)model
{
    [super setModel:model];
    UIImage *image = nil;
    //发送
    if (model.msg_direction) {
        NSString *rootPath = [[ChatCacheFileUtil sharedInstance] userDocPath];
        NSString *path = [model.locFileUrl lastPathComponent];
        NSString *localUrl = StrF(@"%@/%@", rootPath, path);
        if (![StringUtil isEmpty:localUrl]) {
            image = [UIImage imageWithContentsOfFile:localUrl];
        }
        if (!image) {
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.imageRemoteUrl] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [self sizeToFit];
            }];
        }else{
            self.imageView.image = image;
            model.size = image.size;
            [self sizeToFit];
        }
        
    }
    //接收
    else{
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.imageRemoteUrl] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [self sizeToFit];
        }];
    }
    
    return;
    
//    UIImage *image = self.model.msg_direction ? self.model.image : self.model.thumbnailImage;
//    if (!image) {
//        image = self.model.image;
//        if (!image) {
//            // 有两种可能：1.自己发的 2.转发的(加载sdwebimage缓存就可以了)
//            NSString* imagePath = [[ChatCacheFileUtil sharedInstance] handleChatImagePath:model.locFileUrl remoteUrl:model.maskFileUrl];
//            image=[UIImage imageWithContentsOfFile:imagePath];
//        }
//    }
//    if (model.msg_direction) {
//
//
//        if (![StringUtil isEmpty:model.maskFileUrl]) {
//
//            NSString *imageUrl = [self appendImgPath:model.maskFileUrl];
////            UIImage* tmpImage = [PlaceHolderImageManager imageWithPlaceHolder:PlaceHolderImageTypeChatImageDownloading];
//            UIImage* tmpImage = [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 37, 47) resizingMode:UIImageResizingModeStretch];
//            [self.imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//
//                [self sizeToFit];
//            }];
//
//        }else {
//
//            self.imageView.image = image;
//            model.size = image.size;
//            [self sizeToFit];
//        }
//
//    } else {
////        NSString *imageUrl = [self appendImgPath:model.maskFileUrl];
////        [_downloadAnimationImageView startAnimating];
////        UIImage* tmpImage = [PlaceHolderImageManager imageWithPlaceHolder:PlaceHolderImageTypeChatImageDownloading];
////        tmpImage = [tmpImage resizableImageWithCapInsets:UIEdgeInsetsMake(2, 2, 37, 47) resizingMode:UIImageResizingModeStretch];
////        [self.imageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:tmpImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
////            if (image) {
////                _loadingView.hidden=YES;
////                [_downloadAnimationImageView stopAnimating];
////            }
////        }];
//    }
}

#pragma mark - public

-(void)bubbleViewPressed:(id)sender
{
    [self routerEventWithName:kRouterEventImageBubbleTapEventName
                     userInfo:@{KMESSAGEKEY:self.model}];
}


+(CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    CGSize retSize = object.size;
    
    if (retSize.width == 0 || retSize.height == 0) {
        retSize.width = MAX_IMAGE_WIDTH;
        retSize.height = MAX_IMAGE_HEIGHT;
    }else if (retSize.width > MAX_IMAGE_WIDTH) {
        CGFloat height =  MAX_IMAGE_WIDTH  *  retSize.height / retSize.width ;
        retSize.height = height;
        retSize.width = MAX_IMAGE_WIDTH;
        if (retSize.height > MAX_IMAGE_HEIGHT) {
            CGFloat width = MAX_IMAGE_HEIGHT  * retSize.width / retSize.height;
            retSize.width = width;
            retSize.height = MAX_IMAGE_HEIGHT;
        }
    }else {
        CGFloat width = MAX_IMAGE_HEIGHT  * retSize.width / retSize.height;
        retSize.width = width;
        retSize.height = MAX_IMAGE_HEIGHT;
        if (retSize.width > MAX_IMAGE_WIDTH) {
            CGFloat height =  MAX_IMAGE_WIDTH  *  retSize.height / retSize.width ;
            retSize.height = height;
            retSize.width = MAX_IMAGE_WIDTH;
        }
    }
    return 2 * BUBBLE_VIEW_PADDING + retSize.height;
}


@end
