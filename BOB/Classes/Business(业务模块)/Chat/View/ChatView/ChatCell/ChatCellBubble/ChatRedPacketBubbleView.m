//
//  ChatGoodsBubbleView.m
//  MoPromo_Develop
//
//  Created by aken on 15/12/30.
//  Copyright © 2015年 MoPromo. All rights reserved.
//

#import "ChatRedPacketBubbleView.h"
#import "MXJsonParser.h"
#define VoucherWidth [UIScreen mainScreen].bounds.size.width*0.6
static NSInteger imagewidth = 80;
static NSInteger padding = 10;
@interface ChatRedPacketBubbleView ()
//对话背景
@property (nonatomic, strong) UIView      *topView;

@property (nonatomic, strong) UIImageView      *bottomView;

@property (nonatomic, strong) UIImageView *logoImageview;

@property (nonatomic, strong) UILabel      *titleLbl;

@property (nonatomic, strong) UILabel      *subtitleLbl;

@property (nonatomic, strong) UILabel      *bottomtitleLbl;

@end

@implementation ChatRedPacketBubbleView

-(UIView* )topView{
    if (!_topView) {
        _topView = [[UIView alloc]init];
    }
    return _topView;
}

-(UIImageView* )bottomView{
    if (!_bottomView) {
        _bottomView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _bottomView.backgroundColor = [UIColor clearColor];
    }
    return _bottomView;
}

-(UIImageView* )logoImageview{
    if (_logoImageview == nil) {
        _logoImageview = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImageview.image = [UIImage imageNamed:@"icon_hongbao2_nor"];
        
    }
    return _logoImageview;
}

- (UILabel*)titleLbl {
    
    if (_titleLbl == nil) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _titleLbl.font = [UIFont font14];
        _titleLbl.numberOfLines = 0;
        _titleLbl.backgroundColor = [UIColor clearColor];
        _titleLbl.textColor = [UIColor whiteColor];
        _titleLbl.text = @"恭喜发财,大吉大利";
    }
    
    return _titleLbl;
}

- (UILabel*)subtitleLbl {
    
    if (_subtitleLbl == nil) {
        _subtitleLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _subtitleLbl.font = [UIFont font12];
        _subtitleLbl.numberOfLines = 0;
        _subtitleLbl.backgroundColor = [UIColor clearColor];
        _subtitleLbl.textColor = [UIColor whiteColor];
        _subtitleLbl.text = @"查看红包";
    }
    
    return _subtitleLbl;
}

- (UILabel*)bottomtitleLbl {
    
    if (_bottomtitleLbl == nil) {
        _bottomtitleLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _bottomtitleLbl.font = [UIFont font11];
        _bottomtitleLbl.numberOfLines = 0;
        _bottomtitleLbl.textColor = [UIColor grayColor];
        _bottomtitleLbl.backgroundColor = [UIColor clearColor];
        _bottomtitleLbl.text = @"红包";
    }
    
    return _bottomtitleLbl;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.topView];
        [self addSubview:self.bottomView];
        [self.topView addSubview:self.logoImageview];
        [self.topView addSubview:self.titleLbl];
        [self.topView addSubview:self.subtitleLbl];
        [self.bottomView addSubview:self.bottomtitleLbl];
        [self layout];
    }
    return self;
}

-(void)layout{
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.mas_top);
        make.height.equalTo(@(64));
    }];
    
    [self.logoImageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.topView.mas_centerY);
        make.width.equalTo(@(40));
        make.height.equalTo(@(40));
        make.left.equalTo(@(20));
        
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.logoImageview.mas_right).offset(padding);
        make.top.equalTo(self.logoImageview.mas_top);
        make.right.equalTo(self.topView.mas_right).offset(-padding);
        make.height.equalTo(@(20));
    }];
    
    [self.subtitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.logoImageview.mas_right).offset(padding);
        make.bottom.equalTo(self.logoImageview.mas_bottom);
        make.right.equalTo(self.topView.mas_right).offset(-padding);
        make.height.equalTo(@(20));
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.topView.mas_bottom);
        make.bottom.equalTo(self.mas_bottom);
    }];
    
    [self.bottomtitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(20));
        make.centerY.equalTo(self.bottomView.mas_centerY);
        make.right.equalTo(self.bottomView.mas_right).offset(-padding);
        make.height.equalTo(@(20));
    }];
}


+(CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    return 80;
}

-(CGSize)sizeThatFits:(CGSize)size
{
    return CGSizeMake(VoucherWidth, [self.class heightForBubbleWithObject:nil]);
}


-(void)bubbleViewPressed:(id)sender
{
    [self routerEventWithName:kRouterEventRedPacketBubbleTapEventName
                     userInfo:@{KMESSAGEKEY:self.model}];
}
#pragma mark - setter

- (void)setModel:(MessageModel *)model
{
    [super setModel:model];
    NSDictionary* body = [MXJsonParser jsonToDictionary:model.body];
    self.titleLbl.text = body[@"attr2"];
    if (model.isPlay == 1) {
         _subtitleLbl.text = @"红包已领取";
         _logoImageview.image = [UIImage imageNamed:@"icon_hongbao2_open"];
    }else if(model.isPlay == 2){
        _subtitleLbl.text = @"红包已被领完";
         _logoImageview.image = [UIImage imageNamed:@"icon_hongbao2_open"];
    }
    else{
         _subtitleLbl.text = @"查看红包";
         _logoImageview.image = [UIImage imageNamed:@"icon_hongbao2_nor"];
    }
}


@end
