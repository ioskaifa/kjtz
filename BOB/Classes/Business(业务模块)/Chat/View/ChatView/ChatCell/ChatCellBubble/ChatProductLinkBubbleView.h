//
//  ChatProductLinkBubbleView.h
//  BOB
//
//  Created by AlphaGo on 2021/1/21.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "ChatBaseBubbleView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatProductLinkBubbleView : ChatBaseBubbleView

@end

NS_ASSUME_NONNULL_END
