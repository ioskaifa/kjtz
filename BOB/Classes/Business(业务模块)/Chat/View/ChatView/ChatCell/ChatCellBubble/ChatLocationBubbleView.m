//
//  ChatLocationBubbleView.m
//  MoPal_Developer
//
//  Created by aken on 15/4/13.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatLocationBubbleView.h"

@interface ChatLocationBubbleView ()

@property (nonatomic, strong) UIImageView *locationImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) MyLinearLayout *bgView;

@end

@implementation ChatLocationBubbleView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

-(CGSize)sizeThatFits:(CGSize)size {
    return CGSizeMake(LOCATION_SIZE.width + BUBBLE_VIEW_PADDING * 2 , 2 * BUBBLE_VIEW_PADDING + LOCATION_SIZE.height);
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
//    CGRect frame = self.bounds;
//
//    _bgView.frame=CGRectMake(0, 0, CGRectGetWidth(frame)-2*BUBBLE_VIEW_PADDING-BUBBLE_ARROW_WIDTH, CGRectGetHeight(frame)-2*BUBBLE_VIEW_PADDING);
//    _titleLabel.frame = CGRectMake(10, 5, CGRectGetWidth(_bgView.frame) - 20, 20);
//    _addressLabel.frame = CGRectMake(10, 25, CGRectGetWidth(_bgView.frame) - 20, 15);
//    if (self.model.msg_direction) {
//        frame = CGRectMake(0, 45, CGRectGetWidth(frame)-2*BUBBLE_VIEW_PADDING-BUBBLE_ARROW_WIDTH,CGRectGetHeight(frame)-2*BUBBLE_VIEW_PADDING-20);
//    }  else{
//        frame = CGRectMake(0, 45/*BUBBLE_VIEW_PADDING*1*/,CGRectGetWidth(frame)-2*BUBBLE_VIEW_PADDING-BUBBLE_ARROW_WIDTH,CGRectGetHeight(frame)-2*BUBBLE_VIEW_PADDING-20);
//    }
//
//    [self.locationImageView setFrame:frame];
}

#pragma mark - setter

- (void)setModel:(MessageModel *)model {
    [super setModel:model];
    @weakify(self)
    [_locationImageView sd_setImageWithURL:[NSURL URLWithString:model.fileUrl] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        @strongify(self)
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionFade;
        transition.duration = 0.25;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [self.locationImageView.layer addAnimation:transition forKey:nil];
        model.size = self.locationImageView.image.size;
    }]; // 设置地图图片
    
    self.titleLabel.text = model.addr;
    self.addressLabel.text = model.detailAddr;
}

#pragma mark - public

-(void)bubbleViewPressed:(id)sender {
    [self routerEventWithName:kRouterEventLocationBubbleTapEventName userInfo:@{KMESSAGEKEY:self.model}];
}

+(CGFloat)heightForBubbleWithObject:(MessageModel *)object {
   
    return 2 * BUBBLE_VIEW_PADDING + LOCATION_SIZE.height;
}

- (void)createUI {
    self.backImageView.hidden = YES;
    [self addSubview:self.bgView];
    
    [self.bgView addSubview:self.locationImageView];
    [self.bgView addSubview:self.titleLabel];
    [self.bgView addSubview:self.addressLabel];
}

#pragma mark - Init
- (MyLinearLayout *)bgView {
    if (!_bgView) {
        _bgView = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            object.backgroundColor = [UIColor whiteColor];
            [object setViewCornerRadius:6];
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 10;
            object.myBottom = 5;
            object;
        });
    }
    return _bgView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font14];
            object.textColor = [UIColor blackColor];
            object.backgroundColor = [UIColor clearColor];
            object.myHeight = 20;
            object.myTop = 5;
            object.myLeft = 10;
            object.myRight = 10;
            object;
        });
    }
    return _titleLabel;
}

- (UILabel *)addressLabel {
    if (!_addressLabel) {
        _addressLabel = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#666666"];
            object.backgroundColor = [UIColor clearColor];
            object.myHeight = 15;
            object.myLeft = 10;
            object.myRight = 10;
            object.myBottom = 5;
            object;
        });
    }
    return _addressLabel;
}

- (UIImageView *)locationImageView {
    if (!_locationImageView) {
        _locationImageView = ({
            UIImageView *object = [UIImageView new];
            [object setContentMode:UIViewContentModeScaleAspectFill];
            object.clipsToBounds = YES;
            object.backgroundColor = [UIColor colorWithHexString:@"#DCDCDC"];
            object.weight = 1;
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _locationImageView;
}

@end

