//
//  ChatTextBubbleView.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/7/30.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatTextBubbleView.h"
#import <YYKit/YYKit.h>
#import "YYTextHelper.h"
@interface ChatTextBubbleView ()

@property (nonatomic, strong) YYLabel * textLabel;


@end
@implementation ChatTextBubbleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = YES;
        [self addSubview:self.textLabel];
        [self removeGestureRecognizer:self.tap];
    }
    return self;
}

- (YYLabel *)textLabel{
    
    if (_textLabel == nil) {
        _textLabel = [self.class emojiLabel];
        _textLabel.userInteractionEnabled = YES;
    }
    return _textLabel;
}

+ (YYLabel *)emojiLabel{
    
    YYLabel* yyLabel = [YYTextHelper yyLabel];
    yyLabel.font = [UIFont font17];
    return yyLabel;
}

+ (CGFloat)heightForBubbleWithObject:(MessageModel *)object {
    CGSize textSize = [self getBubbleSize:object];
    return textSize.height;
}

+(CGSize)getBubbleSize:(MessageModel *)object {
    
    NSString* content = object.content;
    CGSize textSize =  [self.class getTextSizeByContent:content];
    textSize.height = textSize.height + 2*ChatContentPadding;
    return textSize;
}

-(void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGRect frame = self.bounds;
    frame.size.width -= BUBBLE_ARROW_WIDTH;
    NSString* content = self.model.content;
    CGSize textSize = [self.class getTextSizeByContent:content];
    CGRect textLabelRect ;
    if (self.model.msg_direction) {
        textLabelRect = CGRectMake((frame.size.width-textSize.width)/2, (frame.size.height-textSize.height)/2, textSize.width, textSize.height);
    }else{
        textLabelRect = CGRectMake((frame.size.width-textSize.width)/2+BUBBLE_ARROW_WIDTH, (frame.size.height-textSize.height)/2, textSize.width, textSize.height);
    }
  
    self.textLabel.frame = textLabelRect;
}


-(CGSize)sizeThatFits:(CGSize)size {
    
    NSString* content = self.model.content;
    CGSize textSize = [self.class getTextSizeByContent:content];
    if (textSize.width <20) {
        textSize.width = 20;
    }
//    //适配气泡宽度
    textSize.height = textSize.height ;
    CGSize newSize = CGSizeMake(textSize.width+BUBBLE_ARROW_WIDTH+2*ChatContentPadding, textSize.height+2*ChatContentPadding);
    return newSize;
}


#pragma mark - setter

- (void)setModel:(MessageModel *)model {
    
    [super setModel:model];
    
    NSString* content = model.content;
        // 测试文本
    NSString *text = content;
        
        // 转成可变属性字符串
        NSMutableAttributedString * mAttributedString = [NSMutableAttributedString new];
        
        // 调整行间距段落间距
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        [paragraphStyle setLineSpacing:0];
        [paragraphStyle setParagraphSpacing:0];
        
        // 设置文本属性
    NSDictionary *attri = [NSDictionary dictionaryWithObjects:@[self.textLabel.font, model.msg_direction == 1?[UIColor whiteColor]:[UIColor blackColor], paragraphStyle] forKeys:@[NSFontAttributeName, NSForegroundColorAttributeName, NSParagraphStyleAttributeName]];
        [mAttributedString appendAttributedString:[[NSAttributedString alloc] initWithString:text attributes:attri]];
        
        // 匹配条件
        NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
        
        NSError *error = NULL;
        // 根据匹配条件，创建了一个正则表达式
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];
        if (!regex) {
            NSLog(@"正则创建失败error！= %@", [error localizedDescription]);
        } else {
            NSArray *allMatches = [regex matchesInString:mAttributedString.string options:NSMatchingReportCompletion range:NSMakeRange(0, mAttributedString.string.length)];
            for (NSTextCheckingResult *match in allMatches) {
                NSString *substrinsgForMatch2 = [mAttributedString.string substringWithRange:match.range];
                NSMutableAttributedString *one = [[NSMutableAttributedString alloc] initWithString:substrinsgForMatch2];
                // 利用YYText设置一些文本属性
                one.font = self.textLabel.font;
                one.underlineStyle = NSUnderlineStyleSingle;
                one.color = [UIColor colorWithRed:0.093 green:0.492 blue:1.000 alpha:1.000];
                
                YYTextBorder *border = [YYTextBorder new];
                border.cornerRadius = 3;
//                border.insets = UIEdgeInsetsMake(-2, -1, -2, -1);
                border.insets = UIEdgeInsetsMake(-2, 0, -2, 0);
                border.fillColor = [UIColor colorWithWhite:0.000 alpha:0.220];
                
                YYTextHighlight *highlight = [YYTextHighlight new];
                [highlight setBorder:border];
                [one setTextHighlightRange:one.rangeOfAll color:[UIColor blueColor]  backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                    NSString* urlStr = [text.string substringWithRange:range];
                    NSURL * url = [NSURL URLWithString:urlStr];
                    if ([[UIApplication sharedApplication]canOpenURL:url]) {
                        [[UIApplication sharedApplication]openURL:url options:@{UIApplicationLaunchOptionsURLKey : @YES} completionHandler:^(BOOL success) {
                            //成功后的回调
                            if (!success) {
                                //失败后的回调
                            }
                        }];
                    }else{
                        NSString* str = [NSString stringWithFormat:@"http://%@",urlStr];
                        url = [NSURL URLWithString:str];
                        [[UIApplication sharedApplication]openURL:url options:@{UIApplicationLaunchOptionsURLKey : @YES} completionHandler:^(BOOL success) {
                            //成功后的回调
                            if (!success) {
                                //失败后的回调
                                [NotifyHelper showMessageWithMakeText:@"链接无法打开"];
                            }
                        }];
                    }
                   
                }];
                
                // 根据range替换字符串
                [mAttributedString replaceCharactersInRange:match.range withAttributedString:one];
            }
        }
    _textLabel.attributedText = mAttributedString;
    [_textLabel MXupdateOuterTextProperties];
    
    YYTextContainer *container = [YYTextContainer containerWithSize:CGSizeMake(TEXTLABEL_MAX_WIDTH, MAXFLOAT)];
    YYTextLayout *textLayout = [YYTextLayout layoutWithContainer:container text:_textLabel.attributedText];
    self.textLabel.textLayout = textLayout;
    [self setNeedsLayout];
}

+ (CGSize)getTextSizeByContent:(NSString*)content {
    
    YYLabel *customLabel = [self.class emojiLabel];
    CGSize size = [YYTextHelper calculateHeight:TEXTLABEL_MAX_WIDTH font:customLabel.font text:content];
    return size;
}
@end
