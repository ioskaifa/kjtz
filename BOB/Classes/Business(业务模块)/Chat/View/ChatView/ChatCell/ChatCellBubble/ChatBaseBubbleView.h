//
//  ChatBaseBubbleView.h
//  MoPal_Developer
//
//  Created by aken on 15/3/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"
#import "UIResponder+Router.h"
#import "THProgressView.h"



//#define BUBBLE_ARROW_WIDTH 5 // bubbleView中，箭头的宽度
//#define BUBBLE_VIEW_PADDING 8 // bubbleView 与 在其中的控件内边距
//
//#define BUBBLE_RIGHT_LEFT_CAP_WIDTH 5 // 文字在右侧时,bubble用于拉伸点的X坐标
//#define BUBBLE_RIGHT_TOP_CAP_HEIGHT 35 // 文字在右侧时,bubble用于拉伸点的Y坐标
//
//#define BUBBLE_LEFT_LEFT_CAP_WIDTH 35 // 文字在左侧时,bubble用于拉伸点的X坐标
//#define BUBBLE_LEFT_TOP_CAP_HEIGHT 35 // 文字在左侧时,bubble用于拉伸点的Y坐标

#define BUBBLE_ARROW_WIDTH 8 // bubbleView中，箭头的宽度
#define BUBBLE_VIEW_PADDING 5 // bubbleView 与 在其中的控件内边距

// me send
#define BUBBLE_RIGHT_LEFT_CAP_WIDTH 15 // 文字在右侧时,bubble用于拉伸点的X坐标
#define BUBBLE_RIGHT_TOP_CAP_HEIGHT 25 // 文字在右侧时,bubble用于拉伸点的Y坐标

// recceiver
#define BUBBLE_LEFT_LEFT_CAP_WIDTH 25 // 文字在左侧时,bubble用于拉伸点的X坐标
#define BUBBLE_LEFT_TOP_CAP_HEIGHT 25 // 文字在左侧时,bubble用于拉伸点的Y坐标



#define BUBBLE_PROGRESSVIEW_HEIGHT 10 // progressView 高度

#define KMESSAGEKEY @"message"

#define KCUSTOMKEY @"custom"

@interface ChatBaseBubbleView : UIView
{
    THProgressView *_progressView;
//    MessageModel *_model;
    UIButton *_bubbleBg;
}

@property (nonatomic, strong) MessageModel *model;

@property (nonatomic, strong) UIImageView *backImageView;

@property (nonatomic, strong) THProgressView *progressView;

@property (nonatomic, strong) UITapGestureRecognizer *tap;

- (void)bubbleViewPressed:(id)sender;

+ (CGFloat)heightForBubbleWithObject:(MessageModel *)object;

@end
