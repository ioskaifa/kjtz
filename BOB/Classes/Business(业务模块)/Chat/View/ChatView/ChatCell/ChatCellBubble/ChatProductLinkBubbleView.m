//
//  ChatProductLinkBubbleView.m
//  BOB
//
//  Created by AlphaGo on 2021/1/21.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "ChatProductLinkBubbleView.h"
#import "MXJsonParser.h"
#define VoucherWidth 250
static NSInteger imagewidth = 80;
static NSInteger padding = 10;

@interface ChatProductLinkBubbleView ()
//对话背景
@property (nonatomic, strong) UIImageView *logoImageview;

@property (nonatomic, strong) UILabel      *titleLbl;

@property (nonatomic, strong) UILabel      *subtitleLbl;

@end

@implementation ChatProductLinkBubbleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

+(CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    return 90;
}

-(CGSize)sizeThatFits:(CGSize)size
{
    return CGSizeMake(VoucherWidth, [self.class heightForBubbleWithObject:nil]);
}


-(void)bubbleViewPressed:(id)sender
{
    [self routerEventWithName:kRouterEventProductInfoChatBubbleTapEventName
                     userInfo:@{KMESSAGEKEY:self.model}];
}
#pragma mark - setter

- (void)setModel:(MessageModel *)model {
    [super setModel:model];
    NSDictionary* dict = [MXJsonParser jsonToDictionary:model.body];
    NSString *avatar = dict[@"attr2"];
    if (avatar) {
        [self.logoImageview sd_setImageWithURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    }
    NSString *name = dict[@"attr3"];
    if (name) {
        self.titleLbl.text = name;
        CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(self.titleLbl.width, MAXFLOAT)];
        self.titleLbl.myHeight = size.height;
    }
    
    NSString *price = dict[@"attr4"];
    if (price) {
        self.subtitleLbl.text = [NSString stringWithFormat:@"¥%.2f",[price doubleValue]];
        CGSize size = [self.subtitleLbl sizeThatFits:CGSizeMake(self.subtitleLbl.width, MAXFLOAT)];
        self.subtitleLbl.myHeight = size.height;
    }
}


- (void)createUI {
    self.backImageView.hidden = YES;
    self.backgroundColor = [UIColor whiteColor];
    [self setViewCornerRadius:5];
    
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myTop = 0;
    bottomLayout.myLeft = 0;
    bottomLayout.myRight = 0;
    bottomLayout.myBottom = 0;
    [self addSubview:bottomLayout];
    
    [bottomLayout addSubview:self.logoImageview];
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.gravity = MyGravity_Vert_Between;
    rightLayout.myLeft = 5;
    rightLayout.myRight = 5;
    rightLayout.myTop = 15;
    rightLayout.myBottom = 15;//MyLayoutSize.wrap;
    [bottomLayout addSubview:rightLayout];
    [rightLayout addSubview:self.titleLbl];
    [rightLayout addSubview:self.subtitleLbl];
}

-(UIImageView* )logoImageview{
    if (_logoImageview == nil) {
        _logoImageview = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImageview.backgroundColor = [UIColor clearColor];
        //[_logoImageview setViewCornerRadius:6];
        _logoImageview.size = CGSizeMake(70, 70);
        _logoImageview.mySize = _logoImageview.size;
        _logoImageview.myCenterY = 0;
        _logoImageview.myLeft = 10;
    }
    return _logoImageview;
}

- (UILabel*)titleLbl {
    
    if (_titleLbl == nil) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _titleLbl.font = [UIFont systemFontOfSize:10 weight:UIFontWeightThin];
        _titleLbl.numberOfLines = 3;
        _titleLbl.backgroundColor = [UIColor clearColor];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _titleLbl.width = VoucherWidth - self.logoImageview.width - 20;
        _titleLbl.myWidth = _titleLbl.width;
    }
    
    return _titleLbl;
}

- (UILabel*)subtitleLbl {
    
    if (_subtitleLbl == nil) {
        _subtitleLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _subtitleLbl.font = [UIFont boldSystemFontOfSize:15];
        _subtitleLbl.numberOfLines = 1;
        _subtitleLbl.textColor = [UIColor blackColor];
        _subtitleLbl.width = _titleLbl.width;
        _subtitleLbl.myWidth = _subtitleLbl.width;
        _subtitleLbl.myTop = 6;
    }
    
    return _subtitleLbl;
}

@end
