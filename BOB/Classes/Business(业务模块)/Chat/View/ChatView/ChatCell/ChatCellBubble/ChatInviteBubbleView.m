//
//  ChatInviteBubbleView.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/9/1.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatInviteBubbleView.h"
#import "UIImageView+WebCache.h"
//#import "GroupManager.h"
#import "ChatGroupModel.h"
#import "LcwlChat.h"
#import "NSObject+Additions.h"
#import "MXJsonParser.h"

static int imagewidth = 50;
static int padding = 5;
@interface ChatInviteBubbleView ()
@property (nonatomic, retain) UILabel* labelTitle;
@property (nonatomic, retain) UIImageView* imageview;
@property (nonatomic, retain) UILabel* labelContent;
@property (nonatomic, retain) UILabel* labelTip;
@end
@implementation ChatInviteBubbleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _labelTitle = [[UILabel alloc]init];
        _labelTitle.textColor = [UIColor blackColor];
        _labelTitle.font = [UIFont font12];
        [self addSubview:_labelTitle];
        _labelTitle.text = MXLang(@"Talk_msg_invite_topTip_53", @"邀请你加入群聊");
        _imageview = [[UIImageView alloc]initWithFrame:CGRectZero];
        [self addSubview:_imageview];
        _labelContent = [[UILabel alloc]init];
        _labelContent.textColor = [UIColor grayColor];
        _labelContent.font = [UIFont font12];
        _labelContent.numberOfLines = 2;
        [_labelContent setLineBreakMode:NSLineBreakByCharWrapping];
        [self addSubview:_labelContent];
        _labelTip = [[UILabel alloc]init];
        _labelTip.textColor = [UIColor grayColor];
        _labelTip.font = [UIFont font12];
//        "Talk_msg_invite_click_54"="点击查看详情";
        
        _labelTip.text = MXLang(@"Talk_msg_invite_click_54", @"点击查看详情");
        [self addSubview:_labelTip];
    }
    return self;
}

+(CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    return imagewidth+15+5*padding;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.bounds;
    int width = SCREEN_WIDTH-2*(BUBBLE_LEFT_LEFT_CAP_WIDTH+40);
    _labelTitle.frame = CGRectMake(4*padding, 2*padding, CGRectGetWidth(frame)-2*padding, 15);
    _imageview.frame = CGRectMake(CGRectGetMinX(_labelTitle.frame), CGRectGetMaxY(_labelTitle.frame)+padding, imagewidth, imagewidth);
    _labelContent.frame = CGRectMake(CGRectGetMaxX(_imageview.frame)+padding, CGRectGetMaxY(_labelTitle.frame), width-(CGRectGetMaxX(_imageview.frame)+padding)-2*padding, 30);
    
    _labelTip.frame = CGRectMake(CGRectGetMinX(_labelContent.frame), CGRectGetMaxY(_labelContent.frame), _labelContent.frame.size.width, 20);
    
    
}
-(CGSize)sizeThatFits:(CGSize)size
{
    return CGSizeMake(SCREEN_WIDTH-2*(BUBBLE_LEFT_LEFT_CAP_WIDTH+40), [self.class heightForBubbleWithObject:nil]);
}


#pragma mark - setter

- (void)setModel:(MessageModel *)model
{
    [super setModel:model];
    NSDictionary* jsonDict = [MXJsonParser jsonToDictionary:model.body];
    
    NSString* groupAvatar = jsonDict[@"roomIcon"];
    if ([StringUtil isEmpty:groupAvatar]) {
        groupAvatar = nil;
    }
    
    if (![groupAvatar hasPrefix:@"http:"]) {
        groupAvatar = [self appendImgPath:groupAvatar];
    }
//    [_imageview mx_setImageWithURL:[NSURL URLWithString:groupAvatar] placeholderImage:[UIImage imageNamed:[GroupManager getGroupDefaultPic:0]]];
    NSString* nickName = jsonDict[@"nickName"];
    NSString* roomName = jsonDict[@"roomName"];
    
    NSString* content = [NSString stringWithFormat:MXLang(@"Talk_msg_invite_tip_55", @"%@邀请你加入群聊%@群"),nickName,roomName];
    _labelContent.text = content;
}
-(void)bubbleViewPressed:(id)sender
{
    [self routerEventWithName:kRouterEventInviteBubbleTapEventName
                     userInfo:@{KMESSAGEKEY:self.model}];
}

@end
