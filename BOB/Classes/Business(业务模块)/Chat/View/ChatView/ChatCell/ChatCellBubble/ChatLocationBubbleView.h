//
//  ChatLocationBubbleView.h
//  MoPal_Developer
//
//  Created by aken on 15/4/13.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatBaseBubbleView.h"


#define LOCATION_IMAGE @"motalk_location" // 显示的地图图片
#define LOCATION_IMAGEVIEW_SIZE 120 // 地图图片大小

#define LOCATION_ADDRESS_LABEL_FONT_SIZE  10 // 位置字体大小
#define LOCATION_ADDRESS_LABEL_PADDING 2 // 位置文字与外边间距
#define LOCATION_ADDRESS_LABEL_BGVIEW_HEIGHT 25 // 位置文字显示外框的高度

#define LOCATION_SIZE  CGSizeMake(SCREEN_WIDTH*0.6, SCREEN_WIDTH*0.6*(240.0/460.0))

@interface ChatLocationBubbleView : ChatBaseBubbleView

@end
