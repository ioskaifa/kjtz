//
//  ChatCardBubbleView.m
//  Lcwl
//
//  Created by mac on 2018/12/28.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "ChatCardBubbleView.h"
#import "MXJsonParser.h"
#define VoucherWidth 200
static NSInteger imagewidth = 80;
static NSInteger padding = 10;
@interface ChatCardBubbleView ()
//对话背景
@property (nonatomic, strong) UIImageView *logoImageview;

@property (nonatomic, strong) UILabel      *titleLbl;

@property (nonatomic, strong) UILabel      *subtitleLbl;

@end

@implementation ChatCardBubbleView


- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

+(CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    return 80;
}

-(CGSize)sizeThatFits:(CGSize)size
{
    return CGSizeMake(VoucherWidth, [self.class heightForBubbleWithObject:nil]);
}


-(void)bubbleViewPressed:(id)sender
{
    [self routerEventWithName:kRouterEventCardBubbleTapEventName
                     userInfo:@{KMESSAGEKEY:self.model}];
}
#pragma mark - setter

- (void)setModel:(MessageModel *)model {
    [super setModel:model];
    NSDictionary* dict = [MXJsonParser jsonToDictionary:model.body];
    NSString *avatar = dict[@"attr2"];
    if (avatar) {
        avatar = StrF(@"%@/%@", UDetail.user.qiniu_domain, avatar);
        [self.logoImageview sd_setImageWithURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    }
    NSString *name = dict[@"attr3"];
    if (name) {
        self.titleLbl.text = name;
        CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(self.titleLbl.width, MAXFLOAT)];
        self.titleLbl.myHeight = size.height;
    }
    
    NSString *ID = dict[@"attr1"];
    if (ID) {
        self.subtitleLbl.text = [StringUtil formatUserID:ID];
        CGSize size = [self.subtitleLbl sizeThatFits:CGSizeMake(self.subtitleLbl.width, MAXFLOAT)];
        self.subtitleLbl.myHeight = size.height;
    }
}


- (void)createUI {
    self.backImageView.hidden = YES;
    self.backgroundColor = [UIColor whiteColor];
    [self setViewCornerRadius:5];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font11];
    lbl.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
    lbl.text = @"个人名片";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 5;
    lbl.myLeft = 5;
    [layout addSubview:lbl];
    UIView *line = [UIView fullLine];
    line.myTop = 5;
    [layout addSubview:line];
    
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myTop = 0;
    bottomLayout.myLeft = 0;
    bottomLayout.myRight = 0;
    bottomLayout.weight = 1;
    [layout addSubview:bottomLayout];
    
    [bottomLayout addSubview:self.logoImageview];
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myLeft = 5;
    rightLayout.myRight = 5;
    rightLayout.weight = 1;
    rightLayout.myHeight = MyLayoutSize.wrap;
    rightLayout.myCenterY = 0;
    [bottomLayout addSubview:rightLayout];
    [rightLayout addSubview:self.titleLbl];
    [rightLayout addSubview:self.subtitleLbl];
}

-(UIImageView* )logoImageview{
    if (_logoImageview == nil) {
        _logoImageview = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImageview.backgroundColor = [UIColor clearColor];
        [_logoImageview setViewCornerRadius:6];
        _logoImageview.size = CGSizeMake(40, 40);
        _logoImageview.mySize = _logoImageview.size;
        _logoImageview.myCenterY = 0;
        _logoImageview.myLeft = 5;
    }
    return _logoImageview;
}

- (UILabel*)titleLbl {
    
    if (_titleLbl == nil) {
        _titleLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _titleLbl.font = [UIFont font15];
        _titleLbl.numberOfLines = 0;
        _titleLbl.backgroundColor = [UIColor clearColor];
        _titleLbl.textColor = [UIColor blackColor];
        _titleLbl.width = VoucherWidth - self.logoImageview.width - 15;
        _titleLbl.myWidth = _titleLbl.width;
    }
    
    return _titleLbl;
}

- (UILabel*)subtitleLbl {
    
    if (_subtitleLbl == nil) {
        _subtitleLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _subtitleLbl.font = [UIFont font11];
        _subtitleLbl.numberOfLines = 0;
        _subtitleLbl.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
        _subtitleLbl.width = _titleLbl.width;
        _subtitleLbl.myWidth = _subtitleLbl.width;
        _subtitleLbl.visibility = MyVisibility_Gone;
    }
    
    return _subtitleLbl;
}

@end
