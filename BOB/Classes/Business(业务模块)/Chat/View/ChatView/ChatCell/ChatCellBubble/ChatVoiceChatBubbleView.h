//
//  ChatVoiceChatBubbleView.h
//  BOB
//
//  Created by mac on 2020/7/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChatBaseBubbleView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatVoiceChatBubbleView : ChatBaseBubbleView

@end

NS_ASSUME_NONNULL_END
