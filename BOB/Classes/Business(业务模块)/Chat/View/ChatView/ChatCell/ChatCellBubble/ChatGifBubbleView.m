//
//  ChatGifBubbleView.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/7/30.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatGifBubbleView.h"
#import "MXEmotionExManager.h"
#import "MXEmotionPackage.h"
#import "MXEmotionDetails.h"
#import "MXEmotionDownload.h"
#import "EmotionManagerConfig.h"

#import "FLAnimatedImage.h"
#import "PlaceHolderImageManager.h"
#import "MXJsonParser.h"
#import "SDWebImageDownloader.h"

@interface ChatGifBubbleView ()

@property (nonatomic, strong) NSMutableDictionary *gifDic;
@property (nonatomic, strong) FLAnimatedImageView *faceImageView;
@property (nonatomic, strong) UILabel *tipsLabel;
@property (nonatomic, strong) UIButton *retryButton;


@end
@implementation ChatGifBubbleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        _faceImageView = [[FLAnimatedImageView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];
//        _faceImageView.userInteractionEnabled = YES;
        _faceImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_faceImageView];
        
        _tipsLabel=[[UILabel alloc] init];
        _tipsLabel.font=[UIFont systemFontOfSize:10.0];
        _tipsLabel.text=@"加载失败";
        _tipsLabel.hidden = YES;
        _tipsLabel.textAlignment = NSTextAlignmentCenter;
        [_faceImageView addSubview:_tipsLabel];
        
        _retryButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _retryButton.frame = CGRectMake(CGRectGetMaxX(_faceImageView.frame) + 20, 27.5, 25, 25);
        [_retryButton addTarget:self action:@selector(redownloadGif) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Motak_reload_icon" ofType:@"jpg"];
        UIImage *myImage = [UIImage imageWithContentsOfFile:path];
        [_retryButton setBackgroundImage:myImage forState:UIControlStateNormal];
        
        _retryButton.hidden = YES;
        [self addSubview:_retryButton];
        
        self.backImageView.hidden = YES;
        _gifDic = [NSMutableDictionary dictionary];
        [self loadGif];
    }
    return self;
}

- (void)dealloc {
    if (_gifDic) {
        [_gifDic removeAllObjects];
        _gifDic = nil;
    }
}

- (void)loadGif {
    
    
    NSArray* tmpArray =  [[MXEmotionManager manager] defaultGifs];
    for (NSDictionary* dict  in tmpArray) {
        NSString* faceName = dict[@"faceName"];
        NSString* faceImageName = dict[@"faceImageName"];
        [_gifDic setObject:faceImageName forKey:faceName];
    }
//    NSArray *array = [[MXEmotionExManager manager] loadEmotionsPackages];
//    for (MXEmotionPackage *package in array) {
//        [self fetchGifEmotionsWithPackage:package];
//    }
}

- (void)fetchGifEmotionsWithPackage:(MXEmotionPackage*)package {
    
    if (package) {
        
        
        @autoreleasepool {
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            for (MXEmotionDetails *emtionDetail in package.emotions) {
                
                if (emtionDetail) {
                    
                    NSString *gifName = emtionDetail.emotionImagePath;
                    NSString *fileName = [self stringByDeletingExtension:package.fileUrl];
                    
                    NSString *path = [[MXEmotionDownload sharedInstance] unZipPath];
                    NSString *unzipPath = @"";
                    
                    
                    NSString *nextUnzipPath = [NSString stringWithFormat:@"%@/%@/%@/%@",path,fileName,fileName,emtionDetail.emotionImagePath];
                    BOOL isDirExist = [fileManager fileExistsAtPath:nextUnzipPath];
                    if(isDirExist){
                        unzipPath = nextUnzipPath;
                    }else {
                        unzipPath = [NSString stringWithFormat:@"%@/%@/%@",path,fileName,emtionDetail.emotionImagePath];
                    }
                    
                    [_gifDic setValue:unzipPath forKey:gifName];
                    
                }
            }
            
        }
    }
}

+(CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    return 140;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
}
-(CGSize)sizeThatFits:(CGSize)size
{
    if (self.model.msg_direction) {
        return CGSizeMake(130,120);
    }
    return CGSizeMake(175,120);
}

- (void)redownloadGif {
    
    if (!_tipsLabel.hidden) {
       
        [self downloadGifWith:self.model.content];
         MLog(@"self.mode:%@",self.model.content);
    }
}

- (void)downloadGifWith:(NSString*)url {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:url] options:SDWebImageDownloaderUseNSURLCache progress:nil completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            
            if (!error && finished) {
                
                FLAnimatedImage *gifImage = [FLAnimatedImage animatedImageWithGIFData:data];
                _faceImageView.animatedImage = gifImage;
                _tipsLabel.hidden = YES;
                _retryButton.hidden = YES;
            }else {
                _faceImageView.image = [PlaceHolderImageManager imageWithPlaceHolder:PlaceHolderImageTypeChatImageDownFailure];
                _tipsLabel.hidden = NO;
                _retryButton.hidden = NO;
            }
        }];
    });
}

#pragma mark - setter

- (void)setModel:(MessageModel *)model
{
    [super setModel:model];
    NSString* path = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"MXChatToolBar.bundle"] ;
    NSDictionary* dict = [MXJsonParser jsonToDictionary:self.model.body];
    NSString* attr1 = dict[@"attr1"];
    NSString* imagePath = [NSString stringWithFormat:@"%@/Emotion/gif/%@",path,attr1];
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfFile:imagePath]];
    _faceImageView.animatedImage = image;
    if (model.msg_direction) {
        _faceImageView.frame = CGRectMake(0, 0, 120, 120);
    }else{
        _faceImageView.frame = CGRectMake(10, 0, 120, 120);
        _tipsLabel.frame=CGRectMake(_faceImageView.frame.size.width/2 - 30, _faceImageView.frame.size.height/2 - 10, 60, 20);
    }
}

- (NSString*)stringByDeletingExtension:(NSString*)filePath {
    
    NSString *fileName = nil;
    
    if (filePath) {
        
        if ([filePath rangeOfString:@"."].location !=NSNotFound) {
            
            NSArray *tmpArray = [filePath componentsSeparatedByString:@"."];
            fileName = tmpArray[0];
        }
    }
    
    return fileName;
}


@end
