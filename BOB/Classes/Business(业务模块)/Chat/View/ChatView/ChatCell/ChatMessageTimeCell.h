//
//  ChatMessageTimeCell.h
//  MoPal_Developer
//
//  Created by lixinglou on 15/10/17.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatMessageTimeCell : UITableViewCell

-(void)reloadTime:(NSString*)time;
@end
