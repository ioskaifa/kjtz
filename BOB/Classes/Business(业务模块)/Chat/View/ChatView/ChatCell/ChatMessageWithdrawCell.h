//
//  ChatMessageWithdrawCell.h
//  BOB
//
//  Created by mac on 2020/8/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatMessageWithdrawCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(MessageModel *)obj;

@end

NS_ASSUME_NONNULL_END
