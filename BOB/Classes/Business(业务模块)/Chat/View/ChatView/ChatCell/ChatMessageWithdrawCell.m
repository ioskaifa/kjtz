//
//  ChatMessageWithdrawCell.m
//  BOB
//
//  Created by mac on 2020/8/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChatMessageWithdrawCell.h"
#import "LcwlChat.h"

@interface ChatMessageWithdrawCell ()

@property (nonatomic, strong) UILabel *contentLbl;

@end

@implementation ChatMessageWithdrawCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 44;
}

- (void)configureView:(MessageModel *)obj {
    if (![obj isKindOfClass:MessageModel.class]) {
        return;
    }
    if (obj.subtype != kMXMessageTypeWithdraw) {
        return;
    }
    self.contentLbl.text = [obj formatWithdrawContentTxt];
    [self.contentLbl sizeToFit];
    self.contentLbl.mySize = self.contentLbl.size;
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    [layout addSubview:self.contentLbl];
}

#pragma mark - Init
- (UILabel *)contentLbl {
    if (!_contentLbl) {
        _contentLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor lightGrayColor];
            object.textAlignment = NSTextAlignmentCenter;
            object.myCenter = CGPointZero;
            object;
        });
    }
    return _contentLbl;
}


@end
