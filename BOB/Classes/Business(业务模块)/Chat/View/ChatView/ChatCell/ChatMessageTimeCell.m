//
//  ChatMessageTimeCell.m
//  MoPal_Developer
//
//  Created by lixinglou on 15/10/17.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "ChatMessageTimeCell.h"

static const int margin_chat_time=5;

@interface ChatMessageTimeCell()

@property (nonatomic, strong) UILabel *timeLbl;

@end

@implementation ChatMessageTimeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self addSubview:self.timeLbl];
        self.backgroundColor=[UIColor clearColor];
        self.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(UILabel*)timeLbl{
    if(!_timeLbl){
        _timeLbl=[[UILabel alloc]initWithFrame:CGRectZero];
        [[_timeLbl layer]setCornerRadius:5.0f];
        [[_timeLbl layer]setMasksToBounds:YES];
        [[_timeLbl layer]setBorderWidth:0];
        [_timeLbl setBackgroundColor:[UIColor clearColor]];
        [_timeLbl setFont:[UIFont font11]];
        [_timeLbl setTextColor:[UIColor lightGrayColor]];
        [_timeLbl setTextAlignment:NSTextAlignmentCenter];
    }
    return _timeLbl;
}

- (void)reloadTime:(NSString *)time {
//    CGSize textSize = [time sizeWithFont:self.timeLbl.font constrainedToSize:CGSizeMake(MAXFLOAT, 20) lineBreakMode:NSLineBreakByCharWrapping];
    if ([StringUtil isEmpty:time]) {
        return;
    }
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:time];
    
    NSRange allRange = [time rangeOfString:time];
    [attrStr addAttribute:NSFontAttributeName
                    value:self.timeLbl.font
                    range:allRange];
    [attrStr addAttribute:NSForegroundColorAttributeName
                    value:self.timeLbl.textColor
                    range:allRange];
    NSStringDrawingOptions options =  NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    CGRect rect = [attrStr boundingRectWithSize:CGSizeMake(MAXFLOAT, 20)
                                        options:options
                                        context:nil];
   CGSize textSize = rect.size;

    
    self.timeLbl.text = time;
    @weakify(self)
    [self.timeLbl mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.center.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(textSize.width+margin_chat_time*2, 20));
    }];
    
    [self layoutIfNeeded];
}
@end
