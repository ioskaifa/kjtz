//
//  ChartBarMoreButton.m
//  MoPal_Developer
//
//  Created by aken on 15/9/29.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChartBarMoreButton.h"

@implementation ChartBarMoreButton

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    
    UIImageView *imageView = [self imageView];
    CGRect imageFrame = imageView.frame;
    imageFrame.size.width=54;
    imageFrame.size.height=54;
    imageFrame.origin.y=10;
    imageFrame.origin.x = self.frame.size.width/2 - imageFrame.size.width/2;
    imageView.frame = imageFrame;
    
    UILabel *titleLabel = [self titleLabel];
    CGRect titleFrame = titleLabel.frame;
    titleFrame.size.width=self.frame.size.width;
    titleFrame.size.height=14;
    titleFrame.origin.x = ((self.frame.size.width - titleFrame.size.width)/ 2);
    titleFrame.origin.y=CGRectGetMaxY(imageView.frame) + 5 ;
    titleLabel.frame = titleFrame;
    titleLabel.textAlignment = NSTextAlignmentCenter;
}

@end