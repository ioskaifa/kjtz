//
//  ChatForwardCell.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/4/22.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatForwardCell.h"
#import "UserModel.h"
#import "TalkManager.h"
#import "UIImageView+WebCache.h"
#import "MessageModel.h"
#import "FriendModel.h"
#import "LcwlChat.h"
#import "MXConversation.h"
#import "PlaceHolderImageManager.h"
#import "NSObject+Additions.h"

@implementation ChatForwardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.headImg.layer setCornerRadius:25];
    [self.headImg.layer setMasksToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)reloadData:(id)object{
    if ([object isKindOfClass:[FriendModel class]]) {
        FriendModel* tmp = (FriendModel*)object;
//        [self.headImg mx_setImageWithURL:[NSURL URLWithString:tmp.avatar] placeholderImage:[PlaceHolderImageManager imageWithPlaceHolder:PlaceHolderImageTypeUserAvatar]];
        
        
        NSString* name = tmp.remark;
        if ([StringUtil isEmpty:name]) {
            name = tmp.name;
        }
        self.name.text = name;

    }else if([object isKindOfClass:[MXConversation class]]){
        MXConversation* conversation = (MXConversation*)object;
        if (conversation.conversationType == eConversationTypeChat ) {
            [self setNomalConversation:conversation];
        }
//        else if(conversation.conversationType == eConversationTypeNoticeChat){
//            [self setNoticeConversation:conversation];
//        }
//        else if(conversation.conversationType == eConversationTypeGroupChat){
//            [self setGroupConversation:conversation];
//        }
    }
}

-(void)setNomalConversation:(MXConversation*) conversation{
    FriendModel* user = [[LcwlChat shareInstance].chatManager loadFriendByChatId:conversation.chat_id];
//    [self.headImg mx_setImageWithURL:[NSURL URLWithString:user.headUrl] placeholderImage:[PlaceHolderImageManager imageWithPlaceHolder:PlaceHolderImageTypeUserAvatar]];
    NSString* name = user.remark;
    if ([StringUtil isEmpty:name]) {
        name = user.name;
    }
    self.name.text = name;

}


-(void)setGroupConversation:(MXConversation*) conversation{
//    ChatGroupModel* group = [[LcwlChat sharedInstance].chatManager loadGroupByChatId:conversation.chat_id];
//    if (group != nil) {
//        NSString* headUrl = group.groupAvatar;
//        if ([StringUtil isEmpty:headUrl]) {
//            headUrl = nil;
//        }
//        if (![headUrl hasPrefix:@"http:"]) {
//            headUrl = [self appendImgPath:headUrl];
//        }
//       [self.headImg mx_setImageWithURL:[NSURL URLWithString:headUrl] placeholderImage:[PlaceHolderImageManager imageWithPlaceHolder:PlaceHolderImageTypeUserAvatar]];
//    }
//    NSString* name = group.groupName;
//    if ([StringUtil isEmpty:name]) {
//        name = @"";
//    }
//    self.name.text = name;
}

//-(void)setNoticeConversation:(MXConversation*) conversation{
//    MoYouModel* user = [[MXChat sharedInstance].chatManager loadFriendByChatId:conversation.chat_id];
//    [self.headImg mx_setImageWithURL:[NSURL URLWithString:user.headUrl] placeholderImage:[PlaceHolderImageManager imageWithPlaceHolder:PlaceHolderImageTypeUserAvatar]];
//    NSString* name = user.remark;
//    if ([StringUtil isEmpty:name]) {
//        name = user.name;
//    }
//    self.name.text = name;
//}

@end
