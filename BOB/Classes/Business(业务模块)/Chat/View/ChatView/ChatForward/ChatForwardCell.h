//
//  ChatForwardCell.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/4/22.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatForwardCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *name;
-(void)reloadData:(id)model;
@end
