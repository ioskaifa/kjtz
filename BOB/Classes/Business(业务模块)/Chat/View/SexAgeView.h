//
//  SexAgeView.h
//  MoPal_Developer
//
//  Created by Fly on 15/10/13.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "MPBaseView.h"

@interface SexAgeView : MPBaseView

- (void)setGenderType:(MoGenderType)genderType
          withUserAge:(NSString*)userAge
        withIsShowAge:(BOOL)isShow;

- (void)refreshGenderType:(MoGenderType)genderType;

@end
