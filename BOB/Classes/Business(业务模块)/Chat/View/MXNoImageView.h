//
//  MXNoImageView.h
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/10/26.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MXNoImageView : UIView

+ (UIImage *)imageSize:(CGSize)size;

@end
