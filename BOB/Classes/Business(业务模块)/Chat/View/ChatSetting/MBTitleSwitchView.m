//
//  MBTitleSwitchView.m
//  MoPromo_Develop
//
//  Created by yang.xiangbao on 15/5/18.
//  Copyright (c) 2015年 MoPromo. All rights reserved.
//

#import "MBTitleSwitchView.h"

@interface MBTitleSwitchView ()

@property (nonatomic, strong) UILabel  *titleLbl;
@property (nonatomic, strong) UIButton *tapBtn;

@end

@implementation MBTitleSwitchView

- (instancetype)initWithFrame:(CGRect)frame
                        param:(void (^)(UILabel *titel, UISwitch *sw))param
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.titleLbl];
        
        [self addSubview:self.switchBtn];
        
        if (param) {
            param(self.titleLbl, self.switchBtn);
        }
        
        UIView *superView = self;
        [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(superView.mas_centerY);
            make.left.equalTo(superView.mas_left).offset(15);
            make.right.equalTo(self.switchBtn.mas_left).offset(-10);
        }];
        
        [self.switchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(superView.mas_right).offset(-8);
            make.centerY.equalTo(superView.mas_centerY);
        }];
    }
    return self;
}

- (void)setTitleText:(NSString*)text
{
    self.titleLbl.text = text;
}

- (void)setSwitchOn:(BOOL)isOn
{
    self.switchBtn.on = isOn;
}

- (BOOL)switchIsOn
{
    return self.switchBtn.isOn;
}

- (void)switchEnable:(BOOL)enable
{
    [self.switchBtn setUserInteractionEnabled:enable];
}

#pragma mark - Event
- (void)tapAction:(UIButton*)sender
{
    if (self.tapAction) {
        self.tapAction(sender);
    }
}

- (void)switchAction:(UISwitch *)sender
{
    if (self.switchAction) {
        self.switchAction(sender);
    }
}

#pragma mark - UI
- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        [_titleLbl setTextColor:[UIColor blackColor]];
        [_titleLbl setFont:[UIFont font17]];
    }
    
    return _titleLbl;
}

- (UISwitch *)switchBtn
{
    if (!_switchBtn) {
        _switchBtn = [[UISwitch alloc] initWithFrame:CGRectZero];
//        [_switchBtn setOnTintColor:[UIColor colorWithRed:.54f green:.38f blue:.77f alpha:1.0]];
          [_switchBtn setOnTintColor:[UIColor moBlueColor]];
        [_switchBtn addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    }
    
    return _switchBtn;
}

- (UIButton *)tapBtn
{
    if (!_tapBtn) {
        _tapBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_tapBtn addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _tapBtn;
}

@end
