//
//  MBTitleSwitchCell.m
//  MoPromo_Develop
//
//  Created by yang.xiangbao on 15/5/18.
//  Copyright (c) 2015年 MoPromo. All rights reserved.
//

#import "MBTitleSwitchCell.h"

@interface MBTitleSwitchCell ()


@end

@implementation MBTitleSwitchCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.titleSwitchView];
        
        [self.titleSwitchView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView).insets(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    
    return self;
}

- (BOOL)isOn
{
    return [self.titleSwitchView switchIsOn];
}

- (void)configureTitle:(NSString*)title isOn:(BOOL)isOn
{
    [self.titleSwitchView setTitleText:title];
    [self.titleSwitchView setSwitchOn:isOn];
}

- (void)switchEnable:(BOOL)enable
{
    [self.titleSwitchView switchEnable:enable];
}

- (MBTitleSwitchView *)titleSwitchView
{
    if (!_titleSwitchView) {
        _titleSwitchView = [[MBTitleSwitchView alloc]
                            initWithFrame:CGRectZero
                            param:nil];
        @weakify(self)
        _titleSwitchView.switchAction = ^(UISwitch *sender) {
            @strongify(self)
            if (self.switchAction) {
                self.switchAction(self.index, sender);
            }
        };
    }
    
    return _titleSwitchView;
}

@end
