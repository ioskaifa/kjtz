//
//  MBTitleSwitchView.h
//  MoPromo_Develop
//
//  Created by yang.xiangbao on 15/5/18.
//  Copyright (c) 2015年 MoPromo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TapBlock)(UIButton *sender);
typedef void(^SwitchBlock)(UISwitch *sender);

@interface MBTitleSwitchView : UIView

/**
 *  点击回调
 */
@property (nonatomic, strong) TapBlock tapAction;

/**
 *  UISwitch 开关回调
 */
@property (nonatomic, strong) SwitchBlock switchAction;

/**
 *  初始化方法
 *
 *  @param frame 大小
 *  @param param 对控件的初始化
 *
 *  @return 实例
 */
- (instancetype)initWithFrame:(CGRect)frame
                        param:(void (^)(UILabel *titel, UISwitch *sw))param;

// 设置文字内容
- (void)setTitleText:(NSString*)text;

// 设置按钮开关状态
- (void)setSwitchOn:(BOOL)isOn;

// 读取按钮开关状态
- (BOOL)switchIsOn;

- (void)switchEnable:(BOOL)enable;


@property (nonatomic, strong) UISwitch *switchBtn;
@end
