//
//  MBTitleSwitchCell.h
//  MoPromo_Develop
//
//  Created by yang.xiangbao on 15/5/18.
//  Copyright (c) 2015年 MoPromo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBTitleSwitchView.h"
typedef void (^SwitchStateChangeBlock)(NSIndexPath *index, UISwitch *sender);

@interface MBTitleSwitchCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *index;

// UISwitch 开关回调
@property (nonatomic, strong) SwitchStateChangeBlock switchAction;
@property (nonatomic, strong) MBTitleSwitchView *titleSwitchView;

- (BOOL)isOn;
- (void)configureTitle:(NSString*)title isOn:(BOOL)isOn;
- (void)switchEnable:(BOOL)enable;

@end
