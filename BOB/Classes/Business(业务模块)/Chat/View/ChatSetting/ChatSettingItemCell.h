//
//  ChatSettingItemCell.h
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatSettingItemCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *logoImgView;
@property (nonatomic, strong) UILabel *nameLbl;
-(void)reloadImg:(NSString* )path name:(NSString* )name;
@end

NS_ASSUME_NONNULL_END
