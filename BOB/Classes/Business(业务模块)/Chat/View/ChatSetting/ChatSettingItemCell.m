//
//  ChatSettingItemCell.m
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "ChatSettingItemCell.h"
static int width = 52;
@interface ChatSettingItemCell ()

@end

@implementation ChatSettingItemCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

#pragma mark - UI
- (void)initUI
{
    self.backgroundColor = [UIColor whiteColor];
 
    [self addSubview:self.logoImgView];
    [self addSubview:self.nameLbl];
    
    [self layoutView];
}

-(void)layoutView{
    @weakify(self)
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerY.equalTo(self.mas_centerY).offset(-10);
        make.centerX.equalTo(self.mas_centerX);
        make.width.height.equalTo(@(width));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.logoImgView.mas_bottom).offset(5);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.equalTo(@(20));
        
    }];
    return;
}
- (UIImageView *)logoImgView
{
    if (!_logoImgView) {
        _logoImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImgView.layer.masksToBounds = YES;
        _logoImgView.layer.cornerRadius =  4;
        
    }
    
    return _logoImgView;
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLbl.textAlignment = NSTextAlignmentCenter;
        _nameLbl.text = @"主标题";
        _nameLbl.font = [UIFont font11];
        _nameLbl.textColor = [UIColor lightGrayColor];
    }
    
    return _nameLbl;
}

-(void)reloadImg:(NSString* )path name:(NSString* )name{
    if ([path isEqualToString:@"+"]) {
        self.logoImgView.image = [UIImage imageNamed:@"chat_add_member"];
    }else if ([path isEqualToString:@"-"]) {
        self.logoImgView.image = [UIImage imageNamed:@"chat_remove_member"];
    }else{
        
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/120/h/120",path];
        [self.logoImgView sd_setImageWithURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    }
    self.nameLbl.text = name;
}
@end
