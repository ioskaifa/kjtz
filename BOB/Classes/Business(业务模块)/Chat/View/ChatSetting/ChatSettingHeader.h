//
//  ChatSettingHeader.h
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^AvatarClickBlock)(id sender);

@class ChatSettingHeaderVM;
@interface ChatSettingHeader : UIView

@property (nonatomic , copy) AvatarClickBlock  block;
- (instancetype)initWithFrame:(CGRect)frame dataArray:(ChatSettingHeaderVM* )vm;

- (void)reloadData:(ChatSettingHeaderVM*)vm;

@end

NS_ASSUME_NONNULL_END
