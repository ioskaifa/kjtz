//
//  ChatSettingHeader.m
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "ChatSettingHeader.h"
#import "ChatSettingItemCell.h"
#import "ChatSettingHeaderVM.h"
#import "FriendModel.h"
#import "LcwlChat.h"

static int itemHeight = 100;
@interface  ChatSettingHeader()<UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) ChatSettingHeaderVM* vm;

@end
@implementation ChatSettingHeader

- (instancetype)initWithFrame:(CGRect)frame dataArray:(ChatSettingHeaderVM* )vm{
    self = [super initWithFrame:frame];
    if (self) {
        self.vm = vm;
        self.backgroundColor = [UIColor whiteColor];
        [self setupUI];
    }
    return self;
}
- (void)setupUI{
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
        CGFloat width = SCREEN_WIDTH / 5;
        [layout setItemSize:CGSizeMake(width, itemHeight)];
        [layout setMinimumInteritemSpacing:0];
        [layout setMinimumLineSpacing:0];
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView setScrollEnabled:NO];
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView registerClass:[ChatSettingItemCell class] forCellWithReuseIdentifier:@"ChatSettingItemCell"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
    }
    
    return _collectionView;
}



#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.vm.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatSettingItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatSettingItemCell" forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor clearColor];
    id object = self.vm.dataArray[indexPath.row];
    
    if ([object isKindOfClass:[NSString class]]) {
        NSString* tmp = (NSString *)object;
        if ([tmp isEqualToString:@"+"]) {
            [cell reloadImg:tmp name:@"添加"];
        }else if([tmp isEqualToString:@"-"]){
            [cell reloadImg:tmp name:@"移除"];
        }
    }else{
        if ([object isKindOfClass:[FriendModel class]]) {
            FriendModel* obj = (FriendModel*)object;
            NSString* key = [NSString stringWithFormat:@"nickname_%@",obj.groupid];
            NSInteger show = [[[NSUserDefaults standardUserDefaults] objectForKey:key]boolValue];
            if (show) {
                if ([obj.userid isEqualToString:[LcwlChat shareInstance].user.chatUser_id]) {
                    ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:obj.groupid];
                    [cell reloadImg:obj.avatar name:group.nickname];
                }else{
                    [cell reloadImg:obj.avatar name:obj.groupNickname];
                }
            }else{
                [cell reloadImg:obj.avatar name:obj.remark ?: obj.name];
            }
        }
    }
    return cell;
}
- (void)reloadData:(ChatSettingHeaderVM*)vm{
    self.vm = vm;
    CGRect rect = self.frame;
    rect.size.height = self.vm.dataArray.count/5*(itemHeight)+(self.vm.dataArray.count%5>0?itemHeight:0);
    self.frame = rect;
    [self.collectionView reloadData];
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    id model = [self.vm.dataArray objectAtIndex:indexPath.row];
    if (self.block) {
        self.block(model);
    }
}

@end
