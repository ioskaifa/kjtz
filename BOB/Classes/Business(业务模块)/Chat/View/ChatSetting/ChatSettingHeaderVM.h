//
//  ChatSettingHeaderVM.h
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatSettingHeaderVM : NSObject
@property (nonatomic , strong) NSMutableArray* dataArray;
@end

NS_ASSUME_NONNULL_END
