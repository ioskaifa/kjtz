//
//  LSearchBar.h
//  Lcwl
//
//  Created by 王刚  on 2018/11/19.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger,PlaceHolderPositionType) {
    PlaceHolderPositionType_Manual,
    PlaceHolderPositionType_Auto,
};

@interface LSearchBar : UISearchBar
@property(nonatomic, assign) PlaceHolderPositionType positionType;
@end

NS_ASSUME_NONNULL_END
