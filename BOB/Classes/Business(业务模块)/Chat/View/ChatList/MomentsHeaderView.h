//
//  MomentsHeaderView.h
//  Lcwl
//
//  Created by mac on 2018/11/25.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSearchBar.h"
NS_ASSUME_NONNULL_BEGIN

@interface MomentsHeaderView : UIView
@property (nonatomic, strong) LSearchBar *searchBar;
-(void)laytoutByStatus:(int)status;
@end

NS_ASSUME_NONNULL_END
