//
//  MXSwipeTableViewCell.h
//  MoPal_Developer
//
//  Created by aken on 16/1/13.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MXSwipeTableViewCellDelegate <NSObject>

@required
- (void)mxSwipeTableViewCelldidSelectBtnWithTag:(NSInteger)tag
                                   andIndexPath:(NSIndexPath *)indexpath;

@optional
- (void)mxSwipeCellOptionBtnWillShow;
- (void)mxSwipeCellOptionBtnWillHide;
- (void)mxSwipeCellOptionBtnDidShow;
- (void)mxSwipeCellOptionBtnDidHide;

@end

@interface MXSwipeTableViewCell : UITableViewCell

// 右边滑动按钮数据
@property (nonatomic, strong) NSArray        *rightButtons;

@property (nonatomic, strong) UIView         *customContentView;

@property (nonatomic, assign, readonly) BOOL isRightBtnShow;

@property (nonatomic, weak) id<MXSwipeTableViewCellDelegate>delegate;

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier
          tableView:(UITableView *)tableView
        marginRight:(CGFloat)marginRight height:(NSInteger)height;

@end