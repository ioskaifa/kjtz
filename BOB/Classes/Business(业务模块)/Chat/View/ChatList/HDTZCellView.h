//
//  HDTZCellView.h
//  Lcwl
//
//  Created by mac on 2019/1/23.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class HDTZModel;
@interface HDTZCellView : UIView

-(void)reloadData:(HDTZModel *)model;

@end

NS_ASSUME_NONNULL_END
