//
//  MomentsHeaderView.m
//  Lcwl
//
//  Created by mac on 2018/11/25.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "MomentsHeaderView.h"

@interface MomentsHeaderView()
@property (nonatomic, strong) UIImageView *logoImgView;
@property (nonatomic, strong) UILabel *nameLbl;


@property (nonatomic, strong) UIView *backView;
@end
@implementation MomentsHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self layoutViews];
    }
    return self;
}
-(void)initUI{
    [self addSubview:self.searchBar];
    self.backView = [[UIView alloc] initWithFrame:CGRectMake(13, 54, SCREEN_WIDTH-26, 44)];
    self.backView.backgroundColor = [UIColor colorWithRed:253.0/255.0 green:110.0/255.0 blue:129.0/255.0 alpha:1.0];
    self.backView.layer.cornerRadius = 10;
    [self addSubview:self.backView];
    
    //CGRectMake(10, (_networkStateView.frame.size.height - 20) / 2, 20, 20)
    self.logoImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.logoImgView.image = [UIImage imageNamed:@"nim_g_ic_failed_small"];
    [self.backView addSubview:self.logoImgView];
    
    self.nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
    self.nameLbl.font = [UIFont systemFontOfSize:15.0];
    self.nameLbl.textColor = [UIColor whiteColor];
    self.nameLbl.backgroundColor = [UIColor clearColor];
    self.nameLbl.text = @"当前网络不可用,请检查你的网络设置";
    [self.backView addSubview:self.nameLbl];
    self.backView.hidden = YES;
    
    
    
}

- (LSearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[LSearchBar alloc] initWithFrame:CGRectMake(13, 5, SCREEN_WIDTH-26, 30)];
          //UITextField *searchTextField = [_searchBar valueForKey:@"_searchField"];
        
    }
    return _searchBar;
}

-(void)layoutViews{
    @weakify(self)
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.backView.mas_left).offset(10);
        make.centerY.equalTo(self.backView.mas_centerY);
        make.width.height.equalTo(@(20));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.logoImgView.mas_right).offset(10);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
}
-(void)laytoutByStatus:(int)status{
    if (status == 0) {
        self.backView.hidden = NO;
        self.frame = CGRectMake(0, 0,  SCREEN_WIDTH, 108);
    }else{
        self.backView.hidden = YES;
        self.frame = CGRectMake(0, 0,  SCREEN_WIDTH, 54);
    }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}
@end
