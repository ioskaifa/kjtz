//
//  MXSwipeTableViewCell.m
//  MoPal_Developer
//
//  Created by aken on 16/1/13.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "MXSwipeTableViewCell.h"

#define kMXSwipeTableViewOpenNotifition @"kMXSwipeTableViewOpenNotifition"

static NSInteger const kMXSwipeTableViewMoreItemTag = 100;

@interface MXSwipeTableViewCell()<UIGestureRecognizerDelegate>

@property (nonatomic, weak) UITableView               *superTableView;
@property (nonatomic, strong) UIView                  *cellContentView;
@property (nonatomic, strong) UIPanGestureRecognizer  *panGesture;
@property (nonatomic, strong) UITapGestureRecognizer  *tapGesture;
@property (nonatomic, assign) CGFloat                 judgeWidth;
@property (nonatomic, assign) CGFloat                 rightfinalWidth;
@property (nonatomic, assign) CGFloat                 cellHeight;
@property (nonatomic, assign, readwrite) BOOL         isRightBtnShow;
@property (nonatomic, assign) BOOL                    otherCellIsOpen;
@property (nonatomic, assign) CGFloat                 currentMarginRight;

@end

@implementation MXSwipeTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];

}

- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier
          tableView:(UITableView *)tableView marginRight:(CGFloat)marginRight height:(NSInteger)height{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.currentMarginRight = marginRight;
        self.superTableView = tableView;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.cellHeight = height;
        [self setSwipeTopView];
//        [self addObserverEvent];
        [self addNotification];
    }
    return self;
}

- (void)dealloc {
    
//    [self.superTableView removeObserver:self forKeyPath:@"contentOffset"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMXSwipeTableViewOpenNotifition object:nil];
}

- (void)setRightButtons:(NSArray *)rightButtons {
    
     _rightButtons = [NSArray arrayWithArray:rightButtons];
    [self setupMoreItemButton];
}


#pragma mark prepareForReuser

- (void)prepareForReuse {
    
    [self hideButton];
    [super prepareForReuse];
}

- (void)setupMoreItemButton {
    
    if (_rightButtons.count>0) {
        
        [self removeAllMoreItemFromItemView];
        
        CGFloat lastWidth = 0;
        int i = 0;
        
        for (UIButton *itemButton in _rightButtons) {
            
            itemButton.tag = i + kMXSwipeTableViewMoreItemTag;
            CGRect temRect = itemButton.frame;
            temRect.origin.x = SCREEN_WIDTH - temRect.size.width - lastWidth - self.currentMarginRight;
            itemButton.frame = temRect;
            lastWidth = lastWidth + itemButton.frame.size.width;
            
            itemButton.titleLabel.font = [UIFont font14];
            itemButton.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
            itemButton.titleLabel.numberOfLines = 2;
            itemButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            
            if (!_judgeWidth) {
                _judgeWidth = lastWidth;
            }
            
            [itemButton addTarget:self action:@selector(cellBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [_cellContentView addSubview:itemButton];
            
            i++;
            
        }
        _rightfinalWidth = lastWidth;
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
    }
    
}

- (void)removeAllMoreItemFromItemView {
    
    for (UIView *v in _cellContentView.subviews) {
        if (v) {
            [v removeFromSuperview];
        }
    }
}

- (void)setMoreItemBackgroundView {
    
    // 每次进来都要删除
    if (_cellContentView) {
        
        [_cellContentView removeFromSuperview];
        _cellContentView = nil;
    }
    
    // 用来添加滑动过后要显示菜单项的背景view
    _cellContentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, _cellHeight)];
    [self.contentView addSubview:_cellContentView];
}

- (void)setSwipeTopView {

    [self setMoreItemBackgroundView];
    _customContentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, _cellHeight)];
    _customContentView.backgroundColor = [UIColor brownColor];
    [self.contentView addSubview:_customContentView];
    
    _panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handleGesture:)];
    _panGesture.delegate = self;
     _panGesture.cancelsTouchesInView = NO;
    [_customContentView addGestureRecognizer:_panGesture];
    
    _tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cellTaped:)];
    _tapGesture.delegate = self;
    [_customContentView addGestureRecognizer:_tapGesture];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self hideButton];
}

#pragma mark  - kvo 通知

- (void)addNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNotification:)
                                                 name:kMXSwipeTableViewOpenNotifition
                                               object:nil];
}

- (void)handleNotification:(NSNotification *)notify {
    
    if ([[notify.userInfo objectForKey:@"action"] isEqualToString:@"closeCell"]) {
        [self hideButton];
        
        _otherCellIsOpen = NO;
    }
    else if ([[notify.userInfo objectForKey:@"action"] isEqualToString:@"otherCellIsOpen"]){
        _otherCellIsOpen = YES;
    }
    else if ([[notify.userInfo objectForKey:@"action"] isEqualToString:@"otherCellIsClose"])
    {
        _otherCellIsOpen = NO;
    }
}

- (void)addObserverEvent {
    
    [self.superTableView addObserver:self
                      forKeyPath:@"contentOffset"
                         options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew
                         context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"contentOffset"]) {
        CGPoint oldpoint = [[change objectForKey:@"old"] CGPointValue];
        CGPoint newpoint = [[change objectForKey:@"new"] CGPointValue];
        
        if (oldpoint.y!=newpoint.y) {

            if ((_customContentView.frame.origin.x == -_judgeWidth)) {
                [self hideButton];
            }
        }
    }
}
#pragma mark - 手势

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
        shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return NO;
}

- (void)cellTaped:(UITapGestureRecognizer *)recognizer {
    
    if (_otherCellIsOpen) {
        [[NSNotificationCenter defaultCenter]postNotificationName:kMXSwipeTableViewOpenNotifition object:nil userInfo:@{@"action":@"closeCell"}];
    }
    else{
        NSIndexPath *indexPath = [self.superTableView indexPathForCell:self];
        [self.superTableView.delegate tableView:self.superTableView didSelectRowAtIndexPath:indexPath];
    }
}

- (void)handleGesture:(UIPanGestureRecognizer *)recognizer {
    
    if (self.rightButtons.count == 0) {
        return;
    }
    
    CGPoint translation = [_panGesture translationInView:self];
//    CGPoint location = [_panGesture locationInView:self];

    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            break;
        case UIGestureRecognizerStateChanged:
            if (fabs(translation.x)<fabs(translation.y)) {
                self.superTableView.scrollEnabled = YES;
                return;
            }else{
                self.superTableView.scrollEnabled = NO;
            }
            if (_otherCellIsOpen) {
                return;
            }
            //contentoffse changed
            if (translation.x<0) {
                if (self.customContentView.frame.origin.x==_rightfinalWidth) {
                    translation.x = self.customContentView.frame.origin.x;
                }
                [self movecustomContentView:translation.x];
            }
            else if (translation.x>0){
                //customContentView is moving towards right
                [self hideButton];
            }
            break;
            
        case UIGestureRecognizerStateEnded:
            self.superTableView.scrollEnabled = YES;
            if (_otherCellIsOpen&&!(_customContentView.frame.origin.x == -_judgeWidth)) {
                [[NSNotificationCenter defaultCenter]postNotificationName:kMXSwipeTableViewOpenNotifition object:nil userInfo:@{@"action":@"closeCell"}];
                return;
            }
            //end pan
            [self customContentViewStop];
            break;
            
        case UIGestureRecognizerStateCancelled:
            self.superTableView.scrollEnabled = YES;
            //cancell
            [self customContentViewStop];
            break;
            
        default:
            break;
    }
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self];
}
#pragma mark * UIPanGestureRecognizer delegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] ) {
        //当打开的情况下 全部收回去
        if (_otherCellIsOpen) {
            [[NSNotificationCenter defaultCenter]postNotificationName:kMXSwipeTableViewOpenNotifition object:nil userInfo:@{@"action":@"closeCell"}];
            return NO;
        }
        CGPoint translation = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:self];
        return fabs(translation.x) > fabs(translation.y);
//        if (!_otherCellIsOpen) {
//            CGPoint translation = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:self];
//            return fabs(translation.x) > fabs(translation.y);
//        }else{
//            return NO;
//        }
    }
    return YES;
}
- (void)movecustomContentView:(CGFloat)offset {
    
    CGRect temRect = _customContentView.frame;
    temRect.origin.x = (temRect.origin.x + offset);
    if (temRect.origin.x+(SCREEN_WIDTH)/2.0<0) {
        temRect.origin.x = -SCREEN_WIDTH/2.0;
    }
    if (temRect.origin.x>SCREEN_WIDTH/2.0) {
        temRect.origin.x = SCREEN_WIDTH/2.0;
    }
    _customContentView.frame = temRect;
}

- (void)customContentViewStop {
    
    if ((_customContentView.frame.origin.x == -_judgeWidth)) {
        
 
        if (_customContentView.frame.origin.x + _judgeWidth<0) {
            [self showBtton];
        }
        else {
            [self hideButton];
        }
        
    }else {
        if (_customContentView.frame.origin.x+_judgeWidth>0) {
            
            [self hideButton];
        }
        else{
            [self showBtton];
        }
    }
}

#pragma mark 显示和隐藏

- (void)showBtton {
    
    if (!(_customContentView.frame.origin.x == -_judgeWidth)) {
        
            [self cellWillShow];
    }
    self.superTableView.scrollEnabled = NO;
    
    @weakify(self)
    [UIView animateWithDuration:0.2 animations:^{
        
        CGRect temRect = _customContentView.frame;
        temRect.origin.x = -_rightfinalWidth;
        _customContentView.frame = temRect;
        
    } completion:^(BOOL finished) {
        @strongify(self)
        if (!_isRightBtnShow) {
            [self cellDidShow];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kMXSwipeTableViewOpenNotifition object:nil userInfo:@{@"action":@"otherCellIsOpen"}];
        self.superTableView.scrollEnabled = YES;
    }];
}

- (void)hideButton {    
    if ((_customContentView.frame.origin.x == -_judgeWidth)) {
            [self cellWillHide];
    }
    
    self.superTableView.userInteractionEnabled = NO;
    
    @weakify(self)
    [UIView animateWithDuration:0.3 animations:^{
        CGRect temRect = _customContentView.frame;
        temRect.origin.x = 0;
        _customContentView.frame = temRect;
        
    } completion:^(BOOL finished) {
        
        @strongify(self)
        if (_isRightBtnShow) {
            [self cellDidHide];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kMXSwipeTableViewOpenNotifition object:nil userInfo:@{@"action":@"otherCellIsClose"}];
        self.superTableView.userInteractionEnabled = YES;
    }];
}

#pragma mark - 代理处理

- (void)cellWillHide{
    
    if ([_delegate respondsToSelector:@selector(mxSwipeCellOptionBtnWillHide)]) {
        [_delegate mxSwipeCellOptionBtnWillHide];
    }
}

- (void)cellWillShow{
    
    if ([_delegate respondsToSelector:@selector(mxSwipeCellOptionBtnWillShow)]) {
        [_delegate mxSwipeCellOptionBtnWillShow];
    }
}

- (void)cellDidShow {
    
    if ([_delegate respondsToSelector:@selector(mxSwipeCellOptionBtnDidShow)]) {
        [_delegate mxSwipeCellOptionBtnDidShow];
    }
}

- (void)cellDidHide {
    
    if ([_delegate respondsToSelector:@selector(mxSwipeCellOptionBtnDidHide)]) {
        
        [_delegate mxSwipeCellOptionBtnDidHide];
    }
}

- (void)cellBtnClicked:(UIButton *)sender{
    
    NSIndexPath *indexPath = [self.superTableView indexPathForCell:self];
    
    if ([_delegate respondsToSelector:@selector(mxSwipeTableViewCelldidSelectBtnWithTag:andIndexPath:)]) {
        
        [_delegate mxSwipeTableViewCelldidSelectBtnWithTag:sender.tag - kMXSwipeTableViewMoreItemTag andIndexPath:indexPath];
    }
    [self hideButton];
}


@end

