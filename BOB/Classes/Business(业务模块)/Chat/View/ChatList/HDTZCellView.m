//
//  FriendListView.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "HDTZCellView.h"
#import "MXSeparatorLine.h"
#import <YYKit/YYKit.h>
#import "NSMutableAttributedString+Attributes.h"
#import "HDTZModel.h"
#import "NSDate+Category.h"
#import "QNManager.h"
#import "UIButton+WebCache.h"

static int avatarWidthHeight = 50;
static int padding = 10;
@interface HDTZCellView ()

@property (nonatomic, strong) UIButton *logoImg;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *nameRightLbl;

@property (nonatomic, strong) UILabel *contentLbl;

@property (nonatomic, strong) UILabel *timeLbl;

@property (nonatomic, strong) UIImageView *rightImg;

@property (nonatomic, strong) UIImageView *rightCenterImg;

@property (nonatomic, strong) UILabel *rightTitle;

@property (nonatomic, strong) MXSeparatorLine* line;

@property (nonatomic, weak) HDTZModel *curModel;

@end

@implementation HDTZCellView


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

- (UIButton *)logoImg
{
    if (!_logoImg) {
        _logoImg = [[UIButton alloc] initWithFrame:CGRectZero];
        _logoImg.layer.masksToBounds = YES;
        _logoImg.layer.cornerRadius = avatarWidthHeight/2;
        _logoImg.backgroundColor = [UIColor colorWithHexString:@"#f0f0f0"];
        [_logoImg addTarget:self action:@selector(logoImgClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _logoImg;
}

- (UIImageView *)rightImg
{
    if (!_rightImg) {
        _rightImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        _rightImg.backgroundColor = [UIColor whiteColor];
        _rightImg.layer.masksToBounds = YES;
        _rightImg.layer.cornerRadius = 5;
        _rightImg.contentMode = UIViewContentModeScaleAspectFill;
    }
    
    return _rightImg;
}

- (UIImageView *)rightCenterImg
{
    if (!_rightCenterImg) {
        _rightCenterImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        _rightCenterImg.backgroundColor = [UIColor whiteColor];
        _rightCenterImg.layer.masksToBounds = YES;
        _rightCenterImg.layer.cornerRadius = 20;
        _rightCenterImg.contentMode = UIViewContentModeScaleAspectFit;
        _rightCenterImg.image = [UIImage imageNamed:@"hdtz_playVideo"];
    }
    
    return _rightCenterImg;
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLbl.font = [UIFont font14];
        _nameLbl.textColor =  [UIColor moBlueColor];
        _nameLbl.text = @"";
    }
    
    return _nameLbl;
}
- (UILabel *)nameRightLbl
{
    if (!_nameRightLbl) {
        _nameRightLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameRightLbl.font = [UIFont font14];
        _nameRightLbl.textColor = [UIColor grayColor];
        _nameRightLbl.text = @"";
        _nameRightLbl.textAlignment = NSTextAlignmentLeft;
    }
    
    return _nameRightLbl;
}
- (UILabel *)timeLbl
{
    if (!_timeLbl) {
        _timeLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _timeLbl.font = [UIFont font14];
        _timeLbl.textColor = [UIColor grayColor];
        _timeLbl.text = @"";
    }
    
    return _timeLbl;
}

- (UILabel *)rightTitle
{
    if (!_rightTitle) {
        _rightTitle = [[UILabel alloc] initWithFrame:CGRectZero];
        _rightTitle.font = [UIFont font14];
        _rightTitle.text = @"";
        _rightTitle.textAlignment = NSTextAlignmentCenter;
        _rightTitle.numberOfLines = 0;
    }
    
    return _rightTitle;
}

- (UILabel *)contentLbl
{
    if (!_contentLbl) {
        _contentLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _contentLbl.font = [UIFont font14];
    }
    
    return _contentLbl;
}

- (void)logoImgClick:(id)sender {
    [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":self.curModel.op_user_id ?: @""}];
}

#pragma mark - UI
- (void)initUI
{
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.logoImg];
    [self addSubview:self.nameLbl];
    [self addSubview:self.nameRightLbl];
    [self addSubview:self.contentLbl];
    [self addSubview:self.timeLbl];
    [self addSubview:self.rightImg];
    [self.rightImg addSubview:self.rightCenterImg];
    [self addSubview:self.rightTitle];
    _line =[MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self addSubview:_line];
    [self layoutView];
}


- (void)layoutView
{
    @weakify(self)
    [self.logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(padding);
        make.centerY.equalTo(self.mas_centerY).offset(-10);
        make.width.height.equalTo(@(avatarWidthHeight));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.logoImg.mas_right).offset(5);
        make.top.equalTo(self.logoImg.mas_top).offset(2);
        //make.width.lessThanOrEqualTo(@(80));
        make.right.equalTo(self.nameRightLbl.mas_left).offset(-5);
    }];
    [self.nameRightLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.nameLbl.mas_right).offset(5);
        make.right.equalTo(self.rightImg.mas_left).offset(-10);
        make.centerY.equalTo(self.nameLbl.mas_centerY);
        //make.top.equalTo(self.nameLbl.mas_top);
    }];
    [self.contentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(5);
        make.right.equalTo(self.rightImg.mas_left).offset(-10);;
    }];
    [self.timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.nameLbl.mas_left);
        make.top.equalTo(self.contentLbl.mas_bottom).offset(5);
    }];
    [self.rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.mas_right).offset(-padding);
        make.centerY.equalTo(self.mas_centerY);
        make.width.height.equalTo(@(70));
    }];
    [self.rightCenterImg mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.rightImg.mas_centerX);
        make.centerY.equalTo(self.rightImg.mas_centerY);
        make.width.height.equalTo(@(40));
    }];
    [self.rightTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(@(5));
        make.right.equalTo(self.mas_right).offset(-padding);
        //make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@(80));
        make.height.equalTo(@(80));
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.logoImg.mas_left);
        make.right.equalTo(self.rightImg.mas_right);
        make.height.equalTo(@(0.5));
        make.bottom.equalTo(self.mas_bottom);
    }];
    
    [self.nameLbl setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    [self.nameRightLbl setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    
    [self.nameLbl setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    [self.nameRightLbl setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
}

-(void)reloadData:(HDTZModel *)model{
    self.curModel = model;
    
    [self.logoImg sd_setImageWithURL:[NSURL URLWithString:model.op_user_head_photo] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    //[self.logoImg sd_setImageWithURL:[NSURL URLWithString:model.op_user_head_photo] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    if ([model.notice_type isEqualToString:@"01"]) {
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:@""];
        CGRect imageBound = CGRectMake(0, -4, 17, 17);
        [attr addImageInHead:@"timeline_icon_like" bound:imageBound];
        _contentLbl.attributedText = attr;
        self.nameLbl.text = model.op_user_name;
        self.nameRightLbl.text = @"点赞了你的朋友圈";
//        if ([model.circle_send_type isEqualToString:@"00"]) {
//           self.nameRightLbl.text = @"点赞了你的朋友圈";
//        }else{
//            self.nameRightLbl.text = [NSString stringWithFormat:@"点赞了%@的朋友圈",model.relation_user_name];
//        }
    }else{
       
        if ([model.notice_type isEqualToString:@"04"]) {
            self.nameLbl.text = model.op_user_name;
            
            self.nameRightLbl.text = @"评论了你的朋友圈";
//            if ([model.circle_send_type isEqualToString:@"00"]) {
//                self.nameRightLbl.text = @"评论了你的朋友圈";
//            }else{
//                self.nameRightLbl.text = [NSString stringWithFormat:@"评论了%@的朋友圈",model.relation_user_name];
//            }
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:model.notice_content];
            _contentLbl.attributedText = attr;
        }else if([model.notice_type isEqualToString:@"02"]){
            self.nameLbl.text = model.op_user_name;
            
            self.nameRightLbl.text = @"评论了你的朋友圈";
//            if ([model.circle_send_type isEqualToString:@"00"]) {
//                 self.nameRightLbl.text = @"评论了你的朋友圈";
//            }else{
//                self.nameRightLbl.text = [NSString stringWithFormat:@"评论了%@的朋友圈",model.relation_user_name];
//            }
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:model.notice_content];
            _contentLbl.attributedText = attr;
        }else if([model.notice_type isEqualToString:@"03"]){
            self.nameLbl.text = model.op_user_name;
            self.nameRightLbl.text = @"打赏了你的朋友圈";
            
//            if ([model.circle_send_type isEqualToString:@"00"]) {
//                self.nameRightLbl.text = @"打赏了你的朋友圈";
//            }else{
//                self.nameRightLbl.text = [NSString stringWithFormat:@"打赏了%@的朋友圈",model.relation_user_name];
//            }
            NSString*  tip = [NSString stringWithFormat:@"%@美元",model.notice_content];
            NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:tip];
            _contentLbl.attributedText = attr;
        }
    }
    self.rightCenterImg.hidden = YES;
    if ([StringUtil isEmpty:model.circle_image_type]) {
        //文字
        self.rightImg.hidden = YES;
        self.rightTitle.hidden = NO;
        self.rightTitle.text = model.circle_content;
    }else if([model.circle_image_type hasPrefix:@"2"]){
        self.rightCenterImg.hidden = NO;
        //视频
        NSString* firstPath = [model.circle_link componentsSeparatedByString:@","][0];
        NSString *videoScreenshot = VideoScreenshot(firstPath, 80, 80);
        
        NSString *fixUrl = [self.class addHostForVideoUrl:videoScreenshot];
        [self.rightImg sd_setImageWithURL:[NSURL URLWithString:fixUrl] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
        self.rightImg.hidden = NO;
        self.rightTitle.hidden = YES;
//          [_playBtn setBackgroundImage:GetImageWithName(@"zl_playVideo") forState:UIControlStateNormal];
    }else{
        //图片
        NSString* firstPath = [model.circle_link componentsSeparatedByString:@","][0];
        [self.rightImg sd_setImageWithURL:[NSURL URLWithString:firstPath] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
        self.rightImg.hidden = NO;
        self.rightTitle.hidden = YES;
    }
    NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[model.send_time doubleValue]];
    self.timeLbl.text = [msgDate formattedTime];
}
+ (NSString *)addHostForVideoUrl:(NSString *)url {
    NSURL *fixUrl = [NSURL URLWithString:url];
    if(fixUrl.host.length == 0) {
        return [NSString stringWithFormat:@"%@/%@",[QNManager shared].qnHost,url];
    } else {
        return url;
    }
}
@end
