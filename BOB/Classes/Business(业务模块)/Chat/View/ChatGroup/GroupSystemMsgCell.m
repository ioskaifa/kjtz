//
//  GroupSystemMsgCell.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/8/27.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "GroupSystemMsgCell.h"
#import "FriendModel.h"
#import "FriendsManager.h"
#import "ChatSendHelper.h"
#import "TalkManager.h"
#import "LcwlChat.h"

@interface GroupSystemMsgCell()

@property (nonatomic, retain) YYLabel* tipLabel;

@end
@implementation GroupSystemMsgCell
- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.tipLabel =  [YYLabel new];
        self.tipLabel.font = [UIFont font12];
        self.tipLabel.numberOfLines = 0;
        self.tipLabel.userInteractionEnabled = YES;
        self.tipLabel.preferredMaxLayoutWidth = SCREEN_WIDTH-80;
        YYTextSimpleEmoticonParser *parser = [YYTextSimpleEmoticonParser new];
        parser.emoticonMapper = [[TalkManager manager] emotionMapper];
        self.tipLabel.textParser = parser;
        [self addSubview:self.tipLabel];
    }
    return self;
}
- (void)bubbleViewPressed:(id)sender
{
    NSLog(@"1234");
}
-(void)reloadData:(MessageModel*)msg{
    if (msg.subtype == kMxmessageTypeCustom) {
        NSString* content = msg.content;
        NSMutableAttributedString *detText = [[NSMutableAttributedString alloc] initWithString:content];
        NSRange range1 = [detText.string rangeOfString:@"\n添加好友"];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.alignment = NSTextAlignmentCenter;//（两端对齐的）文本对齐方式：（左，中，右，两端对齐，自然）
        paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
        [detText setParagraphStyle:paragraphStyle];
        
        if (range1.location != NSNotFound) {
            [detText setColor:[UIColor moBlueColor] range:range1];
            [detText setColor:[UIColor lightGrayColor] range:NSMakeRange(0, range1.location)];
            NSTextAttachment *attach = [[NSTextAttachment alloc] init];
            [detText addAttribute:@"YYTextAttachmentAttributeName"value:attach range:NSMakeRange(0, content.length)];
        }else{
            [detText setColor:[UIColor lightGrayColor] range:NSMakeRange(0, content.length)];
        }
        
        self.tipLabel.textTapAction = ^(UIView *containerView, NSAttributedString *text, NSRange range, CGRect rect) {
            //@strongify(self)
            if(NSLocationInRange(range.location, range1)) {
                FriendModel *friend = [[FriendModel alloc] init];
                friend.userid = msg.fromID;
                [MXRouter openURL:@"lcwl://VerifyFriendVC" parameters:@{@"model":friend}];
//                [FriendsManager addFriend:@{@"add_acce_id":msg.chat_with,@"add_type":@"1"} completion:^(id object, NSString *error) {
//                    if (object) {
//                        NSDictionary* dict = (NSDictionary*)object;
//                        NSInteger status = [dict[@"status"]integerValue];
//                        FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:msg.chat_with];
//                        friend.followState = status;
//                        if (status == MoRelationshipTypeStranger) {
//                            NSInteger isverify = [dict[@"isverify"]integerValue];
//                            if (isverify == 1) {
//                                [MXRouter openURL:@"lcwl://VerifyFriendVC" parameters:@{@"model":friend}];
//                            }
//                        }else if(status == MoRelationshipTypeMyFriend){
//                            [[LcwlChat shareInstance].chatManager insertFriends:@[friend]];
//                            //已经是好友了
//                            NSString* tip = [NSString stringWithFormat:@"您已添加了%@，可以开始聊天了",friend.name];
//                            [ChatSendHelper sendChat:tip from:friend.userid messageType:kMxmessageTypeCustom type:eConversationTypeChat];
//
//                        }
//                    }
//                }];
            }
        };
        self.tipLabel.attributedText = detText;
        [self.tipLabel sizeToFit];
        
    }else if (msg.subtype == kMxmessageTypeReceiveRedpacket) {
        NSMutableAttributedString *detText = [[NSMutableAttributedString alloc] initWithString:msg.content];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.alignment = NSTextAlignmentCenter;//（两端对齐的）文本对齐方式：（左，中，右，两端对齐，自然）
        paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
        [detText setParagraphStyle:paragraphStyle];
        detText.font = [UIFont systemFontOfSize:12];
        NSRange range1 = NSMakeRange(msg.content.length-2, 2);
        [detText setColor:[UIColor orangeColor] range:range1];
        [detText setColor:[UIColor lightGrayColor] range:NSMakeRange(0, range1.location)];
        NSTextAttachment *attach = [[NSTextAttachment alloc] init];
        [detText addAttribute:@"YYTextAttachmentAttributeName"value:attach range:NSMakeRange(0, msg.content.length)];
        
        self.tipLabel.attributedText = detText;
    }
    else{
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:msg.content];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.alignment = NSTextAlignmentCenter;//（两端对齐的）文本对齐方式：（左，中，右，两端对齐，自然）
        paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
        [text setParagraphStyle:paragraphStyle];
        
        text.font = [UIFont systemFontOfSize:12];
        text.color = [UIColor lightGrayColor];
        self.tipLabel.attributedText = text;
        
    }
    
    @weakify(self)
    [self.tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@(SCREEN_WIDTH-80));
    }];
    
    self.tipLabel.frame = CGRectMake(40, 5, SCREEN_WIDTH-80, [self.class getHeight:msg]);
    
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, [self.class getHeight:msg]);
}

+(NSInteger)getHeight:(MessageModel*)msg {
    CGFloat height = [msg.content heightForFont:[UIFont font12] width:SCREEN_WIDTH-80];
    return ceil(height);
}

@end
