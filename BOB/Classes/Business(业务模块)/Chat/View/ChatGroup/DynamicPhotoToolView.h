//
//  DynamicPhotoToolView.h
//  MoPromo_Develop
//
//  Created by 王 刚 on 15/5/30.
//  Copyright (c) 2015年 MoPromo. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "GroupFriendModel.h"
#import "FriendModel.h"
#import "YYLabel.h"
#define DynamicPhotoMaxNum 6
typedef enum {
    //以下是枚举成员 TestA = 0,
    ADD_FRIEND,
    DELETE_FRIEND
}OPERTATE_TYPE;//枚举名称
@interface DynamicPhotoToolView : UIView
@property (weak, nonatomic) IBOutlet UIScrollView *imageScroll;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (nonatomic) NSMutableArray* imageArray;
@property (nonatomic) NSInteger type;
-(void)reloadData:(NSMutableArray* )array;
-(void)insertModel:(id)model;
-(void)deleteModel:(id)model;
+(DynamicPhotoToolView* )getInstance;
+(int)getHeight;
@end
