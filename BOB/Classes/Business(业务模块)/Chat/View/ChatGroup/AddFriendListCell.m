//
//  AddFriendListCell.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/13.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "AddFriendListCell.h"
#import "PlaceHolderImageManager.h"
@implementation AddFriendListCell

-(void)awakeFromNib{
    [super awakeFromNib];
    [self addBottomSingleLine:[UIColor moBackground]];
    [self.headImg setViewCornerRadius:20];
    self.lastLine.hidden = YES;
    
    UIView *superView = self.contentView;
    [self.headImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.centerY.mas_equalTo(superView);
        make.left.mas_equalTo(superView.mas_left).offset(24);
    }];
    
    [self.isSelectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.centerY.mas_equalTo(superView);
        make.right.mas_equalTo(superView.mas_right).offset(-15);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.headImg.mas_right).offset(10);
        make.right.mas_equalTo(self.isSelectBtn.mas_left).offset(-10);
        make.centerY.mas_equalTo(self.headImg);
        make.height.mas_equalTo(20);
    }];
    
}
-(void)reloadData:(FriendModel*) model status:(SelectStatus)status{
    NSString* logo = model.avatar;
    if (![StringUtil isEmpty:logo]) {
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/120/h/120",logo];
         [self.headImg sd_setImageWithURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
    }
    self.nameLabel.text = model.name;
    if (status == SelectStatusUnEnable) {
        self.isSelectBtn.enabled = NO;
    }else if(status == SelectStatusUnSelect){
        self.isSelectBtn.selected = NO;
    }else if(status == SelectStatusSelect){
        self.isSelectBtn.selected = YES;
    }
}

@end
