//
//  AddFriendListCell.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/13.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendModel.h"

typedef NS_ENUM(NSInteger, SelectStatus) {
    SelectStatusUnEnable = 0,
    SelectStatusUnSelect = 1,
    SelectStatusSelect   = 2
};


@interface AddFriendListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *isSelectBtn;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) MXSeparatorLine* line;
@property (strong, nonatomic) MXSeparatorLine* lastLine;

-(void)reloadData:(FriendModel*) model status:(SelectStatus)status;
@end
