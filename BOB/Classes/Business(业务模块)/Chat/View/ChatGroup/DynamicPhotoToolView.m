//
//  DynamicPhotoToolView.m
//  MoPromo_Develop
//
//  Created by 王 刚 on 15/5/30.
//  Copyright (c) 2015年 MoPromo. All rights reserved.
//

#import "DynamicPhotoToolView.h"
#import "UIImageView+WebCache.h"
#import <YYKit/YYKit.h>
#define ImageHeight 40
@implementation DynamicPhotoToolView

+(DynamicPhotoToolView* )getInstance{
    return [[[NSBundle mainBundle] loadNibNamed:@"DynamicPhotoToolView" owner:self options:nil] safeObjectAtIndex:0];
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.sureBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.sureBtn.layer.masksToBounds = YES;
    self.sureBtn.layer.cornerRadius = 5;
    MXSeparatorLine* line = [MXSeparatorLine initHorizontalLineWidth:SCREEN_WIDTH orginX:0 orginY:0];
    [self addSubview:line];
    [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(60));
        make.height.equalTo(@(30));
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
    [self.imageScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left);
        make.height.equalTo(@([self.class getHeight]));
        make.right.equalTo(self.sureBtn.mas_left).offset(-10);
    }];
    [self reloadSureBtn:0];
}


-(void)reloadSureBtn:(NSInteger)nums{
    if (nums == 0) {
        NSString* btnTitle  = MXLang(@"Public_NotifyOkButton", @"确定");
        [self.sureBtn setTitle:btnTitle forState:UIControlStateNormal];
        self.sureBtn.enabled = NO;
        self.sureBtn.backgroundColor = [UIColor separatorLine];
    }else{
        NSString* btnTitle = [NSString stringWithFormat:@"确定(%d)",nums];
         [self.sureBtn setTitle:btnTitle forState:UIControlStateNormal];
        self.sureBtn.enabled = YES;
        self.sureBtn.backgroundColor = [UIColor moBlueColor];
    }
}

+(int)getHeight{
    return 80;
}

-(void)reloadData:(NSMutableArray* )array{
    for (UIView* subView in self.imageScroll.subviews) {
        [subView removeFromSuperview];
    }
    int margin = 10;
    for (int i=0; i<array.count; i++) {
        id model = array[i];
        NSString* photo=nil;
        if ([model isKindOfClass:[FriendModel class]]) {
            FriendModel* friend = model;
            if (![StringUtil isEmpty:friend.avatar]) {
                photo = friend.avatar;
            }
        }
        UIImageView* imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ImageHeight, ImageHeight)];
        [imageView sd_setImageWithURL:[NSURL URLWithString:photo] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
        imageView.frame = CGRectMake(i*ImageHeight+margin*(i+1), ([self.class getHeight]-ImageHeight)/2, ImageHeight, ImageHeight);
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = 4;
        [self.imageScroll addSubview:imageView];
    }

    NSInteger maxX = array.count*ImageHeight+margin*(array.count+1);
    self.imageScroll.contentSize = CGSizeMake(maxX,46);
    [self reloadSureBtn:array.count];
    
}

-(void)insertModel:(id)model{
    if (!self.imageArray) {
        self.imageArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    [self.imageArray safeAddObj:model];
    [self reloadData:self.imageArray];
}

-(void)deleteModel:(id)model{
    if (!self.imageArray) {
        self.imageArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    [self.imageArray removeObject:model];
    [self reloadData:self.imageArray];
}
@end
