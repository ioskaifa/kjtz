//
//  GroupSystemMsgCell.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/8/27.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"
#import "YYTextHelper.h"
@interface GroupSystemMsgCell : UIView

-(void)reloadData:(MessageModel*)msg;

+(NSInteger)getHeight:(MessageModel*)msg;

@end
