//
//  ChatListTableCell.m
//  MoPal_Developer
//
//  Created by 王 刚 on 16/2/16.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "ChatListTableCell.h"
#import "LcwlChat.h"
#import "FriendModel.h"
#import "MXChatDBUtil.h"
#import "TalkManager.h"
#import "NSDate+Category.h"
#import "YYTextHelper.h"
#import "LcwlChatHelper.h"
static NSInteger avatarSize = 50;
static NSInteger padding = 10.0f;
static NSInteger stateWidth = 15;
static NSInteger stateHeight = 11;
static NSInteger cellHeight = 80;
@interface ChatListTableCell()
@property (nonatomic, strong) MXSeparatorLine* downLine;
@end
@implementation ChatListTableCell


- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier
          tableView:(UITableView *)tableView
       conversation:(MXConversation *)conver
        marginRight:(CGFloat)marginRight
{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier tableView:tableView marginRight:marginRight height:cellHeight];
    if (self) {
        _conversation = conver;
        [self initUI];
        [self addConstraints];
    }
    return self;
}

-(void)initUI{
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.customContentView addSubview:self.imageviewAvatarBG];
    [self.imageviewAvatarBG addSubview:self.imageviewAvatar];
    [self.customContentView addSubview:self.labelName];
    [self.customContentView addSubview:self.topLabel];
    [self.customContentView addSubview:self.labelTime];
    [self.customContentView addSubview:self.textviewContent];
    [self.imageviewAvatarBG addSubview:self.badgeView];
    [self.imageviewAvatarBG addSubview:self.disturbView];
    [self.customContentView addSubview:self.imageviewDisturb];
    _imageviewState = [[UIImageView alloc]initWithFrame:CGRectZero];
    _imageviewState.backgroundColor = [UIColor clearColor];
    _imageviewState.contentMode = UIViewContentModeScaleAspectFit;
    [self.customContentView addSubview:_imageviewState];
    [self.customContentView addSubview:self.downLine];
    self.customContentView.backgroundColor =[UIColor whiteColor];
}

-(MXSeparatorLine *)downLine{
    if(!_downLine){
        _downLine=[MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    }
    return _downLine;
}

-(void)addConstraints{
    @weakify(self);
    [self.imageviewAvatarBG mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.customContentView.mas_left).offset(padding);
        make.centerY.equalTo(self.customContentView.mas_centerY);
        make.width.equalTo(@(avatarSize));
        make.height.equalTo(@(avatarSize));
    }];
    
    [self.imageviewAvatar mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
//        make.left.equalTo(self.customContentView.mas_left).offset(padding);
//        make.centerY.equalTo(self.customContentView.mas_centerY);
//        make.width.equalTo(@(avatarSize));
//        make.height.equalTo(@(avatarSize));
        make.edges.equalTo(self.imageviewAvatarBG);
    }];
    [self.labelName mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.imageviewAvatar.mas_right).offset(padding);
        make.top.equalTo(self.imageviewAvatar.mas_top);
        make.height.equalTo(@(25));
        make.width.lessThanOrEqualTo(@(SCREEN_WIDTH/2));
    }];
    [self.downLine  mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.labelName.mas_left);
        make.bottom.equalTo(self.mas_bottom).offset(-1);
        make.height.equalTo(@(0.5));
    }];
    [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.labelName.mas_right).offset(5);
        make.centerY.equalTo(self.labelName.mas_centerY);
        make.width.lessThanOrEqualTo(@(40));
        make.width.greaterThanOrEqualTo(@(25));
        make.height.equalTo(@(15));
    }];
    [self.labelTime mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.customContentView.mas_right).offset(-10);
        make.top.equalTo(self.topLabel.mas_top);
        make.width.lessThanOrEqualTo(@(80));
        make.height.equalTo(@(15));
//        make.right.equalTo(self.customContentView.mas_right).offset(-10);
//        make.top.equalTo(self.topLabel.mas_top);
//        make.width.equalTo(@(80));
//        make.height.equalTo(@(15));
    }];
    [self.imageviewState mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.labelName.mas_left);
        make.top.equalTo(self.labelName.mas_bottom).offset(10);
        make.width.equalTo(@(1));
        make.height.equalTo(@(15));
    }];
    [self.textviewContent mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.imageviewState.mas_right).offset(5);
        make.right.equalTo(self.imageviewDisturb.mas_left);
        make.centerY.equalTo(self.imageviewState.mas_centerY);
        make.height.equalTo(@(30));
    }];
//    [self.badgeView mas_makeConstraints:^(MASConstraintMaker *make) {
//        @strongify(self);
//        //make.top.equalTo(self.imageviewAvatar.mas_top);
//        //make.right.equalTo(self.imageviewAvatar.mas_right).offset(-10);
//        //make.width.equalTo(@(10));
//        //make.height.equalTo(@(10));
//        make.centerX.equalTo(self.imageviewAvatar.mas_centerX);
////        make.centerY.equalTo(self.imageviewAvatar.mas_top);
//    }];
    [self.imageviewDisturb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.labelName.mas_bottom).offset(5);
        make.right.equalTo(self.labelTime.mas_right);
        make.width.equalTo(@(29.0/2.0));
        make.height.equalTo(@(32.0/2.0));
    }];
    
    [self.disturbView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.disturbView.size);
        make.left.mas_equalTo(self.imageviewAvatar.mas_right).mas_offset(-(self.disturbView.width/2.0));
        make.bottom.mas_equalTo(self.imageviewAvatar.mas_top).mas_offset(self.disturbView.width/2.0);
    }];
}

-(UIImageView* )imageviewAvatar{
    if (_imageviewAvatar == nil) {
        _imageviewAvatar =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, avatarSize, avatarSize)];
        _imageviewAvatar.layer.masksToBounds = YES;
        _imageviewAvatar.layer.cornerRadius = 4;
        
//        CGSize radii = CGSizeMake(6, 6);
//        // 随意改变4个圆角
//        UIRectCorner corners = UIRectCornerTopLeft | UIRectCornerTopRight;
//        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:_imageviewAvatar.bounds byRoundingCorners:corners cornerRadii:radii];
//        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//        maskLayer.frame = self.bounds;
//        maskLayer.path = path.CGPath;
//        maskLayer.masksToBounds = NO;
//        _imageviewAvatar.layer.mask = maskLayer;
    }
    return _imageviewAvatar;
}

-(UIImageView* )imageviewAvatarBG {
    if (_imageviewAvatarBG == nil) {
        _imageviewAvatarBG = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, avatarSize, avatarSize)];
        _imageviewAvatarBG.backgroundColor = [UIColor whiteColor];
    }
    return _imageviewAvatarBG;
}

-(UILabel* )labelName{
    if (_labelName == nil) {
        _labelName = [[UILabel alloc]initWithFrame:CGRectZero];
        [_labelName setFont:[UIFont font17]];
        [_labelName setBackgroundColor:[UIColor clearColor]];
    }
    return _labelName;
}


-(UILabel* )labelTime{
    if (_labelTime == nil) {
        _labelTime = [[UILabel alloc]initWithFrame:CGRectZero];
        [_labelTime setTextColor:[UIColor lightGrayColor]];
        [_labelTime setFont:[UIFont font12]];
        [_labelTime setBackgroundColor:[UIColor clearColor]];
        [_labelTime setTextAlignment:NSTextAlignmentRight];
        [_labelTime setFrame:CGRectZero];
    }
    return _labelTime;
}
-(UILabel* )topLabel{
    if (_topLabel == nil) {
        _topLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        [_topLabel setTextColor:[UIColor lightGrayColor]];
        [_topLabel setFont:[UIFont font11]];
        _topLabel.text = @"置顶";
        [_topLabel setBackgroundColor:[UIColor whiteColor]];
        [_topLabel setTextAlignment:NSTextAlignmentCenter];
        [_topLabel setFrame:CGRectZero];
        _topLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _topLabel.layer.borderWidth = 0.5;
        _topLabel.layer.masksToBounds = YES;
        _topLabel.layer.cornerRadius = 3;
    }
    return _topLabel;
}

-(YYLabel* )textviewContent{
    if (_textviewContent == nil) {
        _textviewContent = [YYTextHelper yyLabel];
        _textviewContent.font = [UIFont font14];
        _textviewContent.textVerticalAlignment = YYTextVerticalAlignmentTop;
    }
    return _textviewContent;
}
-(JSBadgeView* )badgeView{
    if (_badgeView == nil) {
        _badgeView = [[JSBadgeView alloc] initWithParentView:self.imageviewAvatar alignment:JSBadgeViewAlignmentTopRight frame:CGRectZero];
        //int badgeWidth = [_badgeView getWidth];
        //[_badgeView setFrame:CGRectMake(CGRectGetMaxX(_imageviewAvatar.frame)-badgeWidth+5, 0, 0, 0)];
    }
    return _badgeView;
}
-(UIImageView* )imageviewDisturb{
    if (_imageviewDisturb == nil) {
        _imageviewDisturb = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_imageviewDisturb setImage:[UIImage imageNamed:@"doNotDisturb"]];
        _imageviewDisturb.backgroundColor = [UIColor clearColor];
    }
    return _imageviewDisturb;
}

+ (NSString *)cellIdentifierForMessageModel:(MessageModel *)model
{
    if ([model isKindOfClass:[MessageModel class]]) {
        NSString* sign = @"default";
        if (model.type == MessageTypeNormal) {
            sign = @"normal";
        }else if(model.type == MessageTypeGroupchat){
            sign = @"group";
        }else if(model.type == MessageTypeSgroup){
            sign = @"groupSys";
        }else if(model.type == MessageTypeSs){
            sign = @"normalSys";
        }
        return sign;
    }
    return @"";
}

-(void)setConversation:(MXConversation*)conversation{
    _conversation = conversation;
    if (conversation.conversationType == eConversationTypeChat ) {
        [self setNomalConversation:conversation];
    }else if(conversation.conversationType == eConversationTypeGroupChat){
        [self setGroupConversation:conversation];
    }else if(conversation.conversationType == eConversationTypeHDTZChat ||
             conversation.conversationType == eConversationTypeXDPYChat){
        [self setSSConversation:conversation];
    } else {
        _imageviewAvatar.image = nil;//[UIImage imageNamed:[GroupManager getGroupDefaultPic:2]];
        [_labelName setText:@""];
    }
}
-(void)setSSConversation:(MXConversation*)conversation{
    FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:conversation.chat_id];
    _labelName.text = friend.name;
    _imageviewAvatar.image = [UIImage imageNamed:friend.avatar];
    //设置显示和隐藏的ui
    {
        self.topLabel.hidden = !friend.enable_top;
        _imageviewDisturb.hidden = !friend.enable_disturb;
        
    }
  
    @weakify(self)
    [self.imageviewState mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.labelName.mas_left).offset(-5);
        make.top.equalTo(self.labelName.mas_bottom).offset(10);
        make.width.equalTo(@(1));
        make.height.equalTo(@(15));
    }];
    self.imageviewState.hidden = YES;
    MessageModel* message = conversation.latestMessage;
    if (message) {
        _textviewContent.text = message.content;
        NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[message.sendTime doubleValue]];
        _labelTime.text = [msgDate formattedTime];
    }else{
        _textviewContent.text = @"";
        _labelTime.text = @"";
    }
    
    [self cellBageNumber:conversation.unReadNums enableDisturb:friend.enable_disturb];
}

- (void)cellBageNumber:(NSInteger)num {
    NSString* str = [TalkManager cellBageNumberString:num];
    if(num<=0){
        _badgeView.hidden = YES;
    }else {
        _badgeView.badgeText = str;
        _badgeView.hidden = NO;
    }
}

- (void)cellBageNumber:(NSInteger)num enableDisturb:(BOOL)disturb {
    NSString* str = [TalkManager cellBageNumberString:num];
    if(num<=0){
        _badgeView.hidden = YES;
    }else {
        _badgeView.badgeText = str;
        _badgeView.hidden = NO;
    }
    if (self.badgeView.hidden) {
        self.disturbView.hidden = YES;
        return;
    }
    //开启了免打扰
    if (disturb) {
        self.badgeView.hidden = YES;
        self.disturbView.hidden = NO;
    } else {
        self.badgeView.hidden = NO;
        self.disturbView.hidden = YES;
    }
}

-(NSString*)messageStatusPic:(MessageModel* )message{
    NSString* picName = @"";
    if(message.is_listened == 1){
        picName = @"read";
    }else if(message.is_acked == 1 ){
        picName = @"";
    }else if(message.state >= kMXMessageState_Failure){
        picName = @"sendFail";
    }else if(message.state == kMXMessageStateSending){
        picName = @"sending";
    }
    return picName;
}

-(void)fillAvatarName:(id)model{
    if (model) {
        if ([model isKindOfClass:[FriendModel class]]) {
            FriendModel* friend = (FriendModel*)model;
            _labelName.text = friend.remark?:friend.name;
            NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/120/h/120",friend.avatar];
             [self.imageviewAvatar sd_setImageWithURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
        }
        else if([model isKindOfClass:[ChatGroupModel class]]){
            ChatGroupModel* group = (ChatGroupModel*)model;
            [ChatGroupModel setGroupIconWithURLArray:group.groupAvatar image:_imageviewAvatar];
            
//            if (![StringUtil isEmpty:groupAvatar]) {
//                UIImage* image = [UIImage imageWithContentsOfFile:groupAvatar];
//                if (image) {
//                    [_imageviewAvatar setImage:image];
//                }else{
//                    [_imageviewAvatar setImage:[UIImage imageNamed:groupAvatar]];
//                }
//            }else{
//                 [_imageviewAvatar setImage:[UIImage imageNamed:@"avatar_default"]];
//            }
            if (![StringUtil isEmpty:group.groupName]) {
                [_labelName setText:group.groupName];
            }else{
                [_labelName setText:@""];
            }
        }
    }else{
        [_labelName setText:@""];
        [_imageviewAvatar setImage:[UIImage imageNamed:@"avatar_default"]];
    }
}



-(void)setNomalConversation:(MXConversation*) conversation{
    FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:conversation.chat_id];
    [self fillAvatarName:friend];
    
    //设置显示和隐藏的ui
    {
        self.topLabel.hidden = !friend.enable_top;
        _imageviewDisturb.hidden = !friend.enable_disturb;
       
    }
    MessageModel* message = conversation.latestMessage;
    if (!message) {
        _textviewContent.text = @"";
        if ([StringUtil isEmpty:conversation.lastClearTime]) {
            _labelTime.text = @"";
        }else{
            NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[conversation.lastClearTime doubleValue]];
            _labelTime.text = [msgDate formattedTime];
        }
        @weakify(self)
        [self.imageviewState mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.left.equalTo(self.labelName.mas_left).offset(-5);
            make.top.equalTo(self.labelName.mas_bottom).offset(10);
            make.width.equalTo(@(1));
            make.height.equalTo(@(15));
        }];
        
    }else{
        NSString* pic = [self messageStatusPic:message];
        if (![StringUtil isEmpty:pic]) {
            _imageviewState.image = [UIImage imageNamed:pic];
            @weakify(self)
            [self.imageviewState mas_remakeConstraints:^(MASConstraintMaker *make) {
                @strongify(self);
                make.left.equalTo(self.labelName.mas_left);
                make.top.equalTo(self.labelName.mas_bottom).offset(10);
                make.width.equalTo(@(15));
                make.height.equalTo(@(15));
            }];
        }else{
            @weakify(self)
            [self.imageviewState mas_remakeConstraints:^(MASConstraintMaker *make) {
                @strongify(self);
                make.left.equalTo(self.labelName.mas_left).offset(-5);
                make.top.equalTo(self.labelName.mas_bottom).offset(10);
                make.width.equalTo(@(1));
                make.height.equalTo(@(15));
            }];
        }
        _textviewContent.text = message.content;
        
        NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[message.sendTime doubleValue]];
        _labelTime.text = [msgDate formattedTime];
    }
    [self cellBageNumber:conversation.unReadNums enableDisturb:friend.enable_disturb];
}

-(void)setGroupConversation:(MXConversation*) conversation{
    ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:conversation.chat_id];
    [self fillAvatarName:group];
    //设置显示和隐藏的ui
    {
        self.topLabel.hidden = !group.enable_top;
        _imageviewDisturb.hidden = !group.enable_disturb;
        
    }

    MessageModel* message = conversation.latestMessage;
    if (message == nil) {
        _textviewContent.text = @"";
        if ([StringUtil isEmpty:conversation.lastClearTime]) {
            _labelTime.text = @"";
        }else{
            NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[conversation.lastClearTime doubleValue]];
            _labelTime.text = [msgDate formattedTime];
        }
    }else{
        NSString* content = message.content;//[GroupManager getMSgContent:msg];
        //如果存在@信息
        if (conversation.isAt) {
            NSArray *txtArr = @[@"[有人@我]", content];
            NSArray *colorArr = @[[UIColor redColor], [UIColor grayColor]];
            NSArray *fontArr = @[[UIFont font15], [UIFont font15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
            _textviewContent.attributedText = att;
        } else {
            NSArray *txtArr = @[content];
            NSArray *colorArr = @[[UIColor grayColor]];
            NSArray *fontArr = @[[UIFont font14]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
            _textviewContent.attributedText = att;
        }

        NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[message.sendTime doubleValue]];
        _labelTime.text = [msgDate formattedTime];
    }
    
        @weakify(self)
        [self.imageviewState mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
             make.left.equalTo(self.labelName.mas_left).offset(-5);
            make.top.equalTo(self.labelName.mas_bottom).offset(10);
            make.width.equalTo(@(1));
            make.height.equalTo(@(15));
        }];
    [self cellBageNumber:conversation.unReadNums enableDisturb:group.enable_disturb];
}

-(void)setNoticeConversation:(MXConversation*) conversation{
//    MoYouModel* friend = [[MXChat sharedInstance].chatManager loadFriendByChatId:conversation.chat_id];
//    if (friend != nil) {
//        _imageviewAvatar.image = [UIImage imageNamed:friend.headUrl];
//    }
//    [_labelName setText:friend.name];
//    //设置显示和隐藏的ui
//    {
//        _imageViewTop.hidden = !friend.enable_top;
//        [_imageviewDisturb setHidden:!friend.enable_disturb];
//        _imageviewState.hidden = YES;
//    }
//    MessageModel* msg = conversation.latestMessage;
//    if (msg == nil) {
//        _textviewContent.text = @"";
//        if ([StringUtil isEmpty:conversation.lastClearTime]) {
//            _labelTime.text = @"";
//        }else{
//            NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[conversation.lastClearTime doubleValue]];
//            _labelTime.text = [msgDate formattedTime];
//        }
//    }else{
//        NSString* content = [GroupManager getMSgContent:msg];
//        _textviewContent.text = content;
//        NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[msg.sendTime doubleValue]];
//        _labelTime.text = [msgDate formattedTime];
//    }
    
}

//-(void)layoutSubviews {
//    int badgeWidth = [_badgeView getWidth];
//    [_badgeView setFrame:CGRectMake(CGRectGetMaxX(_imageviewAvatar.frame)-badgeWidth+5, 0, 0, 0)];
//    [super layoutSubviews];
//}

+(float)rowHeight{
    return cellHeight;
}

- (UIView *)disturbView {
    if (!_disturbView) {
        _disturbView = [UIView new];
        _disturbView.backgroundColor = [UIColor redColor];
        _disturbView.size = CGSizeMake(10, 10);
        [_disturbView setViewCornerRadius:_disturbView.height/2.0];
        _disturbView.hidden = YES;
    }
    return _disturbView;
}

@end
