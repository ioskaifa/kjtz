//
//  ChatSettingHeader.m
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "ChatSettingHeader2.h"
#import "ChatSettingItemCell2.h"

static int itemHeight = 80;
@interface  ChatSettingHeader2()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) NSArray* data;

@end
@implementation ChatSettingHeader2

- (instancetype)initWithFrame:(CGRect)frame num:(NSInteger)num{
    self = [super initWithFrame:frame];
    if (self) {
        self.data = @[];
        self.backgroundColor = [UIColor whiteColor];
        self.nums = num;
        [self setupUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame num:(NSInteger)num itemH:(NSInteger)itemH {
    itemHeight = itemH;
    return [self initWithFrame:frame num:num];
}

- (void)setupUI{
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
        CGFloat width = self.width / self.nums;
        [layout setItemSize:CGSizeMake(width, itemHeight)];
        [layout setMinimumInteritemSpacing:0];
        [layout setMinimumLineSpacing:0];
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [_collectionView setScrollEnabled:NO];
        [_collectionView setBackgroundColor:[UIColor clearColor]];
        [_collectionView registerClass:[ChatSettingItemCell2 class] forCellWithReuseIdentifier:@"ChatSettingItemCell2"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
    }
    
    return _collectionView;
}



#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ChatSettingItemCell2 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChatSettingItemCell2" forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor clearColor];
    if(self.lblColor) {
        cell.nameLbl.textColor = self.lblColor;
    }
    id object = self.data[indexPath.row];
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary* obj = (NSDictionary*)object;
        [cell reloadImg:obj[@"image"] name:obj[@"text"]];
    }
    if(self.imgSize > 0) {
        cell.imgSize = self.imgSize;
    }
    return cell;
}

- (void)reloadData:(NSArray*)data{
    self.data = data;
    CGRect rect = self.frame;
    rect.size.height = self.data.count/self.nums*(itemHeight)+(self.data.count%self.nums>0?itemHeight:0);
    self.frame = rect;
    [self.collectionView reloadData];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.block) {
        self.block(@(indexPath.row));
    }
}

@end
