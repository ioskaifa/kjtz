//
//  ChatSettingItemCell.m
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "ChatSettingItemCell2.h"
static int width = 24;
@interface ChatSettingItemCell2 ()

@end

@implementation ChatSettingItemCell2
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

#pragma mark - UI
- (void)initUI
{
    self.backgroundColor = [UIColor clearColor];
 
    [self addSubview:self.logoImgView];
    [self addSubview:self.nameLbl];
    
    [self layoutView];
}

- (void)setImgSize:(CGFloat)imgSize {
    [self.logoImgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(imgSize));
    }];
}

-(void)layoutView{
    @weakify(self)
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerY.equalTo(self.mas_centerY).offset(-width/2);
        make.centerX.equalTo(self.mas_centerX);
        make.width.height.equalTo(@(width));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.logoImgView.mas_bottom).offset(5);
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.height.equalTo(@(20));
        
    }];
    return;
}
- (UIImageView *)logoImgView
{
    if (!_logoImgView) {
        _logoImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
//        _logoImgView.layer.masksToBounds = YES;
//        _logoImgView.layer.cornerRadius =  width/2;
        
    }
    
    return _logoImgView;
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLbl.textAlignment = NSTextAlignmentCenter;
        _nameLbl.font = [UIFont font12];
        _nameLbl.textColor = [UIColor colorWithHexString:@"#222222"];
    }
    
    return _nameLbl;
}

-(void)reloadImg:(NSString* )path name:(NSString* )name{
    self.logoImgView.image = [UIImage imageNamed:path];
    self.nameLbl.text = name;
}
@end
