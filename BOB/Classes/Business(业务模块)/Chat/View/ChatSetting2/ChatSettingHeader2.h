//
//  ChatSettingHeader.h
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface ChatSettingHeader2 : UIView
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic , copy) FinishedBlock  block;
@property (nonatomic) NSInteger  nums;
@property(nonatomic, strong) UIColor *lblColor;
@property(nonatomic, assign) CGFloat imgSize;

- (void)reloadData:(NSArray*)data;

- (instancetype)initWithFrame:(CGRect)frame num:(NSInteger)num;
- (instancetype)initWithFrame:(CGRect)frame num:(NSInteger)num itemH:(NSInteger)itemH;
@end

NS_ASSUME_NONNULL_END
