//
//  ChatTitleView.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/23.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatTitleView.h"

@implementation ChatTitleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)setFrame:(CGRect)frame {
    [super setFrame:CGRectMake(0, 0, self.superview.bounds.size.width, self.superview.bounds.size.height)];
}
@end
