//
//  SelectFileCell.h
//  BOB
//
//  Created by mac on 2020/8/3.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectFileCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(MessageModel *)obj;

@end

NS_ASSUME_NONNULL_END
