//
//  LcwlBlankPageView.m
//  Lcwl
//
//  Created by mac on 2018/12/10.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "LcwlBlankPageView.h"
#define kBlankPageViewTag   10009

@interface LcwlBlankPageView ()

@property (nonatomic, strong) UIImageView *logoImgView;
@property (nonatomic, strong) UILabel     *firstLbl;
@property (nonatomic, strong) UILabel     *secondLbl;
@property (nonatomic, strong) UILabel     *thirdLbl;
@property (nonatomic, strong) UIButton    *setBtn;

@property (nonatomic, copy  ) void (^blankViewTouchedBlock) (void);
@end
@implementation LcwlBlankPageView

+ (void)addBlankPageView:(LcwlBlankPageType)type
           withSuperView:(UIView*)superView
            touchedBlock:(void(^)(void))blankViewTouchedBlock{
    UIView *subView = [superView viewWithTag:kBlankPageViewTag];
    if (!subView) {
        LcwlBlankPageView *blankPageView = [[LcwlBlankPageView alloc] initWithBlankType:type];
        CGRect rect = CGRectInset(superView.bounds,0,44);
        blankPageView.frame = rect;
        blankPageView.tag = kBlankPageViewTag;
        blankPageView.blankViewTouchedBlock = blankViewTouchedBlock;
        [superView addSubview:blankPageView];
        blankPageView.backgroundColor = [UIColor clearColor];
        blankPageView.userInteractionEnabled = YES;
        [superView bringSubviewToFront:blankPageView];
    }
}
- (instancetype)initWithBlankType:(LcwlBlankPageType)type{
    self = [super init];
    if(self) {
        [self initLayout];
        [self setViewLogoImageAndTipTitle:type];
    }
    return self;
}
- (void)initLayout {
//    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureViewTouched)];
//    [self addGestureRecognizer:ges];
    @weakify(self)
    [self addSubview:self.logoImgView];
    [self addSubview:self.firstLbl];
    [self addSubview:self.secondLbl];
    [self addSubview:self.thirdLbl];
    [self addSubview:self.setBtn];
    
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.width.equalTo(@(300*0.72));
        make.height.equalTo(@(300*0.72));
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).offset(-20);
    }];
    
    [self.firstLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.logoImgView.mas_bottom).offset(10);
        make.height.equalTo(@(20));
    }];
    
    [self.secondLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.firstLbl.mas_bottom);
        make.height.equalTo(@(20));
    }];
    
    [self.thirdLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.secondLbl.mas_bottom);
        make.height.equalTo(@(20));
    }];
  
    [self.setBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.thirdLbl.mas_bottom).offset(10);
        make.height.equalTo(@(40));
        make.width.equalTo(@(140));
        make.centerX.equalTo(self.mas_centerX);
    }];
}

-(UIButton*)setBtn{
    if (!_setBtn) {
        _setBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _setBtn.backgroundColor = [UIColor moBlueColor];
        _setBtn.titleLabel.textColor = [UIColor whiteColor];
          _setBtn.titleLabel.font = [UIFont font14];
        _setBtn.layer.masksToBounds = YES;
        _setBtn.layer.cornerRadius = 20;
        [_setBtn setTitle:@"前往设置" forState:UIControlStateNormal];
        [_setBtn addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _setBtn;
}
-(void)btnAction{
    if (self.blankViewTouchedBlock) {
        self.blankViewTouchedBlock();
    }
}

- (void)setViewLogoImageAndTipTitle:(LcwlBlankPageType)type {
    switch (type) {
        case LcwlBlankPageMomentsVCType:{
            self.setBtn.hidden = YES;
            [self configImage:@"momentBlankLogo" first:@"欢迎您成为币火的新用户" second:@"很高兴您开启了币火新生活" third:@"币火期待能为您和朋友们带来愉快的体验！"];
            break;
        }
        case LcwlBlankPageMobileContactVCType:
        {
            [self configImage:@"momentBlankLogo" first:@"需要访问你的通讯录" second:@"你需要进入系统【设置-隐私-通讯录】中" third:@"开启通讯录权限"];
            break;
        }
        case LcwlBlankPageNewFriendVCVCType:
        {
             self.setBtn.hidden = YES;
            //[self configImage:@"momentBlankLogo" first:@"还没有新的好友" second:@" " third:@" "];
            [self configImage:nil first:nil second:@" " third:@" "];
            break;
        }
        case LcwlBlankRecentlyGroupListVCType:
        {
             self.setBtn.hidden = YES;
            [self configImage:@"momentBlankLogo" first:@"还没有群聊" second:@" " third:@" "];
            break;
        }
        case LcwlBlankRecentlyHDTZVCype:
        {
            self.setBtn.hidden = YES;
            [self configImage:@"momentBlankLogo" first:@"还没有互动通知" second:@" " third:@" "];
            break;
        }
            
        default:
            break;
    }
}

- (void)configImage:(NSString *)imageName first:(NSString *)first second:(NSString* )second third:(NSString* )third{
    [self.logoImgView setImage:[UIImage imageNamed:imageName]];
    self.firstLbl.text = first;
    self.secondLbl.text = second;
    self.thirdLbl.text = third;
}

- (UILabel *)firstLbl {
    if (!_firstLbl) {
        _firstLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _firstLbl.textColor = [UIColor moDarkGray];
        [_firstLbl setTextAlignment:NSTextAlignmentCenter];
        [_firstLbl setFont:[UIFont font14]];
        [_firstLbl setNumberOfLines:2];
        [_firstLbl sizeToFit];
    }
    return _firstLbl;
}
- (UILabel *)secondLbl {
    if (!_secondLbl) {
        _secondLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _secondLbl.textColor = [UIColor moDarkGray];
        [_secondLbl setTextAlignment:NSTextAlignmentCenter];
        [_secondLbl setFont:[UIFont font14]];
        [_secondLbl setNumberOfLines:2];
        [_secondLbl sizeToFit];
    }
    return _secondLbl;
}
- (UILabel *)thirdLbl {
    if (!_thirdLbl) {
        _thirdLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _thirdLbl.textColor = [UIColor moDarkGray];
        [_thirdLbl setTextAlignment:NSTextAlignmentCenter];
        [_thirdLbl setFont:[UIFont font14]];
        [_thirdLbl setNumberOfLines:2];
        [_thirdLbl sizeToFit];
    }
    return _thirdLbl;
}
- (UIImageView *)logoImgView {
    if (!_logoImgView) {
        _logoImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImgView.image = [UIImage imageNamed:@"avatar_default"];
    }
    return _logoImgView;
}
+ (void)removeFromSuperView:(UIView*)superView{
    UIView *blankPageView = [superView viewWithTag:kBlankPageViewTag];
    if (blankPageView && [blankPageView superview]) {
        NSAssert([blankPageView isKindOfClass:[LcwlBlankPageView class]], @"该tag的view获取出错");
        [blankPageView removeFromSuperview];
    }
}
@end
