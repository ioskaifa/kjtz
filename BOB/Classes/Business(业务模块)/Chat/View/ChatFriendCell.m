//
//  ChatFriendCell.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/4/2.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatFriendCell.h"
#import <QuartzCore/QuartzCore.h>
//#import "MoYouModel.h"
@implementation ChatFriendCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    //假设这个UIImageView 叫leftimage;
    self.headImg.layer.masksToBounds = YES; //没这句话它圆不起来
    self.headImg.layer.cornerRadius = 20.0; //设置图片圆角的尺度
    
    
    int orignX = CGRectGetMinX(self.nameLabel.frame);
    self.line = [MXSeparatorLine initHorizontalLineWidth:SCREEN_WIDTH-0 orginX:orignX orginY:CGRectGetMaxY(self.bounds)-1];
    [self addSubview:self.line];

    
    self.chatBtn.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//刷新聊天的好友数据
- (IBAction)onClick:(id)sender {
    if (self.callback) {
        self.callback(self.friend);
    }
}

-(void)reloadChatFriend:(MoYouModel*) model{
    self.friend = model;
    
//    NSString* remark = model.remark;
//    if ([StringUtil isEmpty:remark]) {
//        remark = model.name;
//    }
//    self.nameLabel.text = remark;
//    [self.headImg mx_setImageWithURL:[NSURL URLWithString:model.headUrl] placeholderImage:[PlaceHolderImageManager imageWithPlaceHolder:PlaceHolderImageTypeUserAvatar]];
}
+(CGFloat)getHeight{
    return 54;
}
@end
