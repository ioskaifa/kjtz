//
//  ChatListTableCell.h
//  MoPal_Developer
//
//  Created by 王 刚 on 16/2/16.
//  Copyright © 2016年 MoXian. All rights reserved.
//
#import "JSBadgeView.h"
#import "MXConversation.h"
#import <YYKit/YYKit.h>
#import "MXSwipeTableViewCell.h"
@interface ChatListTableCell : MXSwipeTableViewCell
//头像
@property (nonatomic, strong) UIImageView* imageviewAvatar;
//头像
@property (nonatomic, strong) UIImageView* imageviewAvatarBG;
//名字
@property (nonatomic, strong) UILabel* labelName;
//红点
@property (nonatomic, strong) JSBadgeView *badgeView;
///免打扰的红点
@property (nonatomic, strong) UIView *disturbView;
@property (nonatomic, strong) MXConversation *conversation;
//时间
@property (nonatomic, strong) UILabel *labelTime;
//内容
@property (nonatomic, strong) YYLabel *textviewContent;
// 免打扰图标
@property (nonatomic, strong) UIImageView *imageviewDisturb;

@property (nonatomic, strong) UILabel *topLabel ;

//消息状态
@property (nonatomic, strong) UIImageView *imageviewState;

+ (NSString *)cellIdentifierForMessageModel:(MessageModel *)model;
- (id)initWithStyle:(UITableViewCellStyle)style
    reuseIdentifier:(NSString *)reuseIdentifier
          tableView:(UITableView *)tableView
       conversation:(MXConversation *)conver
        marginRight:(CGFloat)marginRight;

+(float)rowHeight;
@end
