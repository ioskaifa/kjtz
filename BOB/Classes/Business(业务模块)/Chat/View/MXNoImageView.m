//
//  MXNoImageView.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/10/26.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "MXNoImageView.h"
#import "SDImageCache.h"

@interface MXNoImageView ()

@property (nonatomic, strong) UILabel *titleLbl;

@end

@implementation MXNoImageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

#pragma mark - UI
- (void)layoutView
{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
    }];
}

- (void)initUI
{
    self.backgroundColor = [UIColor colorWithRed:.90f green:.90f blue:.90f alpha:1.0f];
    [self addSubview:self.titleLbl];
    [self layoutView];
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.textColor = [UIColor moPlaceholderLight];
        _titleLbl.font = [UIFont font19];
        _titleLbl.text = @"Moxian";
    }
    
    return _titleLbl;
}

+ (UIImage *)imageSize:(CGSize)size
{
//    CGFloat scale = [[UIScreen mainScreen] scale];
//    size.width *= scale;
//    size.height *= scale;
    NSString *key = NSStringFromCGSize(size);
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];
    if (image) {
        return image;
    }
    
    MXNoImageView *viewImage = [[MXNoImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    if (size.width < 60) {
        viewImage.titleLbl.font = [UIFont font11];
    } else if (size.width < 100) {
        viewImage.titleLbl.font = [UIFont font12];
    } else {
        viewImage.titleLbl.font = [UIFont font19];
    }
    
    [viewImage layoutIfNeeded];
    [viewImage updateConstraintsIfNeeded];
    
    UIGraphicsBeginImageContextWithOptions(size, YES, SCREEN_SCALE);
    [viewImage.layer renderInContext:UIGraphicsGetCurrentContext()];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    [[SDImageCache sharedImageCache] storeImage:image forKey:key completion:nil];
    
    return image;
}

@end
