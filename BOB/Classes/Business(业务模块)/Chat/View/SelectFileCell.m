//
//  SelectFileCell.m
//  BOB
//
//  Created by mac on 2020/8/3.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SelectFileCell.h"

@interface SelectFileCell ()

@property (nonatomic, strong) UIButton *selectBtn;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *sizeLbl;

@end

@implementation SelectFileCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addBottomSingleLine:[UIColor colorWithHexString:@"#DCDCDC"]];
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 70;
}

- (void)configureView:(MessageModel *)model {
    if (![model isKindOfClass:MessageModel.class]) {
        return;
    }
    if (model.subtype != kMXMessageTypeFile) {
        return;
    }
    NSDictionary *bodyDict =  [MXJsonParser jsonToDictionary:model.body];
    NSString *fileName = bodyDict[@"attr4"];
    NSString *fileSize = bodyDict[@"attr3"];
    NSString *fileType = bodyDict[@"attr2"];
    self.nameLbl.text = StrF(@"%@.%@", fileName, fileType);
    CGSize size = [self.nameLbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 40 - self.imgView.width, MAXFLOAT)];
    size.width = SCREEN_WIDTH - 50 - self.imgView.width - self.selectBtn.width;
    self.nameLbl.mySize = size;
    self.sizeLbl.text = fileSize;
    
    fileType = fileType.lowercaseString;
    kMXFileType type = [MessageModel fileType:fileType];
    self.imgView.image = [UIImage imageNamed:[self.class iconWith:type]];
    self.selectBtn.selected = model.isSelected;
}

+ (NSString *)iconWith:(kMXFileType)type {
    NSString *icon = @"icon_msg_unknow";
    switch (type) {
        case kMXFileTypePDF:{
            icon = @"icon_msg_pdf";
            break;
        }
        case kMXFileTypeWord:{
            icon = @"icon_msg_word";
            break;
        }
        case kMXFileTypeExcel:{
            icon = @"icon_msg_excel";
            break;
        }
        default:
            break;
    }
    return icon;
}

#pragma mark - InitUI
- (void)createUI {
    self.contentView.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.imgView];
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.myLeft = 10;
    rightLayout.myRight = 10;
    rightLayout.weight = 1;
    rightLayout.myCenterY = 0;
    rightLayout.myHeight = MyLayoutSize.wrap;
    
    [layout addSubview:rightLayout];
    [rightLayout addSubview:self.nameLbl];
    [rightLayout addSubview:self.sizeLbl];
    [layout addSubview:rightLayout];
    
    [layout addSubview:self.selectBtn];
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.image = [UIImage imageNamed:@"icon_msg_unknow"];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 15;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.lineBreakMode = NSLineBreakByTruncatingMiddle;
            object.font = [UIFont font15];
            object.textColor = [UIColor moBlack];
            object.numberOfLines = 2;
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)sizeLbl {
    if (!_sizeLbl) {
        _sizeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font13];
            object.textColor = [UIColor grayColor];
            object.myTop = 5;
            object.myHeight = 20;
            object.myLeft = 0;
            object.myRight = 0;
            object;
        });
    }
    return _sizeLbl;
}

- (UIButton *)selectBtn {
    if (!_selectBtn) {
        _selectBtn = [UIButton new];
        [_selectBtn setImage:[UIImage imageNamed:@"icon_public_unselect"] forState:UIControlStateNormal];
        [_selectBtn setImage:[UIImage imageNamed:@"icon_public_select"] forState:UIControlStateSelected];
        [_selectBtn sizeToFit];
        _selectBtn.mySize = _selectBtn.size;
        _selectBtn.myCenterY = 0;
        _selectBtn.myRight = 20;
    }
    return _selectBtn;
}

@end
