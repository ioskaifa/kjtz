//
//  FriendListView.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "FriendListView.h"
#import "MXSeparatorLine.h"
#import "QNManager.h"
static int avatarWidthHeight = 40;
static int padding = 15;
@interface FriendListView ()


@property (nonatomic, strong) UIImageView *arrowImgView;

@property (nonatomic, strong) MXSeparatorLine* line;
@property (nonatomic, assign) RowStyle style;


@end

@implementation FriendListView

-(instancetype)initRowStyleDefault{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self initUI];
        [self layoutIfNeeded];
        [self updateConstraintsIfNeeded];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self layoutIfNeeded];
        [self updateConstraintsIfNeeded];
    }
    return self;
}

    
    
- (UIImageView *)logoImgView
{
    if (!_logoImgView) {
        _logoImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImgView.layer.masksToBounds = YES;
        _logoImgView.layer.cornerRadius = 4;
//        _logoImgView.contentMode = UIViewContentModeScaleAspectFit;
    }
    
    return _logoImgView;
}

- (UIImageView *)arrowImgView
{
    if (!_arrowImgView) {
        _arrowImgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _arrowImgView.image = [UIImage imageNamed:@"rightArrow"];
    }
    
    return _arrowImgView;
}

- (UIButton *)rightBtn
{
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn setTitle:@"邀请" forState:UIControlStateNormal];
        [_rightBtn setTitleColor:[UIColor moBlueColor] forState:UIControlStateNormal];
        _rightBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _rightBtn.layer.cornerRadius = 3;
        _rightBtn.clipsToBounds = YES;
        [_rightBtn addTarget:self action:@selector(rightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _rightBtn;
}

- (UIButton *)selectBnt
{
    if (!_selectBnt) {
        _selectBnt = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectBnt setImage:[UIImage imageNamed:@"chatRadioUnChosed"] forState:UIControlStateNormal];
        [_selectBnt setImage:[UIImage imageNamed:@"chatRadioChosed"] forState:UIControlStateSelected];
        [_selectBnt addTarget:self action:@selector(selectBntClick:) forControlEvents:UIControlEventTouchUpInside];
        _selectBnt.userInteractionEnabled = NO;
        _selectBnt.hidden = YES;
    }
    
    return _selectBnt;
}

-(void)rightBtnClick:(id)sender{
    if (self.rightBtnClickCallback) {
        self.rightBtnClickCallback(self);
    }
}

-(void)selectBntClick:(UIButton *)sender{
    sender.selected = !sender.selected;
}

- (void)setSelectGroupType:(SelectGroupType)selectGroupType {
    _selectGroupType = selectGroupType;
    
    self.selectBnt.hidden = selectGroupType == SelectGroupTypeNone;
    if(selectGroupType != SelectGroupTypeNone) {
        [self.logoImgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(55);
        }];
    }
}

- (UILabel *)nameLbl
{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _nameLbl.text = @"";
        _nameLbl.font = [UIFont systemFontOfSize:15];
    }
    
    return _nameLbl;
}
- (UILabel *)descLbl
{
    if (!_descLbl) {
        _descLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _descLbl.font = [UIFont font12];
        _descLbl.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
        _descLbl.text = @"";
    }
    
    return _descLbl;
}
- (UILabel *)rightLabel
{
    if (!_rightLabel) {
        _rightLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _rightLabel.font = [UIFont font12];
        _rightLabel.text = @"";
        _rightLabel.textAlignment = NSTextAlignmentRight;
    }
    
    return _rightLabel;
}

#pragma mark - UI
- (void)initUI
{
    self.backgroundColor = [UIColor whiteColor];
    _logoImgView.layer.masksToBounds = YES;
    _logoImgView.layer.cornerRadius =  10;
    
    [self addSubview:self.selectBnt];
    [self addSubview:self.logoImgView];
    [self addSubview:self.arrowImgView];
    [self addSubview:self.nameLbl];
    [self addSubview:self.descLbl];
    [self addSubview:self.rightBtn];
    [self addSubview:self.rightLabel];
    _line =[MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self addSubview:_line];
    [self layoutView];
}
-(void)setShowDownLine:(BOOL)isShow{
    if (!isShow) {
        self.line.hidden = !isShow;
    }
}
-(void)setShowArrow:(BOOL)isShow{
    if (!isShow) {
        self.arrowImgView.hidden = !isShow;
    }
}
- (void)layoutView
{
    @weakify(self)
    [self.selectBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(15).priorityLow();
        make.centerY.equalTo(self.mas_centerY);
        make.width.height.equalTo(@(20));
    }];
    
    [self.logoImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(padding);
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@(avatarWidthHeight));
        make.height.equalTo(@(avatarWidthHeight));
    }];
    [self.arrowImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self.mas_right).offset(-padding);
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@(7));
        make.height.equalTo(@(13));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.logoImgView.mas_right).offset(10);
        make.right.equalTo(self.arrowImgView.mas_left).offset(-10);
        make.centerY.equalTo(self.mas_centerY);
        
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.nameLbl.mas_left);
        make.right.equalTo(@(0));
        make.bottom.equalTo(self.mas_bottom).offset(-0.5);
        make.height.equalTo(@(0.5));
        
    }];
    self.rightBtn.hidden = YES;
    self.rightLabel.hidden = YES;
    return;
}

-(void)reloadLocalLogo:(NSString *)logo name:(NSString* )name{
    
    if(logo == nil)  {
        [self.logoImgView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(0));
        }];
    } else {
        self.logoImgView.image = [UIImage imageNamed:logo];
    }
    
    if (![StringUtil isEmpty:name]) {
        self.nameLbl.text = name;
    }
}

-(void)reloadLocalLogo:(NSString *)logo name:(NSString* )name desc:(NSString* )desc{

    [self reloadLocalLogo:logo name:name];
    
    if (![StringUtil isEmpty:desc]) {
        self.descLbl.text = desc;
    }
}

-(void)reloadLocalLogo:(NSString *)logo attrDesc:(NSString* )desc{
    self.logoImgView.image = [UIImage imageNamed:logo];
    self.descLbl.text = desc;
}

-(void)reloadLogo:(NSString *)logo name:(NSString* )name{
    if ([StringUtil isEmpty:logo]) {
          [self.logoImgView setImage:[UIImage imageNamed:@"avatar_default"]];
    }else{
        BOOL isLocalPath = ([logo hasPrefix:@"/Users/"] || [logo hasPrefix:@"/var/"]);
        if (isLocalPath) {
            UIImage* image = [UIImage imageWithContentsOfFile:logo];
            if (image) {
                [self.logoImgView setImage:image];
            }else{
                [self.logoImgView setImage:[UIImage imageNamed:@"avatar_default"]];
            }
        }else{
            [self.logoImgView sd_setImageWithURL:[NSURL URLWithString:logo] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
        }
    }
  
    if (![StringUtil isEmpty:name]) {
        self.nameLbl.text = name;
    }
}

-(void)reloadLogo:(NSString *)logo name:(NSString* )name desc:(NSString* )desc{
    [self reloadLogo:logo name:name];
    if (![StringUtil isEmpty:desc]) {
        self.descLbl.text = desc;
    }
}


-(void)reloadLogo:(NSString *)logo name:(NSString* )name desc:(NSString* )desc btntitle:(NSString *)btntitle{
    [self reloadLogo:logo name:name desc:desc];
    if (![StringUtil isEmpty:btntitle]) {
        [self.rightBtn setTitle:btntitle forState:UIControlStateNormal];
        
        if([btntitle isEqualToString:@"接受"] || [btntitle isEqualToString:@"添加"] || [btntitle isEqualToString:@"邀请"]) {
            self.rightBtn.backgroundColor = [UIColor colorWithHexString:@"#EDEFEC"];
            [self.rightBtn setTitleColor:[UIColor colorWithHexString:@"#0088FF"] forState:UIControlStateNormal];
        } else {
            self.rightBtn.backgroundColor = [UIColor whiteColor];
            [self.rightBtn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:UIControlStateNormal];
        }
    }
}

-(void)reloadLogo:(NSString *)logo attrName:(NSMutableAttributedString* )name {
    [self reloadLogo:logo name:@""];
    self.nameLbl.attributedText = name;
}

-(void)reloadLogo:(NSString *)logo attrName:(NSMutableAttributedString* )name attrDesc:(NSMutableAttributedString* )desc{
     [self reloadLogo:logo name:@""];
    self.nameLbl.attributedText = name;
    self.descLbl.attributedText = desc;

}

-(void)reloadLogo:(NSString *)logo attrName:(NSMutableAttributedString* )name attrDesc:(NSMutableAttributedString* )desc rightTip:(NSString* )tip{
     [self reloadLogo:logo name:@""];
    self.nameLbl.attributedText = name;
    self.descLbl.attributedText = desc;
    self.rightLabel.text = tip;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (self.selectCallback) {
        self.selectCallback(self);
    }else{
        [super touchesBegan:touches withEvent:event];
    }
}

-(void)style:(RowStyle)style{
    self.style = style;
   
    if (self.style == RowStyleDefault) {
        @weakify(self)
        [self.logoImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.mas_left).offset(padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.height.equalTo(@(avatarWidthHeight));
        }];
        [self.arrowImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.right.equalTo(self.mas_right).offset(-padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(7));
            make.height.equalTo(@(13));
        }];
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.logoImgView.mas_right).offset(10);
            make.right.equalTo(self.arrowImgView.mas_left).offset(-10);
            make.centerY.equalTo(self.mas_centerY);
            
        }];
        [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.nameLbl.mas_left);
            make.right.equalTo(self.arrowImgView.mas_right);
            make.bottom.equalTo(self.mas_bottom).offset(-1);
            make.height.equalTo(@(1));
            
        }];
        self.rightBtn.hidden = YES;
        self.rightLabel.hidden = YES;
        return;
    }else if(self.style == RowStyleSubtitle){
        @weakify(self)
        [self.logoImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.mas_left).offset(padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.height.equalTo(@(avatarWidthHeight));
        }];
        [self.arrowImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.right.equalTo(self.mas_right).offset(-padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(7));
            make.height.equalTo(@(13));
        }];
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.logoImgView.mas_right).offset(10);
            make.right.equalTo(self.arrowImgView.mas_left).offset(-10);
            make.bottom.equalTo(self.mas_centerY);
            make.height.equalTo(@(20));
            
        }];
        [self.descLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.nameLbl.mas_left);
            make.right.equalTo(self.arrowImgView.mas_left).offset(-10);
            make.top.equalTo(self.mas_centerY);
            make.height.equalTo(@(20));
            
        }];
        [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.nameLbl.mas_left);
            make.right.equalTo(self.arrowImgView.mas_right);
            make.bottom.equalTo(self.mas_bottom).offset(-1);
            make.height.equalTo(@(1));
            
        }];
        self.rightBtn.hidden = YES;
        self.rightLabel.hidden = YES;
        return;
    }else if(self.style == RowStyleButton){
        @weakify(self)
        [self.logoImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.mas_left).offset(padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.height.equalTo(@(avatarWidthHeight));
        }];
        [self.rightBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.right.equalTo(self.mas_right).offset(-padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(56));
            make.height.equalTo(@(28));
        }];
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.logoImgView.mas_right).offset(10);
            make.right.equalTo(self.rightBtn.mas_left).offset(-10);
            make.bottom.equalTo(self.mas_centerY);
            make.height.equalTo(@(20));
            
        }];
        [self.descLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.nameLbl.mas_left);
            make.right.equalTo(self.rightBtn.mas_left).offset(-10);
            make.top.equalTo(self.mas_centerY);
            make.height.equalTo(@(20));
            
        }];
        [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.nameLbl.mas_left);
//            make.right.equalTo(self.rightBtn.mas_right);
            make.right.equalTo(self.mas_right);
            make.bottom.equalTo(self.mas_bottom);
            make.height.equalTo(@(0.5));
            
        }];
        self.rightBtn.hidden = NO;
        self.rightLabel.hidden = YES;
        return;
    }else if(self.style == RowStyleRightLabel){
        @weakify(self)
        [self.logoImgView mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.mas_left).offset(padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.height.equalTo(@(avatarWidthHeight));
        }];
        [self.rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.right.equalTo(self.mas_right).offset(-padding);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@(100));
            make.height.equalTo(@(30));
        }];
        [self.nameLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.logoImgView.mas_right).offset(10);
            make.right.equalTo(self.rightLabel.mas_left).offset(-10);
            make.bottom.equalTo(self.mas_centerY);
            make.height.equalTo(@(20));
            
        }];
        [self.descLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.nameLbl.mas_left);
            make.right.equalTo(self.rightLabel.mas_left).offset(-10);
            make.top.equalTo(self.mas_centerY);
            make.height.equalTo(@(20));
            
        }];
        [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.nameLbl.mas_left);
            make.right.equalTo(self.mas_right);
            make.bottom.equalTo(self.mas_bottom);
            make.height.equalTo(@(0.5));
            
        }];
        self.rightLabel.hidden = NO;
        self.rightBtn.hidden = YES;
    }
    
    self.arrowImgView.hidden = YES;
    [self setNeedsDisplay];
}

@end
