//
//  FriendViewModel.h
//  Lcwl
//
//  Created by 王刚  on 2018/11/22.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class FriendModel;
@interface FriendViewModel : NSObject
+(NSArray* )headerData;
+(NSMutableArray* )getFriendData;
+(NSMutableArray* )getFriendDatabyFilter:(NSString *)userId;
@end

NS_ASSUME_NONNULL_END
