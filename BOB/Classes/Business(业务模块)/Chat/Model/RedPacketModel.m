//
//  RedPacketModel.m
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "RedPacketModel.h"

@implementation RedPacketModel

+(RedPacketModel *)dictionaryToModal:(NSDictionary *)dict{
    
    RedPacketModel* model = [[RedPacketModel alloc]init];
    model.acceId = [NSString stringWithFormat:@"%@",dict[@"acceId"]];
    model.acceType = [@([dict[@"flg"] integerValue]+1) description];
    model.acceUserAvatar = [NSString stringWithFormat:@"%@",dict[@"acceUserAvatar"]];
    model.acceUserName = [NSString stringWithFormat:@"%@",dict[@"acceUserName"]];
    model.redPacketId = [@([dict[@"id"] integerValue]) description];
    model.redPacketMoney = [NSString stringWithFormat:@"%@",dict[@"total_money"]];
    model.redPacketNum = [@([dict[@"num"] integerValue]) description];
    model.redPacketType = [@([dict[@"flg"] integerValue]) description];
    model.remark = [NSString stringWithFormat:@"%@",dict[@"title"]];
    model.robbedNum = [@([dict[@"last"] integerValue]) description];
    model.sendId = [@([dict[@"uid"] integerValue]) description];
    model.sendTime = [NSString stringWithFormat:@"%@",dict[@"add_time"]];
    model.sendUserName = [NSString stringWithFormat:@"%@",dict[@"nickname"]];
    model.sendUserAvatar = [NSString stringWithFormat:@"%@",dict[@"pic"]];
    model.randomMoney = [NSString stringWithFormat:@"%@",dict[@"randomMoney"]];
    model.type = [NSString stringWithFormat:@"%@",dict[@"type"]];
    
    @onExit {
        [model checkPropertyAndSetDefaultValue];
    };
    
    return model;
}
@end
