//
//  AutoTransferModel.m
//  BOB
//
//  Created by mac on 2019/8/2.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "AutoTransferModel.h"
#import "LcwlChat.h"

@implementation AutoTransferModel

- (NSMutableArray *)selectTeachers {
    if(_selectTeachers == nil) {
        _selectTeachers = [NSMutableArray arrayWithCapacity:1];
    }
    return _selectTeachers;
}

- (NSMutableArray *)selectTargetGroup {
    if(_selectTargetGroup == nil) {
        _selectTargetGroup = [NSMutableArray arrayWithCapacity:1];
    }
    return _selectTargetGroup;
}

- (BOOL)needTransfer:(MessageModel *)model {
    if([UDetail.user.is_group_send integerValue] == 0 || !self.transferStatus || ![self transferTimeIsValidate] || ![self isValidateMessageType:model]) {
        return NO;
    } else {
        return YES;
    }
}

// subtype 消息类型 1-文本 2-图片 3-语音 4-位置 5-GIF  6文件 7视频
- (BOOL)isValidateMessageType:(MessageModel *)model {
    if(model.subtype == 1) {
        return self.transferType & TransferTypeText;
    } else if(model.subtype == 2) {
        return self.transferType & TransferTypePicture;
    } else if(model.subtype == 3) {
        return self.transferType & TransferTypeVoice;
    } else if(model.subtype == 5) {
        return self.transferType & TransferTypeTextEmoji;
    } else {
        return NO;
    }
    
}

- (BOOL)transferTimeIsValidate {
    if(self.transferTime == nil) { //全天转发
        return YES;
    }
    
    NSArray *timesArr = [self.transferTime componentsSeparatedByString:@"~"];
    NSString *startTime = [timesArr firstObject];
    NSString *endTime = [timesArr lastObject];
    NSString *curTime = [self currTime];
    if([startTime compare:curTime] == NSOrderedDescending || [curTime compare:endTime] == NSOrderedDescending) {
        return NO;
    }
    return YES;
}

- (NSTimeInterval )getLocalDate {
    //处理时区差的问题
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:date];
    NSDate *localDate = [date dateByAddingTimeInterval: interval];
    return [localDate timeIntervalSince1970];
}

- (NSString *)currTime {
    NSString *dateStr = [[NSDate dateWithTimeIntervalSince1970:[self getLocalDate]] description];
    NSString *time = [[dateStr componentsSeparatedByString:@" "] safeObjectAtIndex:1];
    return [time substringToIndex:5];
}
@end
