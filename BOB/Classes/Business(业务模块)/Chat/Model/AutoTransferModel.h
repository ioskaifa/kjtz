//
//  AutoTransferModel.h
//  BOB
//
//  Created by mac on 2019/8/2.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BaseObject.h"
#import "ChatGroupModel.h"
#import "MessageModel.h"
#import "FriendModel.h"

///转发支持的类型
typedef NS_ENUM(NSInteger, TransferType) {
    TransferTypeText = 1 << 0,
    TransferTypeVoice = 1 << 1,
    TransferTypePicture = 1 << 2,
    TransferTypeFiles = 1 << 3, ///网页，文件
    TransferTypeTextEmoji = 1 << 4
};

//NS_ASSUME_NONNULL_BEGIN

@interface AutoTransferModel : BaseObject
///转发状态
@property (nonatomic, assign) BOOL transferStatus;

///转发时间,全天转播: nil  自定义时间:HH:MM~HH:MM
@property (nonatomic, copy) NSString *transferTime;

///转发类型
@property (nonatomic, assign) TransferType transferType;

///主讲群id
//@property (nonatomic, strong) NSString *mainChatGroupId;
@property (nonatomic, strong) ChatGroupModel *mainChatGroupModel;

///讲师id
@property (nonatomic, strong) NSMutableArray<FriendModel *> *selectTeachers;

///目标群
//@property (nonatomic, strong) NSMutableArray<NSString *> *selectTargetGroup;
@property (nonatomic, strong) NSMutableArray<ChatGroupModel *> *selectTargetGroup;

///是否需要转发
- (BOOL)needTransfer:(MessageModel *)model;
@end

//NS_ASSUME_NONNULL_END
