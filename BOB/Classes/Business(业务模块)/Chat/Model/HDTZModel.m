//
//  HDTZModel.m
//  Lcwl
//
//  Created by mac on 2019/1/23.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "HDTZModel.h"

@implementation HDTZModel

+ (HDTZModel *)dictionaryToModel:(NSDictionary*)dict{
    HDTZModel *model = [[HDTZModel alloc]init];
    model.circle_content = [NSString stringWithFormat:@"%@",dict[@"circle_content"]];
    model.circle_id = [NSString stringWithFormat:@"%@",dict[@"circle_id"]];
    model.circle_image_size = [NSString stringWithFormat:@"%@",dict[@"circle_image_size"]];
    model.circle_image_type = [NSString stringWithFormat:@"%@",dict[@"circle_image_type"]];
    model.circle_link = [NSString stringWithFormat:@"%@",dict[@"circle_link"]];
    model.circle_message_type = [NSString stringWithFormat:@"%@",dict[@"circle_message_type"]];
    model.circle_open_type = [NSString stringWithFormat:@"%@",dict[@"circle_open_type"]];
    model.circle_send_type = [NSString stringWithFormat:@"%@",dict[@"circle_send_type"]];
    model.circle_user_addr = [NSString stringWithFormat:@"%@",dict[@"circle_user_addr"]];
    model.circle_user_id = [NSString stringWithFormat:@"%@",dict[@"circle_user_id"]];
    model.notice_id = [NSString stringWithFormat:@"%@",dict[@"notice_id"]];
    model.notice_type = [NSString stringWithFormat:@"%@",dict[@"notice_type"]];
    model.op_user_head_photo = [NSString stringWithFormat:@"%@",dict[@"op_user_head_photo"]];
    model.op_user_id = [NSString stringWithFormat:@"%@",dict[@"op_user_id"]];
    model.op_user_name = [NSString stringWithFormat:@"%@",dict[@"op_user_name"]];
    model.relation_user_id = [NSString stringWithFormat:@"%@",dict[@"relation_user_id"]];
    model.relation_user_name = [NSString stringWithFormat:@"%@",dict[@"relation_user_name"]];
    model.send_time = [NSString stringWithFormat:@"%@",dict[@"send_time"]];
    model.notice_content = [NSString stringWithFormat:@"%@",dict[@"notice_content"]];
    return model;
}

@end
