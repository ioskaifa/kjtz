//
//  ChatViewVM.m
//  MoPal_Developer
//
//  Created by 王 刚 on 16/4/28.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "ChatViewVM.h"
//#import "GetFriendsManager.h"
#import "LcwlChat.h"
#import "FriendModel.h"
#import "MXConversation.h"
#import "MessageModel.h"
#import "NSDate+Category.h"
#import "MXChatDBUtil.h"
#import "ChatSettingVC.h"
#import "TalkManager.h"
#import "ChatCacheFileUtil.h"
@implementation ChatViewVM
-(instancetype)init{
    if (self = [super init]) {
        _recordsArray = [[NSMutableArray alloc]initWithCapacity:0];
        
    }
    return self;
}

-(void)addFriend:(id)sender{
}


//给数据库中的数据添加时间标签
-(NSMutableArray*)addTimeLabel:(NSArray*)array{
    NSMutableArray *ret = [NSMutableArray array];
    if (self.recordsArray.count == 0) {
        _chatTagDate = nil;
    }
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj && [obj isKindOfClass:[MessageModel class]]) {
            MessageModel* message = (MessageModel*)obj;
            NSDate *createDate =[NSDate dateWithTimeIntervalInMilliSecondSince1970:[message.sendTime doubleValue]];
            NSTimeInterval tempDate = [createDate timeIntervalSinceDate:self.chatTagDate];
            if (tempDate > 180 || tempDate < -180 || (self.chatTagDate == nil)) {
                [ret safeAddObj:[createDate formattedTime]];
                _chatTagDate = createDate;
            }
            [ret safeAddObj:message];
        }
    }];
    return ret;
}
-(void)resetRefreshChatMessage{
    [self resetChatMessage];
    self.topMsgTime = @"";
}
//恢复数据到初始 并且做一次刷新
-(void)resetChatMessage{
    _chatTagDate = nil;
    [self.recordsArray removeAllObjects];
    
}
//数据累加
-(void)getNextChatMessages{
    if (self.recordsArray.count > 0 ) {
        MessageModel* firstObject = [self.recordsArray objectAtIndex:1];
        if(firstObject){
            _chatTagDate = nil;
            if ([firstObject isKindOfClass:[MessageModel class]]) {
                self.topMsgTime = firstObject.sendTime;
            }
        }
    }else{
        self.topMsgTime = @"";
    }
    
}

-(NSMutableArray*)selectBeforeChatRecords{
    NSString* chatID = self.conversation.chat_id;
    NSArray* tmpArray = [[MXChatDBUtil sharedDataBase] selectBeforeChatRecords:chatID time:self.topMsgTime];
    [self.conversation sendAckReceipt:tmpArray];
    return [self addTimeLabel:tmpArray];
}



-(UIView* )titleView{
    if (_titleView == nil) {
        _titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-160, 44)];
        _titleView.backgroundColor = [UIColor clearColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
        label.tag = 101;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium];
        [_titleView addSubview:label];
        
        UIImageView *sound = [[UIImageView alloc]initWithFrame:CGRectZero];
        sound.tag = 102;
        sound.image = [UIImage imageNamed:@"doNotDisturb"];
        [_titleView addSubview:sound];
        
    }
    return _titleView;
}
//右边的button
- (UIButton* )settingBtn{
    if (_settingBtn == nil) {
        _settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_settingBtn addTarget:self action:@selector(chatSetting:) forControlEvents:UIControlEventTouchUpInside];
//        if (self.conversation.conversationType == eConversationTypeGroupChat) {
//            _settingBtn.frame = CGRectMake(0, 0, 23, 20);
//            [_settingBtn setImage:[UIImage imageNamed:@"topbar_icon_more_black_normal"] forState:UIControlStateNormal];
//            [_settingBtn setImage:[UIImage imageNamed:@"topbar_icon_more_black_normal"] forState:UIControlStateHighlighted];
//        }else{
//            _settingBtn.frame = CGRectMake(0, 0, 20, 20);
//            [_settingBtn setImage:[UIImage imageNamed:@"chat_icon_setting_nor"] forState:UIControlStateNormal];
//            [_settingBtn setImage:[UIImage imageNamed:@"chat_icon_setting_nor"] forState:UIControlStateHighlighted];
//        }
        _settingBtn.frame = CGRectMake(0, 0, 20, 20);
        [_settingBtn setImage:[UIImage imageNamed:@"topbar_icon_more_black_normal"] forState:UIControlStateNormal];
        [_settingBtn setImage:[UIImage imageNamed:@"topbar_icon_more_black_normal"] forState:UIControlStateHighlighted];
        _settingBtn.imageView.tintColor = [UIColor blackColor];
    }
    return _settingBtn;
}

-(void)chatSetting:(id)sender{
//    if (!UDetail.user.is_valid) {
//        ///未授权
//        [NotifyHelper showMessageWithMakeText:@"您暂未获得授权，\n请联系相对应联系人开启授权... "];
//        return;
//    }
    if (self.conversation.conversationType == eConversationTypeGroupChat) {
        [MXRouter openURL:@"lcwl://ChatRoomSettingVC" parameters:@{@"con":self.conversation,@"callback":^{
            [self resetRefreshChatMessage];
        }}];
    }else{
        [MXRouter openURL:@"lcwl://ChatSettingVC" parameters:@{@"con":self.conversation,@"callback":^{
            [self resetRefreshChatMessage];
        }}];
    }
}


-(void)setTitle:(NSString* )title openSound:(BOOL) bol{
    UILabel* label = (UILabel* )[_titleView viewWithTag:101];
    label.text = title;
    
    UIImageView* sound = (UIImageView* )[_titleView viewWithTag:102];
    sound.hidden = bol;

    int width = SCREEN_WIDTH-80-80;
    int height = 44;
    CGRect rect = [label.text boundingRectWithSize:(CGSize) { MAXFLOAT, height } options:NSStringDrawingUsesFontLeading attributes:@{ NSFontAttributeName : label.font } context:nil];
    CGSize size = rect.size;
    if (size.width > width) {
        label.frame = CGRectMake(0, 0, width, height);
    }else{
        label.frame = CGRectMake(((width)-size.width)/2, 0, size.width, height);
    }
    sound.frame = CGRectMake(CGRectGetMaxX(label.frame), (height-12)/2, 12, 12);
}

-(void)sendForwardMessage:(MessageModel* )message{
    message.msg_direction = 1;
    if (message.type == MessageTypeNormal ||
        message.type == MessageTypeGroupchat ) {
        if (message.subtype == kMXMessageTypeImage) {
            NSString* imagePath = [[ChatCacheFileUtil sharedInstance] handleChatImagePath:message.locFileUrl remoteUrl:message.imageRemoteUrl];
            UIImage* image = [UIImage imageWithContentsOfFile:imagePath];
            imagePath = [TalkManager saveChatImage:image];
            message.locFileUrl = imagePath;
            [[LcwlChat shareInstance].chatManager insertMessage:message];
            message.state=kMXMessageStateSending;
            if ([self.conversation.chat_id isEqualToString:message.toID]) {
                [[LcwlChat shareInstance].chatManager sendImageMessage:message delegate:self.vc];
            }else{
                 [[LcwlChat shareInstance].chatManager sendImageMessage:message delegate:nil];
            }
        }else if(message.subtype == kMXMessageTypeVoice){
            [[LcwlChat shareInstance].chatManager insertMessage:message];
            message.state = kMXMessageStateSending;
            if ([self.conversation.chat_id isEqualToString:message.toID]) {
                [[LcwlChat shareInstance].chatManager sendVoiceMessage:message delegate:self.vc];
            }else{
                [[LcwlChat shareInstance].chatManager sendVoiceMessage:message delegate:nil];
            }
        } else if (message.subtype == kMXMessageTypeFile){
            [[LcwlChat shareInstance].chatManager insertMessage:message];
            message.state = kMXMessageStateSending;
            if ([self.conversation.chat_id isEqualToString:message.toID]) {
                [[LcwlChat shareInstance].chatManager sendFileMessage:message delegate:self.vc];
            }else{
                [[LcwlChat shareInstance].chatManager sendFileMessage:message delegate:nil];
            }
        } else {
            [[LcwlChat shareInstance].chatManager sendMessage:message completion:nil];
        }
    }else if(message.type == MessageTypeRich){
        [[LcwlChat shareInstance].chatManager sendMessage:message completion:nil];
    }
  
}
@end
