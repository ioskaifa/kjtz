//
//  HDTZModel.h
//  Lcwl
//
//  Created by mac on 2019/1/23.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HDTZModel : NSObject

@property  (nonatomic,copy) NSString* circle_content;

@property  (nonatomic,copy) NSString* circle_id;

@property  (nonatomic,copy) NSString* circle_image_size;

@property  (nonatomic,copy) NSString* circle_image_type;

@property  (nonatomic,copy) NSString* circle_link;

@property  (nonatomic,copy) NSString* circle_message_type;

@property  (nonatomic,copy) NSString* circle_open_type;

@property  (nonatomic,copy) NSString* circle_send_type;

@property  (nonatomic,copy) NSString* circle_user_addr;

@property  (nonatomic,copy) NSString* circle_user_id;

@property  (nonatomic,copy) NSString* notice_id;

@property  (nonatomic,copy) NSString* notice_type;

@property  (nonatomic,copy) NSString* op_user_head_photo;

@property  (nonatomic,copy) NSString* op_user_id;

@property  (nonatomic,copy) NSString* op_user_name;

@property  (nonatomic,copy) NSString* relation_user_id;

@property  (nonatomic,copy) NSString* relation_user_name;

@property  (nonatomic,copy) NSString* send_time;

@property  (nonatomic,copy) NSString* notice_content;

+ (HDTZModel *)dictionaryToModel:(NSDictionary*)dict;
@end

NS_ASSUME_NONNULL_END
