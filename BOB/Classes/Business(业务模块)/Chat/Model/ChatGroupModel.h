//
//  ChatRoomModel.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/8/4.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatGroupModel: BaseObject

@property (nonatomic, copy  ) NSString  * groupId;

@property (nonatomic, copy  ) NSString  * groupName;

///是否设置群名（0：未设置  1：设置）
@property (nonatomic, copy  ) NSString  * is_set_name;

@property (nonatomic, copy  ) NSString  * creatorId;

@property (nonatomic, copy  ) NSString  * groupAvatar;

@property (nonatomic, assign) NSInteger groupMemNum;

@property (nonatomic, assign) NSInteger maxGroupMemNum;

@property (nonatomic, copy  ) NSString  * createTime;

//免打扰
@property (nonatomic) BOOL enable_disturb;

//置顶
@property (nonatomic) BOOL enable_top;

//显示群成员昵称
@property (nonatomic) BOOL enable_nicknameShow;

//是否保存到通讯录
@property (nonatomic) BOOL enable_contact;

//3 群主 2 管理员 1 成员
@property (nonatomic) NSInteger roleType;

//置顶时间
@property (nonatomic, copy) NSString* lastUpdateTime;

//群里我的昵称
@property (nonatomic, copy) NSString* nickname;

//群二维码
@property (nonatomic, copy) NSString* qrcodeUrl;



+(ChatGroupModel*)dictionaryToModel:(NSDictionary*)dict;


+(ChatGroupModel*)chatDictionaryToModel:(NSDictionary*)dict;

+(void)setGroupIconWithURLArray:(NSString* )avatar image:(UIImageView* )imgageView;

@end
