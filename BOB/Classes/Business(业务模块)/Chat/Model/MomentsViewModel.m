//
//  MomentsViewModel.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/22.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "MomentsViewModel.h"
#import "MXConversation.h"
#import "RoomManager.h"
#import "ChatSendHelper.h"
#import "TalkManager.h"
#import "FriendsManager.h"
#import "UserModel.h"
#import "LcwlChat.h"
#import "QRCodeScanManage.h"

@interface MomentsViewModel ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@end
@implementation MomentsViewModel
- (instancetype)initVC:(UIViewController *)vc{
    if (self = [super init]) {
        self.vc = vc;
    }
    return self;
}

- (DXPopover *)popover {
    if (!_popover) {
        _popover = [DXPopover popover];
        self.popover.backgroundColor = [UIColor blackColor];
        self.popover.contentInset = UIEdgeInsetsZero;
    }
    return _popover;
}

- (UITableView *)popoverTableView {
    if (!_popoverTableView) {
        _popoverTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 160, 144*6) style:UITableViewStylePlain];
        _popoverTableView.backgroundColor = [UIColor clearColor];
        _popoverTableView.delegate = self;
        _popoverTableView.dataSource = self;
        _popoverTableView.rowHeight = 60;
        _popoverTableView.scrollEnabled = NO;
        _popoverTableView.tableFooterView = [UIView new];
        _popoverTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _popoverTableView.separatorColor = [UIColor whiteColor];
        if ([_popoverTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [_popoverTableView setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([_popoverTableView respondsToSelector:@selector(setLayoutMargins:)]) {
            [_popoverTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        Class cls = UITableViewCell.class;
        [_popoverTableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
    }
    return _popoverTableView;
}

- (void)resetPopver {
    self.popoverTableView.width = 150;
    self.popoverTableView.height = MIN(self.popoverTableView.rowHeight * [self popoverData].count, 375);
}

-(NSArray *)popoverData{
    return @[
            @{@"img":@"发起群聊",@"content":@"发起群聊",@"vString":@"lcwl://GroupChatVC",@"callback":^(NSArray* selectArray){
                             UserModel* user = [LcwlChat shareInstance].user;
                             if (selectArray.count == 1) {
                                 
                                 id model = selectArray[0];
                                 if ([model isKindOfClass:[ChatGroupModel class]]) {
                                     ChatGroupModel* group = (ChatGroupModel*)model;
                                     MXConversation* con = [[MXConversation alloc]init];
                                     con.conversationType = eConversationTypeGroupChat;
                                     con.chat_id = group.groupId;
                                     [[LcwlChat shareInstance].chatManager createConversationObject:con];
                                     [TalkManager pushChatViewUserId:group.groupId type:eConversationTypeGroupChat];
                                 }else if([model isKindOfClass:[FriendModel class]]){
                                     FriendModel* friend = (FriendModel*)model;
                                     MXConversation* con = [[MXConversation alloc]init];
                                     con.conversationType = eConversationTypeChat;
                                     con.chat_id = friend.userid;
                                     [[LcwlChat shareInstance].chatManager createConversationObject:con];
                                    [TalkManager pushChatViewUserId:friend.userid type:eConversationTypeChat];
                                 }
                                
                                 return;
                             }
                             __block NSMutableArray* idArray = [[NSMutableArray alloc]initWithCapacity:10];
                              __block NSMutableArray* nameArray = [[NSMutableArray alloc]initWithCapacity:10];
                             [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                FriendModel* friend = (FriendModel*)obj;
                                 [idArray addObject:friend.userid];
                                 [nameArray addObject:friend.name];
                             }];
            //                 NSData *data=[NSJSONSerialization dataWithJSONObject:idArray options:NSJSONWritingPrettyPrinted error:nil];
            //
            //                 NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            //                 jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            //                 jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                             [RoomManager createGroup:@{@"user_ids":[idArray componentsJoinedByString:@","] ?: @""} completion:^(id group, NSString *error) {
                                 if (group) {
                                     ChatGroupModel* model = (ChatGroupModel*)group;
                                     [[LcwlChat shareInstance].chatManager insertGroup:model];
                                     NSString* tip = [NSString stringWithFormat:@"你邀请%@加入了群聊",[nameArray componentsJoinedByString:@"、"]];
                                     
                                     [ChatSendHelper sendSgroup:tip from:model.groupId messageType:SGROUPTypeCreate];
                                     FriendModel* selfModel = [user toFriendModel];
                                     selfModel.groupid = model.groupId;
                                     selfModel.groupNickname = selfModel.name;
                                     [[LcwlChat shareInstance].chatManager insertGroupMember:selfModel];
                                     [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                         FriendModel* friend = (FriendModel*)obj;
                                         friend.groupid = model.groupId;
                                         friend.groupNickname = friend.name;
                                         [[LcwlChat shareInstance].chatManager insertGroupMember:friend];
                                     }];
                                     [TalkManager pushChatViewUserId:model.groupId type:eConversationTypeGroupChat];
                                     
                                 }else{
                                     [NotifyHelper showMessageWithMakeText:error];
                                 }
                                 }] ;
                                
            }},
            @{@"img":@"添加好友",@"content":@"添加好友",@"vString":@"lcwl://AddFriendVC"},
            @{@"img":@"扫一扫_home",@"content":@"扫一扫",@"vString":@"QRCodeScanManage"},
            @{@"img":@"我的团队二维码",@"content":@"我的二维码",@"vString":@"lcwl://MyQRCodeVC"}
    ];
    return @[//@{@"img":@"im_publish_feed_icon",@"content":@"发朋友圈",@"vString":@"lcwl://PublishDynamic"},
             @{@"img":@"发起群聊",@"content":@"发起群聊",@"vString":@"lcwl://GroupChatVC",@"callback":^(NSArray* selectArray){
                 UserModel* user = [LcwlChat shareInstance].user;
                 if (selectArray.count == 1) {
                     
                     id model = selectArray[0];
                     if ([model isKindOfClass:[ChatGroupModel class]]) {
                         ChatGroupModel* group = (ChatGroupModel*)model;
                         MXConversation* con = [[MXConversation alloc]init];
                         con.conversationType = eConversationTypeGroupChat;
                         con.chat_id = group.groupId;
                         [[LcwlChat shareInstance].chatManager createConversationObject:con];
                         [TalkManager pushChatViewUserId:group.groupId type:eConversationTypeGroupChat];
                     }else if([model isKindOfClass:[FriendModel class]]){
                         FriendModel* friend = (FriendModel*)model;
                         MXConversation* con = [[MXConversation alloc]init];
                         con.conversationType = eConversationTypeChat;
                         con.chat_id = friend.userid;
                         [[LcwlChat shareInstance].chatManager createConversationObject:con];
                        [TalkManager pushChatViewUserId:friend.userid type:eConversationTypeChat];
                     }
                    
                     return;
                 }
                 __block NSMutableArray* idArray = [[NSMutableArray alloc]initWithCapacity:10];
                  __block NSMutableArray* nameArray = [[NSMutableArray alloc]initWithCapacity:10];
                 [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    FriendModel* friend = (FriendModel*)obj;
                     [idArray addObject:friend.userid];
                     [nameArray addObject:friend.name];
                 }];
//                 NSData *data=[NSJSONSerialization dataWithJSONObject:idArray options:NSJSONWritingPrettyPrinted error:nil];
//
//                 NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//                 jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//                 jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                 [RoomManager createGroup:@{@"user_ids":[idArray componentsJoinedByString:@","] ?: @""} completion:^(id group, NSString *error) {
                     if (group) {
                         ChatGroupModel* model = (ChatGroupModel*)group;
                         [[LcwlChat shareInstance].chatManager insertGroup:model];
                         NSString* tip = [NSString stringWithFormat:@"你邀请%@加入了群聊",[nameArray componentsJoinedByString:@"、"]];
                         
                         [ChatSendHelper sendSgroup:tip from:model.groupId messageType:SGROUPTypeCreate];
                         FriendModel* selfModel = [user toFriendModel];
                         selfModel.groupid = model.groupId;
                         selfModel.groupNickname = selfModel.name;
                         [[LcwlChat shareInstance].chatManager insertGroupMember:selfModel];
                         [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                             FriendModel* friend = (FriendModel*)obj;
                             friend.groupid = model.groupId;
                             friend.groupNickname = friend.name;
                             [[LcwlChat shareInstance].chatManager insertGroupMember:friend];
                         }];
                         [TalkManager pushChatViewUserId:model.groupId type:eConversationTypeGroupChat];
                         
                     }else{
                         [NotifyHelper showMessageWithMakeText:error];
                     }
                     }] ;
                    
             }},
             @{@"img":@"添加好友2",@"content":@"添加朋友",@"vString":@"lcwl://AddFriendVC"},
             @{@"img":@"扫一扫",@"content":@"扫一扫",@"vString":@"lcwl://MXBarReaderVC"},
             @{@"img":@"申请兑商",@"content":@"申请兑商",@"vString":@"lcwl://ApplyHomeVC"},
             //@{@"img":@"群发助手",@"content":@"群发助手",@"vString":@"clearConversation"},
             //@{@"img":@"im_clear_qr_icon",@"content":@"清除删我的人",@"vString":@"clearUnfocus"}
             ];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = UITableViewCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    UIImageView *imgView = [cell.contentView viewWithTag:1000];
    if (!imgView) {
        imgView = [UIImageView new];
        imgView.tag = 1000;
        [cell.contentView addSubview:imgView];
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(20, 20));
            make.centerY.mas_equalTo(cell.contentView);
            make.left.mas_equalTo(cell.contentView.mas_left).offset(10);
        }];
    }
    UILabel *lbl = [cell.contentView viewWithTag:1001];
    if (!lbl) {
        lbl = [UILabel new];
        lbl.tag = 1001;
        lbl.textColor = [UIColor whiteColor];
        lbl.font = [UIFont font17];
        [cell.contentView addSubview:lbl];
        [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(imgView);
            make.left.mas_equalTo(imgView.mas_right).offset(10);
            make.right.mas_equalTo(cell.contentView.mas_right).offset(-15);
            make.height.mas_equalTo(20);
        }];
    }
    UIView *view = [cell.contentView viewWithTag:1002];
    if (!view) {
        view = [UIView new];
        view.tag = 1002;
        view.backgroundColor = [UIColor whiteColor];
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.3);
            make.left.mas_equalTo(lbl.mas_left);
            make.right.mas_equalTo(cell.contentView.mas_right);
            make.bottom.mas_equalTo(cell.contentView.mas_bottom).offset(-1);
        }];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.popoverData.count) {
        return;
    }
    NSDictionary *tmpDict = self.popoverData[indexPath.row];
    if (![tmpDict isKindOfClass:NSDictionary.class]) {
        return;
    }
    
    NSString *imgName = tmpDict[@"img"];
    UIImageView *imgView = [cell.contentView viewWithTag:1000];
    if (imgView && imgName) {
        imgView.image = [UIImage imageNamed:imgName];
    }
    
    NSString *content = tmpDict[@"content"];
    UILabel *lbl = [cell.contentView viewWithTag:1001];
    if (content && lbl) {
        lbl.text = content;
    }
    UIView *view = [cell.contentView viewWithTag:1002];
    if (!view) {
        return;
    }
    if (indexPath.row == [self popoverData].count - 1) {
        view.hidden = YES;
    } else {
        view.hidden = NO;
    }
}

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.popoverData.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSDictionary* tmpDict = self.popoverData[indexPath.row];
    NSString* vString = [tmpDict objectForKey:@"vString"];
    if ([vString isEqualToString:@"clearConversation"]) {
        [[LcwlChat shareInstance].chatManager deleteAllConversation];
        [[NSNotificationCenter defaultCenter]postNotificationName:kRefreshChatList object:nil];
        NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithCapacity:0];
        NSInteger nums = [[LcwlChat shareInstance].chatManager getAllUnReadNums];
        [userInfo setValue:[NSNumber numberWithInteger:nums] forKey:@"nums"];
        [userInfo setValue:[NSNumber numberWithInteger:3] forKey:@"tabBarIndex"];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"redBadgeCountChanged" object:nil userInfo:userInfo];
    
    }else if(([vString isEqualToString:@"clearUnfocus"])){
        [FriendsManager getRemoveFriendInfo:@{} completion:^(id object, NSString *error) {
            if (object) {
                NSDictionary* dict = (NSDictionary*)object;
                NSString* tip = [NSString stringWithFormat:@"一键清除“删我的人”有%@人，扣除美元%@个",dict[@"user_num"],dict[@"star_num"]];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:tip delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
                alertView.delegate = self;
                [alertView show];
                return;
            }
        }];
    }else if(([vString isEqualToString:@"QRCodeScanManage"])){
        [QRCodeScanManage startScan:^(id data) {
            [self resetSweepTypeWithQrcode:data];
        }];
    }else{
        [MXRouter openURL:vString parameters:tmpDict];
    }
    [self.popover dismiss:NO];
}

#pragma mark-确定扫描模块,分析扫描内容
-(void)resetSweepTypeWithQrcode:(NSString *)qrCode
{
    if(qrCode == nil || ![qrCode isKindOfClass:[NSString class]]) {
        return;
    }
    
    NSString *content = nil;
    content = qrCode;
    //  扫描关注好友
    NSInteger startIndex;
    if ((startIndex = [qrCode rangeOfString:@"user?"].location) != NSNotFound){
        content = [qrCode substringFromIndex:startIndex+@"user?".length];
        [self addFriendWithID:content];
        return;
    }
    
    //扫描群二维码
    if ((startIndex = [qrCode rangeOfString:@"group?"].location) != NSNotFound){
        content = [qrCode substringFromIndex:startIndex+@"group?".length];
        [self inviteVCwithContent:content];
        return;
    }
}

#pragma mark - 扫一扫关注
- (void)addFriendWithID:(NSString*)qrCode
{
    if (![qrCode isKindOfClass:[NSString class]]) {
        return;
    }
    NSInteger startIndex;
    NSString *content;
    if ((startIndex = [qrCode rangeOfString:@"userId="].location) != NSNotFound){
        content = [qrCode substringFromIndex:startIndex+@"userId=".length];
    }
    [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":content}];
}

#pragma mark-扫描群
-(void)inviteVCwithContent:(NSString*)content{
    NSString* groupTag = @"groupId=";
    NSString* inviteTag = @"&inviteCode=";
    NSInteger indexGroup = [content rangeOfString:groupTag].location;
    NSInteger indexInvite = [content rangeOfString:inviteTag].location;
    NSString* groupId = [content substringWithRange:NSMakeRange(indexGroup+groupTag.length,indexInvite - indexGroup-groupTag.length)];
    NSString* inviteId = [content substringWithRange:NSMakeRange(indexInvite+inviteTag.length, content.length-(indexInvite+inviteTag.length))];
    [MXRouter openURL:@"lcwl://GroupDetailVC" parameters:@{@"groupid":groupId,@"inviteid":inviteId}];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [FriendsManager removeManyFriend:@{} completion:^(id object, NSString *error) {
            if (object && [object isKindOfClass:[NSArray class]]) {
                NSArray* array = (NSArray*)object;
                [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary* dict = (NSDictionary*)obj;
                    NSString* friendId = dict[@"friend_id"];
                    FriendModel *friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:friendId];
                    friend.followState = 1;
                    [[LcwlChat shareInstance].chatManager insertFriends:@[friend]];
                    
                }];
                return;
            }
        }];
    }
}


@end
