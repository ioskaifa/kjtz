//
//  CoinModel.h
//  BOB
//
//  Created by mac on 2019/12/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoinModel : BaseObject

@property (nonatomic,strong) NSString* id;
//"balance": "0.00000000",//币种余额
@property (nonatomic,strong) NSString* balance_bb;

@property (nonatomic,strong) NSString* balance_fb;

@property (nonatomic,strong) NSString* balance_hb;
//"rate": "5",//提现手续费
@property (nonatomic,strong) NSString* rate;
//"min": "10",//单次提现下限
@property (nonatomic,strong) NSString* min;
//"max": "1000"//单次提现上限
@property (nonatomic,strong) NSString* max;

@property (nonatomic,strong) NSString* name;
//地址
@property (nonatomic,strong) NSString* def;

@property (nonatomic,strong) NSString* balance;

@property (nonatomic,strong) NSString* price;

@property (nonatomic,assign) NSInteger type;

@property (nonatomic,strong) NSString* rec_fee;

-(NSString*)warning;

-(NSString*)tip;

@end

NS_ASSUME_NONNULL_END
