//
//  ChatRoomModel.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/8/4.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatGroupModel.h"
#import "CommonDefine.h"
#import "UIImage+Addtions.h"
#import "TalkManager.h"
#import "QNManager.h"
#import "XJGroupChatPhoto.h"

@implementation ChatGroupModel
-(id)init{
    if (self = [super init]) {
        self.roleType = 1;
    }
    return self;
}


+(ChatGroupModel*)chatDictionaryToModel:(NSDictionary*)dict{
    ChatGroupModel* model = [[ChatGroupModel alloc]init];
    @onExit {
        [model checkPropertyAndSetDefaultValue];
    };
    NSString* creatorIdString = [NSString stringWithFormat:@"%@",dict[@"crowdOwner"]];
    if (![StringUtil isEmpty:creatorIdString]) {
        model.creatorId = creatorIdString;
    }
    model.groupId =  [NSString stringWithFormat:@"%@",dict[@"groupId"]];
    
    model.groupName = [NSString stringWithFormat:@"%@",dict[@"groupName"]];
    
    NSString* photos = [NSString stringWithFormat:@"%@",dict[@"groupAvatar"]];
    
    model.groupAvatar = photos;
    
    model.groupMemNum = [dict[@"groupMemNum"] integerValue];
    if (dict[@"roleType"]) {
        model.roleType =  [dict[@"roleType"] integerValue];;
    }
    NSString* groupNickName =  [NSString stringWithFormat:@"%@",dict[@"groupNickName"]];
    
    model.nickname = groupNickName;
    
    model.qrcodeUrl =  [NSString stringWithFormat:@"%@",dict[@"qrcodeUrl"]];
    
    return model;
}

+(ChatGroupModel*)dictionaryToModel:(NSDictionary*)dict{
    ChatGroupModel* model = [[ChatGroupModel alloc]init];
    @onExit {
        [model checkPropertyAndSetDefaultValue];
    };
    NSString* creatorIdString = [NSString stringWithFormat:@"%@",dict[@"crowdOwner"]];
    if (![StringUtil isEmpty:creatorIdString]) {
        model.creatorId = creatorIdString;
    }
    model.groupId =  [NSString stringWithFormat:@"%@",dict[@"group_id"]];

    model.groupName = [NSString stringWithFormat:@"%@",dict[@"group_name"]];
    model.is_set_name = [NSString stringWithFormat:@"%@",dict[@"is_set_name"]];
    
    NSString* photos = [NSString stringWithFormat:@"%@",dict[@"group_avatar"]];
    
    model.groupAvatar = photos;
    
    model.groupMemNum = [dict[@"group_mem_num"] integerValue];
    if (dict[@"group_role_type"]) {
        model.roleType =  [dict[@"group_role_type"] integerValue];;
    }
    NSString* groupNickName =  [NSString stringWithFormat:@"%@",dict[@"group_nick_name"]];
    
    model.nickname = groupNickName;
    if (dict[@"is_stick"]) {
        model.enable_top = [dict[@"is_stick"] boolValue];
    }
    if (dict[@"is_free"]) {
        model.enable_disturb = [dict[@"is_free"] boolValue];
    }
    model.qrcodeUrl =  [NSString stringWithFormat:@"%@",dict[@"qrcode_url"]];
    return model;
}

-(NSString*)groupAvatar{
    if ([StringUtil isEmpty:_groupAvatar]) {
        _groupAvatar = @"";
    }
    return _groupAvatar;
}
-(NSString*)lastUpdateTime{
    if ([StringUtil isEmpty:_lastUpdateTime]) {
        _lastUpdateTime = @"";
    }
    return _lastUpdateTime;
}

+(void)setGroupIconWithURLArray:(NSString* )avatar image:(UIImageView* )imageView{
    //imageView.image = [UIImage imageNamed:@"avatar_default"];
    if (![StringUtil isEmpty:avatar]) {
        NSString* cacheAvatar = [[NSUserDefaults standardUserDefaults]objectForKey:avatar];
        if (![StringUtil isEmpty:cacheAvatar]) {
            UIImage* tmpImage = [UIImage imageWithContentsOfFile:cacheAvatar];
            if (tmpImage) {
                imageView.image = tmpImage;
            }else{
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:avatar];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
           
        }else{
            [XJGroupChatPhoto createGroupChatPhotoWithObjs:[avatar componentsSeparatedByString:@","] complete:^(UIImage *image) {
                imageView.image = image;
            }];
//            NSArray* array = [avatar componentsSeparatedByString:@","];
//            NSMutableArray* tmpArray = [[NSMutableArray alloc]initWithCapacity:10];
//            int count = array.count<3?array.count:3;
//            for (int i=0; i<count; i++) {
//                NSString* url = array[i];
//                if ([StringUtil isEmpty:url]) {
//                }else{
//                    NSURL *iconURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?imageView2/1/w/120/h/120", [QNManager shared].qnHost,url]];
//                    [tmpArray addObject:iconURL];
//                }
//            }
//            [UIImage groupIconWithImageArray:tmpArray bgColor:[UIColor groupTableViewBackgroundColor] image:imageView key:avatar];
        }
    }
}

@end
