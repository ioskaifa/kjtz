//
//  RedPacketModel.h
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedPacketModel : NSObject

@property (nonatomic, strong) NSString* redPacketId;

@property (nonatomic, strong) NSString* redPacketMoney;

///1-普通 2-随机
@property (nonatomic, strong) NSString* redPacketType;

///1-用户 2-群
@property (nonatomic, strong) NSString* acceType;

@property (nonatomic, strong) NSString* redPacketNum;

@property (nonatomic, strong) NSString* robbedNum;

@property (nonatomic, strong) NSString* sendId;

@property (nonatomic, strong) NSString* remark;

@property (nonatomic, strong) NSString* sendTime;

@property (nonatomic, strong) NSString* sendUserName;

@property (nonatomic, strong) NSString* sendUserAvatar;

@property (nonatomic, strong) NSString* sendUserRemark;

@property (nonatomic, strong) NSString* acceId;

@property (nonatomic, strong) NSString* acceUserName;

@property (nonatomic, strong) NSString* acceUserAvatar;

@property (nonatomic, strong) NSString* acceUserRemark;

@property (nonatomic, strong) NSString* randomMoney;
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* top;



+(RedPacketModel *)dictionaryToModal:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
