//
//  RedPacketUserModel.m
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "RedPacketUserModel.h"

@implementation RedPacketUserModel
+(RedPacketUserModel *)dictionaryToModal:(NSDictionary *)dict{
    RedPacketUserModel* model = [[RedPacketUserModel alloc]init];
    model.randomMoney = [NSString stringWithFormat:@"%@",dict[@"money"]];
    model.randomSendTime = [NSString stringWithFormat:@"%@",dict[@"add_time"]];
    model.randomUserId = [NSString stringWithFormat:@"%@",dict[@"randomUserId"]];
    model.randomUserName = [NSString stringWithFormat:@"%@",dict[@"nickname"]];
    model.randomUserAvatar = [NSString stringWithFormat:@"%@",dict[@"pic"]];
    model.randomUserRemark = [NSString stringWithFormat:@"%@",dict[@"randomUserRemark"]];
    model.randomLuck = [NSString stringWithFormat:@"%@",dict[@"randomLuck"]];
    model.randomId = [NSString stringWithFormat:@"%@",@([dict[@"uid"] integerValue])];
    model.status = [dict[@"status"] integerValue];
    model.type = [NSString stringWithFormat:@"%@",dict[@"type"]];
    
    return model;
}
@end
