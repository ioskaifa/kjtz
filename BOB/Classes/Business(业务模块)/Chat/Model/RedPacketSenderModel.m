//
//  RedPacketSenderModel.m
//  Lcwl
//
//  Created by mac on 2019/1/6.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "RedPacketSenderModel.h"

@implementation RedPacketSenderModel
+(RedPacketSenderModel *)dictionaryToModal:(NSDictionary *)dict{
    RedPacketSenderModel* model = [[RedPacketSenderModel alloc]init];
    model.acceType = [NSString stringWithFormat:@"%@",dict[@"acceType"]];
    model.randomMoney = [NSString stringWithFormat:@"%@",dict[@"money"]];
    model.redPacketId = [NSString stringWithFormat:@"%@",dict[@"id"]];
    model.redPacketType = [NSString stringWithFormat:@"%@",dict[@"flg"]];
    model.remark = [NSString stringWithFormat:@"%@",dict[@"remark"]];
    model.sendId = [NSString stringWithFormat:@"%@",dict[@"sendId"]];
    model.sendTime = [NSString stringWithFormat:@"%@",dict[@"add_time"]];
    model.sendUserAvatar = [NSString stringWithFormat:@"%@",dict[@"sendUserAvatar"]];
    model.sendUserName = [NSString stringWithFormat:@"%@",dict[@"nickname"]];
    model.redPacketMoney = [NSString stringWithFormat:@"%@",dict[@"total_money"]];
    model.redPacketNum = [NSString stringWithFormat:@"%@",dict[@"num"]];
    model.robbedNum = [NSString stringWithFormat:@"%@",dict[@"robbedNum"]];
    model.type = [NSString stringWithFormat:@"%@",dict[@"type"]];
    
    return model;
}
@end
