//
//  RedPacketUserModel.h
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedPacketUserModel : NSObject

@property (nonatomic, strong) NSString* randomMoney;

@property (nonatomic, strong) NSString* randomSendTime;

@property (nonatomic, strong) NSString* randomUserId;

@property (nonatomic, strong) NSString* randomUserName;

@property (nonatomic, strong) NSString* randomUserAvatar;

@property (nonatomic, strong) NSString* randomUserRemark;

@property (nonatomic, strong) NSString* randomLuck;

@property (nonatomic, strong) NSString* randomId;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSString* type;


+(RedPacketUserModel *)dictionaryToModal:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
