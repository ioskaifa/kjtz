//
//  ManageAuthUserListObj.m
//  BOB
//
//  Created by mac on 2020/8/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ManageAuthUserListObj.h"

@implementation ManageAuthUserListObj

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"ID":@"id"};
}

@end
