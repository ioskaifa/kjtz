//
//  FriendViewModel.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/22.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "FriendViewModel.h"
#import "FriendModel.h"
#import "ContactHelper.h"
#import "LcwlChat.h"
@implementation FriendViewModel
+(NSArray* )headerData{
    return @[
            @{@"name":@"群聊",@"logo":@"icon_groupChat",@"vcString":@"RecentlyGroupListVC"},
            @{@"name":@"新朋友",@"logo":@"icon_new_friends",@"vcString":@"NewFriendVC"},
            @{@"name":@"添加朋友",@"logo":@"icon_new_friends",@"vcString":@"AddFriendVC"}
            ];
}

+(NSMutableArray* )getFriendData{
    NSMutableArray *dataArray = [[NSMutableArray alloc]initWithCapacity:10];
    NSMutableArray *array = [[LcwlChat shareInstance].chatManager friends];
    for (int i = 0;i<array.count; i++) {
        FriendModel* model = array[i];
        if (model.followState == MoRelationshipTypeMyFriend) {
            if (![StringUtil isEmpty:model.name]) {
                [dataArray addObject:model];
            }
        }
    }
   return [ContactHelper indexFriendVCModel:dataArray];
}

+(NSMutableArray* )getFriendDatabyFilter:(NSString *)userId {
    NSMutableArray *dataArray = [[NSMutableArray alloc]initWithCapacity:10];
    NSMutableArray *array = [[LcwlChat shareInstance].chatManager friends];;
    for (int i = 0;i<array.count; i++) {
        FriendModel* model = array[i];
        if (model.followState == MoRelationshipTypeMyFriend) {
            if (![StringUtil isEmpty:model.name] && ![userId isEqualToString:model.userid]) {
                [dataArray addObject:model];
            }
        }
    }
    return [ContactHelper indexFriendVCModel:dataArray];
}

@end
