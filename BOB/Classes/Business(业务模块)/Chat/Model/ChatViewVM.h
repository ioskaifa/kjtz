//
//  ChatViewVM.h
//  MoPal_Developer
//
//  Created by 王 刚 on 16/4/28.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXConversation.h"
@class ChatViewVC;
@interface ChatViewVM : NSObject
//当前聊天的conversation
@property (nonatomic, strong) MXConversation* conversation;
//是否关注过对方
@property (nonatomic) BOOL isFocus;
//聊天数据
@property (nonatomic, strong) NSMutableArray* recordsArray;

@property (nonatomic, weak) ChatViewVC *vc;

@property (nonatomic, weak) UIButton* focusBtn;

@property (nonatomic, copy) NSString* topMsgTime;
//顶端的titleview
@property (nonatomic, strong) UIView* titleView;
//右边的button
@property (strong, nonatomic) UIButton* settingBtn;
//时间戳
@property (readonly, nonatomic) NSDate *chatTagDate;

-(void)resetRefreshChatMessage;
//请求用户关系
//-(MXRequest*)requireRelation:(NSString*)chatId;
//关注新朋友
-(void)addFriend:(id)sender;
//从数据库中查询聊天消息 之前的信息
-(NSMutableArray*)selectBeforeChatRecords;


-(void)resetChatMessage;
//数据累加
-(void)getNextChatMessages;
-(NSMutableArray*)addTimeLabel:(NSArray*)array;
//设置顶部view
-(void)setTitle:(NSString* )title openSound:(BOOL) bol;
//发送转发信息
-(void)sendForwardMessage:(MessageModel* )message;
@end
