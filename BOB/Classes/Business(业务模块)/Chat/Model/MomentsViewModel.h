//
//  MomentsViewModel.h
//  Lcwl
//
//  Created by 王刚  on 2018/11/22.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DXPopover.h"
NS_ASSUME_NONNULL_BEGIN

@interface MomentsViewModel : NSObject
@property (strong, nonatomic) DXPopover            *popover;
@property (nonatomic, strong) UITableView          *popoverTableView;
@property (nonatomic, strong) UIViewController     *vc;

- (void)resetPopver ;

- (instancetype)initVC:(UIViewController *)vc;

@end

NS_ASSUME_NONNULL_END
