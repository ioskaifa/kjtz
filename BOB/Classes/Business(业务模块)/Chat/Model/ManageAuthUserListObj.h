//
//  ManageAuthUserListObj.h
//  BOB
//
//  Created by mac on 2020/8/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ManageAuthUserListObj : BaseObject

@property (nonatomic , copy) NSString              * area;
@property (nonatomic , copy) NSString              * user_tel;
@property (nonatomic , copy) NSString              * sex;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , copy) NSString              * sys_user_account;
@property (nonatomic , copy) NSString              * register_type;
@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , copy) NSString              * nick_name;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * head_photo;

@end

NS_ASSUME_NONNULL_END
