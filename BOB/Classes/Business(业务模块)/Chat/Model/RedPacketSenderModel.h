//
//  RedPacketSenderModel.h
//  Lcwl
//
//  Created by mac on 2019/1/6.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedPacketSenderModel : NSObject

@property (nonatomic, strong) NSString* acceType;

@property (nonatomic, strong) NSString* randomLuck;

@property (nonatomic, strong) NSString* randomMoney;

@property (nonatomic, strong) NSString* redPacketId;

@property (nonatomic, strong) NSString* redPacketType;

@property (nonatomic, strong) NSString* remark;

@property (nonatomic, strong) NSString* sendId;

@property (nonatomic, strong) NSString* sendTime;

@property (nonatomic, strong) NSString* sendUserAvatar;

@property (nonatomic, strong) NSString* sendUserName;

@property (nonatomic, strong) NSString* redPacketMoney;

@property (nonatomic, strong) NSString* redPacketNum;

@property (nonatomic, strong) NSString* robbedNum;

@property (nonatomic, strong) NSString* type;

+(RedPacketSenderModel *)dictionaryToModal:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
