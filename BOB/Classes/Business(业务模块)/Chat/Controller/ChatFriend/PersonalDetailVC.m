//
//  PersonalDetailVC.m
//  BOB
//
//  Created by mac on 2019/7/11.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "PersonalDetailVC.h"
#import "BaseSTDTableViewCell.h"
#import "NSMutableAttributedString+Attributes.h"
#import "TalkManager.h"
#import "LcwlChat.h"
#import "FriendsManager.h"
#import "ChatSendHelper.h"
#import "MXChatDBUtil.h"

@interface PersonalDetailVC ()
@property (nonatomic, strong) UIButton *leftBnt;
@property (nonatomic, strong) UIButton *rightBnt;
///授权管理
@property (nonatomic, strong) MyBaseLayout *authLayout;
@property(nonatomic, strong) FriendModel *friendModel;
@end

@implementation PersonalDetailVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNavBarTitle:Lang(@"好友资料")];
    //[self setNavBarRightBtnWithTitle:Lang(@"更多") andImageName:nil];
    if (![UDetail.user.user_id isEqualToString:self.other_id]) {
        //如果是客服且不是自己好友  则不显示管理按钮, 预防把客服拉黑
        NSString *userId = self.other_id;
        BOOL isService = NO;
        BOOL isFriend = NO;
        NSArray *serviceArr = [MXCache valueForKey:kMXMemberServiceList];
        if ([serviceArr isKindOfClass:NSArray.class]) {
            if ([serviceArr containsObject:userId]) {
                isService = YES;
            }
        }
        FriendModel *tempObj = [[LcwlChat shareInstance].chatManager loadFriendByChatId:userId];
        if (tempObj) {
            if (tempObj.followState != MoRelationshipTypeStranger) {
                isFriend = YES;
            }
        }
        if (isService && !isFriend) {
            
        } else {
            [self setNavBarRightBtnWithTitle:nil andImageName:@"更多"];
        }
    }
    
    [self getData];
}

- (void)configFooter {
    if([self.other_id isEqualToString:UDetail.user.chatUser_id]) {
        return;
    }
    
    MyLinearLayout *footer = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    footer.backgroundColor = [UIColor moBackground];
    footer.myWidth = SCREEN_WIDTH;
    footer.myHeight = MyLayoutSize.wrap;
    footer.myTop = 0;
    footer.myLeft = 0;
    
    [footer addSubview:self.authLayout];
    [footer addSubview:self.leftBnt];
    if(self.creatorRole == 0) {
        [footer addSubview:self.rightBnt];
    }
    
    if (self.friendModel.followState == MoRelationshipTypeStranger) {
        self.rightBnt.visibility = MyVisibility_Visible;
    } else {
        self.rightBnt.visibility = MyVisibility_Gone;
    }
     
    if (self.friendModel.followState != MoRelationshipTypeStranger) {
        self.leftBnt.visibility = MyVisibility_Visible;
    } else {
        self.leftBnt.visibility = MyVisibility_Gone;
    }
    
    [footer layoutSubviews];
    footer.mySize = footer.size;
    self.tableView.tableFooterView = footer;
}

- (void)navBarRightBtnAction:(id)sender {
    [MXRouter openURL:@"lcwl://InfoSettingVC" parameters:@{@"model":self.friendModel,@"group_id":self.group_id ?: @"", @"block":^(id data){
        NSDictionary* dict = (NSDictionary*)data;
        NSString* type = dict[@"type"];
        if ([type isEqualToString:dict[@"deleteFriend"]]) {
            
        }else if([type isEqualToString:dict[@"type"]]){
            NSString* userId = self.friendModel.userid;
            FriendModel* model = [[LcwlChat shareInstance].chatManager loadFriendByChatId:userId];
            if (model) {
                model.remark = dict[@"value"];
                [[LcwlChat shareInstance].chatManager insertFriends:@[model]];
            }
        }
    }}];
}

- (void)leftBntClick:(id)sender {
    UIViewController *superVC = (UIViewController *)self.nextResponder.nextResponder.nextResponder;
    if(superVC && [superVC isKindOfClass:[UIViewController class]]) {
        if(superVC.navigationController != nil) {
            if([[[superVC.navigationController viewControllers] firstObject] isKindOfClass:[MomentsVC class]]) {
                [TalkManager pushChatViewUserId:self.friendModel.userid type:eConversationTypeChat];
                return;
            }
        }
    }
    [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":self.friendModel.userid,@"type":@(eConversationTypeChat)}];
}

- (void)rightBntClick:(id)sender {
    [self addFriend];
}

-(void)addFriend {
    [FriendsManager addFriend:@{@"add_acce_id":self.friendModel.userid,@"add_type":@"1"} completion:^(id object, NSString *error) {
        if (object) {
            NSDictionary* dict = (NSDictionary*)object;
            NSInteger status = [dict[@"status"]integerValue];
            self.friendModel.followState = status;
            if (status == MoRelationshipTypeStranger) {
                NSInteger isverify = [dict[@"isverify"]integerValue];
                if (isverify == 1) {
                    [MXRouter openURL:@"lcwl://VerifyFriendVC" parameters:@{@"model":self.friendModel}];
                }
            }else if(status == MoRelationshipTypeMyFriend){
                [[LcwlChat shareInstance].chatManager insertFriends:@[self.friendModel]];
                NSString* tip = [NSString stringWithFormat:@"您已添加了%@，可以开始聊天了",self.friendModel.name];
                [ChatSendHelper sendChat:tip from:self.friendModel.userid messageType:kMxmessageTypeCustom type:eConversationTypeChat];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PersonalDetailHeaderUpdate" object:nil];
        }
    }];
}

- (void)getData {
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/user/info/getUserInfo"];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    [param setValue:self.other_id forKey:@"other_id"];
    
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data) {
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                NSDictionary *userInfo = [tempDic valueForKeyPath:@"data.userInfo"];
                self.friendModel = [FriendModel initFriendModel2:userInfo];
                [self configFooter];
                [self setupTableViewDataSource];
            }
            Block_Exec(self.block,self.friendModel);
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

#pragma mark - setup

- (void)configTableView:(UITableView *)tableView
{
    tableView.backgroundColor = [UIColor moBackground];
    //[tableView std_registerCellClass:[BaseSTDTableViewCell class]];
    [tableView std_registerCellClass:[BaseSTDTableViewCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
    
    sectionData = [[STDTableViewSection alloc] initWithCellClass:[BaseSTDTableViewCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
    UIColor *grayColor = [UIColor colorWithHexString:@"#969999"];
    UIFont *grayFont = [UIFont systemFontOfSize:13];
    NSString *name = self.friendModel.smartName ?: @"";// ?: self.friendModel.name ?: @"";
    NSString *nickName = nil;//self.friendModel.remark ? nil : [NSString stringWithFormat:@"昵   称：%@",self.friendModel.name];
    //NSString *userTel = self.friendModel.phone ? [NSString stringWithFormat:@"手机号：%@",self.friendModel.phone] : nil;
    NSString *email = [NSString stringWithFormat:@"ID：%@",self.friendModel.chat_no ?: @""];
    NSMutableArray *titlesArr = [NSMutableArray arrayWithCapacity:1];
    [titlesArr safeAddObj:name];
    //[titlesArr safeAddObj:nickName];
    //[titlesArr safeAddObj:userTel];
    [titlesArr safeAddObj:email];
    NSMutableAttributedString *attStr = [NSMutableAttributedString initWithTitles:titlesArr colors:@[[UIColor blackColor],grayColor,grayColor,grayColor] fonts:@[[UIFont systemFontOfSize:21],grayFont,grayFont,grayFont] placeHolder:@"\n"];
    [attStr setLineSpacing:5 substring:attStr.string alignment:NSTextAlignmentLeft];
    
    
    BaseSTDCellModel *headerModel = [BaseSTDCellModel model:self.friendModel.avatar text:attStr rightIcon:nil tip:nil jumpVC:nil];
    headerModel.leftIconRect = CGRectMake(15, 20, 50, 50);
    headerModel.cornerRadius = 25;
    headerModel.textNumberOfLines = 0;
    headerModel.textRect = CGRectMake(85, 20, -1, -1);
    headerModel.leftIconBGColor = [UIColor headBGColor];
    NSArray *section0ItemsArr = @[[BaseSTDTableViewCell cellItemWithData:headerModel cellHeight:90]];
    
    NSMutableAttributedString *remarkAttStr = [NSMutableAttributedString initWithTitles:@[Lang(@"设置备注")] colors:@[[UIColor blackColor]] fonts:@[[UIFont systemFontOfSize:15 weight:UIFontWeightMedium]] placeHolder:@""];
    NSMutableAttributedString *labelAttStr = [NSMutableAttributedString initWithTitles:@[@"朋友圈"] colors:@[[UIColor blackColor]] fonts:@[[UIFont systemFontOfSize:15 weight:UIFontWeightMedium]] placeHolder:@""];
    NSMutableAttributedString *comeFromAttStr = [NSMutableAttributedString initWithTitles:@[Lang(@"更多信息")] colors:@[[UIColor blackColor]] fonts:@[[UIFont systemFontOfSize:15 weight:UIFontWeightMedium]] placeHolder:@""];

    @weakify(self)
    FinishedBlock editRemarkBlock = ^(id data) {
        @strongify(self)
        NSString *fullUrl = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/frined/setFriendRemark"];
        [self request:fullUrl param:@{@"remark":data,@"friend_id":self.friendModel.userid} completion:^(BOOL success, id object, NSString *error) {
            if(success) {
                self.friendModel.remark = data;
                [self setupTableViewDataSource];
                [[LcwlChat shareInstance].chatManager updateFriendInfo:self.friendModel];
                [[LcwlChat shareInstance].chatManager updateRemarkMap:self.friendModel.userid remark:data needCheckExist:NO];
                if(self.group_id) { //修改完备注刷新聊天背景
                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"ReloadChatList_%@",self.group_id] object:nil];
                }
            }
        }];
    };
    
    BOOL isNoFriend = self.friendModel.followState == MoRelationshipTypeStranger;
//    BaseSTDCellModel *model = [BaseSTDCellModel model:remarkAttStr rightIcon:(isNoFriend ? nil : @"Arrow") jumpVC:(isNoFriend ? nil : @"TextChangeVC") jumpParam:@{@"titleStr":Lang(@"备注名"),@"text":name,@"placeholder":Lang(@"请输备注名"),@"block":editRemarkBlock}];
////    FriendModel *m = [[MXChatDBUtil sharedDataBase] selectFriendByChatId:userId];
//    model.detailText = name;//m.remark ?: @"";
    NSArray *section1ItemsArr = @[/*[BaseSTDTableViewCell cellItemWithData:model cellHeight:54],*/
                                  
                                  [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:labelAttStr rightIcon:@"Arrow" jumpVC:@"lcwl://SocialContactMainVC" jumpParam:@{@"otherId":self.other_id ?: @"",@"type":@(2),@"circle_back_img":self.friendModel.circle_back_img ?: @""}] cellHeight:54],
                                  
                                  [BaseSTDTableViewCell cellItemWithData:[BaseSTDCellModel model:comeFromAttStr rightIcon:@"Arrow" jumpVC:@"lcwl://MoreVC" jumpParam:self.friendModel ? @{@"otherModel":self.friendModel ?: @""} : nil] cellHeight:54]];
    
    
    [self.tableView std_removeAllItemAtSection:0];
    [self.tableView std_addItems:section0ItemsArr atSection:0];
    
//    if(![self.other_id isEqualToString:UDetail.user.chatUser_id]) {
//        [self.tableView std_removeAllItemAtSection:1];
//        [self.tableView std_addItems:section1ItemsArr atSection:1];
//    }
    
    //[self.tableView std_insertRows:itemList.count atEmptySection:0 withRowAnimation:UITableViewRowAnimationNone];
    
    //[self.tableView std_insertSection:2 withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:NO];
//
//}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

- (UIButton *)leftBnt {
    if(!_leftBnt) {
        _leftBnt = [[UIButton alloc] init];
        _leftBnt.backgroundColor = [UIColor whiteColor];
        _leftBnt.titleLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
        [_leftBnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_leftBnt setTitle:@"发送消息" forState:UIControlStateNormal];
        [_leftBnt addTarget:self action:@selector(leftBntClick:) forControlEvents:UIControlEventTouchUpInside];
        _leftBnt.myHeight = 54;
        _leftBnt.myLeft = 0;
        _leftBnt.myRight = 0;
        _leftBnt.myBottom = 10;
    }
    return _leftBnt;
}

- (UIButton *)rightBnt {
    if(!_rightBnt) {
        _rightBnt = [[UIButton alloc] init];
        _rightBnt.backgroundColor = [UIColor whiteColor];
        _rightBnt.titleLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
        [_rightBnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_rightBnt setTitle:@"添加好友" forState:UIControlStateNormal];
        [_rightBnt addTarget:self action:@selector(rightBntClick:) forControlEvents:UIControlEventTouchUpInside];
        _rightBnt.myHeight = 54;
        _rightBnt.myLeft = 0;
        _rightBnt.myRight = 0;
        _rightBnt.myBottom = 10;
    }
    return _rightBnt;
}

@end
