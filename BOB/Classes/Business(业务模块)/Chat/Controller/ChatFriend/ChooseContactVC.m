//
//  MobileContactVC.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "ChooseContactVC.h"

#import "FriendModel.h"
#import "ContactHelper.h"
#import "FriendListView.h"
#import "LSearchBar.h"
#import "LcwlChat.h"
#import "SearchAddressVC.h"
#import "AddFriendListCell.h"
#import "SearchGroupVC.h"
#import "DynamicPhotoToolView.h"
#import "GroupListVC.h"
#import "ChatGroupModel.h"
#import "MXConversation.h"
#import "AddFriendTopView.h"

static const CGFloat headerHeight = 22;
static const CGFloat rowHeight = 60;
static int padding = 15;
@interface ChooseContactVC ()<UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating,UISearchBarDelegate>
@property (nonatomic, strong) MXBackButton                 *rightButton;
/** 列表 */
@property (nonatomic, strong) UITableView *tableView;

@property(nonatomic, strong) UINavigationController *searchNavigationController;

/** 搜索框 */
@property (nonatomic, strong) UISearchController *searchController;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchGrayImage;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchWhiteImage;

@property (nonatomic) DynamicPhotoToolView* toolView;

@property (nonatomic, strong)NSMutableArray *selectArray;

@property(nonatomic, strong) UIButton *lastSelectedBnt;

@property (nonatomic, strong) AddFriendTopView *searchView;

@end

@implementation ChooseContactVC
-(void)viewDidLoad{
    [super viewDidLoad];
    [self setNavBarTitle:@"选择联系人"];
    
    self.selectArray = [[NSMutableArray alloc]initWithCapacity:10];
    [self initUI];
    [self initData];
    AdjustTableBehavior(self.tableView)
}

-(void)initUI{
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.definesPresentationContext = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.toolView];
    @weakify(self)
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.toolView.mas_top);
    }];
    [self.toolView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.view);
        make.height.equalTo(@([DynamicPhotoToolView getHeight]));
        make.bottom.equalTo(self.view.mas_bottom);
    }];
}

-(DynamicPhotoToolView *)toolView{
    if (!_toolView) {
        _toolView = [DynamicPhotoToolView getInstance];
        _toolView.type = ADD_FRIEND;
        [_toolView.sureBtn addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _toolView;
}
-(void)confirm:(id)sender{
    if (self.block) {
        self.block(self.selectArray);
    }
  
}
-(void)initData{
    [self getContacts];
    [self.tableView reloadData];
}

- (void)getContacts {
    if (_dataSource.count != 0) {
        return;
    }
    if ([[ContactHelper shareInstance] authorizationed]) {
        NSMutableArray* array = [[LcwlChat shareInstance].chatManager friends];
        _dataSource = [ContactHelper indexFriendVCModel:array];
    }
}

//消息列表
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.searchView;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.sectionIndexColor = [UIColor lightGrayColor];
        AdjustTableBehavior(_tableView);
    }
    return _tableView;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //        NSArray* keys = [NSMutableArray arrayWithArray:[datasource safeObjectAtIndex:indexPath.section]];
    
    static NSString *myCell = @"cell_identifier";
    AddFriendListCell *cell = [tableView dequeueReusableCellWithIdentifier:myCell];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AddFriendListCell" owner:self options:nil]lastObject];
    }
    NSArray* tmpArray = [_dataSource objectAtIndex:indexPath.section];
    FriendModel* model = [tmpArray objectAtIndex:indexPath.row];
    BOOL bol = [self isContainsObject:self.unChangeArray model:model.userid];
    if (bol) {
        cell.userInteractionEnabled = NO;
        [cell reloadData:model status:0];
    }else{
         cell.userInteractionEnabled = YES;
        [cell reloadData:model status:[self.selectArray containsObject:model]?2:1];
    }
    return cell;
}

-(BOOL)isContainsObject:(NSMutableArray*) objAray model:(NSString * )userId{
    for (int i=0; i<objAray.count; i++) {
        
        FriendModel* model = (FriendModel*)objAray[i];
        if ([model isKindOfClass:[FriendModel class]]) {
            if ([model.userid isEqualToString:userId]) {
                return YES;
            }
        }
        
    }
    return NO;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowHeight;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray* tmpArray = [self.dataSource objectAtIndex:section];
    if (tmpArray.count) {
        return tmpArray.count;
    }else
        return 0;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* array = [self.dataSource safeObjectAtIndex:indexPath.section];
    FriendModel* model = array[indexPath.row];
    BOOL bol = [self.unChangeArray containsObject:model];
    if (bol) {
        return;
    }
    if ([self.selectArray containsObject:model]) {
        [self.selectArray removeObject:model];
    }else{
        [self.selectArray addObject:model];
    }
    
    SelectStatus isSelected = SelectStatusUnSelect;
    if ([self.selectArray containsObject:model]) {
        isSelected = SelectStatusSelect;
    }
    
    AddFriendListCell* cell = (AddFriendListCell*)[tableView cellForRowAtIndexPath:indexPath];
    if(self.isSingleSelect) {
        self.lastSelectedBnt.selected = NO;
        self.lastSelectedBnt = cell.isSelectBtn;
        
        [self.selectArray removeAllObjects];
        [self.selectArray addObject:model];
    }
    
    [self.toolView reloadData:self.selectArray];
    
    [cell reloadData:model status:isSelected];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Table view delegate
-(NSString*)sectionTitle:(NSInteger) segIndex section:(NSInteger)section{
    if ([[_dataSource objectAtIndex:section] count] != 0){
        NSString* str = [NSString stringWithFormat:@"%@", [[ALPHA2 substringFromIndex:section] substringToIndex:1]];
        return str;
    }
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString* sectionTitle = [self sectionTitle:0 section:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(padding, (headerHeight-20)/2, 300, 20);
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor blackColor];
    label.font=[UIFont font14];
    label.text=sectionTitle;
    
    // Create header view and add label as a subview
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
    [sectionView setBackgroundColor:[UIColor moBackground]];
    [sectionView addSubview:label];
    
    return sectionView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSString* sectionTitle = [self sectionTitle:0 section:section];
    if (sectionTitle!=nil) {
        return headerHeight;
    }
    return 0.01;
}





- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ALPHA2.length;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray *indices = [NSMutableArray arrayWithObject:UITableViewIndexSearch];
    for (int i = 0; i < ALPHA2.length; i++){
        if ([[_dataSource objectAtIndex:i] count])
            [indices safeAddObj:[[ALPHA2 substringFromIndex:i] substringToIndex:1]];
    }
    return indices;
}

#pragma mark - UISearchBarDelegate
//- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
//    SearchAddressVC* searchVC = [[SearchAddressVC alloc]init];
//    searchVC.callback = ^(){
//        [self.searchNavigationController.view removeFromSuperview];
//        [self.searchNavigationController removeFromParentViewController];
//        [self.navigationController setNavigationBarHidden:NO animated:NO];
//        [[MoApp router] configureCurrentVC:self];
//    };
//
//    [self.searchBar resignFirstResponder];
//    _searchNavigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
//    [[UIApplication sharedApplication].keyWindow addSubview:_searchNavigationController.view];
//
//}

- (void)willPresentSearchController:(UISearchController *)searchController {
    
    //self.tableView.contentInset = UIEdgeInsetsMake(100, 0, 0, 0);
    //[self.view layoutIfNeeded];
    //[self.tableView setContentOffset:CGPointMake(0, 0)];
    //self.tableView.frame = CGRectMake(0, 100, SCREEN_WIDTH, self.view.height);
    //    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
    //        make.left.right.bottom.equalTo(self.view);
    //        make.top.equalTo(self.view).offset(100);
    //    }];
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    //    [UIView animateWithDuration:0.1 animations:^{
    //        self.tableView.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
    //        [self.tableView setContentOffset:CGPointMake(0, -60)];
    //    }];
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
}

- (UISearchController *)searchController {
    if (!_searchController) {
        SearchGroupVC *resultVC = [[SearchGroupVC alloc] init];
        resultVC.unChangeArray = self.unChangeArray;
        resultVC.searchCallback = ^(id  _Nonnull sender, UIViewController * _Nonnull fromVC) {
            BOOL exist = [self.selectArray containsObject:sender];
            if (exist) {
                [self.selectArray removeObject:sender];
            }else{
                [self.selectArray addObject:sender];
            }
            [self.tableView reloadData];
            [self.toolView reloadData:self.selectArray];
        };
        resultVC.selectData = self.selectArray;
        _searchController = [[UISearchController alloc]initWithSearchResultsController:resultVC];
        _searchController.searchBar.delegate = self;
        _searchController.searchResultsUpdater = resultVC;
        //_searchController.delegate = self;
        _searchController.view.backgroundColor = [UIColor whiteColor];
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = YES;
        //[_searchController.searchBar sizeToFit];
        //_searchController.searchBar.tintColor = [UIColor blackColor];
        _searchController.searchBar.placeholder =  @"搜索";
        [_searchController.searchBar sizeToFit];
        UIOffset offset = {5.0,0};
        _searchController.searchBar.searchTextPositionAdjustment = offset;
        _searchController.searchBar.backgroundImage = self.searchGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:self.searchWhiteImage forState:UIControlStateNormal];
        _searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
        _searchController.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;//关闭提示
        _searchController.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;//关闭自动首字母大写
    }
    return _searchController;
}

- (AddFriendTopView *)searchView {
    if (!_searchView) {
        _searchView = [AddFriendTopView new];
        _searchView.size = CGSizeMake(SCREEN_WIDTH, [AddFriendTopView viewHeight]);
        _searchView.placeholderLbl.text = @"搜索";
        @weakify(self);
        [_searchView addAction:^(UIView *view) {
            @strongify(self);
            SearchGroupVC *resultVC = [[SearchGroupVC alloc] init];
            resultVC.unChangeArray = self.unChangeArray;
            resultVC.searchCallback = ^(id  _Nonnull sender, UIViewController * _Nonnull fromVC) {
                @strongify(self);
               BOOL exist = [self.selectArray containsObject:sender];
               if (exist) {
                   [self.selectArray removeObject:sender];
               }else{
                   [self.selectArray addObject:sender];
               }
               [self.tableView reloadData];
               [self.toolView reloadData:self.selectArray];
            };
            [self.navigationController pushViewController:resultVC animated:YES];
        }];
       
    }
    return _searchView;
}

@end
