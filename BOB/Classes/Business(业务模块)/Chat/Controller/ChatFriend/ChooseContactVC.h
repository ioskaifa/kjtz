//
//  ChooseContactVC.h
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
// 两个日期的回调
typedef void(^SelectedCompelte)(NSMutableArray * array);
@interface ChooseContactVC : BaseViewController

@property (nonatomic, strong) NSMutableArray* dataSource;
@property (nonatomic, strong) NSMutableArray* unChangeArray;
@property (nonatomic, strong) NSString* roomId;
@property (nonatomic, assign) BOOL isSingleSelect;
@property (nonatomic, strong) SelectedCompelte block;


@end

NS_ASSUME_NONNULL_END
