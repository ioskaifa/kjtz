//
//  SearchAddressVC.h
//  Lcwl
//
//  Created by mac on 2018/11/24.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^SearchResultCallback)(id sender, UIViewController* fromVC);
typedef void(^CancelBtnClick)() ;

@interface SearchAddressVC : BaseViewController<UISearchResultsUpdating>
@property (nonatomic, copy) CancelBtnClick  callback;
@property (nonatomic, assign) BOOL hidenChatBnt;
@property (nonatomic, copy) NSString* searchTitle;
@property (nonatomic) int type;

@end

NS_ASSUME_NONNULL_END
