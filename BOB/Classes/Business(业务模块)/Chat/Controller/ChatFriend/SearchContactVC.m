//
//  SearchLocalFriendVC.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/25.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "SearchContactVC.h"
#import "ChatTitleView.h"
#import "MXSeparatorLine.h"
#import "NSObject+Additions.h"
#import "LSearchBar.h"
#import "UINavigationBar+Alpha.h"
#import "ContactHelper.h"
#import "FriendModel.h"
#import "FriendListView.h"
#import "LcwlChat.h"
//#import "CareFansCell.h"
#import <MessageUI/MessageUI.h>

static int rowHeight = 60;
static const CGFloat headerHeight = 30;
@interface SearchContactVC ()<UITableViewDelegate,UITableViewDataSource>

//// tableview
@property (nonatomic,strong) UITableView *tableView;

@property (strong, nonatomic) NSMutableArray* searchResult;

@property(nonatomic, strong) UIView *searchView;

@property (strong, nonatomic) UIView *historyView;

@property(nonatomic, strong) LSearchBar *searchBar;

@property(nonatomic, strong) UIButton *cancelButton;

@property(nonatomic, strong) NSMutableArray *friendData;

@property(nonatomic, strong)  UISearchController* vc;

@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation SearchContactVC



- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    self.vc = searchController;
    NSString *searchText = searchController.searchBar.text;
    NSLog(@"正在编辑过程中的改变");
    [_searchResult removeAllObjects];
    [self filterFriendByKeyword:searchText];
    [self.tableView reloadData];
    AdjustTableBehavior(self.tableView)
}


//@synthesize noImageView,noLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarTitle:@"通讯录朋友"];
    _searchResult = [[NSMutableArray alloc]initWithCapacity:10];
    _friendData = [ContactHelper shareInstance].contactArray;
    self.view.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.searchTF];
    [layout addSubview:self.tableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchTF becomeFirstResponder];
}

//消息列表
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.backgroundView = nil;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
        AdjustTableBehavior(_tableView);
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (void)didCustomsearchBartartSearch:(NSString *)text {
    _historyView.hidden = YES;
    if (text.length > 0) {
        [self searchByKeyword:text];
    }

}

- (void)showNoDataTips {
    //    noImageView.hidden=NO;
    //    noLabel.hidden=NO;

    //    [MXBlankPageView addBlankPageView:MXBlankPageMoChatFriendSearchNoResultType withSuperView:self.view];
}

- (void)hideNoDataTips {
    //    noImageView.hidden=YES;
    //    noLabel.hidden=YES;

    //    [MXBlankPageView removeFromSuperView:self.view];
}

// 自动提示搜索
- (void)searchByKeyword:(NSString*)keyword {

    //    _historyView.hidden = NO;
    //    _historyKey = [CreatePlist readPlist];
    //    if ([StringUtil isEmpty:_historyKey]) {
    //        _historyView.hidden = YES;
    //    }
    //    _historyLabel.text = [NSString stringWithFormat:@"%@:%@",MXLang(@"Talk_friend_search_tips_47", @"上次搜索"),_historyKey];
    //    NSArray* tmpArray = [self filterDataByType:self.type keyword:keyword];//[[MXChatDBUtil sharedDataBase]selectFriendByKeyword:keyword];
    //    if (tmpArray.count == 0) {
    //        _tableView.hidden = YES ;
    //        [self showNoDataTips ];
    //    }else{
    //        _tableView.hidden = NO;
    //        [self hideNoDataTips ];
    //    }
    //
    //    self.searchResult = [tmpArray mutableCopy] ;
    //    [_tableView reloadData];





}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        cell.selectionStyle =  UITableViewCellSelectionStyleNone;
        FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
        [view style:RowStyleSubtitle];
        
        [view setShowArrow:NO];
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        
    }
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
    FriendModel* model = [_searchResult objectAtIndex:indexPath.row];
    if ([StringUtil isEmpty:model.userid]) {
        [tmpView style:RowStyleButton];
        tmpView.rightBtnClickCallback = ^(UIView *view){
            UIView* tmp = view.superview.superview;
            NSIndexPath *indexPath = [self.tableView indexPathForCell:tmp];
            FriendModel* model = [_searchResult objectAtIndex:indexPath.row];
            [self sendSysMessage:model];
            
        };
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/60/h/60",model.avatar];
        [tmpView reloadLogo:avatar name:model.addressListName desc:[NSString stringWithFormat:@"%@",model.phone]];
    }else if(model.followState == 0){
        [tmpView style:RowStyleButton];
        tmpView.rightBtnClickCallback = ^(UIView *view){
            
            
        };
        [tmpView reloadLogo:@"avatar_default" name:model.addressListName desc:[NSString stringWithFormat:@"%@",model.phone] btntitle:@"添加"];
    }else{
        [tmpView style:RowStyleSubtitle];
        
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/60/h/60",model.avatar];
        [tmpView reloadLogo:avatar name:model.name desc:[NSString stringWithFormat:@"%@(%@)",model.addressListName,model.phone]];
        
    }
    return cell;
}

-(void)sendSysMessage:(FriendModel* )friend{
    NSString* tip = @"分享给你9聊APP，下载地址：https://dev1.mifengff.com/ygjqg4。";
    [[ContactHelper shareInstance]sendPhoneSMS:tip vc:self phone:friend.phone];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResult.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendModel* model = [self.searchResult objectAtIndex:indexPath.row];
    if (model.followState == MoRelationshipTypeNone) {
        [MXRouter openURL:@"lcwl://InviteUnFriendVC" parameters:@{@"model":model}];
    }else{
        //个人中心
        [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":model.userid}];
    }
    
}

- (void)cancelButtonDidClick {

    if (self.callback) {
        self.callback();
        [self.searchBar resignFirstResponder];
    }
}
- (void)dismissView {

    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        NSString *searchText = textField.text;
        [_searchResult removeAllObjects];
        [self filterFriendByKeyword:searchText];
        [self.tableView reloadData];
    }
}


-(void )filterFriendByKeyword:(NSString *)keyword{
    [_searchResult removeAllObjects];
    if ([StringUtil isEmpty:keyword]) {
        return;
    }
    for (int i=0; i<_friendData.count; i++) {
        FriendModel* model = _friendData[i];
        NSString* friendName = [[StringUtil chinasesCharToLetter:model.name] lowercaseString];
        NSString* contactName = [[StringUtil chinasesCharToLetter:model.addressListName] lowercaseString];
        NSString* phone = [[StringUtil chinasesCharToLetter:model.phone] lowercaseString];
        
        keyword = [[StringUtil chinasesCharToLetter:keyword] lowercaseString];
        if ((![StringUtil isEmpty:friendName]&&[friendName rangeOfString:keyword].location != NSNotFound) || (![StringUtil isEmpty:contactName]&&[contactName rangeOfString:keyword].location != NSNotFound)||(![StringUtil isEmpty:phone]&&[phone rangeOfString:keyword].location != NSNotFound)) {
            [_searchResult addObject:model];
        }
    }
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.vc.searchBar resignFirstResponder];
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = ({
            UITextField *object = [UITextField new];
            [object addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
            [object setViewCornerRadius:15];
            object.font = [UIFont font15];
            object.backgroundColor = [UIColor whiteColor];
            UIView *leftView = [UIView new];
            leftView.size = CGSizeMake(40, 30);
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"im_search_icon"] forState:UIControlStateNormal];
            [leftBtn sizeToFit];
            leftBtn.x = 10;
            leftBtn.y = 5;
            [leftView addSubview:leftBtn];
            object.leftView = leftView;
            object.returnKeyType = UIReturnKeySearch;
            object.leftViewMode = UITextFieldViewModeAlways;
            object.clearButtonMode = UITextFieldViewModeWhileEditing;
            NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索"] colors:@[[UIColor moBlack]] fonts:@[[UIFont font14]]];
            object.attributedPlaceholder = att;
            object.myLeft = 5;
            object.myWidth = SCREEN_WIDTH - 10;
            object.myHeight = 35;
            object.myTop = 10;
            object.myBottom = 10;
            [object setViewCornerRadius:17];
            object;
        });
    }
    return _searchTF;
}

@end

