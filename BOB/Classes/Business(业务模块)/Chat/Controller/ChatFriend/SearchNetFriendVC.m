//
//  MoreSearchVC.m
//  Lcwl
//
//  Created by mac on 2018/11/24.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "SearchNetFriendVC.h"
#import "LSearchBar.h"
#import "FriendListView.h"
#import "FriendsManager.h"
#import "MoreSeachView.h"
#import "MoreSearchVC.h"
#define TipString @"搜一搜:"
@interface SearchNetFriendVC ()<UISearchBarDelegate>

@property(nonatomic, strong) UIView *searchView;

@property(nonatomic, strong) LSearchBar *searchBars;

@property(nonatomic, strong) UIButton *cancelButton;

@property(nonatomic, strong) FriendListView *footer;

@property(nonatomic, strong) UINavigationController *searchNavigationController;

@property(nonatomic, strong) UIView* footerView;

@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation SearchNetFriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarTitle:@"搜索朋友"];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 60)];
    _footerView.mySize = _footerView.size;
    _footer = [[FriendListView alloc] initRowStyleDefault];
    [_footer style:RowStyleSubtitle];
    [_footer reloadLocalLogo:@"icon_search_navi" name:@"123" desc:@""];
    @weakify(self)
    _footer.selectCallback = ^(UIView * view){
        @strongify(self)
        MoreSearchVC* searchVC = [[MoreSearchVC alloc]init];
        searchVC.keyword = self.searchTF.text;
        [self.navigationController pushViewController:searchVC animated:YES];
    };
  
    [_footerView addSubview:_footer];
    
    [self.view addSubview:_footerView];
    [_footer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.footerView);
    }];
    _footerView.hidden = YES;
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.searchTF];
    [layout addSubview:self.footerView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchTF becomeFirstResponder];
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        NSString *searchText = textField.text;
        if (![StringUtil isEmpty:searchText]) {
            _footerView.hidden = NO;
            NSArray *txtArr = @[StrF(@"%@ ", TipString), searchText];
            NSArray *colorArr = @[[UIColor blackColor], [UIColor blueColor]];
            NSArray *fontArr = @[[UIFont font15], [UIFont font15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
            _footer.nameLbl.attributedText = att;
        } else {
            _footerView.hidden = YES;
            _footer.nameLbl.attributedText = [[NSMutableAttributedString alloc]initWithString:TipString];
        }
    }
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = ({
            UITextField *object = [UITextField new];
            [object addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
            [object setViewCornerRadius:15];
            object.font = [UIFont font15];
            object.backgroundColor = [UIColor whiteColor];
            UIView *leftView = [UIView new];
            leftView.size = CGSizeMake(40, 30);
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"im_search_icon"] forState:UIControlStateNormal];
            [leftBtn sizeToFit];
            leftBtn.x = 10;
            leftBtn.y = 5;
            [leftView addSubview:leftBtn];
            object.leftView = leftView;
            object.returnKeyType = UIReturnKeySearch;
            object.leftViewMode = UITextFieldViewModeAlways;
            object.clearButtonMode = UITextFieldViewModeWhileEditing;
            NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索"] colors:@[[UIColor moBlack]] fonts:@[[UIFont font14]]];
            object.attributedPlaceholder = att;
            object.myLeft = 5;
            object.myWidth = SCREEN_WIDTH - 10;
            object.myHeight = 35;
            object.myTop = 10;
            object.myBottom = 10;
            [object setViewCornerRadius:17];
            object;
        });
    }
    return _searchTF;
}

@end
