//
//  InfoSettingVC.h
//  Lcwl
//
//  Created by mac on 2018/12/22.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface InfoSettingVC : BaseViewController

@property (nonatomic) FriendModel *model;
@property (nonatomic,copy) NSString *group_id;
@property(nonatomic, copy) FinishedBlock block;
@end

NS_ASSUME_NONNULL_END
