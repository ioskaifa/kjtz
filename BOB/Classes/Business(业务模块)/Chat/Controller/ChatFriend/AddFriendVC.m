//
//  AddFriendVC.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "AddFriendVC.h"
#import "FriendListView.h"
#import "AddFriendTopView.h"
#import "MXSeparatorLine.h"
#import "MobileAddressVC.h"
#import "MoreSearchVC.h"
#import "SearchNetFriendVC.h"
#import "QRCodeScanManage.h"

@interface AddFriendCell : UITableViewCell

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *describeLbl;

@end

@implementation AddFriendCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addBottomSingleLine:[UIColor colorWithHexString:@"#E9E9E9"]];
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 54;
}

- (void)configureView:(NSDictionary *)dataDic {
    if (![dataDic isKindOfClass:NSDictionary.class]) {
        return;
    }
    if (dataDic[@"name"]) {
        self.titleLbl.text = [dataDic[@"name"] description];
    }
    
    if (dataDic[@"logo"]) {
        self.imgView.image = [UIImage imageNamed:[dataDic[@"logo"] description]];
        [self.imgView  sizeToFit];
        self.imgView.mySize = self.imgView.size;
    }
    
    if (dataDic[@"describe"]) {
        self.describeLbl.text = [dataDic[@"describe"] description];
    }
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.contentView addSubview:layout];
    [layout addSubview:self.imgView];
    
    MyLinearLayout *centerLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    centerLayout.weight = 1;
    centerLayout.myCenterY = 0;
    centerLayout.myLeft = 15;
    centerLayout.myHeight = MyLayoutSize.wrap;
    [layout addSubview:centerLayout];
    [centerLayout addSubview:self.titleLbl];
    [centerLayout addSubview:self.describeLbl];
    
    UIImageView *rightImgView = [UIImageView new];
    rightImgView.image = [UIImage imageNamed:@"Arrow"];
    [rightImgView sizeToFit];
    rightImgView.mySize = rightImgView.size;
    rightImgView.myCenterY = 0;
    rightImgView.myRight = 25;
    rightImgView.myLeft = 15;
    [layout addSubview:rightImgView];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.myLeft = 25;
            object.myCenterY = 0;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font14];
            object.textColor = [UIColor blackColor];
            object.height = 20;
            object.myHeight = object.height;
            object.myWidth = MyLayoutSize.fill;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)describeLbl {
    if (!_describeLbl) {
        _describeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font11];
            object.textColor = [UIColor colorWithHexString:@"#AEAEAE"];
            object.height = 14;
            object.myHeight = object.height;
            object.myWidth = MyLayoutSize.fill;
            object;
        });
    }
    return _describeLbl;
}

@end

@interface AddFriendVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)  UITableView *tb;
@property (nonatomic,strong)  NSArray *tbData;
@property (nonatomic,strong)  AddFriendTopView *headerView;
@end

@implementation AddFriendVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:@"添加朋友"];
    _tbData = @[@{@"name":@"面对面建群",@"logo":@"add_friend_face",@"describe":@"与身边朋友面对面建群",@"vcString":@""},
                @{@"name":@"扫一扫",@"logo":@"add_friend_scan",@"describe":@"扫描二维码名片",@"vcString":@"QRCodeScanManage"},
                @{@"name":@"手机联系人",@"logo":@"add_friend_addr_book",@"describe":@"添加手机联系人",@"vcString":@"MobileAddressVC"}];
     [self initUI];    
}

-(void)initUI{
    [self.view addSubview:self.tb];

    self.tb.tableHeaderView = self.headerView;
    AdjustTableBehavior(self.tb);
}

- (AddFriendTopView *)headerView {
    if (!_headerView) {
        _headerView = [AddFriendTopView new];
        _headerView.size = CGSizeMake(SCREEN_WIDTH, [AddFriendTopView viewHeight]);
        [_headerView addAction:^(UIView *view) {
            MXRoute(@"SearchNetFriendVC", nil);
        }];
    }
    return _headerView;
}
//消息列表
- (UITableView *)tb {
    if (!_tb) {
        _tb = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tb.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tb.rowHeight = [AddFriendCell viewHeight];
        _tb.dataSource = self;
        _tb.delegate = self;
        _tb.scrollEnabled = YES;
        _tb.sectionIndexBackgroundColor = [UIColor clearColor];
        _tb.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tb.backgroundColor = [UIColor whiteColor];
        _tb.backgroundView = nil;
        _tb.backgroundColor = [UIColor clearColor];
        _tb.separatorStyle = UITableViewCellEditingStyleNone;
        Class cls = AddFriendCell.class;
        [_tb registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
      
    }
    return _tb;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Class cls = AddFriendCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.tbData.count) {
        return;
    }
    NSDictionary *dataDic = self.tbData[indexPath.row];
    if (![dataDic isKindOfClass:NSDictionary.class]) {
        return;
    }
    if (![cell isKindOfClass:AddFriendCell.class]) {
        return;
    }
    AddFriendCell *listCell = (AddFriendCell *)cell;
    [listCell configureView:dataDic];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tbData.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row >= self.tbData.count) {
        return;
    }
    NSDictionary *dataDic = self.tbData[indexPath.row];
    if (![dataDic isKindOfClass:NSDictionary.class]) {
        return;
    }
    NSString *vc = dataDic[@"vcString"];
    if([vc isEqualToString:@"QRCodeScanManage"]) {
        [QRCodeScanManage startScan:^(id data) {
            if (data) {
                [self resetSweepTypeWithQrcode:data];
            }
        }];
        return;
    } else if ([StringUtil isEmpty:vc]) {
        [NotifyHelper showMessageWithMakeText:@"功能暂未开放"];
        return;
    }
    MXRoute(vc, nil);
}

#pragma mark-确定扫描模块,分析扫描内容
-(void)resetSweepTypeWithQrcode:(NSString *)qrCode
{
    if(qrCode == nil || ![qrCode isKindOfClass:[NSString class]]) {
        return;
    }
    
    NSString *content = nil;
    content = qrCode;
    //  扫描关注好友
    NSInteger startIndex;
    if ((startIndex = [qrCode rangeOfString:@"user?"].location) != NSNotFound){
        content = [qrCode substringFromIndex:startIndex+@"user?".length];
        [self addFriendWithID:content];
        return;
    }
    
    //扫描群二维码
    if ((startIndex = [qrCode rangeOfString:@"group?"].location) != NSNotFound){
        content = [qrCode substringFromIndex:startIndex+@"group?".length];
        [self inviteVCwithContent:content];
        return;
    }
}

#pragma mark - 扫一扫关注
- (void)addFriendWithID:(NSString*)qrCode
{
    if (![qrCode isKindOfClass:[NSString class]]) {
        return;
    }
    NSInteger startIndex;
    NSString *content;
    if ((startIndex = [qrCode rangeOfString:@"userId="].location) != NSNotFound){
        content = [qrCode substringFromIndex:startIndex+@"userId=".length];
    }
    [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":content}];
}

#pragma mark-扫描群
-(void)inviteVCwithContent:(NSString*)content{
    NSString* groupTag = @"groupId=";
    NSString* inviteTag = @"&inviteCode=";
    NSInteger indexGroup = [content rangeOfString:groupTag].location;
    NSInteger indexInvite = [content rangeOfString:inviteTag].location;
    NSString* groupId = [content substringWithRange:NSMakeRange(indexGroup+groupTag.length,indexInvite - indexGroup-groupTag.length)];
    NSString* inviteId = [content substringWithRange:NSMakeRange(indexInvite+inviteTag.length, content.length-(indexInvite+inviteTag.length))];
    [MXRouter openURL:@"lcwl://GroupDetailVC" parameters:@{@"groupid":groupId,@"inviteid":inviteId}];
}
@end
