//
//  NewFriendVC.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "NewFriendVC.h"
#import "MXBackButton.h"
#import "FriendListView.h"
#import "LcwlChat.h"
#import "FriendsManager.h"
#import "LcwlBlankPageView.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "FriendsManager.h"
#import "MXChatDBUtil.h"
#import "MXConversation.h"
#import "ChatSendHelper.h"
#import "SearchLocalFriendVC.h"
#import "UIImage+Utils.h"
#import "UIView+GeneralView.h"
#import "AddFriendTopView.h"

static NSString *const kReuseIdentifier = @"CellReuseIdentifier";

#define personal_details_backbutton_width 60
#define personal_details_backbutton_height 44
@interface NewFriendVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) MXBackButton                 *rightButton;
/** 列表 */
@property (nonatomic, strong) UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *dataSource;

@property (strong, nonatomic) MXConversation* conversation;

@property(nonatomic, strong)  UIView *headerView;

/** 搜索框 */
@property (nonatomic, strong) UISearchController *searchController;

@property (nonatomic, strong) AddFriendTopView *searchView;

@end

@implementation NewFriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarTitle:@"新朋友"];
    [self initUI];
    [self initData];
    AdjustTableBehavior(self.tableView)
}
-(void)initData{
    self.conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:nf];
    self.dataSource =  [[MXChatDBUtil sharedDataBase]selectMsgByUserId:nf];
    [self.dataSource sortUsingComparator:^NSComparisonResult(MessageModel  * obj1, MessageModel  * obj2) {
        FriendModel* friend1 = [[LcwlChat shareInstance].chatManager loadFriendByChatId:obj1.fromID];
        FriendModel* friend2 = [[LcwlChat shareInstance].chatManager loadFriendByChatId:obj2.fromID];
        if (friend1.followState == 1) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        if (friend2.followState == 1) {
            return (NSComparisonResult)NSOrderedDescending;
        }
         return (NSComparisonResult)NSOrderedSame;
    }];
    [self.conversation markAllMessagesAsRead];
    if (self.dataSource.count > 0) {
        [self.tableView reloadData];
    }
//    else{
//        [LcwlBlankPageView addBlankPageView:LcwlBlankPageNewFriendVCVCType withSuperView:self.view touchedBlock:^{
//
//        }];
//    }
    
}

- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [UIView new];
        _headerView.backgroundColor = [UIColor moBackground];
        MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        layout.myHeight = MyLayoutSize.wrap;
        layout.width = SCREEN_WIDTH;
        layout.myWidth = layout.width;
        layout.myTop = 0;
        layout.myLeft = 0;
        [_headerView addSubview:layout];
        [layout addSubview:self.searchView];
        
        MyLinearLayout *addLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
        addLayout.backgroundColor = [UIColor whiteColor];
        addLayout.myHeight = 54;
        addLayout.myLeft = 0;
        addLayout.myRight = 0;
        SPButton *btn = [SPButton new];
        btn.userInteractionEnabled = NO;
        btn.imagePosition = SPButtonImagePositionLeft;
        btn.imageTitleSpace = 5;
        [btn setImage:[UIImage imageNamed:@"电话"] forState:UIControlStateNormal];
        [btn setTitle:Lang(@"添加手机联系人") forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont font15];
        [btn sizeToFit];
        btn.mySize = btn.size;
        btn.myLeft = 15;
        btn.myCenterY = 0;
        [addLayout addSubview:btn];
        
        UIImageView *imgView = [UIImageView new];
        imgView.image = [UIImage imageNamed:@"Arrow"];
        [imgView sizeToFit];
        imgView.mySize = imgView.size;
        imgView.myCenterY = 0;
        imgView.myLeft = layout.width - btn.width - imgView.width - 15 - 15;
        [addLayout addSubview:imgView];
        [addLayout addAction:^(UIView *view) {
            MXRoute(@"MobileAddressVC", nil);
        }];
        
        [layout addSubview:addLayout];
        
        [layout layoutSubviews];
        _headerView.size = layout.size;
    }
    return _headerView;
}

//消息列表
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.tableFooterView = [[UIView alloc] init];
        AdjustTableBehavior(_tableView);
        //[_tableView registerNib:[UINib nibWithNibName:@"CareFansCell" bundle:nil] forCellReuseIdentifier:@"CareFansCell"];
    }
    return _tableView;
}

-(MXBackButton* )rightButton{
    if (!_rightButton) {
        _rightButton = [MXBackButton buttonWithType:UIButtonTypeCustom];
        _rightButton.frame = CGRectMake(16, personal_details_backbutton_height / 2 - personal_details_backbutton_height / 2, personal_details_backbutton_width, personal_details_backbutton_height);
        [_rightButton setTitle:@"添加朋友" forState:UIControlStateNormal];
        //[_rightButton setTitle:@"清空" forState:UIControlStateHighlighted];
        [_rightButton setImage:nil forState:UIControlStateNormal];
        [_rightButton setImage:nil forState:UIControlStateHighlighted];
        [_rightButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //[_rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        _rightButton.titleLabel.font = [UIFont font14];
        [_rightButton addTarget:self action:@selector(clear:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightButton;
}
-(void)clear:(id)sender{
    UIViewController *vc = [[NSClassFromString(@"AddFriendVC") alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
//    [UIActionSheet actionSheetWithTitle:@"确定清空新的好友" message:nil destructiveButtonTitle:@"确定" buttons:nil showInView:MoApp.window onDismiss:^(NSInteger buttonIndex) {
//        if (buttonIndex == 0) {
//            [self.dataSource removeAllObjects];
//            [[LcwlChat shareInstance].chatManager clearFriendRecords];
//            [self.tableView reloadData];
//        }
//
//    } onCancel:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
         [view style:RowStyleRightLabel];
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        
    }
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
    [tmpView setShowArrow:NO];
    MessageModel* message = [_dataSource objectAtIndex:indexPath.row];
    FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:message.fromID];
    NSString* userName = friend.name;
    if ([StringUtil isEmpty:userName]) {
        userName = @"";
    }
    if (friend.followState == 1) {
        [tmpView style:RowStyleButton];
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/120/h/120",friend.avatar];
        [tmpView reloadLogo:avatar name:userName desc:message.content btntitle:@"接受"];
        tmpView.rightBtnClickCallback=^(UIView * _Nonnull view){
            UITableViewCell* cell = (UITableViewCell* )view.superview.superview;
            NSIndexPath *path = [self.tableView indexPathForCell:cell];
            [FriendsManager addFriend:@{@"add_acce_id":friend.userid,@"add_type":@"2"} completion:^(id object, NSString *error) {
                if (object) {
                    NSDictionary* dict = (NSDictionary*)object;
                    NSInteger status = [dict[@"status"]intValue];
                    friend.followState = status;
                    [self.tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
              
                    if (status == MoRelationshipTypeStranger) {
                        NSInteger isverify = [dict[@"isverify"]integerValue];
                        if (isverify == 1) {
                            [MXRouter openURL:@"lcwl://VerifyFriendVC" parameters:@{@"model":friend}];
                        }
                    }else if(status == MoRelationshipTypeMyFriend){
                        FriendModel* tmpfriend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:friend.userid];
                        tmpfriend.followState = MoRelationshipTypeMyFriend;
                        [[LcwlChat shareInstance].chatManager insertFriends:@[tmpfriend]];
                        //已经是好友了
                        NSString* tip = [NSString stringWithFormat:@"您已添加了%@，可以开始聊天了",tmpfriend.name];
                        [ChatSendHelper sendChat:tip from:tmpfriend.userid messageType:kMxmessageTypeCustom type:eConversationTypeChat];
                        [[MXChatDBUtil sharedDataBase]updateFriendRecords:tmpfriend];
                    }
                    
                }
            }];
        };
    }else if(friend.followState == 3 ){
        [tmpView style:RowStyleRightLabel];
        NSMutableAttributedString* attrName = [[NSMutableAttributedString alloc]initWithString:userName];
        NSMutableAttributedString* attrDesc = [[NSMutableAttributedString alloc]initWithString:message.content ];
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/120/h/120",friend.avatar];
        [tmpView reloadLogo:avatar attrName:attrName attrDesc:attrDesc rightTip:@"已添加"];
    }
 
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)initUI{
    self.tableView.tableHeaderView = self.headerView;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    UIBarButtonItem *barBtnItem1 = [[UIBarButtonItem alloc] initWithCustomView:self.rightButton];
    self.navigationItem.rightBarButtonItem = barBtnItem1;
    
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    self.searchController.searchBar.frame = CGRectMake(0, 6, SCREEN_WIDTH, 30);
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    self.searchController.searchBar.frame = CGRectMake(5, 6, SCREEN_WIDTH-10, 30);
}

- (UISearchController *)searchController {
    if (!_searchController) {
        SearchLocalFriendVC* searchVC = [[SearchLocalFriendVC alloc]init];
        searchVC.callback = ^(){
            [[MoApp router] configureCurrentVC:self];
        };
        _searchController = [[UISearchController alloc]initWithSearchResultsController:searchVC];
        _searchController.searchBar.delegate = self;
        _searchController.delegate = self;
        _searchController.searchResultsUpdater = searchVC;
        //_searchController.delegate = self;
        _searchController.view.backgroundColor = [UIColor whiteColor];
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = YES;
        //[_searchController.searchBar sizeToFit];
        //_searchController.searchBar.tintColor = [UIColor blackColor];
        _searchController.searchBar.placeholder =  @"搜索";
        [_searchController.searchBar sizeToFit];
        UIOffset offset = {5.0,0};
        _searchController.searchBar.searchTextPositionAdjustment = offset;
        //_searchController.searchBar.backgroundImage = self.searchGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:[UIImage createImageWithColor:[UIColor whiteColor] size:CGSizeMake(1, 30)] forState:UIControlStateNormal];
        _searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
        _searchController.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;//关闭提示
        _searchController.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;//关闭自动首字母大写
        
        if (@available(iOS 13.0, *)) {
            _searchController.searchBar.searchTextField.layer.masksToBounds = YES;
            _searchController.searchBar.searchTextField.layer.cornerRadius = 6;
        }
    }
    return _searchController;
}

- (AddFriendTopView *)searchView {
    if (!_searchView) {
        _searchView = [AddFriendTopView new];
        _searchView.placeholderLbl.text = @"搜索";
        _searchView.myLeft = 0;
        _searchView.myWidth = SCREEN_WIDTH;
        _searchView.myHeight = [AddFriendTopView viewHeight];
        _searchView.myTop = 0;
        [_searchView addAction:^(UIView *view) {
            MXRoute(@"SearchLocalFriendVC", nil);
        }];
       
    }
    return _searchView;
}

@end
