//
//  PersonalDetailVC.h
//  BOB
//
//  Created by mac on 2019/7/11.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BaseSTDTableViewVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PersonalDetailVC : BaseSTDTableViewVC
@property (nonatomic,strong) NSString *other_id;
@property (nonatomic,strong) NSString *group_id;
@property(nonatomic,assign) NSInteger creatorRole;
@property(nonatomic, copy) FinishedBlock block;
@end

NS_ASSUME_NONNULL_END
