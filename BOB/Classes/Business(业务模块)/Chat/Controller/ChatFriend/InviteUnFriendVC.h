//
//  InviteFriendVC.h
//  Lcwl
//
//  Created by mac on 2018/12/19.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "FriendModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface InviteUnFriendVC : BaseViewController

@property (nonatomic , strong) FriendModel* model;

@end

NS_ASSUME_NONNULL_END
