//
//  SearchLocalFriendVC.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/25.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^SearchResultCallback)(id sender, UIViewController* fromVC);
typedef void(^CancelBtnClick)() ;


@interface SearchLocalFriendVC : BaseViewController
@property (nonatomic, copy) CancelBtnClick  callback;
@property (nonatomic, assign) BOOL hidenChatBnt;
@property (nonatomic, copy) NSString* searchTitle;
@property (nonatomic) int type;

@end
