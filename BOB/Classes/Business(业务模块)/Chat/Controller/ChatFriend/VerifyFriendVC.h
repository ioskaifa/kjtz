//
//  VerifyFriendVC.h
//  Lcwl
//
//  Created by mac on 2018/12/20.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "FriendModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VerifyFriendVC : BaseViewController

@property (strong, nonatomic)  FriendModel* model;

@property(nonatomic, copy) FinishedBlock block;

@end

NS_ASSUME_NONNULL_END
