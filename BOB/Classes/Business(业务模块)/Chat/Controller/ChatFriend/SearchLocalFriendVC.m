//
//  SearchLocalFriendVC.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/25.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "SearchLocalFriendVC.h"
#import "ChatTitleView.h"
#import "MXSeparatorLine.h"
#import "NSObject+Additions.h"
#import "LSearchBar.h"
#import "UINavigationBar+Alpha.h"
#import "ContactHelper.h"
#import "FriendModel.h"
#import "FriendListView.h"
#import "LcwlChat.h"
#import "MoreSearchVC.h"
#define TipString @"搜一搜:"
static int rowHeight = 60;
static const CGFloat headerHeight = 30;
@interface SearchLocalFriendVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (nonatomic,strong) UITableView *tableView;

@property (strong, nonatomic) NSMutableArray* searchResult;

@property(nonatomic, strong) NSMutableArray *friendData;

@property(nonatomic, strong) NSMutableArray *moyouData;

@property(nonatomic, strong) FriendListView *footer;

@property(nonatomic, strong)  UISearchController* vc;

@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation SearchLocalFriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarTitle:@"搜索联系人"];
    _searchResult = [[NSMutableArray alloc]initWithCapacity:10];
    _friendData = [[LcwlChat shareInstance].chatManager friends];

    self.view.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.searchTF];
    [layout addSubview:self.tableView];
   
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  self.view.bounds.size.width, rowHeight)];
    _footer = [[FriendListView alloc]initRowStyleDefault];
    [_footer style:RowStyleSubtitle];
    
    [_footer reloadLocalLogo:@"icon_search_navi" attrDesc:@""];
    @weakify(self);
    _footer.selectCallback = ^(UIView *view){
        @strongify(self)
        [self.vc.searchBar resignFirstResponder];
        [MXRouter openURL:@"lcwl://MoreSearchVC" parameters:@{@"keyword":self.searchTF.text}];
    };
    [footerView addSubview:_footer];
    [_footer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(footerView);
    }];
    footerView.hidden = YES;
    self.tableView.tableFooterView = footerView;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchTF becomeFirstResponder];
}

//消息列表
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.backgroundView = nil;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *cellId = @"cellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellId];
            if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
                [cell setSeparatorInset:UIEdgeInsetsZero];
            }
            
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins:UIEdgeInsetsZero];
            }
            FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
            view.tag = 101;
            [cell.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(cell.contentView);
            }];
            cell.selectionStyle =  UITableViewCellSelectionStyleNone;
            
        }
        FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
        NSArray* tmpArray = [_searchResult objectAtIndex:indexPath.section];
        FriendModel* model = [tmpArray objectAtIndex:indexPath.row];
        NSString* str = model.name;
        if (![StringUtil isEmpty:model.remark]) {
            str = model.remark;
        }
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:str];
        NSString *keyword = self.searchTF.text;
        if (![StringUtil isEmpty:keyword]) {
            NSRange range = [str rangeOfString:keyword];
            if (range.location != NSNotFound) {
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:range];
            }
        }
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/60/h/60",model.avatar];

        [tmpView reloadLogo:avatar attrName:string];
        return cell;
    } else {
        static NSString *cellId = @"cellIdentifier2";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellId];
            if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
                [cell setSeparatorInset:UIEdgeInsetsZero];
            }
            
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins:UIEdgeInsetsZero];
            }
            
            cell.selectionStyle =  UITableViewCellSelectionStyleNone;
            FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
            [view style:RowStyleSubtitle];
            view.tag = 101;
            [cell.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(cell.contentView);
            }];
        }
        FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
        NSArray* tmpArray = [_searchResult objectAtIndex:indexPath.section];
        FriendModel* model = [tmpArray objectAtIndex:indexPath.row];
        NSString *str = model.addressListName;
        NSMutableAttributedString *attrName = [[NSMutableAttributedString alloc]initWithString:str];
        NSString *keyword = self.searchTF.text;
        if (![StringUtil isEmpty:keyword]) {
            NSRange range = [str rangeOfString:keyword];
            if (range.location != NSNotFound) {
                [attrName addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:range];
            }
        }
        NSMutableAttributedString* attrDesc = [[NSMutableAttributedString alloc]initWithString: model.phones[0]];
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/60/h/60",model.avatar];
        [tmpView reloadLogo:avatar attrName:attrName attrDesc:attrDesc];
        return cell;
    }
   
  
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.searchResult && self.searchResult.count>0) {
        NSArray* array = [self.searchResult objectAtIndex:section];
        if (array && [array isKindOfClass:[NSArray class]]) {
            return array.count;
        }
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.vc.searchBar resignFirstResponder];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FriendModel* model = self.searchResult[indexPath.section][indexPath.row];
    [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":model.userid}];
}

- (void)cancelButtonDidClick {
    if (self.callback) {
        self.callback();
        [self.vc.searchBar resignFirstResponder];
    }
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        NSString *searchText = textField.text;
        [self filterFriendByKeyword:searchText];
        [self.tableView reloadData];
        if ([StringUtil isEmpty:searchText]) {
            _footer.nameLbl.attributedText = [[NSMutableAttributedString alloc]initWithString:TipString];
            self.tableView.tableFooterView.hidden = YES;
        } else {
            NSArray *txtArr = @[StrF(@"%@ ", TipString), searchText];
            NSArray *colorArr = @[[UIColor blackColor], [UIColor blueColor]];
            NSArray *fontArr = @[[UIFont font15], [UIFont font15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
            _footer.nameLbl.attributedText = att;
            self.tableView.tableFooterView.hidden = NO;
        }
    }
}

-(void )filterFriendByKeyword:(NSString *)keyword{
    [_searchResult removeAllObjects];
    if ([StringUtil isEmpty:keyword]) {
        return;
    }
    NSMutableArray* friendArray = [[NSMutableArray alloc]initWithCapacity:10];
    NSMutableArray* contactArray = [[NSMutableArray alloc]initWithCapacity:10];
    for (int i=0; i<_friendData.count; i++) {
        FriendModel* model = _friendData[i];
        NSString* friendName = [[StringUtil chinasesCharToLetter:model.name] lowercaseString];
        NSString* contactName = [[StringUtil chinasesCharToLetter:model.addressListName] lowercaseString];
        NSString *remarkName = [[StringUtil chinasesCharToLetter:model.remark] lowercaseString];
        keyword = [[StringUtil chinasesCharToLetter:keyword] lowercaseString];
        
        if ((![StringUtil isEmpty:friendName] && [friendName rangeOfString:keyword].location != NSNotFound)||
             (![StringUtil isEmpty:contactName] &&  [contactName rangeOfString:keyword].location != NSNotFound) ||
              (![StringUtil isEmpty:remarkName] && [remarkName rangeOfString:keyword].location != NSNotFound)) {
            if (model.followState == MoRelationshipTypeNone) {
                [contactArray addObject:model];
            }else{
                [friendArray addObject:model];
            }
        }
    }
    [_searchResult insertObject:friendArray atIndex:0];
    [_searchResult insertObject:contactArray atIndex:1];
}

-(NSString*)sectionTitle:(NSInteger) segIndex section:(NSInteger)section{
     if (_searchResult && _searchResult.count > 0 ){
         if ([[_searchResult objectAtIndex:section] count] != 0){
             if (section == 0) {
                 return @"好友";
             }else if(section == 1){
                 return @"手机通讯录";
             }
         }
     }
  
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString* sectionTitle = [self sectionTitle:0 section:section];
    if (sectionTitle==nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label=[[UILabel alloc] init];
    label.frame=CGRectMake(12, headerHeight-20, 300, 20);
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor blackColor];
    label.font=[UIFont fontWithName:@"Helvetica-Bold" size:14];
    label.text=sectionTitle;
    
    // Create header view and add label as a subview
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
    [sectionView setBackgroundColor:[UIColor whiteColor]];
    [sectionView addSubview:label];
    
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
      if (_searchResult && _searchResult.count > 0 ){
        NSString* sectionTitle = [self sectionTitle:0 section:section];
          if (sectionTitle!=nil) {
              return headerHeight;
          }
      }
    return 0.01;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = ({
            UITextField *object = [UITextField new];
            [object addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
            [object setViewCornerRadius:15];
            object.font = [UIFont font15];
            object.backgroundColor = [UIColor whiteColor];
            UIView *leftView = [UIView new];
            leftView.size = CGSizeMake(40, 30);
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"im_search_icon"] forState:UIControlStateNormal];
            [leftBtn sizeToFit];
            leftBtn.x = 10;
            leftBtn.y = 5;
            [leftView addSubview:leftBtn];
            object.leftView = leftView;
            object.returnKeyType = UIReturnKeySearch;
            object.leftViewMode = UITextFieldViewModeAlways;
            object.clearButtonMode = UITextFieldViewModeWhileEditing;
            NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索"] colors:@[[UIColor moBlack]] fonts:@[[UIFont font14]]];
            object.attributedPlaceholder = att;
            object.myLeft = 5;
            object.myWidth = SCREEN_WIDTH - 10;
            object.myHeight = 35;
            object.myTop = 10;
            object.myBottom = 10;
            [object setViewCornerRadius:17];
            object;
        });
    }
    return _searchTF;
}

@end
