//
//  FriendVC.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "FriendVC.h"
#import "MXBackButton.h"
#import "MXSeparatorLine.h"
#import "LSearchBar.h"
#import "ChatFriendCell.h"
#import "FriendListView.h"
#import "SearchLocalFriendVC.h"
#import "FriendViewModel.h"
#import "FriendModel.h"
#import "LcwlChat.h"
#import "ContactHelper.h"
#import "ChatViewVC.h"
#import "UIImage+Utils.h"
#import "UIView+SingleLineView.h"
#import "AddFriendTopView.h"
#import "ManageAuthUserListVC.h"
#import "ManageAuthUserListObj.h"
#import "MXChatManager.h"

static const CGFloat headerHeight = 22;
static const CGFloat rowHeight = 52;

@interface FriendVC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)  UITableView                  *tb;

@property(nonatomic, strong)  UIView                       *headerView;

@property(nonatomic, strong)  FriendViewModel* viewModel;

@property(nonatomic, strong)  NSMutableArray* dataSource;

@property (nonatomic, strong) AddFriendTopView *searchView;

@property (nonatomic, strong) ManageAuthUserListVC *manageListVC;

@property (nonatomic, strong) NSMutableArray *serviceListMArr;

@end

@implementation FriendVC

-(void)dealloc{
    
    MLog(@"dealloc-->%@",NSStringFromClass([self class]));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];

    _viewModel = [[FriendViewModel alloc]init];
   
    [self initUI];
    [self setNavBarTitle:@"通讯录"];
    [[ContactHelper shareInstance] getDataFromContact];
    [ContactHelper moblieImportBook];
//    [self fetchServerListData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    //加完索引后的好友数据
    _dataSource =  [FriendViewModel getFriendData];
    if (![self.dataSource containsObject:self.serviceListMArr]) {
        [self.dataSource replaceObjectAtIndex:0 withObject:self.serviceListMArr];
    }
    [self.tb reloadData];
}

- (void)fetchServerListData {
    [self request:@"api/user/info/getMemberServiceList"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        NSArray *dataArr = object[@"data"][@"serviceList"];
        dataArr = [ManageAuthUserListObj modelListParseWithArray:dataArr];
        NSArray *tempArr = [dataArr valueForKeyPath:@"@unionOfObjects.ID"];
        [MXCache setValue:tempArr forKey:kMXMemberServiceList];
        [self.serviceListMArr removeAllObjects];
        NSArray *fried = [[LcwlChat shareInstance].chatManager friends];
        ///未保存在本地的用户
        NSMutableArray *notFriendMArr = [NSMutableArray array];
        for (ManageAuthUserListObj *obj in dataArr) {
            if (![obj isKindOfClass:ManageAuthUserListObj.class]) {
                continue;
            }
            //如果是自己则不显示
            if ([obj.ID isEqualToString:UDetail.user.user_id]) {
                continue;
            }
            FriendModel *model = [FriendModel new];
            model.userid = obj.ID;
            model.name = obj.nick_name;
            model.user_name = obj.nick_name;
            model.nick_name = obj.nick_name;
            model.smartName = obj.nick_name;
            model.head_photo = obj.head_photo;
            model.avatar = obj.head_photo;
            [self.serviceListMArr addObject:model];
            //判断是否本地好友  不是本地好友 写入本地
            NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"userid == '%@'", obj.ID)];
            NSArray *tempArr = [fried filteredArrayUsingPredicate:predicate];
            if (tempArr.count > 0) {
                //把备注带上
                FriendModel *tempObj = tempArr.firstObject;
                if ([tempObj isKindOfClass:FriendModel.class]) {
                    model.remark = tempObj.remark;
                }
                continue;
            }
            model.followState = MoRelationshipTypeStranger;
            [notFriendMArr addObject:model];
        }
        if (notFriendMArr.count > 0) {
            ///写入数据库 聊天都需要从本地数据库取用户信息
            [[MXChatManager sharedInstance] insertFriends:notFriendMArr];
        }
        if (![self.dataSource containsObject:self.serviceListMArr]) {
            [self.dataSource replaceObjectAtIndex:0 withObject:self.serviceListMArr];
        }
        [self.tb reloadData];
    }];
}

#pragma mark - Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
        view.userInteractionEnabled = NO;
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
    }
    
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
    [tmpView setShowArrow:NO];
    NSArray* tmpArray = [_dataSource objectAtIndex:indexPath.section];
    FriendModel* model = [tmpArray objectAtIndex:indexPath.row];
    NSString* name = model.smartName;
    [tmpView reloadLogo:model.avatar name:name];    
    [tmpView setShowDownLine:YES];
    return cell;
        
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

#pragma mark - Table view delegate
-(NSString*)sectionTitle:(NSInteger) segIndex section:(NSInteger)section{
    if ([[_dataSource objectAtIndex:section] count] != 0){
        NSString* str = [NSString stringWithFormat:@"%@", [[ALPHA2 substringFromIndex:section] substringToIndex:1]];
        if ([str isEqualToString:@"客"]) {
            return @"客服人员";
        }
        return str;
    }
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
       NSString *sectionTitle = [self sectionTitle:0 section:section];
    if ([StringUtil isEmpty:sectionTitle]) {
        return nil;
    }

    UIView *sectionView = [UIView new];
    sectionView.frame = CGRectMake(0, 0, SCREEN_WIDTH, headerHeight);
    [sectionView setBackgroundColor:[UIColor moBackground]];
    
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont font11];
    label.text = sectionTitle;
    [label sizeToFit];
    label.x = 15;
    label.centerY = sectionView.centerY;
    [sectionView addSubview:label];
    
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSString* sectionTitle = [self sectionTitle:0 section:section];
    if (sectionTitle!=nil) {
        return headerHeight;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray* tmpArray = [self.dataSource objectAtIndex:section];
    if (tmpArray.count) {
        return tmpArray.count;
    } else {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ALPHA2.length;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    NSMutableArray *indices = [NSMutableArray arrayWithObject:UITableViewIndexSearch];
    for (int i = 0; i < ALPHA2.length; i++){
        if ([[_dataSource objectAtIndex:i] count])
         [indices safeAddObj:[[ALPHA2 substringFromIndex:i] substringToIndex:1]];
    }
    return indices;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FriendModel* friendModel = self.dataSource[indexPath.section][indexPath.row];
    [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":friendModel.userid,@"block":
            ^(FriendModel* model){
                if(model) {
                    friendModel.avatar = model.avatar;
                    friendModel.groupNickname = model.groupNickname;
                    friendModel.nick_name = model.nick_name;
                    [self.tb reloadData];
                }
            }}];
}

#pragma mark - InitUI
-(void)initUI{
    self.tb.tableHeaderView = self.headerView;
    [self.view addSubview:self.tb];
    AdjustTableBehavior(self.tb);
    [self.tb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
//    [self addChildViewController:self.manageListVC];
//    [self.view addSubview:self.manageListVC.view];
//    [self.manageListVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(self.view);
//    }];
//    if (UDetail.user.is_valid) {
//        self.manageListVC.view.hidden = YES;
//    } else {
//        self.manageListVC.view.hidden = NO;
//    }
}

#pragma mark - Init
- (AddFriendTopView *)searchView {
    if (!_searchView) {
        _searchView = [AddFriendTopView new];
        _searchView.placeholderLbl.text = @"搜索联系人";
        _searchView.myLeft = 0;
        _searchView.myRight = 0;
        _searchView.myHeight = [AddFriendTopView viewHeight];
        _searchView.myTop = 0;
        [_searchView addAction:^(UIView *view) {
            MXRoute(@"SearchLocalFriendVC", nil);
        }];
       
    }
    return _searchView;
}

- (ManageAuthUserListVC *)manageListVC {
    if (!_manageListVC) {
        _manageListVC = [ManageAuthUserListVC new];
    }
    return _manageListVC;
}

- (UIView *)headerView {
    if (!_headerView) {
        _headerView = ({
            UIView *object = [UIView new];
            MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            layout.backgroundColor = [UIColor colorWithHexString:@"MyLinearLayout"];
            layout.myTop = 0;
            layout.myLeft = 0;
            layout.myRight = 0;
            layout.myHeight = MyLayoutSize.wrap;
            [object addSubview:layout];
            [layout addSubview:self.searchView];
            NSArray *dataArr = [_viewModel.class headerData];
            for (NSInteger i = 0; i < dataArr.count; i++) {
                NSDictionary *dict = dataArr[i];
                FriendListView *view = [[FriendListView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, rowHeight)];
                view.myHeight = rowHeight;
                if (i == dataArr.count-1) {
                    [view setShowDownLine:NO];
                }
                NSString* logo = dict[@"logo"];
                NSString* name = dict[@"name"];
                __block NSString* vcString = dict[@"vcString"];
                [view reloadLocalLogo:logo name:name];
                view.selectCallback = ^(UIView *view){
                    MXRoute(vcString, nil);
                };
                [layout addSubview:view];
            }
            [layout layoutSubviews];
            object.frame = CGRectMake(0, 0, SCREEN_WIDTH, layout.height);
            object;
        });
    }
    return _headerView;
}

- (void)backAction:(id)sender {
    [self.view endEditing:YES];
    [super backAction:sender];
}

//消息列表
- (UITableView *)tb {
    
    if (_tb == nil) {
         CGRect rect = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(0, 0, 0, 0));
        _tb = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
        //_tb.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tb.dataSource = self;
        _tb.delegate = self;
        _tb.sectionIndexBackgroundColor = [UIColor clearColor];
        _tb.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tb.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tb setSectionIndexColor:[UIColor lightGrayColor]];
        _tb.backgroundView = nil;
        _tb.backgroundColor = [UIColor moBackground];
    }
    return _tb;
}

- (NSMutableArray *)serviceListMArr {
    if (!_serviceListMArr) {
        _serviceListMArr = [NSMutableArray array];
    }
    return _serviceListMArr;
}

@end
