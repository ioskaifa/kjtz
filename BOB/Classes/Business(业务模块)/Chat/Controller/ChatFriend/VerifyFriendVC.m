//
//  TextChangeVC.m
//  Lcwl
//
//  Created by AlphaGO on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "VerifyFriendVC.h"
#import "FriendsManager.h"
//#import "UserModel.h"
#import "LcwlChat.h"

static int padding = 15;
@interface VerifyFriendVC ()
@property (strong, nonatomic)  UITextField *textField;
@property (strong, nonatomic)  UILabel *tipInfoLbl;
@property (strong, nonatomic)  UIView *textfieldBack;
@end

@implementation VerifyFriendVC

-(UITextField*)textField{
    if (!_textField) {
       _textField = [[UITextField alloc] initWithFrame:CGRectZero];
        _textField.font = [UIFont font17];
        _textField.textColor = [UIColor blackColor];
        _textField.backgroundColor = [UIColor clearColor];
    }
    return _textField;
}

-(UILabel*)tipInfoLbl{
    if (!_tipInfoLbl) {
        _tipInfoLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _tipInfoLbl.text = @"你需要发送验证申请,等待对方通过";
        _tipInfoLbl.textColor = [UIColor grayColor];
        _tipInfoLbl.font = [UIFont font12];
    }
    return _tipInfoLbl;
}

-(UIView*)textfieldBack{
    if (!_textfieldBack) {
        _textfieldBack = [[UIView alloc] initWithFrame:CGRectZero];
        _textfieldBack.backgroundColor = [UIColor whiteColor];
        
    }
    return _textfieldBack;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNavBarTitle:@"好友验证"];
    self.edgesForExtendedLayout =  UIRectEdgeNone;
    [self.view addSubview:self.textfieldBack];
    [self.textfieldBack addSubview:self.textField];
    [self.view addSubview:self.tipInfoLbl];
    [self setNavBarRightBtnWithTitle:@"发送" andImageName:nil];
    UserModel* person = [LcwlChat shareInstance].user;
    self.textField.text = [NSString stringWithFormat:@"我是%@",person.smartName];
    
    [self.tipInfoLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(padding);
        make.right.equalTo(self.view).offset(-padding);
        make.top.equalTo(self.view).offset(padding);
    }];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.textfieldBack.mas_centerX);
        make.centerY.equalTo(self.textfieldBack.mas_centerY);
        make.left.equalTo(self.view).offset(padding);
        make.right.equalTo(self.view).offset(-padding);
        make.height.equalTo(@(60));
    }];
    
    [self.textfieldBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tipInfoLbl.mas_bottom).offset(padding);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@(60));
    }];
    
    
    [self.tipInfoLbl becomeFirstResponder];
}

- (void)navBarRightBtnAction:(id)sender {
    Block_Exec(self.block,self.textField.text);
    [FriendsManager sendVerifyMsg:@{@"send_acce_id":self.model.userid,@"account":self.textField.text} completion:^(BOOL success, NSString *error) {
        if (success) {
            [NotifyHelper showMessageWithMakeText:@"发送成功"];
            [super backAction:nil];
        }
    }];
}
@end
