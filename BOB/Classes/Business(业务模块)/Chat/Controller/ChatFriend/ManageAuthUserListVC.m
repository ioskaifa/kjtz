//
//  ManageAuthUserListVC.m
//  BOB
//
//  Created by mac on 2020/8/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ManageAuthUserListVC.h"
#import "ManageAuthUserListObj.h"
#import "ManageAuthUserListCell.h"
#import "TalkManager.h"
#import "MXChatManager.h"
#import "LcwlChat.h"

@interface ManageAuthUserListVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@end

@implementation ManageAuthUserListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

#pragma mark - Private Method
- (void)fetchData {
    [self.dataSourceMArr removeAllObjects];
    [self request:@"/api/user/info/getManageAuthUserList"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefresh];
        if (success) {
            NSArray *dataArr = object[@"data"][@"manageAuthUserList"];
            dataArr = [ManageAuthUserListObj modelListParseWithArray:dataArr];
            NSArray *fried = [[LcwlChat shareInstance].chatManager friends];
            if (dataArr) {
                NSMutableArray *serverMArr = [NSMutableArray arrayWithCapacity:dataArr.count];
                for (ManageAuthUserListObj *obj in dataArr) {
                    if (![obj isKindOfClass:ManageAuthUserListObj.class]) {
                        continue;
                    }
                    if ([StringUtil isEmpty:obj.ID]) {
                        continue;
                    }
                    //判断是否本地好友  不是本地好友 写入本地
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"userid == '%@'", obj.ID)];
                    NSArray *tempArr = [fried filteredArrayUsingPredicate:predicate];
                    if (tempArr.count > 0) {
                        continue;
                    }
                    ///保存昵称 聊天界面展示
                    FriendModel *model = [FriendModel new];
                    model.userid = obj.ID;
                    model.name = obj.nick_name;
                    model.user_name = obj.nick_name;
                    model.nick_name = obj.nick_name;
                    model.smartName = obj.nick_name;
                    model.head_photo = obj.head_photo;
                    model.avatar = obj.head_photo;
                    [serverMArr addObject:model];
                }
                [[MXChatManager sharedInstance] insertFriends:serverMArr];
            }
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            [self.tableView reloadData];
        }
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = ManageAuthUserListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    
    if (![cell isKindOfClass:ManageAuthUserListCell.class]) {
        return;
    }
    ManageAuthUserListObj *obj = self.dataSourceMArr[indexPath.row];
    ManageAuthUserListCell *listCell = (ManageAuthUserListCell *)cell;
    [listCell configureView:obj];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return 0.01;
    }
    return [ManageAuthUserListCell viewHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    ManageAuthUserListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:ManageAuthUserListObj.class]) {
        return;
    }
    
    [TalkManager pushChatViewUserId:obj.ID type:eConversationTypeChat];
}

#pragma mark - InitUI
- (void)createUI {
    self.view.backgroundColor = [UIColor moBackground];
    [self initUI];
}

- (void)initUI {
    [self setNavBarTitle:@"通讯录"];
    [self.view addSubview:self.tableView];
    
    [self layoutUI];
}

- (void)layoutUI {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        Class cls = ManageAuthUserListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.tableFooterView = [UIView new];
        @weakify(self)
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self)
            [self fetchData];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

@end
