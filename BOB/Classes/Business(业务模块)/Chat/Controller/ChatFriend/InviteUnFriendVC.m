//
//  InviteFriendVC.m
//  Lcwl
//
//  Created by mac on 2018/12/19.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "InviteUnFriendVC.h"
#import "ContactHelper.h"
//#import "UserModel.h"
#import "LcwlChat.h"

static int padding = 15;
@interface InviteUnFriendVC ()

@property (nonatomic, strong) UIImageView *logoImg;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *phoneLbl;

@property (nonatomic, strong) UIButton *inviteBtn;

@property (nonatomic, strong) UILabel *inviteLbl;


@end

@implementation InviteUnFriendVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self initData];
}

-(void)initUI{
    [self.view addSubview:self.logoImg];
    [self.view addSubview:self.nameLbl];
    [self.view addSubview:self.phoneLbl];
    [self.view addSubview:self.inviteBtn];
    [self.view addSubview:self.inviteLbl];
    MXSeparatorLine* line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self.view addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.nameLbl);
        make.top.equalTo(self.phoneLbl.mas_bottom).offset(10);
        make.height.equalTo(@(0.5));
    }];
    [self layout];
}

-(void)initData{
    self.nameLbl.text = self.model.addressListName;
//    self.phoneLbl.text =
    NSString* phoneStr = [NSString stringWithFormat:@"手机 %@",self.model.phone];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:phoneStr];
    [attrString addAttribute:NSForegroundColorAttributeName
     
                       value:[UIColor grayColor]
     
                       range:NSMakeRange(2, phoneStr.length-2)];
    [attrString addAttribute:NSFontAttributeName
     
                       value:[UIFont font14]
     
                       range:NSMakeRange(2, phoneStr.length-2)];
    self.phoneLbl.attributedText = attrString;
}

-(void)layout{
    [self.logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(padding);
        make.top.equalTo(self.view.mas_top).offset(50);
        make.width.height.equalTo(@(100));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.logoImg);
        make.right.equalTo(self.view.mas_right).offset(-padding);
        make.top.equalTo(self.logoImg.mas_bottom).offset(padding);
    }];
    [self.phoneLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.nameLbl);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(padding);;
    }];
    [self.inviteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.nameLbl);
        make.bottom.equalTo(self.view.mas_bottom).offset(-80);
        make.height.equalTo(@(50));
    }];
    [self.inviteLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.inviteBtn);
        make.top.equalTo(self.inviteBtn.mas_bottom).offset(10);
    }];
}
- (UIImageView *)logoImg
{
    if (!_logoImg) {
        _logoImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImg.image = [UIImage imageNamed:@"avatar_default"];
    }
    
    return _logoImg;
}

-(UILabel* )nameLbl{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc]init];
        _nameLbl.text = @"张三";
        _nameLbl.font = [UIFont systemFontOfSize:30];
        _nameLbl.textColor = [UIColor blackColor];
        _nameLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _nameLbl;
}

-(UILabel* )phoneLbl{
    if (!_phoneLbl) {
        _phoneLbl = [[UILabel alloc]init];
        _phoneLbl.text = @"手机 15818614416";
        _phoneLbl.font = [UIFont font17];
        _phoneLbl.textColor = [UIColor blackColor];
        _phoneLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _phoneLbl;
}

-(UILabel* )inviteLbl{
    if (!_inviteLbl) {
        _inviteLbl = [[UILabel alloc]init];
        _inviteLbl.text = @"邀请好友加入,得更多美元";
        _inviteLbl.font = [UIFont font14];
        _inviteLbl.textColor = [UIColor grayColor];
        _inviteLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _inviteLbl;
}

-(UIButton *)inviteBtn{
    if (!_inviteBtn) {
        _inviteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _inviteBtn.titleLabel.font = [UIFont font17];
        [_inviteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _inviteBtn.backgroundColor = [UIColor moBlueColor];
        _inviteBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [_inviteBtn addTarget:self action:@selector(inviteClick:) forControlEvents:UIControlEventTouchUpInside];
        [_inviteBtn setTitle:@"邀请" forState:UIControlStateNormal];
        _inviteBtn.layer.masksToBounds = YES;
        _inviteBtn.layer.cornerRadius = 22;
    }
    return _inviteBtn;
}

-(void)inviteClick:(id)sender{
    [self sendSysMessage:self.model];
}


-(void)sendSysMessage:(FriendModel* )friend{
    NSString* tip = @"分享给你9聊APP，下载地址：https://dev1.mifengff.com/ygjqg4。";
    [[ContactHelper shareInstance] sendPhoneSMS:tip vc:self phone:friend.phone];
}



@end
