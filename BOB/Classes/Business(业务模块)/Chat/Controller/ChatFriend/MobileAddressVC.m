//
//  MobileContactVC.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "MobileAddressVC.h"

#import "FriendModel.h"
#import "ContactHelper.h"
#import "FriendListView.h"
#import "LSearchBar.h"
#import "LcwlChat.h"
#import "SearchContactVC.h"
#import "LcwlBlankPageView.h"
#import "FriendsManager.h"
#import "ChatSendHelper.h"
#import "AddFriendTopView.h"

static const CGFloat headerHeight = 22;
static const CGFloat rowHeight = 60;
@interface MobileAddressVC ()<UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating,UISearchBarDelegate,UISearchControllerDelegate>
@property (nonatomic, strong) MXBackButton       *rightButton;
/** 列表 */
@property (nonatomic, strong) UITableView        *tableView;

@property (nonatomic, strong) NSMutableArray     *dataSource;

@property (nonatomic, strong) NSMutableArray     *data;
/** 搜索框 */
@property (nonatomic, strong) UISearchController *searchController;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage            *searchGrayImage;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage            *searchWhiteImage;
/** 列表头视图 */
@property (nonatomic, strong) MyLinearLayout     *tableHeadView;

@property (nonatomic, strong) AddFriendTopView *searchView;

@end

@implementation MobileAddressVC

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setNavBarTitle:@"通讯录朋友"];
    [self initUI];
    [self initData];
}

-(void)initUI{
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.definesPresentationContext = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //self.tableView.frame = CGRectMake(0, 100, SCREEN_WIDTH, self.view.height);
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
}

-(void)initData{
    [self getContacts];
}

- (void)getContacts {
    if ([[ContactHelper shareInstance] authorizationed]) {
        
        @weakify(self)
        [FriendsManager moblieImportBook:[self contactPhoneNoSet] completion:^(id array, NSString *error) {
            @strongify(self)
            if (array && [array isKindOfClass:[NSArray class]]) {
                NSMutableArray* tmpArray = (NSMutableArray* )array;
                for (int i=0; i<[ContactHelper shareInstance].contactArray.count; i++) {
                    FriendModel* contact = [ContactHelper shareInstance].contactArray[i];
                    for (int j=0; j<tmpArray.count; j++) {
                        FriendModel* friend = tmpArray[j];
                        if ([contact.phone isEqualToString:friend.phone]) {
                            contact.name = friend.name;
                            contact.userid = friend.userid;
                            contact.followState = friend.followState;
                        }
                    }
                }
                
                self.data = [ContactHelper shareInstance].contactArray;
                self.dataSource = [ContactHelper indexMobileContactVCModel:self.data];
                
                [self.tableView reloadData];
            }
        }];
    }else{
        
    }
}

-(NSMutableArray* )contactPhoneNoSet{
    NSMutableArray* arr = [[NSMutableArray alloc]initWithCapacity:10];
    [[ContactHelper shareInstance].contactArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        FriendModel *model = (FriendModel*)obj;
        [arr addObject:model.phone];
    }];
    return arr;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    [TalkManager removeEmptyConversation];
    if (![[ContactHelper shareInstance] authorizationed]) {
        [LcwlBlankPageView addBlankPageView:LcwlBlankPageMobileContactVCType withSuperView:self.view touchedBlock:^{
            NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            
            if([[UIApplication sharedApplication] canOpenURL:url]) {
                NSURL*url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
                [[UIApplication sharedApplication] openURL:url];
            }
        }];
    }else{
        [LcwlBlankPageView removeFromSuperView:self.view];
    }
}

//消息列表
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.searchView;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.sectionIndexColor = [UIColor lightGrayColor];
        
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor whiteColor];
        AdjustTableBehavior(_tableView);
    }
    return _tableView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
        [view style:RowStyleSubtitle];
        
        [view setShowArrow:NO];
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        
    }
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];    
    NSArray* tmpArray = [_dataSource objectAtIndex:indexPath.section];
    __block FriendModel* model = [tmpArray objectAtIndex:indexPath.row];
    if ([StringUtil isEmpty:model.userid]) {
        [tmpView style:RowStyleButton];
        tmpView.rightBtnClickCallback = ^(UIView* view){
            UIView* tmp = view.superview.superview;
            NSIndexPath *indexPath = [self.tableView indexPathForCell:tmp];
            FriendModel* model = [[self.dataSource objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
            [self sendSysMessage:model];
        };
        [tmpView reloadLogo:@"avatar_default" name:model.addressListName desc:[NSString stringWithFormat:@"%@",model.phone]];
    }else if(model.followState == 0){
//        NSString* key = [NSString stringWithFormat:@"addfriend%@",model.userid];
//        NSString* value = [[NSUserDefaults standardUserDefaults]objectForKey:key];
//        if ([StringUtil isEmpty:value]) {
            [tmpView style:RowStyleButton];
            [tmpView reloadLogo:model.avatar name:model.addressListName desc:[NSString stringWithFormat:@"%@",model.name] btntitle:@"添加"];
            tmpView.rightBtnClickCallback = ^(UIView * view){
                [self addFriend:model];
            };
//        }else{
//            [tmpView style:RowStyleRightLabel];
//            NSAttributedString* addressListName = [[NSAttributedString alloc]initWithString:model.addressListName];
//            NSAttributedString* name = [[NSAttributedString alloc]initWithString:model.name];
//            [tmpView reloadLogo:model.avatar attrName:addressListName attrDesc:name rightTip:@"已添加"];
//        }
    
    }
    else{
        [tmpView style:RowStyleSubtitle];
         NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/60/h/60",model.avatar];
        [tmpView reloadLogo:avatar name:model.name desc:[NSString stringWithFormat:@"%@(%@)",model.addressListName,model.phone]];
        
    }
    return cell;
}

-(void)addFriend:(FriendModel* )friend{
    [FriendsManager addFriend:@{@"add_acce_id":friend.userid,@"add_type":@"1"} completion:^(id object, NSString *error) {
        if (object) {
            NSDictionary* dict = (NSDictionary*)object;
            NSInteger status = [dict[@"status"]integerValue];
            friend.followState = status;
            if (status == MoRelationshipTypeStranger) {
                NSInteger isverify = [dict[@"isverify"]integerValue];
                if (isverify == 1) {
                    [MXRouter openURL:@"lcwl://VerifyFriendVC" parameters:@{@"model":friend}];
                }
            }else if(status == MoRelationshipTypeMyFriend){
                [[LcwlChat shareInstance].chatManager insertFriends:@[friend]];
                //已经是好友了
                NSString* tip = [NSString stringWithFormat:@"您已添加了%@，可以开始聊天了",friend.name];
                [ChatSendHelper sendChat:tip from:friend.userid messageType:kMxmessageTypeCustom type:eConversationTypeChat];
            }
        }
    }];
}

-(void)sendSysMessage:(FriendModel* )friend{
    NSString* tip = @"分享给你9聊APP，下载地址：https://dev1.mifengff.com/ygjqg4。";
    [[ContactHelper shareInstance]sendPhoneSMS:tip vc:self phone:friend.phone];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowHeight;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray* tmpArray = [self.dataSource objectAtIndex:section];
    if (tmpArray.count) {
        return tmpArray.count;
    }else
        return 0;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.dataSource objectAtIndex:indexPath.section];
    NSArray* dataArray = [self.dataSource objectAtIndex:indexPath.section];
    FriendModel* model = [dataArray objectAtIndex:indexPath.row];
    if (model.followState == MoRelationshipTypeNone) {
        return;
//        [MXRouter openURL:@"lcwl://InviteUnFriendVC" parameters:@{@"model":model}];
    }else{
        //个人中心
        [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":model.userid}];
    }
}

#pragma mark - Table view delegate
-(NSString*)sectionTitle:(NSInteger) segIndex section:(NSInteger)section{
    if ([[_dataSource objectAtIndex:section] count] != 0){
        NSString* str = [NSString stringWithFormat:@"%@", [[ALPHA3 substringFromIndex:section] substringToIndex:1]];
        return str;
    }
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString* sectionTitle = [self sectionTitle:0 section:section];
    if (sectionTitle==nil) {
        return nil;
    }
    
    UILabel *label=[[UILabel alloc] init];
    label.frame=CGRectMake(12, (headerHeight-20)/2, 300, 20);
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor blackColor];
    label.font=[UIFont fontWithName:@"Helvetica-Bold" size:14];
    if ([sectionTitle isEqualToString:@"未"]) {
        sectionTitle = @"未注册";
    }
    label.text=sectionTitle;
    
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
    [sectionView setBackgroundColor:[UIColor whiteColor]];
    [sectionView addSubview:label];
    
    return sectionView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSString* sectionTitle = [self sectionTitle:0 section:section];
    if (sectionTitle!=nil) {
        return headerHeight;
    }
    return 0.01;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ALPHA3.length;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray *indices = [NSMutableArray arrayWithObject:UITableViewIndexSearch];
    for (int i = 0; i < ALPHA3.length; i++){
        if ([[_dataSource objectAtIndex:i] count])
            [indices safeAddObj:[[ALPHA3 substringFromIndex:i] substringToIndex:1]];
    }
    return indices;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    self.searchController.searchBar.backgroundImage = self.searchWhiteImage;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    //改变SearchBar 背景颜色
    self.searchController.searchBar.backgroundImage = self.searchGrayImage;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    //改变SearchBar 背景颜色
    self.searchController.searchBar.backgroundImage = self.searchGrayImage;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    for (id obj in [searchBar subviews]) {
        if ([obj isKindOfClass:[UIView class]]) {
            for (id obj2 in [obj subviews]) {
                if ([obj2 isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)obj2;
                    [btn setTitle:@"取消" forState:UIControlStateNormal];
                }
            }
        }
    }
    return YES;
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
}

- (UISearchController *)searchController {
    if (!_searchController) {
        SearchContactVC *resultVC = [[SearchContactVC alloc] init];
        
        _searchController = [[UISearchController alloc]initWithSearchResultsController:resultVC];
        _searchController.searchBar.delegate = self;
        _searchController.searchResultsUpdater = resultVC;
        //_searchController.delegate = self;
        _searchController.view.backgroundColor = [UIColor whiteColor];
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = YES;
        //[_searchController.searchBar sizeToFit];
        //_searchController.searchBar.tintColor = [UIColor blackColor];
        _searchController.searchBar.placeholder =  @"搜索";
        [_searchController.searchBar sizeToFit];
        UIOffset offset = {5.0,0};
        _searchController.searchBar.searchTextPositionAdjustment = offset;
        _searchController.searchBar.backgroundImage = self.searchGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:self.searchWhiteImage forState:UIControlStateNormal];
        _searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
        _searchController.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;//关闭提示
        _searchController.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;//关闭自动首字母大写
    }
    return _searchController;
}

- (AddFriendTopView *)searchView {
    if (!_searchView) {
        _searchView = [AddFriendTopView new];
        _searchView.size = CGSizeMake(SCREEN_WIDTH, [AddFriendTopView viewHeight]);
        _searchView.placeholderLbl.text = @"搜索";
        [_searchView addAction:^(UIView *view) {
            MXRoute(@"SearchContactVC", nil);
        }];
       
    }
    return _searchView;
}

@end
