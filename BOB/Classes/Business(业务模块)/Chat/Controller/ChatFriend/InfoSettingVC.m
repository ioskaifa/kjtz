//
//  MXChatSettingVC.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/4/20.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "InfoSettingVC.h"
#import "MBTitleSwitchCell.h"
#import "FriendsManager.h"
#import "UIActionSheet+MKBlockAdditions.h"
//#import "SetRemarkVC.h"
#import "ReportViewController.h"
#import "ChatForwadVC.h"
#import "TalkManager.h"
#import "MXAlertViewHelper.h"
#import "ChatSendHelper.h"
#import "ChatGroupModel.h"
#import "LcwlChat.h"

static int padding = 15;
typedef void (^click)(void);

@interface InfoSettingVC () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) NSArray *datasource;
@property (nonatomic, strong) ChatGroupModel *group;
@property (nonatomic, strong) UIButton *deleteFriendBtn;

@end

@implementation InfoSettingVC

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.isShowBackButton = YES;
    [self setNavBarTitle:Lang(@"资料设置")];
    if (self.model.followState == MoRelationshipTypeMyFriend) {
        self.datasource = @[@[
                                @{@"type":@"normal",@"title":@"设置备注名",@"block":^(){
                                    [self setUsername];
                                }},
                                @{@"type":@"normal",@"title":@"推荐给好友",@"block":^(){
                                    [self recommend];
                                }},
                              @{@"type":@"switch",@"title":@"加入黑名单"},
//                                @{@"type":@"normal",@"title":@"投诉",@"block":^(){
//                                    [self complain];
//                                }}
                              ],
                            @[
                              @{@"type":@"delete",@"title":@"删除好友"}
                              ]
                            ];
    }else{
        self.datasource = @[@[
                                @{@"type":@"normal",@"title":@"推荐给好友",@"block":^(){
                                    [self recommend];
                                }},
                              @{@"type":@"switch",@"title":@"加入黑名单"},
//                              @{@"type":@"normal",@"title":@"投诉",@"block":^(){
//                                  [self complain];
//                              }}
                              ]
                            ];
    }

    [self initUI];
    [self initData];
    AdjustTableBehavior(self.tableView)
    //init tableView
    self.tableView.tableFooterView = [MXSeparatorLine initHorizontalLineWidth:[UIScreen mainScreen].bounds.size.width orginX:0 orginY:0];
    [self.tableView registerClass:[MBTitleSwitchCell class] forCellReuseIdentifier:@"MBTitleSwitchCell"];

}
-(void)setUsername{
    
    @weakify(self)
    FinishedBlock editRemarkBlock = ^(id data) {
        @strongify(self)
        if ([data isEqualToString:self.model.nick_name]) {
            return;
        }
        NSString *fullUrl = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/frined/setFriendRemark"];
        [self request:fullUrl param:@{@"remark":data,@"friend_id":self.model.userid}
           completion:^(BOOL success, id object, NSString *error) {
            if(success) {
                self.model.remark = data;
                [[LcwlChat shareInstance].chatManager updateFriendInfo:self.model];
                [[LcwlChat shareInstance].chatManager updateRemarkMap:self.model.userid remark:data needCheckExist:NO];
                if(![StringUtil isEmpty:self.group_id]) { //修改完备注刷新聊天背景
                    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"ReloadChatList_%@",self.group_id] object:nil];
                }
            }
        }];
    };
    NSString *defultName = self.model.remark ?: @"";
    if ([StringUtil isEmpty:defultName]) {
        defultName = self.model.nick_name ? : @"";
    }
    [MXRouter openURL:@"lcwl://TextChangeVC" parameters:@{@"titleStr":@"设置备注",
                                                          @"text":defultName,
                                                          @"block":editRemarkBlock,
                                                          @"placeholder":@"请输入备注"}];
}

-(void)recommend{
    ChatForwadVC* forwardVC = [[ChatForwadVC alloc] init];
    forwardVC.messageModel = nil;
    forwardVC.callback = ^(id  _Nonnull sender, MessageModel * _Nonnull message) {
        NSString* forwardMessage = @"";
        EMConversationType type = eConversationTypeChat;
        NSString* chatter = @"";
        UserModel* user = [LcwlChat shareInstance].user;
        if ([sender isKindOfClass:[FriendModel class]]) {
            FriendModel* friend = (FriendModel*)sender;
            chatter = friend.userid;
//            forwardMessage = friend.name;
//            message.fromID = user.userid;
//            message.toID = friend.userid;
//            message.chat_with = friend.userid;
//            message.chatType = eConversationTypeChat;
            forwardMessage = friend.name;
        }else if([sender isKindOfClass:[ChatGroupModel class]]){
            ChatGroupModel* group = (ChatGroupModel*)sender;
            chatter = group.groupId;
//            forwardMessage = group.groupName;
//            message.fromID = user.userid;
//            message.toID = group.groupId;
//            message.chat_with = group.groupId;
//            message.chatType = eConversationTypeGroupChat;
            forwardMessage = group.groupName;
            type = eConversationTypeGroupChat;
        }
        message.messageId = [TalkManager msgId];
        @weakify(self)
        
        [MXAlertViewHelper showAlertViewWithMessage:forwardMessage title:@"确定发送给:" okTitle:@"确定" cancelTitle:@"取消" completion:^(BOOL cancelled, NSInteger buttonIndex) {
            @strongify(self);
            if (buttonIndex == 1) {
                //名片
                [ChatSendHelper sendCardMessage:@{@"attr1":self.model.userid,@"attr2":self.model.head_photo,@"attr3":self.model.user_name} toUsername:chatter messageType:type];
            }
        }];
    };
    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:forwardVC];
    [self.navigationController presentViewController:nav animated:YES completion:^{
        
    }];
}

//投诉
-(void)complain{
     [MXRouter openURL:@"lcwl://ReportViewController" parameters:@{@"complainId":self.model.userid,@"complain_type":@"1"}];
}

-(UIButton*)deleteFriendBtn{
    if (_deleteFriendBtn == nil) {
        _deleteFriendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteFriendBtn.titleLabel.font = [UIFont font14];
        _deleteFriendBtn.layer.borderColor = [UIColor separatorLine].CGColor;
        _deleteFriendBtn.layer.borderWidth = 0.5;
        [_deleteFriendBtn setBackgroundColor:[UIColor whiteColor]];
        [_deleteFriendBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_deleteFriendBtn setTitle:@"删除好友" forState:UIControlStateNormal];
        [_deleteFriendBtn addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteFriendBtn;
}

-(void)deleteAction:(id)sender{
    [UIActionSheet actionSheetWithTitle:@"删除后,将清除与该好友的聊天记录,且自动取消关注对方"
                                message:@""
                 destructiveButtonTitle:@"删除"
                                buttons:nil
                             showInView:self.view
                              onDismiss:^(NSInteger buttonIndex) {
                                  if (buttonIndex == 0) {
                                      //AppDelegate* app = MoApp;
                                      [FriendsManager delFriend:@{@"del_friend_id":self.model.userid} completion:^(BOOL success, NSString *error) {
                                          if (success) {
                                              self.model.followState = MoRelationshipTypeStranger;
                                              [[LcwlChat shareInstance].chatManager deleteConversationByChatter:self.model.userid];
                                              //FriendModel* friend = [self.model toFriendModel];
                                              [[LcwlChat shareInstance].chatManager insertFriends:@[self.model]];
                                              
                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                              if (self.block) {
                                                  self.block(@{@"type":@"deleteFriend",@"value":[@(self.model.followState) description]});
                                              }
                                          }
                                      }];
                                  }
                              } onCancel:^{
                                  
                              }];
  
}

-(void)initData{
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // init user model
    
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.datasource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray* array = self.datasource[section];
    return array.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 20)];
    sectionView.backgroundColor = [UIColor clearColor];
    return sectionView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* array = [self.datasource objectAtIndex:indexPath.section];
    NSDictionary* dict = array[indexPath.row];
    NSString* type = dict[@"type"];
    if ([type isEqualToString:@"normal"]) {
        NSString * identifier= @"normal";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
            cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.font = [UIFont font17];
        }
        cell.backgroundColor = [UIColor whiteColor];
        //自适应图片（大小）
        cell.textLabel.text = dict[@"title"];
        cell.detailTextLabel.text = @"";
       
        return cell;
    }else if([type isEqualToString:@"switch"]){
        MBTitleSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MBTitleSwitchCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        BOOL isBlack = self.model.followState == MoRelationshipTypeBlack;
        [cell configureTitle:dict[@"title"] isOn:isBlack];
        @weakify(self)
        cell.switchAction = ^(NSIndexPath *index, UISwitch *sender) {
            @strongify(self)
            if (sender.on) {
                [self addBlack];
            }else{
                [self removeBlack];
            }
        };
        return cell;
    }else if([type isEqualToString:@"delete"]){
        NSString * identifier= @"delete";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.font = [UIFont font17];
        }
        cell.backgroundColor = [UIColor whiteColor];
        [cell.contentView addSubview:self.deleteFriendBtn];
        [self.deleteFriendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        return cell;
    }
    return nil;
}

-(void)addBlack{
   
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/black/addBlackUser"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{@"friend_ids":self.model.userid ?: @""})
        .finish(^(id data) {
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                FriendModel* friend = self.model;
                friend.followState = MoRelationshipTypeBlack;
                [[LcwlChat shareInstance].chatManager insertFriends:@[friend]];
                //[NotifyHelper showMessageWithMakeText:@"提交成功"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PersonalDetailHeaderUpdate" object:nil];
            }
        }).failure(^(id error){
            
        })
        .execute();
    }];
}

-(void)removeBlack{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/black/deleteBlackUser"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{@"friend_ids":self.model.userid})
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                FriendModel* friend = self.model;
                friend.followState = MoRelationshipTypeMyFriend;
                [[LcwlChat shareInstance].chatManager insertFriends:@[friend]];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PersonalDetailHeaderUpdate" object:nil];
            }
        }).failure(^(id error){
            
        })
        .execute();
    }];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return padding*2;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* dict = [[self.datasource objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
   click block = dict[@"block"];
    if (block) {
        block();
    }
}

#pragma mark - 导航栏返回
-(void)backAction:(id)sender
{
    [super backAction:sender];
}

#pragma mark - UI
- (void)initUI
{
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}


- (UITableView *)tableView{
    if (!_tableView) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView = tableView;
    }
    return _tableView;
}



@end
