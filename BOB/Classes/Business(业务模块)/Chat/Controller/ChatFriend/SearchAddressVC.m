//
//  SearchLocalFriendVC.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/25.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "SearchAddressVC.h"
#import "ChatFriendCell.h"
#import "ChatTitleView.h"
#import "MXSeparatorLine.h"
#import "NSObject+Additions.h"
#import "LSearchBar.h"
#import "UINavigationBar+Alpha.h"
#import "ContactHelper.h"
#import "FriendModel.h"
#import "FriendListView.h"
#import "LcwlChat.h"
#define TipString @"搜一搜:"
static int rowHeight = 60;
static const CGFloat headerHeight = 30;
@interface SearchAddressVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

// tableview
@property (nonatomic,strong) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* searchResult;
//@property (strong, nonatomic) UIImageView *noImageView;
//@property (strong, nonatomic) UILabel *noLabel;

@property (strong, nonatomic) NSString *historyKey;
@property (strong, nonatomic) UILabel *historyLabel;
@property (strong, nonatomic) UIView *historyView;

@property(nonatomic, strong) NSMutableArray *friendData;
@property(nonatomic, strong) NSMutableArray *moyouData;



@end

@implementation SearchAddressVC

//@synthesize noImageView,noLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    _searchResult = [[NSMutableArray alloc]initWithCapacity:10];
    _friendData = [[LcwlChat shareInstance].chatManager friends];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    @weakify(self)
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.edges.equalTo(self.view);
    }];
    
}

//消息列表
- (UITableView *)tableView {
    
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.backgroundView = nil;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
    }
    return _tableView;
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.barTintColor = [UIColor moBackground];
    self.navigationController.navigationBar.tintColor = [UIColor moBackground];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)createSearchHostory
{
    _historyView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"shopping_search"]];
    imgView.frame = CGRectMake(18, 12, 20, 20);
    [_historyView addSubview:imgView];
    _historyLabel = [[UILabel alloc]initWithFrame:CGRectMake(43, 12, SCREEN_WIDTH-45, 20)];
    _historyLabel.font = [UIFont font14];
    _historyLabel.textColor = [UIColor darkGrayColor];
    _historyLabel.text = [NSString stringWithFormat:@"%@:%@",@"上次搜索",_historyKey];
    [_historyView addSubview:_historyLabel];
    UIView *line = [MXSeparatorLine initHorizontalLineWidth:SCREEN_WIDTH orginX:0 orginY:43];
    [_historyView addSubview:line];
    if ([StringUtil isEmpty:_historyKey]) {
        _historyView.hidden = YES;
    }
    [self.view addSubview:_historyView];
}

- (void)didCustomSearchBarStartSearch:(NSString *)text {
    _historyView.hidden = YES;
    if (text.length > 0) {
        [self searchByKeyword:text];
    }
    
}

- (void)showNoDataTips {
    //    noImageView.hidden=NO;
    //    noLabel.hidden=NO;
    
    //    [MXBlankPageView addBlankPageView:MXBlankPageMoChatFriendSearchNoResultType withSuperView:self.view];
}

- (void)hideNoDataTips {
    //    noImageView.hidden=YES;
    //    noLabel.hidden=YES;
    
    //    [MXBlankPageView removeFromSuperView:self.view];
}

// 自动提示搜索
- (void)searchByKeyword:(NSString*)keyword {
    
    //    _historyView.hidden = NO;
    //    _historyKey = [CreatePlist readPlist];
    //    if ([StringUtil isEmpty:_historyKey]) {
    //        _historyView.hidden = YES;
    //    }
    //    _historyLabel.text = [NSString stringWithFormat:@"%@:%@",MXLang(@"Talk_friend_search_tips_47", @"上次搜索"),_historyKey];
    //    NSArray* tmpArray = [self filterDataByType:self.type keyword:keyword];//[[MXChatDBUtil sharedDataBase]selectFriendByKeyword:keyword];
    //    if (tmpArray.count == 0) {
    //        _tableView.hidden = YES ;
    //        [self showNoDataTips ];
    //    }else{
    //        _tableView.hidden = NO;
    //        [self hideNoDataTips ];
    //    }
    //
    //    self.searchResult = [tmpArray mutableCopy] ;
    //    [_tableView reloadData];
    
    
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendModel* model = [_searchResult objectAtIndex:indexPath.row];
    //未注册
    if (model.followState == MoRelationshipTypeNone) {
        static NSString *cellId = @"cellIdentifier1";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellId];
            if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
                [cell setSeparatorInset:UIEdgeInsetsZero];
            }
            
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins:UIEdgeInsetsZero];
            }
            
            FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
            [view style:RowStyleButton];
            view.tag = 101;
            [cell.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(cell.contentView);
            }];
            
        }
        FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
        FriendModel* model = [_searchResult objectAtIndex:indexPath.row];
        NSString* contactName = model.addressListName;
        NSString* phone = model.phones[0];
        
        [tmpView reloadLogo:@"avatar_default" name:contactName desc:[NSString stringWithFormat:@"%@",phone]];
        return cell;
    }else{
        static NSString *cellId = @"cellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellId];
            if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
                [cell setSeparatorInset:UIEdgeInsetsZero];
            }
            
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins:UIEdgeInsetsZero];
            }
            
            FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
            [view style:RowStyleSubtitle];
            [view setShowArrow:NO];
            view.tag = 101;
            [cell.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(cell.contentView);
            }];
            
        }
        FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
        FriendModel* model = [_searchResult objectAtIndex:indexPath.row];
        NSString* name = model.name;
        NSString* phone = model.phones[0];
        NSString* contactName = model.addressListName;
        [tmpView reloadLogo:@"avatar_default" name:name desc:[NSString stringWithFormat:@"%@(%@)",contactName,phone]];
        return cell;
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResult.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (void)dismissView {
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchText = searchController.searchBar.text;
    NSLog(@"正在编辑过程中的改变");
    [_searchResult removeAllObjects];
    [self filterFriendByKeyword:searchText];
    [self.tableView reloadData];
    AdjustTableBehavior(self.tableView)
}


-(void )filterFriendByKeyword:(NSString *)keyword{
    [_searchResult removeAllObjects];
    if ([StringUtil isEmpty:keyword]) {
        return;
    }
    for (int i=0; i<_friendData.count; i++) {
        FriendModel* model = _friendData[i];
        NSString* friendName = [[StringUtil chinasesCharToLetter:model.name] lowercaseString];
        NSString* contactName = [[StringUtil chinasesCharToLetter:model.addressListName] lowercaseString];
        keyword = [keyword lowercaseString];
        if ([friendName rangeOfString:keyword].location != NSNotFound || [contactName rangeOfString:keyword].location != NSNotFound) {
            [_searchResult addObject:model];
        }
    }
}





@end
