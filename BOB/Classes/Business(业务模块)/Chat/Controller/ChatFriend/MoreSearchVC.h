//
//  MoreSearchVC.h
//  Lcwl
//
//  Created by mac on 2018/11/24.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^SearchResultCallback)(id sender, UIViewController* fromVC);

@interface MoreSearchVC : BaseViewController

@property (nonatomic, copy) NSString* keyword;

@end

NS_ASSUME_NONNULL_END
