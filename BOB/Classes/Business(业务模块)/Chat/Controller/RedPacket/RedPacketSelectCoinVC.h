//
//  RedPacketSelectCoinVC.h
//  BOB
//
//  Created by mac on 2020/1/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RedPacketSelectCoinVC : BaseViewController
@property (nonatomic,strong) FinishedBlock selected;
@end

NS_ASSUME_NONNULL_END
