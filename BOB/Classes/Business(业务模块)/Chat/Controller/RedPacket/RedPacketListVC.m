//
//  RedPacketDetailVC.m
//  Lcwl
//
//  Created by mac on 2019/1/6.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "RedPacketListVC.h"
#import "RedPacketManager.h"
#import "RedPacketSenderModel.h"
#import "RedPacketView.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "RedPacketDetailVC.h"
#import "RedPacketHeaderView.h"

@interface RedPacketListVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic , strong) UITableView* tableView;

@property (nonatomic , strong) MXBackButton* backButton;

@property (nonatomic , strong) NSMutableArray* dataSource;

@property (nonatomic , copy)   NSString* opType;

@property (nonatomic , copy)   UIButton* settingBtn;

@property (nonatomic)          NSInteger page;

@property (nonatomic , strong) RedPacketHeaderView* header;

@end

@implementation RedPacketListVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"RedPacketBG"] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.isShowBackButton = YES;
    [self initUI];
    [self initData];
}

-(void)initUI{
    [self.view addSubview:self.tableView];
    AdjustTableBehavior(self.tableView);
    [self.backBut setTitle:@"关闭" forState:UIControlStateNormal];
    [self.backBut setImage:nil forState:UIControlStateNormal];
    [self.backBut setTitleColor:[UIColor moGolden] forState:UIControlStateNormal];
    [self.backBut setTitleColor:[UIColor moGolden] forState:UIControlStateHighlighted];
    [self.backBut setTitle:@"关闭" forState:UIControlStateHighlighted];
    [self.backBut setImage:nil forState:UIControlStateHighlighted];
    self.navigationItem.rightBarButtonItem =  [[UIBarButtonItem alloc] initWithCustomView:[self settingBtn]];

    self.opType = @"1";
    [self refreshNaviTitle];
}

-(void)refreshNaviTitle{
    if ([self.opType isEqualToString:@"1"]) {
        [self setNavBarTitle:@"收到的红包" color:[UIColor moGolden]];
        
    }else if([self.opType isEqualToString:@"2"]){
         [self setNavBarTitle:@"发出的红包" color:[UIColor moGolden]];
    }
    [RedPacketManager getMyRedPacketTotal:@{@"type":[@([self.opType integerValue]-1) description]} completion:^(id object, NSString *error) {
        if (object) {
            NSDictionary* tmpDict = (NSDictionary* )object;
            [self.header reloadData:tmpDict type:self.opType];
        }
    }];
}
-(void)initData{
    self.page = 1;
    self.dataSource = [[NSMutableArray alloc]initWithCapacity:10];
    @weakify(self)
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self)
        NSString* pageStr = [NSString stringWithFormat:@"%ld",++self.page];
        [RedPacketManager getMyRedPacketList:@{@"type":[@([self.opType integerValue]-1) description],@"page":pageStr} completion:^(id array, NSString *error) {
            [self.tableView.mj_footer endRefreshing];
            if (array) {
                NSArray* tmpArray = (NSArray*)array;
                if (tmpArray.count < 10) {
                    self.tableView.mj_footer.hidden = YES;
                }
                [self.dataSource addObjectsFromArray:array];
                [self.tableView reloadData];
            }
        }];
    }];

    self.tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        @strongify(self)
        self.tableView.mj_footer.hidden = NO;
        self.page = 1;
        NSString* pageStr = [NSString stringWithFormat:@"%ld",self.page];
        [RedPacketManager getMyRedPacketList:@{@"type":[@([self.opType integerValue]-1) description],@"page":pageStr} completion:^(id array, NSString *error) {
            [self.tableView.mj_header endRefreshing];
            if (array) {
                NSArray* tmpArray = (NSArray*)array;
                if (tmpArray.count < 10) {
                    self.tableView.mj_footer.hidden = YES;
                }
                [self.dataSource removeAllObjects];
                [self.dataSource addObjectsFromArray:array];
                [self.tableView reloadData];
            }
        }];
    }];
    [self.tableView.mj_header beginRefreshing];
    
}

//右边的button
- (UIButton* )settingBtn{
    if (!_settingBtn) {
        _settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_settingBtn addTarget:self action:@selector(detail:) forControlEvents:UIControlEventTouchUpInside];
        _settingBtn.frame = CGRectMake(0, 0, 23, 20);
        [_settingBtn setImage:[UIImage imageNamed:@"topbar_icon_more_black_normal"] forState:UIControlStateNormal];
        _settingBtn.imageView.tintColor = [UIColor moGolden];
        //[_settingBtn setImage:[UIImage imageNamed:@"topbar_icon_more_white_normal"] forState:UIControlStateHighlighted];
    }
    return _settingBtn;
}

-(void)detail:(id)sender{
    @weakify(self)
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil
                                                                              message:nil
                                                                       preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:Lang(@"取消")
                                                        style:UIAlertActionStyleCancel
                                                      handler:nil]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:Lang(@"收到的红包")
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          @strongify(self)
                                                          self.opType = @"1";
                                                          [self refreshNaviTitle];
                                                          [self.tableView.mj_header beginRefreshing];
                                                      }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:Lang(@"发出的红包")
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          @strongify(self)
                                                          self.opType = @"2";
                                                          [self refreshNaviTitle];
                                                          [self.tableView.mj_header beginRefreshing];
                                                      }]];

    
    [self presentViewController:alertController animated:YES completion:nil];
    
//    @weakify(self)
//    [UIActionSheet actionSheetWithTitle:nil message:nil buttons:@[@"收到的红包",@"发出的红包"] showInView:MoApp.window onDismiss:^(NSInteger buttonIndex) {
//        @strongify(self)
//        if (buttonIndex == 0) {
//            self.opType = @"1";
//            [self refreshNaviTitle];
//            [self.tableView.mj_header beginRefreshing];
//        }else if(buttonIndex == 1){
//            self.opType = @"2";
//            [self refreshNaviTitle];
//            [self.tableView.mj_header beginRefreshing];
//        }
//        
//    } onCancel:nil];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
#pragma mark - Table view delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        RedPacketView* view = [[RedPacketView alloc]initWithFrame:CGRectZero];
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
    }
    RedPacketView* tmpView = (RedPacketView*)[cell.contentView viewWithTag:101];
    RedPacketSenderModel* model = [_dataSource objectAtIndex:indexPath.row];
    [tmpView reloadSenderData:model opType:self.opType];
    return cell;
    
}

-(UIView* )header{
    if (!_header) {
        _header = [[RedPacketHeaderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 320)];
    }
    return _header;
}

- (UITableView*)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.tableHeaderView = self.header;
        //_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, SafeAreaBottom_Height, 0));
        }];
    }
    return _tableView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RedPacketSenderModel * model = [self.dataSource objectAtIndex:indexPath.row];
    RedPacketDetailVC* vc = [[RedPacketDetailVC alloc]init];
    vc.acceType = [model.acceType intValue] ;
    vc.orderId = model.redPacketId;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)backAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
