//
//  SendRedPacketVC.m
//  Lcwl
//
//  Created by mac on 2018/12/24.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "SendRedPacketVC.h"
#import "UITextView+Placeholder.h"
#import "YQPayKeyWordVC.h"
#import "RedPacketManager.h"
#import "RedPacketListVC.h"
#import "XWMoneyTextField.h"
#import "UIImage+Utils.h"
#import "UIView+GeneralView.h"
#import "CoinModel.h"
#import "LcwlChat.h"
//#import "PersonalDetailModel.h"

static int padding = 15;
@interface SendRedPacketVC ()<UITextViewDelegate,XWMoneyTextFieldLimitDelegate>

@property (nonatomic, strong) UIScrollView *scroll;

@property (strong, nonatomic) UIImageView                  *backImageView1;

@property (strong, nonatomic) UIImageView                  *backImageView2;

@property (strong, nonatomic) UIButton                     *settingBtn;
@property (strong, nonatomic) UIButton                     *selectCoinBnt;

@property (strong, nonatomic) UIView                       *backView;

@property (strong, nonatomic) UILabel                      *titleLbl;

@property (strong, nonatomic) UILabel                      *unitLbl;

@property (strong, nonatomic) XWMoneyTextField                  *textField;

@property (strong, nonatomic) UITextView                   *textview;

@property (strong, nonatomic) UILabel                      *moneyLbl;

@property (strong, nonatomic) UIButton                     *redPacketBtn;

@property (strong, nonatomic) UILabel                      *tipLbl;
@property (strong, nonatomic) CoinModel                    *coinModel;

@end

@implementation SendRedPacketVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:Lang(@"发红包")];
    [self initUI];
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//
//    UIImage* image = [[UIImage imageNamed:@"RedPacketBG"] resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
//    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
//    self.navigationController.navigationBar.translucent = NO;
//    [self.navigationController.navigationBar setShadowImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#D65E60"] size:CGSizeMake(1, 1)]];
//}

+ (BOOL)isNumber:(NSString *)strValue
{
    if (strValue == nil || [strValue length] <= 0)
    {
        return NO;
    }
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
    NSString *filtered = [[strValue componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    if (![strValue isEqualToString:filtered])
    {
        return NO;
    }
    return YES;
}


- (void)valueChange:(id)sender{
    UITextField *textField = (UITextField *)sender;
    NSString* text = textField.text;
    if ([self.class isNumber:text] &&
                ![text isEqualToString:@"0"]&&
                ![text isEqualToString:@"0."]&&
                ![text isEqualToString:@"0.0"]&&
                ![text isEqualToString:@"0.00"]&&
                ![text isEqualToString:@"0.000"]&&
                ![text isEqualToString:@"0.0000"]
                ) {
                
                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:text];
    //            [attributeString addAttribute:NSFontAttributeName
    //                                    value:[UIFont font17]
    //                                    range:NSMakeRange(0 , 2)];
                _moneyLbl.attributedText = attributeString;
                
                _redPacketBtn.enabled = YES;
                _redPacketBtn.alpha = 1.0;
            }else{
                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[self limitPlaceHolderString]];
    //            [attributeString addAttribute:NSFontAttributeName
    //                                    value:[UIFont font17]
    //                                    range:NSMakeRange(0 , 2)];
                _moneyLbl.attributedText = attributeString;
                _redPacketBtn.enabled = NO;
                _redPacketBtn.alpha = 0.5;
            }
    
}

-(void)initUI{
    self.isShowBackButton = YES;
//    [self.backBut setTitle:Lang(@"取消") forState:UIControlStateNormal];
//    [self.backBut setImage:nil forState:UIControlStateNormal];
//    [self.backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self.backBut setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//    //[self.backBut setTitle:@"取消" forState:UIControlStateHighlighted];
//    [self.backBut setImage:nil forState:UIControlStateHighlighted];
//    self.navigationItem.rightBarButtonItem =  [[UIBarButtonItem alloc] initWithCustomView:[self settingBtn]];

    [self.view addSubview:self.backView];
    self.backView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [self.backView addSubview:self.scroll];
    [self.scroll addSubview:self.selectCoinBnt];
    [self.scroll addSubview:self.backImageView1];
    [self.scroll addSubview:self.backImageView2];
    
    [self.backImageView1 addSubview:self.titleLbl];
    [self.backImageView1 addSubview:self.unitLbl];
    [self.backImageView1 addSubview:self.textField];
    
    [self.backImageView2 addSubview:self.textview];
    
    [self.backView addSubview:self.moneyLbl];
    
    [self.backView addSubview:self.redPacketBtn];
    
    [self.backView addSubview:self.tipLbl];
    
    [self layout];
}

-(void)layout{
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.backView);
    }];
    
    [self.selectCoinBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(padding);
        make.right.equalTo(self.backView.mas_right).offset(-padding);
        make.top.equalTo(self.backView.mas_top).offset(padding);
        make.height.equalTo(@(54));
        
    }];
    
    [self.backImageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(padding);
        make.right.equalTo(self.backView.mas_right).offset(-padding);
        make.top.equalTo(self.selectCoinBnt.mas_bottom).offset(padding);
        make.height.equalTo(@(54));
        
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backImageView1.mas_centerY);
        make.left.equalTo(self.backImageView1.mas_left).offset(padding);
        make.width.equalTo(@(100));
    }];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLbl.mas_centerY);
        make.right.equalTo(self.unitLbl.mas_left).offset(-padding);
        make.width.equalTo(@(100));
    }];
    
    [self.unitLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLbl.mas_centerY);
        make.right.equalTo(self.backImageView1.mas_right).offset(-padding);
        //make.width.equalTo(@(30));
    }];
    
    [self.backImageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backImageView1.mas_left);
        make.right.equalTo(self.backImageView1.mas_right);
        make.top.equalTo(self.backImageView1.mas_bottom).offset(padding);
        make.height.equalTo(@(70));
    }];
    
    [self.textview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backImageView2).offset(padding);
        make.right.equalTo(self.backImageView2).offset(-padding);
        //make.top.equalTo(self.backImageView2.mas_top).offset(padding);
        //make.bottom.equalTo(self.backImageView2.mas_bottom).offset(-padding);
        make.centerY.equalTo(self.backImageView2);
        make.height.equalTo(@(33));
    }];
    
    [self.moneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backImageView2);
        make.right.equalTo(self.backImageView2);
        make.top.equalTo(self.backImageView2.mas_bottom).offset(60);
    }];
    
    [self.redPacketBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.right.equalTo(@(-15));
        make.height.equalTo(@(38));
        //make.centerX.equalTo(self.backView.mas_centerX);
        make.top.equalTo(self.moneyLbl.mas_bottom).offset(padding);
    }];
    
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(280));
        make.height.equalTo(@(45));
        make.centerX.equalTo(self.backView.mas_centerX);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-5*padding-SafeAreaBottomHeight);
    }];
}

-(void)backAction:(id)sender{
    [self.textField resignFirstResponder];
    [self.textview resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(UIView *)backView{
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.backgroundColor = [UIColor colorWithHexString:@"#EFEFEF"];
    }
    return _backView;
}

-(UIImageView *)backImageView1{
    if (!_backImageView1) {
        _backImageView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
        _backImageView1.backgroundColor = [UIColor whiteColor];
        _backImageView1.userInteractionEnabled = YES;
        _backImageView1.layer.cornerRadius = 5;
    }
    return _backImageView1;
}

-(UIImageView *)backImageView2{
    if (!_backImageView2) {
        _backImageView2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
        _backImageView2.backgroundColor = [UIColor whiteColor];
        _backImageView2.userInteractionEnabled = YES;
        _backImageView2.layer.cornerRadius = 5;
    }
    return _backImageView2;
}

//右边的button
- (UIButton* )settingBtn{
    if (!_settingBtn) {
        _settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_settingBtn addTarget:self action:@selector(detail:) forControlEvents:UIControlEventTouchUpInside];
        _settingBtn.frame = CGRectMake(0, 0, 23, 20);
        [_settingBtn setImage:[UIImage imageNamed:@"topbar_icon_more_black_normal"] forState:UIControlStateNormal];
        [_settingBtn setImage:[UIImage imageNamed:@"topbar_icon_more_black_normal"] forState:UIControlStateHighlighted];
        _settingBtn.imageView.tintColor = [UIColor blackColor];
    }
    return _settingBtn;
}


-(XWMoneyTextField*)textField{
    if (!_textField) {
        _textField = [[XWMoneyTextField alloc] initWithFrame:CGRectZero];
        _textField.font = [UIFont font14];
        _textField.textColor = [UIColor blackColor];
        _textField.backgroundColor = [UIColor whiteColor];
        //_textField.placeholder = @"0.0000";
        _textField.textAlignment = NSTextAlignmentRight;
        _textField.keyboardType = UIKeyboardTypeDecimalPad;
        _textField.limit.delegate = self;
        _textField.limit.max = @"9999999999999.9999999";
    }
    return _textField;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLbl.backgroundColor = [UIColor clearColor];
        _titleLbl.font = [UIFont font14];
        _titleLbl.textAlignment = NSTextAlignmentLeft;
        _titleLbl.textColor = [UIColor blackColor];
        _titleLbl.text = @"总金额";
    }
    
    return _titleLbl;
}

- (UILabel *)unitLbl {
    if (!_unitLbl) {
        _unitLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _unitLbl.backgroundColor = [UIColor clearColor];
        _unitLbl.font = [UIFont font14];
        _unitLbl.textAlignment = NSTextAlignmentLeft;
        _unitLbl.textColor = [UIColor blackColor];
        _unitLbl.text = @"";
    }
    
    return _unitLbl;
}

- (UILabel *)moneyLbl {
    if (!_moneyLbl) {
        _moneyLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _moneyLbl.backgroundColor = [UIColor clearColor];
        _moneyLbl.font = [UIFont boldSystemFontOfSize:55];
        _moneyLbl.textAlignment = NSTextAlignmentCenter;
        _moneyLbl.textColor = [UIColor blackColor];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"0.0000"];
//        [attributeString addAttribute:NSFontAttributeName
//                              value:[UIFont font17]
//                              range:NSMakeRange(0 , 2)];
        _moneyLbl.attributedText = attributeString;
    }
    
    return _moneyLbl;
}

- (UILabel *)tipLbl {
    if (!_tipLbl) {
        _tipLbl = [[UILabel alloc] initWithFrame:CGRectZero];
        _tipLbl.backgroundColor = [UIColor clearColor];
        _tipLbl.font = [UIFont font14];
        _tipLbl.textAlignment = NSTextAlignmentCenter;
        _tipLbl.textColor = [UIColor colorWithHexString:@"#F5ABAD"];
//        _tipLbl.text = @"可直接使用收到的美元发红包";
    }
    
    return _tipLbl;
}

-(UIScrollView *)scroll{
    if (!_scroll) {
        _scroll = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _scroll.scrollEnabled = YES;
    }
    return _scroll;
}


- (UITextView *)textview
{
    if (!_textview) {
        _textview = [[UITextView alloc] initWithFrame:CGRectZero];
        _textview.font = [UIFont font14];
        _textview.delegate = self;
        _textview.placeholder  = @"恭喜发财,大吉大利";
    }
    return _textview;
}

- (UIButton *)redPacketBtn {
    if(!_redPacketBtn) {
        _redPacketBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_redPacketBtn setTitle:Lang(@"塞钱进红包") forState:UIControlStateNormal];
        _redPacketBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _redPacketBtn.backgroundColor = [UIColor colorWithHexString:@"#F35643"];
        [_redPacketBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _redPacketBtn.layer.masksToBounds = YES;
        _redPacketBtn.layer.cornerRadius = 6;
//        _redPacketBtn.layer.shadowOpacity = 0.4;
//        _redPacketBtn.layer.shadowOffset = CGSizeMake(2, 2);
        //_redPacketBtn.layer.shadowRadius = 22;
        _redPacketBtn.enabled = NO;
        _redPacketBtn.alpha = 0.5;
        [_redPacketBtn addTarget:self action:@selector(sendRedPacket:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _redPacketBtn;
}

-(void)sendRedPacket:(id)sender{
    if([self.textField.text doubleValue] < [self.coinModel.min doubleValue]) {
        [NotifyHelper showMessageWithMakeText:[NSString stringWithFormat:@"最小发送红包金额不得小于%.f",[self.coinModel.min doubleValue]]];
        return;
    }
    
    FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:self.userId];
    NSString* tip = self.textField.text;
    @weakify(self)
    [[YQPayKeyWordVC alloc] showInViewController:[[MXRouter sharedInstance] getTopNavigationController]
                                          tiltle:@"请输入支付密码"
                                        subtitle:tip block:^(NSString * password) {
        @strongify(self)
        NSString* tips = self.textview.placeholder;
        tips = [StringUtil isEmpty:self.textview.text]?tips:self.textview.text;
        NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
        [param setValue:(self.textview.text.length > 0 ? self.textview.text : self.textview.placeholder) forKey:@"title"];
        [param setValue:@"0" forKey:@"flg"];
        [param setValue:@"1" forKey:@"num"];
        [param setValue:self.textField.text forKey:@"amount"];
        [param setValue:[@(self.coinModel.type) description] forKey:@"type"];
        [param setValue:friend.system_user_id forKey:@"touid"];
        [param setValue:UDetail.user.chatUser_id forKey:@"send_uid"];
        [param setValue:self.userId forKey:@"send_touid"];
        [param setValue:password forKey:@"paypass"];
        [RedPacketManager sendPacket:param completion:^(BOOL success, NSString *error) {
            if (success) {
                [self dismissViewControllerAnimated:YES completion:^{
                    if (self.callback) {
                        self.callback(@{@"remark":tips,@"orderId":error});
                    }
                }];
            }
        }];
    }];
}

- (void)textViewDidChange:(UITextView *)textView{
  
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

-(void)detail:(id)sender{

    RedPacketListVC * vc = [[RedPacketListVC alloc]init];
    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:^{
        
    }];
}

- (NSString *)limitPlaceHolderString {
    if(self.coinModel.min.length - 2 > 0) {
        NSMutableString *muString = [[NSMutableString alloc] initWithCapacity:1];
        NSMutableString *muPlaceholderString = [[NSMutableString alloc] initWithCapacity:1];
        for(int i = 0;i < self.coinModel.min.length - 2;i++) {
            [muString appendString:@"9"];
            [muPlaceholderString appendString:@"0"];
        }
        return StrF(@"0.%@",muPlaceholderString);
    }
    return nil;
}

- (UIButton *)selectCoinBnt {
    if (!_selectCoinBnt) {
        _selectCoinBnt = [[UIButton alloc] init];
        [_selectCoinBnt setTitle:@"币种" forState:UIControlStateNormal];
        [_selectCoinBnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _selectCoinBnt.titleLabel.font = [UIFont font14];
        [_selectCoinBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_selectCoinBnt addArrowIconWithRightOffset:-15];
        UILabel *detail = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-30, 54)];
        detail.text = @"请选择币种";
        detail.font = [UIFont font14];
        detail.textColor = self.textview.placeholderColor;
        detail.textAlignment = NSTextAlignmentRight;
        [_selectCoinBnt addCustomWithRightOffset:detail offset:-30 action:nil];
        _selectCoinBnt.backgroundColor = [UIColor whiteColor];
        _selectCoinBnt.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
        _selectCoinBnt.clipsToBounds = YES;
        _selectCoinBnt.layer.cornerRadius = 5;
        WeakSelf;
        [_selectCoinBnt addAction:^(UIButton *btn) {
            [MXRouter openURL:@"lcwl://RedPacketSelectCoinVC" parameters:@{@"selected":^(id data){
                if(data) {
                    weakSelf.coinModel = data;
                    detail.text = [NSString stringWithFormat:@"选择币种%@",weakSelf.coinModel.name];
                    weakSelf.unitLbl.text = weakSelf.coinModel.name;
                    
                    if(weakSelf.coinModel.min.length - 2 > 0) {
                        NSMutableString *muString = [[NSMutableString alloc] initWithCapacity:1];
                        for(int i = 0;i < weakSelf.coinModel.min.length - 2;i++) {
                            [muString appendString:@"9"];
                        }
                        weakSelf.textField.placeholder = [weakSelf limitPlaceHolderString];
                        weakSelf.textField.limit.max = StrF(@"9999999999999.%@",muString);
                    }
                }
            }}];
        }];
    }
    return _selectCoinBnt;
}
@end

