//
//  RedPacketDetailVC.h
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "MXChatDefines.h"


NS_ASSUME_NONNULL_BEGIN

@interface RedPacketDetailVC : BaseViewController

@property (nonatomic, strong) NSString* orderId;

@property (nonatomic) EMConversationType acceType;

@end

NS_ASSUME_NONNULL_END
