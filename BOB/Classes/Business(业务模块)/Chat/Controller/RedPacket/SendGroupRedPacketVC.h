//
//  SendGroupRedPacketVC.h
//  Lcwl
//
//  Created by mac on 2018/12/25.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SendGroupRedPacketVC : BaseViewController

@property (nonatomic , strong) FinishedBlock callback;

@property (nonatomic , strong) NSString* userId;

@end

NS_ASSUME_NONNULL_END
