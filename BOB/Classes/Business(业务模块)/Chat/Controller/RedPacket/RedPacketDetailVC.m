//
//  RedPacketDetailVC.m
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "RedPacketDetailVC.h"
#import "RedPacketTopView.h"
#import "RedPacketManager.h"
#import "RedPacketModel.h"
#import "RedPacketUserModel.h"
#import "RedPacketView.h"
#import "RedPacketListVC.h"
static int padding = 15;
@interface RedPacketDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)  UITableView *tb;

@property(nonatomic, strong)  NSMutableArray* dataSource;

@property (nonatomic,strong)  UIView *tipView;

@property (nonatomic,strong)  UILabel *tipLbl;

@property (nonatomic,strong)  RedPacketTopView* topView;

@property (nonatomic,strong)  RedPacketModel *redPacket;

@property (nonatomic,assign)  int page;

@property (nonatomic,strong) UIView *headerView;

@end

@implementation RedPacketDetailVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"RedPacketBG"] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self initData];
}

-(void)initUI{
    self.page = 1;
    self.tb.tableHeaderView = self.headerView;
    
    [self.view addSubview:self.tb];
    AdjustTableBehavior(self.tb);
//    [self.backBut setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
//    [self.backBut setImage:[UIImage imageNamed:@"btn_back_white"] forState:UIControlStateNormal];
//    [self.backBut setImage:[UIImage imageNamed:@"btn_back_white"] forState:UIControlStateHighlighted];
//    [self.backBut setTitle:@"返回" forState:UIControlStateNormal];
//    [self.backBut setTitle:@"返回" forState:UIControlStateHighlighted];
    [self setNavBarRightBtnWithTitle:@"红包记录" andImageName:nil];
    [self.navBarRightBtn setTitleColor:[UIColor moGolden] forState:UIControlStateNormal];
    [self.tb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    //[self setNavBarTitle:@"红包" color:[UIColor whiteColor]];
    self.backBut.imageView.tintColor = [UIColor moGolden];
    [self addRefreshFooter];
}

- (void)navBarRightBtnAction:(id)sender {
    //子类重写该方法
    RedPacketListVC * vc = [[RedPacketListVC alloc]init];
    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:^{
        vc.view.superview.backgroundColor = [UIColor redColor];
    }];
}
-(void)addRefreshFooter{
    @weakify(self)
    self.tb.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self)

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *redPacketId = self.orderId;
            if([self.orderId isKindOfClass:[NSDictionary class]]) {
                redPacketId = [self.orderId valueForKey:@"redPacketId"];
            }
            //NSString *lastId = [[self.dataSource lastObject] valueForKey:@"randomId"] ?: @"";
            [RedPacketManager getRedPacketDetailList:@{@"id":redPacketId ?: @"",@"page":[@(self.page) description],@"type":@"0"} completion:^(id object, NSString *error) {
                self.page ++;
                [self.tb.mj_footer endRefreshing];
                NSArray* array = (NSArray* )object;
                if (array && array.count > 0) {
                    NSMutableArray* tmpArray = [[NSMutableArray alloc]initWithCapacity:10];
                    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSDictionary* dict = (NSDictionary* )obj;
                        RedPacketUserModel* model = [RedPacketUserModel dictionaryToModal:dict];
                        if([self.redPacket.top integerValue] > 0) {
                            model.randomLuck = [self.redPacket.top integerValue] == [model.randomId integerValue] ? @"0" : nil;
                        }
                        [tmpArray addObject:model];
                    }];
                    [self.dataSource addObjectsFromArray:tmpArray];
                    [self.tb reloadData];

                } else {
                    self.tb.footer.hidden = YES;
                }
            }];
        });
    }];
}

-(void)initData{
    _dataSource = [[NSMutableArray alloc]initWithCapacity:10];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @weakify(self)
        NSString* type = [NSString stringWithFormat:@"%ld",self.acceType+1];
        NSString *redPacketId = self.orderId;
        if([self.orderId isKindOfClass:[NSDictionary class]]) {
            redPacketId = [self.orderId valueForKey:@"redPacketId"];
        }
        [RedPacketManager getRedPacketDetail:@{@"id":redPacketId ?: @"",@"acceType":type} completion:^(id object, NSString *error) {
            @strongify(self)
            if (object) {
                NSDictionary* dict = (NSDictionary* )object;
                self.redPacket = [RedPacketModel dictionaryToModal:dict];
                
                if ([StringUtil isEmpty:self.redPacket.randomMoney]) {
                    //self.topView.frame = CGRectMake(0, 0,  self.view.bounds.size.width, 270);
                    //self.tipView.frame= CGRectMake(0, 0,  self.view.bounds.size.width, 40);
                    self.headerView.frame = CGRectMake(0, 0,  self.view.bounds.size.width, 270);
                    self.tb.tableHeaderView = self.headerView;
                }
                [self.topView reloadData:self.redPacket];
                self.tipLbl.text = [NSString stringWithFormat:@"%@个红包共%@%@",self.redPacket.redPacketNum,self.redPacket.redPacketMoney,self.redPacket.type];
                [self.tb reloadData];
            }
        }];
    });
    [self.tb.footer beginRefreshing];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //self.headerView.height = 270 - scrollView.contentOffset.y;
    [self.topView stretchTopBG:scrollView.contentOffset.y];
}

- (UIView *)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  self.view.bounds.size.width, 265)];
        [_headerView addSubview: self.topView];
//        [_headerView addSubview:self.tipView];
    }
    return _headerView;
}

-(RedPacketTopView *)topView{
    if (!_topView) {
        _topView = [[RedPacketTopView alloc] initWithFrame:CGRectMake(0, 0,  self.view.bounds.size.width, 270)];
    }
    return _topView;
}

-(UIView *)tipView{
    if (!_tipView) {
        _tipView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  self.view.bounds.size.width, 60)];
        // datepicker2
        [_tipView addSubview:self.tipLbl];
//        MXSeparatorLine* line = [MXSeparatorLine initHorizontalLineWidth:SCREEN_WIDTH orginX:0 orginY:0];
//        [_tipView addSubview:line];
//        MXSeparatorLine* line1 = [MXSeparatorLine initHorizontalLineWidth:SCREEN_WIDTH orginX:0 orginY:39];
//        [_tipView addSubview:line1];
        @weakify(self)
        [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            @strongify(self)
            make.left.equalTo(self.tipView.mas_left).offset(padding);
            make.right.equalTo(self.tipView.mas_right).offset(-padding);
            make.height.equalTo(@(20));
            make.bottom.equalTo(self.tipView.mas_bottom).offset(-10);
        }];
    }
    return _tipView;
}

-(UILabel* )tipLbl{
    if (!_tipLbl) {
        _tipLbl = [[UILabel alloc]initWithFrame:CGRectZero];
        _tipLbl.text = @"";
        _tipLbl.font = [UIFont font14];
        _tipLbl.textColor = [UIColor lightGrayColor];
    }
    return _tipLbl;
}

//消息列表
- (UITableView *)tb {
    
    if (_tb == nil) {
        _tb = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _tb.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//        _tb.layer.borderColor = [UIColor blackColor].CGColor;
        _tb.dataSource = self;
        _tb.delegate = self;
        _tb.scrollEnabled = YES;
        _tb.sectionIndexBackgroundColor = [UIColor clearColor];
        _tb.sectionIndexTrackingBackgroundColor = [UIColor clearColor];
        _tb.backgroundColor = [UIColor whiteColor];
        _tb.backgroundView = nil;
        //_tb.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tb;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsMake(0, 70, 0, 0)];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        RedPacketView* view = [[RedPacketView alloc]initWithFrame:CGRectZero];
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
    }
    RedPacketView* tmpView = (RedPacketView*)[cell.contentView viewWithTag:101];
    RedPacketUserModel* model = [_dataSource objectAtIndex:indexPath.row];
    [tmpView reloadUserData:model];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.tipView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

@end
