//
//  RedPacketSelectCoinVC.m
//  BOB
//
//  Created by mac on 2020/1/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "RedPacketSelectCoinVC.h"
//#import "NSObject+Asset.h"
#import "CoinModel.h"

static NSString* identifier = @"cell";

@interface RedPacketSelectCoinVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView* tb;

@property (nonatomic,strong) NSArray* data;

@end

@implementation RedPacketSelectCoinVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self initData];
}

-(void)setupUI{
    [self setNavBarTitle:@"选择币种"];
    [self.view addSubview:self.tb];
    [self.tb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    AdjustTableBehavior(_tb);
}

-(void)initData{
    self.data = @[];
    [self request:@"bonus/wallet" param:@{@"coin": @"0"} useMsg:NO useHud:self.view useEncryption:YES completion:^(BOOL success, id object, NSString *error) {
        if (object && [object isKindOfClass:[NSDictionary class]]) {
//            NSArray* dataArray = [CoinModel modelListParseWithArray:object[@"data"]];
//            self.data = [self sortCoinIndex:dataArray];
            [self.tb reloadData];
        }
    }];
}

- (UITableView *)tb {
    if (!_tb) {
        _tb = [[UITableView alloc]initWithFrame:CGRectZero];
        _tb.delegate = self;
        _tb.dataSource = self;
        _tb.tableFooterView = [[UIView alloc] init];
        _tb.backgroundColor = [UIColor clearColor];
    }
    return _tb;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:identifier];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont font12];
    }
    CoinModel* model = self.data[indexPath.section][indexPath.row];
    cell.textLabel.text = model.name;
    return cell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ALPHA.length;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.data safeObjectAtIndex:section] count];
        
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    //更改索引的背景颜色
    tableView.sectionIndexBackgroundColor = [UIColor clearColor];

    //更改索引的文字颜色:
    //tableView.sectionIndexColor = [UIColor orangeColor];
    
    NSMutableArray *indices = [NSMutableArray arrayWithObject:UITableViewIndexSearch];
    for (int i = 0; i < ALPHA.length; i++){
        if ([[self.data safeObjectAtIndex:i] count])
            [indices safeAddObj:[[ALPHA substringFromIndex:i] substringToIndex:1]];
    }
    return indices;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{

    if ([[self.data safeObjectAtIndex:section] count] == 0) return nil;
    if (section == 0) {
        return   @"🌟";
    }else {
        return [NSString stringWithFormat:@"%@", [[ALPHA substringFromIndex:section] substringToIndex:1]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSString *sectionTitle=[self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle==nil) {
        return 0;
    }
    return 30;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (title == UITableViewIndexSearch)
    {
        return -1;
    }
    return [ALPHA rangeOfString:title].location;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle=[self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle==nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label=[[UILabel alloc] init];
    label.frame=CGRectMake(12, 2, 300, 21);
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor moBlueColor];
    label.font=[UIFont fontWithName:@"Helvetica-Bold" size:11];
    label.text=sectionTitle;
    
    // Create header view and add label as a subview
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [sectionView setBackgroundColor:[UIColor colorWithHexString:@"#EFEFEF"]];
    [sectionView addSubview:label];
    sectionView.backgroundColor  = [UIColor clearColor];
    return sectionView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     CoinModel* model = self.data[indexPath.section][indexPath.row];
    !self.selected?:self.selected(model);
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    NSLog(@"dealloc:SelectCointVC");
}

@end
