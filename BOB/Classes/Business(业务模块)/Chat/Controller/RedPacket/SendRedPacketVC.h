//
//  SendRedPacketVC.h
//  Lcwl
//
//  Created by mac on 2018/12/24.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SendRedPacketVC : BaseViewController

@property (nonatomic , strong) FinishedBlock callback;

@property (nonatomic , strong) NSString* userId;

@end

NS_ASSUME_NONNULL_END
