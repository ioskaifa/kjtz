//
//  SearchChatRecordVC.h
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "MXConversation.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^SearchResultCallback)(id sender, UIViewController* fromVC);
typedef void(^CancelBtnClick)() ;


@interface SearchChatRecordVC : BaseViewController
@property (nonatomic, copy) CancelBtnClick  callback;
@property (nonatomic, assign) BOOL hidenChatBnt;
@property (nonatomic, copy) NSString* searchTitle;

@property (nonatomic, weak) MXConversation *con;

@end

NS_ASSUME_NONNULL_END
