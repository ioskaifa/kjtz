//
//  TextChangeVC.m
//  Lcwl
//
//  Created by AlphaGO on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "GroupNameVC.h"
#import "RoomManager.h"
#import "ChatSendHelper.h"
static int padding = 15;
@interface GroupNameVC ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *tipInfoLbl;
@property (weak, nonatomic) IBOutlet UIView *textfieldBack;

@end

@implementation GroupNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNavBarTitle:@"群名称"];
    [self setNavBarRightBtnWithTitle:@"保存" andImageName:nil];
    
    self.textField.placeholder = self.placeholder;
    self.tipInfoLbl.text = self.tipInfo;
    self.textField.text = self.group.groupName ?: @"";
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view).offset(padding);
        make.right.equalTo(self.view).offset(-padding);
        make.height.equalTo(@(60));
    }];
    [self.textfieldBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@(60));
    }];
    [self.tipInfoLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.top.equalTo(self.textField.mas_bottom).offset(5);
    }];
    
    [self.tipInfoLbl becomeFirstResponder];
}

- (void)navBarRightBtnAction:(id)sender {
    NSDictionary* dict = @{@"group_id":self.group.groupId,@"group_name":self.textField.text};
    [RoomManager updateGroupName:dict completion:^(id object, NSString *error) {
        if (object) {
            NSString* tip = [NSString stringWithFormat:@"你将群名称修改为\"%@\"",object];
            [ChatSendHelper sendSgroup:tip from:self.group.groupId messageType:SGROUPTypeUpdate_Base];
            Block_Exec(self.block,self.textField.text);
            [super backAction:nil];
        }
    }];
    
}
@end
