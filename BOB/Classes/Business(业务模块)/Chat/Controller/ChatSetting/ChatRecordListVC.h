//
//  ChatRecordListVC.h
//  Lcwl
//
//  Created by mac on 2018/12/5.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "MXChatDefines.h"
#import "MessageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatRecordListVC : BaseViewController
@property (nonatomic, strong) NSMutableArray* dataSource;
@property (nonatomic, strong) MessageModel* message;
- (instancetype)initWithChatter:(NSString *)chatter conversationType:(EMConversationType)type;
@end

NS_ASSUME_NONNULL_END
