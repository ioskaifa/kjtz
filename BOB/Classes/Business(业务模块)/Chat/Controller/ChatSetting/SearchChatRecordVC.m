//
//  SearchLocalFriendVC.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/25.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "SearchChatRecordVC.h"
#import "ChatFriendCell.h"
#import "ChatTitleView.h"
#import "MXSeparatorLine.h"
#import "NSObject+Additions.h"
#import "LSearchBar.h"
#import "UINavigationBar+Alpha.h"
#import "MXConversation.h"
#import "MXChatDBUtil.h"
#import "MessageModel.h"
#import "ChatListTableCell.h"
#import "NSDate+Category.h"
#import "ChatRecordListVC.h"
#import "LcwlChat.h"

@interface SearchChatRecordVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (nonatomic,strong) UITableView     *tableView;

@property (nonatomic,strong) NSMutableArray  *searchResult;

@property (nonatomic,strong) NSMutableArray  *dataSource;

@property (nonatomic,strong) UIView          *searchView;

@property (nonatomic,strong) LSearchBar      *searchBars;

@property (nonatomic,strong) UIButton        *cancelButton;

@property (nonatomic) int type;

@property (nonatomic,strong) MessageModel        *msg;

@end

@implementation SearchChatRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _searchResult = [[NSMutableArray alloc]initWithCapacity:10];
    NSString* userid = @"";
    if (self.con.conversationType == eConversationTypeChat) {
        FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:self.con.chat_id];
        userid = friend.userid;
    }else if(self.con.conversationType == eConversationTypeGroupChat){
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.con.chat_id];
        userid = group.groupId;
    }
    _dataSource = [[MXChatDBUtil sharedDataBase] selectAllChatHistory:userid];
    _searchResult = [[NSMutableArray alloc]initWithCapacity:10];
    
    [self loadSearchView];
    self.navigationItem.titleView = self.searchView;
      [self initUI];
}

- (void)configCurrentVC {
    
}

- (void)initUI
{
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (UITableView *)tableView{
    if (!_tableView) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        _tableView = tableView;
    }
    return _tableView;
}

- (void)loadSearchView {
    self.searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH
                                                               , 30)];
    self.navigationItem.leftBarButtonItems =@[];
    self.searchView.userInteractionEnabled = YES;
    _searchBars = [[LSearchBar alloc] initWithFrame:CGRectZero];
    _searchBars.delegate = self;
    _searchBars.tintColor = [UIColor themeColor];
    _searchBars.frame = CGRectMake(0, 0, self.searchView.frame.size.width - 65, 30);
    [self.searchView addSubview:self.searchBars];
    _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_searchBars.frame) - 3, CGRectGetMinY(self.searchBars.frame), 55, 30)];
    [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _cancelButton.titleLabel.font = [UIFont systemFontOfSize:18.];
    [_cancelButton addTarget:self action:@selector(cancelButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.searchView addSubview:_cancelButton];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor moBackground];
    self.navigationController.navigationBar.tintColor = [UIColor moBackground];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length > 0) {
        [self searchByKeyword:searchText];
    }
}

// 自动提示搜索
- (void)searchByKeyword:(NSString*)keyword {
    [_searchResult removeAllObjects];
    @weakify(self)
    [self.dataSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @strongify(self)
        MessageModel* message = (MessageModel *)obj;
        if ([message.content rangeOfString:keyword].location != NSNotFound) {
            [self.searchResult addObject:message];
        }
    }];
    [self.tableView reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [ChatListTableCell rowHeight];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSString *cellIdentifier = [ChatListTableCell cellIdentifierForMessageModel:@"cell"];
        MessageModel* messeage = [self.searchResult safeObjectAtIndex:indexPath.row];
        MXConversation* conversation = [[MXConversation alloc]init];
        conversation.conversationType = messeage.chatType;
        conversation.chat_id = messeage.chat_with;
    
        ChatListTableCell *cell = (ChatListTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[ChatListTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:cellIdentifier
                                                  tableView:tableView conversation:conversation
                                                marginRight:0];
            cell.backgroundColor = [UIColor clearColor];
            cell.customContentView.backgroundColor =[UIColor whiteColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
            cell.topLabel.hidden = YES;
            cell.imageviewDisturb.hidden = YES;
        }
        cell.rightButtons = @[];
        NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[messeage.sendTime doubleValue]];
        cell.labelTime.text = [msgDate formattedTime];
        if (messeage.chatType == eConversationTypeGroupChat){
            ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:messeage.chat_with];
            cell.labelName.text = group.groupName;
            cell.textviewContent.text = messeage.content;
            [cell.imageviewAvatar sd_setImageWithURL:[NSURL URLWithString:group.groupAvatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
            
        }else if(messeage.chatType == eConversationTypeChat){
            FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:messeage.chat_with];
            cell.labelName.text = friend.name;
            cell.textviewContent.text = messeage.content;
            [cell.imageviewAvatar sd_setImageWithURL:[NSURL URLWithString:friend.avatar] placeholderImage:[UIImage imageNamed:@"avatar_default"]];
            
            
        }
        return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResult.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _msg = [self.searchResult objectAtIndex:indexPath.row];
    ChatRecordListVC * vc = [[ChatRecordListVC alloc]initWithChatter:_msg.chat_with conversationType:_msg.chatType];
    vc.message = _msg;
    vc.dataSource = _dataSource;
    [self.navigationController pushViewController:vc animated:YES];
    return;
}


- (void)cancelButtonDidClick {
    [self.searchBars resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
//    if (self.callback) {
//        self.callback();
//        [self.searchBars resignFirstResponder];
//    }
}
- (void)dismissView {
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}


@end
