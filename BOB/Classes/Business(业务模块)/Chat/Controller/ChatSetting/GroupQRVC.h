//
//  GroupQRVC.h
//  Lcwl
//
//  Created by mac on 2018/12/19.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "ChatGroupModel.h"
#import "LcwlChat.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupQRVC : BaseViewController

@property (nonatomic , strong) ChatGroupModel* group;

@end

NS_ASSUME_NONNULL_END
