//
//  TextChangeVC.h
//  Lcwl
//
//  Created by AlphaGO on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "ChatGroupModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyGroupNameVC : BaseViewController
@property (strong, nonatomic) ChatGroupModel *group;
@property (copy, nonatomic) NSString *placeholder;
@property (copy, nonatomic) NSString *tipInfo;
@property(nonatomic, copy) FinishedBlock block;
@end

NS_ASSUME_NONNULL_END
