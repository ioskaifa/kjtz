//
//  TextChangeVC.m
//  Lcwl
//
//  Created by AlphaGO on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "MyGroupNameVC.h"
#import "RoomManager.h"
#import "ChatSendHelper.h"
#import <Masonry/Masonry.h>

static int padding = 15;
@interface MyGroupNameVC ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *tipInfoLbl;
@property (weak, nonatomic) IBOutlet UIView *textfieldBack;
@end

@implementation MyGroupNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNavBarTitle:@"我在本群的昵称"];
    [self setNavBarRightBtnWithTitle:@"保存" andImageName:nil];
    
    self.textField.placeholder = self.placeholder;
    self.tipInfoLbl.text = self.tipInfo;
    self.textField.text = self.group.nickname ?: @"";
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view).offset(padding);
        make.right.equalTo(self.view).offset(-padding);
        make.height.equalTo(@(54));
    }];
    [self.textfieldBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@(54));
    }];
    
    [self.tipInfoLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-0);
        make.top.equalTo(self.textField.mas_bottom).offset(10);
    }];
}

- (void)navBarRightBtnAction:(id)sender {
    NSString *text = self.textField.text;
    if ([StringUtil isEmpty:text]) {
        [NotifyHelper showMessageWithMakeText:@"不能为空"];
        return;
    }
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [RoomManager updateGroupNickName:@{@"group_id":self.group.groupId,@"group_nick_name":text} completion:^(id object, NSString *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.group.nickname = self.textField.text;
                Block_Exec(self.block,self.group);
                [super backAction:nil];
            });
        }];
    });
}
@end
