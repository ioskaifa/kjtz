//
//  ChatViewVC.m
//  MoPal_Developer
//
//  Created by aken on 15/3/23.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatRecordListVC.h"
#import "MXRecordView.h"
#import "ChatMessageView.h"
#import "LcwlChat.h"
#import "VoiceConverter.h"
#import "NSDate+Category.h"
#import "Photo.h"
#import "LocationShareVC.h"
#import "MXChatDBUtil.h"
#import "UserModel.h"
#import "NSObject+CapacityAuthorize.h"
#import "TalkManager.h"
#import "PasteboardShowImage.h"
#import "QBImagePicker.h"
#import "ChatViewCell.h"
#import "QBImagePicker.h"
#import "MXLoadingErrorView.h"
#import "MXFixCllocation2DHelper.h"
#import "FriendModel.h"
#import "ChatSendHelper.h"
#import "MXConversation.h"
#import "UIImage+FixOrientation.h"
#import "UIImage+Utility.h"
#import "ALAssetRepresentation+FileDetection.h"
#import "XHMessageTextView.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "MJPhotoBrowser.h"
#import "MJPhoto.h"
#import "MXMoMessageToolbar.h"
#import "NSObject+Additions.h"
#import "MXDeviceMediaManager.h"
#import "MXDownLoadsManager.h"
#import "FBKVOController.h"
#import "ChatViewVM.h"
#import "ChatCacheFileUtil.h"
#import "NSMutableArray+AsynSafe.h"

#if __has_include(<MXChatToolBarSdk/MXChatToolBarSdk.h>)
#import <MXChatToolBarSdk/MXChatToolBarSdk.h>
#else
#import "MXChatToolBarSdk.h"
#endif

#import "MXChatVoice.h"
#import "TalkConfig.h"
#import "ChatForwadVC.h"
#import "MXAlertViewHelper.h"
#import "MXChatDefines.h"


@interface ChatRecordListVC ()<UIGestureRecognizerDelegate,IMXChatManagerDelegate,
UINavigationControllerDelegate, UIImagePickerControllerDelegate,ChatMessageViewDelegate, QBImagePickerControllerDelegate,UIActionSheetDelegate,SendMessageDelegate,IMXChatToolBarDelegate,IMXChatToolBarDataSource,MXPopPreviewViewDelegate> {
    
    // 消息队列
    dispatch_queue_t _messageQueue;
    
    // 消息队列
    dispatch_queue_t _emotionPackageQueue;
    
}
@property (nonatomic) NSInteger chatViewMaginX;

// 数据列表
@property (strong, nonatomic) ChatMessageView    *chatView;

@property (nonatomic, strong) NSMutableArray     *indexPaths;

@property (nonatomic, strong) ChatViewVM        *viewModel;


@end

@implementation ChatRecordListVC


@synthesize chatViewMaginX;


- (instancetype)initWithChatter:(NSString *)chatter conversationType:(EMConversationType)type
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.viewModel = [[ChatViewVM alloc]init];
        self.viewModel.vc = self;
        self.viewModel.conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:chatter];
        if (self.viewModel.conversation == nil) {
            self.viewModel.conversation = [[MXConversation alloc]init];
            self.viewModel.conversation.chat_id = chatter;
            self.viewModel.conversation.conversationType = type;
        }else{
            //读取所有未读信息
            [self.viewModel.conversation markAllMessagesAsRead];
        }
        self.indexPaths = [[NSMutableArray alloc] init];
    }
    
    return self;
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    int index = [_dataSource indexOfObject:self.message];
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.chatView.msgRecordTable scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

#pragma mark - life circle


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:NO];
    [self initUI];
    [self setTopTitle:self.viewModel.conversation];
}

-(MXConversation*)getCurrentConversation{
    return self.viewModel.conversation;
}

- (void)setTopTitle:(MXConversation*)conversation{
    NSString* chatId = conversation.chat_id;
    if (self.viewModel.conversation.conversationType == eConversationTypeGroupChat) {
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:chatId];
        if (![StringUtil isEmpty:group.groupName]) {
            [self.viewModel setTitle:group.groupName openSound:!group.enable_disturb];
        }
        self.title = group.groupName;
    }else {
        FriendModel* user = [[LcwlChat shareInstance].chatManager loadFriendByChatId:chatId];
        NSString* name = user.name;
        if (![StringUtil isEmpty:user.remark]) {
            name = user.remark;
        }
        self.title = name;
    }
}

- (void)initUI {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.isShowBackButton=YES;
    // 添加聊天列表chatView
    [self.view addSubview:self.chatView];
    NSString* userid = @"";
    if (self.viewModel.conversation.conversationType == eConversationTypeChat) {
        FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:self.viewModel.conversation.chat_id];
        userid = friend.userid;
    }else if(self.viewModel.conversation.conversationType == eConversationTypeGroupChat){
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.viewModel.conversation.chat_id];
        userid = group.groupId;
    }
    self.viewModel.recordsArray.array  = _dataSource;
    
    [self.chatView.msgRecordTable reloadData];
   
    
    
}

- (void)dealloc {
    
}

-(void)backAction:(id)sender{
    //[[LcwlChat shareInstance].chatManager removeDelegate:self];
    //清理缓存
    [super backAction:sender];
    NSArray *controllers=self.navigationController.viewControllers;
    
    // 在TalkManager里面做跳转时，已做了setViewControllers
    //    if (controllers && controllers.count > 0) {
    //
    //        UIViewController *vc = controllers[0];
    //        if ([vc isKindOfClass:[DiscoveryHomeVC class]]) {
    //
    //            [TalkManager popBackToHome:self];
    //
    //            [self performSelector:@selector(popToChatListView) withObject:nil afterDelay:0.5];
    //
    //        }else {
    //             [super backAction:sender];
    //        }
    //
    //    }else {
    //         [super backAction:sender];
    //    }
    
}

- (void)popToChatListView {
    
    AppDelegate* app = MoApp;
    //    [app.mainVC switchToChatList];
}

#define SCREEN_Height [[UIScreen mainScreen] bounds].size.height
#define SCREEN_Width  [[UIScreen mainScreen] bounds].size.width


#pragma mark - getter and setter

// 初始化聊天列表chatView
- (ChatMessageView *)chatView
{
    if (_chatView == nil) {
        
        _chatView = [[ChatMessageView alloc] initWithFrame:CGRectMake(0,0 , SCREEN_SIZE.size.width, SCREEN_HEIGHT-StatuBarHeight- NavBarHeight-SafeAreaBottomHeight)];
        
        _chatView.delegate=self;
        _chatView.backgroundColor = [UIColor clearColor];
        _chatView.viewModel = self.viewModel;
    }
    return _chatView;
}




/****************************************************************************Delegate处理***************************************************************/




@end
