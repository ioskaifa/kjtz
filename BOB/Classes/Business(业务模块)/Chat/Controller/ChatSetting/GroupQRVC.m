//
//  GroupQRVC.m
//  Lcwl
//
//  Created by mac on 2018/12/19.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "GroupQRVC.h"
#import "UIImage+Color.h"
#import "MBProgressHUD.h"
#import "UIView+Utils.h"
#import "UIImage+Utils.h"
#import "UINavigationBar+Alpha.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "MXBarReaderHelper.h"
#import "UIImage+Utils.h"

@interface GroupQRVC ()

@property (strong, nonatomic)  UIImageView *logo;
@property (strong, nonatomic)  UILabel *name;
@property (strong, nonatomic)  UIView *whiteBG;
@property (strong, nonatomic)  UIImageView *qrCode;
@property (strong, nonatomic)  UIButton *savePicBtn;

@end

@implementation GroupQRVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor moBackground];

    [self.view addSubview:self.whiteBG];
    [self.view addSubview:self.savePicBtn];

    [self.whiteBG addSubview:self.logo];
    [self.whiteBG addSubview:self.name];
    [self.whiteBG addSubview:self.qrCode];

    [self setNavBarTitle:@"群二维码"];
    self.edgesForExtendedLayout = UIRectEdgeAll;
    
    self.logo.clipsToBounds = YES;
    self.logo.layer.cornerRadius = 4;
    
    [self.navBarRightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   
    [self.backBut setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateNormal];
    self.qrCode.backgroundColor = [UIColor moBackground];
    NSString* userId = [LcwlChat shareInstance].user.chatUser_id;
    NSString* groupId = self.group.groupId;
    NSString* qrcode = [MXBarReaderHelper MakeGroupQRCode:groupId invite:userId];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *qrCode = [UIImage encodeQRImageWithContent:qrcode size:CGSizeMake(SCREEN_HEIGHT, SCREEN_HEIGHT)];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.qrCode.image = qrCode;
        });
    });
    [ChatGroupModel setGroupIconWithURLArray:self.group.groupAvatar image:self.logo];
    self.name.text = self.group.groupName;
    [self layout];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //[self.navigationController.navigationBar barReset];
}

- (void)savePicToLocal {
    UIImage *image = [self.whiteBG convertViewToImageWithCornerRadius:self.whiteBG.layer.cornerRadius];
    if(image != nil) {
        UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        [NotifyHelper showMessageWithMakeText:@"保存失败"];
    } else {
        [NotifyHelper showMessageWithMakeText:@"保存成功"];
    }
}

- (void)layout {
    [self.logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBG).offset(10);
        make.centerX.equalTo(self.whiteBG);
        make.width.height.equalTo(@(60));
    }];
    
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.logo.mas_bottom).offset(5);
        make.left.equalTo(self.whiteBG).offset(10);
        make.right.equalTo(self.whiteBG).offset(-10);
    }];
     
    [self.qrCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.name.mas_bottom).offset(15);
        make.left.equalTo(@(20));
        make.right.equalTo(@(-20));
        make.height.equalTo(self.qrCode.mas_width);
    }];

    [self.whiteBG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.bottom.equalTo(self.qrCode.mas_bottom).offset(20);
        make.centerY.equalTo(self.view).offset(-50);
    }];
    
    [self.savePicBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.whiteBG.mas_bottom).mas_offset(10);
        make.left.right.equalTo(self.whiteBG);
        make.height.equalTo(@(40));
    }];
}

-(UIImageView *)qrCode{
    if (!_qrCode) {
        _qrCode = [[UIImageView alloc]initWithImage: [UIImage imageNamed:@"my_qr_bg"]];
    }
    return _qrCode;
}

-(UIView *)whiteBG{
    if (!_whiteBG) {
        _whiteBG = [[UIView alloc]init];
        _whiteBG.backgroundColor = [UIColor whiteColor];
        _whiteBG.clipsToBounds = YES;
        _whiteBG.layer.cornerRadius = 10;
    }
    return _whiteBG;
}
-(UIImageView *)logo{
    if (!_logo) {
        _logo = [[UIImageView alloc]init];
    }
    return _logo;
}

-(UILabel *)name{
    if (!_name) {
        _name = [[UILabel alloc]init];
        _name.textAlignment = NSTextAlignmentCenter;
        _name.font = [UIFont systemFontOfSize:15];
        _name.textColor = [UIColor blackColor];
    }
    return _name;
}

- (UIButton *)savePicBtn {
    if (!_savePicBtn) {
        _savePicBtn = [UIButton new];
        [_savePicBtn setViewCornerRadius:10];
        _savePicBtn.titleLabel.font = [UIFont font15];
        [_savePicBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_savePicBtn setTitle:@"保存图片" forState:UIControlStateNormal];
        [_savePicBtn setBackgroundColor:[UIColor whiteColor]];
        @weakify(self)
        [_savePicBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self savePicToLocal];
        }];
    }
    return _savePicBtn;
}

@end
