//
//  MXChatSettingVC.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/4/20.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatSettingVC.h"
#import "MXSeparatorLine.h"
#import "MXChatDBUtil.h"
#import "Masonry.h"
#import "MBTitleSwitchCell.h"
#import "TalkManager.h"
#import "MXConversation.h"
#import "NSObject+Additions.h"
#import "FriendListView.h"
#import "ChatSettingHeader.h"
#import "ChatSettingHeaderVM.h"
#import "ContactHelper.h"
#import "ChooseContactVC.h"
#import "MXAlertViewHelper.h"
#import "ReportViewController.h"
#import "SearchMomentsVC.h"
#import "SearchChatRecordVC.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "RoomManager.h"
#import "ChatSendHelper.h"
#import "MXChatDefines.h"
#import "LcwlChat.h"
#import "UIView+SingleLineView.h"

@interface ChatSettingVC () <UITableViewDelegate, UITableViewDataSource>
{
    UserModel* user;
    
}
@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, assign) BOOL isClearCache;
@property (nonatomic, strong) UIImageView *sound;

@property (nonatomic, strong) ChatSettingHeader *header;
@property (nonatomic, strong) ChatSettingHeaderVM *vm;
@end

@implementation ChatSettingVC

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.isShowBackButton = YES;
    [self setNavBarTitle:@"聊天信息"];
    self.titleArray = @[
                        @"消息免打扰",
                        @"置顶聊天",
                        @"查找聊天内容",
                        @"清空聊天记录",
//                        @"投诉"
                        ];
    
    _vm = [[ChatSettingHeaderVM alloc]init];
    
    [self initUI];
    [self initData];
    AdjustTableBehavior(self.tableView)
    //init tableView
    self.tableView.tableFooterView = [MXSeparatorLine initHorizontalLineWidth:[UIScreen mainScreen].bounds.size.width orginX:0 orginY:0];
    [self.tableView registerClass:[MBTitleSwitchCell class] forCellReuseIdentifier:@"MBTitleSwitchCell"];

}
-(void)initData{
    EMConversationType type = self.con.conversationType;
    if (type == eConversationTypeChat) {
        FriendModel* model = [[LcwlChat shareInstance].chatManager loadFriendByChatId:self.con.chat_id];
        _vm.dataArray = [NSMutableArray arrayWithObjects:model, @"+", nil];
        [self.header reloadData:_vm];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // init user model
   [self initData];
}

-(ChatSettingHeader *)header{
    if (!_header) {
        _header = [[ChatSettingHeader alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0) dataArray:_vm];
        @weakify(self)
        _header.block = ^(id  _Nonnull sender) {
            @strongify(self)
            if ([sender isKindOfClass:[NSString class]]) {
                SelectedCompelte block = ^(NSMutableArray *selectArray){
                    __block NSMutableArray* idArray = [[NSMutableArray alloc]initWithCapacity:10];
                    __block NSMutableArray* nameArray = [[NSMutableArray alloc]initWithCapacity:10];
                    [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        FriendModel* friend = (FriendModel*)obj;
                        [idArray addObject:friend.userid];
                        [nameArray addObject:friend.name];
                    }];
                    //之前选中的
                    [self.vm.dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([obj isKindOfClass:[FriendModel class]]) {
                            FriendModel* friend = (FriendModel*)obj;
                            [idArray addObject:friend.userid];
                            [nameArray addObject:friend.name];
                        }
                    }];
//                    NSData *data=[NSJSONSerialization dataWithJSONObject:idArray options:NSJSONWritingPrettyPrinted error:nil];
//                    
//                    NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//                    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//                    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [RoomManager createGroup:@{@"user_ids":[idArray componentsJoinedByString:@","] ?: @""} completion:^(id group, NSString *error) {
                        if (group) {
                            ChatGroupModel* model = (ChatGroupModel*)group;
                            [[LcwlChat shareInstance].chatManager insertGroup:model];
                            NSString* tip = [NSString stringWithFormat:@"你邀请%@加入了群聊",[nameArray componentsJoinedByString:@"、"]];
                            [ChatSendHelper sendSgroup:tip from:model.groupId messageType:SGROUPTypeCreate];
                            UserModel* user = [LcwlChat shareInstance].user;
                            FriendModel* selfModel = [user toFriendModel];
                            selfModel.groupid = model.groupId;
                            selfModel.groupNickname = selfModel.name;
                            [[LcwlChat shareInstance].chatManager insertGroupMember:selfModel];
                            [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                FriendModel* friend = (FriendModel*)obj;
                                friend.groupid = model.groupId;
                                friend.groupNickname = friend.name;
                                [[LcwlChat shareInstance].chatManager insertGroupMember:friend];
                            }];
                            [TalkManager pushChatViewUserId:model.groupId type:eConversationTypeGroupChat];
                            
                        }else{
                            [NotifyHelper showMessageWithMakeText:error];
                        }
                        
                    }] ;
                    
                    
                    
                    
//                    UserModel* tmp = [LcwlChat shareInstance].user;
//                    FriendModel* user = [tmp toFriendModel];
//                    NSMutableArray* avatarArray = [[NSMutableArray alloc]initWithCapacity:10];
//                    //创建群
//                    ChatGroupModel* group = [[ChatGroupModel alloc]init];
//                    group.groupId = [NSString stringWithFormat:@"group%d",rand()];
//                    group.groupName = [NSString stringWithFormat:@"群组%d",rand()];
//                    group.creatorId = user.userid;
//                    group.groupAvatar = @"http://www.xinhuanet.com/politics/leaders/2018-11/30/1123792308_15436052559521n.jpg";
//                    group.groupMemNum = selectArray.count+2;
//                    group.nickname = user.name;
//                    group.roleType = 3;
//                    //插入群组成员
//                    user.groupid = group.groupId;
//                    user.groupNickname = user.name;
//                    [[LcwlChat shareInstance].chatManager insertGroupMember:user];
//                    [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                        FriendModel* friend = (FriendModel*)obj;
//                        friend.groupid = group.groupId;
//                        friend.groupNickname = friend.name;
//                        friend.avatar = friend.avatar;
//                        [[LcwlChat shareInstance].chatManager insertGroupMember:friend];
//                    }];
//                    //之前选中的
//                    [self.vm.dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                        FriendModel* friend = (FriendModel*)obj;
//                        if ([friend isKindOfClass:[FriendModel class]]) {
//                            friend.groupid =  group.groupId;
//                            friend.groupNickname = friend.name;
//                            friend.avatar = friend.avatar;
//                            [[LcwlChat shareInstance].chatManager insertGroupMember:friend];
//                        }
//                    }];
//
//
//                    [[LcwlChat shareInstance].chatManager insertGroup:group];
//
//
//                    MXConversation* con = [[MXConversation alloc]init];
//                    con.conversationType = eConversationTypeGroupChat;
//                    con.chat_id = group.groupId;
//                    [[LcwlChat shareInstance].chatManager createConversationObject:con];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                };
                
                NSMutableArray* array = [[LcwlChat shareInstance].chatManager friends];
                [MXRouter openURL:@"lcwl://ChooseContactVC" parameters:@{@"unChangeArray":self.vm.dataArray,@"dataSource":[ContactHelper indexFriendVCModel:array],@"block":block}];
            }else if([sender isKindOfClass:[FriendModel class]]){
                FriendModel* model = (FriendModel*)sender;
                [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":model.userid}];
            }
        };
    }
    return _header;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section != 0 && indexPath.section != 1) {
        NSString * identifier= @"cell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.accessoryType =  UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.font = [UIFont font17];
        }
        cell.backgroundColor = [UIColor whiteColor];
        //自适应图片（大小）
        cell.textLabel.text = self.titleArray[indexPath.section];
        return cell;
    }else{
        MBTitleSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MBTitleSwitchCell" forIndexPath:indexPath];
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.contentView addCellBottomSingleLine:[UIColor separatorLine] Offset:15];
        cell.index = indexPath;
        MXConversation* con = self.con;
        FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:con.chat_id];
        [cell configureTitle:self.titleArray[indexPath.section] isOn:(indexPath.section == 1 ? friend.enable_top : friend.enable_disturb)];
        @weakify(self)
        cell.switchAction = ^(NSIndexPath *index, UISwitch *sender) {
            @strongify(self)
            if (index.section == 0) {
                [self setDisturb:sender];
            }else if(indexPath.section == 1){
                [self setTop:sender];
            }
        };
        return cell;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 1) {
        return 0.001;
    }
    return 10;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //查找聊天记录
    if (indexPath.section == 2) {
        SearchChatRecordVC* vc = [[SearchChatRecordVC alloc]init];
        UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:vc];
        nav.modalPresentationStyle = UIModalPresentationOverFullScreen;
        vc.con = self.con;
        [self presentViewController:nav animated:YES completion:^{
            
        }];
    }
    //清空聊天记录
    else if (indexPath.section == 3) {
        @weakify(self);
        [UIActionSheet actionSheetWithTitle:@"确定清空聊天记录?" message:nil destructiveButtonTitle:@"确定" buttons:@[] showInView:MoApp.window onDismiss:^(NSInteger buttonIndex) {
            @strongify(self);
            if (buttonIndex == 0) {
                [[MXChatDBUtil sharedDataBase] clearMessages:self.con.chat_id];
                MXConversation* conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:self.con.chat_id];
                [conversation.messages removeAllObjects];
                if (self.callback) {
                    self.callback();
                }
            }
        } onCancel:nil];
    }else if(indexPath.section == 4){
        [MXRouter openURL:@"lcwl://ReportViewController" parameters:@{@"complainId":self.con.chat_id,@"complain_type":@"1"}];
    }
}

#pragma mark - 聊天置顶
- (void)setTop:(UISwitch*)sender
{
    BOOL isTop = sender.isOn;
    FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:self.con.chat_id];
    NSDictionary *param = @{@"friend_id":self.con.chat_id?:@"",
                            @"is_stick":isTop?@"1":@"0"};
    [RoomManager setFriendMessageStick:param completion:nil];
    if(isTop){
        friend.enable_top = isTop;
        friend.lastUpdateTime =  [NSString stringWithFormat:@"%lf",[[NSDate date] timeIntervalSince1970]];
        [[LcwlChat shareInstance].chatManager updateFriendInfo:friend];

    }else{
        friend.enable_top = isTop;
        friend.lastUpdateTime =  @"0";
    }
    [[LcwlChat shareInstance].chatManager updateFriendInfo:friend];
    
}
#pragma mark - 设置消息免打扰
- (void)setDisturb:(UISwitch*)sender
{
    BOOL isDisturb = sender.isOn;
    FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:self.con.chat_id];
    NSDictionary *param = @{@"friend_id":self.con.chat_id?:@"",
                            @"is_free":isDisturb?@"1":@"0"};
    [RoomManager setFriendMessageFree:param completion:nil];
    if(isDisturb){
        friend.enable_disturb = isDisturb;
        friend.lastUpdateTime =  [NSString stringWithFormat:@"%lf",[[NSDate date] timeIntervalSince1970]];
        [[LcwlChat shareInstance].chatManager updateFriendInfo:friend];
        
    }else{
        friend.enable_disturb = isDisturb;
        friend.lastUpdateTime =  @"0";
    }
    [[LcwlChat shareInstance].chatManager updateFriendInfo:friend];
    
}

#pragma mark - 导航栏返回
-(void)backAction:(id)sender
{
    [super backAction:sender];
}

#pragma mark - UI
- (void)initUI
{
    self.sound = [[UIImageView alloc] initWithFrame:CGRectMake(70, 0, 12, 12)];
    self.sound.image = [UIImage imageNamed:@"doNotDisturb"];

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    self.tableView.tableHeaderView = self.header;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 25)];
    return view;
}

- (UITableView *)tableView{
    if (!_tableView) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView = tableView;
    }
    return _tableView;
}



@end
