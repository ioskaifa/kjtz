//
//  ChatRoomSettingVC.h
//  Lcwl
//
//  Created by mac on 2018/12/2.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "MXConversation.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^clearChatRecordCallback)();
@interface ChatRoomSettingVC : BaseViewController

@property (nonatomic, weak) MXConversation *con;

@property (nonatomic, strong) clearChatRecordCallback callback;

@end

NS_ASSUME_NONNULL_END
