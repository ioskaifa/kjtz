//
//  MXChatSettingVC.h
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/4/20.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "BaseViewController.h"
#import "MXConversation.h"

typedef void(^clearChatRecordCallback)();

@interface ChatSettingVC : BaseViewController

@property (nonatomic, weak) MXConversation *con;

@property (nonatomic, strong) clearChatRecordCallback callback;

@end
