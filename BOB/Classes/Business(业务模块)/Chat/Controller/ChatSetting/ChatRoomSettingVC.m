//
//  MXChatSettingVC.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/4/20.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatRoomSettingVC.h"
#import "MXSeparatorLine.h"
#import "MXChatDBUtil.h"
#import "Masonry.h"
#import "MBTitleSwitchCell.h"
#import "TalkManager.h"
#import "MXConversation.h"
#import "NSObject+Additions.h"
#import "FriendListView.h"
#import "ChatSettingHeader.h"
#import "ChatSettingHeaderVM.h"
#import "ContactHelper.h"
#import "ChooseContactVC.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "ReportViewController.h"
#import "SearchChatRecordVC.h"
#import "RoomManager.h"
#import "ChatSendHelper.h"
#import "MXChatDefines.h"
#import "LcwlChat.h"
#import "IMXChatManager.h"
#import "UIView+SingleLineView.h"
#import "ChatRoomSettingCell.h"

@interface ChatRoomSettingVC () <UITableViewDelegate, UITableViewDataSource>
{
    UserModel* user;
    
}
@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, assign) BOOL isClearCache;
@property (nonatomic, strong) UIImageView *sound;
@property (nonatomic, strong) ChatSettingHeader *header;
@property (nonatomic, strong) ChatSettingHeaderVM *vm;
@property (nonatomic, strong) ChatGroupModel *group;
@property (nonatomic, strong) UIButton *deleteAndExitBtn;

@end

@implementation ChatRoomSettingVC

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.isShowBackButton = YES;
    [self setNavBarTitle: @"聊天信息"];
    self.titleArray = @[
                        @"群聊名称",
                        @"群二维码",
                        @"查找聊天内容",
                        @"置顶聊天",
                        @"消息免打扰",
                        @"我在本群的昵称",
                        @"显示群成员昵称",
                        @"清空聊天记录",
//                        @"投诉"
                        ];
    
    _vm = [[ChatSettingHeaderVM alloc]init];
    
    [self initUI];
    [self initData];
    AdjustTableBehavior(self.tableView)
    //init tableView
    self.tableView.tableFooterView = [MXSeparatorLine initHorizontalLineWidth:[UIScreen mainScreen].bounds.size.width orginX:0 orginY:0];
    [self.tableView registerClass:[MBTitleSwitchCell class] forCellReuseIdentifier:@"MBTitleSwitchCell"];
    
    UIView* footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 88)];
    [footView addSubview:self.deleteAndExitBtn];
    [self.deleteAndExitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(footView.mas_left).offset(0);
        make.right.equalTo(footView.mas_right).offset(0);
        make.centerY.equalTo(footView.mas_centerY);
        make.height.equalTo(@(44));
    }];
    self.tableView.tableFooterView = footView;
    
}

-(UIButton*)deleteAndExitBtn{
    if (_deleteAndExitBtn == nil) {
        _deleteAndExitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteAndExitBtn.titleLabel.font = [UIFont font14];
        [_deleteAndExitBtn setBackgroundColor:[UIColor whiteColor]];
        [_deleteAndExitBtn setTitleColor:[UIColor colorWithHexString:@"#0088FF"] forState:UIControlStateNormal];
        [_deleteAndExitBtn setTitle:@"删除并退出" forState:UIControlStateNormal];
        [_deleteAndExitBtn addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteAndExitBtn;
}

-(void)deleteAction:(id)sender{
     @weakify(self)
     [UIActionSheet actionSheetWithTitle:@"退出后不会通知群聊中其他成员,且不会再接收此群聊消息" message:nil destructiveButtonTitle:@"确定" buttons:@[]     showInView:MoApp.window onDismiss:^(NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            [RoomManager removeMemberOutGroup:@{@"group_id":self.con.chat_id,@"op_type":@"2",@"status":@"0",@"user_id":[LcwlChat shareInstance].user.chatUser_id} completion:^(BOOL success, NSString *error) {
                @strongify(self)
                if (success) {
                    [[LcwlChat shareInstance].chatManager removeGroup:self.group];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }];
            
        }
        
    } onCancel:nil];
}

-(void)initData{
    EMConversationType type = self.con.conversationType;
    self.group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.con.chat_id];
    if (type == eConversationTypeGroupChat) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            @weakify(self)
            [RoomManager getGroupMembers:@{@"group_id":self.group.groupId} completion:^(id array, NSString *error) {
                @strongify(self)
                if (array) {
                    __block NSInteger roleType = 1;
                    __block NSString* groupId = @"";
                    //比对本地好友是否存在备注名
                    NSArray *friedsArr = [[LcwlChat shareInstance].chatManager friends];
                    NSString* userid = [LcwlChat shareInstance].user.chatUser_id;
                    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        FriendModel* model = (FriendModel*)obj;
                        if ([userid isEqualToString:model.userid]) {
                            roleType = model.roleType;
                            groupId = model.groupid;
                        }
                        //判断是否本地好友 是本地好友则要更新备注名
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"userid == '%@'", model.userid)];
                        NSArray *tempArr = [friedsArr filteredArrayUsingPredicate:predicate];
                        if (tempArr.count > 0) {
                            FriendModel *obj = tempArr.firstObject;
                            if (obj.remark) {
                                model.remark = obj.remark;
                            }
                        }
                        [[LcwlChat shareInstance].chatManager insertGroupMember:obj];
                    }];
                     [array addObject:@"+"];
                    if (roleType == 3) {
                        [array addObject:@"-"];
                    }
                    ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:groupId];
                    if (group) {
                        group.roleType = roleType ;
                        [[LcwlChat shareInstance].chatManager insertGroup:group];
                    }
                    self.vm.dataArray = array;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.header reloadData:self.vm];
                        self.tableView.tableHeaderView = self.header;
                        //[self.tableView reloadData];
                        [self.tableView beginUpdates];
                        [self.tableView endUpdates];
                    });
                }
            }];
        });
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // init user model
    
}

#pragma mark - 聊天置顶
- (void)setTop:(UISwitch*)sender
{
    BOOL isTop = sender.isOn;
    NSDictionary *param = @{@"group_id":self.group.groupId?:@"",
                            @"is_stick":isTop?@"1":@"0"};
    [RoomManager setGroupMessageStick:param completion:nil];
    if(isTop){
        self.group.enable_top = isTop;
        self.group.lastUpdateTime =  [NSString stringWithFormat:@"%lf",[[NSDate date] timeIntervalSince1970]];
    }else{
        self.group.enable_top = isTop;
        self.group.lastUpdateTime =  @"0";
    }
    [[LcwlChat shareInstance].chatManager updateGroupTopState:self.group];
    
}
#pragma mark - 设置消息免打扰
- (void)setDisturb:(UISwitch*)sender
{
    BOOL isDisturb = sender.isOn;
    NSDictionary *param = @{@"group_id":self.group.groupId?:@"",
                            @"is_free":isDisturb?@"1":@"0"};
    [RoomManager setGroupMessageFree:param completion:nil];
    if(isDisturb){
        self.group.enable_disturb = isDisturb;
        self.group.lastUpdateTime =  [NSString stringWithFormat:@"%lf",[[NSDate date] timeIntervalSince1970]];
        
    }else{
        self.group.enable_disturb = isDisturb;
        self.group.lastUpdateTime =  @"0";
    }
    [[LcwlChat shareInstance].chatManager updateGroupDisturbState:self.group];
}
#pragma mark - 显示群成员昵称

-(void)setShowNickname:(UISwitch*)sender{
    NSString* key = [NSString stringWithFormat:@"nickname_%@",self.group.groupId];
    if (sender.isOn) {
        [[NSUserDefaults standardUserDefaults] setObject:@(1) forKey:key];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:key];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.header reloadData:self.vm];
}


-(NSRange )getIndex:(NSArray*) data{
    __block NSInteger fromIndex = 0 ;
    __block NSInteger len = 0;
    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[FriendModel class]]) {
            if (fromIndex == 0) {
                NSString* friendId = [LcwlChat shareInstance].user.chatUser_id;
                FriendModel* tmp = (FriendModel* )obj;
                if (![tmp.userid isEqualToString:friendId]) {
                    fromIndex = idx;
                }
            }
            if (fromIndex != 0) {
                len++;
            }
        }
    }];
    return NSMakeRange(fromIndex,len);
}

-(ChatSettingHeader *)header{
    if (!_header) {
        _header = [[ChatSettingHeader alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0) dataArray:_vm];
        
         @weakify(self)
        _header.block = ^(id  _Nonnull sender) {
           @strongify(self)
            if ([sender isKindOfClass:[NSString class]]) {
                if ([sender isEqualToString:@"+"]) {
                    NSMutableArray* array = [[LcwlChat shareInstance].chatManager friends];
                    
//                    NSArray* tmpArray = [self.vm.dataArray subarrayWithRange:[NSMakeRange(2, self.vm.dataArray.count-2)]];
                    NSArray* tmpArray = [self.vm.dataArray subarrayWithRange:[self getIndex:self.vm.dataArray]];
                    //选择完后回调
                    SelectedCompelte block = ^(NSMutableArray *selectArray){
                        NSMutableArray* members = [[NSMutableArray alloc]initWithCapacity:10];
                        [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            FriendModel* friend = (FriendModel*)obj;
                            friend.groupid = self.con.chat_id;
                            friend.groupNickname = friend.name;
                            [members addObject:friend.userid];
                            [[LcwlChat shareInstance].chatManager insertGroupMember:friend];
                        }];
//                        NSData *data=[NSJSONSerialization dataWithJSONObject:members options:NSJSONWritingPrettyPrinted error:nil];
//
//                        NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//
//                        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//
//                        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        //添加成员
                        [RoomManager addFriendInGroup:@{@"user_ids":[members componentsJoinedByString:@","],@"group_id":self.con.chat_id} completion:^(BOOL success, NSString *error) {
                            if (success) {
                                NSMutableArray *nameArray = [[NSMutableArray alloc]initWithCapacity:10];
                                [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    FriendModel* friend = (FriendModel*)obj;
                                    friend.groupid = self.con.chat_id;
                                    friend.groupNickname = friend.name;
                                    [nameArray addObject:friend.name];
                                    [[LcwlChat shareInstance].chatManager insertGroupMember:friend];
                                }];
                                
                                NSString* tip = [NSString stringWithFormat:@"你邀请%@加入了群聊",[nameArray componentsJoinedByString:@"、"]];
                                [ChatSendHelper sendSgroup:tip from:self.con.chat_id messageType:SGROUPTypeAdd_Member];
                                self.group.groupMemNum += selectArray.count;
                                [[LcwlChat shareInstance].chatManager updateGroupMemNum:self.group];
                                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange([self.vm.dataArray indexOfObject:@"+"], selectArray.count)];
                                [self.vm.dataArray insertObjects:selectArray atIndexes:indexSet];
                                //[self.vm.dataArray addObjectsFromArray:selectArray];
                                [self.header reloadData:self.vm];
                                [self.tableView reloadData];
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                            
                        }];
                       
                    };
                    
                    [MXRouter openURL:@"lcwl://ChooseContactVC" parameters:@{@"unChangeArray":tmpArray,@"dataSource":[ContactHelper indexFriendVCModel:array ],@"block":block}];
                }else if(([sender isEqualToString:@"-"])){
                    //选择完后回调
                    SelectedCompelte block = ^(NSMutableArray *selectArray){
                        NSMutableArray* members = [[NSMutableArray alloc]initWithCapacity:10];
                        NSMutableArray* nameArray = [[NSMutableArray alloc]initWithCapacity:10];
                        [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            FriendModel* friend = (FriendModel*)obj;
                            [members addObject:friend.userid];
                            [nameArray addObject:friend.name];
                        }];
//                        NSData *data=[NSJSONSerialization dataWithJSONObject:members options:NSJSONWritingPrettyPrinted error:nil];
//
//                        NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//                        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//
//                        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@" " withString:@""];
                        //删除成员
                        [RoomManager removeMemberOutGroup:@{@"op_type":@"1",@"user_ids":[members componentsJoinedByString:@","],@"group_id":self.con.chat_id,@"status":@"1"} completion:^(BOOL success, NSString *error) {
                            if (success) {
                                [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    FriendModel* friend = (FriendModel*)obj;
                                    [[LcwlChat shareInstance].chatManager removeGroupMember:friend];
                                }];
                                NSString* tip = [NSString stringWithFormat:@"你将%@移除了群聊",[nameArray componentsJoinedByString:@"、"]];
                                [ChatSendHelper sendSgroup:tip from:self.con.chat_id messageType:SGROUPTypeRemove_Member];
                                [self.vm.dataArray removeObjectsInArray:selectArray];
                                self.group.groupMemNum -= selectArray.count;
                                [[LcwlChat shareInstance].chatManager updateGroupMemNum:self.group];
                                [self.header reloadData:self.vm];
                                [self.tableView reloadData];
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                        }];
                    };
                    
//                    NSArray* tmpArray = [self.vm.dataArray subarrayWithRange:NSMakeRange(3, self.vm.dataArray.count-3)];
                    NSArray* tmpArray = [self.vm.dataArray subarrayWithRange:[self getIndex:self.vm.dataArray]];
                   
                    [MXRouter openURL:@"lcwl://ChooseContactVC" parameters:@{@"unChangeArray":@[],@"dataSource":[ContactHelper indexFriendVCModel:tmpArray],@"block":block}];
                }
            }else if([sender isKindOfClass:[FriendModel class]]){
                FriendModel* model = (FriendModel*)sender;
                [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":model.userid,@"group_id":self.con.chat_id}];
            }
            
        };
        
    }
    return _header;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3 || indexPath.section == 4 || indexPath.section == 6) {
        MBTitleSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MBTitleSwitchCell" forIndexPath:indexPath];
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.index = indexPath;
        if (indexPath.section == 3) {
            [cell configureTitle:self.titleArray[indexPath.section] isOn:self.group.enable_top];
        }else if(indexPath.section == 4){
            [cell configureTitle:self.titleArray[indexPath.section] isOn:self.group.enable_disturb];
        }else if(indexPath.section == 6){
            NSString* key = [NSString stringWithFormat:@"nickname_%@",self.group.groupId];
            NSInteger show = [[[NSUserDefaults standardUserDefaults]objectForKey:key]boolValue];
            [cell configureTitle:self.titleArray[indexPath.section] isOn:show];
        }
        @weakify(self)
        cell.switchAction = ^(NSIndexPath *index, UISwitch *sender) {
            @strongify(self)
            if (indexPath.section == 3) {
                [self setTop:sender];
            }else if(indexPath.section == 4){
                [self setDisturb:sender];
            }else if(indexPath.section == 6){
                [self setShowNickname:sender];
            }
        };
        
        if(indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 5 || indexPath.section == 6) {
            [cell.contentView addCellBottomSingleLine:[UIColor separatorLine] Offset:15];
        }
        return cell;
       
    }else{
        Class cls = ChatRoomSettingCell.class;
        NSString *identifier= NSStringFromClass(cls);
        ChatRoomSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        NSString *text = self.titleArray[indexPath.section];
        NSString *detailText = @"";
        if (indexPath.section == 0) {
            detailText = self.group.groupName?:@"未命名";
        } else if (indexPath.section == 5){
            detailText = self.group.nickname;
        }
        [cell configureView:text detailText:detailText];
        if(indexPath.section == 0 || indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 5 || indexPath.section == 6) {
            [cell.contentView addCellBottomSingleLine:[UIColor separatorLine] Offset:15];
        }
        return cell;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0 || section == 2 || section == 5 || section == 8)
        return 10;
    return 0.0001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0001;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
//        if (self.group.roleType == 3) {
            @weakify(self)
            [MXRouter openURL:@"lcwl://GroupNameVC" parameters:@{@"group":self.group,@"block":^(NSString* name){
                @strongify(self)
                self.group.groupName = name;
                self.group.is_set_name = @"1";
                [[LcwlChat shareInstance].chatManager insertGroup:self.group];
                [tableView reloadData];
            }}];
//        }else{
//            [NotifyHelper showMessageWithMakeText:@"非群主无法修改群名"];
//        }
    }else if(indexPath.section == 1){
        [MXRouter openURL:@"lcwl://GroupQRVC" parameters:@{@"group":self.group}];
    }else if(indexPath.section == 2){
        SearchChatRecordVC* vc = [[SearchChatRecordVC alloc]init];
        UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:vc];
        nav.modalPresentationStyle = UIModalPresentationOverFullScreen;
        vc.con = self.con;
        [self presentViewController:nav animated:YES completion:^{
            
        }];
    }else if(indexPath.section == 5){
        [MXRouter openURL:@"lcwl://MyGroupNameVC" parameters:@{@"group":self.group,@"tipInfo":@"在这里可以设置你在这个群里的昵称。这个昵称只会在此群内显示。",@"block":^(id data){
            if ([data isKindOfClass:[ChatGroupModel class]]) {
                ChatGroupModel * tmp = (ChatGroupModel* )data;
                self.group = tmp;
                [[LcwlChat shareInstance].chatManager updateGroupNickName:self.group];
                 
                [self.tableView reloadData];
            }
        }}];
    }else if(indexPath.section == 8){
        [MXRouter openURL:@"lcwl://ReportViewController" parameters:@{@"complainId":self.group.groupId,@"complain_type":@"2"}];
    }else if(indexPath.section == 7){
        @weakify(self)
        [UIActionSheet actionSheetWithTitle:@"确定清空聊天记录?" message:nil destructiveButtonTitle:@"确定" buttons:@[] showInView:MoApp.window onDismiss:^(NSInteger buttonIndex) {
            if (buttonIndex == 0) {
                [[MXChatDBUtil sharedDataBase] clearMessages:self.group.groupId];
                MXConversation* conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:self.group.groupId];
                [conversation.messages removeAllObjects];
                if (self.callback) {
                    self.callback();
                }
            }
        } onCancel:nil];
    }
}

- (void)setSoundIcon:(BOOL)sound
{
    self.sound.hidden = sound;
}

#pragma mark - 设置消息免打扰
- (void)doNotDisturb:(UISwitch*)sender
{
    
}

#pragma mark - 获取个人信息
- (void)fetchUserDetailsInfos {
    
}

#pragma mark - 导航栏返回
-(void)backAction:(id)sender
{
    [super backAction:sender];
}

#pragma mark - UI
- (void)initUI
{
    
    self.sound = [[UIImageView alloc] initWithFrame:CGRectMake(70, 0, 12, 12)];
    self.sound.image = [UIImage imageNamed:@"doNotDisturb"];
    //    [titleView addSubview:self.sound];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    
    self.tableView.tableHeaderView = self.header;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
    return view;
}

- (UITableView *)tableView{
    if (!_tableView) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableView.backgroundColor = [UIColor moBackground];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.separatorColor = [UIColor redColor];
        Class cls = ChatRoomSettingCell.class;
        [tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView = tableView;
    }
    return _tableView;
}



@end
