//
//  ServiceMomentsVC.m
//  BOB
//
//  Created by AlphaGo on 2021/1/20.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "ServiceMomentsVC.h"
#import "LcwlChat.h"
#import "TalkManager.h"
#import "MXConversation.h"

@interface ServiceMomentsVC ()

@end

@implementation ServiceMomentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self.conversationListTableView beginUpdates];
//    [self.conversationListTableView setTableHeaderView:nil];
//    [self.conversationListTableView endUpdates];
}

- (NSMutableArray *)loadDataSource
{
    NSMutableArray<MXConversation *> *conversations = (NSMutableArray*)[[LcwlChat shareInstance].chatManager conversations];
    NSMutableArray *serviceConversations = [NSMutableArray arrayWithCapacity:1];
    [conversations enumerateObjectsUsingBlock:^(MXConversation * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj.creatorRole > 0) { //表示客服会话
            [serviceConversations addObject:obj];
        }
    }];
    return (NSMutableArray*)[TalkManager sortConversation:serviceConversations];
}

-(void)refreshDataSource
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        self.dataSource = [self loadDataSource];
        dispatch_async(dispatch_get_main_queue(), ^{
//            if (self.dataSource.count == 0) {
//                [LcwlBlankPageView addBlankPageView:LcwlBlankPageMomentsVCType withSuperView:self.view touchedBlock:^{
//
//                }];
//            }else{
//                [LcwlBlankPageView removeFromSuperView:self.view];
//            }
            [self.conversationListTableView reloadData];
        });
    });
}

- (void)initNavRightItem {
    
}

- (void)refreshHeaderView {
    UIView *header = [UIView new];
    header.frame = CGRectMake(0, 0, SCREEN_WIDTH, 0.001);
    self.conversationListTableView.tableHeaderView = header;
}

- (void)gotoChatVC:(MXConversation *)con {
    [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":con.chat_id ,@"type":@(eConversationTypeChat),@"creatorRole":@(11)}];
}

@end
