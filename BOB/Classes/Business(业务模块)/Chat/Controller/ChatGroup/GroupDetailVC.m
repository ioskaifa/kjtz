//
//  InviteFriendVC.m
//  Lcwl
//
//  Created by mac on 2018/12/19.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "GroupDetailVC.h"
#import "ContactHelper.h"
#import "RoomManager.h"
#import "ChatViewVC.h"
#import "ChatSendHelper.h"
#import "TalkManager.h"
#import "LcwlChat.h"

static int padding = 15;
@interface GroupDetailVC ()

@property (nonatomic, strong) UIImageView *logoImg;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *numLbl;

@property (nonatomic, strong) UIButton *joinBtn;

@property (nonatomic, strong) NSArray* members;

@property (nonatomic) BOOL isJoined;

@property (nonatomic, strong) ChatGroupModel* group;


@end

@implementation GroupDetailVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self initData];
}

-(void)initUI{
    [self.view addSubview:self.logoImg];
    [self.view addSubview:self.nameLbl];
    [self.view addSubview:self.numLbl];
    [self.view addSubview:self.joinBtn];
    MXSeparatorLine* line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self.view addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.nameLbl);
        make.top.equalTo(self.numLbl.mas_bottom).offset(10);
        make.height.equalTo(@(0.5));
    }];
    [self layout];
    [self setNavBarTitle:@"群二维码名片"];
}

-(void)initData{
    @weakify(self)
    [RoomManager scanQrcode:@{@"group_id":self.groupid,@"user_id":[LcwlChat shareInstance].user.chatUser_id} completion:^(id object, NSString *error) {
        @strongify(self)
        ChatGroupModel* model = (ChatGroupModel*)object;
        self.group = model;
        self.isJoined = [error isEqualToString:@"0"]?NO:YES;
        [self reloadGroup:self.group];
    }];
}

-(void)layout{
    [self.logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(padding);
        make.top.equalTo(self.view.mas_top).offset(50);
        make.width.height.equalTo(@(100));
    }];
    [self.nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.logoImg);
        make.right.equalTo(self.view.mas_right).offset(-padding);
        make.top.equalTo(self.logoImg.mas_bottom).offset(padding);
    }];
    [self.numLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.nameLbl);
        make.top.equalTo(self.nameLbl.mas_bottom).offset(padding);;
    }];
    [self.joinBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.nameLbl);
        make.bottom.equalTo(self.view.mas_bottom).offset(-80);
        make.height.equalTo(@(50));
    }];
}
- (UIImageView *)logoImg
{
    if (!_logoImg) {
        _logoImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        _logoImg.image = [UIImage imageNamed:@"avatar_default"];
        _logoImg.layer.cornerRadius = 10;
        _logoImg.layer.masksToBounds = YES;
    }
    
    return _logoImg;
}

-(UILabel* )nameLbl{
    if (!_nameLbl) {
        _nameLbl = [[UILabel alloc]init];
        _nameLbl.text = @"张三";
        _nameLbl.font = [UIFont systemFontOfSize:30];
        _nameLbl.textColor = [UIColor blackColor];
        _nameLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _nameLbl;
}

-(UILabel* )numLbl{
    if (!_numLbl) {
        _numLbl = [[UILabel alloc]init];
        _numLbl.text = @"（共2人）";
        _numLbl.font = [UIFont font17];
        _numLbl.textColor = [UIColor blackColor];
        _numLbl.textAlignment = NSTextAlignmentLeft;
    }
    return _numLbl;
}

-(UIButton *)joinBtn{
    if (!_joinBtn) {
        _joinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _joinBtn.titleLabel.font = [UIFont font17];
        [_joinBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _joinBtn.backgroundColor = [UIColor moBlueColor];
        _joinBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [_joinBtn addTarget:self action:@selector(joinBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_joinBtn setTitle:@"确认" forState:UIControlStateNormal];
        _joinBtn.layer.masksToBounds = YES;
        _joinBtn.layer.cornerRadius = 22;
    }
    return _joinBtn;
}

-(void)joinBtnClick:(id)sender{
    if (self.isJoined) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [TalkManager pushChatViewUserId:self.groupid type:eConversationTypeGroupChat];
        });
    }else{
//        NSData *data=[NSJSONSerialization dataWithJSONObject:@[[LcwlChat shareInstance].user.chatUser_id] options:NSJSONWritingPrettyPrinted error:nil];
//        
//        NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//        
//        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//        
//        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        //添加成员
        
        [RoomManager addFriendInGroup:@{@"user_ids":[LcwlChat shareInstance].user.chatUser_id ?: @"",@"group_id":self.group.groupId,@"invite_code":self.inviteid} completion:^(BOOL success, NSString *error) {
            if (success) {
                self.group.roleType = 1;
                [[LcwlChat shareInstance].chatManager insertGroup:self.group];
                [TalkManager pushChatViewUserId:self.groupid type:eConversationTypeGroupChat];
            }
        }];
    }
}

-(void)reloadGroup:(ChatGroupModel*)group{
    self.nameLbl.text = group.groupName;
    self.numLbl.text = [NSString stringWithFormat:@"(共%ld人)",group.groupMemNum];
//    NSString* groupAvatar = @"";//[ChatGroupModel groupIconWithURLArray:group.groupAvatar];
    
    [ChatGroupModel setGroupIconWithURLArray:group.groupAvatar image:self.logoImg];
//    if (![StringUtil isEmpty:groupAvatar]) {
//        UIImage* image = [UIImage imageWithContentsOfFile:groupAvatar];
//        if (image) {
//            [self.logoImg setImage:image];
//        }else{
//            [self.logoImg setImage:[UIImage imageNamed:groupAvatar]];
//        }
//    }
//    
//    NSString* userId = [LcwlChat shareInstance].user.chatUser_id;
//    if ([userId isEqualToString:group.creatorId]) {
//        self.isJoined = YES;
//    }
    if (_isJoined) {
        [self.joinBtn setTitle:@"进入聊天" forState:UIControlStateNormal];
    }else{
        [self.joinBtn setTitle:@"立即加入该群" forState:UIControlStateNormal];
    }
}
@end
