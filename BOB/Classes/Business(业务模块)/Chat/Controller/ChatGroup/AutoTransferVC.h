//
//  AutoTransferVC.h
//  BOB
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^AutoTransferCallback)(FriendModel *model);

@interface AutoTransferVC : BaseViewController
@property (nonatomic, copy) NSString *roomId;
@property (nonatomic , strong) AutoTransferCallback callback;
@end

NS_ASSUME_NONNULL_END
