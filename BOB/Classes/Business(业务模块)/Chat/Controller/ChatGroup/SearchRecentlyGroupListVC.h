//
//  SearchRecentlyGroupListVC.h
//  Lcwl
//
//  Created by mac on 2018/12/2.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "ChatGroupModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^SearchResultCallback)(id sender, UIViewController* fromVC);
typedef void(^SelectGroup)(ChatGroupModel* model) ;


@interface SearchRecentlyGroupListVC : BaseViewController
@property (nonatomic, copy) SelectGroup  callback;
@property (nonatomic, assign) BOOL hidenChatBnt;
@property (nonatomic, copy) NSString* searchTitle;
@property (nonatomic) int type;
@end

NS_ASSUME_NONNULL_END
