//
//  GroupDetailVC.h
//  Lcwl
//
//  Created by mac on 2018/12/20.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupDetailVC : BaseViewController

@property (nonatomic , strong) NSString* groupid;

@property (nonatomic , strong) NSString* inviteid;


@end

NS_ASSUME_NONNULL_END
