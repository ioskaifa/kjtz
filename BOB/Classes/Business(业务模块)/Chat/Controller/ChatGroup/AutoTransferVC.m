//
//  AutoTransferVC.m
//  BOB
//
//  Created by mac on 2019/7/31.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "AutoTransferVC.h"
#import "AutoTransferStatusCell.h"
#import "AutoTransferTimeCell.h"
#import "AutoTransferTypeCell.h"
#import "AutoTransferHeaderFooterView.h"
#import "ChooseContactVC.h"
#import "LcwlChat.h"
#import "ContactHelper.h"
#import "CCPPickerView.h"
#import "AutoTransferNormalCell.h"
#import "AutoTransferModel.h"
#import "MXChatManager.h"
#import "RoomManager.h"

@interface AutoTransferVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
//@property (nonatomic,strong) NSString *transferTime;

@property (nonatomic,strong) NSMutableArray *selectTargetGroup;
///自动转发配置信息
@property(nonatomic, strong) AutoTransferModel *autoTransferModel;

@property (nonatomic,strong) NSMutableArray *curGroupMembersArray;
@end

@implementation AutoTransferVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavBarTitle:Lang(@"转发设置")];
    self.isShowBackButton = YES;
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(@(0));
    }];
    
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [RoomManager getGroupMembers:@{@"group_id":self.roomId} completion:^(id array, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        self.curGroupMembersArray = array;
    }];
}

- (void)backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)showTimeSelectView {
    
    CCPPickerView *pickerView = [[CCPPickerView alloc] initWithpickerViewWithCenterTitle:Lang(@"选择时间段") andCancel:Lang(@"取消") andSure:Lang(@"确定") completion:^{
        [pickerView setTime:self.autoTransferModel.transferTime];
    }];
    
    [pickerView pickerVIewClickCancelBtnBlock:^{
        
        NSLog(@"取消");
        
    } sureBtClcik:^(NSString *leftString, NSString *rightString, NSString *leftAndRightString) {
        
        NSLog(@"%@=======%@=======%@",leftString,rightString,leftAndRightString);
        self.autoTransferModel.transferTime = leftAndRightString;
        self.autoTransferModel.transferTime = self.autoTransferModel.transferTime;
        [self.tableView reloadData];
    }];
    [pickerView performSelector:@selector(setTime:) withObject:self.autoTransferModel.transferTime afterDelay:0.25];
    //[pickerView setTime:self.autoTransferModel.transferTime];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 1) {
        if(indexPath.row == 1 && self.autoTransferModel.transferTime == nil) {
            return 0;
        } else {
            return 50;
        }
    } else if(indexPath.section == 2) {
        return 115;
    } else if(indexPath.section == 3) {
        return 60;
    } else if(indexPath.section == 4) {
        return 60;
    } else if(indexPath.section == 5) {
        return 60;
    } else {
        return 50;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section <= 2) {
        return 0.001;
    }
    return 38;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section <= 2) {
        return nil;
    }
    
    AutoTransferHeaderFooterView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:[NSString stringWithFormat:@"Section_%d",3]];
    
    NSString *title = nil;
    NSString *detail = nil;
    if(section == 3) {
        title = Lang(@"主讲群");
        detail = Lang(@"清空主讲群");
        header.detailBnt.tag =  10000;
    } else if(section == 4) {
        title = Lang(@"讲师");
        detail = Lang(@"清空讲师");
        header.detailBnt.tag =  10001;
    } else if(section == 5) {
        title = Lang(@"目标群");
        detail = Lang(@"清空目标群");
        header.detailBnt.tag =  10002;
    }
    header.textLbl.text = title;
    [header.detailBnt setTitle:detail forState:UIControlStateNormal];
    
    header.block = ^(id data) {
        UIButton *bnt = data;
        if(bnt.tag == 10000) {
            self.autoTransferModel.mainChatGroupModel = nil;
        } else if(bnt.tag == 10001) {
            [self.autoTransferModel.selectTeachers removeAllObjects];
        } else if(bnt.tag == 10002) {
            [self.autoTransferModel.selectTargetGroup removeAllObjects];
        }
        [self.tableView reloadData];
    };
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [NSString stringWithFormat:@"Section_%ld",indexPath.section];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.textLabel.text = @"";
    
    @weakify(self)
    if(indexPath.section == 0) {
        AutoTransferStatusCell *autoCell = (AutoTransferStatusCell *)cell;
        cell.textLabel.text = Lang(@"转发状态");
        autoCell.block = ^(id data) {
            @strongify(self)
            self.autoTransferModel.transferStatus = [data boolValue];
        };
    } else if(indexPath.section == 1) {
        AutoTransferTimeCell *autoCell = (AutoTransferTimeCell *)cell;
        cell.textLabel.text = Lang(@"转发时间");
        if(indexPath.row == 1) {
            [autoCell hideAllBnt];
            autoCell.textLabel.text = self.autoTransferModel.transferTime ?: Lang(@"请选择转发时间");
            autoCell.textLabel.textColor = [UIColor colorWithHexString:@"#AFB2B3"];
        } else {
            autoCell.block = ^(id data) {
                @strongify(self)
                if([data integerValue] == 0) {
                    self.autoTransferModel.transferTime = nil;
                } else {
                    self.autoTransferModel.transferTime = self.autoTransferModel.transferTime ?: Lang(@"请选择转发时间");
                }
                [self.tableView beginUpdates];
                [self.tableView endUpdates];
            };
        }
    } else if(indexPath.section == 2) {
        AutoTransferTypeCell *autoCell = (AutoTransferTypeCell *)cell;
        autoCell.block = ^(id data) {
            @strongify(self)
            self.autoTransferModel.transferType = [data integerValue];
        };
    } if(indexPath.section == 3) {
        if(self.autoTransferModel.mainChatGroupModel) {
            [(AutoTransferNormalCell *)cell reloadUI:self.autoTransferModel.mainChatGroupModel.groupAvatar title:self.autoTransferModel.mainChatGroupModel.groupName];
            ((AutoTransferNormalCell *)cell).titleLbl.textColor = [UIColor colorWithHexString:@"#1F1F1F"];
        } else {
            [(AutoTransferNormalCell *)cell reloadUI:nil title:nil];
            cell.textLabel.text = Lang(@"请选择主讲群");
            cell.textLabel.textColor = [UIColor colorWithHexString:@"#AFB2B3"];
        }
    } else if(indexPath.section == 4) {
        if(self.autoTransferModel.selectTeachers.count > 0) {
            FriendModel *teacher = [self.autoTransferModel.selectTeachers safeObjectAtIndex:indexPath.row];
            [(AutoTransferNormalCell *)cell reloadUI:teacher.avatar title:teacher.name];
            ((AutoTransferNormalCell *)cell).titleLbl.textColor = [UIColor colorWithHexString:@"#1F1F1F"];
        } else {
            [(AutoTransferNormalCell *)cell reloadUI:nil title:nil];
            cell.textLabel.text = Lang(@"请选择讲师");
            cell.textLabel.textColor = [UIColor colorWithHexString:@"#AFB2B3"];
        }
    } else if(indexPath.section == 5) {
        if(self.autoTransferModel.selectTargetGroup.count > 0) {
            ChatGroupModel *model = [self.autoTransferModel.selectTargetGroup safeObjectAtIndex:indexPath.row];
            
            [(AutoTransferNormalCell *)cell reloadUI:model.groupAvatar title:model.groupName];
            ((AutoTransferNormalCell *)cell).titleLbl.textColor = [UIColor colorWithHexString:@"#1F1F1F"];
        } else {
            [(AutoTransferNormalCell *)cell reloadUI:nil title:nil];
            cell.textLabel.text = Lang(@"请添加目标群");
            cell.textLabel.textColor = [UIColor colorWithHexString:@"#AFB2B3"];
        }
    }
    return cell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 1) {
        return 2;
    } else if(section == 4) {
        return self.autoTransferModel.selectTeachers.count > 0 ? self.autoTransferModel.selectTeachers.count : 1;
    } else if(section == 5) {
        return self.autoTransferModel.selectTargetGroup.count > 0 ? self.autoTransferModel.selectTargetGroup.count : 1;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 6;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 1 && indexPath.row == 1) {
        [self showTimeSelectView];
    } else if(indexPath.section == 3) {
        if(self.autoTransferModel.mainChatGroupModel) {
            return;
        }
        
        [MXRouter openURL:@"lcwl://GroupListVC" parameters:@{@"selectGroupType":@(1),@"callback":^(id object){
            //目标群不能包含主讲群
            if([self targetGroupIsContainMainGroup:self.autoTransferModel.selectTargetGroup mainGroupId:[object valueForKey:@"groupId"]]) {
                return;
            }
            
            self.autoTransferModel.mainChatGroupModel = object;
            [self.tableView reloadData];
        }}];
    } else if(indexPath.section == 4) {
        if(self.autoTransferModel.selectTeachers.count > 0) {
            return;
        }
        
        SelectedCompelte block = ^(NSMutableArray *selectArray){
            [self.autoTransferModel.selectTeachers removeAllObjects];
            [self.autoTransferModel.selectTeachers addObjectsFromArray:selectArray];
            [self.tableView reloadData];
            [self.navigationController popViewControllerAnimated:YES];
        };

        [MXRouter openURL:@"lcwl://ChooseContactVC" parameters:@{@"isSingleSelect":@(NO),@"unChangeArray":@[],@"dataSource":[ContactHelper indexFriendVCModel:self.curGroupMembersArray],@"block":block}];
    } else if(indexPath.section == 5) {
        if(self.autoTransferModel.selectTargetGroup.count > 0) {
            return;
        }
        
        [MXRouter openURL:@"lcwl://GroupListVC" parameters:@{@"selectGroupType":@(2),@"callback":^(id object){
            //目标群不能包含主讲群
            if([self targetGroupIsContainMainGroup:object mainGroupId:self.autoTransferModel.mainChatGroupModel.groupId]) {
                return;
            }
            
            if(self.autoTransferModel.mainChatGroupModel.groupId )
            [self.autoTransferModel.selectTargetGroup removeAllObjects];
            if([object isKindOfClass:[NSArray class]]) {
                [self.autoTransferModel.selectTargetGroup addObjectsFromArray:object];
                [self.tableView reloadData];
            }
        }}];
    }
}

//目标群是否包含主讲群
- (BOOL)targetGroupIsContainMainGroup:(NSArray *)targetGroup mainGroupId:(NSString *)mainGroupId {
    if(targetGroup == nil || targetGroup.count == 0) {
        return NO;
    }
    
    NSArray *targetGroupIds = [targetGroup valueForKeyPath:@"@distinctUnionOfObjects.groupId"];
    if([targetGroupIds containsObject:mainGroupId]) {
        [NotifyHelper showMessageWithMakeText:Lang(@"请不要转发到主讲群")];
        return YES;
    }
    return NO;
}

- (void)sureBntClick:(id)sender {
//    self.autoTransferModel.mainChatGroupId = self.autoTransferModel.mainChatGroupModel.groupId;
//    self.autoTransferModel.teacherId = self.autoTransferModel.teacherModel.userid;
//    self.autoTransferModel.selectTargetGroup = [self.autoTransferModel.selectTargetGroup valueForKeyPath:@"@distinctUnionOfObjects.groupId"];
//
    if(self.autoTransferModel.mainChatGroupModel == nil ||
       self.autoTransferModel.selectTeachers.count == 0 ||
       self.autoTransferModel.selectTargetGroup.count == 0) {
        [NotifyHelper showMessageWithMakeText:Lang(@"主讲群、讲师、目标群不能为空")];
        return;
    }
    
    
    [MXChatManager sharedInstance].autoTransferModel = self.autoTransferModel;
    [NotifyHelper showMessageWithMakeText:Lang(@"设置成功")];
    [self backAction:nil];
}

//消息列表
- (UITableView *)tableView {
    
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_tableView registerNib:[UINib nibWithNibName:@"AutoTransferStatusCell" bundle:nil] forCellReuseIdentifier:[NSString stringWithFormat:@"Section_%d",0]];
        [_tableView registerNib:[UINib nibWithNibName:@"AutoTransferTimeCell" bundle:nil] forCellReuseIdentifier:[NSString stringWithFormat:@"Section_%d",1]];
        [_tableView registerNib:[UINib nibWithNibName:@"AutoTransferTypeCell" bundle:nil] forCellReuseIdentifier:[NSString stringWithFormat:@"Section_%d",2]];
        [_tableView registerNib:[UINib nibWithNibName:@"AutoTransferNormalCell" bundle:nil] forCellReuseIdentifier:[NSString stringWithFormat:@"Section_%d",3]];
        [_tableView registerNib:[UINib nibWithNibName:@"AutoTransferNormalCell" bundle:nil] forCellReuseIdentifier:[NSString stringWithFormat:@"Section_%d",4]];
        [_tableView registerNib:[UINib nibWithNibName:@"AutoTransferNormalCell" bundle:nil] forCellReuseIdentifier:[NSString stringWithFormat:@"Section_%d",5]];
        
        [_tableView registerClass:[AutoTransferHeaderFooterView class] forHeaderFooterViewReuseIdentifier:[NSString stringWithFormat:@"Section_%d",3]];
        [_tableView registerClass:[AutoTransferHeaderFooterView class] forHeaderFooterViewReuseIdentifier:[NSString stringWithFormat:@"Section_%d",4]];
        [_tableView registerClass:[AutoTransferHeaderFooterView class] forHeaderFooterViewReuseIdentifier:[NSString stringWithFormat:@"Section_%d",5]];
        
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 130)];
        footer.backgroundColor = [UIColor whiteColor];
        UIButton *bnt = [[UIButton alloc] initWithFrame:CGRectMake(0, 80, SCREEN_WIDTH, 49)];
        [bnt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [bnt setTitle:Lang(@"确定") forState:UIControlStateNormal];
        [bnt setBackgroundColor:[UIColor themeColor]];
        [bnt addTarget:self action:@selector(sureBntClick:) forControlEvents:UIControlEventTouchUpInside];
        [footer addSubview:bnt];
        _tableView.tableFooterView = footer;
    }
    return _tableView;
}

- (AutoTransferModel *)autoTransferModel {
    if(!_autoTransferModel) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[MXChatManager sharedInstance].autoTransferModel];
        if(data) {
            _autoTransferModel = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        }
    }
    return _autoTransferModel;
}
@end
