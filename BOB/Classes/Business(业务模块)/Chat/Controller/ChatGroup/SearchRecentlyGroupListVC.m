//
//  SearchLocalFriendVC.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/25.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "SearchRecentlyGroupListVC.h"
#import "ChatTitleView.h"
#import "MXSeparatorLine.h"
#import "NSObject+Additions.h"
#import "LSearchBar.h"
#import "UINavigationBar+Alpha.h"
#import "ContactHelper.h"
#import "FriendModel.h"
#import "FriendListView.h"
#import "LcwlChat.h"

static int rowHeight = 60;
@interface SearchRecentlyGroupListVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* searchResult;

@property(nonatomic, strong)  NSArray* dataSource;

@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation SearchRecentlyGroupListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarTitle:@"搜索群聊"];
    _searchResult = [[NSMutableArray alloc]initWithCapacity:10];
    //加载群组信息
    _dataSource =  [[LcwlChat shareInstance].chatManager loadAllGroupListFromDatabase];
    self.view.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.searchTF];
    [layout addSubview:self.tableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchTF becomeFirstResponder];
}

//消息列表
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.backgroundView = nil;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        NSString *searchText = textField.text;
        [_searchResult removeAllObjects];
        [self filterFriendByKeyword:searchText];
        [self.tableView reloadData];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
        view.nameLbl.lineBreakMode = NSLineBreakByTruncatingMiddle;
        view.userInteractionEnabled = NO;
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        
    }
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
    [tmpView setShowArrow:NO];
    ChatGroupModel* model = [_dataSource objectAtIndex:indexPath.row];
    NSString* groupName = [NSString stringWithFormat:@"%@(%d)",model.groupName,(int)model.groupMemNum];
    [tmpView reloadLogo:@"" name:groupName];
    [ChatGroupModel setGroupIconWithURLArray:model.groupAvatar image:tmpView.logoImgView];
 
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResult.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ChatGroupModel* group = [self.dataSource objectAtIndex:indexPath.row];
    if (self.callback) {
        self.callback(group);
    }

}

- (void)filterFriendByKeyword:(NSString *)keyword{
    [_searchResult removeAllObjects];
    if ([StringUtil isEmpty:keyword]) {
        return;
    }
    for (int i=0; i<_dataSource.count; i++) {
        ChatGroupModel* model = _dataSource[i];
        NSString* groupName = [[StringUtil chinasesCharToLetter:model.groupName] lowercaseString];
        keyword = [keyword lowercaseString];
        if ([groupName rangeOfString:keyword].location != NSNotFound ) {
            [_searchResult addObject:model];
        }
    }
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = ({
            UITextField *object = [UITextField new];
            [object addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
            [object setViewCornerRadius:15];
            object.font = [UIFont font15];
            object.backgroundColor = [UIColor whiteColor];
            UIView *leftView = [UIView new];
            leftView.size = CGSizeMake(40, 30);
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"im_search_icon"] forState:UIControlStateNormal];
            [leftBtn sizeToFit];
            leftBtn.x = 10;
            leftBtn.y = 5;
            [leftView addSubview:leftBtn];
            object.leftView = leftView;
            object.returnKeyType = UIReturnKeySearch;
            object.leftViewMode = UITextFieldViewModeAlways;
            object.clearButtonMode = UITextFieldViewModeWhileEditing;
            NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索"] colors:@[[UIColor moBlack]] fonts:@[[UIFont font14]]];
            object.attributedPlaceholder = att;
            object.myLeft = 5;
            object.myWidth = SCREEN_WIDTH - 10;
            object.myHeight = 35;
            object.myTop = 10;
            object.myBottom = 10;
            [object setViewCornerRadius:17];
            object;
        });
    }
    return _searchTF;
}

@end

