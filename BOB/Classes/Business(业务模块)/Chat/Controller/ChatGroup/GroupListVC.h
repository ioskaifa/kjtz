//
//  GroupListVC.h
//  Lcwl
//
//  Created by mac on 2018/12/1.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendListView.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^SelectGroupListCallback)(id groupArray);
@interface GroupListVC : BaseViewController
@property (nonatomic, assign) SelectGroupType selectGroupType;
@property (nonatomic, strong) SelectGroupListCallback callback;
@end

NS_ASSUME_NONNULL_END
