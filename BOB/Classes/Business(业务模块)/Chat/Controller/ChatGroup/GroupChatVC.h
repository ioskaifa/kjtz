//
//  GroupChatVC.h
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^SelectFriendListCallback)(NSArray* friendArray);
@interface GroupChatVC : BaseViewController
@property (nonatomic, copy) SelectFriendListCallback callback;
@end

NS_ASSUME_NONNULL_END
