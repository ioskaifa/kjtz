//
//  SearchLocalFriendVC.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/25.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "GroupListVC.h"
#import "ChatTitleView.h"
#import "MXSeparatorLine.h"
#import "NSObject+Additions.h"
#import "LSearchBar.h"
#import "UINavigationBar+Alpha.h"
#import "ContactHelper.h"
#import "FriendModel.h"
#import "LcwlChat.h"
//#import "CareFansCell.h"
#import "AddFriendListCell.h"
#import "GroupListVC.h"
#define TipString @"搜一搜:"
static int rowHeight = 60;
static const CGFloat headerHeight = 30;
@interface GroupListVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;

@property(nonatomic, strong) NSMutableArray *groupList;

@property(nonatomic, strong) NSMutableArray *selectGroupArr;
@property(nonatomic, strong) UIButton *sureBnt;
@property(nonatomic, strong) UIButton *lastSelectedBnt;
@property(nonatomic, strong) ChatGroupModel* selectGroup;

@end

@implementation GroupListVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"选择一个群";
    [self setNavBarTitle:@"选择一个群" color:[UIColor blackColor]];
    self.selectGroupArr = [NSMutableArray arrayWithCapacity:1];
    
    _groupList =  [[LcwlChat shareInstance].chatManager groupList];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.sureBnt];
    
    @weakify(self)
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.sureBnt.mas_top);
    }];
    
    [self.sureBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(@(0));
        make.bottom.equalTo(@(-safeAreaInsetBottom()));
        make.height.equalTo(@(49));
    }];
}


//消息列表
- (UITableView *)tableView {
    
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.backgroundView = nil;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
    }
    return _tableView;
}

//
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.barTintColor = [UIColor moBackground];
    self.navigationController.navigationBar.tintColor = [UIColor moBackground];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)showNoDataTips {
    //    noImageView.hidden=NO;
    //    noLabel.hidden=NO;
    
    //    [MXBlankPageView addBlankPageView:MXBlankPageMoChatFriendSearchNoResultType withSuperView:self.view];
}

- (void)hideNoDataTips {
    //    noImageView.hidden=YES;
    //    noLabel.hidden=YES;
    
    //    [MXBlankPageView removeFromSuperView:self.view];
}

// 自动提示搜索
- (void)searchByKeyword:(NSString*)keyword {
    
    //    _historyView.hidden = NO;
    //    _historyKey = [CreatePlist readPlist];
    //    if ([StringUtil isEmpty:_historyKey]) {
    //        _historyView.hidden = YES;
    //    }
    //    _historyLabel.text = [NSString stringWithFormat:@"%@:%@",MXLang(@"Talk_friend_search_tips_47", @"上次搜索"),_historyKey];
    //    NSArray* tmpArray = [self filterDataByType:self.type keyword:keyword];//[[MXChatDBUtil sharedDataBase]selectFriendByKeyword:keyword];
    //    if (tmpArray.count == 0) {
    //        _tableView.hidden = YES ;
    //        [self showNoDataTips ];
    //    }else{
    //        _tableView.hidden = NO;
    //        [self hideNoDataTips ];
    //    }
    //
    //    self.searchResult = [tmpArray mutableCopy] ;
    //    [_tableView reloadData];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
        view.selectGroupType = self.selectGroupType;
        view.userInteractionEnabled = NO;
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        
    }
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
    [tmpView setShowArrow:NO];
    ChatGroupModel* group = [self.groupList objectAtIndex:indexPath.row];
    [tmpView reloadLogo:group.groupAvatar name:group.groupName];
    return cell;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.groupList.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
    tmpView.selectBnt.selected = !tmpView.selectBnt.selected;
    if(self.selectGroupType == SelectGroupTypeSingle) {
        self.lastSelectedBnt.selected = NO;
        self.lastSelectedBnt = tmpView.selectBnt;
    }
    
    ChatGroupModel* group = [self.groupList objectAtIndex:indexPath.row];
    if(self.selectGroupType == SelectGroupTypeMulti) {
        if(tmpView.selectBnt.selected) {
            [self.selectGroupArr addObject:group];
        } else {
            [self.selectGroupArr removeObject:group];
        }
    } else {
        self.selectGroup = group;
    }
}

- (void)dismissView {
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)sureBntClick:(id)sender {
    if(self.selectGroupType == SelectGroupTypeMulti) {
        Block_Exec(self.callback,self.selectGroupArr);
    } else {
        Block_Exec(self.callback,self.selectGroup);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIButton *)sureBnt {
    if(!_sureBnt) {
        UIButton *bnt = [[UIButton alloc] initWithFrame:CGRectMake(0, 100, SCREEN_WIDTH, 49)];
        [bnt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [bnt setTitle:Lang(@"确定") forState:UIControlStateNormal];
        [bnt setBackgroundColor:[UIColor themeColor]];
        [bnt addTarget:self action:@selector(sureBntClick:) forControlEvents:UIControlEventTouchUpInside];
        _sureBnt = bnt;
    }
    return _sureBnt;
}


@end

