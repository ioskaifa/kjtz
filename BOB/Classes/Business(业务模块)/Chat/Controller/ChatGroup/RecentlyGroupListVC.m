//
//  FriendVC.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "RecentlyGroupListVC.h"
#import "MXBackButton.h"
#import "MXSeparatorLine.h"
#import "LSearchBar.h"
#import "ChatFriendCell.h"
#import "FriendListView.h"
#import "SearchRecentlyGroupListVC.h"
#import "FriendViewModel.h"
#import "FriendModel.h"
#import "LcwlChat.h"
#import "ContactHelper.h"
#import "ChatViewVC.h"
#import "RoomManager.h"
#import "LcwlBlankPageView.h"
#import "ChatSendHelper.h"
#import "TalkManager.h"
#import "AddFriendTopView.h"
#define personal_details_backbutton_width 60
#define personal_details_backbutton_height 44

static const CGFloat headerHeight = 22;
static const CGFloat rowHeight = 60;

@interface RecentlyGroupListVC ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
@property (nonatomic, strong) MXBackButton                 *backButton;
@property (nonatomic,strong)  UITableView *tb;

@property(nonatomic, strong)  NSArray* dataSource;


/** 搜索框 */
@property (nonatomic, strong) UISearchController *searchController;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchGrayImage;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchWhiteImage;

@property(nonatomic, strong) UINavigationController *searchNavigationController;

@property (nonatomic, strong) AddFriendTopView *searchView;


@end

@implementation RecentlyGroupListVC

-(void)dealloc{
    
    MLog(@"dealloc-->%@",NSStringFromClass([self class]));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.title = @"群聊";
    [self setNavBarTitle:@"群聊"];
    self.view.backgroundColor = [UIColor whiteColor];
   
    //加载群组信息
    _dataSource = [[NSMutableArray alloc]initWithCapacity:10];//[[LcwlChat shareInstance].chatManager loadAllGroupListFromDatabase];
    [self setNavBarRightBtnWithTitle:@"发起群聊" andImageName:nil];
    [self initUI];
    [self initData];
}

-(void)navBarRightBtnAction:(id)sender{
    NSDictionary * dict = @{ @"vString":@"lcwl://GroupChatVC",@"callback":^(NSArray* selectArray){
        UserModel* user = [LcwlChat shareInstance].user;
        id model = selectArray[0];
        if ([model isKindOfClass:[ChatGroupModel class]]) {
            ChatGroupModel* group = (ChatGroupModel*)model;
            MXConversation* con = [[MXConversation alloc]init];
            con.conversationType = eConversationTypeGroupChat;
            con.chat_id = group.groupId;
            [[LcwlChat shareInstance].chatManager createConversationObject:con];
            [TalkManager pushChatViewUserId:group.groupId type:eConversationTypeGroupChat];
             return ;
            
        }else if([model isKindOfClass:[FriendModel class]]){
            if (selectArray.count == 1) {
                FriendModel* friend = (FriendModel*)model;
                MXConversation* con = [[MXConversation alloc]init];
                con.conversationType = eConversationTypeChat;
                con.chat_id = friend.userid;
                [[LcwlChat shareInstance].chatManager createConversationObject:con];
                
                [TalkManager pushChatViewUserId:friend.userid type:eConversationTypeChat];
                return ;
            }
           
        }
        __block NSMutableArray* idArray = [[NSMutableArray alloc]initWithCapacity:10];
        __block NSMutableArray* nameArray = [[NSMutableArray alloc]initWithCapacity:10];
        [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            FriendModel* friend = (FriendModel*)obj;
            [idArray addObject:friend.userid];
            [nameArray addObject:friend.name];
        }];
//        NSData *data=[NSJSONSerialization dataWithJSONObject:idArray options:NSJSONWritingPrettyPrinted error:nil];
//
//        NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        [RoomManager createGroup:@{@"user_ids":[idArray componentsJoinedByString:@","] ?: @""} completion:^(id group, NSString *error) {
            if (group) {
                ChatGroupModel* model = (ChatGroupModel*)group;
                [[LcwlChat shareInstance].chatManager insertGroup:model];
                NSString* tip = [NSString stringWithFormat:@"你邀请%@加入了群聊",[nameArray componentsJoinedByString:@"、"]];
                [ChatSendHelper sendSgroup:tip from:model.groupId messageType:SGROUPTypeCreate];
                FriendModel* selfModel = [user toFriendModel];
                selfModel.groupid = model.groupId;
                selfModel.groupNickname = selfModel.name;
                [[LcwlChat shareInstance].chatManager insertGroupMember:selfModel];
                [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    FriendModel* friend = (FriendModel*)obj;
                    friend.groupid = model.groupId;
                    friend.groupNickname = friend.name;
                    [[LcwlChat shareInstance].chatManager insertGroupMember:friend];
                }];
                [TalkManager pushChatViewUserId:model.groupId type:eConversationTypeGroupChat];
            }else{
                [NotifyHelper showMessageWithMakeText:error];
            }
           
        }];
    }
    };
    NSString* vString = [dict objectForKey:@"vString"];
    [MXRouter openURL:vString
           parameters:dict];
    
}
-(void)initData{
//    _dataSource = [[LcwlChat shareInstance].chatManager groupList];
//    [self.tb reloadData];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
  
        [RoomManager getGroupList:@{} completion:^(id array, NSString *error) {
            dispatch_async(dispatch_get_main_queue(),^(){
                if (array == nil) {
                    [LcwlBlankPageView addBlankPageView:LcwlBlankRecentlyGroupListVCType withSuperView:self.view touchedBlock:^{
                        
                    }];
                    [NotifyHelper showMessageWithMakeText:error];
                }else{
                    NSArray* tmpArray = (NSArray* )array;
                    if (tmpArray.count == 0) {
                        [LcwlBlankPageView addBlankPageView:LcwlBlankRecentlyGroupListVCType withSuperView:self.view touchedBlock:^{
                            
                        }];
                    }
                    self.dataSource = tmpArray;
                }
                [self.tb reloadData];
            });
        }];
    });
}

-(void)initUI{
    if (@available(iOS 11.0, *)) {
        self.tb.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.definesPresentationContext = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view addSubview:self.tb];
    [self.tb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
}

-(void)addFriend:(id)sender{
    UIViewController *vc = [[NSClassFromString(@"AddFriendVC") alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)backAction:(id)sender {
    [self.view endEditing:YES];
    [super backAction:sender];
}

//消息列表
- (UITableView *)tb {
    
    if (_tb == nil) {
        _tb = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tb.delegate = self;
        _tb.dataSource = self;
        _tb.tableHeaderView = self.searchView;
        _tb.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tb.tableFooterView = [[UIView alloc] init];
        _tb.backgroundColor = [UIColor whiteColor];
          AdjustTableBehavior(_tb);
    }
    return _tb;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
        view.nameLbl.lineBreakMode = NSLineBreakByTruncatingMiddle;
        view.userInteractionEnabled = NO;
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        
    }
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
    [tmpView setShowArrow:NO];
    ChatGroupModel* model = [_dataSource objectAtIndex:indexPath.row];
    NSString* groupName = [NSString stringWithFormat:@"%@(%d)",model.groupName,(int)model.groupMemNum];
//    NSString* groupAvatar = [ChatGroupModel groupIconWithURLArray:model.groupAvatar];
      [tmpView reloadLogo:@"" name:groupName];
      [ChatGroupModel setGroupIconWithURLArray:model.groupAvatar image:tmpView.logoImgView];
  
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowHeight;
}

#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    // Create label with section title
    UILabel *label=[[UILabel alloc] init];
    label.frame=CGRectMake(12, (headerHeight-20)/2, 300, 20);
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor blackColor];
    label.font=[UIFont fontWithName:@"Helvetica-Bold" size:14];
    label.text= @"群聊";
    
    // Create header view and add label as a subview
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
    [sectionView setBackgroundColor:[UIColor whiteColor]];
    [sectionView addSubview:label];
    
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return headerHeight;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource .count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[MXRouter sharedInstance]configureCurrentVC:self];
    ChatGroupModel* group = [self.dataSource objectAtIndex:indexPath.row];
     [TalkManager pushChatViewUserId:group.groupId type:eConversationTypeGroupChat];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    self.searchController.searchBar.backgroundImage = self.searchWhiteImage;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    //改变SearchBar 背景颜色
    self.searchController.searchBar.backgroundImage = self.searchGrayImage;
    
    [[MXRouter sharedInstance]configureCurrentVC:self];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    //改变SearchBar 背景颜色
    self.searchController.searchBar.backgroundImage = self.searchGrayImage;
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    //    [UIView animateWithDuration:0.1 animations:^{
    //        self.tableView.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
    //        [self.tableView setContentOffset:CGPointMake(0, -60)];
    //    }];
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
}

- (UISearchController *)searchController {
    if (!_searchController) {
        SearchRecentlyGroupListVC *resultVC = [[SearchRecentlyGroupListVC alloc] init];
        resultVC.callback = ^(ChatGroupModel * _Nonnull group) {
            [TalkManager pushChatViewUserId:group.groupId type:eConversationTypeGroupChat];
        } ;
        _searchController = [[UISearchController alloc]initWithSearchResultsController:resultVC];
        _searchController.searchBar.delegate = self;
        _searchController.searchResultsUpdater = resultVC;
        //_searchController.delegate = self;
        _searchController.view.backgroundColor = [UIColor whiteColor];
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = YES;
        //[_searchController.searchBar sizeToFit];
        //_searchController.searchBar.tintColor = [UIColor blackColor];
        _searchController.searchBar.placeholder =  @"搜索";
        [_searchController.searchBar sizeToFit];
        UIOffset offset = {5.0,0};
        _searchController.searchBar.searchTextPositionAdjustment = offset;
        _searchController.searchBar.backgroundImage = self.searchGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:self.searchWhiteImage forState:UIControlStateNormal];
        _searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
        _searchController.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;//关闭提示
        _searchController.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;//关闭自动首字母大写
    }
    return _searchController;
}

- (AddFriendTopView *)searchView {
    if (!_searchView) {
        _searchView = [AddFriendTopView new];
        _searchView.placeholderLbl.text = @"搜索";
        _searchView.size = CGSizeMake(SCREEN_WIDTH, [AddFriendTopView viewHeight]);
        @weakify(self);
        [_searchView addAction:^(UIView *view) {
            @strongify(self);
            SearchRecentlyGroupListVC *resultVC = [[SearchRecentlyGroupListVC alloc] init];
            resultVC.callback = ^(ChatGroupModel * _Nonnull group) {
                [TalkManager pushChatViewUserId:group.groupId type:eConversationTypeGroupChat];
            };
            [self.navigationController pushViewController:resultVC animated:YES];
        }];       
    }
    return _searchView;
}

@end
