//
//  SearchLocalFriendVC.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/25.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "SearchGroupVC.h"
#import "ChatTitleView.h"
#import "MXSeparatorLine.h"
#import "NSObject+Additions.h"
#import "LSearchBar.h"
#import "UINavigationBar+Alpha.h"
#import "ContactHelper.h"
#import "FriendModel.h"
#import "FriendListView.h"
#import "LcwlChat.h"
//#import "CareFansCell.h"
#import "AddFriendListCell.h"
#import "GroupListVC.h"
#define TipString @"搜一搜:"
static int rowHeight = 60;
static const CGFloat headerHeight = 30;
@interface SearchGroupVC ()<UITableViewDelegate,UITableViewDataSource>

//// tableview
@property (nonatomic,strong) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* searchResult;
@property(nonatomic, strong) UIView *searchView;
//@property (strong, nonatomic) UIImageView *noImageView;
//@property (strong, nonatomic) UILabel *noLabel;

@property (strong, nonatomic) NSString *historyKey;
@property (strong, nonatomic) UILabel *historyLabel;
@property (strong, nonatomic) UIView *historyView;

@property(nonatomic, strong) LSearchBar *searchBars;
@property(nonatomic, strong) UIButton *cancelButton;

@property(nonatomic, strong) NSMutableArray *friendData;

@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation SearchGroupVC



- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchText = searchController.searchBar.text;
    NSLog(@"正在编辑过程中的改变");
    [_searchResult removeAllObjects];
    [self filterFriendByKeyword:searchText];
    [self.tableView reloadData];
    AdjustTableBehavior(self.tableView)
}


//@synthesize noImageView,noLabel;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarTitle:@"通讯录朋友"];
    _friendData =  [[LcwlChat shareInstance].chatManager friends];
    _searchResult = [[NSMutableArray alloc]initWithCapacity:10];

    self.view.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.searchTF];
    [layout addSubview:self.tableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchTF becomeFirstResponder];
}

//消息列表
- (UITableView *)tableView {
    
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.backgroundView = nil;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
        AdjustTableBehavior(_tableView)
    }
    return _tableView;
}

-(void)createSearchHostory
{
    _historyView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"shopping_search"]];
    imgView.frame = CGRectMake(18, 12, 20, 20);
    [_historyView addSubview:imgView];
    _historyLabel = [[UILabel alloc]initWithFrame:CGRectMake(43, 12, SCREEN_WIDTH-45, 20)];
    _historyLabel.font = [UIFont font14];
    _historyLabel.textColor = [UIColor darkGrayColor];
    _historyLabel.text = [NSString stringWithFormat:@"%@:%@",@"上次搜索",_historyKey];
    [_historyView addSubview:_historyLabel];
    UIView *line = [MXSeparatorLine initHorizontalLineWidth:SCREEN_WIDTH orginX:0 orginY:43];
    [_historyView addSubview:line];
    if ([StringUtil isEmpty:_historyKey]) {
        _historyView.hidden = YES;
    }
    [self.view addSubview:_historyView];
}

- (void)didCustomSearchBarStartSearch:(NSString *)text {
    _historyView.hidden = YES;
    if (text.length > 0) {
        [self searchByKeyword:text];
    }
    
}

- (void)showNoDataTips {
    //    noImageView.hidden=NO;
    //    noLabel.hidden=NO;
    
    //    [MXBlankPageView addBlankPageView:MXBlankPageMoChatFriendSearchNoResultType withSuperView:self.view];
}

- (void)hideNoDataTips {
    //    noImageView.hidden=YES;
    //    noLabel.hidden=YES;
    
    //    [MXBlankPageView removeFromSuperView:self.view];
}

// 自动提示搜索
- (void)searchByKeyword:(NSString*)keyword {
    
    //    _historyView.hidden = NO;
    //    _historyKey = [CreatePlist readPlist];
    //    if ([StringUtil isEmpty:_historyKey]) {
    //        _historyView.hidden = YES;
    //    }
    //    _historyLabel.text = [NSString stringWithFormat:@"%@:%@",MXLang(@"Talk_friend_search_tips_47", @"上次搜索"),_historyKey];
    //    NSArray* tmpArray = [self filterDataByType:self.type keyword:keyword];//[[MXChatDBUtil sharedDataBase]selectFriendByKeyword:keyword];
    //    if (tmpArray.count == 0) {
    //        _tableView.hidden = YES ;
    //        [self showNoDataTips ];
    //    }else{
    //        _tableView.hidden = NO;
    //        [self hideNoDataTips ];
    //    }
    //
    //    self.searchResult = [tmpArray mutableCopy] ;
    //    [_tableView reloadData];
    
    
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //        NSArray* keys = [NSMutableArray arrayWithArray:[datasource safeObjectAtIndex:indexPath.section]];
    
    static NSString *myCell = @"cell_identifier";
    AddFriendListCell *cell = [tableView dequeueReusableCellWithIdentifier:myCell];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AddFriendListCell" owner:self options:nil]lastObject];
    }
    FriendModel* model = [_searchResult objectAtIndex:indexPath.row];
    BOOL bol = [self containsObject:model.userid array:self.unChangeArray];
    if (bol) {
        [cell reloadData:model status:0];
    }else{
        [cell reloadData:model status:[self.selectData containsObject:model]?2:1];
    }
    return cell;
}
-(BOOL)containsObject:(NSString * )chatID array:(NSArray*)array{
    for (int i=0; i<array.count; i++) {
        FriendModel* model = array[i];
        if ([model.userid isEqualToString:chatID]) {
            return YES;
        }
    }
    return NO;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResult.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendModel* model = [_searchResult safeObjectAtIndex:indexPath.row];
    if ([self.unChangeArray containsObject:model]) {
        self.searchCallback = nil;
    }
//    @weakify(self)
//    [self dismissViewControllerAnimated:YES completion:^{
//
//        @strongify(self)
//        if (self.searchCallback) {
//            self.searchCallback([self.searchResult safeObjectAtIndex:indexPath.row], nil);
//        }
//    }];
    [self.navigationController popViewControllerAnimated:YES];
    if (self.searchCallback) {
        self.searchCallback([self.searchResult safeObjectAtIndex:indexPath.row], nil);
    }
}


- (void)cancelButtonDidClick {
    
    if (self.callback) {
        self.callback();
        [self.searchBars resignFirstResponder];
    }
}
- (void)dismissView {
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        NSString *searchText = textField.text;
       [_searchResult removeAllObjects];
       [self filterFriendByKeyword:searchText];
       [self.tableView reloadData];
    }
}

-(void )filterFriendByKeyword:(NSString *)keyword{
    [_searchResult removeAllObjects];
    if ([StringUtil isEmpty:keyword]) {
        return;
    }
    for (int i=0; i<_friendData.count; i++) {
        FriendModel* model = _friendData[i];
        NSString* friendName = [[StringUtil chinasesCharToLetter:model.name] lowercaseString];
        NSString* contactName = [[StringUtil chinasesCharToLetter:model.addressListName] lowercaseString];
        keyword = [keyword lowercaseString];
        if ([friendName rangeOfString:keyword].location != NSNotFound ) {
            [_searchResult addObject:model];
        }
    }
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = ({
            UITextField *object = [UITextField new];
            [object addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
            [object setViewCornerRadius:15];
            object.font = [UIFont font15];
            object.backgroundColor = [UIColor whiteColor];
            UIView *leftView = [UIView new];
            leftView.size = CGSizeMake(40, 30);
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"im_search_icon"] forState:UIControlStateNormal];
            [leftBtn sizeToFit];
            leftBtn.x = 10;
            leftBtn.y = 5;
            [leftView addSubview:leftBtn];
            object.leftView = leftView;
            object.returnKeyType = UIReturnKeySearch;
            object.leftViewMode = UITextFieldViewModeAlways;
            object.clearButtonMode = UITextFieldViewModeWhileEditing;
            NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索"] colors:@[[UIColor moBlack]] fonts:@[[UIFont font14]]];
            object.attributedPlaceholder = att;
            object.myLeft = 5;
            object.myWidth = SCREEN_WIDTH - 10;
            object.myHeight = 35;
            object.myTop = 10;
            object.myBottom = 10;
            [object setViewCornerRadius:17];
            object;
        });
    }
    return _searchTF;
}

@end
