//
//  SearchCardVC.h
//  Lcwl
//
//  Created by mac on 2019/1/4.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "FriendModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^selectModelCallback)(FriendModel* sender);



@interface SearchCardVC : BaseViewController<UISearchResultsUpdating>
@property (nonatomic, copy) selectModelCallback  callback;
@property (nonatomic, assign) BOOL hidenChatBnt;
@property (nonatomic, copy) NSString* searchTitle;
@property (nonatomic) int type;
@end

NS_ASSUME_NONNULL_END
