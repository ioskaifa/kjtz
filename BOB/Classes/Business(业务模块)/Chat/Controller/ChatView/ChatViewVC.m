//
//  ChatViewVC.m
//  MoPal_Developer
//
//  Created by aken on 15/3/23.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatViewVC.h"
#import "MXRecordView.h"
#import "ChatMessageView.h"
#import "LcwlChat.h"
#import "VoiceConverter.h"
#import "NSDate+Category.h"
#import "Photo.h"
#import "LocationShareVC.h"
#import "MXChatDBUtil.h"
#import "UserModel.h"
#import "NSObject+CapacityAuthorize.h"
#import "TalkManager.h"
#import "PasteboardShowImage.h"
#import "QBImagePicker.h"
#import "ChatViewCell.h"
#import "QBImagePicker.h"
#import "MXLoadingErrorView.h"
#import "MXFixCllocation2DHelper.h"
#import "FriendModel.h"
#import "ChatSendHelper.h"
#import "MXConversation.h"
#import "UIImage+FixOrientation.h"
#import "UIImage+Utility.h"
#import "ALAssetRepresentation+FileDetection.h"
#import "XHMessageTextView.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "MJPhotoBrowser.h"
#import "MJPhoto.h"
#import "MXMoMessageToolbar.h"
#import "NSObject+Additions.h"
#import "MXDeviceMediaManager.h"
#import "MXDownLoadsManager.h"
#import "FBKVOController.h"
#import "ChatViewVM.h"
#import "ChatCacheFileUtil.h"
#import "NSMutableArray+AsynSafe.h"

#if __has_include(<MXChatToolBarSdk/MXChatToolBarSdk.h>)
#import <MXChatToolBarSdk/MXChatToolBarSdk.h>
#else
#import "MXChatToolBarSdk.h"
#endif

#import "MXChatVoice.h"
#import "TalkConfig.h"
#import "ChatForwadVC.h"
#import "MXAlertViewHelper.h"
#import "PhotoBrowser.h"
#import "SendRedPacketVC.h"
#import "SendGroupRedPacketVC.h"
#import "LocationNavigateVC.h"
#import "MXJsonParser.h"
#import "RedPacketDetailVC.h"

#import "WSRedPacketView.h"
#import "WSRewardConfig.h"
#import "RedPacketManager.h"
#import "SendCardVC.h"
#import "UIAlertview+MKBlockAdditions.h"
#import "UINavigationBar+Alpha.h"
#import "MXBarReaderHelper.h"
#import "MXChatDefines.h"
#import "AutoTransferVC.h"
#import "MXChatManager.h"
#import "QNManager.h"
#import "VideoPlayManager.h"
#import "IClouManage.h"
#import "FilePreviewVC.h"
#import "ContactHelper.h"
#import "MXPointAtManager.h"
#import "RoomManager.h"
#import "ChatVoiceManage.h"
#import "ProductLinkView.h"

static const NSInteger K_Tag_ChatView_Forbid = 2001;
static const NSInteger kRecordAudioViewWidth = 240;
static const NSInteger kRecordAudioViewHeight = 180;

@interface ChatViewVC ()<UIGestureRecognizerDelegate,IMXChatManagerDelegate,
UINavigationControllerDelegate,ChatMessageViewDelegate, UIActionSheetDelegate,SendMessageDelegate,IMXChatToolBarDelegate,IMXChatToolBarDataSource,MXPopPreviewViewDelegate, LocationShareVCDelegate> {
    
    // 消息队列
    dispatch_queue_t _messageQueue;
    
    // 消息队列
    dispatch_queue_t _emotionPackageQueue;
    
}

// 聊天工具栏
@property (nonatomic, strong) MXChatToolBar      *chatToolBar;

@property (nonatomic, strong) MXFaceView         *faceView;

// 数据列表
@property (strong, nonatomic) ChatMessageView    *chatView;

@property (nonatomic, strong) NSMutableArray     *indexPaths;

@property (nonatomic) MessageModel               *playMsg;

@property (nonatomic) MXRequest                 *request;
//数据模型观察者
@property (nonatomic, strong) FBKVOController   *kvoController;

@property (nonatomic, strong) ChatViewVM        *viewModel;

@property (nonatomic, strong) MXRecordView      *recordView;

@property (nonatomic, strong) NSMutableArray    *emotionDataSource;

@property (nonatomic, strong) UIView            *timeShortView;

@property (nonatomic, strong) MXPopPreviewView  *popPreviewView;

@property (nonatomic, strong) NSMutableArray    *emotionPackages;

@property(nonatomic,strong) ProductLinkView *productLinkView;

@end

@implementation ChatViewVC




- (instancetype)initWithChatter:(NSString *)chatter conversationType:(EMConversationType)type
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.viewModel = [[ChatViewVM alloc]init];
        self.viewModel.vc = self;
        self.viewModel.conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:chatter];
        if (self.viewModel.conversation == nil) {
            self.viewModel.conversation = [[MXConversation alloc]init];
            self.viewModel.conversation.chat_id = chatter;
            self.viewModel.conversation.conversationType = type;
        }else{
            //读取所有未读信息
            [self.viewModel.conversation markAllMessagesAsRead];
        }
        self.indexPaths = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)setBlack{
    if (self.viewModel.conversation.conversationType == eConversationTypeChat) {
        FriendModel * friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:self.viewModel.conversation.chat_id];
        if (friend.followState == 2) {
            [UIAlertView alertViewWithTitle:@"您已将对方加入黑名单，解除后才可和对方聊天" message:nil cancelButtonTitle:@"移除黑名单" otherButtonTitles:@[@"取消"] onDismiss:^(NSInteger buttonIndex) {
                
            } onCancel:^{
                NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/black/deleteBlackUser"];
                [MXNet Post:^(MXNet *net) {
                    net.apiUrl(url).params(@{@"friend_ids":friend.userid})
                    .finish(^(id data) {
                        NSDictionary *tempDic = (NSDictionary*)data;
                        if ([tempDic[@"success"] boolValue]) {
                            friend.followState = MoRelationshipTypeMyFriend;
                            [[LcwlChat shareInstance].chatManager insertFriends:@[friend]];
                            self.chatToolBar.userInteractionEnabled = YES;
                        }else{
                            [NotifyHelper showMessageWithMakeText:tempDic[@"msg"]];
                        }
                    }).failure(^(id error){
                        
                    })
                    .execute();
                }];
            }];
            self.chatToolBar.userInteractionEnabled =NO;
        }else {
            self.chatToolBar.userInteractionEnabled =YES;
        }
    }else{
        ChatGroupModel * group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.viewModel.conversation.chat_id];
        if (group.roleType == 0) {
            self.chatToolBar.hidden = YES;
            self.viewModel.settingBtn.hidden = YES;
        }
    }
  
}
#pragma mark - lift circle
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]){
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    [self.chatToolBar enableKeyboardNotification:YES];
    if (self.viewModel.conversation.conversationType == eConversationTypeGroupChat) {
        [MXPointAtManager sharedManager].groupId = self.viewModel.conversation.chat_id;
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self setBlack];
    [self setTopTitle:self.viewModel.conversation];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[MXDeviceMediaManager sharedInstance].deviceMedia stopPlaying];
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
    if (self.request != nil) {
        [self.request stop];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.chatToolBar enableKeyboardNotification:NO];
}

-(void)initNavBar{
    self.navigationItem.titleView = self.viewModel.titleView;
    if (self.viewModel.conversation.conversationType == eConversationTypeGroupChat) {
        // 添加聊天toolbar
        [self.view addSubview:self.chatToolBar];
        self.navigationItem.rightBarButtonItem =  [[UIBarButtonItem alloc] initWithCustomView:self.viewModel.settingBtn];
    }else{
        [self.view addSubview:self.chatToolBar];
        //如果是客服且不是自己好友  则不显示管理按钮, 预防把客服拉黑
        NSString *userId = self.viewModel.conversation.chat_id;
        BOOL isService = NO;
        BOOL isFriend = NO;
        NSArray *serviceArr = [MXCache valueForKey:kMXMemberServiceList];
        if ([serviceArr isKindOfClass:NSArray.class]) {
            if ([serviceArr containsObject:userId]) {
                isService = YES;
            }
        }
        FriendModel *tempObj = [[LcwlChat shareInstance].chatManager loadFriendByChatId:userId];
        if (tempObj) {
            if (tempObj.followState != MoRelationshipTypeStranger) {
                isFriend = YES;
            }
        }
        if (isService && !isFriend) {
            return;
        }
        self.navigationItem.rightBarButtonItem =  [[UIBarButtonItem alloc] initWithCustomView:self.viewModel.settingBtn];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initQueue];
    [self addNotification];
    [self initUI];
    [self setTopTitle:self.viewModel.conversation];
    //如果是群聊 先拉一次群成员
    if (self.viewModel.conversation.conversationType == eConversationTypeGroupChat) {
        [RoomManager getGroupMembers:@{@"group_id":self.viewModel.conversation.chat_id}
                          completion:^(id array, NSString *error) {
            if (array) {
                //比对本地好友是否存在备注名
                NSArray *friedsArr = [[LcwlChat shareInstance].chatManager friends];
                [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    FriendModel *model = (FriendModel*)obj;
                    //判断是否本地好友 是本地好友则要更新备注名
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"userid == '%@'", model.userid)];
                    NSArray *tempArr = [friedsArr filteredArrayUsingPredicate:predicate];
                    if (tempArr.count > 0) {
                        FriendModel *obj = tempArr.firstObject;
                        if (obj.remark) {
                            model.remark = obj.remark;
                        }
                    }
                    [[LcwlChat shareInstance].chatManager insertGroupMember:model];
                }];
            }
        }];
    }
}

- (void)initQueue{
    _messageQueue = dispatch_queue_create("moxian.com", DISPATCH_QUEUE_CONCURRENT);
    _emotionPackageQueue = dispatch_queue_create("moxian.com.load.emtion", DISPATCH_QUEUE_CONCURRENT);
}
- (NSString*)chatToolBarBundlePath {
    return [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"MXChatToolBar.bundle"];
}

-(MXConversation*)getCurrentConversation{
    return self.viewModel.conversation;
}

- (void)setTopTitle:(MXConversation*)conversation{
    NSString* chatId = conversation.chat_id;
    if (self.viewModel.conversation.conversationType == eConversationTypeGroupChat) {
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:chatId];
        if (![StringUtil isEmpty:group.groupName]) {
            //判断是否是设置的群名
            NSString *groupName = [NSString stringWithFormat:@"%@(%ld)",group.groupName,group.groupMemNum];
//            if([group.is_set_name integerValue] == 0) {
//                groupName = [NSString stringWithFormat:@"群聊(%ld)",group.groupMemNum];
//            }
            [self.viewModel setTitle:groupName openSound:!group.enable_disturb];
        }
    }else {
        FriendModel* user = [[LcwlChat shareInstance].chatManager loadFriendByChatId:chatId];
        NSString* name = user.name;
        if (![StringUtil isEmpty:user.remark]) {
            name = user.remark;
        }
        [self.viewModel setTitle:name openSound:!user.enable_disturb];
    }
}

-(void)addNotification{
    _kvoController = [FBKVOController controllerWithObserver:self];
    @weakify(self)
    //页数
    [_kvoController observe:self.viewModel keyPath:@"topMsgTime" options:NSKeyValueObservingOptionNew block:^(id observer, id object, NSDictionary *change) {
        @strongify(self);
        if (![change[@"new"] isKindOfClass:[NSNull class]]) {
             [self.chatView.msgRecordTable.header  endRefreshing];
            //此处刷新数据
            [self loadMoreMessages];
        }
    }];
    
    // 监听录制语音
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recordAudioNotification) name:kRecordNotificationMaxWorming object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadChatList) name:[NSString stringWithFormat:@"ReloadChatList_%@",self.viewModel.conversation.chat_id] object:nil];
    
    [[LcwlChat shareInstance].chatManager addDelegate:self];

}

-(void)removeNotification{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)reloadChatList {
    [self.chatView.msgRecordTable reloadData];
}

#pragma mark - XMPP Delegate
- (void)didReceiveMessage:(MessageModel *)message {
    MXConversation* con = self.viewModel.conversation;
    if ((con != nil && [con.chat_id isEqualToString:message.chat_with]) || (self.creatorRole > 0 && message.creatorRole > 0)){ //由于con是全局的东西，取到的其实外部消息列表的，所以需要另外判断。如果是客服消息直接加入
        if(message.type == MessageTypeSs ||
           message.type == MessageTypeSgroup ||
           message.type == MessageTypeS){
            message.state=kMXMessageStateSuccess;
            if (message.subtype == kMXMessageTypeVoiceChat) {
                NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:message.body];
                kMXVoiceChatType chatType = [bodyDic[@"attr3"] integerValue];
                if (chatType == kMXVoiceChatTypeBusy ||
                    chatType == kMXVoiceChatTypeCancel ||
                    chatType == kMXVoiceChatTypeEnd ||
                    chatType == kMXVoiceChatTypeOverTime) {
                    [self addMessage:message];
                }
            } else {
                [self addMessage:message];
                ///修改群名称的消息 同步修改本群名称
                if (message.subtype == 3 && message.type == MessageTypeSgroup) {
                    Block_Exec_Main_Async_Safe(^{
                        [self setTopTitle:self.viewModel.conversation];
                    });
                }
            }
        } else if(message.type == MessageTypeGroupchat ||
                  message.type == MessageTypeNormal  ||
                  message.type == MessageTypeRich){
            message.state=kMXMessageStateSuccess;
            if (message.subtype == kMXMessageTypeVoice) {
                [self addMessage:message];
                id<IMXDownLoadsManagerDelegate> downloadDelegate = self;
                [[MXDownLoadsManager sharedInstance] downloadMessageAttachmentWithMessage:message completion:downloadDelegate];
            } else {
                if (message.subtype == kMXMessageTypeVoiceChat) {
                    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:message.body];
                    kMXVoiceChatType chatType = [bodyDic[@"attr3"] integerValue];
                    if (chatType == kMXVoiceChatTypeBusy ||
                        chatType == kMXVoiceChatTypeCancel ||
                        chatType == kMXVoiceChatTypeEnd ||
                        chatType == kMXVoiceChatTypeOverTime) {
                        [self addMessage:message];
                    }
                } else if (message.subtype == kMXMessageTypeWithdraw) {
                    [self hanldWithdrawMessage:message];
                } else {
                    [self addMessage:message];
                }
            }
        }
    }
}
#pragma mark - 收到回执
- (void)didReceiveReceipt:(MessageModel *)message{
    dispatch_barrier_async(_messageQueue, ^(){
        @weakify(self)
        dispatch_async(dispatch_get_main_queue(), ^{
            @strongify(self)
            NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:message];
            if (cellIndex == -1) {
                return ;
            }
            [self updateMessageState:message cellRow:[NSString stringWithFormat:@"%ld",(long)cellIndex] controller:self];
            [self hanldAckedWithdrawMessage:message];
        });
    });
}

#pragma mark - 处理收到自己发出的撤回消息的回执
- (void)hanldAckedWithdrawMessage:(MessageModel *)msg {
    if (![msg isKindOfClass:MessageModel.class]) {
        return;
    }
    if (msg.subtype != kMXMessageTypeWithdraw) {
        return;
    }
    if (![msg.messageId isEqualToString:self.chatView.withdrawMessageID]) {
        return;
    }
    self.chatView.withdrawMessageID = @"";
    [NotifyHelper hideHUDForView:self.chatView animated:YES];
    //查找到对应的message
    __block MessageModel *selectMsg = nil;
    [self.chatView.viewModel.recordsArray enumerateObjectsWithOptions:NSEnumerationReverse
                                                           usingBlock:^(MessageModel   * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:MessageModel.class]) {
            if ([obj.messageId isEqualToString:msg.messageId]) {
                selectMsg = obj;
                *stop = YES;
            }
        }
    }];
    if (![selectMsg isKindOfClass:MessageModel.class]) {
        return;
    }
    NSInteger selectIndex = [self.chatView.viewModel.recordsArray indexOfObject:selectMsg];
    selectMsg.subtype = kMXMessageTypeWithdraw;
    NSDictionary *bodyDic = @{@"attr1":selectMsg.messageId};
    selectMsg.body = [MXJsonParser dictionaryToJsonString:bodyDic];
    [[MXChatDBUtil sharedDataBase] updateWithdrawRowDataByMsgModel:selectMsg];
    //判断是否最后一条
    if (selectIndex == (self.chatView.viewModel.recordsArray.count - 1)) {
        NSArray *tmpArray = [[MXChatDBUtil sharedDataBase] lastMessage:selectMsg.chat_with messageId:@""];
        MXConversation* conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:selectMsg.chat_with];
        //是否还存在信息
        if (tmpArray.count == 0) {
           conversation.lastClearTime = @"";
           [[MXChatDBUtil sharedDataBase] updateClearTime:selectMsg.chat_with
                                                     time:@""
                                                    msgID:@""];
        } else {
           [conversation.messages addObjectsFromArray:tmpArray];
           MessageModel* lastModel = tmpArray.lastObject;
           conversation.lastMessageId = lastModel.messageId;
           [[MXChatDBUtil sharedDataBase] updateClearTime:selectMsg.chat_with
                                                     time:conversation.latestMessage.sendTime
                                                    msgID:lastModel.messageId];
        }
    }
    Block_Exec_Main_Async_Safe(^{
        //判断selectIndex是否在可见视图区域, 不在视图区域不更新
        NSIndexPath *selectIndexPath = [NSIndexPath indexPathForRow:selectIndex inSection:0];
        UITableViewCell *selectCell = [self.chatView.msgRecordTable cellForRowAtIndexPath:selectIndexPath];
        //如果该cell不可见
        if (!selectCell) {
            return;
        }
        NSArray *indexPaths = @[selectIndexPath];
        [self.chatView.msgRecordTable beginUpdates];
        [self.chatView.msgRecordTable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.chatView.msgRecordTable endUpdates];
    });
}

#pragma mark - 处理收到别人的撤回消息
- (void)hanldWithdrawMessage:(MessageModel *)msg {
    if (![msg isKindOfClass:MessageModel.class]) {
        return;
    }
    if (msg.subtype != kMXMessageTypeWithdraw) {
        return;
    }
    //查找到对应的message
    __block MessageModel *selectMsg = nil;
    [self.chatView.viewModel.recordsArray enumerateObjectsWithOptions:NSEnumerationReverse
                                                           usingBlock:^(MessageModel   * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:MessageModel.class]) {
            if ([obj.messageId isEqualToString:msg.messageId]) {
                selectMsg = obj;
                *stop = YES;
            }
        }
    }];
    if (![selectMsg isKindOfClass:MessageModel.class]) {
        return;
    }
    NSInteger selectIndex = [self.chatView.viewModel.recordsArray indexOfObject:selectMsg];
    selectMsg.subtype = kMXMessageTypeWithdraw;
    NSDictionary *bodyDic = @{@"attr1":selectMsg.messageId};
    selectMsg.body = [MXJsonParser dictionaryToJsonString:bodyDic];
    //判断是否最后一条
    if (selectIndex == (self.chatView.viewModel.recordsArray.count - 1)) {
        NSArray *tmpArray = [[MXChatDBUtil sharedDataBase] lastMessage:selectMsg.chat_with messageId:@""];
        MXConversation* conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:selectMsg.chat_with];
        //是否还存在信息
        if (tmpArray.count == 0) {
           conversation.lastClearTime = @"";
           [[MXChatDBUtil sharedDataBase] updateClearTime:selectMsg.chat_with
                                                     time:@""
                                                    msgID:@""];
        } else {
           [conversation.messages addObjectsFromArray:tmpArray];
           MessageModel* lastModel = tmpArray.lastObject;
           conversation.lastMessageId = lastModel.messageId;
           [[MXChatDBUtil sharedDataBase] updateClearTime:selectMsg.chat_with
                                                     time:conversation.latestMessage.sendTime
                                                    msgID:lastModel.messageId];
        }
    }
    Block_Exec_Main_Async_Safe(^{
        //判断selectIndex是否在可见视图区域, 不在视图区域不更新
        NSIndexPath *selectIndexPath = [NSIndexPath indexPathForRow:selectIndex inSection:0];
        UITableViewCell *selectCell = [self.chatView.msgRecordTable cellForRowAtIndexPath:selectIndexPath];
        //如果该cell不可见
        if (!selectCell) {
            return;
        }
        NSArray *indexPaths = @[selectIndexPath];
        [self.chatView.msgRecordTable beginUpdates];
        [self.chatView.msgRecordTable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.chatView.msgRecordTable endUpdates];
    });
}

- (void)initUI {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.isShowBackButton=YES;
    // 添加聊天列表chatView
    [self.view addSubview:self.chatView];
    [self initNavBar];
    // 给背景view添加事件，已重新调整输入框的位置
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHidden)];
    tap.delegate = self;
    [self.chatView addGestureRecognizer:tap];
    @weakify(self)
    self.chatView.msgRecordTable.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        @strongify(self)
        [self.viewModel getNextChatMessages];
    }];

//    [self.chatView.msgRecordTable.header beginRefreshing];
    [self.viewModel getNextChatMessages];
    
    [self.view addSubview:self.popPreviewView];
    
    if(self.productInfo) {
        [self showProductLinkView];
    }
}

- (void)hidePreviewEmotion {
    
    __weak __typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        __strong __typeof(self) strongSelf = weakSelf;
        strongSelf.popPreviewView.hidden = YES;
    });
}

-(void)backAction:(id)sender{
    //清理缓存
    [super backAction:sender];
}

- (void)popToChatListView {
    
    AppDelegate* app = MoApp;
//    [app.mainVC switchToChatList];
}

#pragma mark - getter and setter
// 初始化聊天toolbar
- (MXChatToolBar *)chatToolBar {
    if (!_chatToolBar) {
        _chatToolBar = [[MXChatToolBar alloc] initWithFrame:CGRectMake(0,  self.view.frame.size.height - [MXChatToolBar defaultHeight] - SafeAreaBottom_Height, self.view.frame.size.width, [MXChatToolBar defaultHeight]) style:-1];
        _chatToolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        _faceView = (MXFaceView*)[(MXChatToolBar *)_chatToolBar faceView];
        _chatToolBar.delegate = self;
        _chatToolBar.dataSource = self;
        MXChatBarMoreView* moreView = (MXChatBarMoreView *)_chatToolBar.moreView;
        [moreView setScrollEnabled:NO];
    }
    return _chatToolBar;
}


// 初始化聊天列表chatView
- (ChatMessageView *)chatView
{
    
    if (_chatView == nil) {
        
        _chatView = [[ChatMessageView alloc] initWithFrame:CGRectMake(0,0 , SCREEN_WIDTH, SCREEN_HEIGHT - [MXChatToolBar defaultHeight]-StatuBarHeight- NavBarHeight-SafeAreaBottomHeight)];
                                                                      
        _chatView.delegate=self;
        _chatView.backgroundColor = [UIColor clearColor];
        _chatView.viewModel = self.viewModel;
    }
    return _chatView;
}

- (MXRecordView*)recordView {
    if (!_recordView) {
        _recordView = [[MXRecordView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/2 - kRecordAudioViewWidth/2, self.view.bounds.size.height/2 - kRecordAudioViewHeight/2, kRecordAudioViewWidth, kRecordAudioViewHeight)];
    }
    return _recordView;
}

- (void)chatViewDidScroll {
    
    [self.chatToolBar endEditing:NO];
}

#pragma mark - 点击背景隐藏
-(void)keyBoardHidden{
    
//    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    [[UIMenuController sharedMenuController] setMenuVisible:NO animated:YES];
    [self.chatToolBar endEditing:YES];
}

-(void)loadBeforeMessages{
    @weakify(self)
    dispatch_barrier_async(_messageQueue, ^{
        @strongify(self)
        NSArray* array = [self.viewModel selectBeforeChatRecords];
        if (array.count > 0 ) {
            NSMutableArray* tmpArray = [NSMutableArray array];
            [tmpArray addObjectsFromArray:array];
            [tmpArray addObjectsFromArray:self.viewModel.recordsArray];
            self.viewModel.recordsArray.array = tmpArray;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.chatView.msgRecordTable reloadData];
                if([StringUtil isEmpty:self.viewModel.topMsgTime]){
                    NSIndexPath *scrollPath = [NSIndexPath indexPathForRow:[self.viewModel.recordsArray count] - 1 inSection:0];
                    [self.chatView.msgRecordTable scrollToRowAtIndexPath:scrollPath
                                                        atScrollPosition:UITableViewScrollPositionBottom
                                                                animated:NO];
                }else{
                    NSIndexPath *scrollPath = [NSIndexPath indexPathForRow:array.count-1 inSection:0];
                    [self.chatView.msgRecordTable scrollToRowAtIndexPath:scrollPath
                                                        atScrollPosition:UITableViewScrollPositionTop
                                                                animated:NO];
                }
            });
            // 下载语音
            [self downloadMessageAttachments:self.viewModel.recordsArray];
        }else{
            if(self.viewModel.recordsArray.count == 0){
                [self.viewModel resetChatMessage];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.chatView.msgRecordTable reloadData];
                });
            }
        }
    });
}


- (void)downloadMessageAttachments:(NSArray *)messages {
    
    
    id<IMXDownLoadsManagerDelegate> downloadDelegate = self;
    
    [[MXDownLoadsManager sharedInstance] downloadMessageAttachments:messages completion:downloadDelegate];
}
- (void)downloadMessageAttachmentSuccess:(MessageModel *)model {
    dispatch_barrier_async(_messageQueue, ^(){
        
        NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:model];
        if (cellIndex == -1) {
            return ;
        }
        model.isPlaying = 0;
        [self.chatView.viewModel.recordsArray replaceObjectAtIndex:cellIndex withObject:model];
        dispatch_async(dispatch_get_main_queue(), ^{
            // 有可能要延期刷新
            NSIndexPath* path = [NSIndexPath indexPathForRow:cellIndex inSection:0];
            [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
        });
        
    });
    
    
}
#pragma mark - 重新刷新数据
- (void)loadMoreMessages{
    [self loadBeforeMessages];
}
/****************************************************************************Delegate处理***************************************************************/
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isKindOfClass:[UITableView class]]) {
        [self keyBoardHidden];
        return YES;
    }else if ([touch.view isKindOfClass:[UICollectionView class]] ||
              touch.view.tag == MXChatToolBarViewTagForCellContentView ||
              touch.view.tag == MXChatToolBarStyleTagForBarBackground ||
              [touch.view isKindOfClass:[UIScrollView class]] ||
              [touch.view isKindOfClass:[MXFaceView class]] ||
              [touch.view isKindOfClass:[MXFacialPanelView class]] ||
              [touch.view isKindOfClass:[UIButton class]] ||
              touch.view.tag == MXChatToolBarStyleTagForBottomView||
              [touch.view isKindOfClass:[YYLabel class]] ) {
        return NO;
    }
    return YES;
}

#pragma mark - MXMessageToolBar delegate

- (NSArray *)emotionFormessageViewController:(id)viewController {
    return self.emotionDataSource;
}

- (CGFloat)chatToolBarBottomPancelForMoreViewHeight {
    
    if (self.viewModel.conversation.conversationType != eConversationTypeChat) {
        
        return 100;
    }
    return MXToolBarPanelDefaultHeight;
}

- (void)inputTextViewWillBeginEditing:(XHMessageTextView *)messageInputTextView{
    [self.chatView.menuController setMenuItems:nil];
}

- (void)chatToolbarDidChangeFrameToHeight:(CGFloat)toHeight {
    
    @weakify(self)
    [UIView animateWithDuration:0.25 animations:^{
        @strongify(self)
        CGRect rect = self.chatView.frame;
        rect.origin.y = 0;
        rect.size.height = self.view.frame.size.height  - toHeight-SafeAreaBottomHeight ;
        self.chatView.frame = rect;
        
    } completion:^(BOOL finished) {
        @strongify(self)
        
       
    }];
  
    [self.chatView scrollViewToBottom:YES];
    
    CGRect rect = self.popPreviewView.frame;
    rect.origin.x =  self.view.frame.size.width - rect.size.width - 22;
    rect.origin.y = CGRectGetMinY(self.chatToolBar.frame) - CGRectGetHeight(self.popPreviewView.frame);
    self.popPreviewView.frame = rect;
}

- (void)chatToolBarShouldChangeText:(NSString *)emotionString {
    
    if (emotionString.length > 0) {
        
        // 如果有多个同名的，只显示最第一个
        BOOL isExist = NO;
        
        for (MXEmotionSetting * packages in self.emotionPackages) {
            
            for (MXEmotion *emotion in packages.emotions) {
                
                if ([emotion.emotionName isEqualToString:emotionString]) {
                    
                    MLog(@"emotion.emotionName：%@----》%@",emotionString,emotion.emotionTitle );
                    
                    [self.popPreviewView showData:emotion];
                    self.popPreviewView.hidden = NO;
                    [self hidePreviewEmotion];
                    isExist = YES;
                    
                    break;
                    
                }else {
                    
                    self.popPreviewView.hidden = YES;
                }
            } // for end
            
            if (isExist) {
                break;
            }
            
        } // for end
    }
}

- (BOOL)chatToolBarShouldDelTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@""]) {
        return [[MXPointAtManager sharedManager] delPointAt:self.chatToolBar.inputTextView
                                                  withRange:range];
    }
    if ([text isEqualToString:@"@"]) {
        ///群聊才有@功能
        if (self.viewModel.conversation.conversationType == eConversationTypeGroupChat) {
            [[MXPointAtManager sharedManager] pointAtFriend:self.chatToolBar.inputTextView
                                                    inRange:range];            
        }
    }
    return YES;
}

- (void)didSelectMoreItemAt:(id)emotion {
    
    MXEmotion *tmpMoreItme = (MXEmotion*)emotion;

    switch ([tmpMoreItme.emotionId integerValue]) {
        case MXChatBarMoreItemTypeAblum:
            [self mxSharePickPhoto:nil];
            break;
        case MXChatBarMoreItemTypeCamera:
            [self mxSharePickCamera:nil];
            break;
        case MXChatBarMoreItemTypeLocation:
            [self mxSharePickLocation:nil];
            break;
        case MXChatBarMoreItemTypeRedPackage:
            [self mxShareRedPackage:nil];
             break;
        case MXChatBarMoreItemTypeTransfer:
            [self tranfer:nil];
            break;
        case MXChatBarMoreItemTypeDeal:
            [self deal:nil];
            break;
        case MXChatBarMoreItemTypeFavorite:
            [self favorite:nil];
            break;
        case MXChatBarMoreItemTypeContact:
            [self mxShareContact:nil];
            break;
        case MXChatBarMoreItemTypeAutoTransfer:
            [self mxAutoTransfer:nil];
            break;
        case MXChatBarMoreItemTypeVoice:{
            [self mxVoice];
            break;
        }case MXChatBarMoreItemTypeFile:{
            [self mxFile];
            break;
        }
        default:
            break;
    }
}

///语音通话
- (void)mxVoice {
    [MXDeviceManager requestDeviceAuthod:MXDeviceAuthorizationMicrophone complete:^(BOOL allow) {
        Block_Exec_Main_Async_Safe(^{
            if (allow) {
                ///群组聊天
                if (self.viewModel.conversation.conversationType == eConversationTypeGroupChat) {
                  [NotifyHelper showMessageWithMakeText:@"群聊暂未开放"];
                } else {
                    [[ChatVoiceManage shareInstance] askCallRoom:0
                                                        toUserId:self.viewModel.conversation.chat_id?:@""
                                                        chatType:self.viewModel.conversation.conversationType];
                }
            } else {
                [NotifyHelper showMessageWithMakeText:@"未开启麦克风权限, 无法使用该功能"];
            }
        });
    }];
}

- (void)mxFile {

    void (^selectFileBlock)(NSArray *) = ^(NSArray * files) {
        if (![files isKindOfClass:NSArray.class]) {
            return;
        }
        for (MessageModel *msg in files) {
            if (![msg isKindOfClass:MessageModel.class]) {
                continue;
            }
            MessageModel* tempModel = [ChatSendHelper sendFileMessageByMessage:msg
                                                                      receiver:self.viewModel.conversation.chat_id
                                                                   messageType:self.viewModel.conversation.conversationType
                                                                      delegate:self creatorRole:self.creatorRole];
            [self addMessage:tempModel];
        }
    };
    
    void (^selectICloudBlock) (void) = ^() {
        [[IClouManage shareInstance] presentDocumentPickerInSuperVC:self complete:^(MXDocument * _Nullable doc) {
            if (![doc isKindOfClass:MXDocument.class]) {
                return;
            }
            NSString *rootPath = [[ChatCacheFileUtil sharedInstance] userDocPath];
            NSString *path = [doc.fileURL lastPathComponent];
            NSString *localUrl = StrF(@"%@/%@", rootPath, path);
            [doc.data writeToFile:localUrl atomically:YES];
            //写入本地，上传到服务器
            doc.locFileUrl = localUrl;
            [self sendFileMessage:doc];
        }];
    };
    MXRoute(@"SelectFileVC", (@{@"selectFileBlock":selectFileBlock, @"selectICloudBlock":selectICloudBlock}))
    
}

-(void)mxAutoTransfer:(id)sender{
    AutoTransferVC *autoTransferVC = [[AutoTransferVC alloc]init];
    autoTransferVC.roomId = self.viewModel.conversation.chat_id;
    autoTransferVC.callback = ^(FriendModel * _Nonnull model) {
//        MessageModel * message = [ChatSendHelper sendCardMessage:@{@"attr1":model.userid,@"attr2":model.avatar,@"attr3":model.name} toUsername:self.viewModel.conversation.chat_id messageType:self.viewModel.conversation.conversationType];
//        [self addMessage:message];
    };
    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:autoTransferVC];
    [self.navigationController presentViewController:nav animated:YES completion:^{
        
    }];
}

-(void)mxShareContact:(id)sender{
    SendCardVC *cardVC = [[SendCardVC alloc]init];
    cardVC.userId = self.viewModel.conversation.chat_id;
    cardVC.callback = ^(FriendModel * _Nonnull model) {
        MessageModel * message = [ChatSendHelper sendCardMessage:@{@"attr1":model.userid?:@"",@"attr2":model.avatar,@"attr3":model.name} toUsername:self.viewModel.conversation.chat_id messageType:self.viewModel.conversation.conversationType];
        [self addMessage:message];
    };
    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:cardVC];
    [self.navigationController presentViewController:nav animated:YES completion:^{
        
    }];
}

-(void)mxShareRedPackage:(id)sender{
      [self keyBoardHidden];
    if (self.viewModel.conversation.conversationType == eConversationTypeGroupChat) {
        SendGroupRedPacketVC* redPacket = [[SendGroupRedPacketVC alloc]init];
        redPacket.callback = ^(id data) {
            NSDictionary* dataDict = (NSDictionary* )data;
            [ChatSendHelper sendRedPacketMessage:dataDict[@"remark"] toUsername:self.viewModel.conversation.chat_id redpacketId:dataDict[@"orderId"] messageType:eConversationTypeGroupChat creatorRole:self.creatorRole];
           
        } ;
        redPacket.userId = self.viewModel.conversation.chat_id;
        UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:redPacket];
        [self presentViewController:nav animated:YES completion:^{
            
        }];
    }else{
        SendRedPacketVC* redPacket = [[SendRedPacketVC alloc]init];
        redPacket.callback = ^(id data) {
            NSDictionary* dataDict = (NSDictionary* )data;
            [ChatSendHelper sendRedPacketMessage:dataDict[@"remark"] toUsername:self.viewModel.conversation.chat_id redpacketId:dataDict[@"orderId"] messageType:eConversationTypeChat creatorRole:self.creatorRole];
        } ;
        redPacket.userId = self.viewModel.conversation.chat_id;
        UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:redPacket];
        [self presentViewController:nav animated:YES completion:^{
        }];
    }
}

///发送产品链接
-(void)sendProductLink:(NSDictionary *)param {
    MessageModel * message = [ChatSendHelper sendProductLinkMessage:param toUsername:self.viewModel.conversation.chat_id messageType:self.viewModel.conversation.conversationType creatorRole:self.creatorRole];
    [self addMessage:message];
    
    [self.productLinkView removeFromSuperview];
}

// 按下录音按钮开始录音
- (void)didStartRecordingVoiceAction:(NSString*)error {
    
    MLog(@"%@",@"didStartRecordingVoiceAction");
    
    if (error) {
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定"style:UIAlertActionStyleDefault handler:nil];
        
        UIAlertController *alerVC = [UIAlertController alertControllerWithTitle:@"" message:@"MoXian App需要访问您的麦克风。\n请启用麦克风-设置/隐私/麦克风" preferredStyle:UIAlertControllerStyleAlert];
        
        [alerVC addAction:okAction];
        
        [self presentViewController:alerVC animated:YES completion:nil];
        return;
    }
    
    NSString* chatId = self.viewModel.conversation.chat_id;
    if (self.viewModel.conversation.conversationType == eConversationTypeChat) {
        if (//![chatId containsString:@"_"]
            [chatId rangeOfString:@"_"].location==NSNotFound
            ) {
//            MoYouModel* friend = [[MXChat sharedInstance].chatManager loadFriendByChatId:chatId];
//            if (friend == nil || friend.followState != 1) {
//                [NotifyHelper showMessageWithMakeText: MXLang(@"Talk_friend_noAuth_sendAudio",@"相互关注后才能发送语音哟~")];
//
//                return;
//            }
        }
    }
    
    if ([self.recordView isKindOfClass:[MXRecordView class]]) {
        [(MXRecordView *)self.recordView recordButtonTouchDown];
    }
    
    if ([self canRecord]) {
        
        MXRecordView *tmpView = self.recordView;
        tmpView.frame=CGRectMake(self.view.frame.size.width/2 - tmpView.frame.size.width/2,
                                 self.view.frame.size.height/2 - tmpView.frame.size.height/2,
                                 tmpView.frame.size.width, tmpView.frame.size.height);
        
        [self.view addSubview:tmpView];
        [self.view bringSubviewToFront:tmpView];
        
        UIView *bgView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        bgView.backgroundColor = [UIColor clearColor];
        bgView.tag = K_Tag_ChatView_Forbid;
        bgView.userInteractionEnabled = YES;
        [bgView becomeFirstResponder];
        [self.navigationController.view addSubview:bgView];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.navigationController.view).insets(UIEdgeInsetsZero);
        }];
        if (self.playMsg != nil) {
            self.playMsg.isPlaying = 0;
            dispatch_barrier_async(_messageQueue, ^(){
                NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:self.playMsg];
                if (cellIndex == -1) {
                    return ;
                }
                [self.chatView.viewModel.recordsArray replaceObjectAtIndex:cellIndex withObject:self.playMsg];
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSIndexPath* path = [NSIndexPath indexPathForRow:cellIndex inSection:0];
                    [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
                    self.playMsg = nil;
                });
                
                
            });
            
            
        }
        
        [[[MXDeviceMediaManager sharedInstance] deviceMedia] startRecordAudio:^(NSError *error) {
            if (error) {
                MLog(@"开始录音失败:%@",error);
            }
        }];
    }
}

// 手指向上滑动取消录音
- (void)didCancelRecordingVoiceAction {
    UIView *view = [self.navigationController.view viewWithTag:K_Tag_ChatView_Forbid];
    if(view){
        [view removeFromSuperview];
    }
    
    if ([self.recordView isKindOfClass:[MXRecordView class]]) {
        [(MXRecordView *)self.recordView recordButtonTouchUpOutside];
    }
    [self.recordView removeFromSuperview];
    
    MLog(@"didCancelRecordingVoiceAction");
    // 取消录音
    [[[MXDeviceMediaManager sharedInstance] deviceMedia] cancelCurrentRecording];
}

// 松开手指完成录音
- (void)didFinishRecoingVoiceAction {
    
    if ([self.recordView isKindOfClass:[MXRecordView class]]) {
        [(MXRecordView *)self.recordView recordButtonTouchUpInside];
    }
    
    [self.recordView removeFromSuperview];
    
    [[[MXDeviceMediaManager sharedInstance] deviceMedia]
     stopRecordAudioWithCompletion:^(NSString *recordPath, NSInteger aDuration, NSError *error) {
         
         if (error==nil) {
             UIView *view = [self.navigationController.view viewWithTag:K_Tag_ChatView_Forbid];
             if(view){
                 [view removeFromSuperview];
             }
             MXChatVoice* voice = [[MXChatVoice alloc] init];
             if (aDuration >=60) {
                 voice.duration = 60;//audioRecorder.currentTime;
             }else{
                 voice.duration = aDuration;//audioRecorder.currentTime;
             }
             voice.fileLength = 0;
             voice.localPath = recordPath;
             voice.displayName = [recordPath lastPathComponent];
             MessageModel *tempModel = [ChatSendHelper sendVoiceMessage:voice
                                                             toUsername:self.viewModel.conversation.chat_id
                                                            messageType:self.viewModel.conversation.conversationType
                                                               delegate:self creatorRole:self.creatorRole];
             if (tempModel == nil) {
                 return ;
             }
             [self addMessage:tempModel];
         } else {
             [self showTimeShortView];
         }
     }];
}

- (void)didDragOutsideAction {
    
    if ([self.recordView isKindOfClass:[MXRecordView class]]) {
        [(MXRecordView *)self.recordView recordButtonDragOutside];
    }
}

- (void)didDragInsideAction {
    
    if ([self.recordView isKindOfClass:[MXRecordView class]]) {
        [(MXRecordView *)self.recordView recordButtonDragInside];
    }
    
}

- (void)recordAudioNotification {
    [self.chatToolBar resetRecordState];
}


// 处理动态表情发送
- (void)didSendFace:(NSString *)faceLocalPath {
    
    [self sendGifMessage:faceLocalPath];
}

// 处理文字或静态表情
- (void)didSendText:(NSString *)text {
    
    NSString *tmpText= [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([tmpText length]==0) {
        return;
    }
    [self sendTextMessage:tmpText];
}

- (void)didSendText:(NSString *)text withExt:(NSDictionary *)ext {
    [self sendGifMessage:ext];
}

// 相册选择图片
-(void)mxSharePickPhoto:(id)sender {
    @weakify(self)
    [[PhotoBrowser shared] showThumbnailPhotoLibrary:self completion:^(NSArray<UIImage *> * _Nonnull images, NSArray<PHAsset *> * _Nonnull assets, BOOL isOriginal) {
        @strongify(self)
        ///结果只有一个时候判断是视频还是照片,图片跟视频不能混合选择
        if (images.count == 1) {
            PHAsset *object = (PHAsset *)assets.firstObject;
            if (object.mediaType == PHAssetMediaTypeVideo) {
                PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
                options.version = PHImageRequestOptionsVersionCurrent;
                options.deliveryMode = PHVideoRequestOptionsDeliveryModeAutomatic;
                PHImageManager *manager = [PHImageManager defaultManager];
                [manager requestAVAssetForVideo:object options:options resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                    AVURLAsset *urlAsset = (AVURLAsset *)asset;
                    [self sendVideoMessage:urlAsset.URL];
                }];
            } else {
                UIImage* image = (UIImage*)images.firstObject;
                [self sendImageMessage:image];
            }
            return;
        }
        [images enumerateObjectsUsingBlock:^(UIImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UIImage* image = (UIImage*)obj;
            [self sendImageMessage:image];
        }];
        [self keyBoardHidden];
    }];
    
}


//启动拍照
-(void)mxSharePickCamera:(id)sender {
    [[PhotoBrowser shared] camera:self completion:^(id _Nonnull data) {
        if([data isKindOfClass:[NSURL class]]) {
            [self sendVideoMessage:data];
        } else {
            [self sendImageMessage:data];
        }
    }];
}



// 指定回调方法
- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    if(error != NULL){
        //
    }else{
    }
}

#pragma mark - 发送定位
-(void)mxSharePickLocation:(id)sender {
    LocationShareVC *locationVC = [[LocationShareVC alloc]init];
    locationVC.delegate = self;
    locationVC.mapViewType=MXMapViewTypeAppleMap;
    [self.navigationController pushViewController:locationVC animated:YES];
}

// 发位置
-(void)sendCurrentLocation:(CLLocationCoordinate2D)coordinate2D andAddress:(NSString*)address andSubAddress:(NSString *)subAddress screenShot:(UIImage *)screenShot {
    if(screenShot) {
        [self keyBoardHidden];
        [NotifyHelper showHUDAddedTo:self.view animated:YES];
        [[QNManager shared] uploadImage:screenShot completion:^(id data) {
            [NotifyHelper hideHUDForView:self.view animated:YES];
            MessageModel *tempModel = [ChatSendHelper sendLocationMessageToUsername:self.viewModel.conversation.chat_id latitude:coordinate2D.latitude longitude:coordinate2D.longitude address:address andSubAddress:subAddress key:data messageType:self.viewModel.conversation.conversationType creatorRole:self.creatorRole];
            [self addMessage:tempModel];
        }];
    }
}

//启动转账
- (void)tranfer:(id)sender {
    [NotifyHelper showMessageWithMakeText:@"暂未开放"];
}

//启动快捷交易
- (void)deal:(id)sender {
    [self keyBoardHidden];
    [NotifyHelper showMessageWithMakeText:@"暂未开放"];
//    KJBourseVC *bourseVC = [[KJBourseVC alloc] init];
//    [self.navigationController pushViewController:bourseVC animated:YES];
}

//启动我的收藏
- (void)favorite:(id)sender {
    [NotifyHelper showMessageWithMakeText:@"暂未开放"];
}

/*********************************************消息发送******************************************/
#pragma mark - 文本发送
-(void)sendTextMessage:(NSString *)text{
    MessageModel *tempModel;
    //判断是否有@信息
    if ([MXPointAtManager sharedManager].pointAtDataMDic.count > 0) {
        tempModel = [ChatSendHelper sendAtTextMessage:text
                                           toUsername:self.viewModel.conversation.chat_id
                                          messageType:self.viewModel.conversation.conversationType
                                             atUserId:[MXPointAtManager sharedManager].pointAtDataMDic.allKeys
                                          creatorRole:self.creatorRole];
    } else {
       tempModel = [ChatSendHelper sendTextMessage:text
                             toUsername:self.viewModel.conversation.chat_id
                            messageType:self.viewModel.conversation.conversationType creatorRole:self.creatorRole];
    }
    [self addMessage:tempModel];
    [[MXPointAtManager sharedManager] clearAtMessage];
}

#pragma mark - 发送动态表情
-(void)sendGifMessage:(NSDictionary *)dic
{
//    NSDictionary
    MXEmotion *emotion = dic[MX_EMOTION_DEFAULT_EXT];
    MessageModel* tempModel = [ChatSendHelper sendFaceEmotionMessage:emotion.emotionTitle package:emotion.emotionId toUsername:self.viewModel.conversation.chat_id messageType:self.viewModel.conversation.conversationType creatorRole:self.creatorRole];
    [self addMessage:tempModel];
}

#pragma mark - 发送视频
-(void)sendVideoMessage:(NSURL *)videoUrl
{
    MessageModel* tempModel = [ChatSendHelper sendVideoMessage:videoUrl
                                                    toUsername:self.viewModel.conversation.chat_id
                                                   messageType:self.viewModel.conversation.conversationType delegate:self creatorRole:self.creatorRole];
    [self addMessage:tempModel];
}

#pragma mark - 发送图片
-(void)sendImageMessage:(UIImage *)imageMessage
{
    MessageModel* tempModel = [ChatSendHelper sendImageMessage:[imageMessage fixOrientation]
                                                    toUsername:self.viewModel.conversation.chat_id
                                                   messageType:self.viewModel.conversation.conversationType
                                                      delegate:self creatorRole:self.creatorRole];
    [self addMessage:tempModel];
}

#pragma mark - 发送文件
- (void)sendFileMessage:(MXDocument *)doc {
    NSString *toUser = self.viewModel.conversation.chat_id;
    EMConversationType type = self.viewModel.conversation.conversationType;
    MessageModel* tempModel = [ChatSendHelper sendFileMessagetoUsername:toUser
                                                            messageType:type
                                                                docFile:doc
                                                               delegate:self creatorRole:self.creatorRole];
    [self addMessage:tempModel];
}
    
-(MessageModel*)searchMsgByID:(NSString* )msgId {
    for (MessageModel* msg in self.chatView.viewModel.recordsArray) {
        if ([msg isKindOfClass:[MessageModel class]]) {
            if ([msg.messageId isEqualToString:msgId]) {
                return msg;
            }
        }
        
    }
    return nil;
}

#pragma mark - private

- (BOOL)canRecord
{
    __block BOOL bCanRecord = YES;
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending)
    {
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        if ([audioSession respondsToSelector:@selector(requestRecordPermission:)]) {
            [audioSession performSelector:@selector(requestRecordPermission:) withObject:^(BOOL granted) {
                bCanRecord = granted;
            }];
        }
    }
    
    return bCanRecord;
}

#pragma mark - 添加消息到列表上
-(void)addMessage:(MessageModel *)message
{
    @weakify(self)
    dispatch_async(dispatch_get_main_queue(), ^{
        @strongify(self)
        NSArray *messages = [self.viewModel addTimeLabel:@[message]];
        for (int i = 0; i < messages.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatView.viewModel.recordsArray.count+i inSection:0];
            [self.indexPaths safeAddObj:indexPath];
        }
        
        [self.viewModel.recordsArray addObjectsFromArray:messages];
        if (message.msg_direction) {
            [self.chatView.msgRecordTable reloadData];
            [self.chatView.msgRecordTable scrollToRowAtIndexPath:[self.indexPaths lastObject] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        } else {
            ///如果不在底部 则不滑动到底部 [防止查看历史消息的时候 来新消息页面滚动的情况]
            if ([self.chatView.msgRecordTable isScrollToBottom]) {
                [self.chatView.msgRecordTable reloadData];
                [self.chatView.msgRecordTable scrollToRowAtIndexPath:[self.indexPaths lastObject] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            } else {
                [self.chatView.msgRecordTable reloadData];
            }
        }
        [self.indexPaths removeAllObjects];
    });
    
}

#pragma mark - 更新发送消息的状态
- (void)updateMessageState:(MessageModel*)message cellRow:(NSString*)cellRow controller:(ChatViewVC*) chatVC{
    if (message) {
        dispatch_barrier_async(_messageQueue, ^(){
            [chatVC.chatView.viewModel.recordsArray safeReplaceObjectAtIndex:[cellRow intValue] withObject:message];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSIndexPath* path = [NSIndexPath indexPathForRow:[cellRow intValue] inSection:0];
                [chatVC.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
            });
        });
    }
}


#pragma mark - UIResponder actions - cell点击查看详情

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    
    [self.chatToolBar endEditing:NO];
    MessageModel *model = [userInfo objectForKey:@"message"];
    
    if ([eventName isEqualToString:kRouterEventAvatarBubbleTapEventName]) {
        [self chatAvatarCellPressed:model];
    }else if ([eventName isEqualToString:kRouterEventImageBubbleTapEventName]){
        [self chatImageCellBubblePressed:model];
    }else if ([eventName isEqualToString:kRouterEventLocationBubbleTapEventName]){
        [self chatLocationCellBubblePressed:model];
    }else if ([eventName isEqualToString:kRouterEventVideoBubbleTapEventName]){
        [self chatVideoCellBubblePressed:model imageView:[userInfo objectForKey:@"custom"]];
    }else if ([eventName isEqualToString:kResendButtonTapEventName]){
        @weakify(self)
        [UIActionSheet actionSheetWithTitle:nil  message:nil buttons:@[MXLang(@"LoginRegister_resend_tip_2", @"重新发送")] showInView:self.view onDismiss:^(NSInteger buttonIndex) {
            @strongify(self);
            UITableViewCell* cell = [userInfo objectForKey:kShouldResendCell];
            NSIndexPath* path = [self.chatView.msgRecordTable indexPathForCell:cell];
            model.state=kMXMessageStateSending;
            [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
            
            [ChatSendHelper reSendMessage:model delegate:self];
        } onCancel:^{
            
        }];
    }else if ([eventName isEqualToString:kRouterEventAudioBubbleTapEventName]){
        [self chatAudioCellBubblePressed:model];
    }else if([eventName isEqualToString:kRouterEventInviteBubbleTapEventName]){
    
    }else if ([eventName isEqualToString:kRouterEventAudioBubbleTapEventName]){
        [self chatAudioCellBubblePressed:model];
    }else if(([eventName isEqualToString:kRouterEventRedPacketBubbleTapEventName])){
        [self chatRedPacketBubblePressed:model];
    }else if(([eventName isEqualToString:kRouterEventCardBubbleTapEventName])){
        //名片
         [self chatCardCellPressed:model];
    }else if(([eventName isEqualToString:kRouterEventVoiceChatBubbleTapEventName])){
        [self chatVoiceChat:model];
    }else if(([eventName isEqualToString:kRouterEventFileBubbleTapEventName])){
        ///文件预览
        [self chatFilePreview:model];
    }else if(([eventName isEqualToString:kRouterEventProductInfoChatBubbleTapEventName])){
        [self chatProductInfoCellPressed:model];
    }
    
    
}
#pragma mark - 文件预览
- (void)chatFilePreview:(MessageModel *)model {
    if (![model isKindOfClass:MessageModel.class]) {
        return;
    }
    if (model.subtype != kMXMessageTypeFile) {
        return;
    }
    MXRoute(@"FilePreviewVC", (@{@"meaage":model}))
}


- (void)chatVoiceChat:(MessageModel *)model {
    if (![model isKindOfClass:MessageModel.class]) {
        return;
    }
    if (model.subtype != kMXMessageTypeVoiceChat) {
        return;
    }
    if (model.chatType == eConversationTypeChat) {
        [self mxVoice];
    } else {
        [NotifyHelper showMessageWithMakeText:@"目前语音通话只支持单聊"];
    }
}

-(void)chatCardCellPressed:(MessageModel *)model {
    NSDictionary* cardDict =  [MXJsonParser jsonToDictionary:model.body];
    [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":cardDict[@"attr1"],@"group_id":self.viewModel.conversation.chat_id}];
}

-(void)chatAvatarCellPressed:(MessageModel *)model {
    [MXRouter openURL:@"lcwl://PersonalDetailVC" parameters:@{@"other_id":model.fromID,@"group_id":self.viewModel.conversation.chat_id,@"creatorRole":@(self.creatorRole)}];
}

-(void)chatProductInfoCellPressed:(MessageModel *)model {
    MXRoute(@"GoodInfoVC", @{@"goods_id":model.attr1 ?: @""});
}

#pragma mark - 查看图片详情
// 图片的bubble被点击
-(void)chatImageCellBubblePressed:(MessageModel *)model {
    
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] initWithToolbarClass:[MXMoMessageToolbar class]];
    NSMutableArray *photos = [NSMutableArray array];
    @autoreleasepool {
        NSInteger index = [self.chatView.viewModel.recordsArray indexOfObject:model];
        NSIndexPath *sourceIndexPath = [NSIndexPath indexPathForItem:index inSection:0];
        ChatViewCell* cell = (ChatViewCell*)[self.chatView.msgRecordTable cellForRowAtIndexPath:sourceIndexPath];
        ChatBaseBubbleView* bubbleView = cell.bubbleView;
        ChatImageBubbleView* tmp = (ChatImageBubbleView*)bubbleView;
        UIImageView* sourceImageview = tmp.imageView;
        NSInteger selectedIndex = 0;
        
        for (int i = 0; i <  self.chatView.viewModel.recordsArray.count ; i++) {
            MessageModel* msg = [self.chatView.viewModel.recordsArray objectAtIndex:i];
            if (msg == model) {
                selectedIndex = photos.count;
            }
            if ([msg isKindOfClass:[MessageModel class]]) {
                if (msg.subtype == 2) {
                    MJPhoto *photo = [[MJPhoto alloc] init];
                    if (![StringUtil isEmpty:msg.locFileUrl]) {
                        NSString *rootPath = [[ChatCacheFileUtil sharedInstance]userDocPath];
                        NSString *path = [msg.locFileUrl lastPathComponent];
                        NSString *localUrl = StrF(@"%@/%@", rootPath, path);
                        NSString *imagePath = [[ChatCacheFileUtil sharedInstance]handleChatImagePath:localUrl remoteUrl:msg.imageRemoteUrl];
                        UIImage* image =  [UIImage imageWithContentsOfFile:imagePath];
                        if (image == nil) {
                            photo.url = [NSURL URLWithString:[self appendImgPath:msg.imageRemoteUrl]];
                            photo.placeholder = nil;
                        }else{
                            photo.image = image;
                            photo.placeholder = image;
                        }
                    }else{
                        photo.url = [NSURL URLWithString:[self appendImgPath:msg.imageRemoteUrl]];
                        photo.placeholder = nil;
                    }
                    photo.srcImageView = sourceImageview;
                    [photos safeAddObj:photo];
                }
            }
        }
        browser.photos = photos;
        browser.currentPhotoIndex = selectedIndex;
        [browser show];
    }
}

#pragma mark - 查看视频
// 视频的bubble被点击
-(void)chatVideoCellBubblePressed:(MessageModel *)model imageView:(UIImageView *)imageView {
    if ([model messageFileExists]) {
        [[VideoPlayManager share] playLocal:model.fullPath superView:imageView];
    } else {
        //下载 下次就可以加载本地视频了,不需要再下载
        [TalkManager downloadFile:model completion:^(BOOL success, NSString *error) {
            
        }];
        //播放
        [[VideoPlayManager share] play:model.urlString superView:imageView];
    }
}

#pragma mark - 查看地图详情
- (void)chatLocationCellBubblePressed:(MessageModel *)model {
    
    [self keyBoardHidden];
    __block LocationNavigateVC *navVC=[[LocationNavigateVC alloc] init];
    navVC.city=MXLang(@"Talk_controller_locationVC_title_5", @"位置分享");
    navVC.address=model.addr;
    navVC.toCoordinate2D=CLLocationCoordinate2DMake([model.lat doubleValue], [model.lng doubleValue]);
    MLog(@"apple map");
    navVC.mapViewType=MXMapViewTypeAppleMap;

    [self.navigationController pushViewController:navVC animated:YES];
}

#pragma mark - 语音播放
- (void)chatAudioCellBubblePressed:(MessageModel *)model {
    //当前播放的消息 再次播放的时候要停止之前的播放
    if (self.playMsg != nil) {
        self.playMsg.isPlaying = 0;
          NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:self.playMsg];
        if (cellIndex == -1) {
            return ;
        }
        [self.chatView.viewModel.recordsArray replaceObjectAtIndex:cellIndex withObject:self.playMsg];
        NSIndexPath* path = [NSIndexPath indexPathForRow:cellIndex inSection:0];
        [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
        if ([self.playMsg.messageId isEqualToString:model.messageId]) {
            [[MXDeviceMediaManager sharedInstance].deviceMedia stopPlaying];
            self.playMsg = nil;
            return;
        }
    }
    self.playMsg = model;
    self.playMsg.isPlaying = 1;
    NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:self.playMsg];
    if (cellIndex == -1) {
        return ;
    }
    @weakify(self)
    if (self.playMsg.isPlay == 0) {
        self.playMsg.isPlay = 1;
        [[MXChatDBUtil sharedDataBase]updateMessagePlayState:self.playMsg];
    }
    [self.chatView.viewModel.recordsArray replaceObjectAtIndex:cellIndex withObject:self.playMsg];
    NSIndexPath* path = [NSIndexPath indexPathForRow:cellIndex inSection:0];
    [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
    if ([self.playMsg messageFileExists]) {
        [self playAudios];
    } else {
        [TalkManager downloadAudioFileByMessage:self.playMsg
                                     completion:^(BOOL success, NSString *error) {
                                        @strongify(self)
            Block_Exec_Main_Async_Safe(^{
                if (success) {
                    self.playMsg.locFileUrl = error;
                    [[MXChatDBUtil sharedDataBase] updateRowDataByMsgModel:self.playMsg];
                    [self playAudios];
                }
            });
        }];
    }
    
}

-(void)grabChatRedPacket:(MessageModel *)model {
    //收
//    if (model.msg_direction == 0) {
    [NotifyHelper showHUD];
    NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:model.body];
    NSString *redPacketId = [bodyDict valueForKeyPath:@"attr3"];
    if([redPacketId isKindOfClass:[NSDictionary class]]) {
        redPacketId = [redPacketId valueForKey:@"redPacketId"];
    }
    [RedPacketManager getReceiveStatus:@{@"id":redPacketId} completion:^(id object,NSString *error) {
        [NotifyHelper hideHUD];
        if([object isSuccess]) {
            //状态 0不可领 1可领取 2已领取 3已抢光 4已过期
            NSInteger status = [[object valueForKeyPath:@"data.status"] integerValue];
            if (status == 2 || status == 3) {
                model.isPlay = 1;
                if (status == 3) {
                    model.isPlay = 2;
                }
                [[MXChatDBUtil sharedDataBase]updateMessagePlayState:model];
                NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:model];
                [self.chatView.viewModel.recordsArray replaceObjectAtIndex:cellIndex withObject:model];
                NSIndexPath* path = [NSIndexPath indexPathForRow:cellIndex inSection:0];
                [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
            }
            if(status == 2 || (status == 1 && model.msg_direction == 1) || (status == 3 && model.msg_direction == 1)){
                RedPacketDetailVC* vc = [[RedPacketDetailVC alloc]init];
                NSDictionary* dict = [MXJsonParser jsonToDictionary:model.body];
                vc.orderId = dict[@"attr3"];
                vc.acceType = model.chatType;
                [self.navigationController pushViewController:vc animated:YES];
                return ;
            }else{
                NSString* content = bodyDict[@"attr2"];
                if (status != 1) {
                    content = error;
                }
                NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:model.body];
                WSRewardConfig *info = ({
                    WSRewardConfig *info   = [[WSRewardConfig alloc] init];
                    info.money         = 100.0;
                    info.avatarImage    = model.avatar;
                    info.content = content;
                    info.userName  = model.name;
                    info.redPacketId = bodyDict[@"attr3"];
                    info.status = status;//[object intValue];
                    info.model = model;
                    info;
                });
                [WSRedPacketView showRedPackerWithData:info cancelBlock:^{
                    NSLog(@"取消领取");
                } finishBlock:^(float money) {
                    NSLog(@"领取金额：%f",money);
                    RedPacketDetailVC* vc = [[RedPacketDetailVC alloc]init];
                    NSDictionary* dict = [MXJsonParser jsonToDictionary:model.body];
                    vc.orderId = dict[@"attr3"];
                    vc.acceType = model.chatType;
                    [self.navigationController pushViewController:vc animated:YES];
                    model.isPlay = 1;
                    [[MXChatDBUtil sharedDataBase]updateMessagePlayState:model];
                    NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:model];
                    [self.chatView.viewModel.recordsArray replaceObjectAtIndex:cellIndex withObject:model];
                    NSIndexPath* path = [NSIndexPath indexPathForRow:cellIndex inSection:0];
                    [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
                }detailBlock:^{
                    RedPacketDetailVC* vc = [[RedPacketDetailVC alloc]init];
                    NSDictionary* dict = [MXJsonParser jsonToDictionary:model.body];
                    vc.orderId = dict[@"attr3"];
                    vc.acceType = model.chatType;
                    [self.navigationController pushViewController:vc animated:YES];
                }];
            }
        }
    }];
//    }
//    //发
//    else{
//        RedPacketDetailVC* vc = [[RedPacketDetailVC alloc]init];
//        NSDictionary* dict = [MXJsonParser jsonToDictionary:model.body];
//        vc.orderId = dict[@"attr3"];
//        vc.acceType = model.chatType;
//        [self.navigationController pushViewController:vc animated:YES];
//    }
}

-(void)grabGroupChatRedPacket:(MessageModel *)model{
    [NotifyHelper showHUD];
    NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:model.body];
    NSString *redPacketId = [bodyDict valueForKeyPath:@"attr3"];
    
    if([redPacketId isKindOfClass:[NSDictionary class]]) {
        redPacketId = [redPacketId valueForKey:@"redPacketId"];
    }
    [RedPacketManager getReceiveStatus:@{@"id":redPacketId ?: @""} completion:^(id object,NSString *error) {
        [NotifyHelper hideHUD];
        if([object isSuccess]) {
            //状态 0不可领 1可领取 2已领取 3已抢光 4已过期
            NSInteger status = [[object valueForKeyPath:@"data.status"] integerValue];
            if (status == 2 || status == 3) {
                model.isPlay = 1;
                if (status == 3) {
                    model.isPlay = 2;
                }
                [[MXChatDBUtil sharedDataBase]updateMessagePlayState:model];
                NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:model];
                [self.chatView.viewModel.recordsArray replaceObjectAtIndex:cellIndex withObject:model];
                NSIndexPath* path = [NSIndexPath indexPathForRow:cellIndex inSection:0];
                [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
            }
            if(status == 2){
                RedPacketDetailVC* vc = [[RedPacketDetailVC alloc]init];
                NSDictionary* dict = [MXJsonParser jsonToDictionary:model.body];
                vc.orderId = dict[@"attr3"];
                vc.acceType = model.chatType;
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                NSString* content = bodyDict[@"attr2"];
                if (status != 1) {
                    content = error;
                }
                NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:model.body];
                WSRewardConfig *info = ({
                    WSRewardConfig *info   = [[WSRewardConfig alloc] init];
                    info.money         = 100.0;
                    info.avatarImage    = model.avatar;
                    info.content = content;
                    info.userName  = model.name;
                    info.redPacketId = bodyDict[@"attr3"];
                    info.status = status;//[object intValue];
                    info.model = model;
                    info;
                });
                [WSRedPacketView showRedPackerWithData:info cancelBlock:^{
                    NSLog(@"取消领取");
                } finishBlock:^(float money) {
                    model.isPlay = 1;
                    [[MXChatDBUtil sharedDataBase]updateMessagePlayState:model];
                    NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:model];
                    [self.chatView.viewModel.recordsArray replaceObjectAtIndex:cellIndex withObject:model];
                    NSIndexPath* path = [NSIndexPath indexPathForRow:cellIndex inSection:0];
                    [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
                    
                    RedPacketDetailVC* vc = [[RedPacketDetailVC alloc]init];
                    NSDictionary* dict = [MXJsonParser jsonToDictionary:model.body];
                    vc.orderId = dict[@"attr3"];
                    vc.acceType = model.chatType;
                    [self.navigationController pushViewController:vc animated:YES];
                }detailBlock:^{
                    RedPacketDetailVC* vc = [[RedPacketDetailVC alloc]init];
                    NSDictionary* dict = [MXJsonParser jsonToDictionary:model.body];
                    vc.orderId = dict[@"attr3"];
                    vc.acceType = model.chatType;
                    [self.navigationController pushViewController:vc animated:YES];
                }];
            }
        }
    }];
}

- (void)chatRedPacketBubblePressed:(MessageModel *)model {
    if (model.chatType == eConversationTypeChat) {
        [self grabChatRedPacket:model];
    }else{
        [self grabGroupChatRedPacket:model];
    }
}

-(void)playAudios {
    if (![self.playMsg messageFileExists]) {
        return;
    }
    NSString *voicePlayModel = [MXCache valueForKey:[NSString stringWithFormat:@"ControlSpeak_%@",UDetail.user.chatUser_id]];
    BOOL boolFlag = [voicePlayModel isEqualToString:@"1"]?false:true;
    NSString *audioPath = self.playMsg.fullPath;
    @weakify(self)
    [[MXDeviceMediaManager sharedInstance].deviceMedia playAudioWithPath:audioPath
                                                          speakerEnabled:boolFlag
                                                              completion:^(NSError *error) {
        @strongify(self)
         [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
         NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:self.playMsg];
         if (cellIndex == -1) {
             return ;
         }
         self.playMsg.isPlaying = 0;
         [self.chatView.viewModel.recordsArray replaceObjectAtIndex:cellIndex withObject:self.playMsg];
         NSIndexPath* path = [NSIndexPath indexPathForRow:cellIndex inSection:0];
         [self.chatView.msgRecordTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:path, nil] withRowAnimation:UITableViewRowAnimationNone];
         MessageModel* msg = [self nextVoiceMessageObjectFrom:self.playMsg];
         self.playMsg = nil;
         if (msg != nil) {
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 @strongify(self)
                 [self chatAudioCellBubblePressed:msg];
             });
         }
     }];
}

- (MessageModel*)nextVoiceMessageObjectFrom:(MessageModel*)msg {
    NSInteger index = [self.chatView.viewModel.recordsArray indexOfObject:msg];
    if (index == (self.chatView.viewModel.recordsArray.count-1)) {
        return nil;
    }
    for (NSInteger i = index+1; i<self.chatView.viewModel.recordsArray.count; i++) {
        MessageModel* message = self.chatView.viewModel.recordsArray[i];
        if ([message isKindOfClass:[MessageModel class]]) {
            if (message.type == MessageTypeNormal || message.type == MessageTypeGroupchat) {
                if (message.subtype == kMXMessageTypeVoice && message.isPlay == 0) {
                    return message;
                }
            }
        }
        
    }
    return nil;
}

- (void)qrcodeMenuAction:(NSArray *)modeArray {
    MessageModel* msg = modeArray[0];
    UIImage* image = [UIImage imageWithContentsOfFile:msg.locFileUrl];
    CGImageRef ref = image.CGImage;
    CIDetector*detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{ CIDetectorAccuracy : CIDetectorAccuracyHigh }];     //2. 扫描获取的特征组
    NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:ref]];
    if (features.count == 0) {
        [NotifyHelper showMessageWithMakeText:@"二维码错误"];
        return;
    }
    //3. 获取扫描结果
    CIQRCodeFeature *feature = [features objectAtIndex:0];
    
    NSString *scannedResult = feature.messageString;
    if (![StringUtil isEmpty:scannedResult]) {
        [[[MXBarReaderHelper alloc]init]dealWithReadingData:scannedResult];
    }
    
}

#pragma mark - 转发delegate
- (void)forwardMessage:(NSArray *)modeArray {
    ChatForwadVC* forwardVC = [[ChatForwadVC alloc] init];
    forwardVC.messageModel = [[modeArray objectAtIndex:0] copy];
    forwardVC.callback = ^(id  _Nonnull sender, MessageModel * _Nonnull message) {
        message.msg_direction = 1;
        NSString* forwardMessage = @"";
        UserModel* user = [LcwlChat shareInstance].user;
        
        message.avatar = user.head_photo;
        message.name = user.user_name;
        if ([StringUtil isEmpty:message.name]) {
            message.name = user.nickname;
        }
        if ([sender isKindOfClass:[FriendModel class]]) {
            FriendModel* friend = (FriendModel*)sender;
            forwardMessage = friend.name;
            message.fromID = user.chatUser_id;
            message.toID = friend.userid;
            message.chat_with = friend.userid;
            message.chatType = eConversationTypeChat;
            forwardMessage = friend.name;
        }else if([sender isKindOfClass:[ChatGroupModel class]]){
            ChatGroupModel* group = (ChatGroupModel*)sender;
            forwardMessage = group.groupName;
            message.fromID = user.chatUser_id;
            message.toID = group.groupId;
            message.chat_with = group.groupId;
            message.chatType = eConversationTypeGroupChat;
            forwardMessage = group.groupName;
        }
        message.messageId = [TalkManager msgId];
        message.sendTime = [ChatSendHelper chatSendTime];
        @weakify(self)
        
        [MXAlertViewHelper showAlertViewWithMessage:forwardMessage title:@"确定发送给:" okTitle:@"确定" cancelTitle:@"取消" completion:^(BOOL cancelled, NSInteger buttonIndex) {
            @strongify(self);
            if (buttonIndex == 1) {
                [self.viewModel sendForwardMessage:message];
                if ([self.viewModel.conversation.chat_id isEqualToString:message.toID]) {
                    [self addMessage:message];
                }
            }
        }];
    };
    UINavigationController* nav = [[UINavigationController alloc]initWithRootViewController:forwardVC];
    [self.navigationController presentViewController:nav animated:YES completion:^{
        
    }];
}

- (void)longPressCellBubble:(id)cell {
    if ([self.chatToolBar.inputTextView isFirstResponder]) {
        self.chatToolBar.inputTextView.overrideNextResponder = cell;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuDidHide:) name:UIMenuControllerDidHideMenuNotification object:nil];
    } else {
        [cell becomeFirstResponder];
    }
}

- (void)menuDidHide:(NSNotification*)notification {
    self.chatToolBar.inputTextView.overrideNextResponder = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIMenuControllerDidHideMenuNotification object:nil];
    
    [self.chatToolBar endEditing:YES];
}

- (void)tapPopPreviewEmotion:(id)emotion {
    
    NSDictionary *dic = @{MX_EMOTION_DEFAULT_EXT:emotion};
    
    [self sendGifMessage:dic];
    
    self.popPreviewView.hidden = YES;
    self.chatToolBar.inputTextView.text = @"";
}

#pragma mark - 图片拷贝
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [UIPasteboard removePasteboardWithName:@"CopyImage"];
    UIPasteboard *board = [UIPasteboard pasteboardWithName:@"CopyImage" create:NO];//[UIPasteboard generalPasteboard];
    if (action == @selector(paste:) && (board.images.count > 0) && [(UITextView*)_chatToolBar.inputTextView isFirstResponder])
        return YES;
    return NO;
}

- (void)paste:(id)sender
{
    UIPasteboard *board = [UIPasteboard pasteboardWithName:@"CopyImage" create:NO];//[UIPasteboard generalPasteboard];
    if (board.images.count > 0) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_apply(board.images.count, queue, ^(size_t index) {
            [self sendImageMessage:board.images[index]];
        });
    }
}

// add by yang.xiangbao 2015/7/4
- (UIView *)timeShortView
{
    if (!_timeShortView) {
        _timeShortView = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-200)/2, (SCREEN_HEIGHT - 150)/2, 200, 140)];
        _timeShortView.backgroundColor = [UIColor grayColor];
        _timeShortView.layer.cornerRadius = 5;
        _timeShortView.layer.masksToBounds = YES;
        _timeShortView.alpha = 0.6;
        [self.view addSubview:_timeShortView];
        
        // 声音图标图片
        UIImageView *recordAnimationView = [[UIImageView alloc] initWithFrame:CGRectMake((_timeShortView.bounds.size.width - 66 ) / 2,
                                                                                         25,
                                                                                         66,
                                                                                         66)];
        recordAnimationView.image = [UIImage imageNamed:@"time"];
        [_timeShortView addSubview:recordAnimationView];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(5,
                                                                       _timeShortView.bounds.size.height - 30,
                                                                       _timeShortView.bounds.size.width - 10,
                                                                       25)];
        
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.text = MXLang(@"MoTalk_motalkTheTimeYouRecordTooShort", @"录音时间太短了~");
        [_timeShortView addSubview:textLabel];
        textLabel.font = [UIFont font14];
        textLabel.textColor = [UIColor whiteColor];
        textLabel.layer.cornerRadius = 5;
        textLabel.layer.borderColor = [[UIColor redColor] colorWithAlphaComponent:0.5].CGColor;
        textLabel.layer.masksToBounds = YES;
    }
    
    return _timeShortView;
}

- (void)showTimeShortView
{
    @weakify(self)
    self.timeShortView.alpha = 0;
    [self.view addSubview:self.timeShortView];
    [UIView animateWithDuration:.25f animations:^{
        @strongify(self)
        self.timeShortView.alpha = .6f;
    } completion:^(BOOL finished) {
        @strongify(self)
        [self performSelector:@selector(removeTimeShortViewFromSuperview:) withObject:self.timeShortView afterDelay:1.0f];
    }];
}
-(void)removeTimeShortViewFromSuperview:(UIView*)shortView{
    [shortView removeFromSuperview];
    UIView *view = [self.navigationController.view viewWithTag:K_Tag_ChatView_Forbid];
    if(view){
        [view removeFromSuperview];
    }
    
}
// the end



//上传失败的回调
-(void)uploadMessageFailureCallback:(MessageModel*)message{
    NSInteger cellIndex = [TalkManager searchMessage:self.chatView.viewModel.recordsArray withModel:message];
    if (cellIndex == -1) {
        return ;
    }
    [self updateMessageState:message cellRow:[NSString stringWithFormat:@"%ld",(long)cellIndex] controller:self];
    
}

- (void)setProductInfo:(NSDictionary *)productInfo {
    _productInfo = productInfo;
    self.productLinkView.param = productInfo;
}

- (void)showProductLinkView {
    [self.view addSubview:self.productLinkView];
    self.productLinkView.param = self.productInfo;
    [self.productLinkView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(12));
        make.right.equalTo(@(-12));
        make.height.equalTo(@(153));
        make.bottom.equalTo(self.chatToolBar.mas_top).offset(-10);
    }];
    
    @weakify(self)
    self.productLinkView.sendBlock = ^(id data) {
        @strongify(self)
        [self sendProductLink:data];
    };
}

- (void)dealloc
{
    MLog(@"chatviewVC dealloc: %@",self);
    _chatToolBar.delegate=nil;
    _chatToolBar=nil;
    [_chatView.viewModel.recordsArray removeAllObjects];
    [_indexPaths removeAllObjects];
    _indexPaths=nil;
    [self removeNotification];
    [_kvoController unobserve:self];
    //[[LcwlChat shareInstance].chatManager removeDelegate:self];
}

- (NSMutableArray*)emotionDataSource {
    if (!_emotionDataSource) {
        _emotionDataSource = [NSMutableArray array];
    }
    return _emotionDataSource;
}

- (NSMutableArray*)emotionPackages {
    if (!_emotionPackages) {
        _emotionPackages = [NSMutableArray array];
    }
    return _emotionPackages;
}

- (MXPopPreviewView*)popPreviewView {
    if (!_popPreviewView) {
        _popPreviewView = [[MXPopPreviewView alloc] initWithPreviewFrame:CGRectMake(0, 0, 91, 95)];
        _popPreviewView.hidden = YES;
        _popPreviewView.delegate = self;
    }
    
    return _popPreviewView;
}

-(void)audioPlayAction:(MessageModel *)model{
      [self chatAudioCellBubblePressed:model];
   
    NSString* speak = [MXCache valueForKey:[NSString stringWithFormat:@"ControlSpeak_%@", [LcwlChat shareInstance].user.chatUser_id]];
    if ([speak isEqualToString:@"0"]) {
        speak = @"当前为扬声器播放模式";
    }else{
        speak = @"当前为听筒播放模式";
    }
}


- (ProductLinkView *)productLinkView{
    if(!_productLinkView){
        _productLinkView = ({
            ProductLinkView * object = [[ProductLinkView alloc]init];
            object;
       });
    }
    return _productLinkView;
}
@end
