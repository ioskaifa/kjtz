//
//  ChatViewVC.h
//  MoPal_Developer
//
//  聊天界面
//  Created by aken on 15/3/23.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "BaseViewController.h"
#import "MXConversation.h"
#import "MXChatDefines.h"

@interface ChatViewVC : BaseViewController

///和客服聊天,11代表客服发给用户  ，10代表用户发给客服，
@property(nonatomic,assign) NSInteger creatorRole;
@property(nonatomic,copy) NSDictionary *productInfo;

//转发
- (void)forwardMessage:(NSArray*)model;

//得到当前聊天的conversation
-(MXConversation *)getCurrentConversation;
- (instancetype)initWithChatter:(NSString *)chatter conversationType:(EMConversationType)type;
- (void)loadMoreMessages;
-(void)sendTextMessage:(NSString *)text;
@end
