//
//  AskCallVC.h
//  BOB
//
//  Created by mac on 2020/7/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "MXConversation.h"

typedef NS_ENUM(NSInteger, AskCallType) {
    ///语音发起样式
    AskCallTypePost    = 0,
    ///语音接收样式
    AskCallTypeGet     = 1,
};

NS_ASSUME_NONNULL_BEGIN

@interface AskCallVC : BaseViewController

@property (nonatomic, assign) AskCallType askType;

@property (nonatomic, copy) NSString * toUserId;

@property (nonatomic, assign) EMConversationType messageType;
///提示连接中
@property (nonatomic, strong) UILabel *connectLbl;

@end

NS_ASSUME_NONNULL_END
