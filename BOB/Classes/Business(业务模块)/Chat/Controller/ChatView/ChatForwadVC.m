//
//  MobileContactVC.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "ChatForwadVC.h"

#import "FriendModel.h"
#import "ContactHelper.h"
#import "FriendListView.h"
#import "LSearchBar.h"
#import "LcwlChat.h"
#import "SearchAddressVC.h"
#import "AddFriendListCell.h"
#import "SearchGroupVC.h"
#import "GroupListVC.h"
#import "ChatGroupModel.h"
#import "MXConversation.h"
#import "SearchRecentlyGroupListVC.h"
#import "ChatGroupModel.h"
#import "RoomManager.h"
#import "AddFriendTopView.h"

static const CGFloat headerHeight = 22;
static const CGFloat rowHeight = 60;
@interface ChatForwadVC ()<UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating,UISearchBarDelegate>
@property (nonatomic, strong) MXBackButton                 *rightButton;
/** 列表 */
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView* tableHeadView;

@property (nonatomic, strong) UINavigationController *searchNavigationController;
/** 搜索框 */
@property (nonatomic, strong) UISearchController *searchController;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchGrayImage;
/** 搜索框背景颜色 */
@property (nonatomic, strong) UIImage *searchWhiteImage;

@property (nonatomic, strong) NSMutableArray *selectArray;

@property (nonatomic, strong)  NSArray* dataSource;

@property (nonatomic, strong) AddFriendTopView *searchView;

@end

@implementation ChatForwadVC
-(void)viewDidLoad{
    [super viewDidLoad];
    [self setNavBarTitle:Lang(@"转发至")];
    NSMutableArray* array = [[NSMutableArray alloc]initWithCapacity:10];
    [[[LcwlChat shareInstance].chatManager conversations] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MXConversation* con = (MXConversation* )obj;
        if (con.conversationType == eConversationTypeChat || con.conversationType == eConversationTypeGroupChat) {
            [array addObject:con];
        }
    }];
    //加载群组信息
    _dataSource =  array;
    self.isShowBackButton = YES;
    self.selectArray = [[NSMutableArray alloc]initWithCapacity:10];
    [self initUI];
    [self initData];
    AdjustTableBehavior(self.tableView)
}

- (UIView *)tableHeadView{
    if (!_tableHeadView) {
        _tableHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, [AddFriendTopView viewHeight]+rowHeight)];
        _tableHeadView.backgroundColor = [UIColor moBackground];
        [_tableHeadView addSubview:self.searchView];
        FriendListView* view = [[FriendListView alloc]initWithFrame:CGRectMake(0,  self.searchView.height + 5, SCREEN_WIDTH, rowHeight)];
        view.backgroundColor = [UIColor clearColor];
        view.selectCallback = ^(UIView *view){
            //选择一个群
            [MXRouter openURL:@"lcwl://GroupChatVC" parameters:@{@"callback":^(NSArray* selectArray){
                //创建群
                if (selectArray.count > 1) {
                    [self forwadToUsers:selectArray];
                }else{
                    id model = selectArray[0];
                    if ([model isKindOfClass:[ChatGroupModel class]]) {
                        [self forwadToGroup:selectArray];
                    }else if([model isKindOfClass:[FriendModel class]]){
                        [self forwadToUser:selectArray];
                    }
                }
            }}];
        };
        [view reloadLocalLogo:@"new_chat_icon" name:@"创建新的聊天"];
        
        MXSeparatorLine* line = [MXSeparatorLine initHorizontalLineWidth:SCREEN_WIDTH orginX:0 orginY:rowHeight-1
                                 ];
        [view addSubview:line];
        [view setShowDownLine:NO];
        [_tableHeadView  addSubview:view];
    }
    return _tableHeadView;
}

-(void)forwadToUser:(NSArray* )users{
    FriendModel* model = users[0];
    MXConversation* con = [[MXConversation alloc]init];
    con.conversationType = eConversationTypeChat;
    con.chat_id = model.userid;
    [[LcwlChat shareInstance].chatManager createConversationObject:con];
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.callback) {
            self.callback(model, self.messageModel);
        }
    }];
}

-(void)forwadToGroup:(NSArray* )groups{
    ChatGroupModel* model = groups[0];
    MXConversation* con = [[MXConversation alloc]init];
    con.conversationType = eConversationTypeGroupChat;
    con.chat_id = model.groupId;
    [[LcwlChat shareInstance].chatManager createConversationObject:con];
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.callback) {
            self.callback(model, self.messageModel);
        }
    }];
}

-(void)forwadToUsers:(NSArray* )selectArray{
    UserModel* user = [LcwlChat shareInstance].user;
    __block NSMutableArray* idArray = [[NSMutableArray alloc]initWithCapacity:10];
    [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        FriendModel* friend = (FriendModel*)obj;
        [idArray addObject:friend.userid];
    }];
//    NSData *data=[NSJSONSerialization dataWithJSONObject:idArray options:NSJSONWritingPrettyPrinted error:nil];
//
//    NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
//    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    [RoomManager createGroup:@{@"user_ids":[idArray componentsJoinedByString:@","] ?: @""} completion:^(id group, NSString *error) {
        if (group) {
            ChatGroupModel* model = (ChatGroupModel*)group;
            [[LcwlChat shareInstance].chatManager insertGroup:model];
            
            FriendModel* selfModel = [user toFriendModel];
            selfModel.groupid = model.groupId;
            selfModel.groupNickname = selfModel.name;
            [[LcwlChat shareInstance].chatManager insertGroupMember:selfModel];
            [selectArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                FriendModel* friend = (FriendModel*)obj;
                friend.groupid = model.groupId;
                friend.groupNickname = friend.name;
                [[LcwlChat shareInstance].chatManager insertGroupMember:friend];
            }];
            [self dismissViewControllerAnimated:YES completion:^{
                if (self.callback) {
                    self.callback(model, self.messageModel);
                }
            }];
        }else{
                [NotifyHelper showMessageWithMakeText:error];
            
        }
    }];
      
      
}

-(void)initUI{
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.definesPresentationContext = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view addSubview:self.tableView];
    @weakify(self)
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
}

-(void)initData{
   
    [self.tableView reloadData];
}

//消息列表
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.tableHeadView;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor whiteColor];
        AdjustTableBehavior(_tableView);
    }
    return _tableView;
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
        view.userInteractionEnabled = NO;
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        
    }
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
    [tmpView setShowArrow:NO];
    MXConversation* model = [_dataSource objectAtIndex:indexPath.row];
    if (model.conversationType == eConversationTypeChat) {
        FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:model.chat_id];
        [tmpView reloadLogo:friend.avatar name:friend.name];
    }else if(model.conversationType == eConversationTypeGroupChat){
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:model.chat_id];
        [tmpView reloadLogo:group.groupAvatar name:group.groupName];
    }
    return cell;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowHeight;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id selectedObj = nil;
    MXConversation* model = [_dataSource objectAtIndex:indexPath.row];
    if (model.conversationType == eConversationTypeChat) {
        selectedObj = [[LcwlChat shareInstance].chatManager loadFriendByChatId:model.chat_id];
       
    }else if(model.conversationType == eConversationTypeGroupChat){
        selectedObj = [[LcwlChat shareInstance].chatManager loadGroupByChatId:model.chat_id];
        
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.callback) {
            self.callback(selectedObj, self.messageModel);
        }
    }];
   
}

#pragma mark - Table view delegate


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    // Create label with section title
    UILabel *label=[[UILabel alloc] init];
    label.frame=CGRectMake(12, (headerHeight-20)/2, 300, 20);
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor blackColor];
    label.font=[UIFont fontWithName:@"Helvetica-Bold" size:14];
    label.text= @"最近聊天";
    
    // Create header view and add label as a subview
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
    [sectionView setBackgroundColor:[UIColor whiteColor]];
    [sectionView addSubview:label];
    
    return sectionView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return headerHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - UISearchBarDelegate


- (void)willPresentSearchController:(UISearchController *)searchController {
    
    //self.tableView.contentInset = UIEdgeInsetsMake(100, 0, 0, 0);
    //[self.view layoutIfNeeded];
    //[self.tableView setContentOffset:CGPointMake(0, 0)];
    //self.tableView.frame = CGRectMake(0, 100, SCREEN_WIDTH, self.view.height);
    //    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
    //        make.left.right.bottom.equalTo(self.view);
    //        make.top.equalTo(self.view).offset(100);
    //    }];
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    //    [UIView animateWithDuration:0.1 animations:^{
    //        self.tableView.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
    //        [self.tableView setContentOffset:CGPointMake(0, -60)];
    //    }];
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
}

- (UISearchController *)searchController {
    if (!_searchController) {
        SearchRecentlyGroupListVC *resultVC = [[SearchRecentlyGroupListVC alloc] init];
        _searchController = [[UISearchController alloc]initWithSearchResultsController:resultVC];
        _searchController.searchBar.delegate = self;
        _searchController.searchResultsUpdater = resultVC;
        //_searchController.delegate = self;
        _searchController.view.backgroundColor = [UIColor whiteColor];
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = YES;
        //[_searchController.searchBar sizeToFit];
        //_searchController.searchBar.tintColor = [UIColor blackColor];
        _searchController.searchBar.placeholder =  @"搜索";
        [_searchController.searchBar sizeToFit];
        UIOffset offset = {5.0,0};
        _searchController.searchBar.searchTextPositionAdjustment = offset;
        _searchController.searchBar.backgroundImage = self.searchGrayImage;
        [_searchController.searchBar setSearchFieldBackgroundImage:self.searchWhiteImage forState:UIControlStateNormal];
        _searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
        _searchController.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;//关闭提示
        _searchController.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;//关闭自动首字母大写
    }
    return _searchController;
}

-(void)backAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (AddFriendTopView *)searchView {
    if (!_searchView) {
        _searchView = [AddFriendTopView new];
        _searchView.size = CGSizeMake(SCREEN_WIDTH, [AddFriendTopView viewHeight]);
        _searchView.placeholderLbl.text = @"搜索";
        @weakify(self);
        [_searchView addAction:^(UIView *view) {
            @strongify(self);
            SearchRecentlyGroupListVC *resultVC = [[SearchRecentlyGroupListVC alloc] init];
            resultVC.callback = ^(ChatGroupModel * _Nonnull group) {
                [self forwadToGroup:@[group]];
            };
            [self.navigationController pushViewController:resultVC animated:YES];
        }];
    }
    return _searchView;
}

@end
