//
//  ChatForwadVC.h
//  Lcwl
//
//  Created by mac on 2018/12/3.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "MessageModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^SelectResultCallback)(id sender, MessageModel *message);
@interface ChatForwadVC : BaseViewController
@property (nonatomic , strong) SelectResultCallback callback;
@property (nonatomic , strong) MessageModel *messageModel;

@end

NS_ASSUME_NONNULL_END
