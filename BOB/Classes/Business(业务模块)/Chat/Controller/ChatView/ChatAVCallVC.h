//
//  ChatAVCallVC.h
//  BOB
//
//  Created by mac on 2020/7/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JitsiMeet/JitsiMeet.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatAVCallVC : UIViewController <JitsiMeetViewDelegate>

@property (nonatomic, assign) BOOL isGroup;

///开始聊天
- (void)beginMeeting;

@end

NS_ASSUME_NONNULL_END
