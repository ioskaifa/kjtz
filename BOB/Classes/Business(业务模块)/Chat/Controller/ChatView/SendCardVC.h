//
//  SendCardVC.h
//  Lcwl
//
//  Created by mac on 2019/1/4.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"
#import "FriendModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^SelectCallback)(FriendModel *model);

@interface SendCardVC : BaseViewController

@property (nonatomic , copy) NSString *userId;
@property (nonatomic , strong) SelectCallback callback;

@end

NS_ASSUME_NONNULL_END
