//
//  FriendVC.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "SendCardVC.h"
#import "MXBackButton.h"
#import "MXSeparatorLine.h"
#import "ChatFriendCell.h"
#import "FriendListView.h"
#import "SearchLocalFriendVC.h"
#import "FriendViewModel.h"
#import "FriendModel.h"
#import "LcwlChat.h"
#import "ContactHelper.h"
#import "ChatViewVC.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "SearchCardVC.h"
#import "AddFriendTopView.h"

#define personal_details_backbutton_width 60
#define personal_details_backbutton_height 44

static const CGFloat headerHeight = 22;
static const CGFloat rowHeight = 60;

@interface SendCardVC ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) MXBackButton                 *backButton;

@property (nonatomic,strong)  UITableView                  *tb;

@property (nonatomic, strong) AddFriendTopView             *tableHeadView;

@property(nonatomic, strong)  FriendViewModel              *viewModel;

@property(nonatomic, strong)  NSMutableArray               *dataSource;

@property (nonatomic, strong) UISearchController           *searchController;

@end

@implementation SendCardVC

-(void)dealloc{
    MLog(@"dealloc-->%@",NSStringFromClass([self class]));
}

- (void)configCurrentVC {
    
}

- (AddFriendTopView *)tableHeadView{
    if (!_tableHeadView) {
        _tableHeadView = [AddFriendTopView new];
        _tableHeadView.size = CGSizeMake(SCREEN_WIDTH, [AddFriendTopView viewHeight]);
        @weakify(self)
        [_tableHeadView addAction:^(UIView *view) {
            SearchCardVC *resultVC = [[SearchCardVC alloc] init];
            resultVC.callback = ^(FriendModel * _Nonnull model) {
                @strongify(self)
                [[MXRouter sharedInstance] configureCurrentVC:self];
                NSString* tip = [NSString stringWithFormat:@"确定发送%@的名片到当前聊天?",model.name];
                [UIAlertView alertViewWithTitle:tip message:@"" cancelButtonTitle:@"取消" otherButtonTitles:@[@"确定"] onDismiss:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        [self dismissViewControllerAnimated:YES completion:^{
                            if (self.callback) {
                                self.callback(model);
                            }
                        }];
                    }
                } onCancel:^{
                    
                }];
            };
            @strongify(self)
            [self.navigationController pushViewController:resultVC animated:YES];
        }];
    }
    return _tableHeadView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.isShowBackButton = YES;
    _viewModel = [[FriendViewModel alloc]init];
    //加完索引后的好友数据
    _dataSource =  [FriendViewModel getFriendDatabyFilter:self.userId];
    [self setNavBarTitle:@"发送名片"];
    [self initUI];
}


-(void)initUI{
    self.definesPresentationContext = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self initItem];
    [self.view addSubview:self.tb];
}

-(void)initItem{
    UIBarButtonItem *barBtnItem = [[UIBarButtonItem alloc] initWithCustomView:self.backButton];
    self.navigationItem.leftBarButtonItem = barBtnItem;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:22],NSForegroundColorAttributeName:[UIColor blackColor]}];
}


-(MXBackButton* )backButton{
    if (!_backButton) {
        _backButton = [MXBackButton buttonWithType:UIButtonTypeCustom];
        _backButton.frame = CGRectMake(16, personal_details_backbutton_height / 2 - personal_details_backbutton_height / 2, personal_details_backbutton_width, personal_details_backbutton_height);
        [_backButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [_backButton setImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
        [_backButton setTitle:@"取消" forState:UIControlStateNormal];
        [_backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _backButton.titleLabel.font = [UIFont font14];
        [_backButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

-(void)addFriend:(id)sender{
    UIViewController *vc = [[NSClassFromString(@"AddFriendVC") alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)backAction:(id)sender {
    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//消息列表
- (UITableView *)tb {
    
    if (_tb == nil) {
        CGRect rect = UIEdgeInsetsInsetRect(self.view.bounds, UIEdgeInsetsMake(0, 0, 0, 0));
        _tb = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
        _tb.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tb.dataSource = self;
        _tb.delegate = self;
        _tb.tableHeaderView = self.tableHeadView;
        _tb.sectionIndexBackgroundColor = [UIColor clearColor];
        _tb.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tb.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tb setSectionIndexColor:[UIColor lightGrayColor]];
        _tb.backgroundColor = [UIColor whiteColor];
        _tb.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tb.backgroundView = nil;
        _tb.backgroundColor = [UIColor clearColor];
        AdjustTableBehavior(_tb);
    }
    return _tb;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
        view.userInteractionEnabled = NO;
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        
    }
    FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
    [tmpView setShowArrow:NO];
    NSArray* tmpArray = [_dataSource objectAtIndex:indexPath.section];
    FriendModel* model = [tmpArray objectAtIndex:indexPath.row];
    [tmpView reloadLogo:model.avatar name:model.smartName];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return rowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

#pragma mark - Table view delegate

-(NSString*)sectionTitle:(NSInteger) segIndex section:(NSInteger)section{
    if ([[_dataSource objectAtIndex:section] count] != 0){
        NSString* str = [NSString stringWithFormat:@"%@", [[ALPHA2 substringFromIndex:section] substringToIndex:1]];
        return str;
    }
    return nil;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString* sectionTitle = [self sectionTitle:0 section:section];
    if (sectionTitle==nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label=[[UILabel alloc] init];
    label.frame=CGRectMake(12, (headerHeight-20)/2, 300, 20);
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor blackColor];
    label.font=[UIFont fontWithName:@"Helvetica-Bold" size:14];
    label.text=sectionTitle;
    
    // Create header view and add label as a subview
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
    [sectionView setBackgroundColor:[UIColor whiteColor]];
    [sectionView addSubview:label];
    
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSString* sectionTitle = [self sectionTitle:0 section:section];
    if (sectionTitle!=nil) {
        return headerHeight;
    }
    return 0.01;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray* tmpArray = [self.dataSource objectAtIndex:section];
    if (tmpArray.count) {
        return tmpArray.count;
    }else
        return 0;
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ALPHA2.length;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray *indices = [NSMutableArray arrayWithObject:UITableViewIndexSearch];
    for (int i = 0; i < ALPHA2.length; i++){
        if ([[_dataSource objectAtIndex:i] count])
            [indices safeAddObj:[[ALPHA2 substringFromIndex:i] substringToIndex:1]];
    }
    return indices;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray* array = [self.dataSource objectAtIndex:indexPath.section];
    FriendModel* model = [array objectAtIndex:indexPath.row];
    NSString* tip = [NSString stringWithFormat:@"确定发送%@的名片到当前聊天?",model.name];
    [UIAlertView alertViewWithTitle:tip message:@"" cancelButtonTitle:@"取消" otherButtonTitles:@[@"确定"] onDismiss:^(NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            [self dismissViewControllerAnimated:YES completion:^{
                if (self.callback) {
                    self.callback(model);
                }
            }];
        }
    } onCancel:^{
        
    }];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    for (id obj in [searchBar subviews]) {
        if ([obj isKindOfClass:[UIView class]]) {
            for (id obj2 in [obj subviews]) {
                if ([obj2 isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)obj2;
                    [btn setTitle:@"取消" forState:UIControlStateNormal];
                }
            }
        }
    }
    return YES;
}


- (UISearchController *)searchController {
    if (!_searchController) {
        SearchCardVC *resultVC = [[SearchCardVC alloc] init];
        resultVC.callback = ^(FriendModel * _Nonnull model) {
            [[MXRouter sharedInstance]configureCurrentVC:self];
            NSString* tip = [NSString stringWithFormat:@"确定发送%@的名片到当前聊天?",model.name];
            [UIAlertView alertViewWithTitle:tip message:@"" cancelButtonTitle:@"取消" otherButtonTitles:@[@"确定"] onDismiss:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [self dismissViewControllerAnimated:YES completion:^{
                        if (self.callback) {
                            self.callback(model);
                        }
                    }];
                }
            } onCancel:^{
                
            }];

        } ;
        _searchController = [[UISearchController alloc]initWithSearchResultsController:resultVC];
        _searchController.searchBar.delegate = self;
        _searchController.searchResultsUpdater = resultVC;
        //_searchController.delegate = self;
        _searchController.view.backgroundColor = [UIColor whiteColor];
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = YES;
        //[_searchController.searchBar sizeToFit];
        //_searchController.searchBar.tintColor = [UIColor blackColor];
        _searchController.searchBar.placeholder =  @"搜索";
        [_searchController.searchBar sizeToFit];
        UIOffset offset = {5.0,0};
        _searchController.searchBar.searchTextPositionAdjustment = offset;
        _searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
        _searchController.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;//关闭提示
        _searchController.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;//关闭自动首字母大写
    }
    return _searchController;
}
@end
