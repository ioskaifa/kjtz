//
//  AskCallVC.m
//  BOB
//
//  Created by mac on 2020/7/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AskCallVC.h"
#import "FriendModel.h"
#import "LcwlChat.h"
#import <AVFoundation/AVFoundation.h>
#import "ChatVoiceManage.h"

@interface AskCallVC ()

@property (nonatomic, strong) MyLinearLayout *layout;
///头像
@property (nonatomic, strong) UIImageView *avatarImgView;
///昵称
@property (nonatomic, strong) UILabel *nameLbl;
///提示信息
@property (nonatomic, strong) UILabel *tipLbl;
///挂断
@property (nonatomic, strong) SPButton *offBtn;
///接听
@property (nonatomic, strong) SPButton *onBtn;

@property (nonatomic, strong) FriendModel *friendObj;

@property (nonatomic, strong) AVAudioPlayer *player;

@property (nonatomic, strong) NSTimer *timer;
///一分钟超时
@property (nonatomic, assign) NSInteger timerIndex;

@end

@implementation AskCallVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //获取好友信息
    self.friendObj = [[LcwlChat shareInstance].chatManager loadFriendByChatId:self.toUserId];
    [self createUI];
    if (self.askType == AskCallTypePost) {
        [ChatVoiceManage sendAsk:self.toUserId chatType:self.messageType];
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(overtimeAction:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    [self switchAudioCategaryWithSpeaker:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiceVoiceMsg:) name:kMXVoiceMsgNotifaction object:nil];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self play];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self stop];
    [self endCall];
}

#pragma mark - PrivateMethod
- (void)switchAudioCategaryWithSpeaker:(BOOL)isSpeaker {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error;
    if (isSpeaker) {
        [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    } else {
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    }
}

#pragma mark - IBAction
- (void)cancelAction:(UIButton *)sender {
    [ChatVoiceManage sendCancel];
    [ChatVoiceManage shareInstance].isMeeting = NO;
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)okAction:(UIButton *)sender {
    ///发送接受消息
    [ChatVoiceManage sendAccept];
    ///进入聊天室
    [[ChatVoiceManage shareInstance] acceptEnterChatRoom];
}

- (void)overtimeAction:(NSTimer *)sender {
    self.timerIndex++;
    if (self.timerIndex >= 60) {
        [ChatVoiceManage shareInstance].isMeeting = NO;
        [ChatVoiceManage sendOverTime];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (void)endCall {
    [self.timer invalidate];
    self.timer = nil;
}

#pragma mark - Private Method
- (void)configureAskType:(AskCallType)type {
    if (type == AskCallTypePost) {
        self.onBtn.visibility = MyVisibility_Gone;
        self.tipLbl.visibility = MyVisibility_Gone;
    } else {
        self.onBtn.visibility = MyVisibility_Visible;
        self.tipLbl.visibility = MyVisibility_Visible;
    }
}

- (void)receiceVoiceMsg:(NSNotification *)notifacation {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![notifacation isKindOfClass:NSNotification.class]) {
            return;
        }
        MessageModel *obj = notifacation.object;
        if (![obj isKindOfClass:MessageModel.class]) {
            return;
        }
        NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:obj.body];
        kMXVoiceChatType subType = [bodyDic[@"attr3"] integerValue];
        ///对方忙
        if (subType == kMXVoiceChatTypeBusy) {
            [self.navigationController popViewControllerAnimated:NO];
        }
        ///对方拒绝接听
        if (subType == kMXVoiceChatTypeCancel) {
            [self.navigationController popViewControllerAnimated:NO];
        }
        ///超时未接听
        if (subType == kMXVoiceChatTypeOverTime) {
            [self.navigationController popViewControllerAnimated:NO];
        }
        ///对方已接受
        if (subType == kMXVoiceChatTypeAccept) {
            [[ChatVoiceManage shareInstance] acceptEnterChatRoom];
        }
    });    
}

- (NSString *)friendName {
    if (!self.friendObj) {
        return @"";
    }
    if ([StringUtil isEmpty:self.friendObj.remark]) {
        return self.friendObj.name;
    }else{
        return self.friendObj.remark;
    }
}

- (NSString *)friednAvatar {
    if (!self.friendObj) {
        return @"";
    }
    return StrF(@"%@/%@?imageView2/1/w/90/h/90", UDetail.user.qiniu_domain, self.friendObj.avatar);
}

#pragma mark - AVFoundation
- (void)play {
    if (self.player) {
        [self.player stop];
    }
    NSString *path = [[NSBundle mainBundle] pathForResource:@"dial" ofType:@"m4a"];
    if ([StringUtil isEmpty:path]) {
        return;
    }
    NSData *data = [NSData dataWithContentsOfFile:path];
    if (!data) {
        return;
    }
    self.player = [[AVAudioPlayer alloc] initWithData:data error:nil];
    self.player.volume = 0.7;
    self.player.numberOfLoops = 100;
    [self.player play];
}

- (void)stop {
    if (!self.player) {
        return;
    }
    [self.player stop];
    self.player = nil;
}

#pragma mark - InitUI
- (void)createUI {
    [self.view addSubview:self.layout];
    [self.layout addSubview:self.avatarImgView];
    [self.layout addSubview:self.nameLbl];
    [self.layout addSubview:self.tipLbl];
    [self.layout addSubview:self.connectLbl];
    
    ///占位
    UIView *view = [UIView new];
    view.weight = 1;
    view.myWidth = 1;
    view.myCenterX = 0;
    [self.layout addSubview:view];
    
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myCenterX = 0;
    bottomLayout.myWidth = MyLayoutSize.wrap;
    bottomLayout.myHeight = MyLayoutSize.wrap;
    bottomLayout.myBottom = 80;
    [bottomLayout addSubview:self.offBtn];
    [bottomLayout addSubview:self.onBtn];
    [self.layout addSubview:bottomLayout];
    [self configureAskType:self.askType];
}

- (MyLinearLayout *)layout {
    if (!_layout) {
        _layout = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            object.backgroundColor = [UIColor colorWithHexString:@"#575757"];
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.myBottom = 0;
            object;
        });
    }
    return _layout;
}

- (UIImageView *)avatarImgView {
    if (!_avatarImgView) {
        _avatarImgView = ({
            UIImageView *object = [UIImageView new];
            [object sd_setImageWithURL:[NSURL URLWithString:[self friednAvatar]] placeholderImage:[UIImage defaultAvatar]];
            object.size = CGSizeMake(90, 90);
            [object setCircleView];
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myTop = 150;
            object;
        });
    }
    return _avatarImgView;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.text = [self friendName];
            object.textColor = [UIColor whiteColor];
            object.textAlignment = NSTextAlignmentCenter;
            object.font = [UIFont font16];
            object.height = 25;
            object.myHeight = object.height;
            object.myLeft = 15;
            object.myRight = 15;
            object.myTop = 5;
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)tipLbl {
    if (!_tipLbl) {
        _tipLbl = ({
            UILabel *object = [UILabel new];
            object.text = StrF(@"%@ %@", [self friendName], @"邀请你语音通话");
            object.textColor = [UIColor whiteColor];
            object.textAlignment = NSTextAlignmentCenter;
            object.font = [UIFont font14];
            object.height = 25;
            object.myHeight = object.height;
            object.myLeft = 15;
            object.myRight = 15;
            object.myTop = 30;
            object;
        });
    }
    return _tipLbl;
}

- (UILabel *)connectLbl {
    if (!_connectLbl) {
        _connectLbl = ({
            UILabel *lbl = [UILabel new];
            lbl.font = [UIFont font13];
            lbl.textColor = [UIColor whiteColor];
            lbl.text = @"连接中....";
            [lbl sizeToFit];
            lbl.mySize = lbl.size;
            lbl.myTop = 10;
            lbl.myCenterX = 0;
            lbl.visibility = MyVisibility_Gone;
            lbl;
        });
    }
    return _connectLbl;
}

- (SPButton *)offBtn {
    if (!_offBtn) {
        _offBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionTop;
            object.imageTitleSpace = 5;
            object.titleLabel.font = [UIFont font12];
            [object setTitle:@"取消" forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"icon_hang_up"] forState:UIControlStateNormal];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                [self cancelAction:btn];
            }];
            object;
        });
    }
    return _offBtn;
}

- (SPButton *)onBtn {
    if (!_onBtn) {
        _onBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionTop;
            object.imageTitleSpace = 5;
            object.titleLabel.font = [UIFont font12];
            [object setTitle:@"接听" forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"icon_answer"] forState:UIControlStateNormal];
            [object sizeToFit];
            object.mySize = object.size;
            object.myLeft = 100;
            object.myCenterY = 0;
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                [self okAction:btn];
            }];
            object;
        });
    }
    return _onBtn;
}

@end
