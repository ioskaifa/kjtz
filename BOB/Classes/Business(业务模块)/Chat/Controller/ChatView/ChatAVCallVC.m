//
//  ChatAVCallVC.m
//  BOB
//  语音聊天
//  Created by mac on 2020/7/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChatAVCallVC.h"
#import "ChatVoiceManage.h"
#import <AVFoundation/AVFoundation.h>
#import <WebRTC/WebRTC.h>

@import JitsiMeet;

@interface ChatAVCallVC ()

@property (nonatomic, strong)JitsiMeetView *meetView;

@property (nonatomic, strong) SPButton *timeBtn;
///缩小通话界面按钮
@property (nonatomic, strong) UIButton *callHideBtn;
///免提按钮
@property (nonatomic, strong) SPButton *speakerphoneBtn;
///计时
@property (nonatomic, assign) NSInteger timerIndex;
///定时器
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, assign) BOOL isSpeaker;

@end

@implementation ChatAVCallVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiceVoiceMsg:) name:kMXVoiceMsgNotifaction object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(roteChange:) name:AVAudioSessionRouteChangeNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO];
}

#pragma mark - IBAction
- (void)callHideBtnClick:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    CGRect frame = g_subWindow.frame;
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        g_subWindow.hidden = NO;
        self.view.hidden = YES;
        sender.userInteractionEnabled = YES;
        self.view.frame = CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }];
}

- (void)callShowBtnClick:(UIButton *)sender {
    sender.userInteractionEnabled = NO;
    g_subWindow.hidden = YES;
    self.view.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    } completion:^(BOOL finished) {
        sender.userInteractionEnabled = YES;
    }];
}

- (void)speakerphoneBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.isSpeaker = sender.selected;
    [self switchAudioCategaryWithSpeaker:self.isSpeaker];
}

- (void)switchAudioCategaryWithSpeaker:(BOOL)isSpeaker {
    MLog(@"switchAudioCategaryWithSpeaker.......")
    AVAudioSession* audioSession = [AVAudioSession sharedInstance];
    RTCAudioSession *rtcSession = [RTCAudioSession sharedInstance];
    RTCAudioSessionConfiguration *configure = [RTCAudioSessionConfiguration webRTCConfiguration];
    if (rtcSession.isLocked) {
        [rtcSession unlockForConfiguration];
    }
    [rtcSession lockForConfiguration];
    NSError *error;
    if (isSpeaker) {
        [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
        [configure setCategory:AVAudioSessionCategoryPlayback];
    } else {
        [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
        [configure setCategory:AVAudioSessionCategoryPlayAndRecord];
    }
    [rtcSession setConfiguration:configure error:nil];
}

- (void)roteChange:(NSNotification *)noti {
    RTCAudioSession *rtcSession = [RTCAudioSession sharedInstance];
    MLog(@"过滤声道变化.....%@, rtcSession.....%@", noti, rtcSession.currentRoute)
    
}

- (void)callTimerAction:(NSTimer *)timer {
    self.timerIndex++;
    NSString *str = [NSString stringWithFormat:@"%.2ld:%.2ld", self.timerIndex / 60,self.timerIndex % 60];
    [self.timeBtn setTitle:str forState:UIControlStateNormal];
}

- (void)receiceVoiceMsg:(NSNotification *)notifacation {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![notifacation isKindOfClass:NSNotification.class]) {
            return;
        }
        MessageModel *obj = notifacation.object;
        if (![obj isKindOfClass:MessageModel.class]) {
            return;
        }
        NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:obj.body];
        kMXVoiceChatType subType = [bodyDic[@"attr3"] integerValue];
        if (subType == kMXVoiceChatTypeEnd) {
            [self endCall];
        }
    });    
}

- (void)endCall {
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    if (g_subWindow) {
        [g_subWindow removeFromSuperview];
        g_subWindow = nil;
    }
    [self.meetView leave];
    if (self.view.superview) {
        [self.view removeFromSuperview];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].statusBarHidden = NO;
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    });
}

- (void)beginMeeting {
    Block_Exec_Main_Async_Safe(^{
        [[ChatVoiceManage shareInstance].askVC.navigationController popViewControllerAnimated:YES];
        [ChatVoiceManage shareInstance].askVC = nil;
        self.view.hidden = NO;
        self.timerIndex = 0;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(callTimerAction:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
        UIView *view = [self findSubViewOn:self.meetView];
        if (view) {
            UIView *superview = view.superview;
            if (superview) {
                superview.hidden = YES;
                self.speakerphoneBtn.size = superview.size;
                self.speakerphoneBtn.x = superview.x;
                self.speakerphoneBtn.y = 0;
                [superview.superview addSubview:self.speakerphoneBtn];
            }
        }
    });
}

- (UIView *)findSubViewOn:(UIView *)superView{
    if (![superView isKindOfClass:UIView.class]) {
        return nil;
    }
    if ([self isTargetView:superView]) {
        return superView;
    }
    if (superView.subviews.count == 0) {
        return nil;
    }
    UIView *subView = nil;
    for (UIView *view in superView.subviews) {
        if (![view isKindOfClass:UIView.class]) {
            continue;
        }
        if ([self isTargetView:view]) {
            subView = view;
            break;
        }
        
        if (view.subviews.count == 0) {
            continue;
        }
        subView = [self findSubViewOn:view];
        if (subView) {
            break;
        }
    }
    return subView;
}

///是否是需要查找的目标View
- (BOOL)isTargetView:(UIView *)view {
    if (![view isKindOfClass:NSClassFromString(@"RCTTextView")]) {
        return NO;
    }
    if ([view.description containsString:@"免提"]) {
        return YES;
    }
    return NO;
}

#pragma mark - JitsiMeetViewDelegate
- (void)conferenceWillJoin:(NSDictionary *)data {
    MLog(@"JitsiMeet---conferenceWillJoin")
    [ChatVoiceManage shareInstance].askVC.connectLbl.visibility = MyVisibility_Visible;
}

///已接入聊天
- (void)conferenceJoined:(NSDictionary *)data {
    MLog(@"JitsiMeet---conferenceJoined")
    [ChatVoiceManage shareInstance].isSelfEnterRoom = YES;
    [ChatVoiceManage enterMeetingRoom];
}

///通话结束
- (void)conferenceTerminated:(NSDictionary *)data {
    MLog(@"JitsiMeet---conferenceTerminated")
    ///通话结束
    dispatch_async(dispatch_get_main_queue(), ^{
//        NSString *hour = [NSString stringWithFormat:@"%02ld",self.timerIndex/3600];
        NSString *minu = [NSString stringWithFormat:@"%02ld",(self.timerIndex%3600)/60];
        NSString *sec = [NSString stringWithFormat:@"%02ld",self.timerIndex%60];
        [self.meetView leave];
        [self endCall];
        [ChatVoiceManage sendEndWithTime:StrF(@"通话时长 %@:%@", minu, sec)];
    });
}

- (void)enterPictureInPicture:(NSDictionary *)data {
    MLog(@"JitsiMeet---enterPictureInPicture")
}

#pragma mark - InitUI
- (void)createUI {
    [self initShowHiddenView];
    [self.view addSubview:self.meetView];
    [self.meetView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self.view addSubview:self.callHideBtn];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        RTCAudioSession *rtcSession = [RTCAudioSession sharedInstance];
        if (rtcSession.isLocked) {
            [rtcSession unlockForConfiguration];
        }
        NSString *serverStr = UDetail.user.jitsiServer;
        JitsiMeetConferenceOptions *options
            = [JitsiMeetConferenceOptions fromBuilder:^(JitsiMeetConferenceOptionsBuilder *builder) {
                builder.serverURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",serverStr,[ChatVoiceManage shareInstance].roomID]];
                builder.room = [ChatVoiceManage shareInstance].roomID;
                builder.videoMuted = YES;
                JitsiMeetUserInfo *userInfo = [JitsiMeetUserInfo new];
                userInfo.displayName = UDetail.user.nickname;
                NSString *avatarUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, UDetail.user.head_photo);
                userInfo.avatar = [NSURL URLWithString:avatarUrl];
                builder.userInfo = userInfo;                
            }];
        [self.meetView join:options];
    });
}

- (void)initShowHiddenView {
    g_subWindow.size = CGSizeMake(80, 100);
    g_subWindow.x = SCREEN_WIDTH - g_subWindow.width - 10;
    g_subWindow.y = 50;
    g_subWindow.backgroundColor = [UIColor whiteColor];
    [g_subWindow setViewCornerRadius:2.0];
    g_subWindow.layer.borderWidth = 0.5;
    g_subWindow.layer.borderColor = [[UIColor grayColor] CGColor];
    [g_subWindow addSubview:self.timeBtn];
    self.timeBtn.x = (g_subWindow.width - self.timeBtn.width) / 2.0;
    self.timeBtn.y = (g_subWindow.height - self.timeBtn.height) / 2.0;
    g_subWindow.hidden = YES;
}

- (JitsiMeetView *)meetView {
    if (!_meetView) {
        _meetView = [JitsiMeetView new];
        _meetView.delegate = self;
    }
    return _meetView;
}

- (SPButton *)timeBtn {
    if (!_timeBtn) {
        _timeBtn = [SPButton new];
        _timeBtn.imagePosition = SPButtonImagePositionTop;
        _timeBtn.imageTitleSpace = 5;
        _timeBtn.titleLabel.font = [UIFont font15];
        [_timeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_timeBtn setImage:[UIImage imageNamed:@"callShow"] forState:UIControlStateNormal];
        [_timeBtn setTitle:@"00:00" forState:UIControlStateNormal];
        [_timeBtn sizeToFit];
        CGSize size = _timeBtn.size;
        _timeBtn.size = CGSizeMake(size.width + 10, size.height + 10);
        @weakify(self)
        [_timeBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self callShowBtnClick:btn];
        }];
    }
    return _timeBtn;
}

- (UIButton *)callHideBtn {
    if (!_callHideBtn) {
        _callHideBtn = ({
            UIButton *object = [UIButton new];
            [object setBackgroundImage:[UIImage imageNamed:@"callHide"] forState:UIControlStateNormal];
            object.size = CGSizeMake(40, 40);
            object.x = 20;
            object.y = 30;
            [object addTarget:self action:@selector(callHideBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            object;
        });
    }
    return _callHideBtn;
}

- (SPButton *)speakerphoneBtn {
    if (!_speakerphoneBtn) {
        _speakerphoneBtn = ({
            SPButton *object = [SPButton new];
            object.titleLabel.font = [UIFont font12];
            [object setTitle:@"免提" forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"chat_disSpeakerphone"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"chat_speakerphone"] forState:UIControlStateSelected];
            object.imagePosition = SPButtonImagePositionTop;
            object.imageTitleSpace = 3;
            [object sizeToFit];
            object.selected = YES;
            self.isSpeaker = YES;
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                [self speakerphoneBtnClick:btn];
            }];
            object;
        });
    }
    return _speakerphoneBtn;
}

@end
