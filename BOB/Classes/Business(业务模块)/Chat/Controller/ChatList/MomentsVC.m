//
//  MomentsVC.m
//  Lcwl
//
//  Created by AlphaGO on 2018/11/15.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "MomentsVC.h"
#import "UIImage+Color.h"
#import "LSearchBar.h"
#import "SearchMomentsVC.h"
#import "FriendVC.h"
#import "MomentsViewModel.h"
#import "ContactHelper.h"
#import "LcwlChat.h"
#import "MomentsHeaderView.h"
#import "ChatListTableCell.h"
#import "ChatGroupModel.h"
#import "TalkManager.h"
#import "ChatViewVC.h"
#import "LcwlBlankPageView.h"
#import "ChatGroupModel.h"
#import "RealReachability.h"
#import "MXChatManager.h"
#import "ChatSendHelper.h"
#import "ChatCacheFileUtil.h"
#import "AddFriendTopView.h"
#import "ClientStatusView.h"

@interface MomentsVC ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,IMXChatManagerDelegate>
@property (nonatomic, strong) LSearchBar *searchBar;

@property (nonatomic, strong) UINavigationController  *searchNavigationController;

@property (nonatomic, strong) MomentsViewModel        *viewModel;

@property (nonatomic, strong) FBKVOController         *kvoController;

@property (nonatomic, strong) UITapGestureRecognizer  *singleRecognizer;

@property (nonatomic, strong) UIBarButtonItem         *nextItem;

//红点
@property (nonatomic, strong) JSBadgeView *badgeView ;

@property (nonatomic, strong) UIButton* btn2;

@property (nonatomic, strong) MyLinearLayout *headerView;

@property (nonatomic, strong) AddFriendTopView       *searchView;

@property (nonatomic, strong) ClientStatusView *clientStatusView;

///客户端登录状态
@property (nonatomic, assign) BOOL clientStatus;

@end

@implementation MomentsVC


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.dataSource = [NSMutableArray array];
        [self initReachability];
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setNavBarTitle:Lang(@"消息")];
    _viewModel = [[MomentsViewModel alloc]initVC:self];
    [self initUI];
    //加载通讯录数据
//    [[ContactHelper shareInstance] getDataFromContact];
    [self configObserver];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //初始化聊天列表
    [self refreshDataSource];
    [[LcwlChat shareInstance].chatManager addDelegate:self];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self refreshHeaderView];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.searchBar resignFirstResponder];
     [[LcwlChat shareInstance].chatManager removeDelegate:self];
}

- (void)navBarRightBtnAction:(id)sender {
    [self add:sender];
}

-(void)configObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDataSource) name:kRefreshChatList object:nil];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)friend:(id)sender{
    [MXRouter openURL:@"lcwl://FriendVC"];
}

-(void)add:(id)sender{
    if ([self.viewModel.popover superview]) {
        return;
    }
    [self.viewModel resetPopver];
    UIView *titleView = sender;
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    
    CGRect rect=[titleView convertRect: titleView.bounds toView:window];
    
    CGPoint startPoint =
    CGPointMake(CGRectGetMinX(rect)+rect.size.width/2, CGRectGetMaxY(rect));
    [self.viewModel.popover showAtPoint:startPoint
               popoverPostion:DXPopoverPositionDown
              withContentView:self.viewModel.popoverTableView
                       inView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [ChatListTableCell rowHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MXConversation* conver  = [self.dataSource safeObjectAtIndex:indexPath.row];
    NSString *cellIdentifier = [ChatListTableCell cellIdentifierForMessageModel:conver.latestMessage];
    ChatListTableCell *cell = (ChatListTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        
        cell = [[ChatListTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:cellIdentifier
                                              tableView:tableView conversation:conver
                                            marginRight:0];
        cell.backgroundColor = [UIColor clearColor];
        cell.customContentView.backgroundColor =[UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
    }
    if (conver.conversationType != eConversationTypeHDTZChat && conver.conversationType != eConversationTypeXDPYChat ) {
        cell.rightButtons = [self leftButtons:indexPath];
    }
    cell.conversation = conver;
    return cell;
}

// 滑动按钮
- (NSArray *)leftButtons:(NSIndexPath *)indexPath {
    
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    NSArray* array = nil;
    MXConversation* con = self.dataSource[indexPath.row];
    if (con.conversationType == eConversationTypeGroupChat) {
        ChatGroupModel* group = (ChatGroupModel*)[[LcwlChat shareInstance].chatManager loadGroupByChatId:con.chat_id];
        array = [TalkManager getListOperationByType:con.conversationType top:!group.enable_top];
    }else{
        FriendModel* friend = (FriendModel*)[[LcwlChat shareInstance].chatManager loadFriendByChatId:con.chat_id];
        array = [TalkManager getListOperationByType:con.conversationType top:!friend.enable_top];
    }
    
    NSArray* colorArray = [TalkManager getListColorByType:con.conversationType];
    if (array) {
        CGFloat height = [self.conversationListTableView.delegate tableView:self.conversationListTableView heightForRowAtIndexPath:indexPath];
        for (int i = 0; i < array.count; i++) {
            UIButton *btn1 = [[UIButton alloc]initWithFrame:CGRectMake(i*60, 0, 60, height )];
            btn1.backgroundColor = colorArray[i];
            [btn1 setTitle:array[i] forState:UIControlStateNormal];
            [leftUtilityButtons addObject:btn1];
        }
    }
    
    return leftUtilityButtons;
}

#pragma mark - 表格cell滑动 删除 置顶 销毁 操作 delegate
- (void)mxSwipeTableViewCelldidSelectBtnWithTag:(NSInteger)tag
                                   andIndexPath:(NSIndexPath *)indexpath{
    NSInteger cellIndexNum = indexpath.row;
    MXConversation* conversation = [self.dataSource safeObjectAtIndex:cellIndexNum];
    if (conversation.conversationType == eConversationTypeGroupChat) {
        ChatGroupModel * group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:conversation.chat_id];
        if (group.groupAvatar) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:group.groupAvatar];
            [[NSUserDefaults standardUserDefaults]    synchronize];
        }
    }
    [self deleteMsg:conversation];
    
}

-(void)deleteMsg:(MXConversation*)object{
    [[LcwlChat shareInstance].chatManager deleteConversationByChatter:object.chat_id];
    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithCapacity:0];
    NSInteger nums = [[LcwlChat shareInstance].chatManager getAllUnReadNums];
    [userInfo setValue:[NSNumber numberWithInteger:nums] forKey:@"nums"];
    [userInfo setValue:[NSNumber numberWithInteger:3] forKey:@"tabBarIndex"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"redBadgeCountChanged" object:nil userInfo:userInfo];
}

//关闭打开的滑动cell
-(void)tapAction:(UITapGestureRecognizer*)recognizer  {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"kMXSwipeTableViewOpenNotifition" object:nil userInfo:@{@"action":@"closeCell"}];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    SearchMomentsVC *searchVC = [[SearchMomentsVC alloc]init];
    
    @weakify(self)
    searchVC.callback = ^(){
        @strongify(self)
        [self.searchBar resignFirstResponder];
        [self.searchNavigationController.view removeFromSuperview];
        [self.searchNavigationController removeFromParentViewController];
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [[MoApp router] configureCurrentVC:self];
    };
    
    _searchNavigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
    [[UIApplication sharedApplication].keyWindow addSubview:_searchNavigationController.view];
    [self.searchBar resignFirstResponder];
    //[self performSelector:@selector(abc:) withObject:searchVC afterDelay:1];
    return NO;
}

-(void)refreshDataSource
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        self.dataSource = [self loadDataSource];
        dispatch_async(dispatch_get_main_queue(), ^{
//            if (self.dataSource.count == 0) {
//                [LcwlBlankPageView addBlankPageView:LcwlBlankPageMomentsVCType withSuperView:self.view touchedBlock:^{
//
//                }];
//            }else{
//                [LcwlBlankPageView removeFromSuperView:self.view];
//            }
            [self.conversationListTableView reloadData];
        });
    });
}

- (NSMutableArray *)loadDataSource
{
    NSMutableArray<MXConversation *> *conversations = (NSMutableArray*)[[LcwlChat shareInstance].chatManager conversations];
    NSMutableArray *serviceConversations = [NSMutableArray arrayWithCapacity:1];
    [conversations enumerateObjectsUsingBlock:^(MXConversation * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj.creatorRole == 0) { //过滤客服消息会话，creatorRole>0的会话不应该出现在这里
            [serviceConversations addObject:obj];
        }
    }];
    return (NSMutableArray*)[TalkManager sortConversation:serviceConversations];
}

#pragma mark - 如果有客户端登录的情况，展示客户端的登录状态
- (void)refreshHeaderView {
    if (self.conversationListTableView.tableHeaderView) {
        if (self.clientStatus == UDetail.user.client_status) {
            [self.clientStatusView configureView];
            return;
        }
    }
    self.clientStatus = UDetail.user.client_status;
    [self.clientStatusView configureView];
    if (UDetail.user.client_status) {
        self.clientStatusView.visibility = MyVisibility_Visible;
    } else {
        self.clientStatusView.visibility = MyVisibility_Gone;
    }
    [self.headerView layoutSubviews];
    [self.conversationListTableView beginUpdates];
    [self.conversationListTableView setTableHeaderView:self.headerView];
    [self.conversationListTableView endUpdates];
}

-(void)initUI{
    [self.view addSubview:self.conversationListTableView];
    [self.conversationListTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self initNavRightItem];
}

- (void)initNavRightItem {
    UIButton *addBtn = [UIButton new];
    @weakify(self)
    [addBtn addAction:^(UIButton *btn) {
        @strongify(self)
        [self add:btn];
    }];
    addBtn.size = CGSizeMake(35, 35);
    [addBtn setImage:[UIImage imageNamed:@"聊天_添加"] forState:UIControlStateNormal];
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc] initWithCustomView:addBtn];
    
    UIButton *listBtn = [UIButton new];
    [listBtn addAction:^(UIButton *btn) {
        MXRoute(@"FriendVC", nil);
    }];
    listBtn.size = CGSizeMake(35, 35);
    [listBtn setImage:[UIImage imageNamed:@"聊天_通讯录"] forState:UIControlStateNormal];
    UIBarButtonItem *listItem = [[UIBarButtonItem alloc] initWithCustomView:listBtn];
    self.navigationItem.rightBarButtonItems = @[addItem, listItem];
}

//消息列表
- (UITableView *)conversationListTableView
{
    if (_conversationListTableView == nil) {
        _conversationListTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _conversationListTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _conversationListTableView.dataSource = self;
        _conversationListTableView.delegate = self;
        _conversationListTableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _conversationListTableView.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _conversationListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _conversationListTableView.backgroundColor = [UIColor whiteColor];
        _conversationListTableView.backgroundView = nil;
        _conversationListTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _conversationListTableView.separatorColor = HexColor(@"#E5E5E5");
        _conversationListTableView.separatorInset = UIEdgeInsetsMake(0, 70, 0, 0);
        _singleRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
        //点击的次数
        _singleRecognizer.numberOfTapsRequired = 1; // 单击

        //给self.view添加一个手势监测；

        [_conversationListTableView addGestureRecognizer:_singleRecognizer];
        
    }
    return _conversationListTableView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MXConversation* con = [self.dataSource safeObjectAtIndex:indexPath.row];
    if(con == nil) {
        return;
    }
    
    if (con.conversationType == eConversationTypeXDPYChat) {
         [MXRouter openURL:@"lcwl://NewFriendVC"];
    }else if(con.conversationType == eConversationTypeHDTZChat){
        NSString* messageId = @"";
        if (con.messages.count > 0 ) {
            MessageModel* message = con.messages.lastObject;
            messageId = message.messageId;
        }
        [MXRouter openURL:@"lcwl://HDTZVC" parameters:@{@"id":messageId}];
    }else{
        [self gotoChatVC:con];
    }
}

- (void)gotoChatVC:(MXConversation *)con {
    //用户端的消息列表好友和客服是混合在一起的，需要从好友数据库中查询creatorRole来判断是否是客服
    FriendModel * friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:con.chat_id];
    
    [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":con.chat_id ,@"type":@(eConversationTypeChat),@"creatorRole":@(friend.creatorRole)}];
}

#pragma mark - 接收消息delegate
- (void)didReceiveReceipt:(MessageModel *)message {
    if (message) {
        [self refreshDataSource];
    }
}
- (void)didReceiveMessage:(MessageModel *)message {
    if (message) {
        [self refreshDataSource];
        //[self tansferMultiGroup:message];
    }
}

#pragma mark - 网络监测
-(void)initReachability {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kRealReachabilityChangedNotification
                                               object:nil];
    
    GLobalRealReachability.hostForPing = @"www.baidu.com";
    GLobalRealReachability.hostForCheck = @"www.apple.com";
    GLobalRealReachability.autoCheckInterval = 0.5;
    [GLobalRealReachability startNotifier];
}

- (void)reachabilityChanged:(NSNotification *)notification
{
    RealReachability *reachability = (RealReachability *)notification.object;
    ReachabilityStatus status = [reachability currentReachabilityStatus];
    ReachabilityStatus previousStatus = [reachability previousReachabilityStatus];
    NSLog(@"networkChanged, currentStatus:%@, previousStatus:%@", @(status), @(previousStatus));
    
    [self.searchView configureStatus:(status == RealStatusNotReachable ? 0 : 1)];
    [self.conversationListTableView reloadData];
}

-(JSBadgeView* )badgeView{
    if (_badgeView == nil) {
        _badgeView = [[JSBadgeView alloc] initWithParentView:_btn2 alignment:JSBadgeViewAlignmentCustom frame:CGRectZero];
        int badgeWidth = [_badgeView getWidth];

        _badgeView.userInteractionEnabled = NO;
        [_badgeView setFrame:CGRectMake(30-15, -5, 10, 10)];
        _badgeView.hidden = NO;
    }
    return _badgeView;
}

- (FBKVOController *)kvoController {
    if (!_kvoController) {
        _kvoController = [FBKVOController controllerWithObserver:self];
    }
    
    return _kvoController;
}

- (AddFriendTopView *)searchView {
    if (!_searchView) {
        _searchView = [AddFriendTopView new];
        _searchView.size = CGSizeMake(SCREEN_WIDTH, [AddFriendTopView viewHeight]);
        _searchView.mySize = _searchView.size;
        _searchView.myTop = 0;
        _searchView.myLeft = 0;
        _searchView.placeholderLbl.text = @"搜索联系人";
        [_searchView addAction:^(UIView *view) {
            MXRoute(@"SearchMomentsVC", nil);
        }];
    }
    return _searchView;
}

- (ClientStatusView *)clientStatusView {
    if (!_clientStatusView) {
        _clientStatusView = [ClientStatusView new];
        _clientStatusView.size = CGSizeMake(SCREEN_WIDTH, [ClientStatusView viewHeight]);
        _clientStatusView.mySize = _clientStatusView.size;
        _clientStatusView.myTop = 0;
        _clientStatusView.myLeft = 0;
        [_clientStatusView addAction:^(UIView *view) {
            MXRoute(@"ClientLoginStatusVC", nil)
        }];
    }
    return _clientStatusView;
}

- (MyLinearLayout *)headerView {
    if (!_headerView) {
        _headerView = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        _headerView.myHeight = MyLayoutSize.wrap;
        [_headerView addSubview:self.searchView];
        [_headerView addSubview:self.clientStatusView];
        _headerView.myWidth = SCREEN_WIDTH;
    }
    return _headerView;
}

@end
