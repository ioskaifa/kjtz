//
//  SearchLocalFriendVC.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/6/25.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "SearchMomentsVC.h"
#import "ChatTitleView.h"
#import "MXSeparatorLine.h"
#import "NSObject+Additions.h"
#import "UINavigationBar+Alpha.h"
#import "ContactHelper.h"
#import "FriendModel.h"
#import "FriendListView.h"
#import "LcwlChat.h"
#import "MoreSearchVC.h"
#define TipString @"搜一搜:"
#import "FriendViewModel.h"
#import "ContactHelper.h"
#import "MXConversation.h"
#import "UIImage+Utils.h"

static int rowHeight = 60;
static const CGFloat headerHeight = 30;
@interface SearchMomentsVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

// tableview
@property (nonatomic,strong) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* searchResult;
@property(nonatomic, strong) UIView *searchView;
//@property (strong, nonatomic) UIImageView *noImageView;
//@property (strong, nonatomic) UILabel *noLabel;

@property (strong, nonatomic) NSString *historyKey;
@property (strong, nonatomic) UILabel *historyLabel;
@property (strong, nonatomic) UIView *historyView;

@property(nonatomic, strong) UIButton *cancelButton;

@property(nonatomic, strong) NSMutableArray *friendArray;

@property(nonatomic, strong) NSMutableArray *groupArray;

@property(nonatomic, strong) NSMutableArray *contactArray;

@property(nonatomic, strong) NSMutableArray *conversationArray;

@property(nonatomic, strong) FriendListView *footer;

@property(nonatomic, strong) UINavigationController *searchNavigationController;
@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation SearchMomentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _searchResult = [[NSMutableArray alloc]initWithCapacity:10];
    _friendArray = [[LcwlChat shareInstance].chatManager friends];
    _groupArray = [[LcwlChat shareInstance].chatManager groupList];
    _contactArray =  [[ContactHelper shareInstance] contactArray];
    _conversationArray = [[LcwlChat shareInstance].chatManager conversations];
    [self setNavBarTitle:@"搜索联系人"];
    self.view.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.searchTF];
    [layout addSubview:self.tableView];
    
    UIView* footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  self.view.bounds.size.width, rowHeight)];
    _footer = [[FriendListView alloc]initRowStyleDefault];
    [_footer style:RowStyleSubtitle];
   
    [_footer reloadLocalLogo:@"icon_search_navi" attrDesc:@""];
    @weakify(self)
    _footer.selectCallback = ^(UIView * view){
        @strongify(self)
        MoreSearchVC* searchVC = [[MoreSearchVC alloc]init];
        searchVC.keyword = self.searchTF.text;
        [self.navigationController pushViewController:searchVC animated:YES];
    };
    [footerView addSubview:_footer];
    [_footer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(footerView);
    }];
    footerView.hidden = YES;
    self.tableView.tableFooterView = footerView;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.searchTF becomeFirstResponder];
}

//消息列表
- (UITableView *)tableView {
    
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.backgroundView = nil;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

-(LSearchBar *)searchBars{
    if (!_searchBars) {
        _searchBars = [[LSearchBar alloc] initWithFrame:CGRectMake(0, 0, self.searchView.frame.size.width - 65, 30)];
        _searchBars.delegate = self;
        _searchBars.tintColor = [UIColor themeColor];
    }
    return _searchBars;
}

- (void)loadSearchView {
    self.searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH
                                                               , 30)];
    self.searchView.userInteractionEnabled = YES;
    [self.searchView addSubview:self.searchBars];
    
    _cancelButton = [[UIButton alloc]
                     initWithFrame:CGRectMake(CGRectGetMaxX(_searchBars.frame) - 3, CGRectGetMinY(self.searchBars.frame), 55, 30)];
    [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _cancelButton.titleLabel.font = [UIFont systemFontOfSize:18.];
    [_cancelButton addTarget:self action:@selector(cancelButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.searchView addSubview:_cancelButton];
}


- (void)showNoDataTips {
    //    noImageView.hidden=NO;
    //    noLabel.hidden=NO;
    
    //    [MXBlankPageView addBlankPageView:MXBlankPageMoChatFriendSearchNoResultType withSuperView:self.view];
}

- (void)hideNoDataTips {
    //    noImageView.hidden=YES;
    //    noLabel.hidden=YES;
    
    //    [MXBlankPageView removeFromSuperView:self.view];
}

// 自动提示搜索
- (void)searchByKeyword:(NSString*)keyword {
    
    //    _historyView.hidden = NO;
    //    _historyKey = [CreatePlist readPlist];
    //    if ([StringUtil isEmpty:_historyKey]) {
    //        _historyView.hidden = YES;
    //    }
    //    _historyLabel.text = [NSString stringWithFormat:@"%@:%@",MXLang(@"Talk_friend_search_tips_47", @"上次搜索"),_historyKey];
    //    NSArray* tmpArray = [self filterDataByType:self.type keyword:keyword];//[[MXChatDBUtil sharedDataBase]selectFriendByKeyword:keyword];
    //    if (tmpArray.count == 0) {
    //        _tableView.hidden = YES ;
    //        [self showNoDataTips ];
    //    }else{
    //        _tableView.hidden = NO;
    //        [self hideNoDataTips ];
    //    }
    //
    //    self.searchResult = [tmpArray mutableCopy] ;
    //    [_tableView reloadData];
    
    
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString *cellId = @"cellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellId];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
                [cell setSeparatorInset:UIEdgeInsetsZero];
            }
            
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins:UIEdgeInsetsZero];
            }
            FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
            view.tag = 101;
            view.userInteractionEnabled = NO;
            [cell.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(cell.contentView);
            }];
            
        }
        FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
        NSArray* tmpArray = [_searchResult objectAtIndex:indexPath.section];
        FriendModel* model = [tmpArray objectAtIndex:indexPath.row];
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/60/h/60",model.avatar];

        [tmpView reloadLogo:avatar name:model.name];
        return cell;
    }else if(indexPath.section == 1){
        static NSString *cellId = @"cellIdentifier1";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellId];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
                [cell setSeparatorInset:UIEdgeInsetsZero];
            }
            
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins:UIEdgeInsetsZero];
            }
            FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
            view.tag = 101;
            [cell.contentView addSubview:view];
             view.userInteractionEnabled = NO;
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(cell.contentView);
            }];
        }
        FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
        NSArray* tmpArray = [_searchResult objectAtIndex:indexPath.section];
        ChatGroupModel* group = [tmpArray objectAtIndex:indexPath.row];
        [tmpView reloadLogo:@"" name:group.groupName];
        [ChatGroupModel setGroupIconWithURLArray:group.groupAvatar image:tmpView.logoImgView];
   
        return cell;
    }else if(indexPath.section == 2){
        static NSString *cellId = @"cellIdentifier2";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellId];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
                [cell setSeparatorInset:UIEdgeInsetsZero];
            }
            
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins:UIEdgeInsetsZero];
            }
            FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
            [view style:RowStyleSubtitle];
            view.tag = 101;
            view.userInteractionEnabled = NO;
            [cell.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(cell.contentView);
            }];
        }
        FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
        NSArray* tmpArray = [_searchResult objectAtIndex:indexPath.section];
        FriendModel* model = [tmpArray objectAtIndex:indexPath.row];
        NSString* str = model.addressListName;
        NSString* keyword = self.searchBars.text;
        if ([StringUtil isEmpty:str]) {
            str = @"";
        }
        if ([StringUtil isEmpty:model.phone]) {
            model.phone = @"";
        }
        NSMutableAttributedString* attrName = [[NSMutableAttributedString alloc]initWithString:str];
        NSRange range = [str rangeOfString:keyword];
        if (range.location != NSNotFound) {
            [attrName addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:range];
        }
        NSMutableAttributedString* attrDesc = [[NSMutableAttributedString alloc]initWithString: model.phone];
        NSString* avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/60/h/60",model.avatar];

        [tmpView reloadLogo:avatar attrName:attrName attrDesc:attrDesc];
        return cell;
    }else if(indexPath.section == 3){
        
        static NSString *cellId = @"cellIdentifier3";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:cellId];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
                [cell setSeparatorInset:UIEdgeInsetsZero];
            }
            
            if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
                [cell setLayoutMargins:UIEdgeInsetsZero];
            }
            FriendListView* view = [[FriendListView alloc]initRowStyleDefault];
            [view style:RowStyleSubtitle];
            view.tag = 101;
            view.userInteractionEnabled = NO;
            [cell.contentView addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(cell.contentView);
            }];
        }
        FriendListView* tmpView = (FriendListView*)[cell.contentView viewWithTag:101];
        NSArray* tmpArray = [_searchResult objectAtIndex:indexPath.section];
        MXConversation* con = [tmpArray objectAtIndex:indexPath.row];
        NSString* name = @"";
        NSString* avatar = @"";
        MessageModel* message = con.latestMessage;
       
        
        if (con.conversationType == eConversationTypeGroupChat) {
            ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:con.chat_id];
            name = group.groupName;
            [ChatGroupModel setGroupIconWithURLArray:group.groupAvatar image:tmpView.logoImgView];
        }else{
            FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:con.chat_id];
            name = friend.name;
             avatar = [NSString stringWithFormat:@"%@?imageView2/1/w/60/h/60",friend.avatar];

        }
        NSMutableAttributedString* attrDesc = nil;
        if (message) {
            attrDesc = [[NSMutableAttributedString alloc]initWithString:message.content];
        }else{
            attrDesc = [[NSMutableAttributedString alloc]initWithString:@""];
        }
        NSString* keyword = self.searchBars.text;
        NSRange range = [message.content rangeOfString:keyword];
        if (range.location != NSNotFound) {
            [attrDesc addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:range];
        }
        NSMutableAttributedString* attrName = [[NSMutableAttributedString alloc]initWithString: name];
        
        [tmpView reloadLogo:avatar attrName:attrName attrDesc:attrDesc];
        
      
        return cell;
        
    }
    return nil;
}

-(void)chatClick:(id)sender{
    [self dismissView];
    
    UIButton* btn = (UIButton*)sender;
    NSInteger tag = btn.tag-100;
    MoYouModel* friend = [self.searchResult safeObjectAtIndex:tag];
    //    if ([friend.chatId rangeOfString:@"_"].location != NSNotFound) {
    //
    //    }else{
    //        ChatViewVC* chatVC = [[ChatViewVC alloc] initWithChatter:friend.chatId conversationType:eConversationTypeChat];
    //        [self.navigationController pushViewController:chatVC animated:YES];
    //    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.searchResult && self.searchResult.count>0) {
        NSArray* array = [self.searchResult objectAtIndex:section];
        if (array && [array isKindOfClass:[NSArray class]]) {
            return array.count;
        }
    }
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.searchResult.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [self dismissView];
    if (indexPath.section == 0) {
        FriendModel* model = [[self.searchResult objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
         [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":model.userid,@"type":@(eConversationTypeChat)}];
    }else if(indexPath.section == 1){
        ChatGroupModel* group = [[self.searchResult objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
         [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":group.groupId,@"type":@(eConversationTypeGroupChat)}];
    }else if(indexPath.section == 2){
        //去个人详情
    }else if(indexPath.section == 3){
        MXConversation* con = [[self.searchResult objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        if (con.conversationType == eConversationTypeGroupChat) {
            ChatGroupModel *group  = [[LcwlChat shareInstance].chatManager loadGroupByChatId:con.chat_id];
            [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":group.groupId,@"type":@(eConversationTypeGroupChat)}];
        }else{
            FriendModel *friend  = [[LcwlChat shareInstance].chatManager loadFriendByChatId:con.chat_id];
            [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":friend.userid,@"type":@(eConversationTypeChat)}];
        }
    }
    return;
}


- (void)cancelButtonDidClick {
    
    if (self.callback) {
        self.callback();
        self.searchBars.text = @"";
        [self.searchBars resignFirstResponder];
    }
    
    if([[UIApplication sharedApplication] respondsToSelector:NSSelectorFromString(@"setStatusBarStyle:")]) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent];
    }
    
}
- (void)dismissView {
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    // 正在编辑过程中的改变
    NSLog(@"正在编辑过程中的改变");
    [self filterFriendByKeyword:searchText];
    
    [self.tableView reloadData];
    if (![StringUtil isEmpty:searchText]) {
        NSString* str = [NSString stringWithFormat:@"%@ %@",TipString,searchText];
        NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:str];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(TipString.length,str.length-TipString.length)];
        [string addAttribute:NSFontAttributeName value:[UIFont font14] range:NSMakeRange(TipString.length,str.length-TipString.length)];
        
        _footer.nameLbl.attributedText = string;
        self.tableView.tableFooterView.hidden = NO;
    }else{
        _footer.nameLbl.attributedText = [[NSMutableAttributedString alloc]initWithString:TipString];
        self.tableView.tableFooterView.hidden = YES;
    }
    
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        NSString *searchText = textField.text;
        [self filterFriendByKeyword:searchText];
        
        [self.tableView reloadData];
        if (![StringUtil isEmpty:searchText]) {
            NSArray *txtArr = @[StrF(@"%@ ", TipString), searchText];
            NSArray *colorArr = @[[UIColor blackColor], [UIColor blueColor]];
            NSArray *fontArr = @[[UIFont font15], [UIFont font15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
            
            _footer.nameLbl.attributedText = att;
            self.tableView.tableFooterView.hidden = NO;
        }else{
            _footer.nameLbl.attributedText = [[NSMutableAttributedString alloc]initWithString:TipString];
            self.tableView.tableFooterView.hidden = YES;
        }
    }
}

-(void )filterFriendByKeyword:(NSString *)keyword{
    [_searchResult removeAllObjects];
    if ([StringUtil isEmpty:keyword]) {
        return;
    }
    
    
    keyword = [[StringUtil chinasesCharToLetter:keyword] lowercaseString];
    NSMutableArray* tmpFriendArray = [[NSMutableArray alloc]initWithCapacity:10];
    NSMutableArray* tmpGroupArray = [[NSMutableArray alloc]initWithCapacity:10];
    NSMutableArray* tmpContactArray = [[NSMutableArray alloc]initWithCapacity:10];
    NSMutableArray* tmpConversationArray = [[NSMutableArray alloc]initWithCapacity:10];
    [self.friendArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        FriendModel* model = (FriendModel *)obj;
        NSString* friendName = [[StringUtil chinasesCharToLetter:model.name] lowercaseString];
        if ([friendName rangeOfString:keyword].location != NSNotFound ) {
            [tmpFriendArray addObject:model];
        }
    }];
    [self.groupArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ChatGroupModel* model = (ChatGroupModel *)obj;
        NSString* groupName = [[StringUtil chinasesCharToLetter:model.groupName] lowercaseString];
        if ([groupName rangeOfString:keyword].location != NSNotFound ) {
            [tmpGroupArray addObject:model];
        }
    }];
    
    
    [self.contactArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        FriendModel* model = (FriendModel *)obj;
        NSString* friendName = [[StringUtil chinasesCharToLetter:model.addressListName] lowercaseString];
        if ([friendName rangeOfString:keyword].location != NSNotFound ) {
            [tmpContactArray addObject:model];
        }
    }];
    
    [self.conversationArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MXConversation* model = (MXConversation *)obj;
        if (model.conversationType == eConversationTypeChat) {
            FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:model.chat_id];
            NSString* friendName = [[StringUtil chinasesCharToLetter:friend.name] lowercaseString];
            if ([friendName rangeOfString:keyword].location != NSNotFound ) {
                [tmpConversationArray addObject:model];
            }
        }else if(model.conversationType == eConversationTypeGroupChat){
            ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:model.chat_id];
            NSString* groupName = [[StringUtil chinasesCharToLetter:group.groupName] lowercaseString];
            if ([groupName rangeOfString:keyword].location != NSNotFound ) {
                [tmpConversationArray addObject:model];
            }
        }
    }];
    [self.searchResult addObject:tmpFriendArray];
    [self.searchResult addObject:tmpGroupArray];
    [self.searchResult addObject:tmpContactArray];
    [self.searchResult addObject:tmpConversationArray];
    [self.tableView reloadData];
}

-(NSString*)sectionTitle:(NSInteger) segIndex section:(NSInteger)section{
    if (_searchResult && _searchResult.count > 0 ){
        if ([[_searchResult objectAtIndex:section] count] != 0){
            if (section == 0) {
                return @"好友";
            }else if(section == 1){
                return @"群聊";
            }else if(section == 2){
                return @"手机通讯录";
            }else if(section == 3){
                return @"聊天记录";
            }
        }
    }
    
    return nil;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.searchBars resignFirstResponder];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString* sectionTitle = [self sectionTitle:0 section:section];
    if (sectionTitle==nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label=[[UILabel alloc] init];
    label.frame=CGRectMake(12, headerHeight-20, 300, 20);
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor blackColor];
    label.font=[UIFont fontWithName:@"Helvetica-Bold" size:14];
    label.text=sectionTitle;
    
    // Create header view and add label as a subview
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, headerHeight)];
    [sectionView setBackgroundColor:[UIColor whiteColor]];
    [sectionView addSubview:label];
    
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (_searchResult && _searchResult.count > 0 ){
        NSString* sectionTitle = [self sectionTitle:0 section:section];
        if (sectionTitle!=nil) {
            return headerHeight;
        }
    }
    return 0.01;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = ({
            UITextField *object = [UITextField new];
            [object addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
            [object setViewCornerRadius:15];
            object.font = [UIFont font15];
            object.backgroundColor = [UIColor whiteColor];
            UIView *leftView = [UIView new];
            leftView.size = CGSizeMake(40, 30);
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"im_search_icon"] forState:UIControlStateNormal];
            [leftBtn sizeToFit];
            leftBtn.x = 10;
            leftBtn.y = 5;
            [leftView addSubview:leftBtn];
            object.leftView = leftView;
            object.returnKeyType = UIReturnKeySearch;
            object.leftViewMode = UITextFieldViewModeAlways;
            object.clearButtonMode = UITextFieldViewModeWhileEditing;
            NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索"] colors:@[[UIColor moBlack]] fonts:@[[UIFont font14]]];
            object.attributedPlaceholder = att;
            object.myLeft = 5;
            object.myWidth = SCREEN_WIDTH - 10;
            object.myHeight = 35;
            object.myTop = 10;
            object.myBottom = 10;
            [object setViewCornerRadius:17];
            object;
        });
    }
    return _searchTF;
}

@end
