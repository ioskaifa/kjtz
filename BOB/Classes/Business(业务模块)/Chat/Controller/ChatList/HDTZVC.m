//
//  NewFriendVC.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/21.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "HDTZVC.h"
#import "MXBackButton.h"
#import "HDTZCellView.h"
#import "LcwlChat.h"
#import "FriendsManager.h"
#import "LcwlBlankPageView.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "FriendsManager.h"
#import "MXChatDBUtil.h"
#import "MXConversation.h"
#import "HDTZModel.h"
static NSString *const kReuseIdentifier = @"CellReuseIdentifier";

#define personal_details_backbutton_width 60
#define personal_details_backbutton_height 44
@interface HDTZVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) MXBackButton *rightButton;
/** 列表 */
@property (nonatomic, strong) UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *dataSource;

@property (strong, nonatomic) MXConversation* conversation;

@end

@implementation HDTZVC

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.definesPresentationContext = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNavBarTitle:@"互动通知"];
    [self initUI];
    [self initData];
    AdjustTableBehavior(self.tableView)
}
-(void)initData{
    _dataSource = [[NSMutableArray alloc]initWithCapacity:10];
    self.conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:hdtz];
    [self.conversation markAllMessagesAsRead];
    NSMutableDictionary* paramDict = [[NSMutableDictionary alloc]initWithCapacity:10];
    [paramDict setValue:self.messageId forKey:@"msg_code"];
    @weakify(self)
//    [FriendsManager getCircleNoticeList:paramDict completion:^(id array, NSString *error) {
//        @strongify(self)
//        self.dataSource = array;
//        if (self.dataSource.count == 0) {
//            [LcwlBlankPageView addBlankPageView:LcwlBlankRecentlyHDTZVCype withSuperView:self.view touchedBlock:^{
//
//            }];
//            self.tableView.mj_header.hidden = YES;
//            self.tableView.mj_footer.hidden = YES;
//        }
//        [self.tableView reloadData];
//
//    }];
//    [self.tableView addLegendHeaderWithRefreshingBlock:^{
//        @strongify(self)
//        [self.dataSource removeAllObjects];;
//        [self getCircleNoticeList];
//    }];
//    [self.tableView addLegendFooterWithRefreshingBlock:^{
//        @strongify(self)
//        [self getCircleNoticeList];
//    }];
    
}

-(void)getCircleNoticeList{
    NSMutableDictionary* paramDict = [[NSMutableDictionary alloc]initWithCapacity:10];
    HDTZModel *model = self.dataSource.lastObject;
    if (model != nil) {
        [paramDict setValue:model.notice_id forKey:@"notice_id"];
    }
    if ( _dataSource.count == 0) {
        [paramDict setValue:self.messageId forKey:@"msg_code"];
    }
    @weakify(self)
//    [FriendsManager getCircleNoticeList:paramDict completion:^(id array, NSString *error) {
//        [self.tableView.mj_header endRefreshing];
//        [self.tableView.mj_footer endRefreshing];
//        @strongify(self)
//        if (self.dataSource) {
//            [self.dataSource addObjectsFromArray:array];;
//            [self.tableView reloadData];
//        }else{
//
//        }
//    }];
}


//消息列表
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [[UIView alloc] init];
        AdjustTableBehavior(_tableView);
        [_tableView registerNib:[UINib nibWithNibName:@"CareFansCell" bundle:nil] forCellReuseIdentifier:@"CareFansCell"];
    }
    return _tableView;
}

-(MXBackButton* )rightButton{
    if (!_rightButton) {
        _rightButton = [MXBackButton buttonWithType:UIButtonTypeCustom];
        _rightButton.frame = CGRectMake(16, personal_details_backbutton_height / 2 - personal_details_backbutton_height / 2, personal_details_backbutton_width, personal_details_backbutton_height);
        [_rightButton setTitle:@"清空" forState:UIControlStateNormal];
        [_rightButton setTitle:@"清空" forState:UIControlStateHighlighted];
        [_rightButton setImage:nil forState:UIControlStateNormal];
        [_rightButton setImage:nil forState:UIControlStateHighlighted];
        [_rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        _rightButton.titleLabel.font = [UIFont font14];
        [_rightButton addTarget:self action:@selector(clear:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightButton;
}
-(void)clear:(id)sender{
    [UIActionSheet actionSheetWithTitle:@"确定清空互动通知？" message:nil destructiveButtonTitle:@"确定" buttons:nil showInView:MoApp.window onDismiss:^(NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            HDTZModel *model = self.dataSource.firstObject;
            if (model == nil) {
                return;
            }
            @weakify(self)
//            [FriendsManager delCircleNotice:@{@"notice_id":model.notice_id} completion:^(BOOL success,NSString *error) {
//                @strongify(self)
//                if(success){
//                    [self.dataSource removeAllObjects];
//                    [self.tableView reloadData];
//                    [LcwlBlankPageView addBlankPageView:LcwlBlankRecentlyHDTZVCype withSuperView:self.view touchedBlock:^{
//                        
//                    }];
//                    self.tableView.mj_header.hidden = YES;
//                    self.tableView.mj_footer.hidden = YES;
//                    [[MXChatDBUtil sharedDataBase] clearMessages:hdtz];
//                    MXConversation* conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:hdtz];
//                    [conversation.messages removeAllObjects];
//                }
//            }];
        }
        
    } onCancel:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        HDTZCellView* view = [[HDTZCellView alloc]init];
        view.tag = 101;
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(cell.contentView);
        }];
        
    }
    HDTZCellView* tmpView = (HDTZCellView*)[cell.contentView viewWithTag:101];
    HDTZModel* model = [self.dataSource safeObjectAtIndex:indexPath.row];
    [tmpView reloadData:model];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HDTZModel* model = self.dataSource[indexPath.row];
    [MXRouter openURL:@"lcwl://SCDetailFromTransferListVC" parameters:@{@"circle_id":model.circle_id ?: @""}];
}

-(void)initUI{
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    UIBarButtonItem *barBtnItem1 = [[UIBarButtonItem alloc] initWithCustomView:self.rightButton];
    self.navigationItem.rightBarButtonItem = barBtnItem1;
    [self.navigationController.navigationBar setShadowImage:[self imageWithColor:[UIColor colorWithHexString:@"#f0f0f0"] size:CGSizeMake([UIScreen mainScreen].bounds.size.width, 0.5)]];
}
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    if (!color || size.width <= 0 || size.height <= 0) return nil;
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


@end
