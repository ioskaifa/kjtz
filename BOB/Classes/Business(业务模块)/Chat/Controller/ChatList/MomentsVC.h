//
//  MomentsVC.h
//  Lcwl
//
//  Created by AlphaGO on 2018/11/15.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MomentsVC : BaseViewController

@property (nonatomic, strong) NSMutableArray          *dataSource;

@property (nonatomic, strong) UITableView *conversationListTableView;

///客户端登录状态发生变化的情况，及时刷新
- (void)refreshHeaderView;

@end

NS_ASSUME_NONNULL_END
