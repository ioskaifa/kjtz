//
//  interactiveNotificationVC.h
//  Lcwl
//
//  Created by mac on 2019/1/22.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HDTZVC : BaseViewController

@property (nonatomic , copy) NSString* messageId;

@end

NS_ASSUME_NONNULL_END
