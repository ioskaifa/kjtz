//
//  RedPacketManager.m
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import "RedPacketManager.h"
#import "RedPacketModel.h"
#import "RedPacketSenderModel.h"
@implementation RedPacketManager

+ (MXRequest *)sendPacket:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"bonus/add"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"status"] integerValue] == 0) {
                completion(YES,[tempDic valueForKey:@"data"]);
            }else{
                [NotifyHelper showMessageWithMakeText:tempDic[@"msg"]];
            }
        }).failure(^(id error){
            if (completion) {
                [NotifyHelper showMessageWithMakeText:error];
                completion(false,error);
            }
        })
        .execute();
    }];
    return nil;
}

/*
 54、抢红包（聊天红包模块）(MD5验签方式）

 90—已领取
 91—已失效
 92—已领完
 93—异常
 96—待领取
 99—抢成功
 */
+ (MXRequest *)receiveRedPacket:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"bonus/pay"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic isSuccess]) {
                completion([tempDic valueForKeyPath:@"data.status"],tempDic[@"msg"]);
            }else{
                [NotifyHelper showMessageWithMakeText:tempDic[@"msg"]];
            }
        }).failure(^(id error){
            if (completion) {
                [NotifyHelper showMessageWithMakeText:error];
                completion(nil,error);
            }
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)getRedPacketDetail:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"bonus/desc"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic isSuccess]) {
                Block_Exec(completion,[tempDic valueForKey:@"data"],nil);
            }else{
                [NotifyHelper showMessageWithMakeText:tempDic[@"msg"]];
            }
        }).failure(^(id error){
            if (completion) {
                [NotifyHelper showMessageWithMakeText:error];
                completion(nil,error);
            }
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)getRedPacketDetailList:(NSDictionary*)param completion:(MXHttpRequestListCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"bonus/users"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                completion([tempDic valueForKey:@"data"],nil);
            }else{
                [NotifyHelper showMessageWithMakeText:tempDic[@"msg"]];
            }
        }).failure(^(id error){
            if (completion) {
                [NotifyHelper showMessageWithMakeText:error];
                completion(false,error);
            }
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)getReceiveStatus:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"bonus/check_bonus"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param)
        .finish(^(id data){
            Block_Exec(completion,data,nil);
        }).failure(^(id error){
            if (completion) {
                [NotifyHelper showMessageWithMakeText:error];
                completion(nil,error);
            }
        })
        .execute();
    }];
    return nil;
}


+ (MXRequest *)getMyRedPacketList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"bonus/lists"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic isSuccess]) {
                NSArray* dataArray = [tempDic valueForKey:@"data"];
                if (dataArray) {
                    if (dataArray.count >0) {
                        NSMutableArray* data = [[NSMutableArray alloc]initWithCapacity:10];
                        [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            NSDictionary* tmpDict = (NSDictionary*)obj;
                            RedPacketSenderModel* model = [RedPacketSenderModel dictionaryToModal:tmpDict];
                            [data addObject:model];
                        }];
                        
                        completion(data,nil);
                    }else{
                        completion(@[],nil);
                    }
                }
            }else{
                [NotifyHelper showMessageWithMakeText:tempDic[@"msg"]];
//                 completion(nil,tempDic[@"msg"]);
            }
        }).failure(^(id error){
            if (completion) {
                completion(nil,error);
            }
        })
        .execute();
    }];
    return nil;
}
+ (MXRequest *)getMyRedPacketTotal:(NSDictionary*) param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,@"bonus/index"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic isSuccess]) {
                completion([tempDic valueForKey:@"data"],tempDic[@"msg"]);
            }else{
                [NotifyHelper showMessageWithMakeText:tempDic[@"msg"]];
            }
        }).failure(^(id error){
            if (completion) {
                [NotifyHelper showMessageWithMakeText:error];
                completion(nil,error);
            }
        })
        .execute();
    }];
    return nil;
}
@end
