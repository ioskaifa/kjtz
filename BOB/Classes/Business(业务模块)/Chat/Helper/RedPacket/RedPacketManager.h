//
//  RedPacketManager.h
//  Lcwl
//
//  Created by mac on 2019/1/2.
//  Copyright © 2019 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface RedPacketManager : NSObject

+ (MXRequest *)sendPacket:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion;

+ (MXRequest *)getRedPacketDetail:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion;

+ (MXRequest *)getRedPacketDetailList:(NSDictionary*)param completion:(MXHttpRequestListCallBack)completion;

+ (MXRequest *)receiveRedPacket:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion;
//红包接口（查看红包状态）
+ (MXRequest *)getReceiveStatus:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion;

+ (MXRequest *)getMyRedPacketList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion;

+ (MXRequest *)getMyRedPacketTotal:(NSDictionary*) param completion:(MXHttpRequestObjectCallBack)completion;


@end

NS_ASSUME_NONNULL_END
