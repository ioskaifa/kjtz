//
//  LcwlChatHelper.m
//  Lcwl
//
//  Created by mac on 2018/11/24.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "LcwlChatHelper.h"
#import "MXNet.h"
#import "FriendModel.h"
#import "LcwlChat.h"
#import "FriendsManager.h"
#import "RoomManager.h"
@implementation LcwlChatHelper

+(void)requestFriends{
    [self requestFriend:@""];
}

+ (void)requestFriend:(NSString *)lastId {
    [FriendsManager getFriendList:@{@"last_id":lastId?:@""}
                       completion:^(id array, NSString *error) {
        if (array && [array isKindOfClass:[NSArray class]]) {
            [[LcwlChat shareInstance].chatManager insertFriends:array];
            //如果array不为0 继续拉数据 分页请求
            FriendModel *obj = [array lastObject];
            if ([obj isKindOfClass:FriendModel.class]) {
                NSString *lastId = obj.ID;
                if ([StringUtil isEmpty:lastId]) {
                    return;
                }
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(queue, ^{
                    [self requestFriend:lastId];
                });
            }
        }
    }];
}

+(void)requestGroups{
    [RoomManager getGroupList:@{} completion:^(id array, NSString *error) {
    }];
}

@end
