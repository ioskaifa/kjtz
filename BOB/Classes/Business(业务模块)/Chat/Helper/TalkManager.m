//
//  TalkManager.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/3/31.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "TalkManager.h"
#import "UserModel.h"
#import "MXChatDBUtil.h"
#import "NSDate+Category.h"
#import "MXConversation.h"
#import "LcwlChat.h"
#import "DocumentManager.h"
//#import "GroupModel.h"
//#import "ChatViewVC.h"
//#import "ChatSendHelper.h"
//#import "GetFriendsManager.h"
#import "NSString+DateFormatter.h"
//#import "GetShopManager.h"
//#import "MXShopDetailInfoModel.h"
//#import "IShopInfo.h"
#import "ImageUtil.h"
#import "YYImage.h"
#import "MXEmotionManager.h"
#import "MXFileManager.h"
#import "TalkUploadAudio.h"
#import "QNManager.h"
#import "ChatCacheFileUtil.h"

@implementation TalkManager
{
     NSDictionary *_emojiMapper;
    
     MXFileManager *_fileManger;
}




+ (TalkManager*)manager {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (id)init {
    
    self = [super init];
    
    if (self) {
        _emojiMapper  = [NSMutableDictionary dictionary];
        _fileManger = [MXFileManager manager];
    }
    
    return self;
}

- (void)uploadImageFile:(NSDictionary*)param completion:(MXHttpRequestListCallBack)completion setProgress:(void (^)(float,NSString*))progressBlock{
    
    NSString *file = param[@"image"];
    UIImage* image = [[UIImage alloc]initWithContentsOfFile:file];
    NSData *data = UIImageJPEGRepresentation(image, 0.9);
    [_fileManger uploadImageWithDictionary:param fromData:data progress:^(NSProgress *uploadProgress) {
        
        CGFloat progress =1.0 *uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
        
        if (progressBlock) {
            progressBlock(progress,@"");
        }
        
    } success:^(id responseObject) {
        
        NSDictionary *dic = (NSDictionary*)responseObject;
        
        if (dic) {
            
            NSString *url = dic[@"data"];
            
            if (url.length > 0) {
                NSMutableArray* imageArray = [NSMutableArray array];
                [imageArray addObject:url];
                completion(imageArray,nil);

                
            }else {
   
                completion(nil,nil);
            }
        }
        
        
    } failure:^(NSError *error) {
        
        completion(nil,error.description);
    }];
    

}
+(NSString* )msgId{
    double timeInterval = [[NSDate date] timeIntervalSince1970];
    NSString* time = [NSString stringWithFormat:@"%lld",(long long)(timeInterval * 1000)];
    return [NSString stringWithFormat:@"%d%@",arc4random()%100,time];
}


+(NSString*)deleteInputText:(NSString*)s {
    NSLog(@"%lu",(unsigned long)s.length);
    if([s length]<=0)
        return @"";
    NSInteger n=-1;
    
    if( [s characterAtIndex:[s length]-1] == ']'){
        for(NSInteger i=[s length]-1;i>=0;i--){
            if( [s characterAtIndex:i] == '[' ){
                n = i;
                break;
            }
        }
    }
 
    NSDictionary* emojiDict = [TalkManager manager].emotionMapper;//[OHASBasicHTMLParser getEmotDict];
    
    if (n>=0) {
        NSRange range;
        range.location = n;
        range.length = [s length]-n;
//        NSString* string = [s substringWithRange:range];
        NSString* emojiKey = [s substringWithRange:NSMakeRange(n,(s.length-n))];
        if (emojiDict[emojiKey]) {
            return  [s substringWithRange:NSMakeRange(0,n)];
        }else{
            return  [s substringToIndex:[s length]-1];
        }
    }else{
        return  [s substringToIndex:[s length]-1];
    }
}

+(BOOL) isOverTopNums{
    
//    int friendNums = 0;
//    int groupNums = 0;
//    NSArray* friends = [[MXChat sharedInstance].chatManager users];
//    NSArray* groups = [[MXChat sharedInstance].chatManager groupList];
//    for (MoYouModel* friend in friends) {
//        if (friend.enable_top) {
//            friendNums++;
//        }
//    }
//    for (ChatGroupModel* group in groups) {
//        if (group.enable_top) {
//            groupNums++;
//        }
//    }
//    if ((friendNums+groupNums) > 4) {
//        return YES;
//    }
    return NO;
}


+(NSString*)timeDataString:(NSString*)time{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    NSDate *msgDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:[time doubleValue]];
    if ([msgDate isToday]) {
        [formatter setDateFormat:@"HH:mm"];
    }else {
        [formatter setDateFormat:MXLang(@"Public_dateFormat_6",@"yyyy-MM-dd HH:mm")];
    }
    return [formatter stringFromDate:msgDate];
}



+(NSInteger)searchMessage:(NSMutableArray*) messages withModel:(MessageModel*)message{
    for (int i=0; i<messages.count; i++) {
        id model = [messages safeObjectAtIndex:i];
//        if ([model isKindOfClass:[MXConversation class]]) {
//            MXConversation* conversation = [messages safeObjectAtIndex:i];
//            if ([conversation.chat_id isEqualToString:message.chat_with]) {
//                    return i;
//            }
//        }else
            if([model isKindOfClass:[MessageModel class]]){
            MessageModel* tmp = (MessageModel*)model;
            if ([tmp.messageId isEqualToString:message.messageId]) {
                return i;
            }
        }
    }
    return -1;
}


+ (NSString*)cellBageNumberString:(NSInteger)num {
    NSString* numString = [NSString stringWithFormat:@"%ld",(long)num];
    if (num > 99 ) {
        numString = @"99+";
    }
    return numString;
}
+(NSArray*)getListOperationByType:(EMConversationType)type top:(BOOL)isTop{
    NSMutableArray* operationArray = [[NSMutableArray alloc]initWithCapacity:0];
//    if ( type == eConversationTypeChat) {// || type == econversationTypeSubMopromoChat
//        [operationArray addObject: MXLang(@"Talk_chatview_bottom_toolbar_moreview_11", @"销毁对话")];
//    }
//    NSString* topString = isTop?MXLang(@"Talk_chatList_slider_selectItem_2", @"置顶"):MXLang(@"Talk_chatList_slider_selectItem_4", @"取消置顶");
//    [operationArray addObject:topString];
    [operationArray addObject:MXLang(@"Talk_chatList_slider_selectItem_3", @"删除")];
    return operationArray;
}
+(NSArray*)getListColorByType:(EMConversationType) type{
    NSMutableArray* colorArray = [[NSMutableArray alloc]initWithCapacity:0];
//    if ( type == eConversationTypeChat || type == econversationTypeSubMopromoChat ) {
//        [colorArray addObject: [UIColor colorWithRed:.8f green:.8f blue:.8f alpha:1.0]];
//    }
//    [colorArray addObject:[UIColor colorWithRed:.7f green:.7f blue:.7f alpha:1.0]];
    [colorArray addObject:[UIColor colorWithRed:1.f green:.28f blue:.32f alpha:1.0]];
    return colorArray;

}


+(NSMutableArray*)sortConversation:(NSArray*)conversations{
    NSMutableArray *ret = nil;
    NSArray* sorte = [conversations sortedArrayUsingComparator:
     ^(MXConversation *obj1, MXConversation* obj2){
         if (obj1.conversationType == eConversationTypeXDPYChat) {
             return (NSComparisonResult)NSOrderedAscending;
         }
         if (obj2.conversationType == eConversationTypeXDPYChat) {
             return (NSComparisonResult)NSOrderedDescending;
         }
         if (obj1.conversationType == eConversationTypeHDTZChat) {
             return (NSComparisonResult)NSOrderedAscending;
         }
         if (obj2.conversationType == eConversationTypeHDTZChat) {
             return (NSComparisonResult)NSOrderedDescending;
         }
       
        
         BOOL obj1Top = false;
         BOOL obj2Top = false;
         //判断置顶
         if(obj1.conversationType == eConversationTypeGroupChat){
             ChatGroupModel* group1 = [[LcwlChat shareInstance].chatManager loadGroupByChatId:obj1.chat_id];
             obj1Top = group1.enable_top;
         }else {
             FriendModel *friend1 = [[LcwlChat shareInstance].chatManager loadFriendByChatId:obj1.chat_id];
             obj1Top = friend1.enable_top;
         }
         if(obj2.conversationType == eConversationTypeGroupChat){
             ChatGroupModel* group2 = [[LcwlChat shareInstance].chatManager loadGroupByChatId:obj2.chat_id];
             obj2Top = group2.enable_top;
         }else {
             FriendModel *friend2 = [[LcwlChat shareInstance].chatManager loadFriendByChatId:obj2.chat_id];
             obj2Top = friend2.enable_top;
         }
         //判断消息时间
         MessageModel *message1 = [obj1 latestMessage];
         MessageModel *message2 = [obj2 latestMessage];
         double timestamp1 = 0;
         double timestamp2 = 0;
         if (message1 == nil) {
            timestamp1 = [obj1.lastClearTime doubleValue];
         }else{
            timestamp1 = [message1.sendTime doubleValue];
         }

         if (message2 == nil) {
             timestamp2 = [obj2.lastClearTime doubleValue];
         }else{
             timestamp2 = [message2.sendTime doubleValue];
         }
        
         if (obj1Top && obj2Top) {
             if (timestamp1 > timestamp2) {
                 return(NSComparisonResult)NSOrderedAscending;
             }else{
                 return(NSComparisonResult)NSOrderedDescending;
             }
         }else if(!obj1Top && !obj2Top){
             if(timestamp1 > timestamp2) {
                 return(NSComparisonResult)NSOrderedAscending;
             }else {
                 return(NSComparisonResult)NSOrderedDescending;
             }
         }else{
             if (obj1Top) {
                 return (NSComparisonResult)NSOrderedAscending;
             }
             return(NSComparisonResult)NSOrderedDescending;
         }}];
    ret = [[NSMutableArray alloc] initWithArray:sorte];
    return ret;
}


+(NSString*)getFocusHead:(MessageModel*) model{
    NSString* string = @"";
    if (model.msg_direction) {
        string =  MXLang(@"Talk_Focus_you_follow_tip", @"你关注了对方");
    }else{
        string =  MXLang(@"Talk_Focus_i_follow_tip", @"对方关注了你");
    }
    return string;
}

+(NSArray*)friendSegs{
    return  @[
              @{@"text":MXLang(@"Talk_friend_title_3", @"好友")},
              @{@"text":MXLang(@"Talk_msg_group_title", @"群组")},
              @{@"text":MXLang(@"Talk_friend_attentionShop_title_32", @"爱店")},
              @{@"text":MXLang(@"Talk_friend_Chats_tabBtn_title_friensGroup",@"好友分组")}];
}

+(NSString*)showNotificationContent:(MessageModel*)message{
    NSString* messageStr = @"";
    if (message.type == MessageTypeNormal || message.type == MessageTypeGroupchat ) {
        
        switch (message.subtype) {
            case kMXMessageTypeImage:{
                messageStr = MXLang(@"Public_notify_title1", @"发来了[图片]");
            } break;
            case kMXMessageTypeText:{
                // 表情映射。
                messageStr = message.content;
            } break;
            case kMXMessageTypeVoice:{
                messageStr = MXLang(@"Public_notify_title2", @"发来了[语音消息]");
            } break;
            case kMXMessageTypeLocation: {
                messageStr = MXLang(@"Public_notify_title3",@"发来了[地理位置]");
            } break;
            case kMXMessageTypeGif:{
                messageStr =MXLang(@"Public_notify_title4",  @"发来了[动画表情]");
            }
            default: {
            } break;
        }
        
    }
    return messageStr;
}


+(BOOL)isAvoidDisturbTime{
    NSDate *nowTime = [NSDate date];
    // 此处为设置里面的开关
    BOOL isAvoidDisturbTime = [[MXCache valueForKey:kGlobalAvoidTimeDisturb] boolValue];
    if (isAvoidDisturbTime) {
        NSInteger startTime = [[MXCache valueForKey:kGlobalStartTimeDisturb] integerValue];
        NSInteger endTime   = [[MXCache valueForKey:kGlobalEndTimeDisturb] integerValue];
        endTime %= D_DAY;
        //当前一天的秒数
        NSInteger daySeconds = [nowTime hour] * D_HOUR + [nowTime minute] * D_MINUTE;
        if (startTime < endTime) {
            //表示设置是当天
            if (daySeconds > startTime && daySeconds < endTime) {
                return YES;
            }
        }else if (startTime > endTime) {
            //表示设置是隔天
            if (daySeconds > startTime || daySeconds < endTime) {
                return YES;
            }
        }
        
    }
    
    return NO;
}

+(NSString*)cellIdentifierForMessageModel:(MessageModel*)model{
    NSString *identifier = @"MessageCell";
    if (model.msg_direction) {
        identifier = [identifier stringByAppendingString:@"i"];
    }else{
        identifier = [identifier stringByAppendingString:@"you"];
    }
    if (model.type == MessageTypeNormal || model.type == MessageTypeGroupchat) {
        switch (model.subtype) {
            case kMXMessageTypeText:
            {
                identifier = [identifier stringByAppendingString:@"Text"];
                
            }
                break;
            case kMXMessageTypeLocation:
            {
                identifier = [identifier stringByAppendingString:@"Location"];
            }
                break;
            case kMXMessageTypeImage:
            {
                identifier = [identifier stringByAppendingString:@"Image"];
            }
                break;
            case kMXMessageTypeVoice : {
                identifier = [identifier stringByAppendingString:@"Voice"];
            }
                break;
            case kMXMessageTypeGif:
            {
                identifier = [identifier stringByAppendingString:@"gif"];
            }
                break;
            case kMxmessageTypeRedPack: {
                identifier = [identifier stringByAppendingString:@"redpack"];
                break;
            }
            case kMXMessageTypeVoiceChat: {
                identifier = [identifier stringByAppendingString:@"voiceChat"];
                break;
            }
            case kMXMessageTypeFile: {
                identifier = [identifier stringByAppendingString:@"file"];
                break;
            }
            case kMxmessageTypeVideo: {
                identifier = [identifier stringByAppendingString:@"video"];
                break;
            }
            default:
                break;
        }
    }
    return identifier;
}


+(void)removeEmptyConversation{
    NSArray *conversations = [[LcwlChat shareInstance].chatManager conversations];
    NSMutableArray *needRemoveConversations;
    for (MXConversation *conversation in conversations) {
        if (!conversation.latestMessage && [StringUtil isEmpty:conversation.lastClearTime] ) {
            if (!needRemoveConversations) {
                needRemoveConversations = [[NSMutableArray alloc] initWithCapacity:0];
            }
            if (![StringUtil isEmpty:conversation.chat_id]) {
                [needRemoveConversations safeAddObj:conversation.chat_id];
            }
        }
    }
    
    if (needRemoveConversations && needRemoveConversations.count > 0) {
        for (NSString* chatId in needRemoveConversations) {
            [[LcwlChat shareInstance].chatManager deleteConversationByChatter:chatId];
        }
    }
}

+(NSString*)saveChatImage:(UIImage *)image{

    double timeInterval = [[NSDate date] timeIntervalSince1970];
    NSString* dateString = [NSString stringWithFormat:@"%d%lu",(int)timeInterval, (unsigned long)image.hash];
    NSString *imagePath=[DocumentManager fileSavePath:dateString];
    NSData *tempData=[ImageUtil compressionImage:image length:300];
    //儲存
    [tempData writeToFile:imagePath atomically:NO];
    return imagePath;
}


//提供解析富文本的mapping
- (UIImage *)imageWithName:(NSString *)path {
    NSData *data = [NSData dataWithContentsOfFile:path];
    YYImage *image = [YYImage imageWithData:data scale:2];
    image.preloadAllAnimatedImageFrames = YES;
    return image;
}
- (NSDictionary *)emotionMapper{
    if (_emojiMapper.allKeys.count > 0) {
        return _emojiMapper;
    }
    NSArray* emojiArray = [[MXEmotionManager manager] defaultEmoji];
    [emojiArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj && [obj isKindOfClass:[NSDictionary class]]) {
            NSString* key = obj[@"faceName"];
            NSString* emojiPath = [[MXEmotionManager manager] emojiPath];
            NSString* value = [NSString stringWithFormat:@"%@%@.png",emojiPath,obj[@"faceImageName"]];
            if (![StringUtil isEmpty:key] && ![StringUtil isEmpty:value]) {
                [_emojiMapper setValue:[self imageWithName:value] forKey:key];
            }
        }
    }];
    [_emojiMapper setValue:[UIImage imageNamed:@"icon_hongbao2_nor"] forKey:@"[红包]"];
    return _emojiMapper;
}


+ (MXRequest*)downloadAudioFileByMessage:(MessageModel*)msg completion:(MXHttpRequestCallBack)completion{
    ///文件链接
    NSString *urlString = msg.urlString;
    if ([StringUtil isEmpty:urlString]) {
        return nil;
    }
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (!error) {
            NSString *fullPath = msg.fullPath;
            [[NSFileManager defaultManager]moveItemAtURL:location toURL:[NSURL fileURLWithPath:fullPath] error:nil];
            completion(YES,fullPath);
        }else{
             completion(false,nil);
        }
    }];
    
    [downloadTask resume];
    return nil;
}

+ (MXRequest*)downloadFile:(MessageModel*)msg completion:(MXHttpRequestCallBack)completion {
    NSString *urlString = msg.urlString;
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            NSString *fullPath = msg.fullPath;
            [[NSFileManager defaultManager]moveItemAtURL:location toURL:[NSURL fileURLWithPath:fullPath] error:nil];
            completion(YES,fullPath);
        }else{
             completion(false,nil);
        }
    }];
    
    [downloadTask resume];
    return nil;
}

+(FriendModel*)newFriend{
    FriendModel* friend = [[FriendModel alloc]init];
    friend.avatar = NewFriendAvatar;
    friend.name = MXLang(@"Talk_msg_chat_new_friend_76", @"新的好友");
    friend.userid = nf;
    friend.followState = 1;
    return friend;
}

+(FriendModel*)hudongtongzhi{
    FriendModel* friend = [[FriendModel alloc]init];
    friend.avatar = hudongtongzhiAvatar;
    friend.name = @"互动通知";
    friend.userid = hdtz;
    friend.followState = 1;
    return friend;
}


+(void)pushChatViewUserId:(NSString *)userId type:(EMConversationType)type{
    BaseNavigationController * vc = (BaseNavigationController*)[MXRouter sharedInstance].getTopNavigationController;
    NSArray* tmpArray = vc.viewControllers;
    for (NSInteger i=(tmpArray.count-1); i>=0; i--) {
        UIViewController<IMXChatManagerDelegate>* vc = tmpArray[i];
        if ([vc isKindOfClass:NSClassFromString(@"ChatViewVC")]) {
            [[LcwlChat shareInstance].chatManager removeDelegate:vc];
        }
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [vc popToRootViewControllerAnimated:NO];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [MXRouter openURL:@"lcwl://ChatViewVC" parameters:@{@"chatId":userId,@"type":@(type)}];
        });
    });
}

@end
