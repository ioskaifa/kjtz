//
//  LoginApi.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/2/6.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "MXRequest.h"
#import "MXBaseRequestApi.h"

@interface TalkUploadApi : MXBaseRequestApi
- (id)initWithParameter:(NSDictionary*)dictionary;

- (NSString *)requestUrl ;

@end
