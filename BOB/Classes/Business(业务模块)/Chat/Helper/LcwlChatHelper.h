//
//  LcwlChatHelper.h
//  Lcwl
//
//  Created by mac on 2018/11/24.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LcwlChatHelper : UIView
//抓取好友
+(void)requestFriends;
//请求群组列表
+(void)requestGroups;
//得到消息展示内容
+(NSString*)messageContent:(MessageModel*)message;



@end

NS_ASSUME_NONNULL_END
