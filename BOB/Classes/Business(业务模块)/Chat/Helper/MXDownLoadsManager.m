//
//  MXDownLoadsManager.m
//  MoPal_Developer
//
//  Created by aken on 16/1/5.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "MXDownLoadsManager.h"
#import "TalkManager.h"
#import "MessageModel.h"
#import "NSObject+Additions.h"

#import "LcwlChat.h"
#import "MXChatDBUtil.h"
#import "MXFileManager.h"
#import "MXJsonParser.h"
@interface MXDownLoadsManager()

@property (nonatomic, strong, readwrite) NSMutableDictionary *successBlocksDictForDownload;
@property (nonatomic, strong) MXFileManager *fileManger;

@end

@implementation MXDownLoadsManager

+ (MXDownLoadsManager*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        _successBlocksDictForDownload = [NSMutableDictionary dictionary];
        self.fileManger = [MXFileManager manager];
    }
    
    return self;
}

- (void)downloadMessageAttachments:(NSArray *)messages
                        completion:(id<IMXDownLoadsManagerDelegate>)completion {
    
    if (messages == nil) {
        return;
    }
    
    for (MessageModel *model in messages) {
        
        if ([model isKindOfClass:[MessageModel class]] && model.subtype==kMXMessageTypeVoice &&(model.type == MessageTypeNormal || model.type == MessageTypeGroupchat)) {
            
    
            if ([StringUtil isEmpty:model.locFileUrl] && !model.msg_direction) {
                
                // 防止二次请求
                if (!_successBlocksDictForDownload[[self appendImgPath:model.fileUrl]]) {
                    
                    if ([StringUtil isEmpty:model.imageRemoteUrl]) {
                        continue;
                    }
                    
                    [_successBlocksDictForDownload setValue:model.imageRemoteUrl forKey:model.imageRemoteUrl];
                    [TalkManager downloadAudioFileByMessage:model completion:^(BOOL success, NSString *error) {
                        
                        if (success) {
                            
                            [_successBlocksDictForDownload removeObjectForKey:model.imageRemoteUrl];
                            model.locFileUrl = error;
                            [[MXChatDBUtil sharedDataBase] updateRowDataByMsgModel:model];
                            
                            if (completion && [completion respondsToSelector:@selector(downloadMessageAttachmentSuccess:)]) {
                                [completion downloadMessageAttachmentSuccess:model];
                            }
                            
                        }else {
                            [_successBlocksDictForDownload removeObjectForKey:model.imageRemoteUrl];
                        }
                        
                    }];
                }
            }// if end
            
        }// if end
        
    }// for end
    
}

- (void)downloadMessageAttachmentWithMessage:(MessageModel *)model
                        completion:(id<IMXDownLoadsManagerDelegate>)completion {
    
    if (model == nil) {
        return;
    }
        
    if (model.subtype==kMXMessageTypeVoice &&(model.type == MessageTypeNormal || model.type == MessageTypeGroupchat)) {
        
        
        if ([StringUtil isEmpty:model.locFileUrl] && !model.msg_direction) {
            
            // 防止二次请求
            if (!_successBlocksDictForDownload[model.imageRemoteUrl]) {
                
                [_successBlocksDictForDownload setValue:model.imageRemoteUrl forKey:model.imageRemoteUrl];
                [TalkManager downloadAudioFileByMessage:model completion:^(BOOL success, NSString *error) {
                    
                    if (success) {
                        
                        [_successBlocksDictForDownload removeObjectForKey:model.imageRemoteUrl];
                        model.locFileUrl = error;
                        [[MXChatDBUtil sharedDataBase] updateRowDataByMsgModel:model];
                        
                        if (completion && [completion respondsToSelector:@selector(downloadMessageAttachmentSuccess:)]) {
                            [completion downloadMessageAttachmentSuccess:model];
                        }
                        
                    }else {
                        [_successBlocksDictForDownload removeObjectForKey:model.imageRemoteUrl];
                    }
                    
                }];
            }
        }// if end
        
    }// if end
    
    
}

- (void)uploadAttachmentForImageWithMessage:(MessageModel*)msg
                                completion:(MXUploadMessageAttachment)completion
                                progress:(void (^)(float,NSString*))progressBlock {
    
    //@weakify(self)
    NSMutableDictionary* param = [[NSMutableDictionary alloc] initWithCapacity:0];
    [param setValue:msg.locFileUrl forKey:@"file"];
    [param setValue:[NSNumber numberWithInt:0] forKey:@"fileType"];
    [param setValue:@"3" forKey:@"fileClassfycation"];
    [param setValue:msg.locFileUrl forKey:@"image"];
    UserModel *userModel = UDetail.user;
    NSString* mx_id = userModel.chatUser_id;
    [param setValue:mx_id forKey:@"userId"];
    [param setObject:msg.messageId forKey:@"msgID"];
    
    __block MessageModel *tempModel = msg;
    
//    [TalkManager uploadImageFile:param completion:^(id array, NSString *error)  {
//       // @strongify(self)
//        
//        NSArray* tmpArray = array;
//        
//        if(tmpArray.count > 0){
//            
//            NSMutableDictionary *dic=[NSMutableDictionary dictionary];
//            [dic setValue:msg.sendTime forKey:@"ts"];
//            [dic setValue:@"" forKey:@"gn"];
//            [dic setValue:[NSNumber numberWithInteger:msg.subtype] forKey:@"ty"];
//            [dic setValue:tmpArray[0] forKey:@"msg"];
//            [dic setValue:tmpArray[0] forKey:@"url"];
//            NSString* avatarUrl = UDetail.user.userAvatarUrl;
//            [dic setValue:([StringUtil isEmpty:avatarUrl]?@"":avatarUrl) forKey:@"avatar"];
//            [dic setValue:UDetail.user.nickName forKey:@"name"];
//            [dic setValue:UDetail.user.sex forKey:@"gender"];
//            NSString *tempToUserId = [NSString stringWithFormat:@"%@",msg.toID];
//            [dic setValue:[NSNumber numberWithInt:msg.size.width] forKey:@"width"];
//            [dic setValue:[NSNumber numberWithInt:msg.size.height] forKey:@"height"];
//            
//            // contaisString 是ios8上才能用的
//            if([tempToUserId rangeOfString:@"_"].location !=NSNotFound)
//            {
//                NSArray* userArray = [tempToUserId componentsSeparatedByString:@"_"];
//                if (userArray.count == 3) {
//                    [dic setValue:userArray[2] forKey:@"shopid"];
//                }
//            }
//            
//            NSString * jsonString = [MXJsonParser dictionaryToJsonString:dic];
//            msg.body = jsonString;
//            msg.fileUrl = tmpArray[0];
//            
//            [[MXChat sharedInstance].chatManager sendMessage:msg completion:nil];
//            completion(YES);
//            
//        } else {
//            
//            // 更新数据库，上传图片失败
//            tempModel.state = kMXMessageState_Failure;
//            [[MXChatDBUtil sharedDataBase] updateMessageState:tempModel];
//            completion(NO);
//
//        }
//        
//    } setProgress:^(float degree,NSString* msgId) {
//        progress(degree,msgId);
//    }];
    
    UIImage* image = [[UIImage alloc]initWithContentsOfFile:msg.locFileUrl];
    NSData *data = UIImageJPEGRepresentation(image, 0.9);
    [self.fileManger uploadImageWithDictionary:param fromData:data progress:^(NSProgress *uploadProgress) {
        
        CGFloat progress =1.0 *uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
        if (progressBlock) {
            progressBlock(progress,tempModel.messageId);
        }
        
    } success:^(id responseObject) {
        
        NSDictionary *dic = (NSDictionary*)responseObject;
        
        if (dic) {
            
            NSString *url = dic[@"data"];
            
            if (url.length > 0) {
                
                NSMutableDictionary *dic=[NSMutableDictionary dictionary];
                [dic setValue:msg.sendTime forKey:@"ts"];
                [dic setValue:@"" forKey:@"gn"];
                [dic setValue:[NSNumber numberWithInteger:msg.subtype] forKey:@"ty"];
                [dic setValue:url forKey:@"msg"];
                [dic setValue:url forKey:@"url"];
                NSString* avatarUrl = UDetail.user.head_photo;
                [dic setValue:([StringUtil isEmpty:avatarUrl]?@"":avatarUrl) forKey:@"avatar"];
                [dic setValue:UDetail.user.user_name forKey:@"name"];
                [dic setValue:UDetail.user.sex forKey:@"gender"];
                NSString *tempToUserId = [NSString stringWithFormat:@"%@",msg.toID];
                [dic setValue:[NSNumber numberWithInt:msg.size.width] forKey:@"width"];
                [dic setValue:[NSNumber numberWithInt:msg.size.height] forKey:@"height"];
                
                // contaisString 是ios8上才能用的
                if([tempToUserId rangeOfString:@"_"].location !=NSNotFound)
                {
                    NSArray* userArray = [tempToUserId componentsSeparatedByString:@"_"];
                    if (userArray.count == 3) {
                        [dic setValue:userArray[2] forKey:@"shopid"];
                    }
                }
                
                NSString * jsonString = [MXJsonParser dictionaryToJsonString:dic];
                msg.body = jsonString;
                msg.fileUrl = url;
                
                [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
                completion(YES);
                
            }else {
                // 更新数据库，上传图片失败
                tempModel.state = kMXMessageState_Failure;
                [[MXChatDBUtil sharedDataBase] updateMessageState:tempModel];
                completion(NO);
                
            }
        }else {
            // 更新数据库，上传图片失败
            tempModel.state = kMXMessageState_Failure;
            [[MXChatDBUtil sharedDataBase] updateMessageState:tempModel];
            completion(NO);
            
        }

        
    } failure:^(NSError *error) {
        
        // 更新数据库，上传图片失败
        tempModel.state = kMXMessageState_Failure;
        [[MXChatDBUtil sharedDataBase] updateMessageState:tempModel];
        completion(NO);
    }];
}

- (void)uploadAttachmentForAudioWithMessage:(MessageModel*)message
                                 completion:(MXUploadMessageAttachment)completion {
    
    NSMutableDictionary* param = [[NSMutableDictionary alloc] initWithCapacity:0];
    if ([StringUtil isEmpty:message.locFileUrl]) {
        return;
    }
    [param setValue:message.locFileUrl forKey:@"file"];
    [param setValue:[NSNumber numberWithInt:1] forKey:@"fileType"];
    [param setValue:@"3" forKey:@"fileClassfycation"];
    UserModel *userModel = UDetail.user;
    NSString* mx_id = userModel.chatUser_id?:@"";
    [param setValue:mx_id forKey:@"userId"];
    
    __block MessageModel *tempModel = message;
    
    //    [TalkManager uploadAudioFile:param completion:^(id array, NSString *error) {
    //
    //        NSArray* tmpArray = array;
    //        if(tmpArray.count > 0){
    //            NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    //            [dic setValue:message.sendTime  forKey:@"ts"];
    //            [dic setValue:@"" forKey:@"gn"];
    //            [dic setValue:message.voiceLen forKey:@"len"];
    //            [dic setValue:[NSNumber numberWithInteger:message.subtype] forKey:@"ty"];
    //            [dic setValue:tmpArray[0] forKey:@"url"];
    //            [dic setValue:@"" forKey:@"msg"];
    //            NSString* avatarUrl = UDetail.user.userAvatarUrl;
    //            [dic setValue:([StringUtil isEmpty:avatarUrl]?@"":avatarUrl) forKey:@"avatar"];
    //            [dic setValue:UDetail.user.nickName forKey:@"name"];
    //            NSString *tempToUserId = [NSString stringWithFormat:@"%@",message.toID];
    //
    //            // contaisString 是ios8上才能用的
    //            if([tempToUserId rangeOfString:@"_"].location !=NSNotFound)
    //            {
    //                NSArray* userArray = [tempToUserId componentsSeparatedByString:@"_"];
    //                if (userArray.count == 3) {
    //                    [dic setValue:userArray[2] forKey:@"shopid"];
    //                }
    //            }
    //
    //
    //            NSString * jsonString = [MXJsonParser dictionaryToJsonString:dic];
    //            message.body = jsonString;
    //            [[MXChatDBUtil sharedDataBase] updateMessageBody:message];
    //            message.state = kMXMessageStateSending;
    //
    //            [[MXChat sharedInstance].chatManager sendMessage:message completion:nil];
    //
    //            completion(YES);
    //
    //        }else{
    //
    //            // 更新数据库，上传图片失败
    //            tempModel.state = kMXMessageState_Failure;
    //            [[MXChatDBUtil sharedDataBase] updateMessageState:tempModel];
    //            completion(NO);
    //        }
    //    }];
    
    NSData *data=[NSData dataWithContentsOfFile:message.locFileUrl];
    [self.fileManger uploadAudioWithDictionary:param audioData:data success:^(id responseObject) {
        
        NSDictionary *dic = (NSDictionary*)responseObject;
        if (dic) {
            
            NSString *url = dic[@"data"];
            
            if (url.length > 0) {
                NSMutableDictionary *dic=[NSMutableDictionary dictionary];
                [dic setValue:message.sendTime  forKey:@"ts"];
                [dic setValue:@"" forKey:@"gn"];
                [dic setValue:message.voiceLen forKey:@"len"];
                [dic setValue:[NSNumber numberWithInteger:message.subtype] forKey:@"ty"];
                [dic setValue:url forKey:@"url"];
                [dic setValue:@"" forKey:@"msg"];
                NSString* avatarUrl = UDetail.user.head_photo;
                [dic setValue:([StringUtil isEmpty:avatarUrl]?@"":avatarUrl) forKey:@"avatar"];
                [dic setValue:UDetail.user.user_name forKey:@"name"];
                NSString *tempToUserId = [NSString stringWithFormat:@"%@",message.toID];
                
                // contaisString 是ios8上才能用的
                if([tempToUserId rangeOfString:@"_"].location !=NSNotFound)
                {
                    NSArray* userArray = [tempToUserId componentsSeparatedByString:@"_"];
                    if (userArray.count == 3) {
                        [dic setValue:userArray[2] forKey:@"shopid"];
                    }
                }
                
                
                NSString * jsonString = [MXJsonParser dictionaryToJsonString:dic];
                message.body = jsonString;
                [[MXChatDBUtil sharedDataBase] updateMessageBody:message];
                message.state = kMXMessageStateSending;
                
                [[LcwlChat shareInstance].chatManager sendMessage:message completion:nil];
                
                completion(YES);
                
            }else {
                // 更新数据库，上传语音失败
                tempModel.state = kMXMessageState_Failure;
                [[MXChatDBUtil sharedDataBase] updateMessageState:tempModel];
                completion(NO);
            }
        }else {
            // 更新数据库，上传语音失败
            tempModel.state = kMXMessageState_Failure;
            [[MXChatDBUtil sharedDataBase] updateMessageState:tempModel];
            completion(NO);
        }
        
    } failure:^(NSError *error) {
        // 更新数据库，上传语音失败
        tempModel.state = kMXMessageState_Failure;
        [[MXChatDBUtil sharedDataBase] updateMessageState:tempModel];
        completion(NO);
    }];
}

@end
