//
//  MXChatMessageHandler.h
//  MXChat
//
//  用处：发送消息时，将消息对象放到一个数组里，如果xmpp回调成功，则从数组中删除；
//  Created by aken on 16/4/25.
//  Copyright © 2016年 MXChat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageModel.h"

static const NSInteger kMXChatMessageHandlerTimeOutInterval = 3;

@class MessageModel;

typedef void(^ChatMessageHandler)(MessageModel *model);


@interface MXChatMessageHandler : NSObject

+ (MXChatMessageHandler*)sharedInstance;

// 设置超时时间（单位为秒）
@property (nonatomic) NSInteger timeoutInterval;

- (void)addMessage:(MessageModel*)msg;

- (void)removeMessage:(MessageModel*)msg;

-(void)loadMessages;

-(void)start;

@end

