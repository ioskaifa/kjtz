//
//  RoomManager.m
//  Lcwl
//
//  Created by mac on 2018/12/11.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "RoomManager.h"
#import "MXNet.h"
#import "ChatGroupModel.h"
#import "UserModel.h"
#import "MXRequest.h"
#import "LcwlChat.h"

@implementation RoomManager
+ (MXRequest *)getGroupList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/getGroupList"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{})
        .finish(^(id data){
            
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSDictionary *tempDic = (NSDictionary*)data;
                if ([tempDic[@"success"] boolValue]) {
                    NSArray* arr = [tempDic valueForKeyPath:@"data.groupList"];
                    NSMutableArray* groups = [[NSMutableArray alloc]initWithCapacity:50];
                    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        ChatGroupModel * model = [ChatGroupModel dictionaryToModel:obj];
                        [groups addObject:model];
                    }];
                    [[LcwlChat shareInstance].chatManager insertGroups:groups];
                    completion(groups,nil);
                    
                }
//            });
        }).failure(^(id error){
            completion(nil,error);
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)createGroup:(NSDictionary*) param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/createGroup"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                NSDictionary* dict = [tempDic valueForKeyPath:@"data.groupDetail"];
                ChatGroupModel * group = [ChatGroupModel dictionaryToModel:dict];
                UserModel* model = [LcwlChat shareInstance].user;
                group.creatorId = model.chatUser_id;
                group.nickname = model.user_name;
                group.roleType = 3;
                completion(group,nil);
                
            }else{
                 completion(nil,tempDic[@"msg"]);
                
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)addFriendInGroup:(NSDictionary *)param completion:(MXHttpRequestCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/addGroupUser"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                completion(YES,nil);
            } else {
                completion(NO,nil);
                [NotifyHelper showMessageWithMakeText:tempDic[@"msg"]];
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)removeMemberOutGroup:(NSDictionary *)param completion:(MXHttpRequestCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/removeGroupUser"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                completion(YES,nil);
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)deleteExitGroup:(NSDictionary *)param completion:(MXHttpRequestCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/deleteExitGroup"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                completion(YES,nil);
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
    return nil;
}
/*
 creDate = 20181219;
 creTime = 180005;
 createTime = 0;
 groupAvatar = "15818614416\U3001DD_01\U3001\U5927\U54e5";
 groupId = 181219749;
 groupName = "15818614416\U3001DD_01\U3001\U5927\U54e5";
 groupNickName = "DD_01";
 headPhoto = "http://ck-pay.com/static/front/images/logo.png";
 id = 826;
 roleType = 1;
 userId = 204;
 */
+ (MXRequest *)getGroupMembers:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/getGroupUserList"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                NSArray* array = [tempDic valueForKeyPath:@"data.groupUserList"];
                NSMutableArray* dataArray = [[NSMutableArray alloc] initWithCapacity:10];
                [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSDictionary* dict = (NSDictionary*)obj;
                    FriendModel * member = [FriendModel initMemberModel:dict];
                    [dataArray addObject:member];
                }];
                if (completion) {
                    completion(dataArray,nil);
                }
                
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)updateGroupName:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/updateGroupName"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                completion([param valueForKey:@"group_name"],nil);
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)updateGroupNickName:(NSDictionary*) param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/updateGroupNickName"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                completion(param[@"userName"],nil);
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
    return nil;
}

+ (MXRequest *)scanQrcode:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/scanGroupQrCode"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                NSDictionary* data = [tempDic valueForKeyPath:@"data.groupDetail"];
                ChatGroupModel* group = [ChatGroupModel dictionaryToModel:data];
                group.creatorId = data[@"userId"];
                NSInteger is_free = [data[@"free"] integerValue];
                completion(group, [NSString stringWithFormat:@"%@",@(is_free)]);
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
    return nil;
}

///设置群免打扰
+ (void)setGroupMessageFree:(NSDictionary*) param completion:(_Nullable MXHttpRequestCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/setGroupMessageFree"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                Block_Exec(completion, YES, nil);
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
            Block_Exec(completion, NO, error);
        })
        .execute();
    }];    
}
///设置群置顶
+ (void)setGroupMessageStick:(NSDictionary*) param completion:(_Nullable MXHttpRequestCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/group/setGroupMessageStick"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                Block_Exec(completion, YES, nil);
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
            Block_Exec(completion, NO, error);
        })
        .execute();
    }];
}
///设置好友免打扰
+ (void)setFriendMessageFree:(NSDictionary*) param completion:(_Nullable MXHttpRequestCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/frined/setFriendMessageFree"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                Block_Exec(completion, YES, nil);
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
            Block_Exec(completion, NO, error);
        })
        .execute();
    }];
}
///设置好友置顶
+ (void)setFriendMessageStick:(NSDictionary*) param completion:(_Nullable MXHttpRequestCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot2,@"api/chat/frined/setFriendMessageStick"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                Block_Exec(completion, YES, nil);
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:error];
            Block_Exec(completion, NO, error);
        })
        .execute();
    }];
}

@end
