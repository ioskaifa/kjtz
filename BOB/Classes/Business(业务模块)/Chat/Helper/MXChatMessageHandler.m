//
//  MXChatMessageHandler.m
//  MXChat
//
//  Created by aken on 16/4/25.
//  Copyright © 2016年 MXChat. All rights reserved.
//

#import "MXChatMessageHandler.h"
#import "MXChatDBUtil.h"
#import "ChatSendHelper.h"
@interface MXChatMessageHandler()

@property (strong, nonatomic) NSMutableArray* messageArray;

@property (strong, nonatomic) NSMutableArray* resendArray;

@property (strong, nonatomic) NSTimer* myTimer;
@end

@implementation MXChatMessageHandler

+ (MXChatMessageHandler*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (id)init {
    
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

-(NSMutableArray*)resendArray{
    if (_resendArray == nil) {
        _resendArray = [[NSMutableArray alloc]init];
    }
    return _resendArray;
}

-(NSMutableArray*)messageArray{
    if (_messageArray == nil) {
        _messageArray = [[NSMutableArray alloc]init];
    }
    return _messageArray;
}
#pragma mark - Public


- (void)addMessage:(MessageModel*)msg {
    if (msg.type == MessageTypeNormal || msg.type == MessageTypeGroupchat) {
        __block MessageModel* tmp = nil;
        [self.messageArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[MessageModel class]]) {
                MessageModel* model = (MessageModel* )obj;
                if ([model.messageId isEqualToString:msg.messageId]){
                    tmp = model;
                    *stop = YES;
                }
            }
        }];
        if (!tmp) {
            [[MXChatDBUtil sharedDataBase]insertUnSendMsgToDB:msg];
            [self.messageArray addObject:msg];
        }
    }
}

- (void)removeMessage:(MessageModel*)msg {
    if (msg) {
        if (msg.type == MessageTypeNormal || msg.type == MessageTypeGroupchat) {
            [[MXChatDBUtil sharedDataBase]deleteUnSendMsgByMessageId:msg.messageId];
            NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:self.messageArray];
            if (tmpArray.count > 0) {
                NSMutableArray *tmp = [[NSMutableArray alloc]initWithCapacity:10];
                [self.messageArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([obj isKindOfClass:[MessageModel class]]) {
                        MessageModel* model = (MessageModel* )obj;
                        if ([model.messageId isEqualToString:msg.messageId]) {
                            [tmp addObject:model];
                        }
                    }
                }];
                if (tmp.count>0) {
                    [tmp enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if (obj) {
                            [self.messageArray removeObject:obj];
                        }
                    }];
                }
            }
        }
    }
}

-(void)loadMessages{
    NSMutableArray* array = [[MXChatDBUtil sharedDataBase]loadUnSendMessagesFromDB];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MessageModel* model = (MessageModel*)obj;
        [[MXChatMessageHandler sharedInstance]removeMessage:model];
        model.state = kMXMessageState_Failure;
        model.is_acked = 0;
        model.is_listened = 0;
        [[MXChatDBUtil sharedDataBase]updateMessageFailureState:model];
    }];
    
}

-(void)start{
  self.myTimer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(sendMessages) userInfo:nil repeats:NO];
    [self.myTimer fire];
}

-(void)sendMessages{
    [self.messageArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj && [obj isKindOfClass:[MessageModel class]]) {
            MessageModel* message = (MessageModel*)obj;
            [ChatSendHelper reSendMessage:message delegate:nil];
        }
    }];
    
    [self.messageArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MessageModel* model = (MessageModel*)obj;
        model.state = kMXMessageState_Failure;
        model.is_acked = 0;
        model.is_listened = 0;
        [[MXChatDBUtil sharedDataBase]updateMessageFailureState:model];
    }];
    
    [self.messageArray removeAllObjects];
}
@end
