//
//  AudioVibrateManager.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/4/4.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "AudioVibrateManager.h"
#import <AudioToolbox/AudioToolbox.h>
@interface AudioVibrateManager()

@property (nonatomic, strong) NSDate  *lastPlaySoundDate;

@end

static const CGFloat kDefaultPlaySoundInterval = 1.0;

@implementation AudioVibrateManager

+(void)playAudio:(NSString*) resource{
    SystemSoundID soundID;
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:resource ofType:nil];
    OSStatus error = AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:resourcePath ],&soundID);
    if (error == kAudioServicesNoError) {
        
    }else {
        MLog(@"Failed to create sound ");
    }
    AudioServicesPlaySystemSound(soundID);
}
+(void)playVibrate{
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

///开始持续震动
+(void)playContinuedVibrate {
    //注册震动回调
    AudioServicesAddSystemSoundCompletion(kSystemSoundID_Vibrate, NULL, NULL, vibrationCompleteCallback, NULL);
    //开始震动
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}
///停止持续震动
+(void)stopPlayContinuedVibrate {
    stopContinuedVibration();
}

///震动完成回调，时间长短自己控制
void vibrationCompleteCallback(SystemSoundID sound,void * clientData) {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(800 * NSEC_PER_MSEC)), dispatch_get_global_queue(0, 0), ^{
        AudioServicesPlaySystemSound(sound);
    });
}

///停止震动
void stopContinuedVibration() {
    AudioServicesRemoveSystemSoundCompletion(kSystemSoundID_Vibrate);
    AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate);
}

+(void)playAudioAndVibrate{
    [self playAudio:@"ms_alert.wav"];
    [self playVibrate];
}

- (void)playSound {
    NSDate *nowTime = [NSDate date];
    NSTimeInterval timeInterval = [nowTime timeIntervalSinceDate:self.lastPlaySoundDate];
    
    self.lastPlaySoundDate = [NSDate date];
    //NSLog(@"### ----- %@",self.lastPlaySoundDate);
    if (timeInterval < kDefaultPlaySoundInterval) {
        //接收很多信息的时候 不会一直响
        return;
    }
    [self.class playAudio:@"ms_alert.wav"];
}

- (void)playAudioAndVibrate {
    NSDate *nowTime = [NSDate date];
    NSTimeInterval timeInterval = [nowTime timeIntervalSinceDate:self.lastPlaySoundDate];
    
    self.lastPlaySoundDate = [NSDate date];
    //NSLog(@"### ----- %@",self.lastPlaySoundDate);
    if (timeInterval < kDefaultPlaySoundInterval) {
        //接收很多信息的时候 不会一直响
        return;
    }
    [self.class playAudioAndVibrate];
}

- (void)playVibrate {
    NSDate *nowTime = [NSDate date];
    NSTimeInterval timeInterval = [nowTime timeIntervalSinceDate:self.lastPlaySoundDate];
    
    self.lastPlaySoundDate = [NSDate date];
    //NSLog(@"### ----- %@",self.lastPlaySoundDate);
    if (timeInterval < kDefaultPlaySoundInterval) {
        //接收很多信息的时候 不会一直响
        return;
    }
    [self.class playVibrate];
}

+ (instancetype)shareInstance {
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}


@end
