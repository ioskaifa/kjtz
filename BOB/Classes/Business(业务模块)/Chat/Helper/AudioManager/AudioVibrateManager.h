//
//  AudioVibrateManager.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/4/4.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AudioVibrateManager : NSObject

+(void)playAudio:(NSString*) resource;
///单次震动
+(void)playVibrate;
///持续震动
+(void)playContinuedVibrate;
///停止持续震动
+(void)stopPlayContinuedVibrate;

+(void)playAudioAndVibrate;

- (void)playSound;

- (void)playAudioAndVibrate;

- (void)playVibrate;

+ (instancetype)shareInstance;

@end
