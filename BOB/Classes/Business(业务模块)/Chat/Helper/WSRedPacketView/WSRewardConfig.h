//
//  WSRewardConfig.h
//  RedPacketViewDemo
//
//  Created by tank on 2017/12/19.
//  Copyright © 2017年 tank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MessageModel.h"
typedef NS_ENUM(NSInteger, RedPacketStatus) {
    RedPacketStatusUnValidate = 4,            //失效
    RedPacketStatusReceiveOver = 3,           //领完
    RedPacketStatusReceived = 2,              //已领取
    RedPacketStatusException = 0,             //异常
    RedPacketStatusValidate  = 1              //待领取
};

@interface WSRewardConfig : NSObject

@property (nonatomic, assign) float     money;

@property (nonatomic, strong) NSString   *avatarImage;

@property (nonatomic, copy  ) NSString  *content;

@property (nonatomic, copy  ) NSString  *userName;

@property (nonatomic, copy  ) NSString  *redPacketId;

@property (nonatomic, copy  ) MessageModel* model;

@property (nonatomic) RedPacketStatus status;


@end
