//
//  ChatVoiceManage.h
//  BOB
//  语音聊天相关的管理类
//  Created by mac on 2020/7/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AskCallVC.h"
#import "ChatAVCallVC.h"

#define g_window [UIApplication sharedApplication].keyWindow
#define g_subWindow g_App.subWindow
#define g_App ((AppDelegate*)[[UIApplication sharedApplication] delegate])  //appDelegate单例

///语音聊天相关消息通知
#define kMXVoiceMsgNotifaction @"voiceMsgNotifaction"

NS_ASSUME_NONNULL_BEGIN

@interface ChatVoiceManage : NSObject

///是否语音聊天中
@property (nonatomic, assign) BOOL isMeeting;
///为了更新语音通话
///roomID -> messageID
@property (nonatomic, strong) NSMutableDictionary *msgMDic;
///是否本人发起的语音通话
@property (nonatomic, assign) BOOL isMy;
/// 聊天房间ID
@property (nonatomic, copy) NSString *roomID;
///语音发起人ID
@property (nonatomic, copy) NSString *fromID;
/// 0-单聊 1-群聊
@property (nonatomic, assign) EMConversationType chatType;

@property (nullable, nonatomic, strong) AskCallVC *askVC;

@property (nullable, nonatomic, strong) ChatAVCallVC *chatVC;
///我已进入房间
@property (nonatomic, assign) BOOL isSelfEnterRoom;
///对方已进入房间
@property (nonatomic, assign) BOOL isOtherEnterRoom;

+ (instancetype)shareInstance;

- (void)didReceiveMessage:(MessageModel *)message;
/// 接受语音聊天 进入聊天房间
- (void)acceptEnterChatRoom;
///语音邀请界面
- (void)askCallRoom:(AskCallType)askType
           toUserId:(NSString *)userId
           chatType:(EMConversationType)type;

- (MessageModel *)formatMsg:(MessageModel *)msg;

/*--------语音消息------*/
///发起语音询问
+ (void)sendAsk:(NSString *)toUserId chatType:(EMConversationType)chatType;
///接受语音聊天
+ (void)sendAccept;
///语音提示页 挂断语音聊天
+ (void)sendCancel;
///语音聊天页面 正常挂断语音聊天
+ (void)sendEndWithTime:(NSString *)time;
///语音聊天超时
+ (void)sendOverTime;
///进入聊天室
+ (void)enterMeetingRoom;

@end

NS_ASSUME_NONNULL_END
