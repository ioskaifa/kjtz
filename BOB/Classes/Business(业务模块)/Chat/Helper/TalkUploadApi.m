//
//  LoginApi.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/2/6.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "TalkUploadApi.h"

@interface TalkUploadApi()


@end
@implementation TalkUploadApi

@synthesize paramDictionary;
- (id)initWithParameter:(NSDictionary*)dictionary{
    self = [super init];
    if (self) {
        self.paramDictionary = [dictionary mutableCopy];;
        
    }
    return self;
}
- (NSString *)requestUrl {
    
    return @"" ;
}
- (MXRequestMethod)requestMethod {
    
    return MXRequestMethodPost;
}

- (id)requestArgument {
    return self.paramDictionary;
}

- (MXRequestSerializerType)requestSerializerType {
    
    return MXRequestSerializerTypeJSON;
}


@end
