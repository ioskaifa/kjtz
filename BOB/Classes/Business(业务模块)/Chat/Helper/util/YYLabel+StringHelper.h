//
//  YYLabel+StringHelper.h
//  MoPal_Developer
//
//  Created by wanggang on 17/3/21.
//  Copyright © 2017年 MoXian. All rights reserved.
//

#import <YYKit/YYLabel.h>
@interface YYLabel (StringHelper)

/*!
 @method MXupdateOuterTextProperties
 @brief 将YYText内部的文本转化为attribute属性
 @result  计算yytext高度和文本渲染
 */
- (void)MXupdateOuterTextProperties;

@end
