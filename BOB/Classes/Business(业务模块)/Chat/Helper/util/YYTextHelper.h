//
//  YYTextHelper.h
//  MoPal_Developer
//
//  Created by 王 刚 on 16/9/20.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <YYKit/YYKit.h>
#import "YYLabel+StringHelper.m"
static NSInteger fixedLineHeight = 22;

@interface YYTextHelper : NSObject

//部分字体和颜色的属性记得修改
+ (YYLabel *)yyLabel;

//部分字体和颜色的属性记得修改
+ (YYTextView *)yyTextView;

// 计算高度
+ (CGSize)calculateHeight:(NSInteger)width font:(UIFont *)font text:(NSString *)text;

+ (CGSize)calculateHeightEx:(NSInteger)width yyLabel:(YYLabel *)yyLbl;

@end

//add by xgh
@interface YYLabel (CustomLinks)

@property (nonatomic, strong) NSMutableArray *customLinks;

- (NSURL *)getUrlForRange:(NSRange)range;

- (void)addCustomLink:(NSURL *)linkUrl inRange:(NSRange)range textColor:(UIColor *)textColor;

- (void)removeAllCustomLinks;

- (void)parseUrl;
@end
