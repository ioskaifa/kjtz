//
//  YYTextHelper.m
//  MoPal_Developer
//
//  Created by 王 刚 on 16/9/20.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "YYTextHelper.h"
#import "MXEmotionManager.h"
#import "NSAttributedString+YYText.h"
#import "TalkManager.h"
#import <objc/runtime.h>
#import "MessageModel.h"
@implementation YYTextHelper

+ (YYLabel *)yyLabel{
    
    YYLabel *_textviewContent = [YYLabel new];
    _textviewContent.textVerticalAlignment = YYTextVerticalAlignmentTop;
    _textviewContent.userInteractionEnabled = NO;
    _textviewContent.backgroundColor = [UIColor clearColor];
    YYTextLinePositionSimpleModifier *mod = [YYTextLinePositionSimpleModifier new];
    mod.fixedLineHeight = fixedLineHeight;
    _textviewContent.font = [UIFont font14];
    _textviewContent.numberOfLines = 0;
    _textviewContent.textColor = [UIColor grayColor];
    _textviewContent.linePositionModifier = mod;
    YYTextSimpleEmoticonParser *parser = [YYTextSimpleEmoticonParser new];
    parser.emoticonMapper = [[TalkManager manager] emotionMapper];
    _textviewContent.textParser = parser;
    [_textviewContent sizeToFit];
    
    return _textviewContent;
}

+ (YYTextView *)yyTextView{
    YYTextView *textView = [YYTextView new];
    YYTextSimpleEmoticonParser *parser = [YYTextSimpleEmoticonParser new];
    parser.emoticonMapper =  [[TalkManager manager] emotionMapper];;
    textView.textParser = parser;
    textView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    return textView;
}


+ (CGSize)calculateHeight:(NSInteger)width font:(UIFont *)font text:(NSString *)text {
    
    YYLabel *customLabel = [self.class yyLabel];
    customLabel.font = font;
    customLabel.text = text;
    customLabel.numberOfLines = 0;
    customLabel.lineBreakMode = NSLineBreakByWordWrapping;
    customLabel.preferredMaxLayoutWidth = width;//SCREEN_WIDTH-80;
    [customLabel MXupdateOuterTextProperties];
    CGSize introSize = CGSizeMake(width, CGFLOAT_MAX);
    YYTextLayout *layout = [YYTextLayout layoutWithContainerSize:introSize text:customLabel.attributedText];

    return layout.textBoundingSize;
}

+ (CGSize)calculateHeightEx:(NSInteger)width yyLabel:(YYLabel *)yyLbl {
    YYTextContainer *container = [YYTextContainer containerWithSize:CGSizeMake(width, MAXFLOAT)];
    container.maximumNumberOfRows = yyLbl.numberOfLines; //add by xgh
    YYTextLayout *textLayout = [YYTextLayout layoutWithContainer:container text:yyLbl.attributedText];    
    return textLayout.textBoundingSize;
}

@end

#pragma mark - YYLabel Link Extension

//add by xgh
#define REGULAREXPRESSION_OPTION(regularExpression,regex,option) \
\
static inline NSRegularExpression * k##regularExpression() { \
static NSRegularExpression *_##regularExpression = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_##regularExpression = [[NSRegularExpression alloc] initWithPattern:(regex) options:(option) error:nil];\
});\
\
return _##regularExpression;\
}\

#define REGULAREXPRESSION(regularExpression,regex) REGULAREXPRESSION_OPTION(regularExpression,regex,NSRegularExpressionCaseInsensitive)

REGULAREXPRESSION(URLRegularExpression,@"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(((http[s]{0,1}|ftp)://|)((?:(?:25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)))\\.){3}(?:25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d))))(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)")

@implementation YYLabel (CustomLinks)

- (void)addCustomLink:(NSURL *)linkUrl inRange:(NSRange)range textColor:(UIColor *)textColor {
    if (!self.customLinks) {
        self.customLinks = [NSMutableArray arrayWithCapacity:1];
    }
    NSTextCheckingResult *link = [NSTextCheckingResult linkCheckingResultWithRange:range URL:linkUrl];
    [self.customLinks addObject:link];
    
    [(NSMutableAttributedString *)self.attributedText setTextHighlightRange:range
                                                                         color:textColor
                                                               backgroundColor:[UIColor clearColor]
                                                                     tapAction:nil];
}

/**
 由于魔线表情不同于emoji，文本最终会被处理转换，转换后的range也会改变，因此无法直接根据range添加链接，
 所以这里引入location为-1,来表示除了显式设置链接之外的其他地方
*/
- (NSURL *)getUrlForRange:(NSRange)range {
    __block NSURL *url = nil;
    [self.customLinks enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSTextCheckingResult *link = (NSTextCheckingResult *)obj;
        if((range.location == -1 && link.range.location == -1) || NSLocationInRange(range.location, link.range)) {
            url = link.URL;
            *stop = YES;
        }
    }];
    return url;
}

- (void)parseUrl {
    NSRegularExpression *reg = kURLRegularExpression();
    [reg enumerateMatchesInString:self.attributedText.string options:0 range:NSMakeRange(0, self.attributedText.string.length) usingBlock:^(NSTextCheckingResult *result, __unused NSMatchingFlags flags, __unused BOOL *stop) {
        NSString *actionString = [NSString stringWithFormat:@"%@",[self.attributedText.string substringWithRange:result.range]];
        [self addCustomLink:[NSURL URLWithString:actionString] inRange:result.range textColor:[UIColor moPurple]];
    }];
}

- (void)removeAllCustomLinks {
    objc_removeAssociatedObjects(self);
}

- (void)setCustomLinks:(NSMutableArray *)customLinks {
    objc_setAssociatedObject(self, @selector(customLinks), customLinks, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSMutableArray *)customLinks {
    return objc_getAssociatedObject(self, _cmd);
}

@end
