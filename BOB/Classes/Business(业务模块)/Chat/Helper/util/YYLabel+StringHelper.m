//
//  YYLabel+StringHelper.m
//  MoPal_Developer
//
//  Created by wanggang on 17/3/21.
//  Copyright © 2017年 MoXian. All rights reserved.
//

#import "YYLabel+StringHelper.h"

@implementation YYLabel (StringHelper)

- (void)MXupdateOuterTextProperties{
    NSString* str1 = @"_";
    NSString* str2 = @"OuterTextProperties";
    
    NSString* selectorString = [NSString stringWithFormat:@"%@update%@",str1,str2];
    if([self respondsToSelector:NSSelectorFromString(selectorString)]){
        SuppressPerformSelectorLeakWarning([self performSelector:NSSelectorFromString(selectorString)]);
    }
}

@end
