//
//  MXChatHeaders.h
//  Moxian
//
// @abstract 引入SDK的所有需要的头文件
//  Created by litiankun on 14/12/3.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#ifndef Moxian_MXChatHeaders_h
#define Moxian_MXChatHeaders_h


// managers
#import "IMXChatManager.h"
#import "IMXChatManagerChat.h"
#import "IMXDeviceManager.h"
#import "MXBaseRequestApi.h"
typedef NS_ENUM(NSInteger, ConnectState) {
    Success = 1,
    NetError,
    ServerError
} ;
#endif
