//
//  RoomManager.h
//  Lcwl
//
//  Created by mac on 2018/12/11.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXRequest.h"
NS_ASSUME_NONNULL_BEGIN

@interface RoomManager : NSObject
// 查询用户已加的群列表
+ (MXRequest *)getGroupList:(NSDictionary*) param completion:(MXHttpRequestListCallBack)completion;
// 创建群
+ (MXRequest *)createGroup:(NSDictionary*) param completion:(MXHttpRequestObjectCallBack)completion;
// 添加成员 inviteCode
+ (MXRequest *)addFriendInGroup:(NSDictionary *)param completion:(MXHttpRequestCallBack)completion;
// 删除成员
+ (MXRequest *)removeMemberOutGroup:(NSDictionary *)param completion:(MXHttpRequestCallBack)completion;
// 退出群
+ (MXRequest *)deleteExitGroup:(NSDictionary *)param completion:(MXHttpRequestCallBack)completion;
// 拉取群成员
+ (MXRequest *)getGroupMembers:(NSDictionary*) param completion:(MXHttpRequestObjectCallBack)completion;
// 修改群名称
+ (MXRequest *)updateGroupName:(NSDictionary*) param completion:(MXHttpRequestObjectCallBack)completion;
//修改群昵称
+ (MXRequest *)updateGroupNickName:(NSDictionary*) param completion:(MXHttpRequestObjectCallBack)completion;
//二维码扫描显示群相关信息详情接口
+ (MXRequest *)scanQrcode:(NSDictionary*) param completion:(MXHttpRequestObjectCallBack)completion;
///设置群免打扰
+ (void)setGroupMessageFree:(NSDictionary*) param completion:(_Nullable MXHttpRequestCallBack)completion;
///设置群置顶
+ (void)setGroupMessageStick:(NSDictionary*) param completion:(_Nullable MXHttpRequestCallBack)completion;
///设置好友免打扰
+ (void)setFriendMessageFree:(NSDictionary*) param completion:(_Nullable MXHttpRequestCallBack)completion;
///设置好友置顶
+ (void)setFriendMessageStick:(NSDictionary*) param completion:(_Nullable MXHttpRequestCallBack)completion;

@end

NS_ASSUME_NONNULL_END
