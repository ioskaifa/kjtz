//
//  TalkManager.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/3/31.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXRequest.h"
#import "FriendModel.h"
#import "MXChatDefines.h"
#import "MessageModel.h"
@class ChatGroupModel;
@interface TalkManager : NSObject



+ (TalkManager*)manager;

//上传群头像 用户头像
- (void)uploadImageFile:(NSDictionary*)param completion:(MXHttpRequestListCallBack)completion setProgress:(void (^)(float degree,NSString* msgId))progressBlock;

//下载语音文件
+ (MXRequest*)downloadAudioFileByMessage:(MessageModel*)msg completion:(MXHttpRequestCallBack)completion;
///下载文件  attr1 文件下载链接MD5 attr2 文件类型
+ (MXRequest*)downloadFile:(MessageModel*)msg completion:(MXHttpRequestCallBack)completion;

//消息id
+(NSString* )msgId;

//删除聊天输入框的文本
+(NSString*)deleteInputText:(NSString*)text ;

//是否超过置顶数量的限制
+(BOOL)isOverTopNums;

//销毁对话 第一次提示页
+(void)showFirstDestroyChatTip:(UIViewController*)vc destroyRect:(CGRect)rect;

//得到外层聊天列表的时间格式
+(NSString*)timeDataString:(NSString*)time;

//解析魔友数据组装成有索引的数组
+(NSMutableArray*)getIndexesArray:(NSArray*)array;

//群组的索引
+(NSMutableArray*)getGroupIndexesArray:(NSArray*)datasource;

//查找消息的索引
+(NSInteger)searchMessage:(NSMutableArray*) messages withModel:(id)message;

//红点显示数量
+ (NSString*)cellBageNumberString:(NSInteger)num ;

//列表滑动选项
+(NSArray*)getListOperationByType:(EMConversationType) type top:(BOOL)isTop;

//列表滑动选项颜色
+(NSArray*)getListColorByType:(EMConversationType) type;

//排序外层的conversations
+(NSMutableArray*)sortConversation:(NSArray*)conversations;

//排序店铺的conversations
+(NSArray*)sortShopDataSource:(NSMutableArray*)array;


+(NSString*)getFocusHead:(MessageModel*) model;


+(NSArray*)friendSegs;

+(NSString*)showNotificationContent:(MessageModel*)msg;

+(BOOL)isAvoidDisturbTime;

+(NSString*)cellIdentifierForMessageModel:(MessageModel*)msg;

//清空没有聊天记录的conversation
+(void)removeEmptyConversation;

//缓存图片并压缩 返回文件的路径
+(NSString*)saveChatImage:(UIImage *)image;

//提供解析富文本的mapping
- (NSDictionary *)emotionMapper;

+(FriendModel*)newFriend;

+(FriendModel*)hudongtongzhi;

+(void)pushChatViewUserId:(NSString *)userId type:(EMConversationType)type;

@end
