//
//  MXDocument.m
//  BOB
//
//  Created by mac on 2020/7/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MXDocument.h"

@implementation MXDocument

- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError * _Nullable __autoreleasing *)outError {
    self.data = contents;    
    return YES;
}

- (NSString *)fileTypeEx {
    return [self.locFileUrl pathExtension];
}

@end
