//
//  IClouManage.h
//  BOB
//
//  Created by mac on 2020/7/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXDocument.h"

typedef void(^DocumentPickerComplete)(MXDocument *_Nullable);

NS_ASSUME_NONNULL_BEGIN

@interface IClouManage : NSObject

@property (nonatomic, copy) DocumentPickerComplete complete;

+ (instancetype)shareInstance;
///判断iCloud是否可用
+ (BOOL)iCloudEnable;

- (void)presentDocumentPickerInSuperVC:(UIViewController *)superVC complete:(DocumentPickerComplete)complete;

@end

NS_ASSUME_NONNULL_END
