//
//  IClouManage.m
//  BOB
//
//  Created by mac on 2020/7/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "IClouManage.h"

@interface IClouManage ()<UIDocumentPickerDelegate>

@end

@implementation IClouManage

+ (instancetype)shareInstance {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });    
    return instance;
}

+ (BOOL)iCloudEnable {
    NSFileManager *manager = [NSFileManager defaultManager];
    NSURL *url = [manager URLForUbiquityContainerIdentifier:nil];
    if (url != nil) {
        return YES;
    }
    
    return NO;
}

- (void)presentDocumentPickerInSuperVC:(UIViewController *)superVC complete:(DocumentPickerComplete)complete {
    self.complete = complete;
    NSArray *documentTypes = @[@"public.content",
                               @"public.zip-archive",
                               @"public.executable",
                               @"public.text",
                               @"public.source-code ",
                               @"public.image",
                               @"public.audiovisual-content",
                               @"com.adobe.pdf",
                               @"com.apple.keynote.key",
                               @"com.microsoft.word.doc",
                               @"com.microsoft.excel.xls",
                               @"com.microsoft.powerpoint.ppt"];
       UIDocumentPickerViewController *documentPickerVC = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes
                                                                                                                 inMode:UIDocumentPickerModeOpen];
       documentPickerVC.delegate = self;
       [superVC presentViewController:documentPickerVC animated:YES completion:nil];
}

+ (void)downloadWithDocumentURL:(NSURL*)url complete:(void(^)(MXDocument *data))complete {
    MXDocument *iCloudDoc = [[MXDocument alloc] initWithFileURL:url];
    [iCloudDoc openWithCompletionHandler:^(BOOL success) {
        if (success) {
            [iCloudDoc closeWithCompletionHandler:^(BOOL success) {
                MLog(@"iCloudDoc ---- 关闭成功");
            }];
            if (complete) {
                complete(iCloudDoc);
            }
        }
    }];
}

#pragma mark - UIDocumentPickerDelegate
- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
    
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    NSArray *array = [[url absoluteString] componentsSeparatedByString:@"/"];
    NSString *fileName = [array lastObject];
    fileName = [fileName stringByRemovingPercentEncoding];
//    if (![self.class iCloudEnable]) {
//        [NotifyHelper showMessageWithMakeText:@"iCloud不可用,请检查"];
//        return;
//    }
    ///下载到本地
    [[self class] downloadWithDocumentURL:url complete:^(MXDocument *data) {
        if (![data isKindOfClass:MXDocument.class]) {
            if (self.complete) {
                self.complete(nil);
            }
            return;
        }
        if (self.complete) {
            self.complete(data);
        }
    }];
}

@end
