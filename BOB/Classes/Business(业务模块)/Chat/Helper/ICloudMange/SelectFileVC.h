//
//  SelectFileVC.h
//  BOB
//
//  Created by mac on 2020/8/3.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

typedef void(^SelectFileBlock)(NSArray * _Nonnull files);

typedef void(^SelectICloudBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface SelectFileVC : BaseViewController

@property (nonatomic, copy) SelectFileBlock selectFileBlock;

@property (nonatomic, copy) SelectICloudBlock selectICloudBlock;

@end

NS_ASSUME_NONNULL_END
