//
//  FilePreviewVC.m
//  BOB
//
//  Created by mac on 2020/7/29.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FilePreviewVC.h"
#import <WebKit/WebKit.h>
#import "ChatCacheFileUtil.h"
#import "TalkManager.h"

@interface FilePreviewVC () <WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webView;

@end

@implementation FilePreviewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    if (self.meaage) {
        [self loadFileByMessage:self.meaage];
    } else {
        [self setNavBarTitle:@"文件预览"];
    }
}

- (void)loadFile:(NSString *)filePath {
    if ([StringUtil isEmpty:filePath]) {
        return;
    }
    
    if (![ChatCacheFileUtil fileExistsAtPath:filePath]) {
        ///本地文件不存在
        return;
    }
    NSURL *url = [NSURL fileURLWithPath:filePath];
    [self.webView loadFileURL:url allowingReadAccessToURL:url];
}

- (void)loadFileByMessage:(MessageModel *)model {
    if (![model isKindOfClass:MessageModel.class]) {
        return;
    }
    if (model.subtype != kMXMessageTypeFile) {
        return;
    }
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:model.body];
    if (bodyDic[@"attr4"]) {
        [self setNavBarTitle:[bodyDic[@"attr4"] description]];
    } else {
        [self setNavBarTitle:@"文件预览"];
    }
    NSString *rootPath = [[ChatCacheFileUtil sharedInstance] userDocPath];
    NSString *path = StrF(@"%@.%@", bodyDic[@"attr1"], bodyDic[@"attr2"]);
    NSString *localUrl = StrF(@"%@/%@", rootPath, path);
    if ([ChatCacheFileUtil fileExistsAtPath:localUrl]) {
        ///如果存在则直接读取缓存
        [NotifyHelper showHUDAddedTo:self.view animated:YES];
        [self loadFile:localUrl];
    } else {
        ///先下载 再本地预览
        [NotifyHelper showHUDAddedTo:self.view animated:YES];
        @weakify(self)
        [TalkManager downloadFile:model
                       completion:^(BOOL success, NSString *error) {
            @strongify(self)
            Block_Exec_Main_Async_Safe(^{
                if (success) {
                    [self loadFile:error];
                } else {
                    [NotifyHelper hideHUDForView:self.view animated:YES];
                }
            });
        }];
    }
}

#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [NotifyHelper hideHUDForView:self.view animated:YES];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [NotifyHelper hideHUDForView:self.view animated:YES];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString *_webUrlStr = navigationAction.request.URL.absoluteString;
    NSString *lastName =[[_webUrlStr lastPathComponent] lowercaseString];
    
    if ([lastName containsString:@".txt"]) {
        NSData *data = [NSData dataWithContentsOfURL:navigationAction.request.URL];        
        [webView loadData:data MIMEType:@"text/html" characterEncodingName:@"GBK" baseURL:nil];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)createUI {
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (WKWebView *)webView {
    if (!_webView) {
        _webView = [WKWebView new];
        _webView.navigationDelegate = self;
    }
    return _webView;
}

@end
