//
//  SelectFileVC.m
//  BOB
//
//  Created by mac on 2020/8/3.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SelectFileVC.h"
#import "SelectFileCell.h"
#import "MXChatDBUtil.h"

@interface SelectFileVC () <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UISegmentedControl *segmentedControl;

@property (nonatomic,strong)  UITableView    *tableView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@property (nonatomic, strong) MyBaseLayout *bottomLayout;

@property (nonatomic, strong) UIButton *commitBtn;

@end

@implementation SelectFileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)navBarRightBtnAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)configCurrentVC {
    
}

- (void)fetchData {
    NSArray *fileArr = [[MXChatDBUtil sharedDataBase] selectAllFileChatHistory];
    self.dataSourceArr = fileArr;
    [self.tableView reloadData];
}

- (NSArray *)selectFileMessage {
    if (self.dataSourceArr.count == 0) {
        return @[];
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:StrF(@"isSelected == YES")];
    NSArray *tempArr = [self.dataSourceArr filteredArrayUsingPredicate:predicate];
    return tempArr;
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = SelectFileCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    
    if (![cell isKindOfClass:SelectFileCell.class]) {
        return;
    }
    MessageModel *obj = self.dataSourceArr[indexPath.row];
    SelectFileCell *listCell = (SelectFileCell *)cell;
    [listCell configureView:obj];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return 0.01;
    }
    return [SelectFileCell viewHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    MessageModel *obj = self.dataSourceArr[indexPath.row];
    if (![obj isKindOfClass:MessageModel.class]) {
        return;
    }
    obj.isSelected = !obj.isSelected;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    NSArray *selectArr = [self selectFileMessage];
    [self.commitBtn setTitle:StrF(@"确定(%ld)", selectArr.count) forState:UIControlStateNormal];
    [self.commitBtn sizeToFit];
    self.commitBtn.mySize = self.commitBtn.size;
}

#pragma mark - IBAction
-(void)indexDidChangeForSegmentedControl:(UISegmentedControl *)sender {
    if (sender == self.segmentedControl) {
        NSInteger selecIndex = sender.selectedSegmentIndex;
        if (selecIndex == 1) {
            [self dismissViewControllerAnimated:YES completion:^{
                if (self.selectICloudBlock) {
                    self.selectICloudBlock();
                }
            }];
        }
    }
}

#pragma mark - InitUI
- (void)createUI {
    self.navigationItem.titleView = self.segmentedControl;
    [self setNavBarRightBtnWithTitle:@"取消" andImageName:nil];
    [self.view addSubview:self.tableView];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    
    [layout addSubview:self.tableView];
    [layout addSubview:self.bottomLayout];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        Class cls = SelectFileCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.tableFooterView = [UIView new];
        _tableView.weight = 1;
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.myBottom = 0;
    }
    return _tableView;
}

- (UISegmentedControl *)segmentedControl {
    if (!_segmentedControl) {
        NSArray *titleArr = [NSArray arrayWithObjects:@"聊天中文件",@"iCloud",nil];
        _segmentedControl = [[UISegmentedControl alloc]initWithItems:titleArr];
        _segmentedControl.frame = CGRectMake(0, 0, 180, 30);
        _segmentedControl.selectedSegmentIndex = 0;
        _segmentedControl.tintColor = [UIColor moBlack];
        [_segmentedControl addTarget:self action:@selector(indexDidChangeForSegmentedControl:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentedControl;
}

- (MyBaseLayout *)bottomLayout {
    if (!_bottomLayout) {
        _bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
        _bottomLayout.backgroundColor = [UIColor moBackground];
        _bottomLayout.myHeight = 60;
        _bottomLayout.myTop = 0;
        _bottomLayout.myLeft = 0;
        _bottomLayout.myRight = 0;
        
        UIView *view = [UIView new];
        view.weight = 1;
        view.myCenterY = 0;
        view.myHeight = 1;
        [_bottomLayout addSubview:view];
        [_bottomLayout addSubview:self.commitBtn];
    }
    return _bottomLayout;
}

- (UIButton *)commitBtn {
    if (!_commitBtn) {
        _commitBtn = [UIButton new];
        _commitBtn.myTop = 5;
        _commitBtn.myRight = 20;
        [_commitBtn setTitle:@"确定(0)" forState:UIControlStateNormal];
        [_commitBtn setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
        [_commitBtn sizeToFit];
        _commitBtn.mySize = _commitBtn.size;
        @weakify(self)
        [_commitBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self dismissViewControllerAnimated:YES completion:^{
                if (self.selectFileBlock) {
                    self.selectFileBlock([self selectFileMessage]);
                }
            }];
        }];
    }
    return _commitBtn;
}

@end
