//
//  MXDocument.h
//  BOB
//
//  Created by mac on 2020/7/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MXDocument : UIDocument

@property (nonatomic, strong) NSData *data;

@property (nonatomic, copy) NSString *locFileUrl;

@property (nonatomic, copy) NSString *fileTypeEx;

@end

NS_ASSUME_NONNULL_END
