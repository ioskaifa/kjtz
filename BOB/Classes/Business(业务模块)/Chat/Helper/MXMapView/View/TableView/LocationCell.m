//
//  LocationCell.m
//  MapDemo
//
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "LocationCell.h"

@implementation LocationCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self) {
        
        _selectedImageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_check"]];
        
        _selectedImageView.frame=CGRectMake([UIScreen mainScreen].bounds.size.width - 35,
                                            self.frame.size.height/2 - 16/2,
                                            16, 16);
        _selectedImageView.hidden=YES;
        [self.contentView addSubview:_selectedImageView];
        
    }
    
    
    return self;
}

@end
