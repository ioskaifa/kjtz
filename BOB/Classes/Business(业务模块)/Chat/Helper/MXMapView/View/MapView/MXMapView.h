//
//  MXMapView.h
//  MapDemo
//
//  地图管理
//  Created by aken on 15/4/21.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXMapConfig.h"

@class MXBaseMapView;
@class RegionAnnotation;

@protocol MXMapViewDelegate;

@interface MXMapView : UIView

// 地图对象 
@property (nonatomic, weak) MXBaseMapView *mxBaseMapView;

// 地图初始化类型，默认是apple map
@property (nonatomic, assign) MXMapViewType mapViewType;

// 经纬度
@property (nonatomic) CLLocationCoordinate2D coordinate2D;

// 代理
@property (nonatomic, weak) id<MXMapViewDelegate>delegate;

/**
 *  根据当前位置初始化地图
 *
 *  @param frame     位置
 *  @param location  经纬度
 *  @param mapViewType 地图类型
 *
 *  @return  地图对象
 */
- (id)initWithFrame:(CGRect)frame withLocation:(CLLocationCoordinate2D)location mapViewType:(MXMapViewType)mapViewType;

/**
 *  根据经纬定位到当前视窗
 *
 *  @param centerCoor 经纬度
 */
- (void)animateToCameraPosition:(CLLocationCoordinate2D)centerCoor;

/**
 *  根据经纬度添加单个大头针
 *
 *  @param regionAnnotatioin //  地理位置消息对象，注解
 */
- (void)addAnnotationView:(RegionAnnotation*)regionAnnotatioin;

// 设置是否触摸了地图
- (void)setMapPanned:(BOOL)mapPanned;

@end


@protocol MXMapViewDelegate <NSObject>

@optional

// 点击导航按钮
- (void)mxmapViewDidPressNavigation:(CLLocationCoordinate2D)coordinate;

/**
 *  地图完成定位后调用
 *
 *  @param mapView    地图View
 *  @param coordinate 经纬度
 */
- (void)mapView:(UIView *)mapView didFinishUpdateUserLocation:(CLLocationCoordinate2D)coordinate;

/**
 * 地图区域改变完成后会调用此接口
 * @param coordinate2D 移动后的坐标
 * @param animated 是否动画
 */
- (void)mapViewCoordinate2D:(CLLocationCoordinate2D)coordinate2D regionDidChangeAnimated:(BOOL)animated;


@end
