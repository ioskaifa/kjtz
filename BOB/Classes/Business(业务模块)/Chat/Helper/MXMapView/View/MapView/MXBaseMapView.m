//
//  MXBaseMapView.m
//  MapDemo
//
//  Created by aken on 15/4/21.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "MXBaseMapView.h"
#import "RegionAnnotation.h"

@implementation MXBaseMapView

- (id)initMapViewWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self){

    }
    return self;
}

- (void)animateToCameraPosition:(CLLocationCoordinate2D)centerCoor {
    
}

-(void)addAnnotationView:(RegionAnnotation*)regionAnnotatioin {
    
    
}

- (void)setMapPanned:(BOOL)mapPanned{
    self.isMapPanned = mapPanned;
}

@end
