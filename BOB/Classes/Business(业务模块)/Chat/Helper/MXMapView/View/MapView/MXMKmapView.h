//
//  MXMKmapView.h
//  MapDemo
//
//  Created by aken on 15/4/21.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "MXBaseMapView.h"
#import <MapKit/MapKit.h>


@protocol MXMKmapViewDelegate;

@interface MXMKmapView : MXBaseMapView

// 高德地图
@property (nonatomic, strong) MKMapView *mapView;

// 代理
@property (nonatomic, weak) id<MXMKmapViewDelegate>mkDelegate;

@end

@protocol MXMKmapViewDelegate <NSObject>

@optional
// 点击导航按钮
- (void)mxmkmapViewDidPressNavigation:(CLLocationCoordinate2D)coordinate;

@end