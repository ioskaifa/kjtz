//
//  MXBaseMapView.h
//  MapDemo
//
//  基础地图类，统一管理地图，包括基本的操作
//  Created by aken on 15/4/21.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXMapView.h"


@class RegionAnnotation;

@interface MXBaseMapView : UIView

// 是否是手触摸移动
@property (nonatomic) BOOL isMapPanned;

// 地图当前经纬度
@property (nonatomic) CLLocationCoordinate2D centerCoord;

@property (nonatomic, weak) id<MXMapViewDelegate>delegate;

// 设置是否触摸了地图
- (void)setMapPanned:(BOOL)mapPanned;

/**
 * 根据真实的地图对象,生成一个操作地图实体
 */
- (id)initMapViewWithFrame:(CGRect)frame;

/**
 *  根据经纬定位到当前视窗
 *
 *  @param centerCoor 经纬度
 */
- (void)animateToCameraPosition:(CLLocationCoordinate2D)centerCoor;

/**
 *  根据经纬度添加单个大头针
 *
 *  @param regionAnnotatioin //  地理位置消息对象，注解
 */
- (void)addAnnotationView:(RegionAnnotation*)regionAnnotatioin;

@end
