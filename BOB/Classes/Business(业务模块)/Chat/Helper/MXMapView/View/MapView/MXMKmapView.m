//
//  MXMKmapView.m
//  MapDemo
//
//  Created by aken on 15/4/21.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "MXMKmapView.h"
#import "RegionAnnotation.h"
#import "RegionAnnotationView.h"


@interface MXMKmapView()<MKMapViewDelegate,RegionAnnotationViewDelegate,UIGestureRecognizerDelegate>
{
   
    
}

//定位参数信息
@property(nonatomic,strong) RegionAnnotation *regionAnnotation;

//定位状态，包括6种状态
@property(nonatomic, assign) NSInteger loadStatus;

@end

@implementation MXMKmapView
@synthesize loadStatus;

- (id)initMapViewWithFrame:(CGRect)frame {
    
    self = [super  initMapViewWithFrame:frame];
    if(self){
        
        _mapView = [[MKMapView alloc] initWithFrame:frame];
        _mapView.delegate = self;
        
        // 不可以去掉此属性，此属性会影响didUpdateUserLocation代理去修正
        // 地理位置偏移，此属性会自身修正经纬度，目前网络免费的修正坐标方法，都不对准，
        // 更不能用OC私有方法。
        _mapView.showsUserLocation = YES;
        
        
        UIPanGestureRecognizer *mapPan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragMapAction:)];
        
        mapPan.delegate=self;
        
        [mapPan setMinimumNumberOfTouches:1];
        
        [mapPan setMaximumNumberOfTouches:1];
        
        [_mapView addGestureRecognizer:mapPan];
        
        self.isMapPanned = NO; //flag
        
        [self addSubview:_mapView];
    }
    return self;
}


- (void)animateToCameraPosition:(CLLocationCoordinate2D)centerCoor {
    
    if ( (centerCoor.latitude >= -90) && (centerCoor.latitude <= 90) && (centerCoor.longitude >= -180) && (centerCoor.longitude <= 180) ){
        
        MKCoordinateSpan span;
        //纬度比例(越小越精确)
        span.latitudeDelta = 0.002;
        //经度比例
        span.longitudeDelta = 0.002;
        
        //区域设置
        MKCoordinateRegion region;
        region.span = span;
        
        //设置区域
        region.center =centerCoor;
        
        [_mapView setRegion:region animated:YES];
    }
    
}

// 根据经纬度添加大头针
- (void)addAnnotationView:(RegionAnnotation *)regionAnnotatioin {
    
    if ([_mapView.annotations count]) {
        [_mapView removeAnnotations:_mapView.annotations];
    }
    
    if (!self.regionAnnotation) {
        self.regionAnnotation = [[RegionAnnotation alloc] init];
    }
    
    self.regionAnnotation = regionAnnotatioin;
    
    [_mapView addAnnotation:self.regionAnnotation];
    [_mapView selectAnnotation:self.regionAnnotation animated:YES];

    
}

- (void)setMapPanned:(BOOL)mapPanned {
    
    self.isMapPanned=mapPanned;
}

/******************************************************************delegate************************************************/
#pragma mark - MKMapView delegate
// 一进入页面便会调用此方法(与showsUserLocation相关，为YES时，此代理才会调用)
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapView:didFinishUpdateUserLocation:)]) {
        [self.delegate mapView:self.mapView didFinishUpdateUserLocation:userLocation.location.coordinate];
    }
}

// mapView的区域变化时会调用
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (self.isMapPanned) {
         CLLocationCoordinate2D centerCoor = mapView.centerCoordinate;
        if (self.delegate && [self.delegate respondsToSelector:@selector(mapViewCoordinate2D:regionDidChangeAnimated:)]) {
            [self.delegate mapViewCoordinate2D:centerCoor regionDidChangeAnimated:animated];
        }
    }
}

// 自定义大头针回调
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    if ([annotation isKindOfClass:[self.regionAnnotation class]]) {
        static NSString* identifier = @"MKAnnotationView";
        RegionAnnotationView *annotationView;
        
        annotationView = (RegionAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (!annotationView) {
            annotationView = [[RegionAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.acSheetDelegate = self;
        }
        annotationView.backgroundColor = [UIColor clearColor];
        annotationView.annotation = annotation;
        [annotationView layoutSubviews];
        [annotationView setCanShowCallout:NO];
        
        return annotationView;
    }else{
        return nil;
    }
}

#pragma mark - RegionAnnotationView delegate
- (void)navigationClick {
    
    if (self.mkDelegate && [self.mkDelegate respondsToSelector:@selector(mxmkmapViewDidPressNavigation:)]) {
        [self.mkDelegate mxmkmapViewDidPressNavigation:self.centerCoord];
    }
}

#pragma mark - UIGestureRecognizer delegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    self.isMapPanned = YES;
    
    return YES;
    
}
- (void)dragMapAction:(UIPanGestureRecognizer*)gest {

}

@end
