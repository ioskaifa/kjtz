//
//  MXMapView.m
//  MapDemo
//
//  Created by aken on 15/4/21.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "MXMapView.h"
#import "MXMKmapView.h"
#import "MXBaseMapView.h"

#import "RegionAnnotation.h"

@interface MXMapView()<MXMKmapViewDelegate>

// 高德地图（apple map）
@property (nonatomic, strong) MXMKmapView *mxMKmapView;

//@property (nonatomic, strong) MXGoogleMapView *mxGoogleMapView;

@end

@implementation MXMapView

- (void)dealloc{
    MLog(@"%@",NSStringFromClass([self class]));
}

- (id)initWithFrame:(CGRect)frame withLocation:(CLLocationCoordinate2D)location mapViewType:(MXMapViewType)mapViewType {
    
    self = [super initWithFrame:frame];
    if (self) {

        self.coordinate2D=location;
        
//        if (mapViewType==MXMapViewTypeAppleMap) {
        
            [self initMKMapView:self.frame];
            
            
//        }
//        else if(mapViewType==MXMapViewTypeGoogleMap){
//            
//            [self initGoogleMapView:self.frame];
//        }
        
        self.mapViewType=mapViewType;
    }
    return self;
    
}



// 初始化高德地图
- (void)initMKMapView:(CGRect)frame {
    
    _mxMKmapView=[[MXMKmapView alloc] initMapViewWithFrame:frame];
    _mxMKmapView.mkDelegate=self;
    _mxMKmapView.centerCoord=self.coordinate2D;
    self.mxBaseMapView=_mxMKmapView;
    [self addSubview:_mxMKmapView];
}

//// 初始化Google地图
//- (void)initGoogleMapView:(CGRect)frame {
//    
//    _mxGoogleMapView=[[MXGoogleMapView alloc] initMapViewWithFrame:frame coordinate2D:self.coordinate2D];
//    _mxGoogleMapView.gDelegate=self;
//    _mxGoogleMapView.centerCoord=self.coordinate2D;
//    self.mxBaseMapView=_mxGoogleMapView;
//    
//    [self addSubview:_mxGoogleMapView];
//}

- (void)setDelegate:(id<MXMapViewDelegate>)delegate_ {
    _delegate = delegate_;
    self.mxBaseMapView.delegate = delegate_;
}

- (void)animateToCameraPosition:(CLLocationCoordinate2D)centerCoor {
    
//    if (self.mapViewType==MXMapViewTypeAppleMap) {
    
        [_mxMKmapView animateToCameraPosition:centerCoor];
//        
//    }
//    else if(self.mapViewType==MXMapViewTypeGoogleMap){
//        [_mxGoogleMapView animateToCameraPosition:centerCoor];
//    }
}

- (void)addAnnotationView:(RegionAnnotation*)regionAnnotatioin {
    
//    if (self.mapViewType==MXMapViewTypeAppleMap) {
    
        [_mxMKmapView addAnnotationView:regionAnnotatioin];
        
//    }else if(self.mapViewType==MXMapViewTypeGoogleMap){
//        [_mxGoogleMapView addAnnotationView:regionAnnotatioin];
//    }
}

// 设置是否触摸了地图
- (void)setMapPanned:(BOOL)mapPanned {
    
//    if (self.mapViewType==MXMapViewTypeAppleMap) {
        [_mxMKmapView setMapPanned:mapPanned];
//    }else if (self.mapViewType==MXMapViewTypeGoogleMap){
//        [_mxGoogleMapView setMapPanned:mapPanned];
//    }
}

/******************************************************************delegate************************************************/
#pragma mark - MXMKmapView delegate
- (void)mxmkmapViewDidPressNavigation:(CLLocationCoordinate2D)coordinate {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(mxmapViewDidPressNavigation:)]) {
        [self.delegate mxmapViewDidPressNavigation:coordinate];
    }
    
}

#pragma mark - MXGoogleMapView delegate
- (void)mxgoogleMapViewDidPressNavigation:(CLLocationCoordinate2D)coordinate {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(mxmapViewDidPressNavigation:)]) {
        [self.delegate mxmapViewDidPressNavigation:coordinate];
    }
    
}

@end
