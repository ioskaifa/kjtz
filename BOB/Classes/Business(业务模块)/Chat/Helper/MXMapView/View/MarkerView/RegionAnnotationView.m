//
//  RegionAnnotationView.m
//  MapDemo
//
//  Created by aken on 15/4/22.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "RegionAnnotationView.h"
#import "RegionAnnotation.h"
#import "MXMapConfig.h"
#import "MXMapViewHelper.h"

@implementation RegionAnnotationView
@synthesize regionDetailView;
@synthesize acSheetDelegate;
@synthesize bgImgView;
@synthesize regionAnnotaytion;


- (id)init {
    
    self=[super init];
    if (self) {
        
        [self initUI];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.canShowCallout = NO;
        [self addCallView];
    }
    return self;
}
- (void)layoutRegionSubviews{
    regionAnnotaytion = self.annotation;
    
    self.canShowCallout = NO;
    [self loadNaviView];
}

-(void)addCallView{
    regionAnnotaytion = self.annotation;
    
    self.canShowCallout = NO;
    [self loadNaviView];

}
-(void)initUI{
    if (self.regionDetailView) {
        [self.regionDetailView removeFromSuperview];
    }
    self.regionDetailView = [[UIView alloc] init];
    self.regionDetailView.backgroundColor = [UIColor clearColor];
    self.bgImgView = [[UIImageView alloc] init];
    self.bgImgView.image = [[UIImage imageNamed:mapViewMarkerViewBgImage] stretchableImageWithLeftCapWidth:28 topCapHeight:16];
    [self.regionDetailView addSubview:self.bgImgView];
}
//导航获取地址成功
-(void)loadNaviView{
    [self loadViewSuc:NO];
}


//loading view suc
-(void)loadViewSuc:(BOOL)chooseLoad{
    [self initUI];
    float maxWidth;
    if (chooseLoad) {
        maxWidth = 290.0;
    }else{
        maxWidth = 235.0;
    }
    
    CGSize addSize1 = [MXMapViewHelper getSizeOfString:self.regionAnnotaytion.subtitle maxWidth:maxWidth withFontSize:14];
    CGSize addSize2 = [MXMapViewHelper getSizeOfString:self.regionAnnotaytion.subtitle maxWidth:maxWidth withFontSize:12];
    float width = ([MXMapViewHelper commpareWith:addSize1 size2:addSize2] <= 80) ? 80 : [MXMapViewHelper commpareWith:addSize1 size2:addSize2];
    
    if (chooseLoad) {
        CGRect bgCgrect = CGRectMake(-(width+20)/2, -80, width+20, 56);
        
        regionDetailView.frame = bgCgrect;
        [self addConner:bgCgrect];
        
    }else{
        CGRect anFrame = CGRectMake(0, 0, width+20+65, 162);
        [self addCenter:anFrame];
        
        self.frame = anFrame;
        self.backgroundColor = [UIColor clearColor];
        
        CGRect bgCgrect = CGRectMake(0, 0, width+20+65, 56);
        
        regionDetailView.frame = bgCgrect;
        [self addConner:bgCgrect];
        
        [self createBtn:width+10];
    }
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.frame = CGRectMake(5, 5, width, 15);
    titleLab.font = [UIFont font14];
    titleLab.text = self.regionAnnotaytion.title;
    titleLab.backgroundColor = [UIColor clearColor];
    titleLab.font = [UIFont font14];
    titleLab.textColor = [UIColor whiteColor];
    [regionDetailView addSubview:titleLab];
    
    UILabel *subtitleLab = [[UILabel alloc] init];
    subtitleLab.frame = CGRectMake(5, 25, width, 15);
    subtitleLab.font = [UIFont font12];
    subtitleLab.text = self.regionAnnotaytion.subtitle;
    subtitleLab.textColor = [UIColor whiteColor];
    subtitleLab.backgroundColor = [UIColor clearColor];
    [regionDetailView addSubview:subtitleLab];
    
    [self addSubview:regionDetailView];
}

// 导航按钮
-(void)createBtn:(float)width{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(navigationClitk:) forControlEvents:UIControlEventTouchUpInside];
//    button.backgroundColor=[UIColor brownColor];
    button.frame = CGRectMake(width + 10, 0, 55, 46);
    [self.regionDetailView addSubview:button];
    
    // 导航图标
    UIImageView *navBg = [[UIImageView alloc] init];
    navBg.image=[UIImage imageNamed:mapViewNavigationIcon];
    navBg.frame=CGRectMake(button.frame.size.width/2 - 10 + 5, 5, 20, 20);
    [button addSubview:navBg];
    
    // title
    UILabel *titleLab = [[UILabel alloc] init];
    titleLab.frame = CGRectMake(5, 28, 55, 15);
    titleLab.font = [UIFont font14];
    titleLab.text = MXLang(@"Nav_Button", @"导航");
    titleLab.backgroundColor = [UIColor clearColor];
    titleLab.font = [UIFont font14];
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment=NSTextAlignmentCenter;
    [button addSubview:titleLab];
    
    UIView *line=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, 46)];
    line.backgroundColor=[UIColor colorWithWhite:1 alpha:0.8];
    [button addSubview: line];
    
}

#pragma mark- acSheetDelegate
-(void)navigationClitk:(id)sender{
    
    if (acSheetDelegate && [acSheetDelegate respondsToSelector:@selector(navigationClick)]) {
        [acSheetDelegate navigationClick];
    }
    
}

//添加尖角图片
-(void)addConner:(CGRect)rect{
    self.bgImgView.frame = CGRectMake(0, 0, rect.size.width, rect.size.height-10);
    
    UIImageView *connerView = [[UIImageView alloc] initWithFrame:CGRectMake((rect.size.width-16)/2, rect.size.height-10, 16, 10)];
    connerView.image = [UIImage imageNamed:mapViewMarkerViewCorner];
    
    [self.bgImgView addSubview:connerView];
}
//RegionAnnotationView中心定位
-(void)addCenter:(CGRect)anFrame{
    UIImageView *locImg = [[UIImageView alloc] init];
    locImg.frame = CGRectMake((anFrame.size.width-20)/2, 56, 23.5, 31.5);
    locImg.image = [UIImage imageNamed:mapViewLocationIcon];
    [self.regionDetailView addSubview:locImg];
}



@end