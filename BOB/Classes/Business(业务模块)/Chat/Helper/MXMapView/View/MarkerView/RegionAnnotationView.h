//
//  RegionAnnotationView.h
//  MapDemo
//
//  弹出层界面
//  Created by aken on 15/4/22.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import <MapKit/MapKit.h>



@class RegionAnnotation;

@protocol RegionAnnotationViewDelegate <NSObject>

@optional
// 导航按钮点击
-(void)navigationClick;

@end

@interface RegionAnnotationView : MKAnnotationView

// 背景
@property(nonatomic,strong) UIView *regionDetailView;

// 背景图片
@property(nonatomic,strong) UIImageView *bgImgView;

@property(nonatomic,weak) id<RegionAnnotationViewDelegate>acSheetDelegate;

@property(nonatomic,strong) RegionAnnotation *regionAnnotaytion;

- (void)layoutRegionSubviews;

@end