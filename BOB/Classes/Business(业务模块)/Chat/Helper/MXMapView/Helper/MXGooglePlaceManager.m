//
//  MXGooglePlaceManager.m
//  MapDemo
//
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "MXGooglePlaceManager.h"
#import "GoogleNearbysearchApi.h"
#import "GoogleAutoPlaceApi.h"
#import "GooglePlaceDetailApi.h"
#import "MXMapConfig.h"

#import "RegionAnnotation.h"
#import "SearchAutoPlacesModel.h"

@implementation MXGooglePlaceManager

+ (NSString*)googleKey {
    
    NSString *key=nil;
    
    if ([StringUtil isGroupApp]) {
        
        key=GooglePlaceKey;
    }else{
        key=GooglePlaceForStoreKey;
    }
    
    return key;
}

#pragma mark - 附近搜索相关的位置信息
+ (MXRequest*)searchNearbyWithKeyWord:(NSString*)keyword location:(CLLocationCoordinate2D)coord2d completion:(MXHttpRequestListCallBack)completion {
    
    
    NSString* locString = [NSString stringWithFormat:@"%f,%f", coord2d.latitude, coord2d.longitude];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:keyword forKey:@"input"];
    [dic setValue:locString forKey:@"location"];
    [dic setValue:[NSString stringWithFormat:@"%d",maxSearchRadius] forKey:@"radius"];
    [dic setValue:@"true" forKey:@"sensor"];
    [dic setValue:[self googleKey] forKey:@"key"];
    
    GoogleNearbysearchApi *api = [[GoogleNearbysearchApi alloc] initWithParameter:dic];
//    api.ignoreCache=YES;
    
    [api startWithCompletionBlockWithSuccess:^(MXRequest *request) {

        NSString* errorString = @"";
        NSMutableArray *arrayData=nil;
        
        @try {

            NSDictionary *tempDic=(NSDictionary*)request.responseJSONObject;
                        
            if ([tempDic isKindOfClass:[NSDictionary class]]) {
                
                NSString *code=tempDic[@"status"];
                if ([code isEqualToString:@"OK"]) {
                 
                    NSMutableArray *tempArray=tempDic[@"results"];
                    
                    if ([tempArray isKindOfClass:[NSArray class]]) {
                        
                        arrayData=[NSMutableArray array];
                        
                        for (NSDictionary *dic in tempArray ) {
                            
                            RegionAnnotation *region=[[RegionAnnotation alloc] init];
                            [region dictionaryToModel:dic];
                            [arrayData addObject:region];
                        }
                    }
                }
                
                
            }
            
            completion(arrayData,errorString);
            
        }
        @catch (NSException *exception) {
            errorString = [exception name];
            MLog(@"%@",errorString);
        }
        
        
    } failure:^(MXRequest *request) {
        completion(nil,[api requestServerErrorString]);
    }];
    
    
    return api;
}

#pragma mark - 商家信息自动填充
+ (MXRequest*)searchAutocompletePlaceLists:(NSString*)keyword location:(CLLocationCoordinate2D)coord2d completion:(MXHttpRequestListCallBack)completion {
    
    
    NSString* locString = [NSString stringWithFormat:@"%f,%f", coord2d.latitude, coord2d.longitude];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:keyword forKey:@"input"];
    [dic setValue:locString forKey:@"location"];
    [dic setValue:[NSString stringWithFormat:@"%d",maxSearchRadius] forKey:@"radius"];
    [dic setValue:@"true" forKey:@"sensor"];
    [dic setValue:[self googleKey] forKey:@"key"];
    
    GoogleAutoPlaceApi *api = [[GoogleAutoPlaceApi alloc] initWithParameter:dic];
//    api.ignoreCache=YES;
    
    [api startWithCompletionBlockWithSuccess:^(MXRequest *request) {
        
        NSString* errorString = @"";
        NSMutableArray *arrayData=nil;
        
        @try {
 
            NSDictionary *tempDic=(NSDictionary*)request.responseJSONObject;
                        
            if ([tempDic isKindOfClass:[NSDictionary class]]) {
                
                NSString *code=tempDic[@"status"];
                if ([code isEqualToString:@"OK"]) {
                    
                    NSMutableArray *tempArray=tempDic[@"predictions"];
                    
                    if ([tempArray isKindOfClass:[NSArray class]]) {
                        
                        arrayData=[NSMutableArray array];
                        
                        for (NSDictionary *dic in tempArray ) {
                            
                            SearchAutoPlacesModel *region=[[SearchAutoPlacesModel alloc] init];
                            [region dictionaryToModel:dic];
                            [arrayData addObject:region];
                        }
                    }
                }
                
                
            }
            
            completion(arrayData,errorString);
            
        }
        @catch (NSException *exception) {
            errorString = [exception name];
            MLog(@"%@",errorString);
        }
        
        
    } failure:^(MXRequest *request) {
        completion(nil,[api requestServerErrorString]);
    }];
    
    
    return api;
}

#pragma mark -  根据名字查看地址详情
+ (MXRequest*)googlePlaceDetails:(NSString*)keyword completion:(MXHttpRequestListCallBack)completion {
    
    
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:keyword forKey:@"reference"];
    [dic setValue:@"true" forKey:@"sensor"];
    [dic setValue:[self googleKey] forKey:@"key"];
    
    GooglePlaceDetailApi *api = [[GooglePlaceDetailApi alloc] initWithParameter:dic];
//    api.ignoreCache=YES;
    
    [api startWithCompletionBlockWithSuccess:^(MXRequest *request) {
        
        NSString* errorString = @"";
        NSMutableArray *arrayData=nil;
        
        @try {

            NSDictionary *tempDic=(NSDictionary*)request.responseJSONObject;
            
            if ([tempDic isKindOfClass:[NSDictionary class]]) {
                
                NSString *code=tempDic[@"status"];
                if ([code isEqualToString:@"OK"]) {
                    
                    NSDictionary *tempDic2=tempDic[@"result"];
                    
                    if ([tempDic2 isKindOfClass:[NSDictionary class]]) {
                        arrayData=[NSMutableArray array];
                        RegionAnnotation *region=[[RegionAnnotation alloc] init];
                        [region dictionaryToModel:tempDic2];
                        [arrayData addObject:region];

                    }

                }
                
            }
            
            completion(arrayData,errorString);
            
        }
        @catch (NSException *exception) {
            errorString = [exception name];
            MLog(@"%@",errorString);
        }
        
        
    } failure:^(MXRequest *request) {
        completion(nil,[api requestServerErrorString]);
    }];
    
    
    return api;
}
@end
