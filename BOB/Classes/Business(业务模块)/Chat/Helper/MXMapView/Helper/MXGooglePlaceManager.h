//
//  MXGooglePlaceManager.h
//  MapDemo
//
//  Google Places API 使用

//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXRequest.h"

@interface MXGooglePlaceManager : NSObject

/**
 *  附近搜索相关的位置信息
 *
 *  @param keyword    关键字
 *  @param coord2d    经纬度
 *  @param completion 回调
 *
 *  @return
 */
+ (MXRequest*)searchNearbyWithKeyWord:(NSString*)keyword location:(CLLocationCoordinate2D)coord2d completion:(MXHttpRequestListCallBack)completion;

/**
 *  商家信息自动填充
 *
 *  @param keyword     关键字
 *  @param coord2d    经纬度
 *  @param completion 回调
 *
 *  @return
 */
+ (MXRequest*)searchAutocompletePlaceLists:(NSString*)keyword location:(CLLocationCoordinate2D)coord2d completion:(MXHttpRequestListCallBack)completion;

/**
 *  根据名字查看地址详情
 *
 *  @param keyword    关键字
 *  @param completion 回调
 *
 *  @return
 */
+ (MXRequest*)googlePlaceDetails:(NSString*)keyword completion:(MXHttpRequestListCallBack)completion;

@end
