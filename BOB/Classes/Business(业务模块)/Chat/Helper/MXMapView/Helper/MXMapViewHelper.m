//
//  MXMapViewHelper.m
//  MapDemo
//
//  Created by aken on 15/4/22.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "MXMapViewHelper.h"
#import "MXMapConfig.h"

@implementation MXMapViewHelper

+(NSArray *)checkHasOwnMapApp {
    
    NSArray *mapSchemeArr = @[@"iosamap://navi",@"baidumap://map/",@"http://maps.apple.com/",@"comgooglemaps://"];
//    NSArray *mapSchemeArr = @[@"comgooglemaps://",@"iosamap://navi"];
    NSMutableArray *appListArr = [NSMutableArray arrayWithCapacity:1];
    
    for (int i = 0; i < [mapSchemeArr count]; i++) {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[mapSchemeArr safeObjectAtIndex:i]]]]) {
            if (i == 0) {
                [appListArr addObject:kNavigationAppleMap];
            } else if (i == 1) {
                [appListArr addObject:kNavigationBaiduMap];
            } else if (i == 2) {
                [appListArr addObject:kNavigationMapOfApple];
            } else if (i == 3) {
                [appListArr addObject:kNavigationGoogleMap];
            }
        }
    }
    
    return appListArr;
}

// 获取指定最大宽度和字体大小的string的size
+ (CGSize)getSizeOfString:(NSString *)string maxWidth:(float)width withFontSize:(int)fontSize {
    
    UIFont *font = [UIFont systemFontOfSize:fontSize];
    CGSize size = [string sizeWithFont:font constrainedToSize:CGSizeMake(width, 10000.0f) lineBreakMode:NSLineBreakByWordWrapping];
    return size;
}

//判断title和subtitle宽度
+ (float)commpareWith:(CGSize)size1 size2:(CGSize)size2 {
    
    if (size1.width >= size2.width) {
        return size1.width;
    }else{
        return size2.width;
    }
}

@end
