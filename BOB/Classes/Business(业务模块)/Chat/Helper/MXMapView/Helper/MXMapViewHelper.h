//
//  MXMapViewHelper.h
//  MapDemo
//
//  地图管理工具类
//  Created by aken on 15/4/22.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MXMapViewHelper : NSObject

// 判断手机上是否安装有地图app
+(NSArray *)checkHasOwnMapApp;


/**
 *  获取指定最大宽度和字体大小的string的size
 *
 *  @param string   字符串
 *  @param width    最大宽度
 *  @param fontSize 字体
 *
 *  @return 字符串的总长度
 */
+ (CGSize)getSizeOfString:(NSString *)string maxWidth:(float)width withFontSize:(int)fontSize;

/**
 *  判断title和subtitle宽度
 *
 *  @param size1 当前Size
 *  @param size2 目标Size
 *
 *  @return 最大Width
 */
+ (float)commpareWith:(CGSize)size1 size2:(CGSize)size2;

@end
