//
//  GoogleAutoPlaceApi.m
//  MapDemo
//
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "GoogleAutoPlaceApi.h"
#import "MXMapConfig.h"

@implementation GoogleAutoPlaceApi

- (NSString *)requestUrl {
    return google_autocomplete_place;
}

@end
