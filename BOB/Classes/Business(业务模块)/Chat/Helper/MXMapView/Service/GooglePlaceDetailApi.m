//
//  GooglePlaceDetailApi.m
//  MapDemo
//
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "GooglePlaceDetailApi.h"
#import "MXMapConfig.h"

@implementation GooglePlaceDetailApi

- (NSString *)requestUrl {
    
    return google_service_place_details;
}

@end
