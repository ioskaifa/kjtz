//
//  GoogleNearbysearchApi.m
//  MapDemo
//
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "GoogleNearbysearchApi.h"
#import "MXMapConfig.h"

@interface GoogleNearbysearchApi()

@property (nonatomic, strong) NSMutableDictionary *paramDictionary;

@end

@implementation GoogleNearbysearchApi

- (id)initWithParameter:(NSDictionary*)dictionary{
    self = [super init];
    if (self) {
        self.paramDictionary = [dictionary mutableCopy];;
        
    }
    return self;
}

- (NSString *)requestUrl {
    return google_service_nearbysearch ;
}

- (MXRequestMethod)requestMXMethod {
    
    return MXRequestMethodGet;
}

- (id)requestArgument {
    return self.paramDictionary ;
}

@end