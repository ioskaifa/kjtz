//
//  GoogleNearbysearchApi.h
//  MapDemo
//
//  根据经纬度搜索附近地理信息
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "MXRequest.h"

@interface GoogleNearbysearchApi : MXRequest

- (id)initWithParameter:(NSDictionary*)dictionary;

@end
