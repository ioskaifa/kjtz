//
//  LocationNavigateVC.h
//  MapDemo
//
//  简单的弹出当前位置，点击导航
//  Created by aken on 15/4/25.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXMapConfig.h"

@interface LocationNavigateVC : BaseViewController


// 地图初始化类型，默认是apple map
@property (nonatomic, assign) MXMapViewType mapViewType;

// 目标经纬度
@property (nonatomic) CLLocationCoordinate2D toCoordinate2D;

// 目标地址
@property (nonatomic, strong) NSString *address;

// 目标城市
@property (nonatomic, strong) NSString *city;

@property (nonatomic, assign) BOOL showTitle;


@end
