//
//  LocationShareVC.m
//  MapDemo
//
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "LocationShareVC.h"
#import "MXMapView.h"
#import "LocationCell.h"
#import "MXAddressManager.h"
#import "RegionAnnotation.h"
#import "MXAddressModel.h"
#import "SearchAutoPlacesModel.h"
#import "MXBaseMapView.h"
#import <AMapSearchKit/AMapCommonObj.h>
#import "MXLocationManager.h"
#import "MXFixCllocation2DHelper.h"
#import "LocationSearchVC.h"
#import "UIView+Utils.h"

@interface LocationShareVC () <MXMapViewDelegate, UISearchBarDelegate, UISearchControllerDelegate, UITableViewDataSource, UITableViewDelegate> {
    BOOL _completeLocate;//是否完成定位及周边搜索
    
    BOOL _searchflag;//标明当前是按区域搜索(NO)/key搜素(YES)
}

// 地图对象
@property (nonatomic,strong) MXMapView *mxMapView;

// 当前经纬度
@property (nonatomic) CLLocationCoordinate2D coordinate2D;

// 搜索框
@property (nonatomic,strong) UISearchBar *searchBar;
@property (nonatomic,strong) UISearchController *searchController;

// tableview
@property (nonatomic,strong) UITableView *tableView;
// 源数据
@property (nonatomic,strong) NSMutableArray *dataSource;
// 选中行
@property (nonatomic) NSInteger currentSelected;
// 当前地址
@property (nonatomic,strong) NSString *currentAddress;

//special by lhy 2016年03月03日
// 当前详细地址
@property (nonatomic,strong) NSString *currentSubAddress;

// 当前城市名称
@property (nonatomic,strong) NSString *currentCity;

// 关键字搜索
@property (nonatomic,strong) NSMutableArray *searchDataSource;

// 发送按钮
@property (nonatomic,strong) UIBarButtonItem *nextItem;

//不显示当前位置
@property (nonatomic,strong) UIButton *showCurLocation;

@property (nonatomic,assign) CGRect tableViewRect;

@property (nonatomic, strong) UIView *loadingView;

@property (nonatomic, strong) RegionAnnotation *selectPoiAnnotation;
@property (nonatomic, strong) LocationSearchVC *resultVC;

@end

@implementation LocationShareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.definesPresentationContext=YES;
    
    if ([[[UIDevice currentDevice] systemVersion ] floatValue]  > 6.9) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.currentSelected = -1;
    //    self.navigationController.navigationBar.translucent=NO;
    
    [MXAddressManager sharedInstance].mapViewType=self.mapViewType;
    
    self.dataSource = [NSMutableArray array];
    self.searchDataSource=[NSMutableArray array];
    
    [self initLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    
    _mxMapView=nil;
}

#pragma mark 初始化UI相关消息
- (void)initLayout {
    
    [self setNavBarTitle:@"地图"];
    
    self.isShowBackButton=YES;
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    // 地图
    CLLocationCoordinate2D coordinate=[MXLocationManager shareManager].location;
    _mxMapView=[[MXMapView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,  [UIScreen mainScreen].bounds.size.height/2)
                                   withLocation:coordinate mapViewType:self.mapViewType];
    _mxMapView.delegate=self;
    [self.view addSubview:_mxMapView];
    // 此处不要，用地图当前定位点，见didFinishUpdateUserLocation
//    if (CLLocationCoordinate2DIsValid(coordinate)) {
//        [self initWithCoordinate:coordinate];
//    }
//
    
    // 发送按钮
    UIButton *nextButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton setEnabled:NO];
    nextButton.frame=CGRectMake(0, 0, 80, 40);
    [nextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [nextButton setTitle:MXLang(@"Public_Ok", @"确定") forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(sendClick) forControlEvents:UIControlEventTouchUpInside];
    nextButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.nextItem=[[UIBarButtonItem alloc] initWithCustomView:nextButton];
//    [self.nextItem setEnabled:NO];
    self.navigationItem.rightBarButtonItem=self.nextItem;
    
    // SearchBar
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,
                                                               0,
                                                               [UIScreen mainScreen].bounds.size.width, 44)];
    
    _searchBar.barStyle     = UIBarStyleDefault;
    _searchBar.delegate     = self;
    _searchBar.placeholder  = MXLang(@"Talk_controller_locationVC_search_1", @"搜索");
    _searchBar.keyboardType = UIKeyboardTypeDefault;
    //[self.view addSubview:_searchBar];
    [self.view addSubview:self.searchController.searchBar];
    
    // tableview
    _tableViewRect = CGRectMake(0,
                                CGRectGetHeight(self.view.bounds)/2+(_isNeedShowCurLocation ? 44 : 0),
                                SCREEN_WIDTH,
                                SCREEN_HEIGHT - CGRectGetHeight(_mxMapView.frame)-64-(_isNeedShowCurLocation ? 44 : 0));
    _tableView = [[UITableView alloc]initWithFrame:_tableViewRect style:UITableViewStylePlain];
    _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    // 当前位置
    UIButton *locateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    locateBtn.frame = CGRectMake(20, CGRectGetHeight(self.view.bounds)/2-100 + 44, 35, 35);
    [locateBtn setBackgroundImage:[UIImage imageNamed:@"location_locate1"] forState:UIControlStateNormal];
    [locateBtn addTarget:self action:@selector(locateBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_mxMapView addSubview:locateBtn];
    
    // 中心位置
    UIImageView *centerAnntoView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:mapViewLocationIcon]];
    centerAnntoView.frame = CGRectMake(self.view.bounds.size.width/2-5, _mxMapView.frame.size.height/2 - 10, 32, 32);
    [self.view addSubview:centerAnntoView];
    
    if(self.isNeedShowCurLocation)
    {
        //是否显示当前位置
        _showCurLocation = [UIButton buttonWithType:UIButtonTypeCustom];
        _showCurLocation.frame = CGRectMake(0, CGRectGetMaxY(_mxMapView.frame), SCREEN_WIDTH, 44);
        _showCurLocation.titleLabel.font = [UIFont font17];
        [_showCurLocation setTitle:[NSString stringWithFormat:@"   %@",MXLang(@"MoMessage_NoShowCurrentLocation", @"不显示当前位置")] forState:UIControlStateNormal];
        [_showCurLocation setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_showCurLocation setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_showCurLocation addTarget:self action:@selector(showCurLocationSelector:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_showCurLocation];
        
        UIButton *checkBnt = [UIButton buttonWithType:UIButtonTypeCustom];
        checkBnt.userInteractionEnabled = NO;
        checkBnt.tag = 999;
        checkBnt.frame = CGRectMake(SCREEN_WIDTH-26, (44-16)/2, 16, 16);
        [checkBnt setImage:[UIImage imageNamed:@"unCheckIcon"] forState:UIControlStateNormal];
        [checkBnt setImage:[UIImage imageNamed:@"checkIcon"] forState:UIControlStateSelected];
        [_showCurLocation addSubview:checkBnt];
        
        UIView *line = [MXSeparatorLine initHorizontalLineWidth:SCREEN_WIDTH orginX:0 orginY:44];
        [_showCurLocation addSubview:line];
    }
    [_tableView addSubview:self.loadingView];
}

-(void)initWithCoordinate:(CLLocationCoordinate2D)coordinate{
    [NotifyHelper hideAllHUDsForView:self.view animated:YES];
    self.coordinate2D = coordinate;
    [MXAddressManager sharedInstance].coordinate2D=coordinate;
    
    MLog(@"initWithCoordinate:%f,%f",coordinate.latitude,coordinate.longitude);
    
    [_mxMapView animateToCameraPosition:coordinate];
    
    @weakify(self);// 地理消息反编译
    [[MXAddressManager sharedInstance] reverseGeocodingWithCoordinate:coordinate completion:^(MXAddressModel *model) {
        @strongify(self)
        if (model) {
            self.currentCity=model.cityName;
            self.resultVC.currentCity = self.currentCity;
            self.nextItem.enabled=YES;
            [self searchPlayWithCoordinate:coordinate];
        }
    }];
}

- (void)showCurLocationSelector:(UIButton *)bnt
{
    _showCurLocation.selected = !_showCurLocation.selected;
    UIButton *subBnt = (UIButton *)[_showCurLocation viewWithTag:999];
    subBnt.selected = _showCurLocation.selected;
    CGRect rect = _tableView.frame;
    if(!subBnt.selected)
        rect.size.height = CGRectGetHeight(_tableViewRect);
    else
        rect.size.height = 0;
    [UIView animateWithDuration:0.35 animations:^{
        _tableView.frame = rect;
    }];
}

#pragma mark - 按钮事件
//  发送地理位置
-(void)sendClick {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendCurrentLocation:andAddress:)]) {
        [self.navigationController popViewControllerAnimated:YES];
        
        if(_isNeedShowCurLocation && _showCurLocation.selected){
            
            self.coordinate2D = CLLocationCoordinate2DMake(0, 0);
            self.currentAddress = MXLang(@"MoMessage_NoShowCurrentLocation", @"不显示当前位置");
            [self.delegate sendCurrentLocation:self.coordinate2D andAddress:self.currentAddress];
            return;
        }
            
        //yangjiale
        if ([StringUtil isEmpty:self.currentAddress]) {
            if (self.dataSource.count > 0) {
                RegionAnnotation *poiAnnotation = self.dataSource[0];
                self.currentAddress = poiAnnotation.title;
                self.coordinate2D = poiAnnotation.coordinate;
            } else {
                
                self.currentAddress = @"无法获取地址";
                
//                [NotifyHelper showMessageWithMakeText:@"没获取到定位地址，请打开定位！"];
//                return;
            }
        }

        [self.delegate sendCurrentLocation:self.coordinate2D andAddress:self.currentAddress];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendCurrentLocation:andAddress:andSubAddress:)]) {
        [self.navigationController popViewControllerAnimated:YES];
        
        if(_isNeedShowCurLocation && _showCurLocation.selected){
            
            self.coordinate2D = CLLocationCoordinate2DMake(0, 0);
            self.currentAddress = MXLang(@"MoMessage_NoShowCurrentLocation", @"不显示当前位置");
            [self.delegate sendCurrentLocation:self.coordinate2D andAddress:self.currentAddress];
            return;
        }
        
        //yangjiale
        if ([StringUtil isEmpty:self.currentAddress]) {
            if (self.dataSource.count > 0) {
                RegionAnnotation *poiAnnotation = self.dataSource[0];
                self.currentAddress = poiAnnotation.title;
                self.coordinate2D = poiAnnotation.coordinate;
            } else {
                [NotifyHelper showMessageWithMakeText:@"没获取到定位地址，请打开定位！"];
                return;
            }
        }
        
        [self.delegate sendCurrentLocation:self.coordinate2D andAddress:self.currentAddress andSubAddress:self.currentSubAddress];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendCurrentLocation:andAddress:andSubAddress:screenShot:)]) {
        [self.navigationController popViewControllerAnimated:YES];
        
        if(_isNeedShowCurLocation && _showCurLocation.selected){
            
            self.coordinate2D = CLLocationCoordinate2DMake(0, 0);
            self.currentAddress = MXLang(@"MoMessage_NoShowCurrentLocation", @"不显示当前位置");
            [self.delegate sendCurrentLocation:self.coordinate2D andAddress:self.currentAddress];
            return;
        }
        
        //yangjiale
        if ([StringUtil isEmpty:self.currentAddress]) {
            if (self.dataSource.count > 0) {
                RegionAnnotation *poiAnnotation = self.dataSource[0];
                self.currentAddress = poiAnnotation.title;
                self.currentSubAddress = poiAnnotation.subtitle;
                self.coordinate2D = poiAnnotation.coordinate;
            } else {
                [NotifyHelper showMessageWithMakeText:@"没获取到定位地址，请打开定位！"];
                return;
            }
        }
        
        [self.delegate sendCurrentLocation:self.coordinate2D andAddress:self.currentAddress andSubAddress:self.currentSubAddress screenShot:[UIView imageWithUIView:self.mxMapView imageName:@"wl_location_center"]];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendCurrentLocation:)]) {
        [self.delegate sendCurrentLocation:self.selectPoiAnnotation];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
// 定位当前位置
-(void)locateBtnClick {
    
    [_mxMapView animateToCameraPosition:[MXLocationManager shareManager].location];
}

// 根据关键字搜索
- (void)searchPlaceWithKeyWord:(NSString*)keyword cityCode:(NSString*)cityCode {
    
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    __weak typeof(self) weakVC=self;
    // 高德POI搜索
    [[MXAddressManager sharedInstance] searchPOIWithKey:keyword adcode:cityCode completion:^(NSMutableArray *array) {
        [NotifyHelper hideAllHUDsForView:weakVC.view animated:YES];
        if (array && array) {
            weakVC.dataSource=[NSMutableArray arrayWithArray:array];
            [weakVC.tableView reloadData];
            [weakVC tableView:weakVC.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }];
}

// 根据经纬度搜索附近
- (void)searchPlayWithCoordinate:(CLLocationCoordinate2D)coord {
    __weak typeof(self) weakVC=self;
    // 高德POI搜索
    [[MXAddressManager sharedInstance] searchPOIWithCoordinate:coord andRadius:maxSearchRadius completion:^(NSMutableArray *array) {
        [UIView animateWithDuration:0.5f animations:^{
            weakVC.loadingView.alpha = 0;
        } completion:^(BOOL finished) {
            [weakVC.loadingView removeFromSuperview];
        } ];
        weakVC.loadingView.hidden = YES;
        if (array && array.count > 0) {
            //            [weakVC.dataSource addObjectsFromArray:array];
            weakVC.dataSource=[NSMutableArray arrayWithArray:array];
            [weakVC.tableView reloadData];
        }
    }];
}

// 自动提示搜索
- (void)searchPlaceWithAutoComplete:(NSString *)keyword {
    @weakify(self)
    [[MXAddressManager sharedInstance] searchTipsWithKeyword:keyword cityname:self.currentCity completion:^(NSMutableArray *array) {
        @strongify(self)
        if (array && array.count > 0) {
            [self.searchDataSource setArray:array];
        }
    }];
}

#pragma mark - ==========自定义方法==============
- (void)locateAssign:(CLLocationCoordinate2D)coordinate2D
{
    self.coordinate2D = coordinate2D;
    [_mxMapView animateToCameraPosition:coordinate2D];
}

/*******************************************************************delegate**********************************************************/
#pragma mark - MXMapView delegate
- (void)mapViewCoordinate2D:(CLLocationCoordinate2D)coordinate2D regionDidChangeAnimated:(BOOL)animated {
    self.currentSelected = -1;
    self.currentAddress = @"";
    
    [NotifyHelper hideHUDForView:self.view animated:YES];
    self.coordinate2D=coordinate2D;
    [self searchPlayWithCoordinate:coordinate2D];
}

- (void)mapView:(UIView *)mapView didFinishUpdateUserLocation:(CLLocationCoordinate2D)coordinate {
    
    //引入_completeLocate是为控制该方法只走一次
    if (!_completeLocate) {
        [self initWithCoordinate:coordinate];
        _completeLocate = YES;
    }
    
    MLog(@"didFinishUpdateUserLocation:%f,%f",coordinate.latitude,coordinate.longitude);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *tipCellIdentifier = @"tipCellIdentifier";

    LocationCell *cell = [tableView dequeueReusableCellWithIdentifier:tipCellIdentifier];

    if (cell == nil) {
        cell = [[LocationCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                   reuseIdentifier:tipCellIdentifier];
    }
    
    RegionAnnotation *poiAnnotation = self.dataSource[indexPath.row];
    
    if ([poiAnnotation isKindOfClass:[RegionAnnotation class]]) {
        cell.textLabel.text = poiAnnotation.title;
        cell.detailTextLabel.text = poiAnnotation.subtitle;
    }

    
    if (indexPath.row == self.currentSelected) {
        cell.selectedImageView.hidden = NO;
    } else {
        cell.selectedImageView.hidden = YES;
    }

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.currentSelected=indexPath.row;
    LocationCell *cell=(LocationCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.selectedImageView.hidden=NO;
    
    [tableView reloadData];
    [_mxMapView setMapPanned:NO];
    RegionAnnotation *poiAnnotation = [self.dataSource safeObjectAtIndex:indexPath.row];
    if (poiAnnotation == nil) {
        return;
    }
    CLLocationCoordinate2D centerCoor=poiAnnotation.coordinate;
    [_mxMapView animateToCameraPosition:centerCoor];
    self.currentAddress=poiAnnotation.title;
    self.currentSubAddress = poiAnnotation.subtitle;
    self.selectPoiAnnotation = poiAnnotation;
    self.coordinate2D=centerCoor;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54.f;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *keyword = _searchBar.text;//searchBar.text;
    _searchBar.placeholder = keyword;
    
    [self searchPlaceWithKeyWord:keyword cityCode:self.currentCity];
}

#pragma mark - Init
- (UIView *)loadingView {
    if (!_loadingView) {
        _loadingView = [[UIView alloc] initWithFrame:self.tableView.bounds];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_moya3_nor"]];
        UILabel *tipLbl = [[UILabel alloc] init];
        tipLbl.font = [UIFont font14];
        tipLbl.textColor = [UIColor moDarkGray];
        tipLbl.text = MXLang(@"", @"正在加载....");
        
        [_loadingView addSubview:imgView];
        [_loadingView addSubview:tipLbl];
        
        [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_loadingView);
        }];
        
        [tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(_loadingView.mas_centerX);
            make.top.equalTo(imgView.mas_bottom).offset(5);
        }];
    }
    
    return _loadingView;
}

- (UISearchController *)searchController {
    if (!_searchController) {
        self.resultVC = [[LocationSearchVC alloc] init];
        self.resultVC.superVC = self;
        WeakSelf;
        self.resultVC.block = ^(id data) {
            weakSelf.searchController.active = NO;
        };
        
        _searchController = [[UISearchController alloc]initWithSearchResultsController:self.resultVC];
        _searchController.searchBar.delegate = self;
        _searchController.searchResultsUpdater = self.resultVC;
        //_searchController.delegate = self;
        _searchController.view.backgroundColor = [UIColor whiteColor];
        _searchController.dimsBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = YES;
        //[_searchController.searchBar sizeToFit];
        //_searchController.searchBar.tintColor = [UIColor blackColor];
        _searchController.searchBar.placeholder =  @"搜索";
        [_searchController.searchBar sizeToFit];
        UIOffset offset = {5.0,0};
        _searchController.searchBar.searchTextPositionAdjustment = offset;
        _searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
        _searchController.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;//关闭提示
        _searchController.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;//关闭自动首字母大写
    }
    return _searchController;
}
@end
