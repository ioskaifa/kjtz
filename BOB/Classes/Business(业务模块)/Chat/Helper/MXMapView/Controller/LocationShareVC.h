//
//  LocationShareVC.h
//  MapDemo
//
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXMapConfig.h"
#import "RegionAnnotation.h"

@protocol LocationShareVCDelegate;

@interface LocationShareVC : BaseViewController


// 地图初始化类型，默认是apple map
@property (nonatomic, assign) MXMapViewType mapViewType;
@property (nonatomic, assign) BOOL isNeedShowCurLocation;

@property (nonatomic, weak) id<LocationShareVCDelegate>delegate;

/////设置当前经纬度 special add by lhy 2015年08月31日
-(void)locateAssign:(CLLocationCoordinate2D)coordinate2D;
-(void)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end

@protocol LocationShareVCDelegate <NSObject>
@optional
-(void)sendCurrentLocation:(RegionAnnotation *)poiAnnotation;
-(void)sendCurrentLocation:(CLLocationCoordinate2D)coordinate2D andAddress:(NSString*)address;
-(void)sendCurrentLocation:(CLLocationCoordinate2D)coordinate2D andAddress:(NSString*)address andSubAddress:(NSString *)subAddress;
-(void)sendCurrentLocation:(CLLocationCoordinate2D)coordinate2D andAddress:(NSString*)address andSubAddress:(NSString *)subAddress screenShot:(UIImage *)screenShot;
@end
