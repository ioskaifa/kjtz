//
//  LocationSearchVC.h
//  BOB
//
//  Created by mac on 2019/12/16.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocationSearchVC : UIViewController<UISearchResultsUpdating>
// 当前城市名称
@property (nonatomic,strong) NSString *currentCity;

@property(nonatomic, weak)  UIViewController *superVC;

@property(nonatomic, copy) FinishedBlock block;

@end

NS_ASSUME_NONNULL_END
