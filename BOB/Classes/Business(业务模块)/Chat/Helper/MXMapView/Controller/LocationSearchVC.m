//
//  LocationSearchVC.m
//  BOB
//
//  Created by mac on 2019/12/16.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "LocationSearchVC.h"
#import "MXAddressManager.h"
#import "RegionAnnotation.h"
#import "SearchAutoPlacesModel.h"

@interface LocationSearchVC ()
@property (nonatomic,strong) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* searchResult;
@end

@implementation LocationSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];

    //self.navigationItem.titleView = self.searchView;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    self.searchResult = [NSMutableArray arrayWithCapacity:1];
    
    @weakify(self)
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.edges.equalTo(self.view);
    }];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    //self.vc = searchController;
    NSString *searchText = searchController.searchBar.text;
    NSLog(@"正在编辑过程中的改变%@",searchText);
    [_searchResult removeAllObjects];
    //[self filterFriendByKeyword:searchText];
    //[self searchPlaceWithKeyWord:searchText cityCode:self.currentCity];
    [self searchPlaceWithAutoComplete:searchText];
    [self.tableView reloadData];
    AdjustTableBehavior(self.tableView)
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

#pragma mark - Table view delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellId];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    }
    
    
    SearchAutoPlacesModel *model = self.searchResult[indexPath.row];
    cell.textLabel.text = model.name;
    cell.detailTextLabel.text = model.addressDesc;
    
    
    return cell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResult.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchAutoPlacesModel *searchObj = self.searchResult[indexPath.row];
    if ([searchObj isKindOfClass:[SearchAutoPlacesModel class]]) {
        SearchAutoPlacesModel *model = (SearchAutoPlacesModel*)searchObj;
        //[self searchPlaceWithKeyWord:model.reference cityCode:model.adcode];
        
        if(self.superVC && [self.superVC respondsToSelector:@selector(searchPlaceWithKeyWord:cityCode:)]) {
            [self.superVC performSelector:@selector(searchPlaceWithKeyWord:cityCode:) withObject:model.reference withObject:model.adcode];
            Block_Exec(self.block,nil);
        }
    }
}

- (void)searchPlaceWithAutoComplete:(NSString *)keyword {
    @weakify(self)
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [[MXAddressManager sharedInstance] searchTipsWithKeyword:keyword cityname:self.currentCity completion:^(NSMutableArray *array) {
        [NotifyHelper hideAllHUDsForView:self.view animated:YES];
        @strongify(self)
        if (array && array.count > 0) {
            [self.searchResult addObjectsFromArray:array];
            [self.tableView reloadData];
        }
    }];
}

// 根据关键字搜索
- (void)searchPlaceWithKeyWord:(NSString*)keyword cityCode:(NSString*)cityCode {
    //NSLog(@"### searchPlaceWithKeyWord %@",keyword);
    
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    __weak typeof(self) weakVC=self;
    // 高德POI搜索
    [[MXAddressManager sharedInstance] searchPOIWithKey:keyword adcode:cityCode completion:^(NSMutableArray *array) {
        [NotifyHelper hideAllHUDsForView:weakVC.view animated:YES];
        NSLog(@"@@@ searchPlaceWithKeyWord array:%@",array);
        if (array && array) {
            weakVC.searchResult=[NSMutableArray arrayWithArray:array];
            [weakVC.tableView reloadData];
            [weakVC tableView:weakVC.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
    }];
}

//消息列表
- (UITableView *)tableView {
    
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        _tableView.sectionIndexTrackingBackgroundColor=[UIColor clearColor];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.backgroundView = nil;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
    }
    return _tableView;
}
@end
