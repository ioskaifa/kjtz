//
//  LocationNavigateVC.m
//  MapDemo
//
//  Created by aken on 15/4/25.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "LocationNavigateVC.h"
#import "MXMapView.h"
#import "MXAddressManager.h"
#import "RegionAnnotation.h"
#import "MXMapViewHelper.h"

@interface LocationNavigateVC ()<MXMapViewDelegate,UIActionSheetDelegate>
{
    
    BOOL _completeLocate;//是否完成定位及周边搜索
}

@property (nonatomic,strong) MXMapView *mxMapView;

// 当前经纬度
@property (nonatomic) CLLocationCoordinate2D currentCoordinate2D;

// 位置信息
@property (nonatomic,strong) RegionAnnotation *regionInfos;

@end

@implementation LocationNavigateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    [MXAddressManager sharedInstance].mapViewType=self.mapViewType;
    
    if (!_showTitle) {
        [self setNavBarTitle:@"地图"];
    }
    
    [self initLayout];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initLayout {
    
    self.isShowBackButton=YES;
    
    _mxMapView=[[MXMapView alloc] initWithFrame:[UIScreen mainScreen].bounds withLocation:self.toCoordinate2D mapViewType:self.mapViewType];
    _mxMapView.delegate=self;
    [self.view addSubview:_mxMapView];
    
    [_mxMapView animateToCameraPosition:self.toCoordinate2D];

    self.regionInfos=[[RegionAnnotation alloc] init];
    // 测试数据
    self.regionInfos.title=self.city;
    self.regionInfos.subtitle=self.address;
    self.regionInfos.coordinate=self.toCoordinate2D;//CLLocationCoordinate2DMake(latitude, longitude);
    
    [_mxMapView addAnnotationView:self.regionInfos];
    
}

-(void)doAcSheet{
    NSArray *appListArr = [MXMapViewHelper checkHasOwnMapApp];
    
    UIActionSheet *sheet;
    if ([appListArr count] == 2) {
        sheet = [[UIActionSheet alloc] initWithTitle:MXLang(@"Talk_controller_locationVC_title_9", @"请选择地图") delegate:self cancelButtonTitle:MXLang(@"Public_Cancel", @"取消") destructiveButtonTitle:nil otherButtonTitles:appListArr[0],appListArr[1], nil];
    }else if ([appListArr count] == 3){
        sheet = [[UIActionSheet alloc] initWithTitle:MXLang(@"Talk_controller_locationVC_title_9", @"请选择地图") delegate:self cancelButtonTitle:MXLang(@"Public_Cancel", @"取消")  destructiveButtonTitle:nil otherButtonTitles:appListArr[0],appListArr[1],appListArr[2], nil];
    }else if ([appListArr count] == 4){
        sheet = [[UIActionSheet alloc] initWithTitle:MXLang(@"Talk_controller_locationVC_title_9", @"请选择地图") delegate:self cancelButtonTitle:MXLang(@"Public_Cancel", @"取消")  destructiveButtonTitle:nil otherButtonTitles:appListArr[0],appListArr[1],appListArr[2],appListArr[3], nil];
    }else{
        sheet = [[UIActionSheet alloc] initWithTitle:MXLang(@"Talk_controller_locationVC_title_9", @"请选择地图") delegate:self cancelButtonTitle:MXLang(@"Public_Cancel", @"取消") destructiveButtonTitle:nil otherButtonTitles:appListArr[0], nil];
    }
    
    sheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [sheet showInView:self.view];
}

#pragma mark - MXMapView delegate
- (void)mxmapViewDidPressNavigation:(CLLocationCoordinate2D)coordinate {
    
    
    self.toCoordinate2D=coordinate;
    
    [self doAcSheet];
}

- (void)mapViewCoordinate2D:(CLLocationCoordinate2D)coordinate2D regionDidChangeAnimated:(BOOL)animated {
    
    MLog(@"regionDidChangeAnimated");
}

- (void)mapView:(UIView *)mapView didFinishUpdateUserLocation:(CLLocationCoordinate2D)coordinate {
    
    //引入_completeLocate是为控制该方法只走一次
    if (!_completeLocate) {
        self.currentCoordinate2D=coordinate;
        
        MLog(@"mkmapview_didUpdateUserLocation:%f,%f",coordinate.latitude,coordinate.longitude);
//        
//        [_mxMapView animateToCameraPosition:self.toCoordinate2D];
//        
//        // 测试数据
//        self.regionInfos.title=self.city;
//        self.regionInfos.subtitle=self.address;
//        self.regionInfos.coordinate=self.toCoordinate2D;//CLLocationCoordinate2DMake(latitude, longitude);
//        
//        [_mxMapView addAnnotationView:self.regionInfos];
        _completeLocate = YES;
    }
    
}

#pragma mark - actionSheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *btnTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
//    if (buttonIndex == 0) {
////        if (!([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue]>=6)) {//ios6 调用goole网页地图
////            NSString *urlString = [[NSString alloc]
////                                   initWithFormat:@"http://maps.google.com/maps?saddr=&daddr=%.8f,%.8f&dirfl=d",self.toCoordinate2D.latitude,self.toCoordinate2D.longitude];
////
////            NSURL *aURL = [NSURL URLWithString:urlString];
////            [[UIApplication sharedApplication] openURL:aURL];
////
////        }else{
//            //ios7 跳转apple map
//
//            MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
//            MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:self.toCoordinate2D addressDictionary:nil]];
//            toLocation.name=self.address;
//
//            [MKMapItem openMapsWithItems:[NSArray arrayWithObjects:currentLocation, toLocation, nil] launchOptions:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeDriving, [NSNumber numberWithBool:YES], nil] forKeys:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeKey, MKLaunchOptionsShowsTrafficKey, nil]]];
////        }
//    }
    // 这样写不好，暂时这样写
    if ([btnTitle isEqualToString:kNavigationGoogleMap]) {
        
        
        CLLocationCoordinate2D to=self.toCoordinate2D;//CLLocationCoordinate2DMake(31.23733484, 121.50142656);
        
        NSString *urlStr = [[NSString stringWithFormat:@"comgooglemaps://?saddr=%.8f,%.8f&daddr=%.8f,%.8f&directionsmode=transit",self.currentCoordinate2D.latitude,self.currentCoordinate2D.longitude,to.latitude,to.longitude] stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
        
    }else if ([btnTitle isEqualToString:kNavigationAppleMap]){
        
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"iosamap://navi?sourceApplication=%@&sid=BGVIS1&did=BGVIS2&dlat=%lf&dlon=%lf&dev=0&m=0&t=%@",@"--",self.currentCoordinate2D.latitude,self.currentCoordinate2D.longitude,self.address]];
//        [[UIApplication sharedApplication] openURL:url];
        
        NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"iosamap://navi?sourceApplication=broker&backScheme=openbroker2&poiname=%@&poiid=BGVIS&lat=%.8f&lon=%.8f&dev=1&style=2",self.address,self.currentCoordinate2D.latitude,self.currentCoordinate2D.longitude] stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
        [[UIApplication sharedApplication] openURL:url];
        
    } else if ([btnTitle isEqualToString:kNavigationMapOfApple]) {
        // 苹果
        NSString *url = [[NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",self.currentCoordinate2D.latitude,self.currentCoordinate2D.longitude,self.toCoordinate2D.latitude, self.toCoordinate2D.longitude] stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    } else if ([btnTitle isEqualToString:kNavigationBaiduMap]){
        
        //        double bdNowLat,bdNowLon;
        //        bd_encrypt(self.nowCoords.latitude, self.nowCoords.longitude, &bdNowLat, &bdNowLon);
        //
        CLLocationCoordinate2D to=self.toCoordinate2D;
                NSString *stringURL = [[NSString stringWithFormat:@"baidumap://map/direction?origin=%.8f,%.8f&destination=%.8f,%.8f&mode=driving",self.currentCoordinate2D.latitude,self.currentCoordinate2D.longitude,to.latitude,to.longitude] stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet];
                NSURL *url = [NSURL URLWithString:stringURL];
                [[UIApplication sharedApplication] openURL:url];
    }
}

@end
