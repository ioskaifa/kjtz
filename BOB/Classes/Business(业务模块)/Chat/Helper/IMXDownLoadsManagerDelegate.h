//
//  IMXDownLoadsManagerDelegate.h
//  MoPal_Developer
//
//  Created by aken on 16/1/5.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MessageModel;

@protocol IMXDownLoadsManagerDelegate <NSObject>

@required

/*!
 @class
 @brief 聊天相关的下载管理类
 */
- (void)downloadMessageAttachmentSuccess:(MessageModel*)model;

@end
