//
//  ChatVoiceManage.m
//  BOB
//
//  Created by mac on 2020/7/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChatVoiceManage.h"
#import "ChatSendHelper.h"

@interface ChatVoiceManage()

@property (nonatomic, strong) MessageModel *currentObj;

@end

@implementation ChatVoiceManage

+ (instancetype)shareInstance {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.isMeeting = NO;
        self.msgMDic = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)setIsMeeting:(BOOL)isMeeting {
    _isMeeting = isMeeting;
    if (!isMeeting) {
        _isOtherEnterRoom = NO;
        _isSelfEnterRoom = NO;
    }
}

- (void)setIsSelfEnterRoom:(BOOL)isSelfEnterRoom {
    _isSelfEnterRoom = isSelfEnterRoom;
    [self showMeetingRoom];
}

- (void)setIsOtherEnterRoom:(BOOL)isOtherEnterRoom {
    _isOtherEnterRoom = isOtherEnterRoom;
    [self showMeetingRoom];
}

- (void)showMeetingRoom {
    if (!self.isSelfEnterRoom || !self.isOtherEnterRoom) {
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.chatVC beginMeeting];
    });
}

- (void)didReceiveMessage:(MessageModel *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![message isKindOfClass:MessageModel.class]) {
            return;
        }
        ///如果不是语音类型的消息则不处理
        if (message.subtype != kMXMessageTypeVoiceChat) {
            return;
        }
        NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:message.body];
        kMXVoiceChatType voiceChatType = [bodyDic[@"attr3"] integerValue];
        self.currentObj = message;
        switch (voiceChatType) {
            case kMXVoiceChatTypeAsk: {
                [self askCall:message];
                break;
            }
            case kMXVoiceChatTypeBusy: {
                [self busyCall:message];
                break;
            }
            case kMXVoiceChatTypeAccept: {
                [self acceptCall:message];
                break;
            }
            case kMXVoiceChatTypeCancel: {
                [self cancelCall:message];
                break;
            }
            case kMXVoiceChatTypeEnd: {
                [self endCall:message];
                break;
            }
            case kMXVoiceChatTypeOverTime: {
                [self overTimeCall:message];
                break;
            }
            case kMXVoiceChatTypeEnterRoom: {
                [self otherEnterRoom:message];
                break;
            }
            default:
                break;
        }
    });
}

///对方已挂断
- (void)endCall:(MessageModel *)obj {
    self.isMeeting = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kMXVoiceMsgNotifaction object:obj];
}

///对方已经接受
- (void)acceptCall:(MessageModel *)obj {
    [[NSNotificationCenter defaultCenter] postNotificationName:kMXVoiceMsgNotifaction object:obj];
}

- (void)otherEnterRoom:(MessageModel *)obj {
    self.isOtherEnterRoom = YES;
}

- (void)overTimeCall:(MessageModel *)obj {
    self.isMeeting = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kMXVoiceMsgNotifaction object:obj];
}

- (void)busyCall:(MessageModel *)obj {
    [NotifyHelper showMessageWithMakeText:@"对方忙"];
    self.isMeeting = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kMXVoiceMsgNotifaction object:obj];
}

///对方已拒绝
- (void)cancelCall:(MessageModel *)obj {
    self.isMeeting = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kMXVoiceMsgNotifaction object:obj];
}

- (void)askCall:(MessageModel *)obj {
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:obj.body];
    if (bodyDic[@"attr2"]) {
        self.roomID = [bodyDic[@"attr2"] description];
    }
    if (self.isMeeting) {
        //如果在语音中,提示正在忙
        NSString *roomID = self.roomID;
        self.roomID = [bodyDic[@"attr2"] description];
        [ChatSendHelper sendVoiceCallMessage:@"对方忙"
                                  toUsername:obj.fromID
                                 messageType:obj.chatType
                              subMessageType:kMXVoiceChatTypeBusy];
        self.roomID = roomID;
        return;
    }
    self.roomID = [bodyDic[@"attr2"] description];
    self.fromID = obj.fromID;
    self.chatType = obj.chatType;
    self.isMeeting = YES;
    self.isMy = NO;
    
    [self askCallRoom:AskCallTypeGet
             toUserId:self.fromID?:@""
             chatType:self.chatType];
    
}

- (void)askCallRoom:(AskCallType)askType
           toUserId:(NSString *)userId
           chatType:(EMConversationType)type {
    AskCallVC *vc = [AskCallVC new];
    vc.askType = askType;
    vc.toUserId = userId;
    vc.messageType = type;
    UINavigationController *nav = [[MXRouter sharedInstance] getTopViewController].navigationController;
    [nav pushViewController:vc animated:YES];
    self.askVC = vc;
}

- (void)acceptEnterChatRoom {
    ChatAVCallVC *vc = [ChatAVCallVC new];
    vc.isGroup = NO;
    vc.view.frame = [UIScreen mainScreen].bounds;
    [g_window addSubview:vc.view];
    //持有VC 不然会被销毁
    self.chatVC = vc;
    //先隐藏 等双方都进入加入聊天室了 再显示，防止进入房间,长时间不能聊天的问题。
    self.chatVC.view.hidden = YES;
}

- (MessageModel *)formatMsg:(MessageModel *)msg {
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:msg.body];
    NSString *roomID = bodyDic[@"attr2"];
    if ([StringUtil isEmpty:roomID]) {
        return msg;
    }
    kMXVoiceChatType chatType = [bodyDic[@"attr3"] integerValue];
    
    NSString *content = bodyDic[@"attr1"];
    switch (chatType) {
        case kMXVoiceChatTypeBusy:{
            content = self.isMy ?@"发起了语音通话":@"对方忙";
            break;
        }
        case kMXVoiceChatTypeCancel:{
            content = self.isMy ?@"已取消":@"对方已取消";
            break;
        }
        case kMXVoiceChatTypeEnd:{
            content = content;
            break;
        }
        case kMXVoiceChatTypeOverTime:{
            content = self.isMy ?@"对方无应答":@"未接听";
            break;
        }
        default:
            break;
    }
    msg.content = content;
    bodyDic = @{@"attr1":content,
                @"attr2":roomID,
                @"attr3":@(chatType)};
    msg.body = [MXJsonParser dictionaryToJsonString:bodyDic];
    if (self.isMy) {
        msg.msg_direction = 1;
        if (![msg.fromID isEqualToString:UDetail.user.user_id]) {
            msg.toID = msg.fromID;
            msg.fromID = UDetail.user.user_id;
        }
    } else {
        msg.msg_direction = 0;
        if ([msg.fromID isEqualToString:UDetail.user.user_id]) {
            msg.fromID = msg.toID;
            msg.toID = UDetail.user.user_id;
        }
    }
    return msg;
}

+ (void)sendAsk:(NSString *)toUserId chatType:(EMConversationType)chatType {
    if ([StringUtil isEmpty:[ChatVoiceManage shareInstance].roomID]) {
        [ChatVoiceManage shareInstance].roomID = UDetail.user.roomNum;
    }
    [ChatVoiceManage shareInstance].isMy = YES;
    [ChatVoiceManage shareInstance].isMeeting = YES;
    [ChatVoiceManage shareInstance].fromID = toUserId;
    [ChatVoiceManage shareInstance].chatType = chatType;
    [ChatSendHelper sendVoiceCallMessage:@"邀请您语音通话"
                              toUsername:[ChatVoiceManage shareInstance].fromID
                             messageType:[ChatVoiceManage shareInstance].chatType
                          subMessageType:kMXVoiceChatTypeAsk];
}

+ (void)sendAccept {
    [ChatSendHelper sendVoiceCallMessage:@"已接受您语音通话"
                              toUsername:[ChatVoiceManage shareInstance].fromID
                             messageType:[ChatVoiceManage shareInstance].chatType
                          subMessageType:kMXVoiceChatTypeAccept];
}

+ (void)sendCancel {
    [ChatSendHelper sendVoiceCallMessage:@"对方已拒绝"
                              toUsername:[ChatVoiceManage shareInstance].fromID
                             messageType:[ChatVoiceManage shareInstance].chatType
                          subMessageType:kMXVoiceChatTypeCancel];
}

+ (void)sendEndWithTime:(NSString *)time {
    if (![ChatVoiceManage shareInstance].isMeeting) {
        return;
    }
    [ChatVoiceManage shareInstance].isMeeting = NO;
    [ChatVoiceManage shareInstance].chatVC = nil;
    [ChatSendHelper sendVoiceCallMessage:time
                              toUsername:[ChatVoiceManage shareInstance].fromID
                             messageType:[ChatVoiceManage shareInstance].chatType
                          subMessageType:kMXVoiceChatTypeEnd];
    [ChatVoiceManage shareInstance].roomID = @"";
}

+ (void)sendOverTime {
    [ChatSendHelper sendVoiceCallMessage:@"对方无应答"
                              toUsername:[ChatVoiceManage shareInstance].fromID
                             messageType:[ChatVoiceManage shareInstance].chatType
                          subMessageType:kMXVoiceChatTypeOverTime];
}

+ (void)enterMeetingRoom {
    [ChatSendHelper sendVoiceCallMessage:@"已进入聊天室"
                              toUsername:[ChatVoiceManage shareInstance].fromID
                             messageType:[ChatVoiceManage shareInstance].chatType
                          subMessageType:kMXVoiceChatTypeEnterRoom];
}

@end
