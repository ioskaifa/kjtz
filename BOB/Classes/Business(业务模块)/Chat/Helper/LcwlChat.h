//
//  LcwlChat.h
//  Lcwl
//
//  Created by mac on 2018/11/23.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FriendModel.h"
//#import "UserModel.h"
#import "IMXChatManager.h"
#import "IMXDeviceManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface LcwlChat : NSObject

@property (nonatomic, strong) UserModel* user;

/*!
 @property
 @brief 聊天管理器, 获取该对象后, 可以做登录、聊天、加好友等操作
 */
@property (nonatomic,readonly,strong) id<IMXChatManager> chatManager;

@property (nonatomic,readonly,strong) id<IMXDeviceManager> deviceManager;

+ (instancetype)shareInstance;

-(void)chatLogin:(UserModel*)user;

@end

NS_ASSUME_NONNULL_END
