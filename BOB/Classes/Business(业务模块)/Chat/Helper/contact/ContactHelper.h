//
//  ContactHelper.h
//  MoPal_Developer
//
//  Created by aken on 15/12/16.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
@interface ContactHelper : NSObject


@property (nonatomic, strong) NSString *string;
@property (nonatomic, strong) NSString *pinYin;
@property (nonatomic, strong) NSMutableArray *phones;
//通讯录数据
@property (nonatomic, strong) NSMutableArray* contactArray ;
+ (instancetype)shareInstance;

-(BOOL)authorizationed;

-(NSMutableArray *)getDataFromContact;

+(NSMutableArray *)letterSortArray:(NSArray*)stringArr;

+ (NSMutableArray *)indexFriendVCModel:(NSArray *)datasource;

+ (NSMutableArray *)indexMobileContactVCModel:(NSArray *)datasource ;

+(void)moblieImportBook;











//调用手机短信
-(void)sendPhoneSMS:(NSString *) sms vc:(UIViewController*)vc phone:(NSString *)phone;
@end
