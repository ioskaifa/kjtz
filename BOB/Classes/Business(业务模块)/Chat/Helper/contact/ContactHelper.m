//
//  ContactHelper.m
//  MoPal_Developer
//
//  Created by aken on 15/12/16.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "ContactHelper.h"
#import "FriendModel.h"

#import <MessageUI/MessageUI.h>
#import "FriendsManager.h"
@interface ContactHelper ()

@property (nonatomic) ABAddressBookRef addressBook ;

@property (nonatomic, strong) UIViewController* vc ;

@end

@implementation ContactHelper

+ (instancetype)shareInstance {
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}
-(id)init
{
    if (self=[super init]) {
        _contactArray  = [[NSMutableArray alloc]initWithCapacity:10];
        
    }
    return self;
    
}

+(NSMutableArray*)letterSortArray:(NSArray*)stringArr {
    
    NSMutableArray *tempArray = [self sortChineseArrar:stringArr];
    NSMutableArray *letterResult = [NSMutableArray array];
//    NSMutableArray *item = [NSMutableArray array];
//    NSString *tempString;
    //拼音分组
    for (NSString* object in tempArray) {
        
        if ([StringUtil isEmpty:((ContactHelper*)object).pinYin]) {
            continue;
        }

        NSString *string = ((ContactHelper*)object).string;
        NSMutableArray *array = ((ContactHelper*)object).phones;
        
        FriendModel *model=[[FriendModel alloc] init];
        model.addressListName = string;
        model.phones = array;

        [letterResult addObject:model];
    }
    
    return letterResult;
}

//返回排序好的字符拼音

+(NSMutableArray*)sortChineseArrar:(NSArray*)stringArr {
    
    //获取字符串中文字的拼音首字母并与字符串共同存放
    NSMutableArray *chineseStringsArray=[NSMutableArray array];
    
    for(int i=0;i<[stringArr count];i++) {
        ContactHelper *chineseString = [[ContactHelper alloc]init];
        id model=stringArr[i];
        if ([model isKindOfClass:[FriendModel class]]) {
            chineseString.string = ((FriendModel*)model).addressListName;
            chineseString.phones=((FriendModel*)model).phones;
        }
        
//            chineseString.string = [NSString stringWithString:[stringArr objectAtIndex:i]];
//        }
        
        if(chineseString.string == nil){
            chineseString.string = @"";
            continue;
        }
        //去除两端空格和回车
        chineseString.string  = [chineseString.string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        chineseString.string = [self removeSpecialCharacter:chineseString.string];
        
        
        //判断首字符是否为字母
        NSString *regex = @"[A-Za-z]+";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        NSString *initialStr = [chineseString.string length]?[chineseString.string substringToIndex:1]:@"";
        if ([predicate evaluateWithObject:initialStr]) {
            MLog(@"chineseString.string== %@",chineseString.string);
            //首字母大写
            chineseString.pinYin = [chineseString.string capitalizedString] ;
        }else{
            if(![chineseString.string isEqualToString:@""]){
                NSString *pinYinResult = [NSString string];
                for(int j=0;j<chineseString.string.length;j++){
                    NSString *singlePinyinLetter = [StringUtil pinYinFirstLetterToUppercase:chineseString.string];
                    
                    pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
                }
                chineseString.pinYin = pinYinResult;
            }else{
                chineseString.pinYin = @"";
            }
        }
        [chineseStringsArray addObject:chineseString];
        
    }
    //按照拼音首字母对这些Strings进行排序
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    MLog(@"-----------------------------");
    return chineseStringsArray;
}

//过滤指定字符串里面的指定字符根据自己的需要添加 过滤特殊字符
+(NSString*)removeSpecialCharacter: (NSString *)str {
    NSRange urgentRange = [str rangeOfCharacterFromSet: [NSCharacterSet characterSetWithCharactersInString: @",.？、 ~￥#&<>《》()[]{}【】^@/￡¤|§¨「」『』￠￢￣~@#&*（）——+|《》$_€"]];
    if (urgentRange.location != NSNotFound)
    {
        return [self removeSpecialCharacter:[str stringByReplacingCharactersInRange:urgentRange withString:@""]];
    }
    return str;
}


+ (NSMutableArray *)indexFriendVCModel:(NSArray *)datasource {
    NSMutableArray *tmpArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < ALPHA2.length; i++) {
        [tmpArray addObject:[NSMutableArray array]];
    }
    
    NSString *tempSectionName = nil;
    
    for (int i=0; i<datasource.count; i++) {
        FriendModel* model = datasource[i];
        NSString *name = model.name;
        if ([StringUtil isEmpty:name]) {
            continue;
        }
        tempSectionName = [StringUtil pinYinFirstLetterToUppercase:name];
        NSUInteger firstLetter = [ALPHA2 rangeOfString:[tempSectionName substringToIndex:1]].location;
        if (firstLetter != NSNotFound){
            [[tmpArray safeObjectAtIndex:firstLetter] addObject:model];
        }
    }
    
    return tmpArray;
}
+ (NSMutableArray *)indexMobileContactVCModel:(NSArray *)datasource {
    NSMutableArray *tmpArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < ALPHA3.length; i++) {
        [tmpArray addObject:[NSMutableArray array]];
    }
    NSString *tempSectionName = nil;
    for (int i=0; i<datasource.count; i++) {
        FriendModel* model = datasource[i];
        if (![StringUtil isEmpty:model.userid]) {
            NSString *name = model.name;
            tempSectionName = [StringUtil pinYinFirstLetterToUppercase:name];
            NSUInteger firstLetter = [ALPHA3 rangeOfString:[tempSectionName substringToIndex:1]].location;
            if (firstLetter != NSNotFound){
                [[tmpArray safeObjectAtIndex:firstLetter] addObject:model];
            }
        }else{
             [[tmpArray safeObjectAtIndex:ALPHA3.length-1] addObject:model];
        }
    }
    
    return tmpArray;
}

-(BOOL)authorizationed{
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
        _addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        //等待同意后向下执行
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);//发出访问通讯录的请求
        ABAddressBookRequestAccessWithCompletion(_addressBook, ^(bool granted, CFErrorRef error)
                                                 {
                                                     dispatch_semaphore_signal(sema);
                                                 });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        ABAuthorizationStatus statu=ABAddressBookGetAuthorizationStatus();
        if (statu == kABAuthorizationStatusDenied) {
            return NO;
        }
    }else{
         self.addressBook = ABAddressBookCreate();
    }
    return YES;
}

-(NSMutableArray* )getDataFromContact{
    if (![self authorizationed]) {
        return _contactArray;
    }
    if (_contactArray.count == 0) {
        CFArrayRef records;
        if (_addressBook) {
            // 获取通讯录中全部联系人
            records = ABAddressBookCopyArrayOfAllPeople(_addressBook);
            NSMutableArray *tempContactArray=[NSMutableArray array];
            for (int i=0; i<CFArrayGetCount(records); i++) {
                ABRecordRef record = CFArrayGetValueAtIndex(records, i);
                CFTypeRef items = ABRecordCopyValue(record, kABPersonPhoneProperty);
                NSString *personName = (__bridge NSString*)ABRecordCopyValue(record, kABPersonFirstNameProperty);
                NSString *lastname = (__bridge NSString*)ABRecordCopyValue(record, kABPersonLastNameProperty);
                NSString *name = [NSString stringWithFormat:@"%@%@",personName?:@"",lastname?:@""];
                NSMutableArray *ary = [NSMutableArray array];
                FriendModel *friend = [[FriendModel alloc]init];
                CFArrayRef phoneNums = ABMultiValueCopyArrayOfAllValues(items);
                if (phoneNums) {
                    for (int j=0; j<CFArrayGetCount(phoneNums); j++) {
                        NSString *phone = (NSString*)CFArrayGetValueAtIndex(phoneNums, j);
                        NSString *aPhone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];//去掉“-”
                        NSString *thePhone = [aPhone stringByReplacingOccurrencesOfString:@" " withString:@""];//去掉“ ”
                        [ary safeAddObj:thePhone];
                    }
                    friend.addressListName = name;
                    friend.phones = ary;
                    friend.phone = ary[0];
                    if (friend.phone.length < 11) {
                        continue;
                    }
                    [tempContactArray safeAddObj:friend];
                }else{}
            }
            [_contactArray addObjectsFromArray:tempContactArray];
            CFRelease(_addressBook);
        }
    }
    return _contactArray;
}


-(void)sendPhoneSMS:(NSString *) sms vc:(UIViewController*)vc phone:(NSString *)phone{
    self.vc = vc;
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *smsController = [[MFMessageComposeViewController alloc] init];
        smsController.messageComposeDelegate = self;
        [smsController setBody:sms];
        smsController.recipients = [NSArray arrayWithObjects:phone, nil];
        //        [self.viewController presentModalViewController:smsController animated:YES];
        [vc presentViewController:smsController animated:YES completion:nil];
    }
    else
    {
        [NotifyHelper showMessageWithMakeText:@"该设备不支持发送短信"];
    }
}

//记得实现回调
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    //比如这里只要求让短信界面消失，不做其他处理
    //    [self.viewController dismissModalViewControllerAnimated:YES];
    [controller dismissViewControllerAnimated:YES completion:nil];
}


+(void)moblieImportBook{
    if ([[ContactHelper shareInstance] authorizationed]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSArray* contactArray = [self contactPhoneNoSet];
            int maxNum = 200;
            if (contactArray.count > maxNum) {
                NSInteger times = contactArray.count/maxNum+(contactArray.count%maxNum==0?0:1);
                for ( int i=0; i<times; i++) {
                    int fromIndex = i*maxNum;
                    NSInteger len = fromIndex+maxNum>contactArray.count?contactArray.count-fromIndex:maxNum;
                    NSRange range = NSMakeRange(fromIndex, len);
                    NSArray* tmpArray = [contactArray subarrayWithRange:range];
                    [FriendsManager moblieImportBook:tmpArray completion:^(id array, NSString *error) {
                        if (array && [array isKindOfClass:[NSArray class]]) {
                            NSMutableArray* tmpArray = (NSMutableArray* )array;
                            for (int i=0; i<[ContactHelper shareInstance].contactArray.count; i++) {
                                FriendModel* contact = [ContactHelper shareInstance].contactArray[i];
                                for (int j=0; j<tmpArray.count; j++) {
                                    FriendModel* friend = tmpArray[j];
                                    if ([contact.phone isEqualToString:friend.phone]) {
                                        contact.name = friend.name;
                                        contact.userid = friend.userid;
                                        contact.followState = friend.followState;
                                        contact.avatar = friend.avatar;
                                    }
                                }
                            }
                        }
                    }];
                }
            }else{
                [FriendsManager moblieImportBook:[self contactPhoneNoSet] completion:^(id array, NSString *error) {
                    if (array && [array isKindOfClass:[NSArray class]]) {
                        NSMutableArray* tmpArray = (NSMutableArray* )array;
                        for (int i=0; i<[ContactHelper shareInstance].contactArray.count; i++) {
                            FriendModel* contact = [ContactHelper shareInstance].contactArray[i];
                            for (int j=0; j<tmpArray.count; j++) {
                                FriendModel* friend = tmpArray[j];
                                if ([contact.phone isEqualToString:friend.phone]) {
                                    contact.name = friend.name;
                                    contact.userid = friend.userid;
                                    contact.followState = friend.followState;
                                    contact.avatar = friend.avatar;
                                }
                            }
                        }
                    }
                }];
            }
        });
    }else{
        
    }
}

+(NSMutableArray* )contactPhoneNoSet{
    NSMutableArray* arr = [[NSMutableArray alloc]initWithCapacity:10];
    [[ContactHelper shareInstance].contactArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        FriendModel *model = (FriendModel*)obj;
        [arr addObject:model.phone];
    }];
    return arr;
}
@end
