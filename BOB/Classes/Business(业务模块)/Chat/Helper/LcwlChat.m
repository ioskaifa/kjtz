//
//  LcwlChat.m
//  Lcwl
//
//  Created by mac on 2018/11/23.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "LcwlChat.h"
#import "FriendModel.h"
#import "MXChatManager.h"
#import "MXDeviceManager.h"
#import "MXChatDBUtil.h"
#import "LcwlChatHelper.h"
#import "SRWebSocketManager.h"
#import "TalkManager.h"
#import "MXConversation.h"

@interface  LcwlChat()
//@property (nonatomic) Reachability *hostReachability;
@end

@implementation LcwlChat
+ (instancetype)shareInstance {
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}

//-(id)init
//{
//    if (self=[super init]) {
//        [self initReachability];
//    }
//    return self;
//}


-(id)chatManager{
    return [MXChatManager sharedInstance];
}

-(id)deviceManager{
    return [MXDeviceManager sharedInstance];
}

-(void)chatLogin:(UserModel*)user{
//    if (!user.is_chat) {
//        return;
//    }
    if (user) {
        _user = user;
        //创建数据库
        [[MXChatDBUtil sharedDataBase]initChatConfig:user.chatUser_id];
        //加载数据库中的所有表
        [self.chatManager loadAllConfigFromDatabase] ;
        [self.chatManager login:_user.chatUser_id];
        [self initDefaultConversation];
      
        NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithCapacity:0];
        NSInteger nums = [[LcwlChat shareInstance].chatManager getAllUnReadNums];
        [userInfo setValue:[NSNumber numberWithInteger:nums] forKey:@"nums"];
        [userInfo setValue:[NSNumber numberWithInteger:3] forKey:@"tabBarIndex"];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"redBadgeCountChanged" object:nil userInfo:userInfo];
        [[NSNotificationCenter defaultCenter]postNotificationName:kRefreshChatList object:nil userInfo:userInfo];
    }
}

-(void)initDefaultConversation{
    
//    FriendModel* model1 = [TalkManager hudongtongzhi];
//    FriendModel* tmp1 = [[LcwlChat shareInstance].chatManager loadFriendByChatId:model1.userid];
//    //好友信息以后由增量更新来完成
//    if (tmp1 == nil) {
//        [[LcwlChat shareInstance].chatManager insertFriends:@[model1]];
//        MXConversation* con = [[MXConversation alloc] init];
//        con.chat_id = model1.userid;
//        con.conversationType = eConversationTypeHDTZChat;
//        con.chat_id = model1.userid;
//        [[LcwlChat shareInstance].chatManager createConversationObject:con];
//    }
//    FriendModel* model = [TalkManager newFriend];
//    FriendModel* tmp = [[LcwlChat shareInstance].chatManager loadFriendByChatId:model.userid];
//    //好友信息以后由增量更新来完成
//    if (tmp == nil) {
//        [[LcwlChat shareInstance].chatManager insertFriends:@[model]];
//        MXConversation* con = [[MXConversation alloc] init];
//        con.chat_id = model.userid;
//        con.conversationType = eConversationTypeXDPYChat;
//        con.chat_id = model.userid;
//        [[LcwlChat shareInstance].chatManager createConversationObject:con];
//    }
}
@end
