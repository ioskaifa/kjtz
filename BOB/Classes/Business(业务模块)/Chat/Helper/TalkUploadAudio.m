//
//  TalkUploadAudio.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/4/1.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "TalkUploadAudio.h"

@implementation TalkUploadAudio

- (AFConstructingBlock)constructingBodyBlock {
    return ^(id<AFMultipartFormData> formData) {
        NSData *data=[NSData dataWithContentsOfFile:[self.paramDictionary objectForKey:@"file"]];
        NSString *name = @"amr";
        NSString *formKey = @"uploadFile";
        NSString *type = @"audio/amr";
        [formData appendPartWithFileData:data name:formKey fileName:name mimeType:type];
    };
}

- (CGFloat)requestTimeoutInterval {
    return 120;
}

@end
