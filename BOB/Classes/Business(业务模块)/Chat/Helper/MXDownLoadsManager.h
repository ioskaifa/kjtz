//
//  MXDownLoadsManager.h
//  MoPal_Developer
//
//  聊天相关的下载管理类
//  Created by aken on 16/1/5.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMXDownLoadsManagerDelegate.h"

typedef void(^MXUploadMessageAttachment)(BOOL success);

/*!
 @class
 @brief 聊天相关的下载管理类
 */
@interface MXDownLoadsManager : NSObject

/*!
 @method
 @brief 聊天相关的下载管理类的单实例
 @result MXDownLoadsManager实例对象
 */
+ (MXDownLoadsManager *)sharedInstance;

/*!
 @property
 @brief 是否有下载
 */
@property (nonatomic, strong, readonly) NSMutableDictionary *successBlocksDictForDownload;

/*!
 @method
 @brief 批量语音下载
 @param messages 下载数据
 @param completion 回调代理
 */
- (void)downloadMessageAttachments:(NSArray *)messages
                        completion:(id<IMXDownLoadsManagerDelegate>)completion;

/*!
 @method
 @brief 单个语音下载
 @param model 下载数据
 @param completion 回调代理
 */
- (void)downloadMessageAttachmentWithMessage:(MessageModel *)model
                                  completion:(id<IMXDownLoadsManagerDelegate>)completion;

/*!
 @method
 @brief 图片上传
 @param msg   图片对象
 @param completion 回调代理
 @param progressBlock   上传进度回调代理
 */
- (void)uploadAttachmentForImageWithMessage:(MessageModel*)msg
                                 completion:(MXUploadMessageAttachment)completion
                                   progress:(void (^)(float,NSString*))progressBlock;

/*!
 @method
 @brief 语音上传
 @param msg   图片对象
 @param completion 回调代理
 @param progress   上传进度回调代理
 */
- (void)uploadAttachmentForAudioWithMessage:(MessageModel*)message
                                 completion:(MXUploadMessageAttachment)completion;

@end
