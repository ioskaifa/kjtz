//
//  IMXDeviceManagerMediaDelegate.h
//  MoPal_Developer
//
//  Created by aken on 15/3/24.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IMXDeviceManagerMediaDelegate <NSObject>

@optional

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag;

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag;

@end