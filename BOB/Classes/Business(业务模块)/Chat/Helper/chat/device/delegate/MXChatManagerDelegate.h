//
//  MXChatManagerLoginDelegate.h
//  Moxian
//  @abstract 关于ChatManager的异步回调内部协议
//  Created by 王 刚 on 14/12/4.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXChatManagerLoginDelegate.h"
#import "MXChatManagerChatDelegate.h"
#import "MXChatManagerUtilDelegate.h"
/*!
 @protocol
 @brief 本协议包括：收发消息, 登陆注销, 加密解密, 媒体操作的回调等其它操作
 @discussion
 */
@protocol MXChatManagerDelegate <MXChatManagerLoginDelegate,
                                 MXChatManagerChatDelegate,
                                 MXChatManagerUtilDelegate
                                >

@optional

@end