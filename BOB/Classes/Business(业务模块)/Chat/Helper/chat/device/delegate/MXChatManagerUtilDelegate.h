//
//  MXChatManagerUtilDelegate.h
//  Moxian
//
//  @abstract 此协议提供了一些实用工具类的回调协议
//  Created by litiankun on 14/12/10.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    eMXConnectionConnected,   //连接成功
    eMXConnectionDisconnected,//未连接
}MXConnectionState;

@protocol MXChatManagerUtilDelegate <NSObject>

@optional
/*!
 @method
 @brief SDK连接服务器的状态变化时的回调
 @discussion 有以下几种情况, 会引起该方法的调用:
 1. 登录成功后, 手机无法上网时, 会调用该回调
 2. 登录成功后, 网络状态变化时, 会调用该回调
 由于网络变化时, 都会调用到该方法,
 如果需要保存前一次的connectionState, 需要自己手动保存该变量
 @param connectionState 当前SDK连接服务器的状态变化
 */
- (void)didConnectionStateChanged:(MXConnectionState)connectionState;


@end
