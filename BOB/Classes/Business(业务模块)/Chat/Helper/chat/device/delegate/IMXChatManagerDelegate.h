//
//  MXChatManagerLoginDelegate.h
//  Moxian
//  @abstract 关于ChatManager的异步回调协议
//  Created by 王 刚 on 14/12/4.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXChatManagerDelegate.h"

@class MessageModel;
@class NewFriendModel;
/**
 *  本协议主要处理聊天时的一些回调操作（如发送消息成功、收到消息、收到添加好友邀请等消息时的回调操作）
 */
@protocol IMXChatManagerDelegate <MXChatManagerDelegate>

@optional
- (void)didReceiveMessage:(MessageModel *)message;

- (void)didReceiveReceipt:(MessageModel *)message;

- (void)didReceiveSS:(MessageModel *)model
;
@end
