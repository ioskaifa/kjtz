//
//  IMXDeviceManagerDelegate.h
//  MoPal_Developer
//
//  MXDeviceManagerDelegate各类协议的合集
//  Created by aken on 15/3/24.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMXDeviceManagerMediaDelegate.h"

/*!
 @protocol
 @brief MXDeviceManagerDelegate各类协议的合集
 @discussion
 */

@protocol IMXDeviceManagerDelegate <IMXDeviceManagerMediaDelegate>

@end
