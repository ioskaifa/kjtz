//
//  IMXChatManager.h
//  Moxian
//
//  Created by litiankun on 14/12/3.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMXChatManagerLogin.h"
#import "IMXChatManagerChat.h"
#import "IMXChatManagerConversation.h"
#import "IMXChatManagerBase.h"
#import "IMXChatManagerGroup.h"
/*!
 @protocol
 @brief 登录、聊天、保存会话、加解密、多媒体支持等协议的集合
 @discussion 可以通过EaseMob类获得此接口的实例, 示例代码如下:
 [[MXChat sharedInstance] chatManager]
 */
//
@protocol IMXChatManager <IMXChatManagerLogin,
                          IMXChatManagerChat,
                          IMXChatManagerConversation,
                          IMXChatManagerGroup
                        >

@end
