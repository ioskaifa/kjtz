//
//  MXChatManager+MessageOperate.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/8/18.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "MXChatManager.h"
#import "MXChatDefines.h"


@interface MXChatManager (MessageOperate)
// 群聊 单聊 的参数值
+(NSString*)getChatHeaderType:(EMConversationType)chatType;
//
//+(XMPPMessage*)createXMPPMainTag:(MessageModel*) message;
//
//+(DDXMLElement*)createNormalXMPPExtendTag:(MessageModel*) message value:(NSString*)value;
//
//+(DDXMLElement*)createDestroyXMPPExtendTag:(MessageModel*) message value:(NSString*)value;
////文本 语音 等普通消息解析
//+(MessageModel*)modelWithMessage:(XMPPMessage*)message;
////组系统消息解析
//+(MessageModel*)modelWithGroupSystemMessage:(XMPPMessage*)message;
////单聊系统消息解析
//+(MessageModel*)modelWithSystemMessage:(XMPPMessage*)message;
////富消息解析
//+(MessageModel*)modelWithRichMessage:(XMPPMessage*)message;
////销毁消息解析
//+(MessageModel*)modelWithDestroyMessage:(XMPPMessage*)message;
//
//+(XMPPMessage*)createReceiptXMPPMainTag:(MessageModel*) message value:(NSString*)value;
//
//+(MessageModel*)modelWithNoticeMessage:(XMPPMessage*)message;
//
//+(MessageModel*)modelWithFocusMessage:(XMPPMessage*)message;
//
//+(MessageModel*)parseRisterAutoFocusNoticeMessage:(XMPPMessage*)message model:(MessageModel*) model ;
//
////区分是回执还是消息
//+(BOOL)isRequest:(XMPPMessage* )message;
@end
