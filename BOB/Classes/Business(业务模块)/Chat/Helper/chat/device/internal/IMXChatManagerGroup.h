//
//  IMXChatGroupManager.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/8/12.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMXChatManagerBase.h"
#import "ChatGroupModel.h"
#import "MXHttpCallBackConfig.h"

@protocol IMXChatManagerGroup <IMXChatManagerBase>
@optional
//获取所有群组
- (NSArray *)loadAllGroupListFromDatabase;
// 清空群组
-(BOOL)clearGroup;
// 添加群组
- (BOOL)insertGroup:(ChatGroupModel*) group;
// 批量添加群组
- (BOOL)insertGroups:(NSArray*) groups;

// 删除群组
- (BOOL)removeGroup:(ChatGroupModel*) group;

// 删除并退出群组
- (void)asyncDissolveGroup:(NSString *)groupId completion:(MXHttpRequestCallBack)callback;
//查询某一个群组
- (ChatGroupModel*)loadGroupByChatId:(NSString*) groupId;

// 添加成员
- (BOOL)insertGroupMember:(FriendModel*) member;

- (NSMutableArray*)loadGroupMembers:(NSString*) roomId;


//查询某个成员
- (FriendModel*)selectGroupMember:(NSString*) roomId memberId:(NSString*)memberId;

//删除某个成员
- (BOOL)removeGroupMember:(FriendModel*) member;

//修改群组的置顶状态
-(BOOL)updateGroupTopState:(ChatGroupModel*)group;

//修改群组的免打扰状态
-(BOOL)updateGroupDisturbState:(ChatGroupModel*)group;

//修改在群中通讯录的状态
//-(BOOL)updateGroupContactState:(ChatGroupModel*)group;

///更新群人数
-(BOOL)updateGroupMemNum:(ChatGroupModel*)group;

//修改在群中的昵称
-(BOOL)updateGroupNickName:(ChatGroupModel* )group;

//群列表 所有群 全部在里面
@property (nonatomic, readonly) NSMutableArray * groupList;

-(NSMutableArray*)getShowGroupList;

- (NSMutableArray*)getFriendRecords ;

- (BOOL)clearFriendRecords ;

///更新备注映射表
- (void)updateRemarkMap:(NSString *)userId remark:(NSString *)remark needCheckExist:(BOOL)needCheckExist;

///如果有备注返回备注，没有返回群昵称名
- (NSString *)remark:(NSString *)userId;
@end
