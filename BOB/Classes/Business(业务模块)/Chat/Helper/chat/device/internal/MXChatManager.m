//
//  MXChatManager.m
//  Moxian
//
//  Created by 王 刚 on 14/12/4.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import "MXChatManager+MessageOperate.h"
#import "IMXChatManagerDelegate.h"
#import "IMXChatManager.h"
#import "MXChatDBUtil.h"
#import "MXJsonParser.h"
#import "ChatViewVC.h"
#import "MXChatMessageHandler.h"
#import "QNManager.h"
#import "ChatGroupModel.h"
#import "MXDownLoadsManager.h"
#import "MXReachabilityManager.h"
#import "MXConversation.h"
#import "FriendModel.h"
#import "LcwlChat.h"
#import "SRWebSocketManager.h"
#import "AutoTransferModel.h"
#import "ChatCacheFileUtil.h"
#import "TalkManager.h"
#import "ChatSendHelper.h"
#import "DocumentManager.h"
#import <FFToast/FFToast.h>

// 消息回调函数
typedef void(^MessageCallBack)(MessageModel *model, NSError *error);

 static id sharedInstanceNH;

static dispatch_once_t onceNH;

@interface MXChatManager()<IMXChatManager,LcwlSRWebSocketDelegate>
{
    dispatch_queue_t conversationQueue;
    
    dispatch_queue_t delegateQueue;
}
@property (strong, nonatomic) NSPointerArray* delegateArray ;

@property (strong, nonatomic) MessageCallBack messageCallBack;

@property (strong, nonatomic) MessageModel *tempModel;
//外层会话列表
@property (strong, nonatomic) NSMutableArray* conversations;
//好友列表
@property (strong, nonatomic) NSMutableArray* users;
//群组列表
@property (strong, nonatomic) NSMutableArray* groupList;
//数据库中所有的表
@property (strong, nonatomic) NSMutableArray* chatAllTables;

///好友备注配置表 --- 虽然发送的聊天信息里已经自带了昵称，但是如果昵称改变了，之前的聊天内容的昵称也需要改变，
///这里使用一个全局的映射表，在获取名称的时候直接从该名称去查询，这样的好处是不用每次获得消息都去遍历修改每条信息的值
@property(nonatomic, strong) NSMutableDictionary *remarkMapTable;

@property(nonatomic, copy) NSString *userId;

@end

@implementation MXChatManager
@synthesize autoTransferModel = _autoTransferModel;
+ (MXChatManager*)sharedInstance {
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

+ (void)setNil
{
    sharedInstanceNH = nil;
    onceNH = 0;
}


-(id)init
{
    if (self=[super init]) {
        _delegateArray = [NSPointerArray weakObjectsPointerArray];//[[NSMutableArray alloc]initWithCapacity:10];
        
        _conversations = [[NSMutableArray alloc]initWithCapacity:10];

        _users = [[NSMutableArray alloc]initWithCapacity:10];
        
        _groupList = [[NSMutableArray alloc]initWithCapacity:10];
        
        _chatAllTables = [[NSMutableArray alloc]initWithCapacity:10];
        
        _remarkMapTable = [NSMutableDictionary dictionaryWithCapacity:1];

        conversationQueue = dispatch_queue_create("com.moxian.conversation", DISPATCH_QUEUE_CONCURRENT);
        
        delegateQueue = dispatch_queue_create("com.moxian.chatDelegate", DISPATCH_QUEUE_CONCURRENT);
        
        [SRWebSocketManager sharedInstance].delegate = self;
    }
    return self;
    
}


-(BOOL)clearFriendRecords{
     return [[MXChatDBUtil sharedDataBase] clearFriendRecords];
}
/**********************************************************聊天***********************************************/
#pragma mark - 登录

- (void)login:(NSString *)userId {
    if (![StringUtil isEmpty:userId]) {
        self.userId = userId;
        [[SRWebSocketManager sharedInstance] connect:userId];
    }
}

- (void)receiveMessage:(MessageModel *)message{
    
}
- (void)sendMessage:(MessageModel *)message{
    
}

///更新备注映射表，群昵称的级别小于备注级别，如果有备注则不需要更新，此时需设置needCheckExist为YES
- (void)updateRemarkMap:(NSString *)userId remark:(NSString *)remark needCheckExist:(BOOL)needCheckExist {
    if(userId.length == 0 || [StringUtil isEmpty:remark]) {
        return;
    }
    
    //如果存在则不更新,并且是好友的情况下就不用更新了，如果非好友依然需要更新，因为非好友无法设置备注
    FriendModel *model = [[MXChatDBUtil sharedDataBase] selectFriendByChatId:userId];
    if(needCheckExist && model != nil && ![StringUtil isEmpty:model.remark]) {
        if([self.remarkMapTable valueForKey:userId] != nil) {
            return;
        }
    }
    [self.remarkMapTable setValue:remark forKey:userId];
}

///如果有备注返回备注，没有返回群昵称名
- (NSString *)remark:(NSString *)userId {
    if(userId.length == 0) {
        return @"";
    }
    NSString *remark = [self.remarkMapTable valueForKey:userId];
    return remark;
}

/**********************************************************聊天***********************************************/
-(NSMutableArray*)getShowGroupList{
    NSMutableArray* tmpArray = [[NSMutableArray alloc]initWithCapacity:0];
//    for (ChatGroupModel* group in _groupList) {
//        if (group.enable_contact) {
//            [tmpArray addObject:group];
//        }
//    }
//    [tmpArray sortUsingComparator:^NSComparisonResult(ChatGroupModel* obj1, ChatGroupModel* obj2) {
//        if ([obj2.createTime doubleValue] > [obj1.createTime doubleValue]) {
//            return YES;
//        }
//        return NO;
//    }];
    return tmpArray;
}

- (AutoTransferModel *)autoTransferModel {
    if(!_autoTransferModel) {
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",@"AutoTransferModel",UDetail.user.chatUser_id]];
        if(data) {
            _autoTransferModel = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        } else {
            _autoTransferModel = [[AutoTransferModel alloc] init];
        }
    }
    return _autoTransferModel;
}



- (void)setAutoTransferModel:(AutoTransferModel *)autoTransferModel {
    if(autoTransferModel) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:autoTransferModel];
        if(data) {
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:[NSString stringWithFormat:@"%@_%@",@"AutoTransferModel",UDetail.user.chatUser_id]];
            [[NSUserDefaults standardUserDefaults] synchronize];
            _autoTransferModel = nil;
        }
    }
}

- (void)dealloc {
    self.delegateArray = nil;
}

#pragma mark - 聊天操作
- (void)addDelegate:(id<IMXChatManagerDelegate>)delegate{
    dispatch_barrier_async(delegateQueue, ^(){
        /////清空数组中的所有NULL,注意:经过测试如果直接compact是无法清空NULL,需要在compact之前,调用一次[_weakPointerArray addPointer:NULL],才可以清空
        [self.delegateArray addPointer:NULL];
        [self.delegateArray compact];
        //CFAbsoluteTime startTime =CFAbsoluteTimeGetCurrent();
        [self.delegateArray addPointer:(__bridge void * _Nullable)(delegate)];
//        CFAbsoluteTime linkTime = (CFAbsoluteTimeGetCurrent() - startTime);
//        NSLog(@"###Linked in %f ms", linkTime *1000.0);
//
//        NSMutableArray *arr = [NSMutableArray arrayWithCapacity:1];
//
//        startTime =CFAbsoluteTimeGetCurrent();
//        [arr addObject:delegate];
//        linkTime = (CFAbsoluteTimeGetCurrent() - startTime);
//        NSLog(@"!!!Linked in %f ms", linkTime *1000.0);
    });
}

- (void)removeDelegate:(id<IMXChatManagerDelegate>)delegate{
    dispatch_barrier_async(delegateQueue, ^(){
        if (self.delegateArray.count == 0) {
            return;
        }
        NSUInteger index = [[self.delegateArray allObjects] indexOfObject:delegate];
        if (index > self.delegateArray.count) {
            return;
        }
        [self.delegateArray removePointerAtIndex:index];
    });
}

- (id)lastObj {
    if(_delegateArray == nil || _delegateArray.count == 0) {
        return nil;
    }
    
    id lastestObject = [_delegateArray pointerAtIndex:_delegateArray.count-1];
    return lastestObject;
}

-(MXConversation*)getLastestConversation{
    ChatViewVC *lastestObject = [self lastObj];
    if ([lastestObject isKindOfClass:[ChatViewVC class]]) {
        ChatViewVC* vc = (ChatViewVC*)lastestObject;
        return vc.getCurrentConversation;
    }
    return nil;
}

-(BOOL)isBelongExistConversation:(NSString*)chatID{
    BOOL isBelong = NO;
    id<IMXChatManagerDelegate> vc = [self lastObj];
    if ([vc isKindOfClass:[ChatViewVC class]]) {
        ChatViewVC* chatVC = (ChatViewVC*)vc;
        NSString* conversationId = chatVC.getCurrentConversation.chat_id;
        if ([conversationId isEqualToString:chatID]) {
            isBelong = YES;
        }
    }
    return isBelong;
}

- (void)removeAllChatDelegate {
    if (_delegateArray) {
        _delegateArray = [NSPointerArray weakObjectsPointerArray];
    }
}

- (void)loginOut
{
    [self.class setNil];
    [[SRWebSocketManager sharedInstance] loginOut];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"token"];
}

-(void)didSendReceipt:(MessageModel *)message vValue:(NSString*)value{
//    if([MXXMPPManager sharedInstance].xmppStream==nil || [[MXXMPPManager sharedInstance].xmppStream isDisconnected]){
//        return;
//    }
//    //生成消息对象
//    XMPPMessage* aMessage = [MXChatManager createReceiptXMPPMainTag:message value:value];
//    // 添加扩展标签
//    DDXMLElement* ele = [MXChatManager createNormalXMPPExtendTag:message  value:value];
//    [aMessage addChild:ele];
//    [[MXXMPPManager sharedInstance].xmppStream sendElement:aMessage andGetReceipt:nil];
    
}

-(void)didSendDestroyReceipt:(MessageModel *)message  vValue:(NSString*)value{
//    if([MXXMPPManager sharedInstance].xmppStream==nil || [[MXXMPPManager sharedInstance].xmppStream isDisconnected]){
//        [MXAlertViewHelper showAlertViewWithMessage:MXLang(@"MoTalk_moTalkCannotSendOffLine", @"MoTalk离线不可发送消息!")];
//    }
//    //生成消息对象
//    XMPPMessage* aMessage = [MXChatManager createXMPPMainTag:message];
//    // 添加扩展标签
//     DDXMLElement* ele = [MXChatManager createDestroyXMPPExtendTag:message value:value];
//    [aMessage addChild:ele];
//    [[MXXMPPManager sharedInstance].xmppStream sendElement:aMessage andGetReceipt:nil];
}




#pragma mark - 提供基础消息操作
/**********************************************************提供基础消息操作***********************************************/
#pragma mark - 发送消息
- (void)sendMessage:(MessageModel *)message completion:(void (^)(MessageModel *model, NSError *error))completion {
    BOOL isNeedInsert = YES;
    if (message.type == MessageTypeNormal ||
        message.type == MessageTypeGroupchat ||
        message.type == MessageTypeClient) {
        if (message.subtype == kMXMessageTypeVoice ||
            message.subtype == kMXMessageTypeImage ||
            message.subtype == kMXMessageTypeVoiceChat ||
            message.subtype == kMXMessageTypeFile ||
            message.subtype == kMXMessageTypeWithdraw ||
            message.subtype == kMXMessageTypeClient) {
            isNeedInsert = NO;
        }
    }
    if (isNeedInsert) {
        [[LcwlChat shareInstance].chatManager insertMessage:message];
        // 将message添加到发送中的消息队列中
        [[MXChatMessageHandler sharedInstance] addMessage:message];
    }
    [[SRWebSocketManager sharedInstance] sendMsg:[message jsonString]];
    ///有些消息不是在ChatViewVC发送的，需要生成聊天记录
    if (message.subtype == kMXMessageTypeVoiceChat) {
        [[SRWebSocketManager sharedInstance] receiveMessage:message];
    }
    [self tansferMultiGroup:message];
}


-(void)sendVoiceMessage:(MessageModel*)message delegate:(id<SendMessageDelegate>) sender{    
    [[QNManager shared] uploadAudio:message.locFileUrl completion:^(id data) {
        if(![StringUtil isEmpty:data]) {
            NSString *pathExtension = [message.locFileUrl pathExtension];
            message.imageRemoteUrl = data;
            NSDictionary* bodyDict = @{@"attr1":message.imageRemoteUrl,
                                       @"attr2":message.voiceLen,
                                       @"attr3":pathExtension};
            message.body = [MXJsonParser dictionaryToJsonString:bodyDict];
            ///上传完成后写入本地
            NSURL *locFileUrl = [NSURL fileURLWithPath:message.locFileUrl];
            NSString *newUrl = message.fullPath;
            [[NSFileManager defaultManager] moveItemAtURL:locFileUrl
                                                    toURL:[NSURL fileURLWithPath:newUrl]
                                                    error:nil];
                        
            [[MXChatDBUtil sharedDataBase] updateFileMessage:message];
            [[LcwlChat shareInstance].chatManager sendMessage:message completion:^(MessageModel *message, NSError *error) {
                
            }];
        }else{
            if ([sender respondsToSelector:@selector(uploadMessageFailureCallback:)]) {
                message.state = kMXMessageState_Failure;
                [[MXChatDBUtil sharedDataBase]updateMessageState:message];
                [sender uploadMessageFailureCallback:message];
                [[MXChatMessageHandler sharedInstance]removeMessage:message];
            }
        }
    }];
}

-(void)sendImageMessage:(MessageModel*)message delegate:(id<SendMessageDelegate>) sender{
    if (![StringUtil isEmpty:message.imageRemoteUrl]) {
        [self sendMessage:message completion:nil];
    }else {
        NSString *rootPath = [[ChatCacheFileUtil sharedInstance] userDocPath];
        NSString *path = [message.locFileUrl lastPathComponent];
        NSString *localUrl = StrF(@"%@/%@", rootPath, path);
        UIImage *image = [UIImage imageWithContentsOfFile:localUrl];
        [[QNManager shared] uploadImage:image completion:^(id data) {
            if(![StringUtil isEmpty:data]) {
                message.imageRemoteUrl = data;
                NSDictionary* bodyDict = @{@"attr1":message.imageRemoteUrl,
                                           @"attr2":@(message.size.width),
                                           @"attr3":@(message.size.height)};
                message.body = [MXJsonParser dictionaryToJsonString:bodyDict];
                [[MXChatDBUtil sharedDataBase] updateFileMessage:message];
                [[LcwlChat shareInstance].chatManager sendMessage:message completion:^(MessageModel *message, NSError *error) {
                    
                }];
            }else{
                if ([sender respondsToSelector:@selector(uploadMessageFailureCallback:)]) {
                    message.state = kMXMessageState_Failure;
                    [[MXChatDBUtil sharedDataBase]updateMessageState:message];
                    [sender uploadMessageFailureCallback:message];
                    [[MXChatMessageHandler sharedInstance]removeMessage:message];
                }
            }
        }];
    }
}

-(void)sendFileMessage:(MessageModel*)message delegate:(id<SendMessageDelegate>) sender{
    if (![StringUtil isEmpty:message.fileUrl]) {
        [self sendMessage:message completion:nil];
    }else {
        NSString *rootPath = [[ChatCacheFileUtil sharedInstance]userDocPath];
        NSString *path = [message.locFileUrl lastPathComponent];
        NSString *localPath = StrF(@"%@/%@", rootPath, path);
        [[QNManager shared] uploadFilePath:localPath completion:^(id data) {
            if(![StringUtil isEmpty:data]) {
                ///上传完成后修改本地文件名、防止重名的问题
                NSURL *localUrl = [NSURL fileURLWithPath:localPath];
                NSString *newPath = StrF(@"%@/%@.%@", rootPath, data, [path pathExtension]);
                NSURL *newUrl = [NSURL fileURLWithPath:newPath];
                [[NSFileManager defaultManager] moveItemAtURL:localUrl
                                                        toURL:newUrl
                                                        error:nil];
                message.fileUrl = data;
                message.locFileUrl = newPath;
                NSDictionary* bodyDict = @{@"attr1":data ?: @"",
                                           @"attr2":message.fileSubType ?: @"",
                                           @"attr3":[StringUtil convertFileSize:message.fileSize],
                                           @"attr4":message.fileName ?: @""};
                message.body = [MXJsonParser dictionaryToJsonString:bodyDict];
                [[MXChatDBUtil sharedDataBase] updateFileMessage:message];
                [[LcwlChat shareInstance].chatManager sendMessage:message completion:^(MessageModel *message, NSError *error) {
                    
                }];
            }else{
                if ([sender respondsToSelector:@selector(uploadMessageFailureCallback:)]) {
                    message.state = kMXMessageState_Failure;
                    [[MXChatDBUtil sharedDataBase]updateMessageState:message];
                    [sender uploadMessageFailureCallback:message];
                    [[MXChatMessageHandler sharedInstance]removeMessage:message];
                }
            }
        }];
    }
}

-(void)sendVideoMessage:(MessageModel*)message delegate:(id<SendMessageDelegate>) sender{
    if (![StringUtil isEmpty:message.fileUrl]) {
        [self sendMessage:message completion:nil];
    }else {
        //把视频写入本地,无法重新发送的问题
        NSString *filePath = message.locFileUrl;
        NSString *path = [filePath hasPrefix:@"file://"] ? [filePath substringFromIndex:@"file://".length] : filePath;
        NSData *fileData = [NSData dataWithContentsOfFile:path];
        //取当前文件路径 - app每次打开该路径都发生变化 - 每次都要重新获取
        NSString *rootPath = [ChatCacheFileUtil sharedInstance].userDocPath;
        //获取文件名跟后缀
        NSString *fileName = [message.locFileUrl lastPathComponent];
        NSString *fullPath;
        if (fileData) {
            fullPath = [rootPath stringByAppendingPathComponent:fileName];
            [fileData writeToFile:fullPath atomically:YES];
        }
        [[QNManager shared] uploadFilePath:[rootPath stringByAppendingPathComponent:fileName] completion:^(id data) {
            if(![StringUtil isEmpty:data]) {
                message.fileUrl = data;
                ///如果fullpath不为空,则要把发送前保存本地的视频重新命名,防止重名的情况
                if (fullPath) {
                    NSURL *localUrl = [NSURL fileURLWithPath:fullPath];
                    NSString *fileName = StrF(@"%@.%@", data,[filePath pathExtension]);
                    NSString *newFullPath = [rootPath stringByAppendingPathComponent:fileName];
                    NSURL *newUrl = [NSURL fileURLWithPath:newFullPath];
                    [[NSFileManager defaultManager] moveItemAtURL:localUrl
                                                            toURL:newUrl
                                                            error:nil];
                } else {
                    ///如果为空 则直接写入本地缓存，防止重复请求问题
                    NSString *filePath = message.locFileUrl;
                    NSString *path = [filePath hasPrefix:@"file://"] ? [filePath substringFromIndex:@"file://".length] : filePath;
                    NSData *fileData = [NSData dataWithContentsOfFile:path];
                    if (fileData) {
                        NSString *fileName = StrF(@"%@.%@", data,[filePath pathExtension]);
                        NSString *fullPath = [rootPath stringByAppendingPathComponent:fileName];
                        [fileData writeToFile:fullPath atomically:YES];
                    }
                }
                NSDictionary* bodyDict = @{@"attr1":data ?: @"",
                                           @"attr4":[message.locFileUrl pathExtension] ?: @""};
                message.body = [MXJsonParser dictionaryToJsonString:bodyDict];
                [[MXChatDBUtil sharedDataBase] updateVideoUrlMessage:message];
                [[LcwlChat shareInstance].chatManager sendMessage:message completion:^(MessageModel *message, NSError *error) {
                    
                }];
            }else{
                if ([sender respondsToSelector:@selector(uploadMessageFailureCallback:)]) {
                    message.state = kMXMessageState_Failure;
                    [[MXChatDBUtil sharedDataBase]updateMessageState:message];
                    [sender uploadMessageFailureCallback:message];
                    [[MXChatMessageHandler sharedInstance]removeMessage:message];
                }
            }
        }];
    }
}

#pragma mark - 多群转发
-(void)sendForwardMessage:(MessageModel* )message{
    message.msg_direction = 1;
    if (message.type == MessageTypeNormal || message.type == MessageTypeGroupchat ) {
        if (message.subtype == kMXMessageTypeImage) {
            NSString* imagePath = [[ChatCacheFileUtil sharedInstance] handleChatImagePath:message.locFileUrl remoteUrl:message.imageRemoteUrl];
            UIImage* image = [UIImage imageWithContentsOfFile:imagePath];
            imagePath = [TalkManager saveChatImage:image];
            message.locFileUrl = imagePath;
            [[LcwlChat shareInstance].chatManager insertMessage:message];
            message.state=kMXMessageStateSending;
            [[LcwlChat shareInstance].chatManager sendImageMessage:message delegate:nil];
        }else if(message.subtype == kMXMessageTypeVoice){
            [[LcwlChat shareInstance].chatManager insertMessage:message];
            message.state = kMXMessageStateSending;
            [[LcwlChat shareInstance].chatManager sendVoiceMessage:message delegate:nil];
        }else{
            [[LcwlChat shareInstance].chatManager sendMessage:message completion:nil];
        }
    }else if(message.type == MessageTypeRich){
        [[LcwlChat shareInstance].chatManager sendMessage:message completion:nil];
    }
    
}

- (void)tansferMultiGroup:(MessageModel *)message {
    if(![[MXChatManager sharedInstance].autoTransferModel needTransfer:message]) {
        return;
    }
    //当此群是主讲群，且消息的发送者为讲师的情况下才对该消息进行转发，并且该消息不能转发到主讲群
    NSArray *teachersUserid = [[MXChatManager sharedInstance].autoTransferModel.selectTeachers valueForKeyPath:@"@distinctUnionOfObjects.userid"];
    if([message.chat_with isEqualToString:[MXChatManager sharedInstance].autoTransferModel.mainChatGroupModel.groupId] &&
       [teachersUserid containsObject:message.fromID]) {
        [[MXChatManager sharedInstance].autoTransferModel.selectTargetGroup enumerateObjectsUsingBlock:^(ChatGroupModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ChatGroupModel* group = (ChatGroupModel*)obj;
            MessageModel* tmpModel = [message copy];
            tmpModel.fromID = UDetail.user.chatUser_id;
            tmpModel.avatar = UDetail.user.head_photo;
            tmpModel.name = UDetail.user.user_name;
            tmpModel.toID = group.groupId;
            tmpModel.chat_with = group.groupId;
            tmpModel.chatType = eConversationTypeGroupChat;
            
            tmpModel.messageId = [TalkManager msgId];
            tmpModel.sendTime = [ChatSendHelper chatSendTime];
            [self sendForwardMessage:tmpModel];
        }];
    }
}

#pragma mark - 代理
/**********************************************************代理***********************************************/
#pragma mark - 接收消息回调
- (void)didReceiveSS:(MessageModel *)message{
    if (![SRWebSocketManager sharedInstance].isConnect) {
        return;
    }
    dispatch_barrier_async(delegateQueue, ^(){
        NSArray* tmpArray = [self.delegateArray allObjects];
        if (tmpArray.count > 0) {
            [tmpArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                id<IMXChatManagerDelegate> chat = obj;
                if (chat && [chat respondsToSelector:@selector(didReceiveMessage:)]) {
                    [chat didReceiveMessage:message];
                }
            }];
        }
    });
}

- (void)didReceiveMessage:(MessageModel*)message {
    if (![SRWebSocketManager sharedInstance].isConnect) {
        return;
    }
    dispatch_barrier_async(delegateQueue, ^(){
        NSArray* tmpArray = [self.delegateArray allObjects];
        if (tmpArray.count > 0) {
            [tmpArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                id<IMXChatManagerDelegate> chat = obj;
                if (chat && [chat respondsToSelector:@selector(didReceiveMessage:)]) {
                    [chat didReceiveMessage:message];
                    [self tansferMultiGroup:message];
                }
            }];
        }
    });
}

- (void)didReceiveSystemMsg:(MessageModel *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        FFToast *toast = [[FFToast alloc]initToastWithTitle:@"系统公告" message:message.attr3 iconImage:[UIImage imageNamed:@"notiIcon"]];
        toast.toastPosition = FFToastPositionBelowStatusBarWithFillet;
        toast.titleFont = [UIFont systemFontOfSize:14];
        [toast show:^{
            //Toast is called when clicked
            MXRoute(@"MessageCenterVC", nil);
        }];
    });
}

#pragma mark - 发送消息回调
- (void)didReceiveReceipt:(MessageModel *)message {
    if (![SRWebSocketManager sharedInstance].isConnect) {
        return;
    }
    if (message == nil) {
        return;
    }
    dispatch_barrier_async(delegateQueue, ^(){
        NSArray* tmpArray = [self.delegateArray allObjects];
        if (tmpArray > 0) {
            [tmpArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                id<IMXChatManagerDelegate> chat = obj;
                if (chat && [chat respondsToSelector:@selector(didReceiveReceipt:)]) {
                    [chat didReceiveReceipt:message];
                }
            }];
        }
    });
}

- (void)sendReceipt:(MessageModel *)message{
    message.is_acked = 1;
    [[LcwlChat shareInstance].chatManager updateMsgAck:message];
    [[SRWebSocketManager sharedInstance] sendMsg:[message receiptString]];
}
//更新记录
#pragma mark - 好友操作
- (NSMutableArray *)users{
    return _users;
}

- (NSMutableArray *)friends{
    NSMutableArray* tmpFriendArray = [[NSMutableArray alloc] initWithCapacity:0];
    [_users enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        FriendModel* moyou = (FriendModel* )obj;
        if (moyou.followState == MoRelationshipTypeMyFriend) {
            [tmpFriendArray addObject:moyou];
        }
    }];
    return tmpFriendArray;
}
-(FriendModel*)loadFriendByChatId:(NSString*)chat_id{
    int index = [self findFriendPosition:chat_id];
    if (index >= 0 ) {
        return self.users[index];
    }
    return nil;
}

-(int)findFriendPosition:(NSString*)chatId{
    for (int i=0;i< self.users.count;i++) {
        FriendModel* model = self.users[i];
        if ([model.userid isEqualToString:chatId]) {
            return i;
        }
    }
    return -1;
}

-(BOOL)updateFriendInfo:(FriendModel *) moyou{
    // add by yang.xiangbao 2015/11/9
    if (!moyou) {
        return NO;
    }
    // the end

    int index = [self findFriendPosition:moyou.userid];
    if(index < self.users.count)
        [self.users replaceObjectAtIndex:index withObject:moyou];
    return [[MXChatDBUtil sharedDataBase]updateMoYouToDB:moyou];
}

- (BOOL)insertFriends:(NSArray*) friends{
    if (friends.count == 0) {
        return NO;
    }
    __block NSMutableArray* sqlArray = [[NSMutableArray alloc]initWithCapacity:10];
    [friends enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        FriendModel* friend = (FriendModel*)obj;
        int index = [self findFriendPosition:friend.userid];
        BOOL isExist = YES;
        if (index == -1) {
            isExist = NO;
        }
        if (!isExist) {
            [self.users addObject:friend];
        }else{
            FriendModel *orignModel = [self.users safeObjectAtIndex:index];
            if (orignModel) {
                friend.enable_disturb = orignModel.enable_disturb;
                friend.enable_top = orignModel.enable_top;            
            }
            [self.users replaceObjectAtIndex:index withObject:friend];
        }
        NSString* tmpSql = [[MXChatDBUtil sharedDataBase]getFriendHandleSqlDB:friend isExist:isExist];
        [sqlArray addObject:tmpSql];
    }];
    return [[MXChatDBUtil sharedDataBase]executeMoYouSql:sqlArray model:friends ];
}

//- (BOOL)insertFriend:(MoYouModel*) moyou{
//    // add by yang.xiangbao 2015/11/9
//    if (!moyou) {
//        return NO;
//    }
//    // the end
//
//    int index = [self findFriendPosition:moyou.chatId];
//    BOOL isExist = NO;
//    if (index == -1) {
//        [self.users addObject:moyou];
//    } else {
//        isExist = YES;
//        MoYouModel *orignModel = [self.users objectAtIndex:index];
//        if (orignModel) {
//            moyou.enable_disturb = orignModel.enable_disturb;
//            moyou.enable_top = orignModel.enable_top;
//        }
//        [self.users replaceObjectAtIndex:index withObject:moyou];
//    }
//
//    return [[MXChatDBUtil sharedDataBase]insertFriendToDB:moyou isExist:isExist];
//}
- (BOOL)clearFriend:(NSString*)chat_id{
    
    [[MXChatDBUtil sharedDataBase]deleteFriendByID:chat_id];
    return YES;
}

-(MXConversation*)loadConversationObject:(NSString*) chat_id{
    return [self loadConversation:chat_id];
}

-(MXConversation*)loadConversation:(NSString*) chat_id{
    __block MXConversation* tmp = nil;
    @synchronized(_conversations) {
        [_conversations enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MXConversation* conversation = (MXConversation*)obj;
            if ([conversation.chat_id isEqualToString:chat_id]) {
                 tmp = conversation;
            }
        }];
    }
    return tmp;
}
//-(MXConversation*)loadShopConversation:(NSString*) chat_id{
//    @synchronized(_shopConversations) {
//        for (MXConversation* conversation in _shopConversations) {
//            if ([conversation.chat_id isEqualToString:chat_id]) {
//                return conversation;
//            }
//        }
//    }
//
//    return nil;
//}
//
- (MXConversation*)createConversationObject:(MXConversation*)obj{
//    BOOL isShop = [TalkManager isShop:obj.chat_id];
//    if (isShop) {
//        return [self createShopConversation:obj];
//    }else{
        return [self createConversation:obj];
//    }
}
//
-(MXConversation*)createConversation:(MXConversation *)conversation{
    [[MXChatDBUtil sharedDataBase] createConversation:conversation];
    MXConversation* tmpConversation = [self loadConversationObject:conversation.chat_id];
    if (tmpConversation == nil) {
        tmpConversation = conversation;
          [_conversations addObject:tmpConversation];
       
    }
    return tmpConversation;
}
//
//-(MXConversation*)createShopConversation:(MXConversation *)conversation{
//    [[MXChatDBUtil sharedDataBase] createShopConversation:conversation];
//    MXConversation* tmpConversation = [self loadConversationObject:conversation.chat_id];
//    if (tmpConversation == nil) {
//        tmpConversation = conversation;
//        @synchronized(_shopConversations) {
//            [_shopConversations insertObject:tmpConversation queue:_conversationQueue];
//        }
//    }
//    return tmpConversation;
//}
//
- (BOOL)deleteConversationByChatter:(NSString *) chat_id{
    MXConversation* conversation = [self loadConversationObject:chat_id];
    [_conversations removeObject:conversation];
    if (conversation.conversationType == eConversationTypeGroupChat) {
        ChatGroupModel* group = [self loadGroupByChatId:chat_id];
        [_groupList removeObject:group];
         [[MXChatDBUtil sharedDataBase]removeChatGroup:group];
    }
  
    [[MXChatDBUtil sharedDataBase]deleteConversation:chat_id];
    [[MXChatDBUtil sharedDataBase]clearMessages:chat_id];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:kRefreshChatList object:nil];
    return YES;
}

- (BOOL)deleteAllConversation{
    [_conversations enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(MXConversation  *conversation, NSUInteger idx, BOOL * _Nonnull stop) {
        if (conversation.conversationType == eConversationTypeChat || conversation.conversationType == eConversationTypeGroupChat) {
            NSString* chat_id = conversation.chat_id;
            MXConversation* conversation = [self loadConversationObject:chat_id];
            [_conversations removeObject:conversation];
            if (conversation.conversationType == eConversationTypeGroupChat) {
                ChatGroupModel* group = [self loadGroupByChatId:chat_id];
                [_groupList removeObject:group];
                [[MXChatDBUtil sharedDataBase]removeChatGroup:group];
            }
            
            [[MXChatDBUtil sharedDataBase]deleteConversation:chat_id];
            [[MXChatDBUtil sharedDataBase]clearMessages:chat_id];
        }
    }];
    return YES;
}

-(NSArray*)allShopTables{
    NSMutableArray* tbNameArray = [[NSMutableArray alloc]initWithCapacity:0];
    [self.chatAllTables enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSString class]]) {
            if ([obj rangeOfString:@"_"].location != NSNotFound) {
                [tbNameArray addObject:obj];
            }
        }
    }];
    return tbNameArray;
}
//此处只取出一条数据
- (NSMutableArray *)conversations{
    NSMutableArray* tmpArray = [[NSMutableArray alloc] initWithCapacity:2];
    NSArray* tmpConversation = [_conversations copy];
    [tmpConversation enumerateObjectsUsingBlock:^(MXConversation  *conversation, NSUInteger idx, BOOL * _Nonnull stop) {
        MXConversation* newConversation = [[MXConversation alloc]init];
        newConversation.conversationType = conversation.conversationType;
        newConversation.chat_id = conversation.chat_id;
        newConversation.lastClearTime = conversation.lastClearTime;
        newConversation.unReadNums = conversation.unReadNums;
        newConversation.associate_id = conversation.associate_id;
        newConversation.lastMessageId = conversation.lastMessageId;
        newConversation.isAt = conversation.isAt;
        newConversation.creatorRole = conversation.creatorRole;
        NSArray* messageArray = [[MXChatDBUtil sharedDataBase]lastMessage:conversation.chat_id messageId:newConversation.lastMessageId];
        [newConversation.messages addObjectsFromArray:messageArray];
        [tmpArray addObject:newConversation];
            
        
    }];
    return tmpArray;
}

//- (NSMutableArray *)shopConversations{
//    NSMutableArray* tmpArray = [NSMutableArray array];
//    [_shopConversations enumerateObjectsUsingBlock:^(MXConversation  *conversation, NSUInteger idx, BOOL * _Nonnull stop) {
//        MXConversation* newConversation = [[MXConversation alloc]init];
//        newConversation.conversationType = conversation.conversationType;
//        newConversation.chat_id = conversation.chat_id;
//        newConversation.lastClearTime = conversation.lastClearTime;
//        newConversation.unReadNums = conversation.unReadNums;
//        newConversation.associate_id = conversation.associate_id;
//        NSArray* messageArray = [[MXChatDBUtil sharedDataBase]selectMsgByUserId:newConversation.chat_id];
//        [newConversation.messages addObjectsFromArray:messageArray];
//        [tmpArray addObject:newConversation];
//
//    }];
//    return tmpArray;
//}


-(void)loadAllConfigFromDatabase{
    [self loadAllConversationsFromDB];
    [self loadAllGroupListFromDatabase];
    [self loadAllTablesFromDB];
    [[MXChatMessageHandler sharedInstance]loadMessages];
}


- (void)loadAllConversationsFromDB{
    _conversations = [[MXChatDBUtil sharedDataBase]loadConversationsFromDB];
    if (!_conversations) {
        _conversations = [[NSMutableArray alloc]initWithCapacity:2];
    }
    self.users = [[MXChatDBUtil sharedDataBase]loadFriendFromDatabase];
}

-(NSArray*)loadAllTablesFromDB{
    self.chatAllTables = [[MXChatDBUtil sharedDataBase]loadAllTablesFromDB];
    return self.chatAllTables;
}
#pragma mark - 群组 操作
- (NSArray *)loadAllGroupListFromDatabase{
    self.groupList = [[MXChatDBUtil sharedDataBase]loadChatGroupFromDatabase];
    return self.groupList;
}
-(BOOL)clearGroup{
    return  [[MXChatDBUtil sharedDataBase]clearGroup];
}

-(BOOL)updateGroupNickName:(ChatGroupModel* )group{
    ChatGroupModel* tmp = [self loadGroupByChatId:group.groupId];
    tmp.nickname = group.nickname;
    return [[MXChatDBUtil sharedDataBase]updateGroupNickname:tmp.nickname groupId:group.groupId];
}

- (BOOL)insertGroup:(ChatGroupModel*) group{
    ChatGroupModel* tmp = [self loadGroupByChatId:group.groupId];
    if (tmp == nil) {
        [[MXChatDBUtil sharedDataBase]insertGroup:group];
        [self.groupList addObject:group];
    }else{
        NSInteger index = [self.groupList indexOfObject:tmp];
        group.enable_disturb = tmp.enable_disturb;
        group.enable_top = tmp.enable_top;
        [self.groupList replaceObjectAtIndex:index withObject:group];
        [[MXChatDBUtil sharedDataBase]updateGroup:group];
    }
    return YES;
}

- (BOOL)insertGroups:(NSArray*) groups{
    __block NSMutableArray* sqlArray = [[NSMutableArray alloc]initWithCapacity:10];
    [groups enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ChatGroupModel* group = (ChatGroupModel*)obj;
        ChatGroupModel* tmp = [self loadGroupByChatId:group.groupId];
        BOOL isExist = YES;
        if (tmp == nil) {
            isExist = NO;
        }
        if (!isExist) {
             [self.groupList addObject:group];
        }else{
            NSInteger index = [self.groupList indexOfObject:tmp];
            group.enable_disturb = tmp.enable_disturb;
            group.enable_top = tmp.enable_top;
            [self.groupList replaceObjectAtIndex:index withObject:group];
        }
        NSString* tmpSql = [[MXChatDBUtil sharedDataBase]getGroupHandleSqlDB:group isExist:isExist];
        [sqlArray addObject:tmpSql];
    }];
    [[MXChatDBUtil sharedDataBase]executeGroupSql:sqlArray model:groups];
    return YES;
}

- (BOOL)removeGroup:(ChatGroupModel*) group{
    [self deleteConversationByChatter:group.groupId];
    return YES;
}
- (BOOL)insertGroupMember:(FriendModel*) member{
    return [[MXChatDBUtil sharedDataBase] insertGroupMemeber:member];
}
-(BOOL)removeGroupMember:(FriendModel*) member{
    return [[MXChatDBUtil sharedDataBase] removeGroupMember:member];
}
- (NSMutableArray*)loadGroupMembers:(NSString*) roomId {
    return [[MXChatDBUtil sharedDataBase] loadGroupMembers:roomId];
}

- (FriendModel*)selectGroupMember:(NSString*) roomId memberId:(NSString*)memberId{
    return [[MXChatDBUtil sharedDataBase] selectGroupMember:roomId memberId:memberId];
}
- (ChatGroupModel*)loadGroupByChatId:(NSString*) groupId{
    for (ChatGroupModel* group in self.groupList) {
        if ([group.groupId isEqualToString:groupId]) {
            return group;
        }
    }
    return nil;
}
//- (void)asyncDissolveGroup:(NSString *)groupId completion:(MXHttpRequestCallBack)callback{
//    NSDictionary* param = @{@"roomId":groupId};
//    [GroupManager asyncDissolveGroup:param completion:^(BOOL success, NSString *error) {
//        if (success) {
//            ChatGroupModel* orignGroup = [self loadGroupByChatId:groupId];
//            [self.groupList removeObject:orignGroup];
//            [[MXChat sharedInstance].chatManager removeGroup:orignGroup];
//            [[MXChatDBUtil sharedDataBase]clearGroupMembers:orignGroup.groupId];
//        }
//        callback(success,error);
//    }];
//}

-(BOOL)updateGroupTopState:(ChatGroupModel*)group{
    ChatGroupModel* orignGroup = [self loadGroupByChatId:group.groupId];
    orignGroup.enable_top = group.enable_top;
    return [[MXChatDBUtil sharedDataBase]updateGroupTopState:orignGroup];
}

-(BOOL)updateGroupDisturbState:(ChatGroupModel*)group{
    ChatGroupModel* orignGroup = [self loadGroupByChatId:group.groupId];
    orignGroup.enable_disturb = group.enable_disturb;
    return [[MXChatDBUtil sharedDataBase]updateGroupDisturbState:orignGroup];
}

-(BOOL)updatePersonalDisturbState:(FriendModel*)model {
    return [[MXChatDBUtil sharedDataBase]updateFriendDisturbState:model];
}

///更新群人数
-(BOOL)updateGroupMemNum:(ChatGroupModel*)group {
    return [[MXChatDBUtil sharedDataBase] updateGroupMemNum:group];
}

//-(BOOL)updateGroupContactState:(ChatGroupModel*)group{
//    ChatGroupModel* orignGroup = [self loadGroupByChatId:group.groupId];
//    orignGroup.enable_contact = group.enable_contact;
//    return [[MXChatDBUtil sharedDataBase]updateSaveToContact:orignGroup];
//}
- (MessageModel*)insertMessage:(MessageModel*)msg{
    if ([StringUtil isEmpty:msg.chat_with]) {
        return nil;
    }
    //撤回消息只更新 不做新消息处理
    if (msg.subtype == kMXMessageTypeWithdraw) {
        [[MXChatDBUtil sharedDataBase] updateWithdrawRowDataByMsgModel:msg];
        return msg;
    }
    MXConversation* con = [self loadConversationObject:msg.chat_with];
    if (con == nil) {
        con = [[MXConversation alloc] init];
        con.chat_id = msg.chat_with;
        con.conversationType = msg.chatType;
        con.lastMessageId = msg.messageId;
        con.creatorRole = msg.creatorRole;
        con = [self createConversationObject:con];
    }else{
        MessageModel* latestMessage = con.latestMessage;
        if (latestMessage) {
            double timestamp1 = [latestMessage.sendTime doubleValue];
            double timestamp2 = [msg.sendTime doubleValue];
            if (timestamp2 > timestamp1) {
                con.lastMessageId = msg.messageId;
                [[MXChatDBUtil sharedDataBase]updateLastMessage:msg.chat_with associateId:msg.chat_with messageId:msg.messageId];
            }
        }else{
            con.lastMessageId = msg.messageId;
            [[MXChatDBUtil sharedDataBase]updateLastMessage:msg.chat_with associateId:msg.chat_with messageId:msg.messageId];
        }
    }
    if (![self tbExist:con.chat_id]) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,con.chat_id];
        [[MXChatDBUtil sharedDataBase]createChatTB:tbName];
        [self.chatAllTables addObject:tbName];
    }

    BOOL success = [[MXChatDBUtil sharedDataBase] insertMessage:msg];
    //判断是否@自己的信息
    if (msg.type == MessageTypeGroupchat && msg.subtype == kMXMessageTypeText) {
        NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:msg.body];
        NSString *useIds = [bodyDic[@"attr2"] description];
        if (![StringUtil isEmpty:useIds]) {
            NSArray *userIdsArr = [useIds componentsSeparatedByString:@","];
            NSString *userId = [LcwlChat shareInstance].user.user_id;
            BOOL isFind = NO;
            for (NSString *obj in userIdsArr) {
                if ([StringUtil isEmpty:obj]) {
                    continue;
                }
                if ([obj isEqualToString:userId]) {
                    isFind = YES;
                    break;
                }
            }
            if (isFind) {
                con.isAt = YES;
            }
        }
    }
    if (success) {
        if (msg.is_new == 0 ) {
            if (msg.type == MessageTypeNormal ||
                msg.type == MessageTypeGroupchat  ||
                msg.type == MessageTypeSs ) {
                if (!msg.inDisturb) {
                    [self addUnReadNums:con];
                }
            }
        }
        [con.messages addObject:msg];
        return msg;
    }else{
        return nil;
    }
    return msg;
}
-(BOOL)isNeedNoticeSys:(NSString*) chat_id array:(NSArray*)array{
    __block BOOL isExist = NO;
      [array enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        MessageModel* msg = (MessageModel *)obj;
        if(![StringUtil isEmpty:msg.fromID]){
            if ([msg.fromID isEqualToString:chat_id]) {
                if (msg.is_new == 0) {
                    isExist = YES;
                    *stop = YES;
                }
            }
        }
    }];
    return isExist;
}
//修改conversation未读数量
-(void)addUnReadNums:(MXConversation*)conversation{
    conversation.unReadNums++;
    [[MXChatDBUtil sharedDataBase]updateUnReadNums:conversation.unReadNums atStatus:conversation.isAt chatId:conversation.chat_id];
}
////清空conversation未读数量
-(void)clearUnReadNums:(NSString*)chatId{
    MXConversation* conversation = [[LcwlChat shareInstance].chatManager loadConversationObject:chatId];
    if (conversation != nil) {
        conversation.unReadNums = 0;
        conversation.isAt = NO;
        [[MXChatDBUtil sharedDataBase]updateUnReadNums:conversation.unReadNums atStatus:conversation.isAt chatId:conversation.chat_id];
    }
}

///获取所有非客服消息未读数量
-(int)getAllUnReadNums{
    int num = 0;
    @synchronized(_conversations) {
        for (MXConversation* con in _conversations) {
            if(con.creatorRole > 0) { ///过滤客服消息
                continue;
            }
            
            if (con.conversationType == eConversationTypeChat) {
                FriendModel *friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:con.chat_id];
                if ([friend isKindOfClass:friend.class]) {
                    //如果设置了免打扰 不计入未读消息数量
                    if (friend.enable_disturb) {
                        continue;
                    }
                }
                num  += con.unReadNums;
            } else if (con.conversationType == eConversationTypeGroupChat){
                ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:con.chat_id];
                if ([group isKindOfClass:ChatGroupModel.class]) {
                    //如果设置了免打扰 不计入未读消息数量
                    if (group.enable_disturb) {
                        continue;
                    }
                }
                num  += con.unReadNums;
            } else if (con.conversationType == eConversationTypeXDPYChat ||
                       eConversationTypeHDTZChat == con.conversationType) {
                num  += con.unReadNums;
            }
        }
    }
    return num;
}

///获取所有未读咨询消息
-(int)getServiceAllUnReadNums {
    int num = 0;
    @synchronized(_conversations) {
        for (MXConversation* con in _conversations) {
            if(con.creatorRole == 0) {
                continue;
            }
            
            if (con.conversationType == eConversationTypeChat) {
                FriendModel *friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:con.chat_id];
                if ([friend isKindOfClass:friend.class]) {
                    //如果设置了免打扰 不计入未读消息数量
                    if (friend.enable_disturb) {
                        continue;
                    }
                }
                num  += con.unReadNums;
            } else if (con.conversationType == eConversationTypeGroupChat){
                ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:con.chat_id];
                if ([group isKindOfClass:ChatGroupModel.class]) {
                    //如果设置了免打扰 不计入未读消息数量
                    if (group.enable_disturb) {
                        continue;
                    }
                }
                num  += con.unReadNums;
            } else if (con.conversationType == eConversationTypeXDPYChat ||
                       eConversationTypeHDTZChat == con.conversationType) {
                num  += con.unReadNums;
            }
        }
    }
    return num;
}

- (BOOL)checkEnableDisturb:(MessageModel *)message {
    return [[MXChatDBUtil sharedDataBase] checkEnableDisturb:message];;
}

-(BOOL)tbExist:(NSString*)name{
    NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,name];
    for (NSString* tb in self.chatAllTables) {
        if (![StringUtil isEmpty:tb] && [tb isEqualToString:tbName]) {
            return YES;
        }
    }
    return NO;
}
- (MessageModel*)getMessageByMsgId:(NSString*)msgId chatId:(NSString *)chatId{
    return [[MXChatDBUtil sharedDataBase] getMessageByID:msgId chatId:chatId];
}

- (BOOL)updateMsgAck:(MessageModel*)msg{
    return [[MXChatDBUtil sharedDataBase] updateMsgAck:msg];
}

- (BOOL)updateMsgReadState:(MessageModel*)msg{
     return [[MXChatDBUtil sharedDataBase] updateOtherReadState:msg];
}

-(BOOL)updateMsgFailure:(MessageModel*)msg{
    return[[MXChatDBUtil sharedDataBase] updateMessageFailureState:msg];
}


- (void)fetchOfflineMessageWithBatchNo:(NSString *)batchNo complete:(void(^)(void))complete {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/info/getOfflineMessage",LcwlServerRoot2];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{@"batch_no":batchNo?:@""})
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                NSDictionary *dataDict = dict[@"data"];
                NSString *batchNo = dataDict[@"batch_no"];
                NSArray *dataArr = dataDict[@"message_list"];
                for (NSDictionary *dic in dataArr) {
                    [[SRWebSocketManager sharedInstance] handleReceiveMessage:dic];
                }
                if ([StringUtil isEmpty:batchNo]) {
                    //处理完离线消息 开始上线
                    [SRWebSocketManager sharedInstance].isConnect = YES;
                    [self login:self.userId];
                    //刷新tab角标
                    NSMutableDictionary* userInfo = [[NSMutableDictionary alloc] initWithCapacity:0];
                    NSInteger nums = [[LcwlChat shareInstance].chatManager getAllUnReadNums];
                    [userInfo setValue:[NSNumber numberWithInteger:nums] forKey:@"nums"];
                    [userInfo setValue:[NSNumber numberWithInteger:3] forKey:@"tabBarIndex"];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"redBadgeCountChanged" object:nil userInfo:userInfo];
                    //刷新首页数据
                    [[NSNotificationCenter defaultCenter]postNotificationName:kRefreshChatList object:nil userInfo:userInfo];
                    if (complete) {
                        complete();
                    }
                } else {
                    [self fetchOfflineMessageWithBatchNo:batchNo complete:complete];
                }
            }else{
                
            }
        }).failure(^(id error){
            
        })
        .execute();
    }];
}



@end
