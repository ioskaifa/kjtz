//
//  MXChatManager.h
//  Moxian
//
//  Created by 王 刚 on 14/12/4.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MXConversation;
#import "MessageModel.h"
#import "FriendModel.h"
#import "ChatGroupModel.h"
#import "AutoTransferModel.h"
//@class MoYouModel;
//@class ChatGroupModel;
//@class GroupMemberModel;
@interface MXChatManager : NSObject

///自动转发配置信息
@property(nonatomic, strong, readwrite) AutoTransferModel *autoTransferModel;

/*!
 @method
 @brief 获取单实例
 @discussion
 @result EaseMob实例对象
 */
+ (MXChatManager *)sharedInstance;

/*!
 @method
 @brief 获取当前登录用户的会话列表
 @param append2Chat  是否加到内存中。
 YES为加到内存中。加到内存中之后, 会有相应的回调被触发从而更新UI;
 NO为不加到内存中。如果不加到内存中, 则只会直接添加进DB, 不会有SDK的回调函数被触发从而去更新UI。
 @result 会话对象列表
 */
- (void)loadAllConversationsFromDB;

/*!
 @method
 @brief 创建会话
 @discussion
 @result EaseMob实例对象
 */
- (MXConversation*)createConversationObject:(MXConversation*)conversation;
/*!
 @method
 @brief 通过chatid删除记录
 @discussion
 @result EaseMob实例对象
 */
- (BOOL)deleteConversationByChatter:(NSString *) chat_id;

/*!
 @method
 @brief 通过chatid删除记录
 @discussion
 @result EaseMob实例对象
 */

- (BOOL)deleteAllConversation;

/*!
 @method
 @brief 通过chat_id得到好友model
 @discussion
 @result EaseMob实例对象
 */
-(FriendModel*)loadFriendByChatId:(NSString*)chat_id;

/*!
 @method
 @brief 通过id得到会话
 @discussion
 @result EaseMob实例对象
 */
-(MXConversation*)loadConversationObject:(NSString*) chat_id;

/*!
 @method
 @brief 清空未读数量
 @discussion
 @result EaseMob实例对象
 */
-(void)clearUnReadNums:(MXConversation*)conversation;

/*!
 @method
 @brief 所有未读数量
 @discussion
 @result EaseMob实例对象
 */
-(int)getAllUnReadNums;

///获取所有未读咨询消息
-(int)getServiceAllUnReadNums;

- (BOOL)checkEnableDisturb:(MessageModel *)message;

/*!
 @method
 @brief 插入聊天信息
 @discussion
 @result EaseMob实例对象
 */
// 插入聊天信息
- (MessageModel*):(MessageModel*)msg;

/*!
 @method
 @brief 插入聊天信息
 @discussion
 @result EaseMob实例对象
 */
// 得到聊天信息
- (MessageModel*)getMessageByMsgId:(NSString*)msgId chatId:(NSString *)chatId;
/*!
 @method
 @brief 更新ask状态
 @discussion
 @result EaseMob实例对象
 */
// 得到聊天信息
- (BOOL)updateMsgAck:(MessageModel*)msg;
/*!
 @method
 @brief 更新read状态
 @discussion
 @result EaseMob实例对象
 */
// 得到聊天信息
- (BOOL)updateMsgReadState:(MessageModel*)msg;
/*!
 @method
 @brief 更新好友信息
 @discussion
 @result EaseMob实例对象
 */
-(BOOL)updateFriendInfo:(FriendModel*) moyou;

/*!
 @method
 @brief 更新好友信息
 @discussion
 @result EaseMob实例对象
 */
- (BOOL)insertFriend:(MoYouModel*) moyou;

/*!
 @method
 @brief 更新好友信息
 @discussion
 @result EaseMob实例对象
 */
- (BOOL)insertFriends:(NSArray*) friends;


/*!
 @method
 @brief 清理好友信息
 @discussion
 @result EaseMob实例对象
 */
- (BOOL)clearFriend:(NSString*)chat_id;

/*!
 @method
 @brief 插入群组对象
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */

- (BOOL)insertGroup:(ChatGroupModel*) group;
/*!
 @method
 @brief 删除群组对象
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
- (BOOL)removeGroup:(ChatGroupModel*) group;

/*!
 @method
 @brief 获取所有群组
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
- (NSArray *)loadAllGroupListFromDatabase;

/*!
 @method
 @brief 插入群组成员
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
- (BOOL)insertGroupMember:(FriendModel*) member;

/*!
 @method
 @brief 获取群组某个成员
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
- (FriendModel*)selectGroupMember:(NSString*) roomId memberId:(NSString*)memberId;

/*!
 @method
 @brief 获取群组某个成员
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
- (NSMutableArray*)loadGroupMembers:(NSString*) roomId ;


/*!
 @method
 @brief 获取某个群组
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
//- (ChatGroupModel*)loadGroupByChatId:(NSString*) groupId;

/*!
 @method
 @brief 退出群组
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
- (void)asyncDissolveGroup:(NSString *)groupId completion:(MXHttpRequestCallBack)callback;

/*!
 @method
 @brief 修改群组的置顶状态
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
-(BOOL)updateGroupTopState:(ChatGroupModel*)group;

/*!
 @method
 @brief 修改群组的免打扰状态
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
-(BOOL)updateGroupDisturbState:(ChatGroupModel*)group;

-(BOOL)updatePersonalDisturbState:(FriendModel*)model;

-(BOOL)updateGroupMemNum:(ChatGroupModel*)group;
/*!
 @method
 @brief 修改群组的保存到通讯录状态
 @discussion
 @param chatter 需要获取会话对象的用户名, 对于群组是群组ID，聊天室则是聊天室ID
 @result 会话对象
 */
//-(BOOL)updateGroupContactState:(ChatGroupModel*)group;

-(id)getLastestConversation;

-(BOOL)isBelongExistConversation:(NSString*)chatID;

-(BOOL)clearGroup;

-(NSMutableArray*)getShowGroupList;

@end
