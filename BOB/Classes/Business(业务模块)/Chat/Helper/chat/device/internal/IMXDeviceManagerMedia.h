//
//  IMXDeviceManagerMedia.h
//  MoPal_Developer
//
//  Created by aken on 15/3/24.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "MXChatVoice.h"

@protocol IMXDeviceManagerMedia <NSObject>


// 播放语音文件
- (void)didPlayAudio:(NSString *)aFilePath error:(NSError *)error;

- (void)didPlayAudio:(NSString *)aFilePath error:(NSError *)error completion:(void(^)(BOOL successfully))completion;

// 获取peakPower
- (float)peekRecorderVoiceMeter;

// 开始语录
- (void)startRecordAudio:(NSError *)error ;

// 停止录音
//- (void)didStopRecordAudio:(void(^)(MXChatVoice* voice,NSError *error))completion;

// 暂停录音
- (void)pauseRecordAudio;

// 结束播放声音
-(void)endPlayAudio;

// 播放共享
- (void)audioSessionAmbient;

@end
