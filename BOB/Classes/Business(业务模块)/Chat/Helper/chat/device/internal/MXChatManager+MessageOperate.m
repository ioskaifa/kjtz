//
//  MXChatManager+MessageOperate.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/8/18.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "MXChatManager+MessageOperate.h"
#import "MXChatDefines.h"


//#import "MXChat.h"
//#import "MXConversation.h"
//#import "NSObject+Additions.h"
//#import "TalkManager.h"
//#import "SDImageCache.h"
@implementation MXChatManager (MessageOperate)


//得到聊天头的类型
+(NSString*)getChatHeaderType:(EMConversationType)chatType{
    if(chatType == eConversationTypeGroupChat){
        return @"groupchat";
    }
    return @"chat";
}
//
//+(EMConversationType)getTypeByChatType:(XMPPMessage*) message{
//    NSString* type = message.type;
//    if([message.type isEqualToString:@"chat"]) {
//        return eConversationTypeChat;
//    }else if([type isEqualToString:@"groupchat"]){
//        return eConversationTypeGroupChat;
//    }
//    return eConversationTypeChat;
//}
//
//+(XMPPMessage*)createXMPPMainTag:(MessageModel*) message{
//    XMPPMessage *aMessage = nil;
//    if (message.chatType == eConversationTypeGroupChat) {
//        NSString* fromUser = [NSString stringWithFormat:@"%@@%@",message.fromID,TestMotalkHost1];
//        NSString* toUser = [NSString stringWithFormat:@"%@@%@%@",message.toID,@"conference.",TestMotalkHost1];
//        aMessage=[XMPPMessage messageWithType:[self getChatHeaderType:message.chatType] to:[XMPPJID jidWithString:toUser] elementID:message.msgID];
//        [aMessage addAttributeWithName:@"from" stringValue:fromUser];
//
//    }else{
//        NSString *tempToUserId = [NSString stringWithFormat:@"%@",message.toID];
//        if (([tempToUserId rangeOfString:@"_"].location !=NSNotFound)) {
//            NSArray* userArray = [tempToUserId componentsSeparatedByString:@"_"];
//            if (userArray.count == 3) {
//                tempToUserId = [NSString stringWithFormat:@"%@_%@",userArray[0],userArray[1]];
//            }
//        }
//        aMessage=[XMPPMessage messageWithType:[self getChatHeaderType:message.chatType] to:[XMPPJID jidWithString:[NSString stringWithFormat:@"%@@%@",tempToUserId,TestMotalkHost1]] elementID:message.msgID];
//        NSString* account = [NSString stringWithFormat:@"%@@%@",message.fromID,TestMotalkHost1];
//        [aMessage addAttributeWithName:@"from" stringValue:account];
//    }
//    return aMessage;
//}
////创建回执对象
//+(XMPPMessage*)createReceiptXMPPMainTag:(MessageModel*) message value:(NSString*)value{
//    //    //生成消息对象
//
//    NSString* toUser = TestMotalkHost1;
//    NSDictionary* shopidBody = nil;
//    if (![value isEqualToString:@"received"]) {
//        if (message.type == MessageTypeNormal || message.type == MessageTypeRich) {
//            NSString *tempToUserId = [NSString stringWithFormat:@"%@",message.fromID];
//            if (([tempToUserId rangeOfString:@"_"].location !=NSNotFound)) {
//                NSArray* tempArray = [tempToUserId componentsSeparatedByString:@"_"];
//                tempToUserId = [NSString stringWithFormat:@"%@_%@",tempArray[0],tempArray[1]];
//                shopidBody = @{@"shopid":tempArray[2]};
//            }
//            toUser = [NSString stringWithFormat:@"%@@%@",tempToUserId,TestMotalkHost1];
//        }
//    }
//    XMPPMessage *aMessage=[XMPPMessage messageWithType:[self getChatHeaderType:eConversationTypeChat] to:[XMPPJID jidWithString:toUser] elementID:message.msgID];
//
//    if(shopidBody != nil){
//        [aMessage addChild:[DDXMLNode elementWithName:@"body" stringValue:[MXJsonParser dictionaryToJsonString:shopidBody]]];
//    }
//    return aMessage;
//}
////普通消息
//+(DDXMLElement*)createNormalXMPPExtendTag:(MessageModel*) message value:(NSString*)value{
//    // 添加扩展标签
//    DDXMLElement *ele=[DDXMLNode elementWithName:@"mx" stringValue:nil];
//    [ele addAttributeWithName:@"xmlns" stringValue:@"urn:xmpp:mx"];
//    [ele addAttributeWithName:@"id" stringValue:message.msgID];
//    [ele addAttributeWithName:@"v" stringValue:value];
//    [ele addAttributeWithName:@"subtype" stringValue:[NSString stringWithFormat:@"%@",@(message.subtype)]];
//    if ([value isEqualToString:@"read"]) {
//        [ele addAttributeWithName:@"from" stringValue:[MXChat sharedInstance].getLoginID];
//    }
//    NSString *tempToUserId = [NSString stringWithFormat:@"%@",message.toID];
//    if (([tempToUserId rangeOfString:@"_"].location !=NSNotFound)) {
//        NSArray* userArray = [tempToUserId componentsSeparatedByString:@"_"];
//        if (userArray.count == 3) {
//                [ele addAttributeWithName:@"shopid" stringValue:userArray[2]];
//        }
//    }
//
//    if (message.type == MessageTypeNormal) {
//        [ele addAttributeWithName:@"ty" stringValue:@"normal"];
//    }else if(message.type == kMXMessageTypeDestroy){
//       [ele addAttributeWithName:@"ty" stringValue:KDelivery_state_destroy];
//    }else if(message.type == MessageTypeRich){
//       [ele addAttributeWithName:@"ty" stringValue:KDelivery_state_rich];
//    }else if(message.type == MessageTypeGroupchat){
//        [ele addAttributeWithName:@"ty" stringValue:KDelivery_state_group];
//    }else if(message.type == MessageTypeSgroup){
//        [ele addAttributeWithName:@"ty" stringValue:KDelivery_state_sgroup];
//    }else if(message.type == MessageTypeSs){
//        [ele addAttributeWithName:@"ty" stringValue:KDelivery_state_s];
//    }else if(message.type == kMXMessageTypeNoticeSystem){
//        [ele addAttributeWithName:@"ty" stringValue:KDelivery_state_ss];
//    }else if(message.type == kMXMessageTypeFollow){
//        [ele addAttributeWithName:@"ty" stringValue:KDelivery_state_follow];
//    }
//    return ele;
//}
////销毁消息
//+(DDXMLElement*)createDestroyXMPPExtendTag:(MessageModel*) message value:(NSString*)value{
//    // 添加扩展标签
//    DDXMLElement *ele = [self createNormalXMPPExtendTag:message value:value];
//    [ele addAttributeWithName:@"ty" stringValue:@"destroy"];
//    return ele;
//}
//
//+(MessageModel*)parseTextMessageToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
//
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options: NSJSONReadingMutableContainers
//                                                                error: nil];
//    model.toID=[message to].user;
//    model.content=[NSString stringWithFormat:@"%@",dictionary[@"msg"]];
//    model.msgID=[[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    model.name = dictionary[@"name"];
//    model.avatar = dictionary[@"avatar"];
//    model.shopId = dictionary[@"shopid"];
//    model.body = body;
//    return model;
//}
//+(MessageModel*)parseImageMessageToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options: NSJSONReadingMutableContainers
//                                                                error: nil];
//    model.toID=[message to].user;
//    model.content=[NSString stringWithFormat:@"%@",dictionary[@"msg"]];
//    model.msgID=[[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    model.name = dictionary[@"name"];
//    model.avatar = dictionary[@"avatar"];
//    if (!dictionary[@"url"]) {
//        model.maskFileUrl = [NSString stringWithFormat:@"%@",dictionary[@"url"]];
//    } else {
//        model.maskFileUrl = dictionary[@"url"];
//    }
//    NSString* path = [model.maskFileUrl appendImgPath:model.maskFileUrl];
//    model.locFileUrl = [[SDImageCache sharedImageCache] defaultCachePathForKey:path];
//    model.name = dictionary[@"name"];
//    model.avatar = dictionary[@"avatar"];
//    model.shopId = dictionary[@"shopid"];
//    model.body = body;
//    int width = [dictionary[@"width"] intValue];
//    int height = [dictionary[@"height"] intValue];
//    if (width != 0 && height != 0) {
//        model.size = CGSizeMake(width, height);
//    }
//    return model;
//}
//+(MessageModel*)parseVoiceMessageToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options: NSJSONReadingMutableContainers
//                                                                error: nil];
//
//    model.toID=[message to].user;
//    model.content=[NSString stringWithFormat:@"%@",dictionary[@"msg"]];
//    model.msgID=[[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    model.name = dictionary[@"name"];
//    model.avatar = dictionary[@"avatar"];
//    model.fileUrl=[NSString stringWithFormat:@"%@",dictionary[@"url"]];
//    model.voiceLen=[NSString stringWithFormat:@"%@",dictionary[@"len"]];
//    model.content=[NSString stringWithFormat:@"%@",dictionary[@"url"]];
//    model.name = dictionary[@"name"];
//    model.avatar = dictionary[@"avatar"];
//    model.shopId = dictionary[@"shopid"];
//    model.body = body;
//    return model;
//}
//+(MessageModel*)parseLocationMessageToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options: NSJSONReadingMutableContainers
//                                                                error: nil];
//
//    model.toID=[message to].user;
//    model.content=[NSString stringWithFormat:@"%@",dictionary[@"msg"]];
//    model.msgID=[[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    model.name = dictionary[@"name"];
//    model.avatar = dictionary[@"avatar"];
//    model.lat=[NSString stringWithFormat:@"%@",dictionary[@"x"]];
//    model.lng=[NSString stringWithFormat:@"%@",dictionary[@"y"]];
//    if ([StringUtil isEmpty:[NSString stringWithFormat:@"%@",dictionary[@"addr"]]]) { // android传过来的
//        model.addr=[NSString stringWithFormat:@"%@",dictionary[@"msg"]];
//    }else{
//        model.addr=[NSString stringWithFormat:@"%@",dictionary[@"addr"]];
//    }
//    model.name = dictionary[@"name"];
//    model.avatar = dictionary[@"avatar"];
//    model.shopId = dictionary[@"shopid"];
//    model.body = body;
//    return model;
//}
//+(MessageModel*)parseGifMessageToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options: NSJSONReadingMutableContainers
//                                                                error: nil];
//    model.toID=[message to].user;
//    model.content=[NSString stringWithFormat:@"%@",dictionary[@"msg"]];
//    model.msgID=[[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    model.name = dictionary[@"name"];
//    model.avatar = dictionary[@"avatar"];
//    model.fileUrl=body;
//    if (!dictionary[@"url"]) {
//        model.maskFileUrl = [NSString stringWithFormat:@"%@",dictionary[@"url"]];
//    } else {
//        model.maskFileUrl = dictionary[@"url"];
//    }
//
//    model.locFileUrl = [[SDImageCache sharedImageCache] defaultCachePathForKey:model.maskFileUrl];
//    // 特殊处理
//    // 1428129244959
////    if (model.sendTime.length > 10) {
////        model.sendTime=[model.sendTime substringToIndex:10];
////    }
//    model.name = dictionary[@"name"];
//    //    model.sex = [dictionary[@"gender"] intValue];
//    model.avatar = dictionary[@"avatar"];
//    model.shopId = dictionary[@"shopid"];
//    model.body = body;
//    return model;
//}
//
//
//+(MessageModel*)parseRichMessageToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
////    NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:model.body];
//    model.fromID = [message from].user;
//    model.chat_with = model.fromID;
//    model.toID=[message to].user;
//
//    return model;
//}
//
//+(MessageModel*)parseInviteMessageToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary = [MXJsonParser jsonToDictionary:body];
//
////    model.sendTime=[NSString stringWithFormat:@"%@",dictionary[@"ts"]];
//    model.content = dictionary[@"key"];
//    model.fromID = [message from].user;
//    model.chat_with = model.fromID;
//    model.toID=[message to].user;
//    return model;
//}
//+(MessageModel*)parseAddMemberMessageToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options: NSJSONReadingMutableContainers
//                                                                error: nil];
//    model.fromID = dictionary[@"roomId"];
//    model.chat_with = model.fromID;
//    model.toID=[message to].user;
//    return model;
//}
//+(MessageModel*)parseUpdateAttributeToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options: NSJSONReadingMutableContainers
//                                                                error: nil];
//    model.fromID = dictionary[@"roomId"];
//    model.chat_with = model.fromID;
//    model.toID = [message to].user;
//    model.name = dictionary[@"roomName"];
//    model.avatar = dictionary[@"photoUrl"];
//
//    return model;
//}
//
//+(MessageModel*)parseAddManagersToModel:(XMPPMessage*)message withMessageModel:(MessageModel*)model{
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options: NSJSONReadingMutableContainers
//                                                                error: nil];
//    model.fromID = dictionary[@"roomId"];
//    model.chat_with = model.fromID;
//    model.toID = [message to].user;
//    model.name = dictionary[@"roomName"];
//    model.avatar = dictionary[@"photoUrl"];
//
//    return model;
//}
//
//
//
//+(MessageModel*)modelWithFocusMessage:(XMPPMessage*)message{
//    MessageModel* model = [[MessageModel alloc]init];
//    int messageType = [[[[message elementForName:kExternXmlTag] attributeForName:@"subtype"] stringValue]intValue];
//    model.chatType = [self getTypeByChatType:message];
//    model.subtype = messageType;
//    model.type = kMXMessageTypeFollow;
//    model.msg_direction = 0;
//    model.msgID=[[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    return model;
//}
//
//+(MessageModel*)modelWithMessage:(XMPPMessage*)message{
//    MessageModel* model = [[MessageModel alloc]init];
//    BOOL isRequest = [self isRequest:message];
//    if (isRequest) {
//        int messageType = [[[[message elementForName:kExternXmlTag] attributeForName:@"subtype"] stringValue]intValue];
//        model.chatType = [self getTypeByChatType:message];
//        model.msg_direction = 0;
//        model.subtype = messageType;
//        model.sendTime = [[[message elementForName:kExternXmlTag] attributeForName:@"ts"]stringValue];
//        if (model.chatType == eConversationTypeGroupChat) {
//            NSString* from = [message from].resource;
//            //发送的信息格式不一致 特殊处理
//            if ([StringUtil isEmpty:from]) {
//                model.fromID = [message from].user;
//            }else{
//                model.fromID = [message from].resource;
//            }
//            model.chat_with = [message from].user;
//        }else{
//            NSString* fromID = [message from].user;
//            if ([fromID rangeOfString:@"_"].location!=NSNotFound) {
//                NSString* fromID = [message from].user;
//                NSString* shopID = [[[message elementForName:kExternXmlTag] attributeForName:@"shopid"] stringValue];
//                if ([StringUtil isEmpty:shopID] || [StringUtil isEmpty:fromID]) {
//                    return nil;
//                }
//                model.fromID= [NSString stringWithFormat:@"%@_%@",fromID,[NSString stringWithFormat:@"%@",shopID]];
//                model.chat_with = model.fromID;
//                model.chatType = econversationTypeSubMopromoChat;
//            }else{
//                model.fromID= [message from].user;
//                model.chat_with = model.fromID;
//            }
//        }
//        if (messageType == kMXMessageTypeText) {
//            model = [self parseTextMessageToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeImage){
//            model = [self parseImageMessageToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeVoice){
//            model = [self parseVoiceMessageToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeLocation){
//            model = [self parseLocationMessageToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeGif){
//            model = [self parseGifMessageToModel:message withMessageModel:model];
//        }else if(messageType == 0){
//            model.msgID=[[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//        }
//        if ([[MXChat sharedInstance].chatManager isBelongExistConversation:model.chat_with]) {
//            model.is_new = 1;
//        }else{
//            model.is_new = 0;
//        }
//    }
//    return model;
//}
////subtype    是    Integer    1 群基本属性更新(群昵称， 群图片，群主) 2 新增管理员列表 3 取消管理员 4 新增群成员 5 删除群成员 6 群解散
//+(MessageModel*)modelWithGroupSystemMessage:(XMPPMessage*)message{
//    MessageModel* model = [[MessageModel alloc]init];
//    BOOL isRequest = [self isRequest:message];
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    if (isRequest) {
//        int messageType = [[[[message elementForName:kExternXmlTag] attributeForName:@"subtype"] stringValue]intValue];
//        model.chatType = eConversationTypeGroupChat;
//        model.subtype = messageType;
//        model.msg_direction = 0;
//        model.type = MessageTypeSgroup;
//        model.body = body;
//        model.sendTime=[[[message elementForName:kExternXmlTag] attributeForName:@"ts"] stringValue];
//        model.msgID=[[message attributeForName:@"id"]stringValue];
//
//        if (messageType == kMXMessageTypeUpdateAttribute) {
//            model = [self parseUpdateAttributeToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeAddManagers){
//            model = [self parseAddManagersToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeCancelManagers){
//            model = [self parseAddManagersToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeAddMembers){
//            model = [self parseAddManagersToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeDeleteMembers){
//            model = [self parseAddManagersToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeRemoveGroup){
//            model = [self parseAddManagersToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeUpdateMyNickname){
//            model = [self parseAddManagersToModel:message withMessageModel:model];
//        }else if(messageType == kMXMessageTypeCreateGroup){
//            model = [self parseAddManagersToModel:message withMessageModel:model];
//        }
//        if ([[MXChat sharedInstance].chatManager isBelongExistConversation:model.chat_with]) {
//            model.is_new = 1;
//        }else{
//            model.is_new = 0;
//        }
//
//    }
//    return model;
//
//}
//+(MessageModel*)modelWithSystemMessage:(XMPPMessage*)message{
//    MessageModel* model = [[MessageModel alloc]init];
//    BOOL isRequest = [self isRequest:message];
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    if (isRequest) {
//        int messageType = [[[[message elementForName:kExternXmlTag] attributeForName:@"subtype"] stringValue]intValue];
//        model.chatType = eConversationTypeChat;
//        model.subtype = messageType;
//        model.msg_direction = 0;
//        model.type = MessageTypeSs;
//        model.body = body;
//        model.msgID=[[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//        model.sendTime = [[[message elementForName:kExternXmlTag] attributeForName:@"ts"] stringValue];
//        if (messageType == kMXMessageTypeInviteMember) {
//            model = [self parseInviteMessageToModel:message withMessageModel:model];
//        }
//    }
//    if ([[MXChat sharedInstance].chatManager isBelongExistConversation:model.chat_with]) {
//        model.is_new = 1;
//    }else{
//        model.is_new = 0;
//    }
//
//    return model;
//
//}
//
//+(MessageModel*)modelWithRichMessage:(XMPPMessage*)message{
//    MessageModel* model = [[MessageModel alloc]init];
//    BOOL isRequest = [self isRequest:message];
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    if (isRequest) {
//        int messageType = [[[[message elementForName:kExternXmlTag] attributeForName:@"subtype"] stringValue]intValue];
//        model.chatType = eConversationTypeChat;
//        model.subtype = messageType;
//        model.msg_direction = 0;
//        model.type = MessageTypeRich;
//        model.body = body;
//        model.msgID=[[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//         model.sendTime = [NSString stringWithFormat:@"%@",[[[message elementForName:kExternXmlTag] attributeForName:@"ts"] stringValue]];
//        if (messageType == kMXMessageTypeVoucher || messageType == kMxmessageTypeCertificate) {
//            if ([TalkManager isShop:[message from].user]) {
//                NSString *shopId =[[[message elementForName:kExternXmlTag] attributeForName:@"shopid"] stringValue];
//                NSString *from = [NSString stringWithFormat:@"%@_%@",[message from].user,shopId];
//                model.fromID = from;//[message from].user;
//                model.chat_with =from;
//                model.toID=[message to].user;
//                model.shopId = shopId;
//                model.chatType = eConversationTypeChat;
//            }else{
//                model = [self parseRichMessageToModel:message withMessageModel:model];
//            }
//        }else if(messageType == kMXMessageTypeGoods) {
//            NSString *shopId =[[[message elementForName:kExternXmlTag] attributeForName:@"shopid"] stringValue];
//            NSString *from = [NSString stringWithFormat:@"%@_%@",[message from].user,shopId];
//            model.fromID = from;//[message from].user;
//            model.chat_with =from;
//            model.toID=[message to].user;
//            model.shopId = shopId;
//            model.chatType = eConversationTypeChat;
//        }
//        NSString *body = [[message elementForName:@"body"] stringValue];
//        NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                                  options: NSJSONReadingMutableContainers
//                                                                    error: nil];
//        model.name = dictionary[@"name"];
//        model.avatar = dictionary[@"avatar"];
//    }
//    if ([[MXChat sharedInstance].chatManager isBelongExistConversation:model.chat_with]) {
//        model.is_new = 1;
//    }else{
//        model.is_new = 0;
//    }
//    if (model.msg_direction) {
//        model.is_new = 1;
//        model.msg_direction = 1;
//    }
//
//    return model;
//}
//
//
//+(MessageModel*)modelWithNoticeMessage:(XMPPMessage*)message{
//    MessageModel* model = [[MessageModel alloc]init];
//    BOOL isRequest = [self isRequest:message];
//    if (isRequest) {
//        int messageType = [[[[message elementForName:kExternXmlTag] attributeForName:@"subtype"] stringValue]intValue];
//        if (messageType == 1 || messageType == 3 ||messageType == 15||messageType == 16|| messageType == 22 || messageType == 24 || messageType == 25 || messageType == 26 || messageType == 23 || messageType == 21 || messageType == 37 || messageType == 38 ||
//            messageType == 43 || messageType == 44) {
//
//            model = [self parseNoticeToNomalMessage:message model:model];
//
//            // 这里比较特殊，boss后台推过来的系统消息的from id不是小魔100001，此处做一个处理
//            model.fromID = @"100001";
//
//        }else if(messageType == 4 || messageType == 2){
//            model = [self parseNoticeMessage:message model:model];
//        }else if (messageType == -4) {
//
//            model = [self parseNoticeMessage:message model:model];
//        } else if(messageType == 54) {
//            model = [self modelWithSystemMessage:message];
//        }
//    }
//    if ([[MXChat sharedInstance].chatManager isBelongExistConversation:model.chat_with]) {
//        model.is_new = 1;
//    }else{
//        model.is_new = 0;
//    }
//    model.msg_direction = 0;
//    return model;
//}
//
//+(NSString*)replaceMJID:(NSString*)str{
//    NSRange range = [str rangeOfString:@"@"];
//    if (range.location != NSNotFound) {
//        return  [str substringToIndex:range.location];
//    }
//    return str;
//}
//
//+(MessageModel*)modelWithDestroyMessage:(XMPPMessage*)message{
//    MessageModel *model = [[MessageModel alloc] init];
//    NSString* vValue = [[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    model.msgID = vValue;
//    model.type = kMXMessageTypeDestroy;
//    model.subtype = kMXMessageTypeDestroy;
//    model.fromID= [message from].user;
//    model.chat_with = model.fromID;
//    if ([[MXChat sharedInstance].chatManager isBelongExistConversation:model.chat_with]) {
//        model.is_new = 1;
//    }else{
//        model.is_new = 0;
//    }
//    model.msg_direction = 0;
//    [self parseDestroyMessage:message model:model];
//
//    return model;
//}
//+(MessageModel*)parseDestroyMessage:(XMPPMessage*)message model:(MessageModel*) model{
//
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    if ([StringUtil isEmpty:body]) {
//        return model;
//    }
//    NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData: [body dataUsingEncoding:NSUTF8StringEncoding]
//                                                              options: NSJSONReadingMutableContainers
//                                                                error: nil];
//    model.toID=[message to].user;
//    model.name = dictionary[@"name"];
//    model.avatar = dictionary[@"avatar"];
//
////    model.chat_with = dictionary[@"id"];
////    model.fromID = model.chat_with;
//    model.body = body;
//    return model;
//}
//
//+(MessageModel*)parseNoticeToNomalMessage:(XMPPMessage*)message model:(MessageModel*) model{
//    model.chatType = [self getTypeByChatType:message];
//    model.type = MessageTypeNormal;
//    model.fromID = [message from].user;
//    model.chat_with = model.fromID;
//    model.toID = [message to].user;
//    model.msg_direction = 0;
//    model.msgID = [[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    model.sendTime = [[[message elementForName:kExternXmlTag] attributeForName:@"ts"] stringValue];
//    model.subtype = 1;
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary = [MXJsonParser jsonToDictionary:body];
//    NSString* content = [NSString stringWithFormat:@"%@",dictionary[@"field1"]];
//    NSDictionary* bodyDict = @{@"msg":content};
//    model.body = [MXJsonParser dictionaryToJsonString:bodyDict];
//    model.content = content;
//    return model;
//}
//+(MessageModel*)parseNoticeMessage:(XMPPMessage*)message model:(MessageModel*) model{
//    model.chatType = eConversationTypeNoticeChat;
//    model.type = kMXMessageTypeNoticeSystem;
//    model.fromID = [message from].user;
//    model.toID = [message to].user;
//    model.chat_with = NewFriendChatWith;
//    model.msg_direction = 0;
//    model.msgID = [[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    model.sendTime = [[[message elementForName:kExternXmlTag] attributeForName:@"ts"] stringValue];
//    model.body = [[message elementForName:@"body"] stringValue];
//    NSString *body = [[message elementForName:@"body"] stringValue];
//    NSDictionary *dictionary = [MXJsonParser jsonToDictionary:body];
//    model.subtype = [[[[message elementForName:kExternXmlTag] attributeForName:@"subtype"] stringValue]intValue];
//    model.content = dictionary[@"field1"];
//    return model;
//}
//
//// 注册时自动关注
//+(MessageModel*)parseRisterAutoFocusNoticeMessage:(XMPPMessage*)message model:(MessageModel*) model {
//    model.chatType = eConversationTypeChat;
//    model.type = kMXMessageTypeFollow;
//    model.fromID = [message from].user;
//    model.chat_with = model.fromID;
//    model.toID = [message to].user;
//    model.msg_direction = 0;
//    model.msgID = [[[message elementForName:kExternXmlTag] attributeForName:@"id"] stringValue];
//    model.sendTime = [[[message elementForName:kExternXmlTag] attributeForName:@"ts"] stringValue];
//
//    model.body = [[message elementForName:@"body"] stringValue];
//    NSString *body = [[message elementForName:@"body"] stringValue];
//
//    NSDictionary *dictionary = [MXJsonParser jsonToDictionary:body];
//    model.subtype = 1;
//
//    model.avatar = dictionary[@"field4"];
//
//    return model;
//
//}
//
//+(BOOL)isRequest:(XMPPMessage* )message{
//    NSString* vValue = [[[message elementForName:kExternXmlTag] attributeForName:@"v"] stringValue];
//    return [vValue isEqualToString:KDelivery_state_nomal];
//}
@end
