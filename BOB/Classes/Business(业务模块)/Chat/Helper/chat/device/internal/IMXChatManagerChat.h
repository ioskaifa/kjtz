//
//  IMXChatManagerChat.h
//  Moxian
//
//  Created by litiankun on 14/12/3.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMXChatManagerBase.h"

@protocol SendMessageDelegate<NSObject>
@optional

-(void)uploadMessageFailureCallback:(MessageModel*)msg;

@end

@class MessageModel;

@protocol IMXChatManagerChat <IMXChatManagerBase>


/*!
 @method
 @brief 异步方法, 发送一条消息
 @discussion 待发送的消息对象和发送后的消息对象是同一个对象, 在发送过程中对象属性可能会被更改
 @param message  消息对象(包括from, to, body列表等信息)
 @param completion       发送消息完成后的回调
 @result 发送的消息对象(因为是异步方法, 不能作为发送完成或发送成功失败与否的判断)
 */
- (void)sendMessage:(MessageModel *)message
                     completion:(void (^)(MessageModel *message,
                                          NSError *error))completion;


-(void)sendReceipt:(MessageModel *)message ;

///发送语音消息
-(void)sendVoiceMessage:(MessageModel*)message delegate:(id<SendMessageDelegate>) sender;

///发送图片消息
-(void)sendImageMessage:(MessageModel*)message delegate:(id<SendMessageDelegate>) sender;

///发送视频消息
-(void)sendVideoMessage:(MessageModel*)message delegate:(id<SendMessageDelegate>) sender;

///发送文件word excel pdf....
-(void)sendFileMessage:(MessageModel*)message delegate:(id<SendMessageDelegate>) sender;

///获取离线消息
- (void)fetchOfflineMessageWithBatchNo:(NSString *)batchNo complete:(void(^)(void))complete;

@end
