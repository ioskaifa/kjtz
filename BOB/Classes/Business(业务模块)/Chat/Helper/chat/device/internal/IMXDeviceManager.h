//
//  IMXDeviceManager.h
//  MoPal_Developer
//
//  MXDeviceManager各类协议的合集
//  Created by aken on 15/3/24.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IMXDeviceManagerMedia.h"
#import "IMXDeviceManagerProximitySensor.h"
#import "IMXDeviceManagerBase.h"

/*!
 @protocol
 @brief MXDeviceManager各类协议的合集
 @discussion
 */

@protocol IMXDeviceManager <IMXDeviceManagerProximitySensor,IMXDeviceManagerMedia,IMXDeviceManagerBase>

@end
