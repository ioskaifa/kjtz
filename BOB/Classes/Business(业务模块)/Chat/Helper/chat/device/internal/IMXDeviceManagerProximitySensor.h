//
//  IMXDeviceManagerProximitySensor.h
//  MoPal_Developer
//
//  Created by aken on 15/3/24.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>


/*!
 @protocol
 @brief ProximitySensor协议
 @discussion
 */


@protocol IMXDeviceManagerProximitySensor <NSObject>

@end
