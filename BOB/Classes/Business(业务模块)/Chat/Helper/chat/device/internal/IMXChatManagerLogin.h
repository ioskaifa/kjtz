//
//  MXChatManagerLogin.h
//  Moxian
//
//  Created by litiankun on 14/12/2.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMXChatManagerBase.h"

@protocol IMXChatManagerLogin

/*!
 @property
 @brief 当前登录的用户信息
 */
@property (nonatomic, strong, readonly) NSDictionary *loginInfo;

@optional
- (void)login:(NSString *)userId;

// add by yang.xiangbao 2015/6/11
- (void)loginOut;

@end
