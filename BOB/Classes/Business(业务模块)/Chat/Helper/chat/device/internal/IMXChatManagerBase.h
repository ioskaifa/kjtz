//
//  MXChatManagerBase.h
//  Moxian
//
//  Created by litiankun on 14/12/2.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMXChatManagerDelegate.h"
#import "IMXChatManagerBase.h"
/*!
 @protocol
 @brief 聊天的基础协议, 用于注册对象到监听列表和从监听列表中移除对象
 @discussion
 */

@protocol IMXChatManagerBase <NSObject>
@required
/*!
 @method
 @brief 注册一个监听对象到监听列表中
 @discussion 把监听对象添加到监听列表中准备接收相应的事件
 @param delegate 需要注册的监听对象
 @param aQueue 通知监听对象时的线程
 @result
 */
- (void)addDelegate:(id<IMXChatManagerDelegate>)delegate;

/*!
 @method
 @brief 从监听列表中移除一个监听对象
 @discussion 把监听对象从监听列表中移除,取消接收相应的事件
 @param delegate 需要移除的监听对象
 @result
 */
- (void)removeDelegate:(id<IMXChatManagerDelegate>)delegate;

// 移除所有的代理
- (void)removeAllChatDelegate;
@end
