//
//  SRWebSocketManager.m
//  Lcwl
//
//  Created by mac on 2018/12/10.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "SRWebSocketManager.h"
#import "SRWebSocket.h"
#import "MXJsonParser.h"
#import "ChatViewVC.h"
#import "FriendModel.h"
#import "TalkManager.h"
#import "MXChatMessageHandler.h"
#import "ChatSendHelper.h"
#import "AppDelegate+Helper.h"
#import "LcwlChat.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "ChatVoiceManage.h"
#define kDelivery_state_acked @"ACK" //回执
#define kDelivery_state_ss @"SS"  //系统消息
#define kDelivery_state_poppush @"POPPUSH"  //系统公告消息
#define kDelivery_state_sgroup @"SGROUP" //群系统消息
#define kDelivery_state_kickoff @"KICKOFF" //踢下线
#define KDelivery_state_nomal @"NORMAL" //普通消息，文字，图片...
#define KDelivery_state_group @"GROUPCHAT" //群里的普通消息
#define dispatch_main_async_safe(block)\
if ([NSThread isMainThread]) {\
block();\
} else {\
dispatch_async(dispatch_get_main_queue(), block);\
}
typedef enum : NSUInteger {
    disConnectByUser = 1000 ,
    disConnectByServer,
} DisConnectType;

static const uint16_t Kport = 6969;


@interface SRWebSocketManager()<SRWebSocketDelegate>
{
    SRWebSocket *webSocket;
    NSTimeInterval reConnectTime;
}
@property (nonatomic , strong) NSTimer *heartBeat;

@property (nonatomic , strong) NSString *userId;

@property (nonatomic) NSInteger connectState;


//id<LcwlSRWebSocketDelegate>)objects
@end

@implementation SRWebSocketManager
+ (SRWebSocketManager*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

-(instancetype)init{
    if (self = [super init]) {
    }
    return self;
}
//初始化连接
- (void)initSocket
{
    if (webSocket) {
        return;
    }
    
    NSString* webscoket_path = UDetail.user.webscoket_path;
    NSString* token = UDetail.user.chatToken;
    if ([StringUtil isEmpty:webscoket_path] || [StringUtil isEmpty:token]) {
        return;
    }
    NSString* path = [[NSString stringWithFormat:@"%@/iOS_%@",webscoket_path,token] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    webSocket = [[SRWebSocket alloc]initWithURL:[NSURL URLWithString:path]];    
    webSocket.delegate = self;
    
    //设置代理线程queue
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    queue.maxConcurrentOperationCount = 1;
    
    [webSocket setDelegateOperationQueue:queue];
    
    //连接
    [webSocket open];
    MLog(@"聊天连接-开始连接 ---- 聊天URL:%@",path);
}

//初始化心跳
- (void)initHeartBeat
{
    
    if (@available(iOS 10.0, *)) {
        dispatch_main_async_safe((^{
            
            [self destoryHeartBeat];
            
            __weak typeof(self) weakSelf = self;
            //心跳设置为3分钟，NAT超时一般为5分钟
            self.heartBeat = [NSTimer scheduledTimerWithTimeInterval:15 repeats:YES block:^(NSTimer * _Nonnull timer) {
                //和服务端约定好发送什么作为心跳标识，尽可能的减小心跳包大小
                NSDictionary* pong = @{@"sessionType":@"PING", @"from":UDetail.user.chatUser_id};
                [weakSelf sendMsg:[MXJsonParser dictionaryToJsonString:pong]];
            }];
            [[NSRunLoop currentRunLoop] addTimer:self.heartBeat forMode:NSRunLoopCommonModes];
        }))
    } else {
        // Fallback on earlier versions
    }
    
}

//取消心跳
- (void)destoryHeartBeat
{
    @weakify(self)
    dispatch_main_async_safe(^{
        @strongify(self)
        if (self.heartBeat) {
            [self.heartBeat invalidate];
            self.heartBeat = nil;
        }
    })
    
}


#pragma mark - 对外的一些接口

//建立连接
- (void)connect:(NSString *)userid
{
    if (!self.isConnect) {
        return;
    }
    self.userId = userid;
    [self initSocket];
    self.connectState = disConnectByServer;
    //每次正常连接的时候清零重连时间
    reConnectTime = 0;
}

//断开连接
- (void)disConnect
{
    
    if (webSocket) {
        [webSocket close];
        webSocket = nil;
    }
}

- (void)useDisConnect {
    reConnectTime = 0;
    self.connectState = disConnectByUser;
    if (webSocket) {
        [webSocket close];
    }
    [self destoryHeartBeat];
    [self disConnect];
}

- (void)loginOut{
    reConnectTime = 0;
    self.connectState = disConnectByUser;
    if (webSocket) {
        [webSocket close];
    }
    [self destoryHeartBeat];
    [self disConnect];
    [[LcwlChat shareInstance].chatManager removeAllChatDelegate];
}
//发送消息
- (void)sendMsg:(NSString *)msg
{
    if (webSocket == nil) {
        reConnectTime = 0;
        [self reConnect];
        return;
    }
    if (webSocket.readyState == SR_CONNECTING) {
        return;
    }
#ifdef DEBUG
    if (![msg containsString:@"PING"]) {
        MLog(@"sendMsg....%@", msg)
    }
#endif
    [webSocket send:msg];
}

//重连机制
- (void)reConnect
{
    [self disConnect];
    
    //超过一分钟就不再重连 所以只会重连5次 2^5 = 64
    if (reConnectTime > 64) {
        return;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(reConnectTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        webSocket = nil;
        [self initSocket];
    });
    
    
    //重连时间2的指数级增长
    if (reConnectTime == 0) {
        reConnectTime = 2;
    }else{
        reConnectTime *= 2;
    }
    
}


//pingPong
- (void)ping{
    if (webSocket.readyState == SR_CONNECTING) {
        return;
    }
    [webSocket sendPing:nil];
}




- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{
    NSLog(@"聊天连接成功............");
    [[MXChatMessageHandler sharedInstance]start];
    //连接成功了开始发送心跳
    [self initHeartBeat];
}

//open失败的时候调用
- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
    MLog(@"聊天连接失败............error%@", error);
    if (self.connectState == disConnectByUser) {
        return;
    }
    //失败了就去重连
    [self reConnect];
}

//网络连接中断被调用
- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{
    
    NSLog(@"被关闭连接，code:%ld,reason:%@,wasClean:%d",code,reason,wasClean);
    
    //如果是被用户自己中断的那么直接断开连接，否则开始重连
    if (self.connectState == disConnectByUser) {
        [self disConnect];
    }else{
        [self reConnect];
    }
    //断开连接时销毁心跳
    [self destoryHeartBeat];
    
}

//sendPing的时候，如果网络通的话，则会收到回调，但是必须保证ScoketOpen，否则会crash
- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload
{
    NSLog(@"收到pong回调");
    
}

- (void)receiveMessage:(MessageModel *)model {
    //语音通话消息 只有结束的时候才需要显示
    if (model.subtype == kMXMessageTypeVoiceChat) {
       NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:model.body];
       kMXVoiceChatType chatType = [bodyDic[@"attr3"] integerValue];
       if (chatType == kMXVoiceChatTypeAsk ||
           chatType == kMXVoiceChatTypeAccept ||
           chatType == kMXVoiceChatTypeEnterRoom) {
           return;
       }
       ///格式化提示信息
       [[ChatVoiceManage shareInstance] formatMsg:model];
    }
    MessageModel* tmpMsg = [[LcwlChat shareInstance].chatManager insertMessage:model];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReceiveMessage:)]) {
        if (tmpMsg  != nil) {
            [self.delegate didReceiveMessage:model];
        }
    }
}

#pragma mark - SRWebSocketDelegate
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    [self handleReceiveMessage:message];
}

- (void)handleReceiveMessage:(id)message {
    NSMutableDictionary* dict = [[MXJsonParser jsonToDictionary:message] mutableCopy];
    //如果是客服，将sessionType直接修改为NORMAL
    if([[dict valueForKey:@"sessionType"] isEqualToString:@"SEVICE"]) {
        [dict setValue:KDelivery_state_nomal forKey:@"sessionType"];
    }
    NSString* type = [self.class sessionType:dict];
    if (![StringUtil isEmpty:type]) {
        if ([type isEqualToString:kDelivery_state_acked]) {
            [self handleReceipt:dict];
        }else if([type isEqualToString:KDelivery_state_nomal] ||[type isEqualToString:KDelivery_state_group] ){
            MLog(@"收到消息----%@", message)
            [self handleMessage:dict];
        }else if(([type isEqualToString:kDelivery_state_ss])){
            [self handleSS:dict];
        }else if(([type isEqualToString:kDelivery_state_poppush])){
            [self handlePOPPUSH:dict];
        }else if([type isEqualToString:kDelivery_state_sgroup]){
            [self handleSgroup:dict];
        }else if(([type isEqualToString:kDelivery_state_kickoff])){
           
            @weakify(self)
            dispatch_async(dispatch_get_main_queue(), ^{
                @strongify(self)
                 MLog(@"踢下线了.............");
                //UDetail.user.chatToken = nil;
                [self destoryHeartBeat];
                self.connectState = disConnectByUser;
                [self disConnect];
                
                NSString *msg = nil;
                #ifdef DEBUG
                msg = @"当前账户信息：";
                msg = [msg stringByAppendingString:[NSString stringWithFormat:@"account: %@\n",UDetail.user.account]];
                msg = [msg stringByAppendingString:[NSString stringWithFormat:@"token: %@\n",UDetail.user.token]];
                msg = [msg stringByAppendingString:[NSString stringWithFormat:@"chatToken: %@\n",UDetail.user.chatToken]];
                
                msg = @"socket kickoff:";
                msg = [msg stringByAppendingString:[NSString stringWithFormat:@"%@",[dict description]]];
                #endif
                [MoApp kickOff:msg];
            });
        }
    }
}

-(void)handleReceipt:(NSDictionary* )dict{
    NSString* to = [NSString stringWithFormat:@"%@",dict[@"to"]];
    NSString* msgId = [NSString stringWithFormat:@"%@",dict[@"msgCode"]];
    MessageModel* msg = [[LcwlChat shareInstance].chatManager getMessageByMsgId:msgId chatId:to];
    if (dict[@"msgType"]) {
        msg.subtype = [dict[@"msgType"] integerValue];
    }
    if (msg != nil) {
        [[MXChatMessageHandler sharedInstance]removeMessage:msg];
        if (msg.chatType == eConversationTypeChat) {
            NSInteger status = [dict[@"status"]integerValue];
            if (status == 10) {
                msg.is_acked = 1;
                msg.state = kMXMessageState_Failure;
                FriendModel* friend = [[LcwlChat shareInstance].chatManager loadFriendByChatId:msg.chat_with];
                NSString* tip = @"";
                if (friend == nil) {
                    tip = @"你们还不是你的好友，添加好友后才能聊天\n添加好友";
                }else{
                    tip = [NSString stringWithFormat:@"%@还不是你的好友，添加好友后才能聊天\n添加好友",friend.name];
                }
                [ChatSendHelper sendChat:tip from:msg.chat_with messageType:kMxmessageTypeCustom type:eConversationTypeChat];
            } else if (status == 11) {
                msg.is_acked = 1;
                msg.state = kMXMessageState_Failure;
                
                NSString* tip = @"消息已发出，但被对方拒绝了。";
                [ChatSendHelper sendChat:tip from:msg.chat_with messageType:kMxmessageTypeCustom type:eConversationTypeChat];
            } else{
                msg.is_acked = 1;
                msg.state = kMXMessageStateSuccess;
            }
        }else{
            msg.is_acked = 1;
            msg.state = kMXMessageStateSuccess;
        }
     
        [[LcwlChat shareInstance].chatManager updateMsgAck:msg];
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(didReceiveReceipt:)]) {
                [self.delegate didReceiveReceipt:msg];
            }
        }
    }
}

-(void)handleMessage:(NSDictionary* )dict{
    MessageModel *model= [self dictionatyToModel:dict];
    if (model == nil) {
        return;
    }
    if (model.chatType == eConversationTypeChat) {
        FriendModel* friend = [[FriendModel alloc]init];
        friend.avatar = model.avatar;
        friend.name = model.name;
        friend.userid = model.chat_with;
        FriendModel* tmp = [[LcwlChat shareInstance].chatManager loadFriendByChatId:friend.userid];
        //如果是pc端同步过来的自己发的消息(fromID = 自己ID)  不需要插入本地好友
        if (!model.msg_direction) {
            if (!tmp) {
                friend.followState = 3;
                [[LcwlChat shareInstance].chatManager insertFriends:@[friend]];
            } else {
                tmp.name = friend.name;
                tmp.avatar = friend.avatar;
                [[LcwlChat shareInstance].chatManager insertFriends:@[tmp]];
            }
        }
    }else if(model.chatType == eConversationTypeGroupChat){
        NSDictionary* groupDict = dict[@"group"];
        if (groupDict) {
            NSString* groupID = groupDict[@"groupId"];
            ChatGroupModel* tmpGroup = [[LcwlChat shareInstance].chatManager loadGroupByChatId:groupID];
            if (!tmpGroup) {
                ChatGroupModel* model = [[ChatGroupModel alloc]init];
                model.roleType = 1;
                model.groupId = groupID;
                model.groupName = groupDict[@"groupName"];
                model.groupAvatar = groupDict[@"groupAvatar"];
                [[LcwlChat shareInstance].chatManager insertGroup:model];
            }
        }
        FriendModel* member = [[LcwlChat shareInstance].chatManager selectGroupMember:model.chat_with memberId:model.fromID];
        //如果是pc端同步过来的自己发的消息(fromID = 自己ID)  不需要插入本地好友
        if (!model.msg_direction) {
            if (!member) {
                member = [[FriendModel alloc] init];
                member.userid = model.fromID;
                member.groupid = model.chat_with;
                member.avatar = model.avatar;
                member.name = model.name;
                [[LcwlChat shareInstance].chatManager insertGroupMember:member];
            } else {
                if ((![member.avatar isEqualToString:model.avatar]) || (![model.name isEqualToString:member.name])) {
                    member.name = model.name;
                    member.avatar = model.avatar;
                    [[LcwlChat shareInstance].chatManager insertGroupMember:member];
                }
            }
        }
    }
    //语音通话消息 只有结束的时候才需要显示
    if (model.subtype == kMXMessageTypeVoiceChat) {
        NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:model.body];
        kMXVoiceChatType chatType = [bodyDic[@"attr3"] integerValue];
        if (chatType == kMXVoiceChatTypeAccept ||
            chatType == kMXVoiceChatTypeAsk ||
            chatType == kMXVoiceChatTypeEnterRoom) {
            [[ChatVoiceManage shareInstance] didReceiveMessage:model];
            if (self.delegate && [self.delegate respondsToSelector:@selector(sendReceipt:)]) {
                [self.delegate sendReceipt:model];
            }
            return;
        }
        ///格式化提示信息
        [[ChatVoiceManage shareInstance] formatMsg:model];
    }
    MessageModel* tmpMsg = [[LcwlChat shareInstance].chatManager insertMessage:model];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReceiveMessage:)]) {
        if (tmpMsg  != nil) {
            [self.delegate didReceiveMessage:model];
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(sendReceipt:)]) {
            [self.delegate sendReceipt:model];
        }
    }
}

+(NSString* )sessionType:(NSDictionary* )dict{
    return dict[@"sessionType"];
}

-(void)handleSS:(NSDictionary* )dict{
    
    MessageModel* message = [self.class modelWithSSMessage:dict];
    
    //好友
    if (message.subtype == SSTypeFollowed) {
        FriendModel* friend = [[FriendModel alloc]init];
        NSDictionary* bodyDict = dict[@"body"];
        friend.userid = [NSString stringWithFormat:@"%@",bodyDict[@"attr1"]];
        friend.name = [NSString stringWithFormat:@"%@",bodyDict[@"attr2"]];
        friend.avatar = [NSString stringWithFormat:@"%@",bodyDict[@"attr3"]];
        friend.followState = 3;
        [[LcwlChat shareInstance].chatManager insertFriends:@[friend]];
        [self sendSSReceipt:dict];
    }else if(message.subtype == SSTypeFollow){
        FriendModel* friend = [[FriendModel alloc]init];
        NSDictionary* bodyDict = dict[@"body"];
        friend.userid = [NSString stringWithFormat:@"%@",bodyDict[@"attr1"]];
        friend.name = [NSString stringWithFormat:@"%@",bodyDict[@"attr2"]];
        friend.avatar = [NSString stringWithFormat:@"%@",bodyDict[@"attr3"]];
        friend.followState = 1;
        FriendModel* model = [TalkManager newFriend];
        FriendModel* tmp = [[LcwlChat shareInstance].chatManager loadFriendByChatId:model.userid];
        //好友信息以后由增量更新来完成
        if (tmp == nil) {
            [[LcwlChat shareInstance].chatManager insertFriends:@[model]];
        }
        [[LcwlChat shareInstance].chatManager insertMessage:message];
        [[LcwlChat shareInstance].chatManager insertFriends:@[friend]];
        if (self.delegate) {
            NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithCapacity:0];
            NSInteger nums = [[LcwlChat shareInstance].chatManager getAllUnReadNums];
            [userInfo setValue:[NSNumber numberWithInteger:nums] forKey:@"nums"];
            [userInfo setValue:[NSNumber numberWithInteger:3] forKey:@"tabBarIndex"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"redBadgeCountChanged" object:nil userInfo:userInfo];
            if ([self.delegate respondsToSelector:@selector(didReceiveSS:)]) {
                [self.delegate didReceiveSS:message];
            }
            [self sendSSReceipt:dict];
            
        }
    }/*else if(message.subtype == SSTypeHdtz){
        message.chat_with = hdtz;
        message.chatType = eConversationTypeHDTZChat;
        [[LcwlChat shareInstance].chatManager insertMessage:message];
        if ([self.delegate respondsToSelector:@selector(didReceiveSS:)]) {
            [self.delegate didReceiveSS:message];
        }
        [self sendSSReceipt:dict];
    }*/
    
}

-(void)handlePOPPUSH:(NSDictionary* )dict {
    MessageModel* message = [self.class modelWithSSMessage:dict];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReceiveSystemMsg:)]) {
         if (message != nil) {
             [self.delegate didReceiveSystemMsg:message];
         }
     }
    [self sendSSReceipt:dict];
}

-(void)handleSgroup:(NSDictionary* )dict{
    MessageModel* message = [self.class modelWithSgroupMessage:dict];
    if (message == nil) {
        return;
    }
    NSDictionary* groupDict = dict[@"group"];
   if(groupDict){
       ChatGroupModel* group = [ChatGroupModel chatDictionaryToModel:groupDict];
       if (message.subtype == 6) {
           group.roleType = 0;
           ChatGroupModel* localGroup = [[LcwlChat shareInstance].chatManager loadGroupByChatId:group.groupId];
           if (!localGroup) {
               [[LcwlChat shareInstance].chatManager insertGroup:group];
           }else{
               [[LcwlChat shareInstance].chatManager insertGroup:group];
           }
       }else if(message.subtype == 7){
           group.roleType = 0;
           ChatGroupModel* localGroup = [[LcwlChat shareInstance].chatManager loadGroupByChatId:group.groupId];
           if (!localGroup) {
               [[LcwlChat shareInstance].chatManager insertGroup:group];
           }else{
               [[LcwlChat shareInstance].chatManager insertGroup:group];
           }
       }
       else if(message.subtype == 2 || message.subtype == 10){
           ChatGroupModel* localGroup = [[LcwlChat shareInstance].chatManager loadGroupByChatId:group.groupId];
           if (!localGroup) {
               [[LcwlChat shareInstance].chatManager insertGroup:group];
           }else{
               if (group.roleType == 0) {
                   group.roleType = 1;
               }
               [[LcwlChat shareInstance].chatManager insertGroup:group];
           }
       }else{
           ChatGroupModel* localGroup = [[LcwlChat shareInstance].chatManager loadGroupByChatId:group.groupId];
           if (!localGroup) {
               [[LcwlChat shareInstance].chatManager insertGroup:group];
           }else{
               [[LcwlChat shareInstance].chatManager insertGroup:group];
               
           }
       }
     
   }
   
    FriendModel* member = [[LcwlChat shareInstance].chatManager selectGroupMember:message.chat_with memberId:message.fromID];
    if (member == nil) {
        member = [[FriendModel alloc] init];
        member.userid = message.fromID;
        member.groupid = message.chat_with;
        member.avatar = message.avatar;
        member.name = message.name;
        [[LcwlChat shareInstance].chatManager insertGroupMember:member];
    }else{
        if ((![member.avatar isEqualToString:message.avatar]) || (![message.name isEqualToString:member.name])) {
            member.name = message.name;
            member.avatar = message.avatar;
            [[LcwlChat shareInstance].chatManager insertGroupMember:member];
        }
    }
    
   MessageModel* tmpMsg = [[LcwlChat shareInstance].chatManager insertMessage:message];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReceiveMessage:)]) {
        if (tmpMsg  != nil) {
            [self.delegate didReceiveMessage:message];
        }
         [self sendSSReceipt:dict];
    }
}




-(void)sendSSReceipt:(NSDictionary* )dict{
    NSMutableDictionary* muDict = [NSMutableDictionary dictionaryWithDictionary:dict];
    [muDict setObject:@"ACK" forKey:@"sessionType"];
    NSString* sessionId = [NSString stringWithFormat:@"%@@%@",dict[@"to"],dict[@"sessionType"]];
    [muDict setObject:sessionId forKey:@"sessionId"];
    [[SRWebSocketManager sharedInstance]sendMsg:[MXJsonParser dictionaryToJsonString:muDict]];
}

+(MessageModel*)modelWithSgroupMessage:(NSDictionary*)dict{
    MessageModel* message = [[MessageModel alloc]init];
    message.subtype = [dict[@"msgType"] intValue];
    message.type = [MessageModel stringToSessionType:dict[@"sessionType"]];
    message.sendTime = [NSString stringWithFormat:@"%@",dict[@"msgTime"]];
    message.state = kMXMessageStateSuccess;
    NSDictionary* userDict =  dict[@"user"];
    if (userDict) {
        message.name = [NSString stringWithFormat:@"%@",userDict[@"name"]];
        message.avatar = [NSString stringWithFormat:@"%@",userDict[@"avatar"]];
    }
    NSString* from = [NSString stringWithFormat:@"%@",dict[@"from"]];
    NSString* to = [NSString stringWithFormat:@"%@",dict[@"to"]];
    message.chat_with = from;
    message.chatType = [MessageModel getTypeByChatType:message.type];
    message.fromID = [NSString stringWithFormat:@"%@",dict[@"from"]];
    message.toID = to;
    message.body = [MXJsonParser dictionaryToJsonString:dict[@"body"]];
    if (message.body) {
        NSDictionary* bodyDict =  dict[@"body"];
        message.content = [NSString stringWithFormat:@"%@",bodyDict[@"attr1"]];
    }
    if ([[LcwlChat shareInstance].chatManager isBelongExistConversation:message.chat_with]) {
        message.is_new = 1;
    }else{
        message.is_new = 0;
    }
    return message;
}

+(MessageModel*)modelWithSSMessage:(NSDictionary*)dict{
    MessageModel* model = [[MessageModel alloc]init];
    model.type = MessageTypeSs;
    model.subtype = [dict[@"msgType"]integerValue];
    model.msg_direction = 0;
    NSDictionary* body = dict[@"body"];
    model.content =  [NSString stringWithFormat:@"%@",body[@"attr4"]];
    model.body = [MXJsonParser dictionaryToJsonString:dict[@"body"]];
    model.messageId = [NSString stringWithFormat:@"%@",dict[@"msgCode"]];
    model.chatType = eConversationTypeXDPYChat;
    model.fromID = [NSString stringWithFormat:@"%@",dict[@"from"]];
    model.toID = [NSString stringWithFormat:@"%@",dict[@"to"]];
    model.chat_with = nf;
    model.is_new = 0;
    model.sendTime = [NSString stringWithFormat:@"%@",dict[@"msgTime"]];
    return model;
}


-(MessageModel* )dictionatyToModel:(NSDictionary* )dict{
    MessageModel* message = [[MessageModel alloc]init];
    NSString* from = [NSString stringWithFormat:@"%@",dict[@"from"]];
    NSString* to = [NSString stringWithFormat:@"%@",dict[@"to"]];
    message.msg_direction = 0;
    message.messageId = [NSString stringWithFormat:@"%@",dict[@"msgCode"]];
    int messageType =  [dict[@"msgType"] intValue];
    message.subtype = messageType;
    message.type = [MessageModel stringToSessionType:dict[@"sessionType"]];
    message.creatorRole = [dict[@"creatorRole"] intValue];;
    
    message.sendTime = [NSString stringWithFormat:@"%@",dict[@"msgTime"]];
    message.state = kMXMessageStateSuccess;
    NSDictionary* userDict = dict[@"user"];
    if (userDict) {
        message.name = [NSString stringWithFormat:@"%@",userDict[@"name"]];
        message.avatar = [NSString stringWithFormat:@"%@",userDict[@"avatar"]];
        NSString *fromID = [NSString stringWithFormat:@"%@",userDict[@"userid"]];
        if ([fromID isEqualToString:[LcwlChat shareInstance].user.user_id]) {
            message.msg_direction = 1;
        }
        //不管是否是好友，每次都更新昵称，这样只要对方设置新的备注的时候整个列表都可以立马刷新（没有设置备注的前提下）
        if (!message.msg_direction) {
            [[LcwlChat shareInstance].chatManager updateRemarkMap:userDict[@"userid"] remark:message.name needCheckExist:YES];
        }
    }
    
    message.chatType = [MessageModel getTypeByChatType:message.type];
    message.chat_with = from;
    if (message.msg_direction) {
        if (message.chatType == eConversationTypeGroupChat) {
            message.chat_with = from;
        } else {
            message.chat_with = to;
        }
    }
    if (message.chatType == eConversationTypeGroupChat) {
        message.fromID = [NSString stringWithFormat:@"%@",userDict[@"userid"]];;
    }else{
        message.fromID = from;
    }
    message.toID = to;
  
    message.body = [MXJsonParser dictionaryToJsonString:dict[@"body"]];
    if (message.subtype == kMXMessageTypeText || message.subtype == kMXMessageTypeGif) {
        if (message.body) {
            NSDictionary* bodyDict = dict[@"body"];
            if([bodyDict isKindOfClass:[NSDictionary class]]) {
                message.content = [NSString stringWithFormat:@"%@",bodyDict[@"attr1"]];
            }
        }
    }else if(message.subtype == kMXMessageTypeImage){
        if (message.body) {
            NSDictionary* bodyDict = dict[@"body"];
            message.imageRemoteUrl = bodyDict[@"attr1"];
            int width = [bodyDict[@"attr2"] intValue];
            int height = [bodyDict[@"attr3"] intValue];
            message.size = CGSizeMake(width, height);
        }
    }else if(message.subtype == kMXMessageTypeLocation){
        if (message.body) {
            NSDictionary* bodyDict =  dict[@"body"];
            message.addr = bodyDict[@"attr1"];
            message.lat = bodyDict[@"attr2"];
            message.lng = bodyDict[@"attr3"];
            message.detailAddr = bodyDict[@"attr4"];
            message.fileUrl = bodyDict[@"attr5"];
        }
    }else if(message.subtype == kMxmessageTypeVideo){
        if (message.body) {
            NSDictionary* bodyDict = dict[@"body"];
            message.fileUrl = bodyDict[@"attr1"];
        }
    }else if(message.subtype == kMXMessageTypeVoice){
        if (message.body) {
            NSDictionary* bodyDict = dict[@"body"];
            message.imageRemoteUrl = bodyDict[@"attr1"];
            message.voiceLen = bodyDict[@"attr2"];
        }
    }else if(message.subtype == kMXMessageTypeCard){
        
    }else if(message.subtype == kMxmessageTypeRedPack){
        
    }else if(message.subtype == kMXMessageTypeFile){
        if (message.body) {
            NSDictionary* bodyDict = dict[@"body"];
            message.fileUrl = bodyDict[@"attr1"];            
        }
    } else if (message.subtype == kMXMessageTypeProductLink) {
        
    }
  
    if ([[LcwlChat shareInstance].chatManager isBelongExistConversation:message.chat_with]) {
        message.is_new = 1;
    }else{
        message.is_new = 0;
    }
    if (message.msg_direction) {
        message.is_new = 1;
    }
    NSString *device_type = dict[@"device_type"];
    if (![StringUtil isEmpty:device_type]) {
        message.isPCMsg = YES;
    } else {
        message.isPCMsg = NO;
    }
    return message;
}


@end
