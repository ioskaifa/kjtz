//
//  MXDeviceManager.m
//  MoPal_Developer
//
//  Created by aken on 15/3/24.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "MXDeviceManager.h"
#import <AVFoundation/AVFoundation.h>
#import "IMXDeviceManagerDelegate.h"
#import "ChatCacheFileUtil.h"
#import "MXChatVoice.h"
#import "VoiceConverter.h"

#import "UserModel.h"
#import "FriendModel.h"
#import "LcwlChat.h"

typedef void(^RecordingAudioCallBack)(MXChatVoice* voice,NSError *error);

typedef void(^PlayAudioCallBack)(BOOL success);

#define WAVE_UPDATE_FREQUENCY   0.05

@interface MXDeviceManager()<AVAudioRecorderDelegate,AVAudioPlayerDelegate>
{
    
    id<IMXDeviceManagerDelegate> deviceDelegate;
    BOOL isPlaying;
    BOOL isRecording;
    NSTimer * timer_;
    
    AVAudioPlayer *audioPlayer;
    AVAudioRecorder *audioRecorder;
    
    NSURL *pathURL;
}

@property (nonatomic , strong) RecordingAudioCallBack recordingAudioCallBack;
@property (nonatomic , strong) PlayAudioCallBack playAudioCallBack;

@end

@implementation MXDeviceManager

+ (MXDeviceManager*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

-(id)init{
    if (self = [super init]) {
//        UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
//        AudioSessionSetProperty(kAudioSessionProperty_AudioCategory,
//                                sizeof(sessionCategory),
//                                &sessionCategory);
//        
//        UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
//        AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,
//                                 sizeof (audioRouteOverride),
//                                 &audioRouteOverride);
//        
//        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
//        //默认情况下扬声器播放
//        [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
//        // 一进来不能设置为YES,如果为YES,则音乐app在播着音乐时，进入魔线app，就会被stop掉,需要播时打开，播放完后要关掉
////        [audioSession setActive:YES error:nil];
//        audioSession = nil;
        
        
        AVAudioSession *session =   [AVAudioSession sharedInstance];
        NSError *error;
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
        [session setMode:AVAudioSessionModeVoiceChat error:&error];
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        
        return self;
    }
    return nil;
}

- (void)addDelegate:(id<IMXDeviceManagerDelegate>)delegate onQueue:(dispatch_queue_t)aQueue{
    deviceDelegate = delegate;
}

- (void)removeDelegate:(id<IMXDeviceManagerDelegate>)delegate{
    
}
#pragma mark - 语音播放
- (void)didPlayAudio:(NSString *)aFilePath error:(NSError *)error{
   
    [self didPlayAudio:aFilePath turnOnSpeaker:YES error:error completion:nil];
}

- (void)didPlayAudio:(NSString *)aFilePath turnOnSpeaker:(BOOL)enbled error:(NSError *)error completion:(void (^)(BOOL))completion {
    
    
    if (isPlaying) {
        [audioPlayer stop];
    }
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    // update by aken 2015-09-14
    NSString *wavPath =nil;
    NSString *fileName=[[aFilePath lastPathComponent] pathExtension];
    if ([fileName isEqualToString:@"wav"]) {
        wavPath=aFilePath;
    }else{
        wavPath=[VoiceConverter amrToWav:aFilePath];
    }
    
    if (wavPath==nil && completion) {
        completion(NO);
        return;
    }
    
    [[UIDevice currentDevice] setProximityMonitoringEnabled:YES]; //建议在播放之前设置yes，播放结束设置NO，这个功能是开启红外感应
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    // 默认开启扬声器
    if (enbled) {
        [self enableSpeakerPhone];
    }else{
        FriendModel* model = [LcwlChat shareInstance].user;
        //wg 0224
        NSString* boolFlag = [MXCache valueForKey:[NSString stringWithFormat:@"ControlSpeak_%@",[model valueForKey:@"userid"]]];
        if ([StringUtil isEmpty:boolFlag]) {
            [self enableSpeakerPhone];
        }else if ([boolFlag boolValue]){
            [self enableSpeakerPhone];
        }else{
            [self disableSpeakerPhone];
        }
        
        
    }
    
    audioPlayer = [[AVAudioPlayer alloc] initWithData:[NSData dataWithContentsOfFile:wavPath] error:&error];
    [audioPlayer setVolume:1];
    [audioPlayer prepareToPlay];
    [audioPlayer setDelegate:self];
    
    isPlaying = YES;
    [audioPlayer play];
    
    self.playAudioCallBack=completion;

}

- (void)didPlayAudio:(NSString *)aFilePath error:(NSError *)error completion:(void (^)(BOOL))completion {
    
    [self didPlayAudio:aFilePath turnOnSpeaker:NO error:error completion:completion];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [[UIDevice currentDevice] setProximityMonitoringEnabled:NO]; //建议在播放之前设置yes，播放结束设置NO，这个功能是开启红外感应

    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    
    [self audioSessionAmbient];
    if (!isPlaying ) {
        return;
    }
    isPlaying = NO;
    if ([deviceDelegate respondsToSelector:@selector(audioPlayerDidFinishPlaying:successfully:)]) {
        [deviceDelegate audioPlayerDidFinishPlaying:player successfully:flag];
    }
    if (self.playAudioCallBack) {
        self.playAudioCallBack(flag);
    }
}
#pragma mark - 语音录制

- (void)startRecordAudio:(NSError *)error {
    
    
    if (isRecording) {
        return;
    }
    [audioPlayer pause];
    

    if (audioRecorder==nil) {
        
        NSDictionary *settings=[NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithFloat:8000],AVSampleRateKey,
                                [NSNumber numberWithInt:kAudioFormatLinearPCM],AVFormatIDKey,
                                [NSNumber numberWithInt:1],AVNumberOfChannelsKey,
                                [NSNumber numberWithInt:8],AVLinearPCMBitDepthKey,
                                [NSNumber numberWithBool:NO],AVLinearPCMIsBigEndianKey,
                                [NSNumber numberWithBool:NO],AVLinearPCMIsFloatKey,
                                nil];
        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayAndRecord error: nil];
        NSURL *url = [NSURL fileURLWithPath:[self audioFilePath]];
        pathURL=url;
        audioRecorder = [[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error];
        audioRecorder.delegate = self;
        [audioRecorder prepareToRecord];
        [audioRecorder setMeteringEnabled:YES];
        [audioRecorder peakPowerForChannel:0];
    
        isRecording = YES;
        [audioRecorder record];
        MLog(@"audioRecorder==nil");
        
    }else{
         isRecording = YES;
        [audioRecorder record];
        MLog(@"audioRecorder!=nil");
    }
        MLog(@"audioRecorder.currentTime:%f",audioRecorder.currentTime);
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
}

- (void)didStopRecordAudio:(RecordingAudioCallBack)completion {
    
   
    if (!isRecording) {
        return;
    }
   
    NSError *error=nil;
    
    isRecording = NO;
    if (completion) {
         MLog(@"audioRecorder.currentTime:%f completion",audioRecorder.currentTime);
        float timeLen=audioRecorder.currentTime;
        [audioRecorder stop];
        [[AVAudioSession sharedInstance] setActive:NO error:nil];
        
        if (timeLen<1) {
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"语音录制过短" forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:@"" code:1000 userInfo:userInfo];
            
            completion(nil,error);
        }else{
            
            MXChatVoice* voice = [[MXChatVoice alloc] init];
            if (timeLen >=60) {
                voice.duration = 60;//audioRecorder.currentTime;
            }else{
                voice.duration = timeLen;//audioRecorder.currentTime;
            }
            
            //
            NSString* amrPath = [VoiceConverter wavToAmr:pathURL.path];
            NSData *recordData = [NSData dataWithContentsOfFile:amrPath];
            NSInteger size = recordData.length;
            
            voice.fileLength = size;
            voice.localPath = amrPath;
            voice.displayName = [amrPath lastPathComponent];
            
            // 因为录音会存两份，一份为wav，一份为amr
            [[ChatCacheFileUtil sharedInstance] deleteWithContentPath:pathURL.path];
            audioRecorder=nil;
            completion(voice,error);
        }
        
    }
}

- (void)pauseRecordAudio {
    
    [audioRecorder pause];
}


-(NSString*)audioFilePath{
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyyMMddHHmmss"];
    UserModel *userModel=UDetail.user;
    NSString *fullPath = [[[ChatCacheFileUtil sharedInstance] userDocPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"rec_%@_%@.wav",userModel.chatUser_id,[dateFormater stringFromDate:now]]];
    return fullPath;
}

- (float)peekRecorderVoiceMeter{
    if (audioRecorder) {
        [audioRecorder updateMeters];
    }
    float peakPower = [audioRecorder averagePowerForChannel:0];
    
    double alpha = 0.05;
    return  pow(10, (alpha * peakPower));
}

-(void)endPlayAudio{
    if (isPlaying) {
        [audioPlayer stop];
        audioPlayer = nil;
         [[UIDevice currentDevice] setProximityMonitoringEnabled:NO]; 
    }
}

- (void)audioSessionAmbient {
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
}

- (void)enableSpeakerPhone {
    EnableSpeakerPhone();
}

- (void)disableSpeakerPhone {
    DisableSpeakerPhone();
}

void EnableSpeakerPhone ()
{
    UInt32 dataSize = sizeof(CFStringRef);
    CFStringRef currentRoute = NULL;
    OSStatus result = noErr;
    
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &dataSize, &currentRoute);
    
    // Set the category to use the speakers and microphone.
    UInt32 sessionCategory = kAudioSessionCategory_PlayAndRecord;
    result = AudioSessionSetProperty (
                                      kAudioSessionProperty_AudioCategory,
                                      sizeof (sessionCategory),
                                      &sessionCategory
                                      );
    assert(result == kAudioSessionNoError);
    
    Float64 sampleRate = 44100.0;
    dataSize = sizeof(sampleRate);
    result = AudioSessionSetProperty (
                                      kAudioSessionProperty_PreferredHardwareSampleRate,
                                      dataSize,
                                      &sampleRate
                                      );
    assert(result == kAudioSessionNoError);
    
    // Default to speakerphone if a headset isn't plugged in.
    UInt32 route = kAudioSessionOverrideAudioRoute_Speaker;
    dataSize = sizeof(route);
    result = AudioSessionSetProperty (
                                      // This requires iPhone OS 3.1
                                      kAudioSessionProperty_OverrideCategoryDefaultToSpeaker,
                                      dataSize,
                                      &route
                                      );
    assert(result == kAudioSessionNoError);
    
    AudioSessionSetActive(YES);
}

void DisableSpeakerPhone ()
{
    UInt32 dataSize = sizeof(CFStringRef);
    CFStringRef currentRoute = NULL;
    OSStatus result = noErr;
    
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &dataSize, &currentRoute);
    
    // Set the category to use the speakers and microphone.
    UInt32 sessionCategory = kAudioSessionCategory_PlayAndRecord;
    result = AudioSessionSetProperty (
                                      kAudioSessionProperty_AudioCategory,
                                      sizeof (sessionCategory),
                                      &sessionCategory
                                      );
    assert(result == kAudioSessionNoError);
    
    Float64 sampleRate = 44100.0;
    dataSize = sizeof(sampleRate);
    result = AudioSessionSetProperty (
                                      kAudioSessionProperty_PreferredHardwareSampleRate,
                                      dataSize,
                                      &sampleRate
                                      );
    assert(result == kAudioSessionNoError);
    
    // Default to speakerphone if a headset isn't plugged in.
    // Overriding the output audio route
    // The Trick is here
    
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_None;
    dataSize = sizeof(audioRouteOverride);
    AudioSessionSetProperty(
                            kAudioSessionProperty_OverrideAudioRoute,
                            dataSize,
                            &audioRouteOverride);
    
    assert(result == kAudioSessionNoError);
    
    AudioSessionSetActive(YES);
}

#pragma mark - 获取设备权限
+ (void)requestDeviceAuthod:(MXDeviceAuthorization)authType complete:(void(^)(BOOL))complete {
    switch (authType) {
        case MXDeviceAuthorizationMicrophone: {
            [self requestDeviceAuthodMicrophone:complete];
        }
            break;
            
        default:
            break;
    }    
}

+ (void)requestDeviceAuthodMicrophone:(void(^)(BOOL))complete {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if (status == AVAuthorizationStatusNotDetermined) {
        //未请求过该权限
        [self requestAuthodMicrophone:complete];
    } else if (status == AVAuthorizationStatusRestricted ||
               status == AVAuthorizationStatusDenied) {
        //请求过该权限 被用户拒绝 - 弹窗提示用户去开启
        [self alterDeviceAuthod:MXDeviceAuthorizationMicrophone complete:complete];
    } else {
        //成功授权
        if (complete) {
            complete(YES);
        }
    }
}

///获取麦克风权限  -- 只有在状态为 AVAuthorizationStatusNotDetermined 的时候去获取，其它拒绝状态则弹出提示用户去设置
+ (void)requestAuthodMicrophone:(void(^)(BOOL))complete {
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
        if (complete) {
            complete(granted);
        }
    }];
}

+ (void)alterDeviceAuthod:(MXDeviceAuthorization)authType complete:(void(^)(BOOL))complete{
    NSString *trips = [self authodTrips:authType];
    [MXAlertViewHelper showAlertViewWithMessage:trips title:@"提示"
                                        okTitle:@"去设置"
                                    cancelTitle:@"取消"
                                     completion:^(BOOL cancelled, NSInteger buttonIndex) {
        if (cancelled) {
            if (complete) {
                complete(NO);
            }
            return;
        }
        if (buttonIndex == 1) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            UIApplication *application = [UIApplication sharedApplication];
            if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
                [application openURL:url options:@{} completionHandler:nil];
            } else {
                [application openURL:url];
            }
        }
    }];
}

+ (NSString *)authodTrips:(MXDeviceAuthorization)authType {
    NSString *trip;
    NSDictionary *infoDic = [NSBundle mainBundle].infoDictionary;
    trip = infoDic[[self authodInfoKey:authType]];
    return trip;
}

+ (NSString *)authodInfoKey:(MXDeviceAuthorization)authType {
    NSString *key = @"";
    switch (authType) {
        case MXDeviceAuthorizationMicrophone:
            key = @"NSMicrophoneUsageDescription";
            break;
            
        default:
            break;
    }
    return key;
}

@end
