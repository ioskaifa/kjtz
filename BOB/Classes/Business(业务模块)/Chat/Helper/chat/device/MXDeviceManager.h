//
//  MXDeviceManager.h
//  MoPal_Developer
//
//  本类主要处理：录音、播放录音、停止录音、震动等操作
//  Created by aken on 15/3/24.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMXDeviceManager.h"

typedef NS_ENUM(NSInteger, MXDeviceAuthorization) {
    ///麦克风权限
    MXDeviceAuthorizationMicrophone,
};

/*!
 @class
 @brief 本类主要处理：录音、播放录音、停止录音、震动等操作
 @discussion 最先使用的类, 所有的类对象, 均是通过这个单实例来获取, 示例代码如下:
 [MXChatManager sharedInstance]
 */

@interface MXDeviceManager : NSObject<IMXDeviceManager>

/*!
 @method
 @brief 获取单实例
 @discussion
 @result MXDeviceManager实例对象
 */
+ (MXDeviceManager *)sharedInstance;

- (void)enableSpeakerPhone;

- (void)disableSpeakerPhone;

///获取设备权限
+ (void)requestDeviceAuthod:(MXDeviceAuthorization)authType complete:(void(^)(BOOL))complete;
///未开启权限时的提示弹窗
+ (void)alterDeviceAuthod:(MXDeviceAuthorization)authType;


@end
