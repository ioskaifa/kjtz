//
//  SRWebSocketManager.h
//  Lcwl
//
//  Created by mac on 2018/12/10.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol LcwlSRWebSocketDelegate <NSObject>
@optional

- (void)didReceiveMessage:(MessageModel *)message;

///接受系统消息
- (void)didReceiveSystemMsg:(MessageModel *)message;

//- (void)sendMsg:(MessageModel *)message;
//
- (void)didReceiveReceipt:(MessageModel *)message;
//
- (void)sendReceipt:(MessageModel *)messageId;

- (void)didReceiveSS:(MessageModel *)message;

@end

@interface SRWebSocketManager : NSObject

@property (nonatomic, weak) id<LcwlSRWebSocketDelegate>delegate;
///是否可以连接  no 处理离线消息中 不刷新UI 不连socket
@property (nonatomic, assign) BOOL isConnect;

+ (SRWebSocketManager*)sharedInstance ;

- (void)connect:(NSString *)userid;

- (void)disConnect;
///用户主动断开
- (void)useDisConnect;

- (void)sendMsg:(NSString *)msg;
///有些消息不是在ChatViewVC发送的，需要生成聊天记录
- (void)receiveMessage:(MessageModel *)message;

- (void)ping;

- (void)loginOut;
///重连
- (void)reConnect;
///收到消息处理
- (void)handleReceiveMessage:(id)message;

@end

NS_ASSUME_NONNULL_END
