//
//  MXChatDefines.h
//  MoPal_Developer
//
//  Created by aken on 15/4/13.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#ifndef MoPal_Developer_MXChatDefines_h
#define MoPal_Developer_MXChatDefines_h


typedef NS_ENUM(NSInteger, EMConversationType){
    ///单聊
    eConversationTypeChat,
    ///群聊
    eConversationTypeGroupChat,
    eConversationTypeXDPYChat,
    eConversationTypeHDTZChat,
    ///电脑端操作相关消息
    eConversationTypeClientChat,
};

//总的消息种类
typedef NS_ENUM(NSInteger, MessageType){
    ///单聊
    MessageTypeNormal = 1,
    ///群聊
    MessageTypeGroupchat = 2,
    MessageTypeRich = 3,
    MessageTypeDestroy = 4,
    ///群系统消息
    MessageTypeSgroup = 5,
    /// 推送系统消息
    MessageTypeSs = 6,
    ///个人系统消息
    MessageTypeS = 7,
    ///电脑端操作相关消息
    MessageTypeClient = 8,
};

// 基本消息的类型
typedef NS_ENUM(NSInteger, kMXMessageType){
    kMXMessageTypeText = 1,
    kMXMessageTypeImage =2,
    ///语音
    kMXMessageTypeVoice =3,
    ///位置
    kMXMessageTypeLocation =4,
    ///gif
    kMXMessageTypeGif,
    kMXMessageTypeRemind,
    ///个人名片
    kMXMessageTypeCard = 7,
    kMxmessageTypeRedPack,
    ///文件
    kMXMessageTypeFile = 9,
    ///视频
    kMxmessageTypeVideo = 10,
    kMxmessageTypeReceiveRedpacket = 11,
    ///语音通话
    kMXMessageTypeVoiceChat = 13,
    ///消息撤回 attr1 存messageID
    kMXMessageTypeWithdraw = 15,
    ///电脑端操作相关
    kMXMessageTypeClient = 16,
    kMXMessageTypeProductLink = 17,
    kMxmessageTypeCustom = 99,
    kMXMessageTypeNone=-1,
};

///语音通话类型
typedef NS_ENUM(NSInteger, kMXVoiceChatType) {
    ///询问是否准备好语音通话
    kMXVoiceChatTypeAsk = 20,
    ///对方忙
    kMXVoiceChatTypeBusy = 21,
    ///已接受语音通话
    kMXVoiceChatTypeAccept = 22,
    ///拒绝语音通话
    kMXVoiceChatTypeCancel = 23,
    ///结束语音通话
    kMXVoiceChatTypeEnd = 24,
    ///语音通话超时
    kMXVoiceChatTypeOverTime = 25,
    ///进入聊天室
    kMXVoiceChatTypeEnterRoom = 26,
};

///文件类型
typedef NS_ENUM(NSInteger, kMXClientType) {
    ///识别登录二维码
    kMXClientTypeIdentify = 0,
    ///确认登录
    kMXClientTypeConfirmLogin = 1,
    ///取消登录
    kMXClientTypeCancelLogin = 2,
    ///退出登录
    kMXClientTypeLogOut = 3,
};

///文件类型
typedef NS_ENUM(NSInteger, kMXFileType) {
    ///未知文件类型
    kMXFileTypeUnknow = 0,
    ///PDF文件类型
    kMXFileTypePDF = 1,
    ///Word文件类型
    kMXFileTypeWord = 2,
    ///Excel文件类型
    kMXFileTypeExcel = 3,
};

//MessageTypeSgroup 系统消息的类型
typedef NS_ENUM(NSInteger, SGROUPType){
    SGROUPTypeCreate = 1,
    SGROUPTypeAdd_Member,
    SGROUPTypeUpdate_Base,
    SGROUPTypeRemove_Member = 6,
    SGROUPTypeDissolve,
    SGROUPTypeQuit_From_Group,
    SGROUPTypeInvite,
};
//MessageTypeSs 邀请消息类型
typedef NS_ENUM(NSInteger, SSType){
    SSTypeFollow = 1,
    SSTypeSYSTEM_STEAL = 2,
    SSTypeFollowed = 3,
    SSTypeHdtz = 4,
    SSTypeSystemNotice = 5,
};
//MessageTypeRich 富文本消息类型
typedef NS_ENUM(NSInteger, kMXRichMessageType){
    kMXMessageTypeGoods = 1,
    kMXMessageTypeVoucher = 2,
    kMXMessageTypeDynamic = 3,
    kMxmessageTypeCertificate=5
};
//系统推送消息类型 待完善
enum kMXPushMessageType {
    kMXPushMessageTypeReport = 1,//举报反馈
    kMXPushMessageTypeCancelFocus ,
    kMXPushMessageTypeReportSituation,
    kMXPushMessageTypeFocus,
    kMXPushMessageTypeQualificationSuccess,
    kMXPushMessageTypeQualificationFailure,
    kMXPushMessageTypeActivityRegister,
    kMXPushMessageTypeActivityComment
};
typedef NS_ENUM(NSInteger, kMXMessageState) {
    kMXMessageStateSending,           //发送中
    kMXMessageStateSuccess,
    kMXMessageState_Failure,
    kMXMessageStateUploadFailure,    //文件上传失败
    kMXMessageStateDownloadFailure  //文件下载失败
};

enum kMXMessageNoticeSystemSubType {
    kMXMessageNoticeSystemSubTypeReport = 1,                //  1.举报成功
    kMXMessageNoticeSystemSubTypeFriendUnFollow,                  //  2.取消关注
    kMXMessageNoticeSystemSubTypeReportDealed,              //  3. 举报处理情况消息
    kMXMessageNoticeSystemSubTypeFriendFollow,              //  4. 新用户关注提醒
    kMXMessageNoticeSystemSubTypeCertificationSuccess,      //  5. 资质认证成功
    kMXMessageNoticeSystemSubTypeCertificationFailure,      //  6. 资质认证失败
    kMXMessageNoticeSystemSubTypeActivityEnroll,            //  7. 活动报名通知
    kMXMessageNoticeSystemSubTypeActivityComment,           //  8. 活动评论通知
    kMXMessageNoticeSystemSubTypeOperatingReport,           //  9. 经营报表推送
    kMXMessageNoticeSystemSubTypeGetCoupon,                 //  10. 优惠券领取通知
    kMXMessageNoticeSystemSubTypeBuyGoods,                  //  11. 商品购买通知
    kMXMessageNoticeSystemSubTypeGoodsComment,              //  12. 商品评论
    kMXMessageNoticeSystemSubTypeDynamicPraise,             //  13. 动态点赞通知
    kMXMessageNoticeSystemSubTypeNewFans,                   //  14. 新增粉丝
    kMXMessageNoticeSystemSubTypeBossSysService,             //  15. 后台客服的系统公告
    kMXMessageNoticeSystemSubTypeRegisterFriendFollow = -4  //  4. 新用户关注提醒
};

// Bubble View
#define  kRouterEventImageBubbleTapEventName  @"kRouterEventImageBubbleTapEventName"
#define  kRouterEventLocationBubbleTapEventName  @"kRouterEventLocationBubbleTapEventName"
#define  kRouterEventVideoBubbleTapEventName  @"kRouterEventVideoBubbleTapEventName"
#define  kRouterEventAvatarBubbleTapEventName  @"kRouterEventAvatarBubbleTapEventName"
#define  kRouterEventAudioBubbleTapEventName @"kRouterEventAudioBubbleTapEventName"
#define  kRouterEventInviteBubbleTapEventName  @"kRouterEventInviteBubbleTapEventName"
#define  kRouterEventVoucherBubbleTapEventName @"kRouterEventVoucherBubbleTapEventName"
#define  kRouterEventCardBubbleTapEventName @"kRouterEventCardBubbleTapEventName"
#define  kRouterEventRedPacketBubbleTapEventName @"kRouterEventRedPacketBubbleTapEventName"
#define  kRouterEventVoiceChatBubbleTapEventName @"kRouterEventVoiceChatBubbleTapEventName"
#define  kRouterEventProductInfoChatBubbleTapEventName @"kRouterEventProductInfoChatBubbleTapEventName"

#define  kRouterEventFileBubbleTapEventName @"kRouterEventFileBubbleTapEventName"

// Cell
#define  kRouterEventChatCellBubbleTapEventName  @"kRouterEventChatCellBubbleTapEventName"

#define  kResendButtonTapEventName  @"kResendButtonTapEventName"
#define  kShouldResendCell  @"kShouldResendCell"

#define ChatAvatarWidth 40 // 头像宽度

#define ChatAvatarPadding 5 // 头像到cell的内间距和头像到bubble的间距

#define ChatContentPadding 10 //  内容的边距

#define ChatServerTime @"ChatServerTime"
#define ChatLocalTime @"ChatLocalTime"

#define  kImageDownloadFinish @"kImageDownloadFinish"

#define kMaxRecordTime 60

#define NewFriendAvatar @"icon_new_friends"

#define hudongtongzhiAvatar @"icon_feature_navi_hdtz"



#define nf @"nf2"

#define hdtz @"hdtz2"

#define tbPrefix  @"chat"

#define kRefreshChatList @"refreshChatNotification"

#define kMXMemberServiceList @"MemberServiceList"

#endif
