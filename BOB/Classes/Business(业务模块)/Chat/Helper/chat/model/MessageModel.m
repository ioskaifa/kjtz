//
//  MessageModel.m
//  Moxian
//
//  Created by litiankun on 14-10-11.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import "MessageModel.h"
#import "ChatSendHelper.h"
#import "TalkManager.h"
#import "MXChatVoice.h"
#import "LcwlChat.h"
#import "FMResultSet.h"
#import "FriendModel.h"
#import "MXJsonParser.h"
#import "MXChatDefines.h"
#import "ChatVoiceManage.h"
#import "ChatCacheFileUtil.h"


@implementation MessageModel
- (instancetype)init {
    self=[super init];
    if (self) {
        self.dictionary = [[NSMutableDictionary alloc] init];
        // 消息ID
        self.messageId = [TalkManager msgId];
        // 发送者ID
        self.fromID =  [LcwlChat shareInstance].user.chatUser_id;
        // 接收者ID(或群组ID)
        self.toID=@"";
        //发送方向
        self.msg_direction = 1;
        //发送时间
        self.sendTime = [ChatSendHelper chatSendTime];
        self.type = 1;
        // 消息类型 1-文本 2-图片 3-语音 4-位置 5-GIF  6文件 7视频
        self.subtype = -1;
        // 0-单聊 1-群聊
        self.chatType = 0;
        // 0-发送中 1-发送成功 2-发送失败
        self.state = kMXMessageStateSending;
        //是否有读取过
        self.is_listened = 0;
        //服务器收到的回执
        self.is_acked = 0;
        //对方收到 收到的回执
        self.is_delivered = 0;
        //是否新的消息
        self.is_new = 1;
        // 文本内容/表情
        self.content=@"";
        
        //image **************************************************
        self.fileData=nil;
        self.imageHeight=0;
        self.imageWith=0;
        // 文件大小
        self.fileSize=0;
        
        //语音长度
        self.voiceLen= @"";
        
        // 文件名
        self.fileName=@"";
        
        // 文件下载地址(相对地址)
        self.fileUrl=@"";
        // 本地文件路径 图片
        self.locFileUrl=@"";
        
        
        
        // 纬度
        self.lat=@"";
        
        // 经度
        self.lng=@"";
        
        // 地址
        self.addr=@"";
        
        //是否播放
        self.isPlay=0;
        
        self.imageWith = @"";
        
        self.imageHeight = @"";
        
        
        
        
    }
    return self;
}

//
//-(void)initWithDict:(NSDictionary*)dict{
//
//    NSString* _id = [NSString stringWithFormat:@"%@",[dict objectForKey:@"id"]];
//    NSString* fromID = [NSString stringWithFormat:@"%@",[dict objectForKey:@"fromID"]];
//    NSString* toID = [NSString stringWithFormat:@"%@",[dict objectForKey:@"toID"]];
//    int type = [[dict objectForKey:@"type"]intValue];
//    int chatType = [[dict objectForKey:@"chatType"] intValue];
//    NSString* content = [NSString stringWithFormat:@"%@",[dict objectForKey:@"content"]];
//    //此处缺少sendTime
//    NSString* fileUrl = [NSString stringWithFormat:@"%@",[dict objectForKey:@"fileUrl"]];
//    NSString* lat = [NSString stringWithFormat:@"%@",[dict objectForKey:@"lat"]];
//    NSString* lng = [NSString stringWithFormat:@"%@",[dict objectForKey:@"lng"]];
//    NSString* addr =  [NSString stringWithFormat:@"%@",[dict objectForKey:@"addr"]];
//    self.messageId = _id;
//    self.fromID = fromID;
//    self.toID = toID;
//    self.subtype = type;
//    self.chatType = chatType;
//    self.content = content;
//    self.fileUrl = fileUrl;
//    self.imageRemoteUrl=[NSString stringWithFormat:@"%@!t200x200.jpg",fileUrl];
//    self.lat = lat;
//    self.lng = lng;
//    self.addr = addr;
//    self.sendTime=[NSString stringWithFormat:@"%@",[dict objectForKey:@"sendTime"]];
//
//    self.voiceLen =[NSString stringWithFormat:@"%@",dict[@"voiceLen"]] ;
//    self.fileName=[NSString stringWithFormat:@"%@",[dict objectForKey:@"fileName"]];
//
//    self.isPlay = 0;
//    NSArray* array = [_id componentsSeparatedByString:@"-"];
//    if (array.count > 2) {
//        self.chat_with = array[1];
//    }
//
//
//}
//将对象转换为字典
-(NSMutableDictionary*)toDictionary
{
    NSDateFormatter* f=[[NSDateFormatter alloc]init];
    [f setDateFormat:[NSString stringWithFormat:@"%@ HH:mm:ss",MXLang(@"Public_dateFormat",@"yyyy年MM月dd日")]];//@"MM/dd/yyyy HH:mm:ss"];
    
    [self.dictionary setValue:self.messageId forKey:kMESSAGE_ID];
    [self.dictionary setValue:self.fromID forKey:kMESSAGE_FROM];
    [self.dictionary setValue:self.toID forKey:kMESSAGE_TO];
    [self.dictionary setValue:self.content forKey:kMESSAGE_CONTENT];
    //    [self.dictionary setValue:[f stringFromDate:self.timeSend] forKey:kMESSAGE_TIMESEND];
    [self.dictionary setValue:@"1" forKey:kMESSAGE_TYPE];
    [self.dictionary setValue:nil forKey:kMESSAGE_FILEDATA];
    [self.dictionary setValue:nil forKey:kMESSAGE_FILENAME];
    [self.dictionary setValue:0 forKey:kMESSAGE_FILESIZE];
    [self.dictionary setValue:0 forKey:kMESSAGE_LOCATION_X];
    [self.dictionary setValue:0 forKey:kMESSAGE_LOCATION_Y];
    [self.dictionary setValue:0 forKey:kMESSAGE_TIMELEN];
    [self.dictionary setValue:0 forKey:kMESSAGE_No];
    [self.dictionary setValue:@"No" forKey:kMESSAGE_ISSEND];
    [self.dictionary setValue:@"Yes" forKey:kMESSAGE_ISREAD];
    
    return self.dictionary ;
}
-(void)setToID:(NSString *)toID{
    _toID =toID;
}

+(int)stringToSessionType:(NSString *)type{
    if ([type isEqualToString:@"NORMAL"]) {
        return MessageTypeNormal;
    }else if([type isEqualToString:@"GROUPCHAT"]){
        return MessageTypeGroupchat;
    }else if([type isEqualToString:@"SGROUP"]){
        return MessageTypeSgroup;
    }
    return 0;
}

+(NSString* )intToSession:(NSInteger )type{
    if (type == MessageTypeNormal) {
        return @"NORMAL";
    }else if(type == MessageTypeGroupchat) {
        return @"GROUPCHAT";
    }else if(type == MessageTypeSgroup){
        return @"SGROUP";
    }else if (type == MessageTypeClient) {
        return @"CLIENT";
    }
    return 0;
}

+(EMConversationType)getTypeByChatType:(NSInteger )type{
    if (type == MessageTypeNormal) {
        return eConversationTypeChat;
    }else if(type == MessageTypeGroupchat) {
        return eConversationTypeGroupChat;
    }else if(type == MessageTypeSgroup){
        return eConversationTypeGroupChat;
    }
    return eConversationTypeChat;
}
    
- (id)initWithTextMessage:(NSString *)receiver text:(NSString*)text messageType:(EMConversationType)messageType{
    self = [self init];
    self.subtype = kMXMessageTypeText;
    self.chatType = messageType;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.toID = receiver;
    self.chat_with = receiver;
    self.content = text;
    
    UserModel* user = [LcwlChat shareInstance].user;
    if (messageType == eConversationTypeChat) {
        self.type = MessageTypeNormal;
        self.name = user.smartName;
    }else{
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    }
    self.is_new = 1;
    
    self.avatar = user.head_photo;
    NSDictionary* bodyDict = @{@"attr1":self.content};
    self.body = [MXJsonParser dictionaryToJsonString:bodyDict];

    return self;
}

- (id)initWithAtTextMessage:(NSString *)receiver
                    atUseId:(NSArray *)atUseIdArr
                       text:(NSString*)text
                messageType:(EMConversationType)messageType{
    self = [self init];
    self.subtype = kMXMessageTypeText;
    self.chatType = messageType;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.toID = receiver;
    self.chat_with = receiver;
    self.content = text;
    
    UserModel* user = [LcwlChat shareInstance].user;
    if (messageType == eConversationTypeChat) {
        self.type = MessageTypeNormal;
        self.name = user.smartName;
    }else{
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    }
    self.is_new = 1;
    
    self.avatar = user.head_photo;
    NSString *atUserIds = atUseIdArr.count ? [atUseIdArr componentsJoinedByString:@","]:@"0";
    NSDictionary *bodyDict = @{@"attr1":self.content,
                               @"attr2":atUserIds};
    self.body = [MXJsonParser dictionaryToJsonString:bodyDict];

    return self;
}

- (id)initWithImageMessage:(NSString *)receiver image:(UIImage* )image messageType:(EMConversationType)messageType{
    self = [self init];
    self.subtype = kMXMessageTypeImage;
    self.chatType = messageType;
    self.toID = receiver;
    UserModel* user = [LcwlChat shareInstance].user;
    self.avatar = user.head_photo;
    self.fromID = user.chatUser_id;
    self.chat_with = receiver;
    self.locFileUrl = [TalkManager saveChatImage:image];
    self.fileUrl = @"";
    self.image = image;
    self.size = image.size;
    self.is_new = 1;
    
    if (self.chatType == eConversationTypeGroupChat) {
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    } else if(self.chatType == eConversationTypeChat) {
        self.name = user.smartName;
    }
    
    return self;
}

- (id)initWithVideoMessage:(NSString *)receiver videoUrl:(NSURL* )videoUrl messageType:(EMConversationType)messageType {
    self = [self init];
    self.subtype = kMxmessageTypeVideo;
    self.chatType = messageType;
    self.toID = receiver;
    UserModel* user = [LcwlChat shareInstance].user;
    self.avatar = user.head_photo;
    self.fromID = user.chatUser_id;
    self.chat_with = receiver;
    //self.locFileUrl = [TalkManager saveChatImage:image];
    self.locFileUrl = videoUrl.absoluteString;
    self.is_new = 1;
    
    if (self.chatType == eConversationTypeGroupChat) {
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    } else if(self.chatType == eConversationTypeChat) {
        self.name = user.smartName;
    }
    
    return self;
}

- (id)initWithEmotionMessage:(NSString *)receiver text:(NSString*)text messageType:(EMConversationType)messageType package:(NSString*)package {
    
//    self = [self initWithTextMessage:receiver text:text messageType:messageType];
    self = [self init];
    
    if (self) {
        
//        self.subtype = kMXMessageTypeGif;
//        self.chatType = messageType;
//        self.fromID = [LcwlChat shareInstance].user.chatUser_id;
//        self.toID = receiver;
//        self.chat_with = receiver;
//        self.content = text;
//        if (self.chatType == eConversationTypeGroupChat) {
//            self.type = MessageTypeGroupchat;
//        }
//        self.is_new = 1;
//
//        self.ext = @{@"package":package};
        
        self.subtype = kMXMessageTypeGif;
        self.chatType = messageType;
        self.fromID = [LcwlChat shareInstance].user.chatUser_id;
        self.toID = receiver;
        self.chat_with = receiver;
        self.content = text;
        
        self.is_new = 1;
        UserModel* user = [LcwlChat shareInstance].user;
        if (messageType == eConversationTypeChat) {
            self.type = MessageTypeNormal;
            self.name = user.smartName;
        }else{
            ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
            self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
            self.type = MessageTypeGroupchat;
        }
        
        self.avatar = user.head_photo;
        NSDictionary* bodyDict = @{@"attr1":package};
        self.body = [MXJsonParser dictionaryToJsonString:bodyDict];
        
        
    }
    
    return self;
}


- (id)initWithImageMessage:(NSString *)receiver path:(NSString*)path messageType:(EMConversationType)messageType {
    self = [self init];
    self.subtype = kMXMessageTypeImage;
    self.chatType = messageType;
    self.toID = receiver;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.chat_with = receiver;

    UIImage *image = [UIImage imageWithContentsOfFile:path];
    self.size = image.size;
    self.locFileUrl = path;
    self.fileUrl = @"";
    self.image = image;
    self.size = image.size;
    self.is_new = 1;
    
    UserModel* user = [LcwlChat shareInstance].user;
    self.avatar = user.head_photo;
    if (self.chatType == eConversationTypeGroupChat) {
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    } else {
        self.name = user.smartName;
    }
    
    NSDictionary* bodyDict = @{@"attr1":self.imageRemoteUrl,@"attr2":[NSNumber numberWithInt:self.size.width],@"attr3":[NSNumber numberWithInt:self.size.height]};
    self.body = [MXJsonParser dictionaryToJsonString:bodyDict];

    return self;
}

- (id)initWithLocationMessage:(NSString *)receiver latitude:(double )latitude longitude:(double )longitude  address:(NSString *)address andSubAddress:(NSString *)subAddress key:(NSString *)key messageType:(EMConversationType)messageType {
    self = [self init];
    self.content = address;
    self.subtype = kMXMessageTypeLocation;
    self.chatType = messageType;
    self.toID = receiver;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.chat_with = receiver;
    self.fileUrl = key;
    self.lat = [NSString stringWithFormat:@"%f",latitude];
    self.lng = [NSString stringWithFormat:@"%f",longitude];
    
    self.addr = address;
    self.detailAddr = subAddress;
    self.is_new = 1;
    
    UserModel* user = [LcwlChat shareInstance].user;
    self.avatar = user.head_photo;
    if (self.chatType == eConversationTypeGroupChat) {
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    } else {
        self.name = user.smartName;
    }
    NSDictionary* bodyDict = @{@"attr1":self.addr?:@"",@"attr2":self.lat,@"attr3":self.lng,@"attr4":subAddress?:@"",@"attr5":key?:@""};
    self.body = [MXJsonParser dictionaryToJsonString:bodyDict];

    return self;
}

- (id)initWithVoiceMessage:(NSString *)receiver voice:(MXChatVoice *)voice messageType:(EMConversationType)messageType{
    self = [self init];
    self.subtype = kMXMessageTypeVoice;
    self.chatType = messageType;
    self.toID = receiver;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.locFileUrl = voice.localPath;
    self.fileName = voice.displayName;
    self.chat_with = receiver;
    self.voiceLen = [NSString stringWithFormat:@"%@",@(voice.duration)];
    self.fileSize = voice.fileLength;
    self.isPlay = 1;
    self.is_new = 1;
    UserModel* user = [LcwlChat shareInstance].user;
    self.name = user.smartName;
    self.avatar = user.head_photo;
    if (self.chatType == eConversationTypeGroupChat) {
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    }
     NSDictionary* bodyDict = @{@"attr2":self.voiceLen,@"attr1":self.imageRemoteUrl};
    self.body = [MXJsonParser dictionaryToJsonString:bodyDict];

    return self;
}

- (id)initWithCardMessage:(NSString *)receiver user:(NSDictionary* )userDict messageType:(EMConversationType)messageType{
    self = [self init];
    self.subtype = kMXMessageTypeCard;
    self.chatType = messageType;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.toID = receiver;
    self.chat_with = receiver;
    
    self.is_new = 1;
    UserModel* user = [LcwlChat shareInstance].user;
    if (messageType == eConversationTypeChat) {
        self.type = MessageTypeNormal;
        self.name = user.smartName;
    }else{
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    }
    self.avatar = user.head_photo;
    self.body = [MXJsonParser dictionaryToJsonString:userDict];
    
    return self;
}

- (id)initWithProductLinkMessage:(NSString *)receiver user:(NSDictionary* )userDict messageType:(EMConversationType)messageType {
    self = [self init];
    self.subtype = kMXMessageTypeProductLink;
    self.chatType = messageType;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.toID = receiver;
    self.chat_with = receiver;
    
    self.is_new = 1;
    UserModel* user = [LcwlChat shareInstance].user;
    if (messageType == eConversationTypeChat) {
        self.type = MessageTypeNormal;
        self.name = user.smartName;
    }else{
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    }
    self.avatar = user.head_photo;
    self.body = [MXJsonParser dictionaryToJsonString:userDict];
    
    return self;
}



- (id)initWithRedPackMessage:(NSString *)receiver text:(NSString* )text redpacketId:(NSString* ) redPacketId messageType:(EMConversationType)messageType{
    self = [self init];
    
    self.subtype = kMxmessageTypeRedPack;
    self.chatType = messageType;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.toID = receiver;
    self.chat_with = receiver;
    self.content = text;
    
    NSDictionary* bodyDict = @{@"attr2":text,@"attr3":redPacketId};
    self.body = [MXJsonParser dictionaryToJsonString:bodyDict];
    UserModel* user = [LcwlChat shareInstance].user;
    self.name = user.user_name;
    if (self.chatType == eConversationTypeGroupChat) {
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    }
    
    self.avatar = user.head_photo;
    self.is_new = 1;
    return self;
    
}


+(MessageModel*)initWithFMResultSet:(FMResultSet*)rs{
    MessageModel* msg = [[MessageModel alloc]init];
    msg.is_acked = [rs intForColumn:@"is_acked"];
    msg.is_delivered = [rs intForColumn:@"is_delivered"];
    msg.is_listened = [rs intForColumn:@"is_listened"];
    msg.state = [rs intForColumn:@"msg_status"];
    msg.sendTime = [rs stringForColumn:@"msg_time"];
    msg.msg_direction = [rs intForColumn:@"msg_direction"];
    msg.is_new = [rs intForColumn:@"is_new"];
    msg.messageId = [rs stringForColumn:@"msg_code"];
    msg.avatar = [rs stringForColumn:@"avatar"];
    msg.name = [rs stringForColumn:@"name"];
//    NSInteger msg_direction = msg.msg_direction;
    msg.chat_with = [rs stringForColumn:@"chat_with"];
    msg.locFileUrl = [rs stringForColumn:@"local_file_url"];
    msg.fileUrl = [rs stringForColumn:@"file_url"];
    msg.body = [rs stringForColumn:@"msg_body"];
    msg.isPlay = [rs intForColumn:@"is_play"];
    msg.chatType = [rs intForColumn:@"isGroup"];
    msg.type = [rs intForColumn:@"type"];
    msg.subtype = [rs intForColumn:@"subtype"];
    msg.fromID = [rs stringForColumn:@"fromID"];
    msg.toID = [rs stringForColumn:@"toID"];
    msg.addr = [rs stringForColumn:@"toID"];
    [self.class parse:msg];

    return msg;
}



+(void)parse:(MessageModel*) model{
    NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:model.body];
    if (model.type == MessageTypeSs) {
         model.content = bodyDict[@"attr4"];
    }
    else if(model.type == MessageTypeSgroup){
        if (model.subtype == SGROUPTypeAdd_Member) {
            model.content = bodyDict[@"attr1"];
        }
    }else{
        if (model.subtype == kMXMessageTypeText) {
            model.content = bodyDict[@"attr1"];
        }else if(model.subtype == kMXMessageTypeImage){
            model.imageRemoteUrl = bodyDict[@"attr1"];
            int width = [bodyDict[@"attr2"]intValue];
            int height = [bodyDict[@"attr3"]intValue];
            model.size = CGSizeMake(width, height);
        }else if(model.subtype == kMXMessageTypeLocation){
            model.addr = bodyDict[@"attr1"];
            model.detailAddr = bodyDict[@"attr4"];
            model.lat = bodyDict[@"attr2"];
            model.lng = bodyDict[@"attr3"];
        }else if(model.subtype == kMXMessageTypeVoice){
            model.imageRemoteUrl = bodyDict[@"attr1"];
            model.voiceLen = bodyDict[@"attr2"];
        }
    }
  
}

- (id)copyWithZone:(NSZone *)zone
{
    MessageModel *copy = [[[self class] allocWithZone:zone] init];
    copy.fromID = [self.fromID copy];
    copy.toID = [self.toID copy];
    copy.subtype = self.subtype;
    copy.type = self.type;
    copy.chatType = self.chatType;
    copy.content = [self.content copy];
    copy.imageWith = [self.imageWith copy];
    copy.imageHeight = [self.imageHeight copy];
    copy.imageRemoteUrl = [self.imageRemoteUrl copy];
    copy.fileSize = self.fileSize;
    copy.size = self.size;
    copy.voiceLen = [self.voiceLen copy];
    copy.fileName = [self.fileName copy];
    copy.fileUrl = [self.fileUrl copy];
    copy.locFileUrl = [self.locFileUrl copy];
    copy.lat = [self.lat copy];
    copy.lng = [self.lng copy];
    copy.addr = [self.addr copy];
    copy.messageId = [self.messageId copy];
    copy.sendTime = [self.sendTime copy];
    copy.msg_direction = self.msg_direction;
    copy.is_acked = self.is_acked;
    copy.is_delivered = self.is_delivered;
    copy.is_listened = self.is_listened;
    copy.is_new = self.is_new;
    copy.state = self.state;
    copy.body = [self.body copy];
    copy.chat_with = [self.chat_with copy];
    copy.avatar = [self.avatar copy];
    copy.name = [self.name copy];
    
    return copy;
}
- (id)mutableCopyWithZone:(NSZone *)zone
{
    MessageModel *copy = [[[self class] allocWithZone:zone] init];
    copy.fromID = [self.fromID copy];
    copy.toID = [self.toID copy];
    copy.subtype = self.subtype;
    copy.type = self.type;
    copy.chatType = self.chatType;
    copy.content = [self.content copy];
    copy.imageWith = [self.imageWith copy];
    copy.imageHeight = [self.imageHeight copy];
    copy.imageRemoteUrl = [self.imageRemoteUrl copy];
    copy.fileSize = self.fileSize;
    copy.size = self.size;
    copy.voiceLen = [self.voiceLen copy];
    copy.fileName = [self.fileName copy];
    copy.fileUrl = [self.fileUrl copy];
    copy.locFileUrl = [self.locFileUrl copy];
    copy.lat = [self.lat copy];
    copy.lng = [self.lng copy];
    copy.addr = [self.addr copy];
    copy.messageId = [self.messageId copy];
    copy.sendTime = [self.sendTime copy];
    copy.msg_direction = self.msg_direction;
    copy.is_acked = self.is_acked;
    copy.is_delivered = self.is_delivered;
    copy.is_listened = self.is_listened;
    copy.is_new = self.is_new;
    copy.state = self.state;
    copy.body = [self.body copy];
    copy.chat_with = [self.chat_with copy];
    copy.avatar = [self.avatar copy];
    copy.name = [self.name copy];
    
    return copy;
}


-(NSString* )jsonString{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithCapacity:10];
    if (self.type == MessageTypeNormal || self.type == MessageTypeGroupchat) {
        if (self.subtype == kMXMessageTypeText ||
            self.subtype == kMXMessageTypeGif ||
            self.subtype == kMXMessageTypeVoiceChat ||
            self.subtype == kMXMessageTypeFile) {
            [dict setValue:[MXJsonParser jsonToDictionary:self.body]forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,@"avatar":self.avatar,@"userid":self.fromID} forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        }else if(self.subtype == kMXMessageTypeImage){
            [dict setValue:[MXJsonParser jsonToDictionary:self.body] forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,@"avatar":self.avatar,@"userid":self.fromID} forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        }else if(self.subtype == kMXMessageTypeLocation){
            [dict setValue:[MXJsonParser jsonToDictionary:self.body] forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,@"avatar":self.avatar,@"userid":self.fromID} forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        }else if(self.subtype == kMXMessageTypeVoice){
            [dict setValue:[MXJsonParser jsonToDictionary:self.body] forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,@"avatar":self.avatar,@"userid":self.fromID} forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        }else if(self.subtype == kMxmessageTypeVideo){
            [dict setValue:[MXJsonParser jsonToDictionary:self.body] forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,@"avatar":self.avatar,@"userid":self.fromID} forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        }else if(self.subtype == kMXMessageTypeCard){
            [dict setValue:[MXJsonParser jsonToDictionary:self.body] forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,@"avatar":self.avatar,@"userid":self.fromID} forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        }else if(self.subtype == kMXMessageTypeFile){
            [dict setValue:[MXJsonParser jsonToDictionary:self.body] forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,@"avatar":self.avatar,@"userid":self.fromID} forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        }else if(self.subtype == kMXMessageTypeVoiceChat){
            [dict setValue:[MXJsonParser jsonToDictionary:self.body] forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,@"avatar":self.avatar,@"userid":self.fromID} forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        } else if (self.subtype == kMXMessageTypeWithdraw){
            [dict setValue:[MXJsonParser jsonToDictionary:self.body] forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,
                             @"avatar":self.avatar,
                             @"userid":self.fromID}
                    forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        }else if(self.subtype == kMXMessageTypeProductLink){
            [dict setValue:[MXJsonParser jsonToDictionary:self.body] forKey:@"body"];
            [dict setValue:self.fromID forKey:@"from"];
            [dict setValue:self.messageId forKey:@"msgCode"];
            [dict setValue:self.sendTime forKey:@"msgTime"];
            [dict setValue:[self.class intToSession:self.type] forKey:@"sessionType"];
            [dict setValue:self.toID forKey:@"to"];
            [dict setValue:@{@"name":self.name,@"avatar":self.avatar,@"userid":self.fromID} forKey:@"user"];
            [dict setValue:@(self.subtype) forKey:@"msgType"];
        }
        
        if(self.type == MessageTypeGroupchat){
            ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
            if (group) {
                 [dict setValue:@{@"groupId":group.groupId,
                                  @"groupName":group.groupName,
                                  @"groupAvatar":group.groupAvatar}
                         forKey:@"group"];
            }
        }
        
        //如果creatorRole为10或11，表示和客服聊天
        if(self.creatorRole > 0) {
            [dict setValue:@(self.creatorRole) forKey:@"creatorRole"];
            [dict setValue:@"SEVICE" forKey:@"sessionType"];
        }
        
        return  [MXJsonParser dictionaryToJsonString:dict];
    } else if (self.type == MessageTypeClient) {
        [dict setValue:[MXJsonParser jsonToDictionary:self.body]
                forKey:@"body"];
        [dict setValue:self.fromID forKey:@"from"];
        [dict setValue:self.messageId forKey:@"msgCode"];
        [dict setValue:self.sendTime forKey:@"msgTime"];
        [dict setValue:[self.class intToSession:self.type]
                forKey:@"sessionType"];
        [dict setValue:self.toID forKey:@"to"];
        [dict setValue:@{@"name":self.name,
                         @"avatar":self.avatar,
                         @"userid":self.fromID}
                forKey:@"user"];
        [dict setValue:@(self.subtype) forKey:@"msgType"];
        
        //如果creatorRole为10或11，表示和客服聊天
        if(self.creatorRole > 0) {
            [dict setValue:@(self.creatorRole) forKey:@"creatorRole"];
            [dict setValue:@"SEVICE" forKey:@"sessionType"];
        }
        return  [MXJsonParser dictionaryToJsonString:dict];
    }
    return @"";
}

-(NSString* )receiptString{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc]initWithCapacity:10];
    [dict setValue:@"ACK" forKey:@"sessionType"];
    [dict setValue:self.messageId forKey:@"msgCode"];
    if (self.chatType == eConversationTypeGroupChat) {
        [dict setValue:[NSString stringWithFormat:@"%@@%@",self.toID,[self.class intToSession:self.type]] forKey:@"sessionId"];
        [dict setValue:self.chat_with forKey:@"from"];
        [dict setValue:self.toID forKey:@"to"];
    }else{
        [dict setValue:[NSString stringWithFormat:@"%@@%@",self.toID,[self.class intToSession:self.type]] forKey:@"sessionId"];
        [dict setValue:self.fromID forKey:@"from"];
        [dict setValue:self.toID forKey:@"to"];
    }
    [dict setValue:@(self.subtype) forKey:@"msgType"];
    [dict setValue:self.sendTime forKey:@"msgTime"];
    [dict setValue:[LcwlChat shareInstance].user.chatUser_id forKey:@"recipient"];
    return  [MXJsonParser dictionaryToJsonString:dict];
}

-(NSString* )content{
    if ([StringUtil isEmpty:_content]) {
        NSString *str = @"";
        if(self.type == MessageTypeNormal ||
           self.type == MessageTypeGroupchat ||
           self.type == MessageTypeS){
            switch (self.subtype) {
                case kMXMessageTypeImage:{
                    str = MXLang(@"Talk_chat_type_image", @"[图片]");
                } break;
                case kMXMessageTypeText:{
                    NSDictionary* dict = [MXJsonParser jsonToDictionary:self.body];
                    str = dict[@"attr1"];
                } break;
                case kMXMessageTypeGif:{
                    str = @"[动画表情]";
                } break;
                    
                case kMXMessageTypeVoice:{
                    str = MXLang(@"Talk_chat_type_audio",  @"[语音消息]");
                } break;
                case kMXMessageTypeLocation: {
                    str =MXLang(@"Talk_chat_type_location",  @"[地理位置]");
                } break;
                case kMxmessageTypeVideo: {
                    str =MXLang(@"Talk_chat_type_location",  @"[视频]");
                } break;
                case kMXMessageTypeCard:{
                    str = @"[名片]";
//                    NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:self.body];
//                    if ([self.fromID isEqualToString: [LcwlChat shareInstance].user.chatUser_id]) {
//                        str = [NSString stringWithFormat:@"我推荐了%@",bodyDict[@"attr3"]];
//                    }else{
//                        str = [NSString stringWithFormat:@"%@推荐了%@",self.name,bodyDict[@"attr3"]];
//                    }
                } break;
                case kMxmessageTypeRedPack:{
                    NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:self.body];
                    str = [NSString stringWithFormat:@"[红包] %@",bodyDict[@"attr2"]];
                    break;
                } case kMxmessageTypeCustom:{
                      NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:self.body];
                      str = bodyDict[@"attr1"];
                } break;
                case kMxmessageTypeReceiveRedpacket:{
                    NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:self.body];
                    str = bodyDict[@"attr1"];
                } break;
                case kMXMessageTypeFile:{
                    str = @"[文件]";
                } break;
                case kMXMessageTypeVoiceChat:{
                    str = @"[语音通话]";
                    break;
                }
                case kMXMessageTypeWithdraw:{
                    str = [self formatWithdrawContentTxt];
                    break;
                }
                case kMXMessageTypeProductLink:{
                    str = @"[商品链接]";
                    break;
                }
                default: {
                     NSDictionary* bodyDict = [MXJsonParser jsonToDictionary:self.body];
                     str = bodyDict[@"attr1"];
                } break;
            }
        }else if(self.type == MessageTypeSs){
            switch (self.subtype) {
                case SSTypeFollow:{
                    NSDictionary* dict = [MXJsonParser jsonToDictionary:self.body];
                    str = dict[@"attr4"];
                }break;
                case SSTypeHdtz:{
                    NSDictionary* dict = [MXJsonParser jsonToDictionary:self.body];
                    str = dict[@"attr1"];
                }   break;
                default: {
                } break;
            }
                    
        }else if(self.type == MessageTypeSgroup){
//            switch (self.subtype) {
//                case SGROUPTypeAdd_Member:{
                    NSDictionary* dict = [MXJsonParser jsonToDictionary:self.body];
                    str = dict[@"attr1"];
//                }break;
//                default: {
//                } break;
//            }
            
        }
        _content = str;
    } else {
        if (self.type == MessageTypeNormal ||
            self.type == MessageTypeGroupchat) {
            if (self.subtype == kMXMessageTypeVoiceChat) {
                _content = @"[语音通话]";
            }
        }
    }
    return _content;
}

- (id)initWithSgroupMessage:(NSString *)tip user:(NSString* )chatter messageType:(SGROUPType)messageType{
    self = [self init];
    self.type = MessageTypeSgroup;
    self.subtype = messageType;
    self.chatType = eConversationTypeGroupChat;
    self.fromID = chatter;
    self.chat_with = chatter;
    self.is_new = 1;
    UserModel* user = [LcwlChat shareInstance].user;
    self.toID = user.chatUser_id;
    self.name = user.user_name;
    self.avatar = user.head_photo;
    NSDictionary* bodyDict = @{@"attr1":tip};
    self.body = [MXJsonParser dictionaryToJsonString:bodyDict];
    return self;
}

- (id)initWithVoiceCallMessage:(NSString *)receiver text:(NSString*)text messageType:(EMConversationType)messageType subMessageType:(kMXVoiceChatType)subMessageType {
    self = [self init];
    self.subtype = kMXMessageTypeVoiceChat;
    self.chatType = messageType;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.toID = receiver;
    self.chat_with = receiver;
    self.content = text;
    self.state = kMXMessageStateSuccess;
    
    UserModel* user = [LcwlChat shareInstance].user;
    if (messageType == eConversationTypeChat) {
        self.type = MessageTypeNormal;
        self.name = user.smartName;
    }else{
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    }
    self.is_new = 1;
    
    self.avatar = user.head_photo;
    NSDictionary* bodyDict = @{@"attr1":text,
                               @"attr2":[ChatVoiceManage shareInstance].roomID,
                               @"attr3":@(subMessageType)};
    self.body = [MXJsonParser dictionaryToJsonString:bodyDict];

    return self;
}

- (id)initWithFileMessage:(NSString *)receiver
                 filetype:(NSString*)fileType
                     size:(long long)size
                 fileName:(NSString*)fileName
              messageType:(EMConversationType)messageType {
    self = [self init];
    self.subtype = kMXMessageTypeFile;
    self.chatType = messageType;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.toID = receiver;
    self.chat_with = receiver;
    self.content = @"[文件]";
    
    UserModel* user = [LcwlChat shareInstance].user;
    if (messageType == eConversationTypeChat) {
        self.type = MessageTypeNormal;
        self.name = user.smartName;
    }else{
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    }
    self.is_new = 1;
    
    self.avatar = user.head_photo;
    self.fileSubType = fileType;
    self.fileName = fileName;
    self.fileSize = size;
    return self;
}

- (id)initWithFileMessage:(MessageModel *)msg
                 receiver:(NSString *)receiver
              messageType:(EMConversationType)messageType {
    self = [self init];
    self.subtype = kMXMessageTypeFile;
    self.chatType = messageType;
    self.fromID = [LcwlChat shareInstance].user.chatUser_id;
    self.toID = receiver;
    self.chat_with = receiver;
    self.content = @"[文件]";
    
    UserModel* user = [LcwlChat shareInstance].user;
    if (messageType == eConversationTypeChat) {
        self.type = MessageTypeNormal;
        self.name = user.smartName;
    }else{
        self.type = MessageTypeGroupchat;
        ChatGroupModel* group = [[LcwlChat shareInstance].chatManager loadGroupByChatId:self.chat_with];
        self.name = group.nickname.length > 0 ? group.nickname : user.smartName;
    }
    self.avatar = user.head_photo;
    self.is_new = 1;
    self.body = msg.body;
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:msg.body];
    if (bodyDic[@"attr2"]) {
        self.fileSubType = [bodyDic[@"attr2"] description];
    }
    if (bodyDic[@"attr4"]) {
        self.fileName = [bodyDic[@"attr4"] description];
    }
    if (bodyDic[@"attr1"]) {
        self.fileUrl = [bodyDic[@"attr1"] description];
    }
    self.fileSize = msg.fileSize;
    return self;
}

- (id)initWithClientMessageSubType:(kMXClientType)subType {
    self = [self init];
    self.type = MessageTypeClient;
    self.name = UDetail.user.smartName;
    self.avatar = UDetail.user.head_photo;
    self.chatType = eConversationTypeClientChat;
    self.subtype = kMXMessageTypeClient;
    self.fromID = UDetail.user.user_id;
    self.toID = UDetail.user.client_authCode;
    NSDictionary *bodyDic = @{@"attr1":@(subType)};
    NSString *bodyString = [MXJsonParser dictionaryToJsonString:bodyDic];
    self.body = bodyString;
    return self;
}

- (NSString *)smartName {
    return [[LcwlChat shareInstance].chatManager remark:_fromID] ?: _name;
}

- (kMXFileType)fileType {
    if (self.subtype != kMXMessageTypeFile) {
        return kMXFileTypeUnknow;
    }
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:self.body];
    NSString *type = bodyDic[@"attr2"];
    return [MessageModel fileType:type];
}

+ (kMXFileType)fileType:(NSString *)type {
    if ([StringUtil isEmpty:type]) {
        return kMXFileTypeUnknow;
    }
    type = type.lowercaseString;
    if ([type isEqualToString:@"doc"] ||
        [type isEqualToString:@"docx"]) {
        return kMXFileTypeWord;
    } else if ([type isEqualToString:@"xls"] ||
               [type isEqualToString:@"xlsx"]) {
        return kMXFileTypeExcel;
    } else if ([type isEqualToString:@"pdf"]) {
        return kMXFileTypePDF;
    }
    return kMXFileTypeUnknow;
}

- (BOOL)messageFileExists {
    if ([StringUtil isEmpty:self.fullPath]) {
        return NO;
    }
    return [ChatCacheFileUtil fileExistsAtPath:self.fullPath];
}

- (NSString *)fullPath {
    if (!_fullPath) {
        NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:self.body];
        if (![bodyDic isKindOfClass:NSDictionary.class]) {
            return @"";
        }
        NSString *fileName = self.actualFileName;
        if ([StringUtil isEmpty:fileName]) {
            return @"";
        }
        NSString *rootPath = [ChatCacheFileUtil sharedInstance].userDocPath;
        _fullPath = [rootPath stringByAppendingPathComponent:fileName];
    }
    return _fullPath;
}

///后缀名 语音attr3 文件attr2 视频attr4
- (NSString *)actualFileName {
    if (!_actualFileName) {
        NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:self.body];
        if (![bodyDic isKindOfClass:NSDictionary.class]) {
            return @"";
        }
        ///后缀名
        NSString *pathExtension;
        if (self.subtype == kMXMessageTypeVoice) {
            pathExtension = bodyDic[@"attr3"];
            if ([StringUtil isEmpty:pathExtension]) {
                pathExtension = @"amr";
            }
        } else if (self.subtype == kMxmessageTypeVideo) {
            pathExtension = bodyDic[@"attr4"];
            if ([StringUtil isEmpty:pathExtension]) {
                pathExtension = @"MP4";
            }
        } else if (self.subtype == kMXMessageTypeFile) {
            pathExtension = bodyDic[@"attr2"];
        }
        if ([StringUtil isEmpty:pathExtension]) {
            return @"";
        }
        ///文件青牛链接
        NSString *urlStr = bodyDic[@"attr1"];
        NSString *fileName = StrF(@"%@.%@", urlStr, pathExtension);
        _actualFileName = fileName;
    }
    return _actualFileName;
}

- (NSString *)urlString {
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:self.body];
    if (![bodyDic isKindOfClass:NSDictionary.class]) {
        return @"";
    }
    if (bodyDic[@"attr1"]) {
        return StrF(@"%@/%@", UDetail.user.qiniu_domain, bodyDic[@"attr1"]);
    }
    return @"";
}

- (NSString *)formatWithdrawContentTxt {
    NSString *txt = @"你撤回了一条消息";
    if (self.msg_direction) {
        return txt;
    }
    FriendModel *model = [[LcwlChat shareInstance].chatManager loadFriendByChatId:self.fromID];
    if ([model isKindOfClass:FriendModel.class]) {
        NSString *name = model.remark?:model.nick_name?:model.name;
        txt = StrF(@"'%@'%@", name, @"撤回一条消息");
    } else {
        txt = StrF(@"'%@'%@", self.name, @"撤回一条消息");
    }
    return txt;
}

- (BOOL)isOutOfWithdrawTimeLimit {
    NSTimeInterval nowTime = [[ChatSendHelper chatSendTime] doubleValue] /1000;
    NSTimeInterval currentTime = [self.sendTime doubleValue] / 1000;
    NSTimeInterval balance = nowTime - currentTime;
    balance = balance / 60;
    NSInteger min = (NSInteger)balance;
    if (min <= 5) {
        return NO;
    }
    return YES;
}

///PC端过来消息 且设置手机静音
- (BOOL)inDisturb {
    if (self.isPCMsg) {
        if ([LcwlChat shareInstance].user.client_silenceStatus) {
            return YES;
        }
    }
    return NO;
}

- (id)attr1 {
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:self.body];
    return [bodyDic valueForKey:NSStringFromSelector(_cmd)];
}

- (id)attr2 {
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:self.body];
    return [bodyDic valueForKey:NSStringFromSelector(_cmd)];
}

- (id)attr3 {
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:self.body];
    return [bodyDic valueForKey:NSStringFromSelector(_cmd)];
}

- (id)attr4 {
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:self.body];
    return [bodyDic valueForKey:NSStringFromSelector(_cmd)];
}
@end
