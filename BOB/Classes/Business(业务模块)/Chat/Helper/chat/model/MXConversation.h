//
//  MXConversation.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/7/27.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXChatDefines.h"
#import "MessageModel.h"



@interface MXConversation : NSObject<NSCopying,NSMutableCopying>

/*!
 @property
 @brief 会话对方的用户名. 如果是群聊, 则是群组的id
 */
@property (nonatomic, strong) NSString *chat_id;

/*!
 @property
 @brief 此会话中的消息列表
 */
@property (nonatomic, strong) NSMutableArray *messages ;

/*!
 @property
 @brief 会话类型
 */
@property (nonatomic) EMConversationType conversationType;
/*!
 @property
 @brief 上次清理时间
 */
@property (nonatomic, strong) NSString* lastClearTime;

/*!
 @property
 @brief 未读数量
 */
@property (nonatomic) int unReadNums;

/*!
 @property
 @brief  为了解决逛街动态的层级关系扩展
 */
@property (nonatomic ,copy) NSString* associate_id;


/*!
 @property
 @brief  为了解决逛街动态的层级关系扩展
 */
@property (nonatomic ,copy) NSString* lastMessageId;
///该会话是否包含未读的@信息
@property (nonatomic ,assign) BOOL isAt;

///用来判断是否是和客服的会话，11代表客服发给用户  ，10代表用户发给客服，
@property (nonatomic ,assign) NSInteger creatorRole;

/*!
 @property
 @brief 最后一条信息
 */
-(MessageModel*)latestMessage;


/*!
 @method
 @brief 把本对话里的所有消息标记为已读/未读(此方法调用后，需要从数据库重新获取数据)
 @param isRead 已读或未读
 @result 
 */
- (void)markAllMessagesAsRead;

/*!
 @method
 @brief 发送读取信息的回执
 @param isRead 已读
 @result
 */
-(void)sendAckReceipt:(NSArray*)array;
@end
