//
//  MessageModel.h
//  Moxian
//
//  聊天信息结构体
//  Created by litiankun on 14-10-11.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXChatDefines.h"


@class FMResultSet;
@class MallGoodsDetailModel;
@class CoinDetailModel;
@class MXChatVoice;
// 暂时用到，到时会删除掉
#define kMESSAGE_TYPE @"type"
#define kMESSAGE_FROM @"fromUserId"
#define kMESSAGE_TO @"toUserId"
#define kMESSAGE_CONTENT @"content"
#define kMESSAGE_DATE @"timeSend"
#define kMESSAGE_ID @"messageId"
#define kMESSAGE_No @"messageNo"
//#define kMESSAGE_TIMESEND @"timeSend"
#define kMESSAGE_TIMERECEIVE @"timeReceive"
#define kMESSAGE_FILEDATA @"fileData"
#define kMESSAGE_FILENAME @"fileName"
#define kMESSAGE_LOCATION_X @"location_x"
#define kMESSAGE_LOCATION_Y @"location_y"
#define kMESSAGE_TIMELEN @"timeLen"
#define kMESSAGE_ISSEND @"isSend"
#define kMESSAGE_ISREAD @"isRead"
#define kMESSAGE_FILESIZE @"fileSize"

@interface MessageModel : NSObject<NSCopying,NSMutableCopying>
/*
 {
 "body": "{\"attr1\":\"[神马]\"}",
 "from": "44",
 "msgCode": "4415458163221410.8898456141486553",
 "msgTime": 1545816323797,
 "msgType": 0,
 "offline": 0,
 "sessionType": "NORMAL",
 "to": "37",
 "user": "{\"realName\":\"用户111\",\"gender\":0,\"name\":\"用户111\",\"avatar\":\"http://pjkh5ako6.bkt.clouddn.com/logo.png\"}"
 }
 
 normal（0）    单聊消息
 groupchat（1）    群聊消息
 rich(2)    富消息（卡券类）
 destroy(3)    销毁消息
 sgroup（4）    群系统消息
 ss（5）    推送系统消息
 s（6）    单系统消息
 ping/pong（7）    心跳消息
 
 msgRecordTable
 */

// 发送者ID
@property (nonatomic, strong) NSString *fromID;

// 接收者ID(或群组ID)
@property (nonatomic, strong) NSString *toID;

// 消息类型 1-文本 2-图片 3-语音 4-位置 5-GIF  6文件 7视频 13语音通话
@property (nonatomic, assign) NSInteger subtype;

@property (nonatomic, assign) NSInteger type;

///creatorRole 消息类型是SEVICE时， 11代表客服发给用户  ，10代表用户发给客服，其它消息类型后台这边是不会处理这个字段的
@property (nonatomic, assign) NSInteger creatorRole;
// 0-单聊 1-群聊
@property (nonatomic, assign) EMConversationType chatType;

// 文本内容/表情
@property (nonatomic, copy) NSString *content;

//image **************************************************
// 图片的二进制
@property (nonatomic,strong) NSData* fileData;
//当信息失败时 暂时用作msgid容器
@property (nonatomic,strong) NSString*  imageWith;

@property (nonatomic,strong) NSString*  imageHeight;
// 文件大小
@property (nonatomic, assign) long long fileSize;

//image
@property (nonatomic) CGSize size;

@property (nonatomic) CGSize thumbnailSize;

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong) UIImage *thumbnailImage;

//语音长度
@property (nonatomic, strong) NSString *voiceLen;

// 文件名
@property (nonatomic, strong) NSString *fileName;
///缓存中的文件名
@property (nonatomic, strong) NSString *actualFileName;

// 文件下载地址(相对地址)
@property (nonatomic, strong) NSString *fileUrl;

// 本地文件路径 图片
@property (nonatomic, strong) NSString *locFileUrl;

// 文件类型
@property (nonatomic, assign) kMXFileType fileType;

// 文件具体类型 eg: doc docx
@property (nonatomic, copy) NSString *fileSubType;

// mask 图片
@property (nonatomic, strong) NSString *imageRemoteUrl;

// 纬度
@property (nonatomic, strong) NSString *lat;

// 经度
@property (nonatomic, strong) NSString *lng;

// 地址
@property (nonatomic, strong) NSString *addr;

// 详细地址
@property (nonatomic, strong) NSString *detailAddr;
///是否下载中  - 针对文件类型需要下载后再显示的
@property (nonatomic, assign) BOOL isDownloading;

//是否播放
@property (nonatomic) NSInteger isPlay;

//是否在播放中
@property (nonatomic) NSInteger isPlaying;

@property (nonatomic,strong) NSMutableDictionary*  dictionary;

// 扩展属性
@property (nonatomic,strong) NSDictionary* ext;

// 消息ID
@property (nonatomic, strong) NSString *messageId;

// 消息时间，Unix时间戳
@property (nonatomic, strong) NSString *sendTime;

///消息方向 1 发送 0 接收
@property (nonatomic) NSInteger msg_direction;

// 是否发送到服务器成功
@property (nonatomic) NSInteger is_acked;

//红包是否打开过
@property (nonatomic) NSInteger is_delivered;

//是否有读取过
@property (nonatomic, assign) NSInteger is_listened;

//是否是新消息 1 表示被读取过
@property (nonatomic, assign) NSInteger is_new;

@property (nonatomic, assign) kMXMessageState state;
//json body
@property (nonatomic, strong) NSString* body;

@property(nonatomic,strong,readonly) id attr1;
@property(nonatomic,strong,readonly) id attr2;
@property(nonatomic,strong,readonly) id attr3;
@property(nonatomic,strong,readonly) id attr4;

//属于和谁的聊天记录
@property (nonatomic,copy) NSString* chat_with;
//当前发送者头像
@property (nonatomic, copy) NSString* avatar;
//当前发送者名字
@property (nonatomic, copy) NSString* name;
//当前发送者备注
@property (nonatomic, copy) NSString* remark;
//当前发送者名字
@property (nonatomic, copy) NSString* smartName;
///是否选中
@property (nonatomic, assign) BOOL isSelected;
///返回文件在缓存中位置
@property (nonatomic, copy) NSString *fullPath;
///文件的青牛链接
@property (nonatomic, copy) NSString *urlString;
///是否PC端过来消息 - 考虑免打扰的问题
@property (nonatomic, assign) BOOL isPCMsg;
///该消息是否需要免打扰
@property (nonatomic, assign) BOOL inDisturb;

-(NSMutableDictionary*)toDictionary;

-(void)initWithDict:(NSDictionary*)dict;
//文本类型 messageType 单聊 群聊
//文本
- (id)initWithTextMessage:(NSString *)receiver text:(NSString*)text messageType:(EMConversationType)messageType;

///带有@文本消息  @所有人 atUseIdArr 为空 attr2 传@"0"
- (id)initWithAtTextMessage:(NSString *)receiver
                    atUseId:(NSArray *)atUseIdArr
                       text:(NSString*)text
                messageType:(EMConversationType)messageType;

//gif
- (id)initWithEmotionMessage:(NSString *)receiver text:(NSString* )image messageType:(EMConversationType)messageType package:(NSString*)package;
//图片
- (id)initWithImageMessage:(NSString *)receiver image:(UIImage* )image messageType:(EMConversationType)messageType;

- (id)initWithVideoMessage:(NSString *)receiver videoUrl:(NSURL* )videoUrl messageType:(EMConversationType)messageType;

- (id)initWithImageMessage:(NSString *)receiver path:(NSString*)path messageType:(EMConversationType)messageType;
//位置
- (id)initWithLocationMessage:(NSString *)receiver latitude:(double )latitude longitude:(double )longitude  address:(NSString *)address andSubAddress:(NSString *)subAddress key:(NSString *)key messageType:(EMConversationType)messageType;
//语音
- (id)initWithVoiceMessage:(NSString *)receiver voice:(MXChatVoice *)voice messageType:(EMConversationType)messageType;
//红包
- (id)initWithRedPackMessage:(NSString *)receiver text:(NSString* )text redpacketId:(NSString* ) redPacketId messageType:(EMConversationType)messageType;
//通讯录
- (id)initWithCardMessage:(NSString *)receiver user:(NSDictionary* )userDict messageType:(EMConversationType)messageType;

///发送商品链接
- (id)initWithProductLinkMessage:(NSString *)receiver user:(NSDictionary* )userDict messageType:(EMConversationType)messageType;

- (id)initWithSgroupMessage:(NSString *)tip user:(NSString* )chatter messageType:(SGROUPType)messageType;
///语音通话相关
- (id)initWithVoiceCallMessage:(NSString *)receiver text:(NSString*)text messageType:(EMConversationType)messageType subMessageType:(kMXVoiceChatType)subMessageType;

///文件
- (id)initWithFileMessage:(NSString *)receiver
                 filetype:(NSString*)fileType
                     size:(long long)size
                 fileName:(NSString*)fileName
              messageType:(EMConversationType)messageType;

- (id)initWithFileMessage:(MessageModel *)msg
                 receiver:(NSString *)receiver
              messageType:(EMConversationType)messageType;

///电脑端登录相关消息
- (id)initWithClientMessageSubType:(kMXClientType)subType;

//组装body信息
-(NSString*)makeBody;
//转化model
+(MessageModel*)initWithFMResultSet:(FMResultSet*)set;

-(NSString* )jsonString;

-(NSString* )receiptString;

+(int)stringToSessionType:(NSString *)type;

+(NSString* )intToSession:(NSInteger )type;

+(EMConversationType)getTypeByChatType:(NSInteger )type;


- (id)initWithAddFriendMessage:(NSString *)tip user:(NSString* )chatter messageType:(SGROUPType)messageType;

+ (kMXFileType)fileType:(NSString *)type;
///查看该message是否存在本地缓存
- (BOOL)messageFileExists;
///撤回消息的显示内容
- (NSString *)formatWithdrawContentTxt;
///是否超出撤回的时间限制 5分钟
- (BOOL)isOutOfWithdrawTimeLimit;

@end
