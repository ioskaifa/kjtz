//
//  MXChatVoice.h
//  MoPal_Developer
//
//  声音操作model
//  Created by aken on 15/3/24.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface MXChatVoice : NSObject
/*!
 @property
 @brief 文件对象的显示名
 */
@property (nonatomic, strong) NSString *displayName;

/*!
 @property
 @brief 文件对象本地磁盘位置的全路径
 */
@property (nonatomic, strong) NSString *localPath;

/*!
 @property
 @brief 文件对象所对应的文件的大小, 以字节为单位
 */
@property (nonatomic) long long fileLength;

/*!
 @property
 @brief 语音的时长, 单位是秒
 */
@property (nonatomic) NSInteger duration;


/*!
 @method
 @brief 以NSData构造语音对象
 @discussion
 @param aData 语音内容
 @param aDisplayName 语音对象的显示名
 @result 语音对象
 */
//- (id)initWithData:(NSData *)aData displayName:(NSString *)aDisplayName;

/*!
 @method
 @brief 以文件路径构造文件对象
 @discussion
 @param filePath 磁盘文件全路径
 @param aDisplayName 文件对象的显示名
 @result 文件对象
 */
//- (id)initWithFile:(NSString *)filePath displayName:(NSString *)aDisplayName;
@end
