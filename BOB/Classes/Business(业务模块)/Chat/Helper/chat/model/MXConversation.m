//
//  MXConversation.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/7/27.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "MXConversation.h"
#import "MXChatDBUtil.h"
#import "LcwlChat.h"
@implementation MXConversation

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.messages = [[NSMutableArray alloc]initWithCapacity:0];

    }
    return self;
}

-(MessageModel*)latestMessage{
    return [self.messages lastObject];
}

- (void)markAllMessagesAsRead{
    NSString* chatId = self.chat_id;
    //标记所有都为老信息
    BOOL result = [[MXChatDBUtil sharedDataBase]readMessageByChatID:chatId];
    if (result) {
        [[LcwlChat shareInstance].chatManager clearUnReadNums:self.chat_id];
        NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithCapacity:0];
        NSInteger nums = [[LcwlChat shareInstance].chatManager getAllUnReadNums];
        [userInfo setValue:[NSNumber numberWithInteger:nums] forKey:@"nums"];
        [userInfo setValue:[NSNumber numberWithInteger:3] forKey:@"tabBarIndex"];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"redBadgeCountChanged" object:nil userInfo:userInfo];
    }
}

-(void)sendAckReceipt:(NSArray*)array{
    NSMutableArray* tmpArray = [array copy];
    [tmpArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[MessageModel class]]) {
            MessageModel* msg = (MessageModel*)obj;
            if (msg.type == MessageTypeNormal ||
                msg.type == MessageTypeGroupchat) {
                if (!msg.msg_direction && msg.is_acked == 0) {
                    [[LcwlChat shareInstance].chatManager sendReceipt:msg];
                }
            }
        }
    }];
}

- (id)copyWithZone:(NSZone *)zone
{
    MXConversation *copy = [[[self class] allocWithZone:zone]init];
    copy.chat_id =  [self.chat_id copy];
    copy.messages =  [self.messages copy];
    copy.conversationType =  self.conversationType;
    copy.lastClearTime =  [self.lastClearTime copy];
    copy.unReadNums =  self.unReadNums;
    copy.isAt =  self.isAt;
    copy.associate_id =  [self.associate_id copy];
    copy.lastMessageId =  [self.lastMessageId copy];
    return copy;
    
}
- (id)mutableCopyWithZone:(NSZone *)zone
{
    MXConversation *copy = [[[self class] allocWithZone:zone]init];
    copy.chat_id =  [self.chat_id copy];
    copy.messages =  [self.messages copy];
    copy.conversationType =  self.conversationType;
    copy.lastClearTime =  [self.lastClearTime copy];
    copy.unReadNums =  self.unReadNums;
    copy.isAt =  self.isAt;
    copy.associate_id =  [self.associate_id copy];
    copy.lastMessageId =  [self.lastMessageId copy];
    return copy;
}
@end
