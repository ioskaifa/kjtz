//
//  MXChat.h
//  Moxian
//
//  魔聊客户端的基本类
//  Created by 王 刚 on 14/12/2.
//  Copyright (c) 2016年 Moxian. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
@class MoYouModel;
@class MessageModel;
@class MXConversation;
@class ChatGroupModel;
@class NewFriendModel;
@class FriendModel;
@interface MXChatDBUtil : NSObject
{
    FMDatabaseQueue * fmdb;//操作数据库指针
}
-(id)init;

+(MXChatDBUtil*)sharedDataBase;

//初始化所有普通表
-(void)initChatConfig:(NSString*)name;
//抓取数据库中的所有表
-(NSMutableArray*)loadAllTablesFromDB;
//创建聊天表
-(BOOL)createChatTB:(NSString*)dbName;
//创建未发送信息表
-(BOOL)createUnSendChatDB;

/*************************************************会话begin************************************************************/
// 创建会话
-(BOOL)createConversation:(MXConversation*)conversation;

// 创建店铺的conversation
-(BOOL)createShopConversation:(MXConversation*)conversation;

//从数据库中抓取聊天列表
-(NSMutableArray*)loadConversationsFromDB;

//从数据库中抓取聊天列表
-(NSMutableArray*)loadShopConversationsFromDB;

//删除普通的conversation
-(BOOL)deleteConversation:(NSString*) chatId;

//删除店铺的conversation
-(BOOL)deleteShopConversation:(NSString*) chatId;

//修改未读数量
-(BOOL)updateUnReadNums:(int)unReadNums atStatus:(BOOL)atStatus chatId:(NSString*)chatId;

//修改最后一条信息
-(BOOL)updateLastMessage:(NSString*) chatId associateId:(NSString*)associateId messageId:(NSString* )messageId;

////修改消息免打扰
-(BOOL)updateEnableDisturb:(int)enableDisturb chatId:(NSString*)chatId;
/*************************************************会话end************************************************************/

-(NSMutableArray*)loadUnSendMessagesFromDB;
//修改删除消息后更新conversation
-(BOOL)updateClearTime:(NSString*)chatId time:(NSString*)time msgID:(NSString* )messageId;

//插入单条信息
-(BOOL)insertMessage:(MessageModel*)item ;

//更新视频消息数据
-(BOOL)updateVideoUrlMessage:(MessageModel *)msg;


//取出之前的聊天数据
-(NSMutableArray*)selectBeforeChatRecords:(NSString* )chatId time:(NSString* )time;
 
//取出用户全部的聊天数据
- (NSArray*)selectMsgByUserId:(NSString* )imid;

//删除某个人的所有信息
-(BOOL)clearMessages:(NSString*)chatId ;

//删除某个人的发送信息
-(BOOL)deleteSendMsgByImid:(NSString*)imid;

//删除某条信息
-(BOOL)deleteMsgByMessageId:(NSString*)msgId chatId:(NSString*)chatId;

//设置所有信息为已读
-(BOOL)readMessageByChatID:(NSString*)imid;

//统计未读数量
- (NSInteger)getUnReadNumByImid:(NSString*)imid;

///消息撤回
-(BOOL)updateWithdrawRowDataByMsgModel:(MessageModel*)model;

//修改消息的状态
- (BOOL)updateMessageState:(MessageModel*) msg;

-(BOOL)updateRowDataByMsgModel:(MessageModel*)model;

-(BOOL)updateMessagePlayState:(MessageModel*)msg;

- (BOOL)updateMessageBody:(MessageModel*) msg;

//修改信息到达服务器状态
-(BOOL)updateMsgAck:(MessageModel*) msg;
//修改信息被对方读取
-(BOOL)updateOtherReadState:(MessageModel*) msg;
//修改信息发送失败
- (BOOL)updateMessageFailureState:(MessageModel*) msg;
//查询某一条聊天记录
-(MessageModel* )getMessageByID:(NSString* )msgID chatId:(NSString*)chatId;

//查询最后一条信息
- (NSArray*)lastMessage:(NSString* )chatwith messageId:(NSString* )messageId;

-(BOOL)insertUnSendMsgToDB:(MessageModel *)msg;

-(BOOL)deleteUnSendMsgByMessageId:(NSString*)msgId ;

-(NSMutableArray*)selectAllChatHistory:(NSString* )chatId ;
/*************************************************好友begin************************************************************/
//好友表
-(BOOL)insertFriendToDB:(MoYouModel *)moyou isExist:(BOOL)isExist;

-(BOOL)updateFriendIsTop:(MoYouModel*) moyou;

- (BOOL)deleteFriendByID:(NSString*)chatID;
// 设置某个用户是否置顶
//- (BOOL)setFriendIsTop:(BOOL)isTop chatId:(NSString*)chatID;

-(NSMutableArray*)loadFriendFromDatabase;
//加载新朋友
-(NSMutableArray*)loadNewFriendFromDatabase;
//把原好友的关注改为0
- (BOOL)clearFriend:(NSString*)chatID ;
//修改置顶
-(BOOL)updateMoYouToDB:(FriendModel*) moyou;

- (int)getFriendTopNums;

-(NSArray*)selectFriendByKeyword:(NSString*)keyword;
//查找好友
-(FriendModel*)selectFriendByChatId:(NSString*)chatId;
//批量组装好友插入数据
- (NSString *)getFriendHandleSqlDB:(FriendModel *)moyou isExist:(BOOL)isExist;
//批量插入好友
-(BOOL)executeMoYouSql:(NSArray*)sqlArray model:(NSArray*) friends;

- (BOOL)clearFriendRecords;
/*************************************************好友end************************************************************/

/*************************************************群组begin************************************************************/

//批量组装群组插入数据
-(NSString*)getGroupHandleSqlDB:(ChatGroupModel*)group isExist:(BOOL)isExist;
//批量插入群组数据
-(BOOL)executeGroupSql:(NSArray*)sqlArray model:(NSArray*) groups;
//清空群组
-(BOOL)clearGroup;
//插入群组
-(BOOL)insertGroup:(ChatGroupModel*)group;
//修改群组
-(BOOL)updateGroup:(ChatGroupModel*)group ;
//抓取群组
-(NSMutableArray*)loadChatGroupFromDatabase;
//删除群组
-(BOOL)removeChatGroup:(ChatGroupModel*)group;
//修改群组昵称
-(BOOL)updateGroupNickname:(NSString*) nickName groupId:(NSString*)groupId;

//插入群成员
-(BOOL)insertGroupMemeber:(FriendModel*) member;
//查询群成员
-(FriendModel*)selectGroupMember:(NSString*) roomId memberId:(NSString*)memberId;
//删除群成员
-(BOOL)removeGroupMember:(FriendModel*) member;
//修改群组置顶
-(BOOL)updateGroupTopState:(ChatGroupModel*)group;
//修改群组免打扰
-(BOOL)updateGroupDisturbState:(ChatGroupModel*)group;
//修改保存到通讯录
-(BOOL)updateSaveToContact:(ChatGroupModel*)group;
//删除全部群成员
-(BOOL)clearGroupMembers:(NSString*) groupId;
//查询群组成员
- (NSMutableArray*)loadGroupMembers:(NSString*) roomId ;

//查询整个群成员
-(NSMutableArray*)loadGroupMembersFromDatabase:(NSString*) roomId;
/*************************************************群组end************************************************************/

-(BOOL)updateFriendRecords:(FriendModel* )model;

///更新个人消息免打扰设置
-(BOOL)updateFriendDisturbState:(FriendModel*)model;

////查询是否打开消息免打扰
-(BOOL)checkEnableDisturb:(MessageModel *)message;

///更新群人数
-(BOOL)updateGroupMemNum:(ChatGroupModel*)group;

/*管理文件消息相关*/
///更新文件消息数据
-(BOOL)updateFileMessage:(MessageModel *)msg;
///查看本地所有文件
-(NSMutableArray*)selectAllFileChatHistory;

@end
