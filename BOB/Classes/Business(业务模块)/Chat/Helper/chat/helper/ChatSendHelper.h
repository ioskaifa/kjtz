//
//  ChatSendHelper.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/7/16.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "MXChatDefines.h"
#import "IMXChatManagerChat.h"
#import "MXChatVoice.h"
#import "MXDocument.h"

@interface ChatSendHelper : NSObject
/**
 *  发送文字消息
 *
 *  @param str               发送的文字
 *  @return 封装的消息体
 */
//+(MessageModel *)sendTextMessageWithString:(NSString *)str;
//+(MessageModel *)sendTextMessage:(NSString *)str toUsername:(NSString*)chatter messageType:(EMConversationType)type;

///new text type
+(MessageModel *)sendTextMessage:(NSString *)text toUsername:(NSString*)chatter messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole;

///带有@功能的文本消息
+(MessageModel *)sendAtTextMessage:(NSString *)text toUsername:(NSString*)chatter messageType:(EMConversationType)type atUserId:(NSArray *)arr creatorRole:(NSInteger)creatorRole;
/**
 *  发送表情
 *
 *  @param str               发送的文字
 *  @return 封装的消息体
 */
//+(MessageModel *)sendFaceEmotionMessageWithString:(NSString *)str;
+(MessageModel *)sendFaceEmotionMessage:(NSString *)text package:(NSString*)package toUsername:(NSString*)chatter messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole;

+(MessageModel *)sendVideoMessage:(NSURL *)videoUrl toUsername:(NSString*)chatter messageType:(EMConversationType)type delegate:(id<SendMessageDelegate>)sender creatorRole:(NSInteger)creatorRole;

/**
 *  发送图片消息
 *
 *  @param image             发送的图片
 *  @return 封装的消息体
 */
//+(MessageModel *)sendImageMessageWithImage:(UIImage *)image;
+(MessageModel *)sendImageMessage:(UIImage *)image toUsername:(NSString*)chatter messageType:(EMConversationType)type delegate:(id<SendMessageDelegate>)sender creatorRole:(NSInteger)creatorRole;

+(MessageModel *)sendImageMessageWithPath:(NSString *)path toUsername:(NSString*)chatter messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole;
/**
 *  发送位置消息（定位）
 *
 *  @return 封装的消息体
 */
//+(MessageModel *)sendLocationLatitude:(double)latitude
//                         longitude:(double)longitude
//                              address:(NSString *)address;
+(MessageModel *)sendLocationMessageToUsername:(NSString*)chatter  latitude:(double )latitude longitude:(double )longitude  address:(NSString *)address andSubAddress:(NSString *)subAddress key:(NSString *)key messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole;
/**
 *  发送视频文件消息
 *
 *  @param
 *  @return 封装的消息体
 */
//+(MessageModel *)sendVideo:(MXChatVoice *)video;
+(MessageModel *)sendVoiceMessage:(MXChatVoice*)voice toUsername:(NSString*)chatter messageType:(EMConversationType)type delegate:(id<SendMessageDelegate>) delegate creatorRole:(NSInteger)creatorRole;

/**
 *  重新发送消息
 *
 *  @param
 *  @return 封装的消息体
 */
+(void)reSendMessage:(MessageModel*)model delegate:(id<SendMessageDelegate>) delegate;

/**
 *  发送个人通讯录消息
 *
 *  @param
 *  @return 封装的消息体
 */
+(MessageModel *)sendCardMessage:(NSDictionary *)dict toUsername:(NSString*)chatter messageType:(EMConversationType)type;

+(MessageModel *)sendProductLinkMessage:(NSDictionary *)dict toUsername:(NSString*)chatter messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole;

/**
 *  发送红包消息
 *
 *  @param
 *  @return 封装的消息体
 */
+(MessageModel *)sendRedPacketMessage:(NSString *)text toUsername:(NSString*)chatter redpacketId:(NSString* ) redPacketId messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole;



+(NSString*)chatSendTime;



/**
 *  发送群系统消息
 *
 *  @param
 *  @return 封装的消息体
 */
+(MessageModel *)sendSgroup:(NSString *)tip from:(NSString*)chatter messageType:(SGROUPType)type;

+(MessageModel *)sendChat:(NSString *)tip from:(NSString*)chatter messageType:(kMXMessageType)type type:(EMConversationType)type;

/**
 *  发送语音通话
 *
 *  @param content  发送的文字
 *  @return 封装的消息体
 */
+(MessageModel *)sendVoiceCallMessage:(NSString *)content toUsername:(NSString*)chatter messageType:(EMConversationType)type subMessageType:(kMXVoiceChatType)subType;

+(MessageModel *)sendFileMessagetoUsername:(NSString*)chatter
                               messageType:(EMConversationType)type
                                   docFile:(MXDocument *)doc
                                  delegate:(id<SendMessageDelegate>)sender creatorRole:(NSInteger)creatorRole;

+(MessageModel *)sendFileMessageByMessage:(MessageModel *)msg
                                 receiver:(NSString *)receiver
                              messageType:(EMConversationType)messageType
                                 delegate:(id<SendMessageDelegate>)sender creatorRole:(NSInteger)creatorRole;
///发送客户端登录相关消息
+(MessageModel *)sendClientMessageBySubType:(kMXClientType)subType;

@end
