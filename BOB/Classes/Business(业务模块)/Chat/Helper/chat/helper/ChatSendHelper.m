//
//  ChatSendHelper.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/7/16.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "ChatSendHelper.h"
#import "TalkManager.h"
#import "LcwlChat.h"
#import "TimeFormat.h"
#import "MXChatMessageHandler.h"
#import "SRWebSocketManager.h"

@implementation ChatSendHelper

+(NSString*)chatSendTime{
    NSString* chatTimeDiff = [MXCache valueForKey:ChatServerTime];
    if ([StringUtil isEmpty:chatTimeDiff]) {
        NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
        NSString* msgTime =  [NSString stringWithFormat:@"%f",time];
        msgTime = [msgTime stringByReplacingOccurrencesOfString:@"." withString:@""];
        NSRange newRange;
        newRange.location = 0;
        newRange.length = msgTime.length<13?msgTime.length:13;
        msgTime = [msgTime substringWithRange:newRange];
        return msgTime;
    }else{
        double localTime = [[MXCache valueForKey:ChatLocalTime] doubleValue];
        double nowTime = [[NSDate date] timeIntervalSince1970]*1000;
        double diffTime = nowTime-localTime;
        double serverTime = [[MXCache valueForKey:ChatServerTime] doubleValue];
        NSString* msgTime =  [NSString stringWithFormat:@"%f",serverTime+diffTime];
        NSRange range = [msgTime rangeOfString:@"."];
        if (range.location != NSNotFound) {
            msgTime = [msgTime substringToIndex:range.location];
        }
        NSRange newRange;
        newRange.location = 0;
        newRange.length = msgTime.length<13?msgTime.length:13;
        msgTime = [msgTime substringWithRange:newRange];
        return msgTime;
    }
}

+(NSString*)msgShopid:(NSString*)toUser{
    NSArray* toUserArray = (NSArray*)[toUser componentsSeparatedByString:@"_"];
    if (toUserArray.count == 3) {
        return [NSString stringWithFormat:@"%@",toUserArray[2]];
    }else{
        return nil;
    }
}

//new text type
//+(MessageModel *)sendTextMessage:(NSString *)text toUsername:(NSString*)chatter messageType:(EMConversationType)type {
//    MessageModel* msg = [[MessageModel alloc]initWithTextMessage:chatter text:text messageType:type];
//    [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
//    return msg;
//}

//new text type
+(MessageModel *)sendTextMessage:(NSString *)text toUsername:(NSString*)chatter messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole {
    MessageModel* msg = [[MessageModel alloc]initWithTextMessage:chatter text:text messageType:type];
    msg.creatorRole = creatorRole;
    [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
    return msg;
}

///带有@ text message
+(MessageModel *)sendAtTextMessage:(NSString *)text toUsername:(NSString*)chatter messageType:(EMConversationType)type atUserId:(NSArray *)arr creatorRole:(NSInteger)creatorRole {
    MessageModel *msg = [[MessageModel alloc] initWithAtTextMessage:chatter atUseId:arr text:text messageType:type];
    msg.creatorRole = creatorRole;
    [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
    return msg;
}

// new emotion type
+(MessageModel *)sendFaceEmotionMessage:(NSString *)text package:(NSString*)package toUsername:(NSString*)chatter messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole {
    MessageModel* msg = [[MessageModel alloc]initWithEmotionMessage:chatter text:text messageType:type package:package];
    msg.creatorRole = creatorRole;
    [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
    return msg;
}

+(MessageModel *)sendVideoMessage:(NSURL *)videoUrl toUsername:(NSString*)chatter messageType:(EMConversationType)type delegate:(id<SendMessageDelegate>)sender creatorRole:(NSInteger)creatorRole {
    MessageModel* msg = [[MessageModel alloc]initWithVideoMessage:chatter videoUrl:videoUrl messageType:type];
    msg.creatorRole = creatorRole;
    [[LcwlChat shareInstance].chatManager insertMessage:msg];
    [[MXChatMessageHandler sharedInstance] addMessage:msg];
    [[LcwlChat shareInstance].chatManager sendVideoMessage:msg delegate:sender];
    return msg;
}

// new image type
+(MessageModel *)sendImageMessage:(UIImage *)image toUsername:(NSString*)chatter messageType:(EMConversationType)type delegate:(id<SendMessageDelegate>)sender creatorRole:(NSInteger)creatorRole {
    MessageModel* msg = [[MessageModel alloc]initWithImageMessage:chatter image:image messageType:type];
    msg.creatorRole = creatorRole;
    [[LcwlChat shareInstance].chatManager insertMessage:msg];
    [[MXChatMessageHandler sharedInstance] addMessage:msg];
    [[LcwlChat shareInstance].chatManager sendImageMessage:msg delegate:sender];
    return msg;
}

+(MessageModel *)sendFileMessagetoUsername:(NSString*)chatter
                               messageType:(EMConversationType)type
                                   docFile:(MXDocument *)doc
                                  delegate:(id<SendMessageDelegate>)sender creatorRole:(NSInteger)creatorRole{
    MessageModel* msg = [[MessageModel alloc] initWithFileMessage:chatter
                                                         filetype:doc.fileTypeEx
                                                             size:doc.data.length
                                                         fileName:doc.localizedName
                                                      messageType:type];
    msg.creatorRole = creatorRole;
    msg.locFileUrl = doc.locFileUrl;
    [[LcwlChat shareInstance].chatManager insertMessage:msg];
    [[MXChatMessageHandler sharedInstance] addMessage:msg];
    [[LcwlChat shareInstance].chatManager sendFileMessage:msg delegate:sender];
    return msg;
}

+(MessageModel *)sendFileMessageByMessage:(MessageModel *)msg
                                 receiver:(NSString *)receiver
                              messageType:(EMConversationType)messageType
                                 delegate:(id<SendMessageDelegate>)sender creatorRole:(NSInteger)creatorRole {
    MessageModel* message = [[MessageModel alloc] initWithFileMessage:msg receiver:receiver messageType:messageType];
    message.creatorRole = creatorRole;
    [[LcwlChat shareInstance].chatManager insertMessage:message];
    [[MXChatMessageHandler sharedInstance] addMessage:message];
    [[LcwlChat shareInstance].chatManager sendFileMessage:message delegate:sender];
    return message;
}

+(MessageModel *)sendImageMessageWithPath:(NSString *)path toUsername:(NSString*)chatter messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole {
    MessageModel* msg = [[MessageModel alloc]initWithImageMessage:chatter path:path messageType:type];
    return msg;
}

// new location type
+(MessageModel *)sendLocationMessageToUsername:(NSString*)chatter  latitude:(double )latitude longitude:(double )longitude  address:(NSString *)address andSubAddress:(NSString *)subAddress key:(NSString *)key messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole {
    MessageModel* msg = [[MessageModel alloc]initWithLocationMessage:chatter latitude:latitude longitude:longitude  address:address andSubAddress:subAddress key:key messageType:type];
    msg.creatorRole = creatorRole;
    [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
    return msg;
}

// new voice type
+(MessageModel *)sendVoiceMessage:(MXChatVoice*)voice toUsername:(NSString*)chatter messageType:(EMConversationType)type delegate:(id<SendMessageDelegate>) delegate creatorRole:(NSInteger)creatorRole {
    MessageModel* msg = [[MessageModel alloc]initWithVoiceMessage:chatter voice:voice messageType:type];
    msg.creatorRole = creatorRole;
    if ([StringUtil isEmpty:msg.locFileUrl]) {
        return nil;
    }
    [[LcwlChat shareInstance].chatManager insertMessage:msg];
    [[MXChatMessageHandler sharedInstance] addMessage:msg];
    [[LcwlChat shareInstance].chatManager sendVoiceMessage:msg delegate:delegate];
    return msg;
}

+(MessageModel *)sendRedPacketMessage:(NSString *)text toUsername:(NSString*)chatter redpacketId:(NSString* ) redPacketId messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole {
    MessageModel* message = [[MessageModel alloc]initWithRedPackMessage:chatter text:text redpacketId: redPacketId messageType:type];
    message.creatorRole = creatorRole;
    SRWebSocketManager * manager = [SRWebSocketManager sharedInstance];
    message.state = kMXMessageStateSuccess;
    [[LcwlChat shareInstance].chatManager insertMessage:message];
    
    if (manager.delegate && [manager.delegate respondsToSelector:@selector(didReceiveMessage:)]) {
        [manager.delegate didReceiveMessage:message];
    }
    return message;
}

+(MessageModel *)sendCardMessage:(NSDictionary *)dict toUsername:(NSString*)chatter messageType:(EMConversationType)type {
    MessageModel* msg = [[MessageModel alloc]initWithCardMessage:chatter user:dict messageType:type];
    [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
    return msg;
}

+(MessageModel *)sendProductLinkMessage:(NSDictionary *)dict toUsername:(NSString*)chatter messageType:(EMConversationType)type creatorRole:(NSInteger)creatorRole {
    MessageModel* msg = [[MessageModel alloc]initWithProductLinkMessage:chatter user:dict messageType:type];
    msg.creatorRole = creatorRole;
    [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
    return msg;
}

+(void)reSendMessage:(MessageModel*)message delegate:(id<SendMessageDelegate>) delegate {
    message.msg_direction = 1;
    if (message.type == MessageTypeNormal || message.type == MessageTypeGroupchat ) {
        if (message.subtype == kMXMessageTypeImage) {
            message.state=kMXMessageStateSending;
            [[LcwlChat shareInstance].chatManager sendImageMessage:message delegate:delegate];
        }else if(message.subtype == kMXMessageTypeVoice){
            message.state = kMXMessageStateSending;
            [[LcwlChat shareInstance].chatManager sendVoiceMessage:message delegate:delegate];
        }else if(message.subtype == kMXMessageTypeFile){
            message.state = kMXMessageStateSending;
            [[LcwlChat shareInstance].chatManager sendFileMessage:message delegate:delegate];
        }else{
            [[LcwlChat shareInstance].chatManager sendMessage:message completion:nil];
        }
    }
}

+(MessageModel *)sendSgroup:(NSString *)tip from:(NSString*)chatter messageType:(SGROUPType)type {
    MessageModel* message = [[MessageModel alloc]initWithSgroupMessage:tip user:chatter messageType:type];
    [[LcwlChat shareInstance].chatManager insertMessage:message];
    
    SRWebSocketManager * manager = [SRWebSocketManager sharedInstance];
    if (manager.delegate && [manager.delegate respondsToSelector:@selector(didReceiveMessage:)]) {
        [manager.delegate didReceiveMessage:message];
    }
    return message;
}

+(MessageModel *)sendChat:(NSString *)tip from:(NSString*)chatter messageType:(kMXMessageType)type type:(EMConversationType)con_type {
    SRWebSocketManager * manager = [SRWebSocketManager sharedInstance];
    MessageModel* message = [[MessageModel alloc]initWithSgroupMessage:tip user:chatter messageType:type];
    message.type = MessageTypeS;
    message.chatType = con_type;
    
    message.state = kMXMessageStateSuccess;
    [[LcwlChat shareInstance].chatManager insertMessage:message];
    
    if (manager.delegate && [manager.delegate respondsToSelector:@selector(didReceiveMessage:)]) {
        [manager.delegate didReceiveMessage:message];
    }
    return message;
}

+(MessageModel *)sendVoiceCallMessage:(NSString *)content toUsername:(NSString*)chatter messageType:(EMConversationType)type subMessageType:(kMXVoiceChatType)subType {
    MessageModel* msg = [[MessageModel alloc] initWithVoiceCallMessage:chatter text:content messageType:type subMessageType:subType];
    [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
    return msg;
}

+(MessageModel *)sendClientMessageBySubType:(kMXClientType)subType {
    MessageModel *msg = [[MessageModel alloc] initWithClientMessageSubType:subType];
    [[LcwlChat shareInstance].chatManager sendMessage:msg completion:nil];
    return msg;
}

@end
