#import "MXChatDBUtil.h"
#import "StringUtil.h"
#import "UserModel.h"
#import "MXJsonParser.h"
#import "DocumentManager.h"
#import "FriendModel.h"
#import "MXConversation.h"
#import "ChatGroupModel.h"
#define fetch_pgae_size 10
static NSString* dbName = @"moxian_chat.db";
static NSString* friendTBName = @"friendTb";
static NSString* newFriendTBName = nf;
static NSString* conversationTBName = @"conversationTbs";
static NSString* groupTBName = @"groupTb";
static NSString* memTBName = @"memberTb";
static NSString* dbAllTBs = @"SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;";
static NSString* sendingTBName = @"pre_send_msg";
static NSString *chatFileTB = @"chatFileTB";

@implementation MXChatDBUtil

+ (MXChatDBUtil*)sharedDataBase {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}
-(id)init
{
    if (self=[super init]) {
    }
    return self;
}
#pragma -mark 数据库配置
/*************************************************数据库配置begin************************************************************/

-(void)initChatConfig:(NSString*)name{
    NSString* path = [self.class createChatDirByName:name];
    if ([StringUtil isEmpty:path]) {
        return;
    }
    fmdb = [FMDatabaseQueue databaseQueueWithPath:path];
    [self initChatTB];
}

+(NSString*)createChatDirByName:(NSString*) name {
    NSString * path = [[DocumentManager chatCacheDocDirectory:name]stringByAppendingPathComponent:dbName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:path])
    {
        Boolean b =  [fileManager createFileAtPath:path contents:nil attributes:nil];
        if (b) {
            return path;
        }
    }else{
        return path;
    }
    return @"";
}

-(void)initChatTB{
    //好友记录表
    [self createFriendTB];
    //外层聊天记录表
    [self createConversationTB];
//    //群组表
    [self createGroupTB];
//    //组员表
    [self createGroupMember];
    
    //等待发送的表
    [self createUnSendChatDB];
    //保存文件的表 方便文件查找
    [self createFileDB];
}

-(BOOL)createFileDB{
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * createSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(msg_code TEXT(80) PRIMARY KEY,msg_time TEXT(80) DEFAULT NULL,msg_direction integer,is_acked integer ,is_delivered integer,msg_status integer,chat_with TEXT(100),is_listened integer,msg_body TEXT(4096), local_file_url Text(100) DEFAULT NULL, file_url Text(100) DEFAULT NULL,to_userId TEXT(100), is_new integer, is_play integer,avatar TEXT(80) DEFAULT NULL , name TEXT(80) DEFAULT NULL ,isGroup integer,type integer,subtype integer,fromID VARCHAR,toID VARCHAR,ext1 VARCHAR);",chatFileTB];
        BOOL res = [db executeUpdate:createSql];
        if (!res) {
        }
    }];
    return FALSE;
}

//加载数据库里所有的数据
-(NSMutableArray*)loadAllTablesFromDB{
    NSString * selectSql = [NSString stringWithFormat:@"SELECT name FROM sqlite_master WHERE type='table' and name like '%%%@%%' ORDER BY name;",tbPrefix];
    NSMutableArray *resultArray = [NSMutableArray array];
    [fmdb inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]){
            NSString* tbName = [rs stringForColumn:@"name"];
            [resultArray safeAddObj:tbName];
        }
        [rs close];
    }];
    return resultArray;
}

/*************************************************数据库配置end************************************************************/

#pragma -mark 消息
/*************************************************消息begin************************************************************/

//创建待发送的信息表
-(BOOL)createUnSendChatDB{
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * createSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(msg_code TEXT(80) PRIMARY KEY,msg_time TEXT(80) DEFAULT NULL,msg_direction integer,is_acked integer ,is_delivered integer,msg_status integer,chat_with TEXT(100),is_listened integer,msg_body TEXT(4096), local_file_url Text(100) DEFAULT NULL, file_url Text(100) DEFAULT NULL,to_userId TEXT(100), is_new integer, is_play integer,avatar TEXT(80) DEFAULT NULL , name TEXT(80) DEFAULT NULL ,isGroup integer,type integer,subtype integer,fromID VARCHAR,toID VARCHAR,ext1 VARCHAR);",sendingTBName];
        BOOL res = [db executeUpdate:createSql];
        if (!res) {
        }
    }];
    return FALSE;
}

//创建魔聊的表
-(BOOL)createChatTB:(NSString*)dbName{
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * createSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(msg_code TEXT(80) PRIMARY KEY,msg_time TEXT(80) DEFAULT NULL,msg_direction integer,is_acked integer ,is_delivered integer,msg_status integer,chat_with TEXT(100),is_listened integer,msg_body TEXT(4096), local_file_url Text(100) DEFAULT NULL, file_url Text(100) DEFAULT NULL,to_userId TEXT(100), is_new integer, is_play integer,avatar TEXT(80) DEFAULT NULL , name TEXT(80) DEFAULT NULL ,isGroup integer,type integer,subtype integer,fromID VARCHAR,toID VARCHAR,ext1 VARCHAR,creatorRole integer DEFAULT 0);",dbName];
        BOOL res = [db executeUpdate:createSql];
        if (!res) {
        }
    }];
    return FALSE;
}

//插入数据进入聊天表
-(BOOL)insertMessage:(MessageModel *)msg
{
    if (msg.subtype == kMXMessageTypeFile) {
        [self insertFileMessage:msg];
    }
    __block BOOL isSuccess = false;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,msg.chat_with];
        NSString * insertSql=[NSString stringWithFormat:@"insert into %@(msg_code,msg_time,msg_direction,is_acked,is_delivered,msg_status,chat_with,is_listened,msg_body,local_file_url,file_url,is_new,is_play,avatar,name,isGroup,type ,subtype,fromID,toID,creatorRole) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",tbName];
        NSString* bodyString = msg.body;
        NSString* chat_with = msg.chat_with;
        kMXMessageState state = msg.state;
        isSuccess = [db executeUpdate:insertSql,msg.messageId,
                     msg.sendTime,
                     [NSNumber numberWithInteger:msg.msg_direction],
                     [NSNumber numberWithInteger:msg.is_acked],
                     [NSNumber numberWithInteger:msg.is_delivered],
                     [NSNumber numberWithInteger:state],
                     chat_with,
                     [NSNumber numberWithInteger:msg.is_listened],
                     bodyString,
                     msg.locFileUrl,
                     msg.fileUrl,
                     [NSNumber numberWithInteger:msg.is_new],
                     [NSNumber numberWithInteger:msg.isPlay],
                     msg.avatar,
                     msg.name,
                     [NSNumber numberWithInteger:msg.chatType],
                     [NSNumber numberWithInteger:msg.type],
                     [NSNumber numberWithInteger:msg.subtype],msg.fromID,msg.toID,[NSNumber numberWithInteger:msg.creatorRole]];
    }];
    return isSuccess;
}

#pragma mark - 插入文件消息
-(BOOL)insertFileMessage:(MessageModel *)msg {
    if (msg.subtype != kMXMessageTypeFile) {
        return NO;
    }
    __block BOOL isSuccess = false;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * insertSql=[NSString stringWithFormat:@"insert into %@(msg_code,msg_time,msg_direction,is_acked,is_delivered,msg_status,chat_with,is_listened,msg_body,local_file_url,file_url,is_new,is_play,avatar,name,isGroup,type ,subtype,fromID,toID) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",chatFileTB];
        NSString* bodyString = msg.body;
        NSString* chat_with = msg.chat_with;
        kMXMessageState state = msg.state;
        isSuccess = [db executeUpdate:insertSql,msg.messageId,
                     msg.sendTime,
                     [NSNumber numberWithInteger:msg.msg_direction],
                     [NSNumber numberWithInteger:msg.is_acked],
                     [NSNumber numberWithInteger:msg.is_delivered],
                     [NSNumber numberWithInteger:state],
                     chat_with,
                     [NSNumber numberWithInteger:msg.is_listened],
                     bodyString,
                     msg.locFileUrl,
                     msg.fileUrl,
                     [NSNumber numberWithInteger:msg.is_new],
                     [NSNumber numberWithInteger:msg.isPlay],
                     msg.avatar,
                     msg.name,
                     [NSNumber numberWithInteger:msg.chatType],
                     [NSNumber numberWithInteger:msg.type],
                     [NSNumber numberWithInteger:msg.subtype],msg.fromID,msg.toID];
    }];
    return isSuccess;
}

#pragma mark - 删除某条文件消息
-(BOOL)deleteFileMsgByMessageId:(NSString*)msgId chatId:(NSString*)chatId{
    __block BOOL res = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * deleteSql = [NSString stringWithFormat:@"delete  from %@ where msg_code = '%@'" ,chatFileTB,msgId];
        res = [db executeUpdate:deleteSql];
    }];
    return res;
}

#pragma mark - 更新某条文件消息
-(BOOL)updateFileMessage:(MessageModel *)msg {
    [self updateFileMsg:msg];
    __block BOOL isSuccess = false;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,msg.chat_with];
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set file_url = '%@', msg_body = '%@', local_file_url = '%@' where msg_code='%@'",tbName,msg.fileUrl,msg.body,msg.locFileUrl,msg.messageId];
        isSuccess = [db executeUpdate:updateSql];
    }];
    return isSuccess;
}

#pragma mark - 更新文件表的文件状态
-(BOOL)updateFileMsg:(MessageModel *)msg {
    if (msg.subtype != kMXMessageTypeFile) {
        return NO;
    }
    __block BOOL isSuccess = false;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set file_url = '%@', msg_body = '%@', local_file_url = '%@' where msg_code='%@'",chatFileTB,msg.fileUrl,msg.body,msg.locFileUrl,msg.messageId];
        isSuccess = [db executeUpdate:updateSql];
    }];
    return isSuccess;
}

#pragma mark - 查看本地所有文件
-(NSMutableArray*)selectAllFileChatHistory{
    NSMutableArray *resultArray = [NSMutableArray array];
    NSString* selectSql = [NSString stringWithFormat:@"select * from %@",chatFileTB];
    [fmdb inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]){
            MessageModel* msg = [MessageModel initWithFMResultSet:rs];
            if (msg.subtype != kMXMessageTypeFile) {
                continue;
            }
            [resultArray insertObject:msg atIndex:0];
        }
        [rs close];
    }];
    return resultArray;
}

//更新视频消息数据
-(BOOL)updateVideoUrlMessage:(MessageModel *)msg
{
    //@"update %@  set msg_status=%@ where msg_code='%@'"
    __block BOOL isSuccess = false;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,msg.chat_with];
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set file_url = '%@', msg_body = '%@' where msg_code='%@'",tbName,msg.fileUrl,msg.body,msg.messageId];
        isSuccess = [db executeUpdate:updateSql];
    }];
    return isSuccess;
}

//插入数据进入聊天表
-(BOOL)insertUnSendMsgToDB:(MessageModel *)msg
{
    __block BOOL isSuccess = false;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * insertSql=[NSString stringWithFormat:@"insert into %@(msg_code,msg_time,msg_direction,is_acked,is_delivered,msg_status,chat_with,is_listened,msg_body,local_file_url,file_url,is_new,is_play,avatar,name,isGroup,type ,subtype,fromID,toID) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",sendingTBName];
        NSString* bodyString = msg.body;
        NSString* chat_with = msg.chat_with;
        kMXMessageState state = msg.state;
        isSuccess = [db executeUpdate:insertSql,msg.messageId,
                     msg.sendTime,
                     [NSNumber numberWithInteger:msg.msg_direction],
                     [NSNumber numberWithInteger:msg.is_acked],
                     [NSNumber numberWithInteger:msg.is_delivered],
                     [NSNumber numberWithInteger:state],
                     chat_with,
                     [NSNumber numberWithInteger:msg.is_listened],
                     bodyString,
                     msg.locFileUrl,
                     msg.fileUrl,
                     [NSNumber numberWithInteger:msg.is_new],
                     [NSNumber numberWithInteger:msg.isPlay],
                     msg.avatar,
                     msg.name,
                     [NSNumber numberWithInteger:msg.chatType],
                     [NSNumber numberWithInteger:msg.type],
                     [NSNumber numberWithInteger:msg.subtype],msg.fromID,msg.toID];
    }];
    return isSuccess;
}

//删除某条信息
-(BOOL)deleteUnSendMsgByMessageId:(NSString*)msgId {
    __block BOOL res = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * deleteSql = [NSString stringWithFormat:@"delete  from %@ where msg_code = '%@'" ,sendingTBName,msgId];
        res = [db executeUpdate:deleteSql];
    }];
    return res;
}

-(NSMutableArray*)loadUnSendMessagesFromDB{
    NSMutableArray *resultArray = [NSMutableArray array];
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* selectSql = [NSString stringWithFormat:@"select * from %@",sendingTBName];
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]){
            MessageModel* msg = [MessageModel initWithFMResultSet:rs];
            [resultArray insertObject:msg atIndex:0];
        }
        [rs close];
    }];
    return resultArray;
}

//取出之前的聊天数据
-(NSMutableArray*)selectBeforeChatRecords:(NSString* )chatId time:(NSString* )time{
    NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,chatId];
    NSMutableArray *resultArray = [NSMutableArray array];
    NSString* selectSql = [NSString stringWithFormat:@"select * from %@",tbName];
    if (![StringUtil isEmpty:time]) {
        selectSql = [NSString stringWithFormat:@"%@ where msg_time < '%@'",selectSql,time];
    }
    selectSql = [NSString stringWithFormat:@"%@ order by msg_time desc limit %@",selectSql,@(fetch_pgae_size)];
    [fmdb inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]){
            MessageModel* msg = [MessageModel initWithFMResultSet:rs];
            [resultArray insertObject:msg atIndex:0];
        }
        [rs close];
    }];
    return resultArray;
}

-(NSMutableArray*)selectAllChatHistory:(NSString* )chatId {
    NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,chatId];
    NSMutableArray *resultArray = [NSMutableArray array];
    NSString* selectSql = [NSString stringWithFormat:@"select * from %@",tbName];
    [fmdb inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]){
            MessageModel* msg = [MessageModel initWithFMResultSet:rs];
            [resultArray insertObject:msg atIndex:0];
        }
        [rs close];
    }];
    return resultArray;
}


#pragma makr - 取出用户最新的聊天数据
//- (NSArray*)lastMessage:(NSString* )tbName
- (NSArray*)lastMessage:(NSString* )chatwith messageId:(NSString* )messageId
{
    
    NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,chatwith];
    if ([StringUtil isEmpty:messageId]) {
        NSMutableArray *resultArray=[NSMutableArray arrayWithCapacity:0];
        [fmdb inDatabase:^(FMDatabase *db) {
            NSString * selectSql=[NSString stringWithFormat:@"select * from %@ order by msg_time desc limit 0,1",tbName];
            FMResultSet *rs = [db executeQuery:selectSql];
            while ([rs next]) {
                MessageModel* msg = [MessageModel initWithFMResultSet:rs];
                [resultArray safeAddObj:msg];
            }
            [rs close];
        }];
        return resultArray;
    }else{
        NSMutableArray *resultArray=[NSMutableArray arrayWithCapacity:0];
        [fmdb inDatabase:^(FMDatabase *db) {
            NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where msg_code = '%@'",tbName,messageId];
            FMResultSet *rs = [db executeQuery:selectSql];
            while ([rs next]) {
                MessageModel* msg = [MessageModel initWithFMResultSet:rs];
                [resultArray safeAddObj:msg];
            }
            [rs close];
        }];
        return resultArray;
    }
   
}


#pragma makr - 取出用户全部的聊天数据
- (NSArray*)selectMsgByUserId:(NSString* )imid
{
    __block NSMutableArray *resultArray=[NSMutableArray array];
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,imid];
        NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where chat_with = '%@'  and (case(type = 2 and msg_direction = 1) when 1 then 0 else 1 end)   ",tbName,imid];
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]) {
            MessageModel* msg = [MessageModel initWithFMResultSet:rs];
            [resultArray safeAddObj:msg];
        }
        [rs close];
    }];
    return resultArray;
}


-(MessageModel* )getMessageByID:(NSString* )msgID chatId:(NSString*)chatId{
    __block MessageModel* msg = nil;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,chatId];
        NSString * selectSql=[NSString stringWithFormat:@"select *  from %@ where msg_code = '%@' " ,tbName,msgID];
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]) {
            msg = [MessageModel initWithFMResultSet:rs];
        }
        [rs close];
    }];
    return msg;
}

/**
 *  删除一个人的信息
 *
 *  @param mx_id 人的id
 */
-(BOOL)clearMessages:(NSString*)imid
{
    __block BOOL res = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,imid];
        NSString * deleteSql = [NSString stringWithFormat:@"delete  from %@ where chat_with = '%@'" ,tbName,imid];
        res = [db executeUpdate:deleteSql];
    }];
    return res;
}
-(BOOL)deleteFocosMessages:(NSString*)imid{
    __block BOOL res = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,imid];
        NSString * deleteSql = [NSString stringWithFormat:@"delete  from %@ where chat_with = '%@'" ,tbName,imid];
        res = [db executeUpdate:deleteSql];
    }];
    return res;
}


//删除一个人的发送信息
-(BOOL)deleteSendMsgByImid:(NSString*)imid{
    __block BOOL res = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,imid];
        NSString * deleteSql = [NSString stringWithFormat:@"delete  from %@ where chat_with = '%@' and msg_direction = 0 " ,tbName,imid];
        res = [db executeUpdate:deleteSql];
    }];
    return res;
}
//删除某条信息
-(BOOL)deleteMsgByMessageId:(NSString*)msgId chatId:(NSString*)chatId{
    [self deleteFileMsgByMessageId:msgId chatId:chatId];
    __block BOOL res = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,chatId];
        NSString * deleteSql = [NSString stringWithFormat:@"delete  from %@ where msg_code = '%@'" ,tbName,msgId];
        res = [db executeUpdate:deleteSql];
    }];
    return res;
}


-(NSInteger)getUnReadNumByImid:(NSString*)imid{
    __block NSInteger count ;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,imid];
        NSString * selectSql=[NSString stringWithFormat:@"select count(1) count from %@ where chat_with = '%@' and is_new = 0  ",tbName,imid];
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]) {
            count = [rs intForColumn:@"count"];
        }
        [rs close];
    }];
    return count;
}

-(BOOL)readMessageByChatID:(NSString*)imid{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,imid];
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set is_new = 1 where chat_with=? ",tbName];
        ret = [db executeUpdate:updateSql,imid];
    }];
    return ret;
}

//修改信息到达服务器状态
-(BOOL)updateMsgAck:(MessageModel*) msg{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,msg.chat_with];
        NSString * updateSql = [NSString stringWithFormat:@"update %@  set is_acked = %@,msg_status = %@ where msg_code='%@'",tbName,@(msg.is_acked),@(msg.state),msg.messageId];
        ret = [db executeUpdate:updateSql];
    }];
    return ret;
}

//修改信息被对方读取
-(BOOL)updateOtherReadState:(MessageModel*) msg{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,msg.chat_with];
        NSString * updateSql = [NSString stringWithFormat:@"update %@  set is_listened = 1,msg_status=%@,msg_time=%@ where msg_code='%@'",tbName,@(msg.state),msg.sendTime,msg.messageId];
        ret = [db executeUpdate:updateSql];
    }];
    return ret;
}



//修改信息被对方读取
-(BOOL)updateOtherReceiveState:(MessageModel*) msg{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,msg.chat_with];
        NSString * updateSql = [NSString stringWithFormat:@"update %@  set is_delivered = 1,msg_status=%@ where msg_code='%@'",tbName,@(msg.state),msg.messageId];
        ret = [db executeUpdate:updateSql];
    }];
    return ret;
}

- (BOOL)updateMessageState:(MessageModel*) msg{
    if (msg.state == kMXMessageStateSuccess) {
        if(msg.is_listened == 1){
            return [self updateOtherReadState:msg];
        }else if (msg.is_acked == 1) {
            return [self updateMsgAck:msg];
        }else if(msg.is_delivered == 1){
            return [self updateOtherReceiveState:msg];
        }
    }else if(msg.state >= kMXMessageState_Failure){
        [self updateMessageFailureState:msg];
    }
    return NO;
}


- (BOOL)updateMessageBody:(MessageModel*) msg{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,msg.chat_with];
        NSString * updateSql = [NSString stringWithFormat:@"update %@ set msg_body =? , msg_status=%@ ,local_file_url='%@' where msg_code='%@'",tbName,@(msg.state),msg.locFileUrl,msg.messageId];
        ret = [db executeUpdate:updateSql,msg.body];
    }];
    return ret;
}

- (BOOL)updateMessageFailureState:(MessageModel*) msg{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,msg.chat_with];
        NSString * updateSql = [NSString stringWithFormat:@"update %@  set msg_status=%@ where msg_code='%@'",tbName,@(msg.state),msg.messageId];
        ret = [db executeUpdate:updateSql,msg.body];
    }];
    return ret;
}

-(BOOL)updateMessagePlayState:(MessageModel*)msg{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,msg.chat_with];
        NSString * updateSql = [NSString stringWithFormat:@"update %@  set is_play = 1 where msg_code='%@'",tbName,msg.messageId];
        ret = [db executeUpdate:updateSql];
    }];
    return ret;
}

-(BOOL)updateWithdrawRowDataByMsgModel:(MessageModel*)model {
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,model.chat_with];
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set subtype=%@, msg_body='%@' where msg_code='%@' ",
                              tbName,
                              [NSNumber numberWithInteger:model.subtype],
                              model.body,
                              model.messageId];
        ret = [db executeUpdate:updateSql];
    }];
    return ret;
}

-(BOOL)updateRowDataByMsgModel:(MessageModel*)model{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,model.chat_with];
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set msg_status=%@,local_file_url='%@' where msg_code='%@' ",tbName,
                              @(model.state),model.locFileUrl,
                              model.messageId];
        ret = [db executeUpdate:updateSql];
    }];
    return ret;
}
/*************************************************消息end************************************************************/

#pragma -mark 好友
/*************************************************好友begin************************************************************/

-(BOOL)clearFriendRecords{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,newFriendTBName];
        NSString * deleteSql=[NSString stringWithFormat:@"delete from %@ ",tbName];
        ret = [db executeUpdate:deleteSql];
    }];
    return ret;
}
-(BOOL)updateFriendRecords:(FriendModel* )model{
    __block BOOL ret = FALSE;

    [fmdb inDatabase:^(FMDatabase *db) {
        
        NSString* tbName = [NSString stringWithFormat:@"%@%@",tbPrefix,newFriendTBName];
        NSString * updateSql = [NSString stringWithFormat:@"update %@ set followState = ? where chat_id = ?",tbName];
        ret = [db executeUpdate:updateSql,[NSNumber numberWithInteger:model.followState],model.userid];
    }];
    return ret;
}
//创建好友表
-(BOOL)createFriendTB{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * tableSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(chat_id TEXT(100) PRIMARY KEY,sex_type integer,time integer,name TEXT(100),headUrl TEXT(100),remark TEXT(100),nick_name TEXT(100),lastUpdateTime VARCHAR,enable_disturb BOOL,enable_top BOOL,followState integer, phoneNo VARCHAR, creatorRole integer DEFAULT 0);",friendTBName];
        ret = [db executeUpdate:tableSql];
    }];
    return ret;
}

//加载好友数据
-(NSMutableArray*)loadFriendFromDatabase{
    NSMutableArray *resultArray=[NSMutableArray arrayWithCapacity:0];
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * selectSql=[NSString stringWithFormat:@"select *  from %@ ",friendTBName];
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]) {
            FriendModel* friend = [[FriendModel alloc]init];
            friend.userid = [rs stringForColumn:@"chat_id"];
            friend.enable_top = [rs boolForColumn:@"enable_top"];
            friend.enable_disturb = [rs boolForColumn:@"enable_disturb"];
            friend.gender = [rs intForColumn:@"sex_type"];
            friend.name = [rs stringForColumn:@"name"];
            friend.avatar = [rs stringForColumn:@"headUrl"];
            friend.followState = [rs boolForColumn:@"followState"];
            friend.phone = [rs stringForColumn:@"phoneNo"];
            friend.remark = [rs stringForColumn:@"remark"];
            friend.creatorRole = [rs intForColumn:@"creatorRole"];
            [resultArray safeAddObj:friend];
        }
        [rs close];
    }];
    return resultArray;
}



//插入数据进入聊天表
//-(BOOL)insertFriendToDB:(MoYouModel *)moyou isExist:(BOOL)isExist
//{
//    //好友不存在
//    if (!isExist) {
//        [self insertFriendInfo:moyou];
//    }else{
//        [self updateMoYouToDB:moyou];
//    }
//    return FALSE;
//}
//-(BOOL)insertFriendInfo:(MoYouModel*)moyou{
//    __block BOOL ret = FALSE;
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString * insertSql=[NSString stringWithFormat:@"insert into %@(chat_id,enable_top,enable_disturb,sex_type,lastUpdateTime,name,headUrl,remark,followState) values (?,?,?,?,?,?,?,?,?);",friendTBName];
//        ret = [db executeUpdate:insertSql,moyou.chatId,[NSNumber numberWithBool:moyou.enable_top],[NSNumber numberWithBool:moyou.enable_disturb], [NSNumber numberWithInt:moyou.sexType],moyou.lastUpdateTime,moyou.name,moyou.headUrl,moyou.remark,[NSNumber numberWithInt:moyou.followState]];
//    }];
//    return ret;
//}
//
- (NSString *)getFriendHandleSqlDB:(FriendModel *)moyou isExist:(BOOL)isExist{
    if (!isExist) {
        NSString * insertSql=[NSString stringWithFormat:@"insert into %@(chat_id,enable_top,enable_disturb,sex_type,lastUpdateTime,name,headUrl,remark,nick_name,followState,phoneNo,creatorRole) values (?,?,?,?,?,?,?,?,?,?,?,?);",friendTBName];
        return insertSql;
    }else{
        NSString * updateSql = [NSString stringWithFormat:@"update %@ set name = ?, headUrl = ?,followState = ?,enable_top = ?,enable_disturb = ? ,lastUpdateTime=?,remark=?,nick_name=?  where chat_id =?",friendTBName];
        return updateSql;
    }
}
-(BOOL)executeMoYouSql:(NSArray*)sqlArray model:(NSArray*) friends{
    [fmdb inDatabase:^(FMDatabase *db) {
        @try {
            [db beginTransaction];
            [sqlArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString* sql = (NSString* )obj;
                FriendModel* friend = friends[idx];
                if ([sql hasPrefix:@"i"]) {
                    [db executeUpdate:sql,friend.userid,[NSNumber numberWithBool:friend.enable_top],[NSNumber numberWithBool:friend.enable_disturb], [NSNumber numberWithInt:friend.gender],friend.lastUpdateTime,friend.name,friend.avatar,friend.remark,friend.nick_name,[NSNumber numberWithInteger:friend.followState],friend.phone,[NSNumber numberWithInteger:friend.creatorRole]];
                    NSLog(@"insert...");
                }else if([sql hasPrefix:@"u"]){
                    [db executeUpdate:sql,friend.name,friend.avatar,[NSNumber numberWithInteger:friend.followState],[NSNumber numberWithBool:friend.enable_top],[NSNumber numberWithBool:friend.enable_disturb],friend.lastUpdateTime,friend.remark,friend.nick_name,friend.userid];
                    NSLog(@"update...");
                }
            }];
            [db commit];
        } @catch (NSException *exception) {
            MLog(@"failure");
        }
        @finally {

        }
    }];
    return TRUE;
}

-(BOOL)updateMoYouToDB:(FriendModel*)moyou{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql = [NSString stringWithFormat:@"update %@ set name = ?, headUrl = ?,followState = ?,enable_top = ?,enable_disturb = ? ,lastUpdateTime=?,remark=?  where chat_id =?",friendTBName];
        ret = [db executeUpdate:updateSql,moyou.name,moyou.avatar,[NSNumber numberWithInteger:moyou.followState],[NSNumber numberWithBool:moyou.enable_top],[NSNumber numberWithBool:moyou.enable_disturb],moyou.lastUpdateTime,moyou.remark,moyou.userid];
    }];
    return ret;
}
//此处只是做修改
- (BOOL)deleteFriendByID:(NSString*)chatID {
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* sql = [NSString stringWithFormat:@"update %@ set followState = 1 where chat_id = '%@'",friendTBName,chatID];
        ret = [db executeUpdate:sql];
    }];
    return ret;
}
//
////此处只是做修改
//- (BOOL)clearFriend:(NSString*)chatID {
//    __block BOOL ret = FALSE;
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString* sql = [NSString stringWithFormat:@"update %@ set followState = 0 where chat_id like '%@' and followState = 1 ",friendTBName,chatID];
//        ret = [db executeUpdate:sql];
//    }];
//    return ret;
//}
//
//- (int)getFriendTopNums{
//    __block BOOL ret = FALSE;
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString* selectSql = [NSString stringWithFormat:@"select count(chat_id) num from %@ where enable_top != 0 ",friendTBName];
//        ret = [db executeUpdate:selectSql];
//    }];
//    return ret;
//}
//
-(FriendModel*)selectFriendByChatId:(NSString*)chatId {
    __block FriendModel* friend = nil;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * selectSql=[NSString stringWithFormat:@"select *  from %@ where chat_id='%@'" ,friendTBName,chatId];
        FMResultSet* rs = [db executeQuery:selectSql];
        while ([rs next]) {
            friend = [[FriendModel alloc] init];
            friend.userid = [rs stringForColumn:@"chat_id"];
            friend.name = [rs stringForColumn:@"name"];
            friend.remark = [rs stringForColumn:@"remark"];
            friend.nick_name = [rs stringForColumn:@"nick_name"];
            friend.followState = [rs intForColumn:@"followState"];
            friend.creatorRole = [rs intForColumn:@"creatorRole"];
            break;
        }
        [rs close];
    }];
    return friend;
}
//
//-(NSArray*)selectFriendByKeyword:(NSString*)keyword{
//    NSMutableArray* resultArray = [NSMutableArray array];
//    [fmdb inDatabase:^(FMDatabase *db) {
//        if ([db open]) {
//            NSString * selectSql=[NSString stringWithFormat:@"select *  from %@  where remark like '%%%@%%' or name like '%%%@%%' or chat_id like '%%%@%%'" ,friendTBName,keyword,keyword,keyword];
//            FMResultSet* rs = [db executeQuery:selectSql];
//            while ([rs next]) {
//                MoYouModel* moyou = [[MoYouModel alloc]init];
//                moyou.chatId = [rs stringForColumn:@"chat_id"];
//                moyou.enable_top = [rs intForColumn:@"enable_top"];
//                moyou.enable_disturb = [rs intForColumn:@"enable_disturb"];
//                moyou.sexType = [rs intForColumn:@"sex_type"];
//                moyou.lastUpdateTime = [rs stringForColumn:@"lastUpdateTime"];
//                moyou.name = [rs stringForColumn:@"name"];
//                moyou.headUrl = [rs stringForColumn:@"headUrl"];
//                moyou.remark = [rs stringForColumn:@"remark"];
//                moyou.followState = [rs intForColumn:@"followState"];
//                [resultArray addObject:moyou];
//            }
//        }
//    }];
//    return resultArray;
//}

////修改信息被对方读取
//-(BOOL)updateFriendIsTop:(MoYouModel*) moyou{
//    __block BOOL ret = FALSE;
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString * updateSql = [NSString stringWithFormat:@"update %@  set enable_top = %@ ,enable_disturb = %@, lastUpdateTime=%@ where chat_id='%@'",friendTBName,[NSNumber numberWithBool:moyou.enable_top],[NSNumber numberWithBool:moyou.enable_disturb],moyou.lastUpdateTime,moyou.chatId];
//        ret = [db executeUpdate:updateSql];
//    }];
//    return ret;
//}
///*************************************************好友end************************************************************/
//
//
///*************************************************群组begin************************************************************/
//
////创建群组
/*
 is_set_name: 是否设置群名（0：未设置  1：设置）
 */
-(BOOL)createGroupTB{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * tableSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(groupId VARCHAR PRIMARY KEY,groupName VARCHAR,is_set_name VARCHAR,groupAvatar VARCHAR,groupMemNum integer,roleType integer,enable_disturb BOOL,enable_top BOOL,enable_contact BOOL,lastUpdateTime VARCHAR,nickname VARCHAR ,qrcode_url VARCHAR,groupMaxMemNum VARCHAR,create_time VARCHAR);",groupTBName];
        ret = [db executeUpdate:tableSql];
    }];
    return ret;
}
//
//-(BOOL)clearGroup{
//    __block BOOL ret = FALSE;
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString * updateSql=[NSString stringWithFormat:@"update %@ set enable_contact=? ",groupTBName];
//        ret = [db executeUpdate:updateSql,@"0"];
//    }];
//    return ret;
//}
//
//插入群组数据
-(BOOL)insertGroup:(ChatGroupModel*)group {
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        //[db open];
        NSString * insertSql=[NSString stringWithFormat:@"insert into %@(groupId,groupName,is_set_name,groupAvatar,groupMemNum,roleType,enable_disturb,enable_top,enable_contact,lastUpdateTime,qrcode_url,groupMaxMemNum,create_time,nickName ) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?);",groupTBName];
        ret = [db executeUpdate:insertSql,group.groupId,group.groupName,group.is_set_name,group.groupAvatar,[NSNumber numberWithInteger:group.groupMemNum],[NSNumber numberWithInteger:group.roleType],[NSNumber numberWithBool:group.enable_disturb],[NSNumber numberWithBool:group.enable_top],[NSNumber numberWithBool:group.enable_contact],group.lastUpdateTime,group.qrcodeUrl,[NSNumber numberWithInteger:group.maxGroupMemNum],group.createTime,group.nickname];
    }];
    return ret;
}

//修改群组数据
-(BOOL)updateGroup:(ChatGroupModel*)group {
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set groupName = ?,is_set_name = ?,groupAvatar =?,groupMemNum=?,roleType=?,qrcode_url=?,enable_contact=? where groupId=?",groupTBName];
        ret = [db executeUpdate:updateSql,group.groupName,group.is_set_name,group.groupAvatar,[NSNumber numberWithInteger:group.groupMemNum],[NSNumber numberWithInteger:group.roleType],group.qrcodeUrl,[NSNumber numberWithBool:group.enable_contact],group.groupId];
    }];
    return ret;
}

-(NSString*)getGroupHandleSqlDB:(ChatGroupModel*)group isExist:(BOOL)isExist{
    NSString* sql = @"";
    if (!isExist) {
        sql =[NSString stringWithFormat:@"insert into %@(groupId,groupName,is_set_name,groupAvatar,groupMemNum,roleType,enable_disturb,enable_top,enable_contact,lastUpdateTime,qrcode_url,groupMaxMemNum,create_time,nickName) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?);",groupTBName];
    }else{
        sql = [NSString stringWithFormat:@"update %@ set groupName = ?,is_set_name = ?,groupAvatar =?,groupMemNum=?,roleType=?,qrcode_url=?,enable_contact=? where groupId=?",groupTBName];
    }
    return sql;
}
//
-(BOOL)executeGroupSql:(NSArray*)sqlArray model:(NSArray*) groups{
    [fmdb inDatabase:^(FMDatabase *db) {
        dispatch_queue_t queue=dispatch_queue_create("com.privateQueue", NULL);
        @try {
            [db beginTransaction];
            [sqlArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString* sql = (NSString* )obj;
                ChatGroupModel* group = groups[idx];
                   dispatch_async(queue, ^{
                        if ([sql hasPrefix:@"i"]) {
                            [db executeUpdate:sql,group.groupId,group.groupName,group.is_set_name,group.groupAvatar,[NSNumber numberWithInteger:group.groupMemNum],[NSNumber numberWithInteger:group.roleType],[NSNumber numberWithBool:group.enable_disturb],[NSNumber numberWithBool:group.enable_top],[NSNumber numberWithBool:group.enable_contact],group.lastUpdateTime,group.qrcodeUrl,[NSNumber numberWithInteger:group.maxGroupMemNum],group.createTime,group.nickname];
                        }else if([sql hasPrefix:@"u"]){
                            [db executeUpdate:sql,group.groupName,group.is_set_name,group.groupAvatar,[NSNumber numberWithInteger:group.groupMemNum],[NSNumber numberWithInteger:group.roleType],group.qrcodeUrl,[NSNumber numberWithBool:group.enable_contact],group.groupId];
                        }
                   });
            }];
            [db commit];
        } @catch (NSException *exception) {
            MLog(@"failure");
        }
        @finally {

        }
    }];
    return TRUE;
}

//
-(NSMutableArray*)loadChatGroupFromDatabase{
    NSMutableArray* resultArray = [NSMutableArray array];
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * selectSql=[NSString stringWithFormat:@"select * from %@" ,groupTBName];
        FMResultSet* rs = [db executeQuery:selectSql];
        while ([rs next]) {
            ChatGroupModel* group = [[ChatGroupModel alloc]init];
            group.groupId = [rs stringForColumn:@"groupId"];
            group.groupName = [rs stringForColumn:@"groupName"];
            group.groupAvatar = [rs stringForColumn:@"groupAvatar"];
            group.is_set_name = [rs stringForColumn:@"is_set_name"];
            group.groupMemNum = [rs intForColumn:@"groupMemNum"];
            group.roleType = [rs intForColumn:@"roleType"];
            group.enable_top = [rs boolForColumn:@"enable_top"];
            group.enable_disturb = [rs boolForColumn:@"enable_disturb"];
            group.enable_contact = [rs boolForColumn:@"enable_contact"];
            group.nickname = [rs stringForColumn:@"nickName"];
            if (group.enable_top ) {
                MLog(@"设置成功");
            }
            group.lastUpdateTime = [rs stringForColumn:@"lastUpdateTime"];
            group.qrcodeUrl = [rs stringForColumn:@"qrcode_url"];
            group.maxGroupMemNum = [[rs stringForColumn:@"groupMaxMemNum"] intValue];
            group.createTime = [rs stringForColumn:@"create_time"];
            [resultArray addObject:group];
        }
    }];
    return resultArray;
}
//
-(BOOL)removeChatGroup:(ChatGroupModel*)group{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql=[NSString stringWithFormat:@"delete from %@  where groupId = '%@'",groupTBName,group.groupId];
        ret = [db executeUpdate:updateSql,[NSNumber numberWithBool:NO]];
    }];
    return ret;
}

//修改我的群昵称
-(BOOL)updateGroupNickname:(NSString*) nickName groupId:(NSString*)groupId{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set  nickname = ? where groupId = ?",groupTBName];
        ret = [db executeUpdate:updateSql,nickName,groupId];
    }];
    return ret;
}
//创建成员表
-(BOOL)createGroupMember{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * tableSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(groupId VARCHAR ,memberId VARCHAR,nickName VARCHAR,roleType integer,photoUrl VARCHAR,name VARCHAR,remarkName VARCHAR);",memTBName];
        ret = [db executeUpdate:tableSql];
    }];
    return ret;

}
//
//
-(BOOL)isExistChatGroupMember:(FriendModel*)member{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where groupId like '%@' and memberId like '%@'",memTBName,member.groupid,member.userid];
        FMResultSet* rs = [db executeQuery:selectSql];
        if ([rs next]) {
            ret = TRUE;
        }
        [rs close];
    }];
    return ret;
}

//插入群组成员数据
-(BOOL)insertGroupMemeber:(FriendModel*) member{
    __block BOOL ret = FALSE;
    if (![self isExistChatGroupMember:member]) {
        [fmdb inDatabase:^(FMDatabase *db) {
            NSString * insertSql=[NSString stringWithFormat:@"insert into %@(groupId,memberId,nickName,roleType,photoUrl,name,remarkName) values (?,?,?,?,?,?,?);",memTBName];
            ret = [db executeUpdate:insertSql,
                   member.groupid,
                   member.userid,
                   member.groupNickname,
                   [NSNumber numberWithInteger:member.roleType],
                   member.avatar,
                   member.name,
                   member.remark?:@""];
        }];
    }else{
        [fmdb inDatabase:^(FMDatabase *db) {
            NSString * updateSql=[NSString stringWithFormat:@"update %@ set roleType = ?,nickName = ?,photoUrl = ? ,remarkName = ? where groupId = ? and memberId = ? and name = ?",memTBName];
            ret = [db executeUpdate:updateSql,
                   [NSNumber numberWithInteger:member.roleType],
                   member.groupNickname,
                   member.avatar,
                   member.remark?:@"",
                   member.groupid,
                   member.userid,
                   member.name];
        }];
    }
    return ret;
}
//
-(BOOL)removeGroupMember:(FriendModel*) member{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* sql = [NSString stringWithFormat:@"delete from %@ where groupId = ? and memberId = ?",memTBName];
        ret = [db executeUpdate:sql,member.groupid,member.userid];
    }];
    return  ret;
}
//
////删除所有群成员
//-(BOOL)clearGroupMembers:(NSString*) groupId{
//    return YES;
//    __block BOOL ret = FALSE;
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString* sql = [NSString stringWithFormat:@"delete from %@ where groupId = ? ",memTBName];
//        ret = [db executeUpdate:sql,groupId];
//    }];
//    return ret;
//}
//
-(BOOL)updateGroupTopState:(ChatGroupModel*)group{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set enable_top = ? , lastUpdateTime = ?  where groupId=?",groupTBName];
        ret = [db executeUpdate:updateSql,[NSNumber numberWithBool:group.enable_top],group.lastUpdateTime,group.groupId];
    }];
    return ret;
}

///更新群消息免打扰设置
-(BOOL)updateGroupDisturbState:(ChatGroupModel*)group{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set enable_disturb = ?  where groupId=?",groupTBName];
        ret = [db executeUpdate:updateSql,[NSNumber numberWithBool:group.enable_disturb],group.groupId];
    }];
    return ret;
}

///更新个人消息免打扰设置
-(BOOL)updateFriendDisturbState:(FriendModel*)model{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set enable_disturb = ?  where chat_id=?",friendTBName];
        ret = [db executeUpdate:updateSql,[NSNumber numberWithBool:model.enable_disturb],model.userid];
    }];
    return ret;
}

////查询是否打开消息免打扰
-(BOOL)checkEnableDisturb:(MessageModel *)message {
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * selectSql = nil;
        NSString * fromId = nil;
        NSString * _id = nil;
        
        if(message.chatType == eConversationTypeGroupChat) {
            selectSql=[NSString stringWithFormat:@"select * from %@ ",groupTBName];
            fromId = message.chat_with;
            _id = @"groupId";
        } else {
            selectSql=[NSString stringWithFormat:@"select * from %@ ",friendTBName];
            fromId = message.fromID;
            _id = @"chat_id";
        }
        
        FMResultSet * rs=[db executeQuery:selectSql];
        while ([rs next]) {
            if([fromId isEqualToString:[rs stringForColumn:_id]]) {
                ret = [rs intForColumn:@"enable_disturb"] == 1;
                break;
            }
        }
        [rs close];
    }];
    return ret;
}

///更新群人数
-(BOOL)updateGroupMemNum:(ChatGroupModel*)group {
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set  groupMemNum = ? where groupId = ?",groupTBName];
        ret = [db executeUpdate:updateSql,@(group.groupMemNum),group.groupId];
    }];
    return ret;
}

//
//-(BOOL)updateSaveToContact:(ChatGroupModel*)group{
//    __block BOOL ret = FALSE;
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString * updateSql=[NSString stringWithFormat:@"update %@ set enable_contact = ?  where groupId=?",groupTBName];
//        ret = [db executeUpdate:updateSql,[NSNumber numberWithBool:group.enable_contact],group.groupId];
//    }];
//    return ret;
//}
-(FriendModel*)selectGroupMember:(NSString*) roomId memberId:(NSString*)memberId{
    __block FriendModel* member = nil;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where groupId like '%@' and memberId like '%@'" ,memTBName,roomId,memberId];
        FMResultSet* rs = [db executeQuery:selectSql];
        if ([rs next]) {
            member = [[FriendModel alloc]init];
            member.userid = [rs stringForColumn:@"memberId"];
            member.groupid = [rs stringForColumn:@"groupId"];
            member.roleType = [rs intForColumn:@"roleType"];
            member.groupNickname = [rs stringForColumn:@"nickName"];
            member.avatar = [rs stringForColumn:@"photoUrl"];
            member.name = [rs stringForColumn:@"name"];
            member.remark = [rs stringForColumn:@"remarkName"];
        }
        [rs close];
    }];
    return member;
}
- (NSMutableArray*)loadGroupMembers:(NSString*) roomId {
    __block NSMutableArray* members = [[NSMutableArray alloc]initWithCapacity:10];
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where groupId like '%@' " ,memTBName,roomId];
        FMResultSet* rs = [db executeQuery:selectSql];
        while ([rs next]) {
            FriendModel * member = [[FriendModel alloc]init];
            member.userid = [rs stringForColumn:@"memberId"];
            member.groupid = [rs stringForColumn:@"groupId"];
            member.roleType = [rs intForColumn:@"roleType"];
            member.groupNickname = [rs stringForColumn:@"nickName"];
            member.avatar = [rs stringForColumn:@"photoUrl"];
            member.name = [rs stringForColumn:@"name"];
            member.remark = [rs stringForColumn:@"remarkName"];
            [members addObject:member];
        }
        [rs close];
    }];
    return members;
}
//
//-(NSMutableArray*)loadGroupMembersFromDatabase:(NSString*) roomId{
//    NSMutableArray* resultArray = [NSMutableArray array];
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString * selectSql=[NSString stringWithFormat:@"select *  from %@ where groupId like '%@' " ,memTBName,roomId];
//        FMResultSet * rs=[db executeQuery:selectSql];
//        while ([rs next]) {
//            GroupMemberModel* member = [[GroupMemberModel alloc]init];
//            member.userId = [rs stringForColumn:@"memberId"];
//            member.roomId = [rs stringForColumn:@"groupId"];
//            member.roleType = [rs intForColumn:@"roleType"];
//            member.nickName = [rs stringForColumn:@"nickName"];
//            member.photoUrl = [rs stringForColumn:@"photoUrl"];
//            [resultArray addObject:member];
//        }
//        [rs close];
//    }];
//    return resultArray;
//}

/*************************************************群组end************************************************************/

/*************************************************会话begin************************************************************/

//创建外层聊天列表
-(BOOL)createConversationTB{
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * createSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(chat_id VARCHAR PRIMARY KEY,conversationType integer,lastClearTime VARCHAR DEFAULT NULL,unReadNums integer,enableDisturb integer,associate_id VARCHAR,message_id VARCHAR,ext2 VARCHAR,isAt integer,creatorRole integer DEFAULT 0);",conversationTBName];
        BOOL res = [db executeUpdate:createSql];
        if (!res) {
        }
    }];
    return YES;
}


//修改最后一次清空的时间
-(BOOL)updateClearTime:(NSString*)chatId time:(NSString*)time msgID:(NSString* )messageId{
    if ([StringUtil isEmpty:time]) {
        return NO;
    }
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set lastClearTime = '%@' , unReadNums = 0,message_id = '%@' where chat_id=?",conversationTBName, time,messageId];
        ret = [db executeUpdate:updateSql,chatId];
    }];
    return ret;
}
////修改未读数量
-(BOOL)updateUnReadNums:(int)unReadNums atStatus:(BOOL)atStatus chatId:(NSString*)chatId{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = conversationTBName;
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set unReadNums = %d, isAt = %@ where chat_id=?",tbName, unReadNums, [NSNumber numberWithBool:atStatus]];
        ret = [db executeUpdate:updateSql,chatId];
    }];
    return ret;
}
////修改最后一条消息内容和时间
-(BOOL)updateLastMessage:(NSString*) chatId associateId:(NSString*)associateId messageId:(NSString* )messageId{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {//, associateId,messageId
        NSString * updateSql=[NSString stringWithFormat:@"update %@ set associate_id = ? , message_id = ?  where chat_id = ?",conversationTBName];
        ret = [db executeUpdate:updateSql,associateId,messageId,chatId];
    }];
    return ret;
}

////查询聊天首页列表
-(NSMutableArray*)loadConversationsFromDB{
    NSMutableArray* dataArray = [[NSMutableArray alloc]initWithCapacity:0];
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * selectSql=[NSString stringWithFormat:@"select * from %@ ",conversationTBName];
        FMResultSet * rs=[db executeQuery:selectSql];
        while ([rs next]) {
            MXConversation* model = [[MXConversation alloc] init];
            model.chat_id = [rs stringForColumn:@"chat_id"];
            model.conversationType = [rs intForColumn:@"conversationType"];
            model.lastClearTime = [rs stringForColumn:@"lastClearTime"];
            model.unReadNums = [rs intForColumn:@"unReadNums"];
            model.associate_id = [rs stringForColumn:@"associate_id"];
            model.lastMessageId = [rs stringForColumn:@"message_id"];
            model.isAt = [rs boolForColumn:@"isAt"];
            model.creatorRole = [rs intForColumn:@"creatorRole"];
            [dataArray addObject:model];
        }
        [rs close];
    }];
    return dataArray;
}

////查询聊天首页列表
//-(NSMutableArray*)loadShopConversationsFromDB{
//    NSMutableArray* dataArray = [[NSMutableArray alloc]initWithCapacity:0];
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString * selectSql=[NSString stringWithFormat:@"select * from %@ ",shopConversationTBName];
//        FMResultSet * rs=[db executeQuery:selectSql];
//        while ([rs next]) {
//            MXConversation* model = [[MXConversation alloc] init];
//            model.chat_id = [rs stringForColumn:@"chat_id"];
//            model.conversationType = [rs intForColumn:@"conversationType"];
//            model.lastClearTime = [rs stringForColumn:@"lastClearTime"];
//            model.unReadNums = [rs intForColumn:@"unReadNums"];
//            model.associate_id = [rs stringForColumn:@"associate_id"];
//            [dataArray addObject:model];
//        }
//        [rs close];
//    }];
//    return dataArray;
//}

-(BOOL)createConversation:(MXConversation*)conversation{
    if ([StringUtil isEmpty:conversation.chat_id]) {
        return NO;
    }
    __block BOOL ret = false;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * insertSql=[NSString stringWithFormat:@"insert into %@(chat_id,conversationType,unReadNums,associate_id,isAt,creatorRole) values (?,?,?,?,?,?);",conversationTBName];
        ret = [db executeUpdate:insertSql,
               conversation.chat_id,
               [NSNumber numberWithInteger:conversation.conversationType],
               conversation.unReadNums,
               conversation.associate_id,
               [NSNumber numberWithBool:conversation.isAt],
               [NSNumber numberWithInteger:conversation.creatorRole]];
    }];
    return ret;
}
//
//-(BOOL)createShopConversation:(MXConversation*)conversation{
//    if ([StringUtil isEmpty:conversation.chat_id]) {
//        return NO;
//    }
//    __block BOOL ret = false;
//    [fmdb inDatabase:^(FMDatabase *db) {
//        NSString * insertSql=[NSString stringWithFormat:@"insert into %@(chat_id,conversationType,unReadNums,associate_id) values (?,?,?,?);",shopConversationTBName];
//        ret = [db executeUpdate:insertSql,conversation.chat_id,[NSNumber numberWithInteger:conversation.conversationType],conversation.unReadNums,conversation.associate_id];
//    }];
//    return ret;
//}

-(BOOL)deleteConversation:(NSString*) chatId{
    __block BOOL ret = false;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* sql = [NSString stringWithFormat:@"delete from %@ where chat_id = '%@'",conversationTBName,chatId];
        ret = [db executeUpdate:sql];
    }];
    return ret;
}

/*************************************************会话end************************************************************/
@end
