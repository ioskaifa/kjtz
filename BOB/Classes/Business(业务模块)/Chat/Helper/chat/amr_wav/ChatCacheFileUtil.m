//
//  ChatCacheFileUtil.m
//  NewMC
//
//  Created by 话语科技 on 12-10-25.
//
//

#import "ChatCacheFileUtil.h"
#import "DocumentManager.h"
#import "LcwlChat.h"

NSString *const myUSERID=@"myUSERID";

@implementation ChatCacheFileUtil

static ChatCacheFileUtil *sharedInstance;

+ (ChatCacheFileUtil*)sharedInstance
{
    if (sharedInstance==nil) {
        sharedInstance = [[ChatCacheFileUtil alloc] init];
    }
    return sharedInstance;
}

- (id)init
{
    return [super init];
}

- (NSString*)userDocPath{
    
    UserModel *userModel=[LcwlChat shareInstance].user;
    
    NSString *userFolderPath = [[DocumentManager cacheDocDirectory] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/",userModel.user_id]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:userFolderPath]) {
        [fileManager createDirectoryAtPath:userFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return userFolderPath;
}

- (BOOL) deleteWithContentPath:(NSString *)thePath{
    NSError *error=nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:thePath]) {
        [fileManager removeItemAtPath:thePath error:&error];
    }
    if (error) {
        MLog(@"删除文件时出现问题:%@",[error localizedDescription]);
        return NO;
    }
    return YES;
}

+ (BOOL)fileExistsAtPath:(NSString *)thePath {
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return[fileManager fileExistsAtPath:thePath isDirectory:&isDir];
}

- (NSString*)chatCachePathWithFriendId:(NSString*)theFriendId andType:(NSInteger)theType
{
    NSString *userChatFolderPath = [[self userDocPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"chatLog/%@/",theFriendId]];
    switch (theType) {
        case 1:
            userChatFolderPath = [userChatFolderPath stringByAppendingPathComponent:@"voice/"];
            break;
        case 2:
            userChatFolderPath = [userChatFolderPath stringByAppendingPathComponent:@"image/"];
            break;
        default:
            break;
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:userChatFolderPath]) {
        [fileManager createDirectoryAtPath:userChatFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return userChatFolderPath;
}

- (void)deleteFriendChatCacheWithFriendId:(NSString*)theFriendId
{
    NSString *userChatFolderPath = [[self userDocPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"chatLog/%@/",theFriendId]];
    
    [[NSFileManager defaultManager] removeItemAtPath:userChatFolderPath error:nil];
}

- (void)deleteAllFriendChatDoc
{
    NSString *userChatFolderPath = [[self userDocPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"chatLog/"]];
    
    [[NSFileManager defaultManager] removeItemAtPath:userChatFolderPath error:nil];
    
}

-(NSString*)handleChatImagePath:(NSString*) localPath remoteUrl:(NSString* )remoteUrl{
    if (![StringUtil isEmpty:localPath]) {
        return localPath;
    }
    return remoteUrl;
 
}
#pragma mark - 拼接图片路径
- (NSString *)appendImgPath:(NSString *)path
{
    if ([StringUtil isEmpty:path]) {
        return @"";
    }
    
    if ([path hasPrefix:@"http"]) {
        return path;
    } else if ([path compare:@"/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, 1)] != NSOrderedSame) {
        path = [@"/" stringByAppendingString:path];
    }
    
//    return MOXIAN_URL_STR_NEW(@"image", path);
    return @"";
}
@end
