//
//  SexModel.h
//  MoPal_Developer
//
//  用户注册对象
//  Created by litiankun on 15/1/31.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CountryCodeMacro @"countryCode"
#define CountryCodeNameMacro @"countryCodeName"

@interface RegisterModel : NSObject

// 基本属性
//性别（男：1 女：0）
@property (nonatomic) MoGenderType sexType;
@property (nonatomic,strong) NSString *nickName;
@property (nonatomic) NSString *birthday;
@property (nonatomic) NSInteger birthdayViableType;

@property (nonatomic,strong) NSString *inviteCode;
@property (nonatomic,strong) NSString *captcha;
@property (nonatomic,strong) NSString *phoneNo;

@property (nonatomic,strong) NSString *password;
@property (nonatomic,strong) NSString *confirmPassword;

// 扩展属性
@property (nonatomic,strong) NSMutableDictionary *ext;

//yangjiale
@property (nonatomic,strong) NSString *work;
@property (nonatomic,strong) NSString *country;
//0注册1登陆
@property (nonatomic) int type;
//token
@property (nonatomic,strong) NSString *token;

@property (nonatomic,strong) NSString *picCode;


@property (nonatomic,strong) NSString *img_id;

@property (nonatomic,strong) NSString *img_io;

@property (nonatomic) int loginType;


-(UIImage*)ioImage;

@end
