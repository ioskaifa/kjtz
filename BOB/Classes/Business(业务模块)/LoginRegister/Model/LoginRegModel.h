//
//  LoginRegModel.h
//  AdvertisingMaster
//
//  Created by mac on 2019/4/6.
//  Copyright © 2019年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, LoginType) {
    RegisterPhoneType,
    RegisterEmailType,
    LoginAccountType,     // 账户登陆
    ForgetPhoneType,
    ForgetEmailType
};

@interface LoginRegModel : NSObject

//账号
@property (nonatomic,copy) NSString *regAccount;
//账号
@property (nonatomic,copy) NSString *account;
//密码
@property (nonatomic,copy) NSString *password;
//确认密码
@property (nonatomic,copy) NSString *confirm_password;
//密码
@property (nonatomic,copy) NSString *tradePwd;
//确认密码
@property (nonatomic,copy) NSString *confirm_tradePwd;
///手机号
@property (nonatomic,copy) NSString *phoneNo;
///短信验证码
@property (nonatomic,copy) NSString *captcha;
//登陆类型
@property (nonatomic) LoginType type;
//邮箱
@property (nonatomic,copy) NSString *emailNo;
///邀请码
@property (nonatomic,copy) NSString *inviteCode;

///用户名
@property (nonatomic,copy) NSString *userName;
///昵称
@property (nonatomic,copy) NSString *nickName;
//二次密码密码
@property (nonatomic,copy) NSString *rePassword;

//图形验证码
@property (nonatomic,copy) NSString *picCode;

//图形验证码
@property (nonatomic,copy) NSString *img_id;

@property (nonatomic,copy) NSString *img_io;


///交易密码
@property (nonatomic,copy) NSString *pay_password;

@property (nonatomic,copy) NSString *valid_flag;

- (UIImage*)ioImage;
@end


NS_ASSUME_NONNULL_END
