//
//  SexModel.m
//  MoPal_Developer
//
//  Created by litiankun on 15/1/31.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "RegisterModel.h"

@implementation RegisterModel
-(id)init{
    if (self = [super init]) {
        self.img_id = @"";
        self.img_io = @"";
    }
    return self;
}

-(UIImage*)ioImage{
    NSData * decodeData = [[NSData alloc] initWithBase64EncodedString:self.img_io options:(NSDataBase64DecodingIgnoreUnknownCharacters)];
    if (decodeData) {
        UIImage *decodedImage = [UIImage imageWithData:decodeData];
        if (decodedImage) {
            return decodedImage;
        }
    }
  
    return nil;
}
@end
