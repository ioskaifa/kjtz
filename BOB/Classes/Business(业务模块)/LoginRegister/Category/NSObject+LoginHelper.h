//
//  NSObject+LoginHelper.h
//  RatelBrother
//
//  Created by mac on 2019/5/24.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (LoginHelper)
//图形验证码
- (void)createImgCode:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion;
///绑定手机号 - 需要token
- (void)send_codeAccept:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion;
///发送手机验证码 - 不需要token
- (void)send_code:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion;
///发送手机验证码 - 需要token
- (void)send_codeToken:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion;
//注册
- (void)register1:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion;
- (void)register2:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
///检查该手机号是否已注册
- (void)checkUserExist:(NSString *)phone completion:(RequestResultObjectCallBack)completion;

- (void)clearUserToken:(MXHttpRequestResultObjectCallBack)completion;

//登录
- (void)login:(NSDictionary*)param superView:(UIView *)superView completion:(MXHttpRequestCallBack)completion;
//退出登录
- (void)loginout:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion;
//忘记密码
- (void)userForgetPass:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion;
//重置密码
- (void)userResetPass:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion;

- (void)sendSmsCodeToken:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion;

- (void)userLogOut:(NSDictionary*)param view:(UIView *)superV completion:(MXHttpRequestCallBack)completion;

-(BOOL)isValidateEmail:(NSString *)email;

- (void)login_get_version:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion;
@end

NS_ASSUME_NONNULL_END
