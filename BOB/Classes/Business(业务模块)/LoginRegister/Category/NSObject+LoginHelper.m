//
//  NSObject+LoginHelper.m
//  RatelBrother
//
//  Created by mac on 2019/5/24.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "NSObject+LoginHelper.h"
#import "SRWebSocketManager.h"
#import "UserModel.h"
#import "LcwlChat.h"
#import "LcwlChatHelper.h"
#import "AppDelegate+Helper.h"
#import "MXRemotePush.h"
#import "TCAccountMgrModel.h"
#import "HUDHelper.h"
#import "TCRoomListModel.h"
#import "MLVBLiveRoom.h"
#import "TXLiteAVSDKManage.h"

@implementation NSObject (LoginHelper)

- (void)createImgCode:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/api/common/imgCode/createImgCode",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary *tempDic = (NSDictionary*)data;
            if ([tempDic[@"success"] boolValue]) {
                completion(tempDic[@"data"],nil);
            }else{
                completion(nil,tempDic[@"msg"]);
            }
        }).failure(^(id error){
            completion(nil,error);
        })
        .execute();
    }];
}

- (void)send_code:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/api/common/smsCode/sendSmsCodeOnly",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt()
        .params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,nil);
            }else{
                completion(NO,nil);
            }
            [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil);

//            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

- (void)send_codeAccept:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/common/smsCode/sendSmsCodeAccept",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt()
        .params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,nil);
            }else{
                completion(NO,nil);
            }
            [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil);

//            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

- (void)send_codeToken:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/api/common/smsCode/sendSmsCodeToken",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt()
        .params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,nil);
            }else{
                completion(NO,nil);
            }
            [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil);

//            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

/*
 {
 code = "code_999999";
 data =     {
 "sys_user_account" = Hao;
 token = "25486|XOZHIEBJM3SKLOU3H7CBRZQ2638TU6WB";
 };
 msg = "\U64cd\U4f5c\U6210\U529f";
 success = 1;
 }*/
- (void)register1:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/api/user/login/userRegisterFirst",LcwlServerRoot];
    NSString *version_no = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] systemVersion]];
    NSMutableDictionary* muParam = [[NSMutableDictionary alloc] initWithDictionary:param];
    [muParam setObject:@"iOS" forKey:@"device_type"];
    [muParam setObject:version_no?:@"1.0.1" forKey:@"version_no"];
    [muParam setObject:[DeviceManager getAppBundleVersion] forKey:@"app_version"];
    NSString *UUIDKey = @"KJTZ-UUID";
    NSString *UUIDString = [MXCache getContentFromKeyChain:UUIDKey];
    if ([StringUtil isEmpty:UUIDString]) {
        NSUUID *UUID = [NSUUID UUID];
        UUIDString = UUID.UUIDString;
        [MXCache saveContentToKeyChain:UUIDString forKey:UUIDKey];
    }
    [muParam setObject:UUIDString?:@"" forKey:@"device_no"];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(muParam).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(dict,nil);
            }else{
                completion(nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            completion(nil,nil);
        })
        .execute();
    }];
}

- (void)register2:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion{
    NSMutableDictionary* muParam = [[NSMutableDictionary alloc] initWithDictionary:param];
    NSString *version_no = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] systemVersion]];
    [muParam setObject:@"iOS" forKey:@"device_type"];
    [muParam setObject:version_no?:@"1.0.1" forKey:@"version_no"];
    [muParam setObject:[DeviceManager getAppBundleVersion] forKey:@"app_version"];
    NSString *UUIDKey = @"KJTZ-UUID";
    NSString *UUIDString = [MXCache getContentFromKeyChain:UUIDKey];
    if ([StringUtil isEmpty:UUIDString]) {
        NSUUID *UUID = [NSUUID UUID];
        UUIDString = UUID.UUIDString;
        [MXCache saveContentToKeyChain:UUIDString forKey:UUIDKey];
    }
    [muParam setObject:UUIDString?:@"" forKey:@"device_no"];
    NSString *url = [NSString stringWithFormat:@"%@/api/user/login/userRegisterSecond",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(muParam).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                NSDictionary* dataDict = dict[@"data"];
                completion(YES,dataDict,nil);
            }else{
                completion(NO,nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            completion(NO,nil,nil);
        })
        .execute();
    }];
}

- (void)checkUserExist:(NSString *)phone completion:(RequestResultObjectCallBack)completion {
    [self request:@"api/user/info/checkUserExist"
            param:@{@"user_account":phone}
       completion:^(BOOL success, id object, NSString *error) {
        if (completion) {
            completion(success, object, error);
        }
    }];
}

- (void)clearUserToken:(MXHttpRequestResultObjectCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/login/userLogOut",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(@{})
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                NSDictionary* dataDict = dict[@"data"];
                UDetail.user.token = @"";
                UDetail.user.chatToken = @"";
                [UDetail updateUserModel:UDetail.user];
                completion(YES,dataDict,nil);
            }else{
                completion(NO,nil,nil);
                //[NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            completion(NO,nil,nil);
        })
        .execute();
    }];
}

/*
 {
 code = "code_999999";
 data =     {
 "head_photo" = "defaultAvatar.png";
 "nick_name" = Who;
 "pay_password" = "";
 "qiniu_domain" = "http://cdn.rxhcn.com";
 "qr_code_url" = "http://www.proecosystem.com/html/rst.html?account=Who";
 "register_type" = 1;
 "sys_time" = 1562575524038;
 "sys_user_account" = Who;
 token = "25485|1EFFFTRQO4ZUVYY6XDEYH4JK0D8VMSG9";
 "user_email" = "";
 "user_id" = 25485;
 "user_tel" = 15818614416;
 };
 msg = "\U64cd\U4f5c\U6210\U529f";
 success = 1;
 }
 */
- (void)login:(NSDictionary*)param superView:(UIView *)superView completion:(MXHttpRequestCallBack)completion {
    [NotifyHelper showHUDAddedTo:superView animated:YES];
    NSString *version_no = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] systemVersion]];
    NSMutableDictionary* muParam = [[NSMutableDictionary alloc] initWithDictionary:param];
    [muParam setObject:@"iOS" forKey:@"device_type"];
    [muParam setObject:version_no?:@"1.0.1" forKey:@"version_no"];
    [muParam setObject:[DeviceManager getAppBundleVersion] forKey:@"app_version"];
    NSString *UUIDKey = @"KJTZ-UUID";
    NSString *UUIDString = [MXCache getContentFromKeyChain:UUIDKey];
    if ([StringUtil isEmpty:UUIDString]) {
        NSUUID *UUID = [NSUUID UUID];
        UUIDString = UUID.UUIDString;
        [MXCache saveContentToKeyChain:UUIDString forKey:UUIDKey];
    }
    [muParam setObject:UUIDString?:@"" forKey:@"device_no"];
//        [muParam setObject:[MXRemotePush sharedInstance].voipToken?:@"" forKey:@"device_no2"];
    #ifdef DEBUG
        [muParam setObject:@"debug" forKey:@"envir_type"];
    #else
        [muParam setObject:@"release" forKey:@"envir_type"];
    #endif
    NSString *url = [NSString stringWithFormat:@"%@/api/user/login/userLogin",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(muParam)
        .finish(^(id object){
            Block_Exec_Main_Async_Safe(^{
                [NotifyHelper hideHUDForView:superView animated:YES];
            });
            NSDictionary *dict = (NSDictionary*)object;
            if ([dict[@"success"] boolValue]) {
                [MoApp resetKickOffState];
                NSDictionary* dataDict = dict[@"data"];
                UserModel * model = [UserModel modelParseWithDict:dataDict];
                if (dataDict[@"user_tel"]) {
                    model.user_tel = [dataDict[@"user_tel"] description];
                }
                if (dataDict[@"token"]) {
                    model.token = [dataDict[@"token"] description];
                }
                if (dataDict[@"websocket_path"]) {
                    model.webscoket_path = [dataDict[@"websocket_path"] description];
                }
                if (dataDict[@"user_id"]) {
                    model.user_id = [dataDict[@"user_id"] description];
                }
                if (dataDict[@"qiniu_domain"]) {
                    model.qiniu_domain = [dataDict[@"qiniu_domain"] description];
                }
                if (dataDict[@"nick_name"]) {
                    model.nickname = [dataDict[@"nick_name"] description];
                }
                if (dataDict[@"head_photo"]) {
                    model.head_photo = [dataDict[@"head_photo"] description];
                }
                if (dataDict[@"audio_path"]) {
                    model.jitsiServer = [dataDict[@"audio_path"] description];
                }
                if (dataDict[@"qr_code_url"]) {
                    model.qr_code_url = [dataDict[@"qr_code_url"] description];
                }
                if (dataDict[@"uid"]) {
                    model.user_uid = [dataDict[@"uid"] description];
                }
                if (dataDict[@"is_pclogin"]) {
                    model.client_status = [dataDict[@"is_pclogin"] boolValue];
                }
                if (dataDict[@"give_status"]) {
                    model.give_status = [dataDict[@"give_status"] boolValue];
                }
                if (dataDict[@"give_sla_num"]) {
                    model.give_sla_num = [dataDict[@"give_sla_num"] description];
                }
                //如果是token登录, 需要把chatToken保存起来
                if ([@"token" isEqualToString:muParam[@"login_type"]?:@""]) {
                    model.chatToken = UDetail.user.chatToken;
                }
                if ([StringUtil isEmpty:dataDict[@"pay_password"]]) {
                    model.isSetPayPassword = NO;
                } else {
                    model.isSetPayPassword = YES;
                }
                [UDetail updateUserModel:model];
                [self createUserToken:@{@"sys_system_type":@"mallLive",
                                        @"system_user_id":model.user_id?:@"",
                                        @"chat_no":model.user_uid?:@"",
                                        @"head_photo":model.head_photo?:@"",
                                        @"login_type":muParam[@"login_type"]?:@"account",
                                        @"nick_name":model.nickname?:@"",
                                        @"user_tel":model.user_tel,
                                        @"sex":model.sex?:@"1",
                } superView:nil
                           completion:^(BOOL success, id object, NSString *error) {
                    if (success) {
                        UserModel *user = UDetail.user;
                        NSDictionary *dataDic = object[@"data"];
                        if (dataDic[@"chat_user_id"]) {
                            user.chatUser_id = [dataDic[@"chat_user_id"] description];
                        }
                        if (dataDic[@"websocket_path"]) {
                            user.webscoket_path = [dataDic[@"websocket_path"] description];
                        }
                        if (dataDic[@"chat_token"]) {
                            user.chatToken = [dataDic[@"chat_token"] description];
                        }
                        [UDetail updateUserModel:user];
                        //更新token到服务器
                        [[MXRemotePush sharedInstance] registerDeviceTokenToServer];
                        //先拉离线消息 不连socket
                        [SRWebSocketManager sharedInstance].isConnect = NO;
                        [[LcwlChat shareInstance] chatLogin:user];
                        //拉取离线消息
                        [[LcwlChat shareInstance].chatManager fetchOfflineMessageWithBatchNo:@""
                                                                                    complete:^{
                            Block_Exec_Main_Async_Safe(^{
                                [NotifyHelper hideHUDForView:superView animated:YES];
                            });
                        }];
                        
                        //请求好友列表
                        [LcwlChatHelper requestFriends];
                    }
                }];
                
                [[NSUserDefaults standardUserDefaults] setValue:[object valueForKeyPath:@"data.qiniu_domain"] forKey:@"qiniu_domain"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [UDetail get_userinfo:nil];
                [UDetail updateWallet:nil];
                [self loginLiveServer];
                completion(dataDict,nil);
            }else{
                completion(nil,nil);
                if(![dict[@"msg"] isEqualToString:@"token有误"]) {
                    [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
                }
                [NotifyHelper hideHUDForView:superView animated:YES];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if ([MoApp respondsToSelector:NSSelectorFromString(@"loginOutLogic")]) {
                         [MoApp performSelector:NSSelectorFromString(@"loginOutLogic") withObject:nil afterDelay:0];
                     }
                 });
            }
        }).failure(^(id error){
            completion(nil,error);
        })
        .execute();
    }];
}

///注册小直播后台
- (void)registerFromTCServer {
    // 用户名密码注册
    __weak typeof(self) weakSelf = self;
    [[HUDHelper sharedInstance] syncLoading];
    [[TCAccountMgrModel sharedInstance] registerWithUsername:UDetail.user.zhibo_account password:UDetail.user.zhibo_password succ:^(NSString *userName, NSString *md5pwd) {
        // 注册成功后直接登录
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TCAccountMgrModel sharedInstance] loginWithUsername:UDetail.user.zhibo_account password:UDetail.user.zhibo_password succ:^(NSString *userName, NSString *md5pwd) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[HUDHelper sharedInstance] syncStopLoading];
                    
//                    __strong typeof(weakSelf) self = weakSelf;
//                    if (self) {
//                        [self.delegate loginSuccess:userName hashedPwd:md5pwd];
//                    }
                    
//                    [self request:@"" param:@{} completion:^(BOOL success, id object, NSString *error) {
//
//                    }];
                });
            } fail:^(int errCode, NSString *errMsg) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[HUDHelper sharedInstance] syncStopLoading];
                    [HUDHelper alertTitle:@"登录失败" message:errMsg cancel:@"确定"];
                    NSLog(@"%s %d %@", __func__, errCode, errMsg);
                });
            }];
        });
        
    } fail:^(int errCode, NSString *errMsg) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[HUDHelper sharedInstance] syncStopLoading];
            //[HUDHelper alertTitle:@"注册失败" message:errMsg cancel:@"确定"];
            NSLog(@"%s %d %@", __func__, errCode, errMsg);
        });
    }];
}

- (void)loginLiveServer {
    [TCRoomListMgr sharedMgr].liveRoom = [MLVBLiveRoom sharedInstance];
    [[TCAccountMgrModel sharedInstance] loginWithUsername:(UDetail.user.zhibo_account/*@"alphago2"*/) password:UDetail.user.zhibo_password succ:^(NSString *userName, NSString *md5pwd) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[HUDHelper sharedInstance] syncStopLoading];
            
//            __strong typeof(weakSelf) self = weakSelf;
//            if (self) {
//                [self loginSuccess:userName hashedPwd:md5pwd];
//            }
        });

    } fail:^(int errCode, NSString *errMsg) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //[[HUDHelper sharedInstance] syncStopLoading];
            //[HUDHelper alertTitle:@"登录失败" message:errMsg cancel:@"确定"];
            
            NSLog(@"%s %d %@", __func__, errCode, errMsg);
            
            if(errCode == -1) { //用户不存在，去注册
                [self registerFromTCServer];
            } else {
                [NotifyHelper showMessageWithMakeText:errMsg];
            }
        });
    }];
}


- (void)createUserToken:(NSDictionary*)param superView:(UIView *)superView completion:(MXHttpRequestResultObjectCallBack)completion{
//    NSString *version_no = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] systemVersion]];
//    NSMutableDictionary* muParam = [[NSMutableDictionary alloc] initWithDictionary:param];
//    [muParam setObject:@"iOS" forKey:@"device_type"];
//    [muParam setObject:version_no?:@"1.0.1" forKey:@"version_no"];
//    [muParam setObject:APPSHORTVERSION forKey:@"app_version"];
//
    NSString *url = [NSString stringWithFormat:@"%@/api/user/info/createUserToken",LcwlServerRoot2];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data) {
            completion([data[@"success"] boolValue],data,nil);
        }).failure(^(id error) {
            completion(NO,nil,nil);
        })
        .execute();
    }];
}

///发送短信验证码接口（二：RSA加密方式+token验证）
- (void)sendSmsCodeToken:(NSDictionary*)param completion:(MXHttpRequestCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/api/common/smsCode/sendSmsCodeToken",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.useEncrypt()
        .apiUrl(url).params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,nil);
            }else{
                completion(NO,nil);
            }
            [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(NO,nil);

            //[NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

- (void)userForgetPass:(NSDictionary*)param completion:(MXHttpRequestResultObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/api/user/login/userForgetPass",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param).useMsg()
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(YES,dict,nil);
            }else{
                completion(NO,nil,nil);
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
            }
        }).failure(^(id error){
            completion(NO,nil,nil);
        })
        .execute();
    }];
}

- (void)userResetPass:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/api/user/login/userResetPass",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                completion(@"success",nil);
            }else{
                completion(nil,nil);
            }
            
            [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
        }).failure(^(id error){
            completion(nil,nil);
        })
        .execute();
    }];
}

- (void)userLogOut:(NSDictionary*)param view:(UIView *)superV completion:(MXHttpRequestCallBack)completion {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/login/userLogOut",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param).useHud(superV)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {

                completion(YES,nil);
            }else{
                completion(NO,nil);
            }

        }).failure(^(id error){
            completion(NO,nil);
        })
        .execute();
    }];
}


-(BOOL)isValidateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",emailRegex];
    return [emailTest evaluateWithObject:email];
}



- (void)login_get_version:(NSDictionary*)param completion:(MXHttpRequestObjectCallBack)completion{
    NSString *url = [NSString stringWithFormat:@"%@/login/get_version",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(param)
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                dict = dict[@"data"];
                completion(dict,nil);
            }else{
                completion(nil,nil);
            }

        }).failure(^(id error){
            completion(nil,nil);
        })
        .execute();
    }];
}
@end
