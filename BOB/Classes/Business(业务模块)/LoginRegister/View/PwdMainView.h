//
//  PwdMainView.h
//  RatelBrother
//
//  Created by mac on 2019/5/24.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LoginRegModel;

NS_ASSUME_NONNULL_BEGIN

@interface PwdMainView : UIView<LanguageDelegate>

@property (nonatomic, strong) dispatch_block_t loginBlock;

@property (nonatomic, strong) dispatch_block_t forgetBlock;

-(void)configModel:(LoginRegModel*)model;

+(NSInteger)getHeight;

@end

NS_ASSUME_NONNULL_END
