//
//  RegisterMainView.h
//  RatelBrother
//
//  Created by mac on 2019/5/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class LoginRegModel;

@interface RegisterMainView : UIView<LanguageDelegate>

@property (nonatomic, strong) dispatch_block_t regBlock;

@property (nonatomic, strong) dispatch_block_t loginBlock;

@property (nonatomic, strong) FinishedBlock nextBlock;

-(void)configModel:(LoginRegModel*)model;

-(void)switchClick;

+(int)getHeight;

@end

NS_ASSUME_NONNULL_END
