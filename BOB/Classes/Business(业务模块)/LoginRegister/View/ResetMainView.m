//
//  LoginMainView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "ResetMainView.h"
#import "PhoneNumView.h"
#import "ImgCaptchaView.h"
#import "LoginRegModel.h"
#import "NSObject+LoginHelper.h"
#import "InvitationCodeView.h"
#import "TextCaptchaView.h"
#import "NSString+NumFormat.h"
static NSInteger rowPadding = 10;
static NSInteger padding = 15;
static NSInteger height = 50;
@interface ResetMainView()

@property (nonatomic,strong) InvitationCodeView  *pwdView;

@property (nonatomic,strong) InvitationCodeView  *rePwdView;

@property (nonatomic,strong) UIButton        *submitBtn;

@property (nonatomic,strong) LoginRegModel   *model;

@end

@implementation ResetMainView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        self.tag = 998877;
        [self initUI];
    }
    return self;
}

-(void)initUI{
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.pwdView];
    [self addSubview:self.rePwdView];
    [self addSubview:self.submitBtn];
    [self layout];
}

-(void)layout{
    @weakify(self)
    [self.pwdView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(padding);
        make.right.equalTo(self.mas_right).offset(-padding);
        make.height.equalTo(@(height));
        make.top.equalTo(self).offset(padding);
    }];
    
    [self.rePwdView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.pwdView);
        make.height.equalTo(@(height));
        make.top.equalTo(self.pwdView.mas_bottom);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.mas_centerX);
        make.height.equalTo(@(44));
        make.width.equalTo(@(180));
        make.top.equalTo(self.rePwdView.mas_bottom).offset(130);
    }];
    
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitBtn.layer.masksToBounds = YES;
        _submitBtn.layer.cornerRadius = 22;
        [_submitBtn setTitle:Lang(@"下一步") forState:UIControlStateNormal];
        _submitBtn.enabled = NO;
        _submitBtn.alpha = 0.5;
        _submitBtn.clipsToBounds =NO;
        [_submitBtn addTarget:self action:@selector(goTo:) forControlEvents:UIControlEventTouchUpInside];
        [_submitBtn setBackgroundColor:[UIColor moBlueColor]];
    }
    return _submitBtn;
}


-(void)goTo:(id)sender{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    if (![self.model.password isEqualToString:self.model.rePassword]) {
        [NotifyHelper showMessageWithMakeText:@"密码不一致"];
        return;
    } else if(![self.model.password checkPasswordValidate]) {
        return;
    }
    [self userResetPass:@{@"sys_user_account":self.model.userName,@"user_account":self.model.phoneNo,@"valid_flag":self.model.valid_flag,@"login_password":self.model.password} completion:^(id object, NSString *error) {
        if (object) {
            if (self.block) {
                self.block(nil);
            }
        }
    }];
}



-(void)switchTo:(LoginRegModel *)model{
    
}

-(void)configModel:(LoginRegModel*)model{
    self.model = model;
    [self.model addObserver:self
                 forKeyPath:@"password"
                    options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                    context:nil];
    [self.model addObserver:self
                 forKeyPath:@"rePassword"
                    options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                    context:nil];
}

/* KVO,只要object的keyPath属性发生变化，就会调用此函数*/
- (void)observeValueForKeyPath:(NSString *)keyP1ath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    // 控制提交按钮
    if (self.model.password.length > 0 &&
        self.model.rePassword.length > 0) {
            self.submitBtn.enabled = YES;
            self.submitBtn.alpha = 1;
            return;
        }
    self.submitBtn.enabled = NO;
    self.submitBtn.alpha = 0.5;
}

-(void)dealloc{
    [self.model removeObserver:self forKeyPath:@"password"];
    [self.model removeObserver:self forKeyPath:@"rePassword"];
}

-(InvitationCodeView* )pwdView{
    if (!_pwdView) {
        _pwdView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
        [_pwdView reloadImg:@"密码" placeHolder:Lang(@"新的登录密码")];
        [_pwdView setValue:@(YES) forKeyPath:@"tf.secureTextEntry"];
        @weakify(self)
        _pwdView.getText = ^(id data) {
            @strongify(self)
            self.model.password = data;
        };
    }
    return _pwdView;
}


-(InvitationCodeView* )rePwdView{
    if (!_rePwdView) {
        _rePwdView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
        [_rePwdView reloadImg:@"密码" placeHolder:Lang(@"确认密码")];
        [_rePwdView setValue:@(YES) forKeyPath:@"tf.secureTextEntry"];;
        @weakify(self)
        _rePwdView.getText = ^(id data) {
            @strongify(self)
            self.model.rePassword = data;
        };
    }
    return _rePwdView;
}

@end
