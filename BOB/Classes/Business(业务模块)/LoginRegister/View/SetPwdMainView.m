//
//  LoginMainView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "SetPwdMainView.h"
#import "PhoneNumView.h"
#import "ImgCaptchaView.h"
#import "CipherView.h"
#import "LoginRegModel.h"
#import "InvitationCodeView.h"
#import "RegisterModel.h"
#import "NSObject+LoginHelper.h"
static NSInteger height = 44;
static NSInteger rowPadding = 10;
@interface SetPwdMainView()

@property (nonatomic,strong) CipherView          *pwdView1;

@property (nonatomic,strong) CipherView          *pwdView2;

@property (nonatomic,strong) CipherView          *pwdView3;

@property (nonatomic,strong) CipherView          *pwdView4;

@property (nonatomic,strong) InvitationCodeView  *inviteCodeView;

@property (nonatomic,strong) UIButton            *submitBtn;

@property (nonatomic,strong) LoginRegModel       *model;

@end

@implementation SetPwdMainView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        self.tag = 998877;
        [self initUI];
    }
    return self;
}

-(void)initUI{
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.pwdView1];
    [self addSubview:self.pwdView2];
    [self addSubview:self.pwdView3];
    [self addSubview:self.pwdView4];
    [self addSubview:self.inviteCodeView];
    [self addSubview:self.submitBtn];
    [self layout];
}

-(void)layout{
    @weakify(self)
    [self.pwdView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.left.right.equalTo(self);
        make.height.equalTo(@(height));
    }];
    
    [self.pwdView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.height.equalTo(@(height));
        make.top.equalTo(self.pwdView1.mas_bottom).offset(rowPadding);
    }];
    
    [self.pwdView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.height.equalTo(@(height));
        make.top.equalTo(self.pwdView2.mas_bottom).offset(rowPadding);
    }];
    
    [self.pwdView4 mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.height.equalTo(@(height));
        make.top.equalTo(self.pwdView3.mas_bottom).offset(rowPadding);
    }];
    
    [self.inviteCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.height.equalTo(@(height));
        make.top.equalTo(self.pwdView4.mas_bottom).offset(rowPadding);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.height.equalTo(@(44));
        make.top.equalTo(self.inviteCodeView.mas_bottom).offset(35);
    }];
}

-(CipherView* )pwdView1{
    if (!_pwdView1) {
        _pwdView1 = [CipherView new];
        [_pwdView1 setTitleWidth:80];
        [_pwdView1 reloadTitle:Lang(@"登录密码") placeHolder:Lang(@"请输入登录密码")];
        @weakify(self)
        _pwdView1.getText = ^(id data) {
            @strongify(self)
            self.model.password = data;
        };
    }
    return _pwdView1;
}

-(CipherView* )pwdView2{
    if (!_pwdView2) {
        _pwdView2 = [CipherView new];
        [_pwdView2 setTitleWidth:80];
        [_pwdView2 reloadTitle:Lang(@"确认登录密码") placeHolder:Lang(@"请再次输入登录密码")];
        @weakify(self)
        _pwdView2.getText = ^(id data) {
            @strongify(self)
            self.model.confirm_password = data;
        };
    }
    return _pwdView2;
}

-(CipherView* )pwdView3{
    if (!_pwdView3) {
        _pwdView3 = [CipherView new];
        _pwdView3.tf.keyboardType = UIKeyboardTypeNumberPad;
        [_pwdView3 setTitleWidth:80];
        [_pwdView3 reloadTitle:Lang(@"交易密码") placeHolder:Lang(@"请输入交易密码")];
        @weakify(self)
        _pwdView3.getText = ^(id data) {
            @strongify(self)
            self.model.tradePwd = data;
        };
    }
    return _pwdView3;
}

-(CipherView* )pwdView4{
    if (!_pwdView4) {
        _pwdView4 = [CipherView new];
        _pwdView4.tf.keyboardType = UIKeyboardTypeNumberPad;
        [_pwdView4 setTitleWidth:80];
        [_pwdView4 reloadTitle:Lang(@"确认交易密码") placeHolder:Lang(@"请再次输入交易密码")];
        @weakify(self)
        _pwdView4.getText = ^(id data) {
            @strongify(self)
            self.model.confirm_tradePwd = data;
        };
    }
    return _pwdView4;
}

-(InvitationCodeView* )inviteCodeView{
    if (!_inviteCodeView) {
        _inviteCodeView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
        [_inviteCodeView setTitleWidth:80];
        [_inviteCodeView reloadTitle:Lang(@"邀请码") placeHolder:@"请输入邀请码"];
        _inviteCodeView.hidden = YES;
        @weakify(self)
        _inviteCodeView.getText = ^(id data) {
            @strongify(self)
            self.model.inviteCode = data;
        };
    }
    return _inviteCodeView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitBtn.layer.masksToBounds = YES;
        _submitBtn.layer.cornerRadius = 22;
        _submitBtn.enabled = NO;
        _submitBtn.alpha = 0.5;
        [_submitBtn setTitle:Lang(@"注册") forState:UIControlStateNormal];
        _submitBtn.clipsToBounds = NO;
        [_submitBtn addTarget:self action:@selector(regClick:) forControlEvents:UIControlEventTouchUpInside];
        [_submitBtn az_setGradientBackgroundWithColors:@[[UIColor colorWithHexString:@"#154AE1"],[UIColor colorWithHexString:@"#067BE9"]] locations:nil startPoint:CGPointMake(1, 0) endPoint:CGPointMake(0, 0)];
    }
    return _submitBtn;
}

-(void)regClick:(id)sender{
    if(![self.pwdView1.tf.text isEqualToString:self.pwdView2.tf.text]) {
        [NotifyHelper showMessageWithMakeText:@"登录密码不一致"];
        return;
    } else if(![self.pwdView3.tf.text isEqualToString:self.pwdView4.tf.text]) {
        [NotifyHelper showMessageWithMakeText:@"交易密码不一致"];
        return;
    }
    NSDictionary *param = @{@"password":self.pwdView1.tf.text ?: @"",@"pay_password":self.pwdView3.tf.text ?: @"",/*@"pid":self.inviteCodeView.tf.text ?: @""*/};
    !_regBlock?:_regBlock(param);
}

-(void)configModel:(LoginRegModel*)model{
    self.model = model;
    [self.model addObserver:self
                 forKeyPath:@"password"
                    options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                    context:nil];
    [self.model addObserver:self
                 forKeyPath:@"confirm_password"
                    options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                    context:nil];
    [self.model addObserver:self
                 forKeyPath:@"tradePwd"
                    options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                    context:nil];
    [self.model addObserver:self
                 forKeyPath:@"confirm_tradePwd"
                    options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                    context:nil];
    [self.model addObserver:self
                 forKeyPath:@"inviteCode"
                    options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                    context:nil];
}

/* KVO,只要object的keyPath属性发生变化，就会调用此函数*/
- (void)observeValueForKeyPath:(NSString *)keyP1ath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
        // 控制提交按钮
    if (self.model.password.length > 0 &&
        self.model.confirm_password.length > 0&&
        self.model.tradePwd.length > 0 &&
        self.model.confirm_tradePwd.length > 0
        /*&&self.model.inviteCode.length > 0*/) {
        self.submitBtn.enabled = YES;
        self.submitBtn.alpha = 1;
        return;
    }
    self.submitBtn.enabled = NO;
    self.submitBtn.alpha = 0.5;
}

-(void)dealloc{
    [self.model removeObserver:self forKeyPath:@"password"];
    [self.model removeObserver:self forKeyPath:@"confirm_password"];
    [self.model removeObserver:self forKeyPath:@"tradePwd"];
    [self.model removeObserver:self forKeyPath:@"confirm_tradePwd"];
    [self.model removeObserver:self forKeyPath:@"inviteCode"];
}

-(void)updateForLanguageChanged{
    
}

+(NSInteger)getHeight{
    return 350;
}
@end
