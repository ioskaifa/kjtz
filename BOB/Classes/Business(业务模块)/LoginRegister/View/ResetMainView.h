//
//  ResetMainView.h
//  BOB
//
//  Created by mac on 2019/6/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LoginRegModel;
NS_ASSUME_NONNULL_BEGIN

@interface ResetMainView : UIView

@property (nonatomic , strong) FinishedBlock getText;

@property (nonatomic, strong) FinishedBlock block;

-(void)configModel:(LoginRegModel*)model;


@end

NS_ASSUME_NONNULL_END
