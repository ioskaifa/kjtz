//
//  ForgetPwdView.h
//  BOB
//
//  Created by mac on 2019/6/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class LoginRegModel;
@interface ForgetPwdView : UIView

@property (nonatomic, strong) FinishedBlock block;

-(void)configModel:(LoginRegModel*)model;

+(NSInteger)getHeight;

@end

NS_ASSUME_NONNULL_END
