//
//  AuthorizationFailView.m
//  BOB
//
//  Created by mac on 2020/7/4.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AuthorizationFailView.h"

@interface AuthorizationFailView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *tip1Lbl;

@property (nonatomic, strong) UILabel *tip2Lbl;

@end

@implementation AuthorizationFailView

- (instancetype)init {
    if (self = [super init]) {
        [self createUI];
    }
    return self;
}

- (void)setFailType:(AuthorizationFailType)failType {
    _failType = failType;
    NSString *tip1 = @"";
    NSString *tip2 = @"";
    NSString *imgName = @"";
    switch (_failType) {
        case AuthorizationFailTypeLogin:{
            tip1 = @"登录失败";
            tip2 = @"还未授权，请耐心等待";
            imgName = @"登录_未授权";
            break;
        }
        case AuthorizationFailTypeRegist:{
            tip1 = @"恭喜您，注册成功";
            tip2 = @"等待授权";
            imgName = @"注册成功_未授权";
            break;
        }
        default:
            tip1 = @"登录失败";
            tip2 = @"还未授权，请耐心等待";
            imgName = @"登录_未授权";
            break;
    }
    self.tip1Lbl.text = tip1;
    [self.tip1Lbl sizeToFit];
    self.tip1Lbl.mySize = self.tip1Lbl.size;
    
    self.tip2Lbl.text = tip2;
    [self.tip2Lbl sizeToFit];
    self.tip2Lbl.mySize = self.tip2Lbl.size;
    
    self.imgView.image = [UIImage imageNamed:imgName];
}

#pragma mark - InitUI
- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self addSubview:layout];
    
    [layout addSubview:self.imgView];
    [layout addSubview:self.tip1Lbl];
    [layout addSubview:self.tip2Lbl];
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.image = [UIImage imageNamed:@"登录_未授权"];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myTop = 25;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)tip1Lbl {
    if (!_tip1Lbl) {
        _tip1Lbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor whiteColor];
            object.font = [UIFont font24];
            object.text = @"登录失败";
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myTop = 25;
            object;
        });
    }
    return _tip1Lbl;
}

- (UILabel *)tip2Lbl {
    if (!_tip2Lbl) {
        _tip2Lbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor whiteColor];
            object.font = [UIFont font24];
            object.text = @"还未授权，请耐心等待";
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myTop = 25;
            object;
        });
    }
    return _tip2Lbl;
}

@end
