//
//  LoginBottomView.m
//  BOB
//
//  Created by mac on 2019/12/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "LoginBottomView.h"
#import "SPButton.h"
@interface LoginBottomView()

@property (nonatomic,strong) MXSeparatorLine* leftLine;

@property (nonatomic,strong) MXSeparatorLine* rightLine;

@property (nonatomic,strong) UILabel* tipLbl;

@property (nonatomic,strong) SPButton* wechatBtn;

@end

@implementation LoginBottomView

-(instancetype)init{
    if (self = [super init]) {
        [self setupUI];
    }
    return self;
}

-(void)setupUI{
    [self addSubview:self.leftLine];
    [self addSubview:self.rightLine];
    [self addSubview:self.tipLbl];
    [self addSubview:self.wechatBtn];
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.centerX.equalTo(self);
    }];
    [self.leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.tipLbl.mas_left).offset(-10);
        make.centerY.equalTo(self.tipLbl);
        make.width.equalTo(@(50));
        make.height.equalTo(@(0.5));
    }];
    [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tipLbl.mas_right).offset(10);
        make.centerY.equalTo(self.tipLbl);
        make.width.equalTo(@(50));
        make.height.equalTo(@(0.5));
    }];
    [self.wechatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.tipLbl.mas_bottom).offset(10);
    }];
}

-(MXSeparatorLine*)leftLine{
    if (!_leftLine) {
        _leftLine = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    }
    return _leftLine;
}

-(MXSeparatorLine*)rightLine{
    if (!_rightLine) {
        _rightLine = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    }
    return _rightLine;
}

-(UILabel*)tipLbl{
    if (!_tipLbl) {
        _tipLbl = [UILabel new];
        _tipLbl.font = [UIFont font12];
        _tipLbl.textColor = [UIColor moPlaceHolder];
        _tipLbl.text = Lang(@"第三方登录");
    }
    return _tipLbl;
}

- (SPButton *)wechatBtn{
    if (!_wechatBtn) {
        _wechatBtn = [[SPButton alloc] initWithImagePosition:SPButtonImagePositionLeft];
        _wechatBtn.frame = CGRectMake(0, 0, 100, 25);
        [_wechatBtn setTitle:Lang(@"微信登录") forState:UIControlStateNormal];
        [_wechatBtn setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
        _wechatBtn.titleLabel.font = [UIFont font14];
        [_wechatBtn setImage:[UIImage imageNamed:@"微信"] forState:UIControlStateNormal];
        [_wechatBtn setImageTitleSpace:12];
        [_wechatBtn addTarget:self action:@selector(wechatLogin:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _wechatBtn;
}

-(void)wechatLogin:(id)sender{
    !_wechatBlock?:_wechatBlock();
}
@end
