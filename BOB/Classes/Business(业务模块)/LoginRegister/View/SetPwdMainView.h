//
//  SetPwdMainView.h
//  BOB
//
//  Created by mac on 2019/12/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginRegModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SetPwdMainView : UIView

@property (nonatomic,strong) FinishedBlock regBlock;

-(void)configModel:(LoginRegModel*)model;

+(NSInteger)getHeight;

@end

NS_ASSUME_NONNULL_END
