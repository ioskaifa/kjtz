//
//  LoginMainView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "ForgetPwdView.h"
#import "PhoneNumView.h"
#import "ImgCaptchaView.h"
#import "LoginRegModel.h"
#import "NSObject+LoginHelper.h"
#import "InvitationCodeView.h"
#import "TextCaptchaView.h"
#import "CipherView.h"
static NSInteger rowPadding = 10;
static NSInteger height = 44;
@interface ForgetPwdView()

@property (nonatomic,strong) InvitationCodeView  *regAccountView;

@property (nonatomic,strong) InvitationCodeView  *accountView;

@property (nonatomic,strong) TextCaptchaView     *codeView;

@property (nonatomic,strong) CipherView          *pwdView1;

@property (nonatomic,strong) CipherView          *pwdView2;

@property (nonatomic,strong) UIButton            *submitBtn;

@property (nonatomic,strong) LoginRegModel       *model;

@end

@implementation ForgetPwdView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        self.tag = 998877;
        [self initUI];
        
    }
    return self;
}

-(void)initUI{
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.regAccountView];
    [self addSubview:self.codeView];
    [self addSubview:self.pwdView1];
    [self addSubview:self.pwdView2];
    [self addSubview:self.submitBtn];
    [self layout];
}

-(void)layout{
    @weakify(self)
    [self.regAccountView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self.mas_left).offset(15);
        make.right.equalTo(self.mas_right).offset(-15);
        make.height.equalTo(@(height));
        make.top.equalTo(self);
    }];

    [self.codeView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.regAccountView);
        make.height.equalTo(@(height));
        make.top.equalTo(self.regAccountView.mas_bottom).offset(rowPadding);
    }];

    [self.pwdView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.regAccountView);
        make.height.equalTo(@(height));
        make.top.equalTo(self.codeView.mas_bottom).offset(rowPadding);
    }];
    
    [self.pwdView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.regAccountView);
        make.height.equalTo(@(height));
        make.top.equalTo(self.pwdView1.mas_bottom).offset(rowPadding);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.height.equalTo(@(40));
        make.top.equalTo(self.pwdView2.mas_bottom).offset(35);
    }];
}

-(void)goTo:(id)sender{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [self userForgetPass:@{@"account":self.model.account,@"phone":self.model.regAccount,@"code":self.model.captcha,@"password":self.model.password} completion:^(BOOL result,id object, NSString *error) {
        if (result) {
            //self.model.valid_flag = object;
            //[MXRouter openURL:@"lcwl://ResetPwdVC" parameters:@{@"model":self.model} ];
            Block_Exec(self.block,nil);
        }
    }];
}

-(void)switchTo:(LoginRegModel *)model{

}

-(void)configModel:(LoginRegModel*)model{
    self.model = model;
}


-(InvitationCodeView* )regAccountView{
    if (!_regAccountView) {
        _regAccountView.backgroundColor = [UIColor whiteColor];
        _regAccountView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
        [_regAccountView setTitleWidth:60];
        [_regAccountView reloadTitle:@"手机号" placeHolder:@"请输入手机号"];
        @weakify(self)
        _regAccountView.getText = ^(id data) {
            @strongify(self)
            self.model.regAccount = data;
        };
    }
    return _regAccountView;
}

-(TextCaptchaView* )codeView{
    if (!_codeView) {
        _codeView = [[TextCaptchaView alloc]initWithFrame:CGRectZero];
        [_codeView setTitleWidth:60];
        [_codeView reloadTitle:@"验证码" placeHolder:@"请输入验证码"];
        @weakify(self)
        _codeView.getText = ^(id data) {
            @strongify(self)
            self.model.captcha = data;
        };
        _codeView.sendCodeBlock  = ^(id data) {
           @strongify(self)
           NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
           [param setValue:self.model.regAccount forKey:@"phone"];
           [param setValue:@(2) forKey:@"type"];
           [param setValue:self.model.account forKey:@"account"];
           [self send_code:param completion:^(BOOL success, NSString *error) {
               if (!success) {
                   
               }else{
                   [self.codeView startCountDowView];
               }
           }];
        };
    }
    return _codeView;
}

-(CipherView* )pwdView1{
    if (!_pwdView1) {
        _pwdView1 = [CipherView new];
        _pwdView1.backgroundColor = [UIColor whiteColor];
        [_pwdView1 setTitleWidth:60];
        [_pwdView1 reloadTitle:Lang(@"新密码") placeHolder:Lang(@"请输入新的登录密码")];
        @weakify(self)
        _pwdView1.getText = ^(id data) {
            @strongify(self)
            self.model.password = data;
        };
    }
    return _pwdView1;
}

-(CipherView* )pwdView2{
    if (!_pwdView2) {
        _pwdView2.backgroundColor = [UIColor whiteColor];
        _pwdView2 = [CipherView new];
        [_pwdView2 setTitleWidth:60];
        [_pwdView2 reloadTitle:Lang(@"确认密码") placeHolder:Lang(@"请再次输入登录密码")];
        @weakify(self)
        _pwdView2.getText = ^(id data) {
            @strongify(self)
            self.model.confirm_password = data;
        };
    }
    return _pwdView2;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.titleLabel.font = [UIFont font16];
        [_submitBtn setTitleColor:[UIColor colorWithHexString:@"#0088FF"] forState:UIControlStateNormal];
        [_submitBtn setTitle:Lang(@"确定") forState:UIControlStateNormal];
        [_submitBtn addTarget:self action:@selector(goTo:) forControlEvents:UIControlEventTouchUpInside];
         [_submitBtn setBackgroundColor:[UIColor whiteColor]];
    }
    return _submitBtn;
}

+(NSInteger)getHeight{
    return 250+88;
}
@end
