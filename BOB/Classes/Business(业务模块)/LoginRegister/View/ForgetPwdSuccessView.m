//
//  ForgetPwdSuccessView.m
//  BOB
//
//  Created by mac on 2020/7/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ForgetPwdSuccessView.h"

@interface ForgetPwdSuccessView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *tip1Lbl;

@property (nonatomic, strong) UILabel *tip2Lbl;

@end

@implementation ForgetPwdSuccessView

- (instancetype)init {
    if (self = [super init]) {
        [self createUI];
    }
    return self;
}

#pragma mark - InitUI
- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self addSubview:layout];
    
    [layout addSubview:self.imgView];
    [layout addSubview:self.tip1Lbl];
    [layout addSubview:self.tip2Lbl];
    [layout addSubview:self.loginBtn];
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.image = [UIImage imageNamed:@"icon_modify_succes"];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myTop = 25;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)tip1Lbl {
    if (!_tip1Lbl) {
        _tip1Lbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor blackColor];
            object.font = [UIFont font26];
            object.text = @"修改成功";
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myTop = 25;
            object;
        });
    }
    return _tip1Lbl;
}

- (UILabel *)tip2Lbl {
    if (!_tip2Lbl) {
        _tip2Lbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor blackColor];
            object.font = [UIFont font14];
            object.text = @"您的密码已经修改成功，请重新登录";
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myTop = 10;
            object;
        });
    }
    return _tip2Lbl;
}

- (UIButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = ({
            UIButton *object = [UIButton new];
            object.titleLabel.font = [UIFont font14];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setBackgroundColor:[UIColor colorWithHexString:@"#0088FF"]];
            [object setTitle:@"确定" forState:UIControlStateNormal];
            object.height = 34;
            [object setViewCornerRadius:object.height/2.0];
            object.myHeight = object.height;
            object.myTop = 60;
            object.myLeft = 15;
            object.myRight = 15;
            object.myBottom = 40;
            object;
        });
    }
    return _loginBtn;
}

@end
