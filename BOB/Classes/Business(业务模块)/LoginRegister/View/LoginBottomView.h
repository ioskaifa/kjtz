//
//  LoginBottomView.h
//  BOB
//
//  Created by mac on 2019/12/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginBottomView : UIView

@property (nonatomic, strong) dispatch_block_t wechatBlock;

@end

NS_ASSUME_NONNULL_END
