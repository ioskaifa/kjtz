//
//  LoginMainView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "PwdMainView.h"
#import "PhoneNumView.h"
#import "ImgCaptchaView.h"
#import "CipherView.h"
#import "LoginRegModel.h"
#import "InvitationCodeView.h"
#import "RegisterModel.h"
#import "NSObject+LoginHelper.h"
static NSInteger height = 44;
@interface PwdMainView()

@property (nonatomic,strong) InvitationCodeView  *userView;

@property (nonatomic,strong) CipherView          *pwdView;

@property (nonatomic,strong) UIButton            *submitBtn;

@property (nonatomic,strong) UIButton            *forgetBtn;

@property (nonatomic,strong) LoginRegModel       *model;

@end

@implementation PwdMainView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        self.tag = 998877;
        [self initUI];
    }
    return self;
}


- (void)configureSubmitBtnStatus {
    if ([StringUtil isEmpty:self.model.phoneNo]) {
        [self configureSubmitBtnDisable];
        return;
    }
    
    if ([StringUtil isEmpty:self.model.password]) {
        [self configureSubmitBtnDisable];
        return;
    }
    [self configureSubmitBtnEnable];
}

- (void)configureSubmitBtnEnable {
    self.submitBtn.userInteractionEnabled = YES;
    [self.submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.submitBtn.layer.borderColor = [UIColor colorWithHexString:@"#0088FF"].CGColor;
    self.submitBtn.layer.borderWidth = 1;
    [self.submitBtn setBackgroundColor:[UIColor colorWithHexString:@"#0088FF"]];
}

- (void)configureSubmitBtnDisable {
    self.submitBtn.userInteractionEnabled = NO;
    [self.submitBtn setTitleColor:[UIColor colorWithHexString:@"#AEAEAE"] forState:UIControlStateNormal];
    self.submitBtn.layer.borderColor = [UIColor colorWithHexString:@"#AEAEAE"].CGColor;
    self.submitBtn.layer.borderWidth = 1;
    [self.submitBtn setBackgroundColor:[UIColor moBackground]];
}

-(void)initUI{
    self.backgroundColor = [UIColor moBackground];
    [self addSubview:self.userView];
    [self.userView addBottomSingleLine:[UIColor colorWithHexString:@"#AEAEAE"]];
    [self addSubview:self.pwdView];
    [self.pwdView addBottomSingleLine:[UIColor colorWithHexString:@"#AEAEAE"]];
    [self addSubview:self.submitBtn];
    [self configureSubmitBtnStatus];
    [self addSubview:self.forgetBtn];
    [self layout];
}

-(void)layout{
    @weakify(self)
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.height.equalTo(@(height));
        make.top.equalTo(self);
    }];
    
    [self.pwdView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.userView);
        make.height.equalTo(@(height));
        make.top.equalTo(self.userView.mas_bottom).offset(10);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self);
        make.height.equalTo(@(34));
        make.top.equalTo(self.pwdView.mas_bottom).offset(60);
    }];
    
    [self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.submitBtn.mas_bottom).offset(10);
        make.size.mas_equalTo(self.forgetBtn.size);
        make.centerX.mas_equalTo(self);
    }];
}

-(InvitationCodeView* )userView{
    if (!_userView) {
        _userView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
        [_userView reloadTitle:@"账号" placeHolder:@"手机号"];
        _userView.tf.text = UDetail.user.user_tel;
        self.model.account = UDetail.user.user_tel;
        @weakify(self)
        _userView.getText = ^(id data) {
            @strongify(self)
            self.model.phoneNo = data;
            [self configureSubmitBtnStatus];
        };
    }
    return _userView;
}

-(CipherView* )pwdView{
    if (!_pwdView) {
        _pwdView = [CipherView new];
        [_pwdView reloadTitle:@"密码" placeHolder:@"请输入密码"];
        #if DEBUG
//        _pwdView.tf.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"login_password"];
//        self.model.password = _pwdView.tf.text;
        #endif
        @weakify(self)
        _pwdView.getText = ^(id data) {
            @strongify(self)
            self.model.password = data;
            [self configureSubmitBtnStatus];
        };
    }
    return _pwdView;
}

-(UIButton* )forgetBtn{
    if (!_forgetBtn) {
        _forgetBtn = ({
            UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.titleLabel.font = [UIFont font14];
            [btn setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
            [btn setTitle:Lang(@"忘记密码?") forState:UIControlStateNormal];
            [btn sizeToFit];
            btn;
        });
        [_forgetBtn addTarget:self action:@selector(forget:) forControlEvents:UIControlEventTouchUpInside];
        _forgetBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    return _forgetBtn;
}

-(void)forget:(id)sender{
    !_forgetBlock?:_forgetBlock();
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_submitBtn setViewCornerRadius:17];
        _submitBtn.titleLabel.font = [UIFont font16];
        [_submitBtn setTitle:Lang(@"登录") forState:UIControlStateNormal];
        [_submitBtn addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitBtn;
}

-(void)login:(id)sender{
    self.model.phoneNo = _userView.tf.text;
    self.model.password = _pwdView.tf.text;
    !_loginBlock?:_loginBlock();
}

-(void)configModel:(LoginRegModel*)model{
    self.model = model;
    _userView.tf.text = self.model.phoneNo;
    [self configureSubmitBtnStatus];
}

-(void)updateForLanguageChanged{
    
}

+(NSInteger)getHeight{
    return 250;
}
@end
