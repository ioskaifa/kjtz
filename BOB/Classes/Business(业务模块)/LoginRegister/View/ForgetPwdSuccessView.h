//
//  ForgetPwdSuccessView.h
//  BOB
//
//  Created by mac on 2020/7/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ForgetPwdSuccessView : UIView

@property (nonatomic, strong) UIButton *loginBtn;

@end

NS_ASSUME_NONNULL_END
