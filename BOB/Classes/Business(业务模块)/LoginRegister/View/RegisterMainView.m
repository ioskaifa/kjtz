//
//  LoginMainView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "RegisterMainView.h"
#import "PhoneNumView.h"
#import "TextCaptchaView.h"
#import "ImgCaptchaView.h"
#import "CipherView.h"
#import "LoginRegModel.h"
#import "InvitationCodeView.h"
#import "NSObject+LoginHelper.h"
#import "UIImage+Utils.h"
#import "UIButton+ActionBlock.h"
#import "NSString+NumFormat.h"
#import "NSMutableAttributedString+Attributes.h"
#import "NSAttributedString+YYText.h"
#import "YYLabel.h"
static NSInteger rowPadding = 10;
static NSInteger height = 44;
@interface RegisterMainView()

@property (nonatomic,strong) PhoneNumView    *numView;

@property (nonatomic,strong) InvitationCodeView  *emailView;

@property (nonatomic,strong) TextCaptchaView  *codeView;

@property (nonatomic,strong) InvitationCodeView  *userView;

@property (nonatomic,strong) UIButton        *submitBtn;

@property (nonatomic,strong) UIButton        *regBtn;

@property (nonatomic,strong) UIButton        *loginBtn;

@property (nonatomic,strong) LoginRegModel   *model;


@property (nonatomic,strong) YYLabel            *tipLbl;
@end

@implementation RegisterMainView

- (instancetype)initWithFrame:(CGRect)frame {
    self=[super initWithFrame:frame];
    if (self) {
        self.tag = 998877;
        [self initUI];
    }
    return self;
}

-(void)initUI{
    self.userInteractionEnabled = YES;
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.numView];
    [self addSubview:self.emailView];
    [self addSubview:self.codeView];
    [self addSubview:self.userView];
    [self addSubview:self.tipLbl];
    [self addSubview:self.submitBtn];
    [self addSubview:self.regBtn];
    [self addSubview:self.loginBtn];
    [self layout];
}

-(void)layout{
    @weakify(self)
    [self.numView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.left.right.equalTo(self);
        make.height.equalTo(@(height));
    }];
    
    [self.emailView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.left.right.equalTo(self);
        make.height.equalTo(@(height));
    }];
    
    [self.userView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.numView);
        make.height.equalTo(@(height));
        make.top.equalTo(self.numView.mas_bottom).offset(rowPadding);
    }];
    
    [self.codeView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.numView);
        make.height.equalTo(@(height));
        make.top.equalTo(self.userView.mas_bottom).offset(rowPadding);
    }];
    [self.tipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.codeView);
        make.top.equalTo(self.codeView.mas_bottom).offset(10);
    }];
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.right.equalTo(self.codeView);
        make.height.equalTo(@(44));
        make.top.equalTo(self.codeView.mas_bottom).offset(60);
    }];
    [self.regBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       @strongify(self)
       make.top.equalTo(self.submitBtn.mas_bottom).offset(rowPadding);
       make.left.equalTo(self.numView);
   }];
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.submitBtn.mas_bottom).offset(rowPadding);
        make.right.equalTo(self.numView);
    }];
}

+(int)getHeight{
    return 300;
}

-(PhoneNumView* )numView{
    if (!_numView) {
        _numView = [[PhoneNumView alloc]initWithFrame:CGRectZero];
        [_numView reloadTitle:Lang(@"手机号") placeholder:Lang(@"请输入您的注册手机号")];
        @weakify(self)
        _numView.getText = ^(id data) {
            @strongify(self)
            self.model.phoneNo = data;
        };
    }
    return _numView;
}

-(InvitationCodeView* )emailView{
    if (!_emailView) {
        _emailView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
        [_emailView reloadTitle:Lang(@"邮箱号") placeHolder:Lang(@"请输入您的注册邮箱号")];
        @weakify(self)
        _emailView.getText = ^(id data) {
            @strongify(self)
            self.model.emailNo = data;
        };
    }
    return _emailView;
}

-(TextCaptchaView* )codeView{
    if (!_codeView) {
        _codeView = [[TextCaptchaView alloc]initWithFrame:CGRectZero];
        [_codeView reloadTitle:@"验证码" placeHolder:@"请输入验证码"];
        @weakify(self)
        _codeView.getText = ^(id data) {
            @strongify(self)
            self.model.captcha = data;
        };
        _codeView.sendCodeBlock  = ^(id data) {
            @strongify(self)
            NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
            [param setValue:self.model.phoneNo forKey:@"phone"];
            [param setValue:@(1) forKey:@"type"];
            [param setValue:self.userView.tf.text forKey:@"account"];
            [self send_code:param completion:^(BOOL success, NSString *error) {
                if (!success) {
                    
                }else{
                    [self.codeView startCountDowView];
                }
            }];
        };
    }
    return _codeView;
}

-(InvitationCodeView* )userView{
    if (!_userView) {
        _userView = [[InvitationCodeView alloc]initWithFrame:CGRectZero];
        [_userView reloadTitle:Lang(@"账号") placeHolder:Lang(@"请输入币火账号，6~18位字母、数字")];
        @weakify(self)
        _userView.getText = ^(id data) {
            @strongify(self)
            self.model.account = data;
        };
    }
    return _userView;
}



-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _submitBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _submitBtn.layer.masksToBounds = YES;
        _submitBtn.layer.cornerRadius = 22;
        [_submitBtn setTitle:Lang(@"下一步") forState:UIControlStateNormal];
        _submitBtn.enabled = NO;
        _submitBtn.alpha = 0.5;
        _submitBtn.clipsToBounds =NO;
        [_submitBtn addTarget:self action:@selector(goTo:) forControlEvents:UIControlEventTouchUpInside];
         [_submitBtn az_setGradientBackgroundWithColors:@[[UIColor colorWithHexString:@"#154AE1"],[UIColor colorWithHexString:@"#067BE9"]] locations:nil startPoint:CGPointMake(1, 0) endPoint:CGPointMake(0, 0)];
    }
    return _submitBtn;
}

-(void)goTo:(id)sender{
    @weakify(self)
    [self register1:@{@"account":self.userView.tf.text ?: @"",@"phone":[self.numView.tf.text stringByReplacingOccurrencesOfString:@" " withString:@""],@"code":self.codeView.tf.text ?: @""} completion:^(id object, NSString *error) {
        @strongify(self)
        if([object[@"success"] boolValue]) {
            Block_Exec(self.nextBlock,self.model);
        }
    }];
}

-(UIButton* )regBtn{
    if (!_regBtn) {
        _regBtn = ({
            UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.titleLabel.font = [UIFont font12];
            [btn setTitleColor:[UIColor themeColor] forState:UIControlStateNormal];
            [btn setTitle:Lang(@"邮箱注册") forState:UIControlStateNormal];
            btn;
        });
        [_regBtn addTarget:self action:@selector(email:) forControlEvents:UIControlEventTouchUpInside];
        _regBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    return _regBtn;
}

-(void)email:(id)sender{
    !_regBlock?:_regBlock();
}
-(UIButton* )loginBtn{
    if (!_loginBtn) {
        _loginBtn = ({
            UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.titleLabel.font = [UIFont font12];
            [btn setTitleColor:[UIColor themeColor] forState:UIControlStateNormal];
            NSString* str1 = Lang(@"已有币火账号?");
            NSString* str2 = Lang(@"登录");
            NSMutableAttributedString* attr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ %@",str1,str2]];
            [attr addColor:[UIColor moPlaceHolder] substring:str1];
            [attr addColor:[UIColor themeColor] substring:str2];
            [attr addFont:[UIFont font12] substring:str1];
            [attr addFont:[UIFont font12] substring:str2];
            [btn setAttributedTitle:attr forState:UIControlStateNormal];
            btn;
        });
        [_loginBtn addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
        _loginBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    return _loginBtn;
}

-(void)login:(id)sender{
    !_loginBlock?:_loginBlock();
}

-(void)configModel:(LoginRegModel*)model{
    self.model = model;

    [self.model addObserver:self
                 forKeyPath:@"captcha"
                    options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                    context:nil];
    
    [self.model addObserver:self
                 forKeyPath:@"account"
                    options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                    context:nil];
    
    if (model.type == RegisterPhoneType) {
        [self.model addObserver:self
                        forKeyPath:@"phoneNo"
                           options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                           context:nil];
        self.emailView.hidden = YES;
        [self.regBtn setTitle:Lang(@"邮箱注册") forState:UIControlStateNormal];
    }else{
        [self.model addObserver:self
                        forKeyPath:@"emailNo"
                           options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew
                           context:nil];
         self.numView.hidden = YES;
        [self.regBtn setTitle:Lang(@"手机注册") forState:UIControlStateNormal];
    }
}

/* KVO,只要object的keyPath属性发生变化，就会调用此函数*/
- (void)observeValueForKeyPath:(NSString *)keyP1ath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (self.model.type == RegisterPhoneType) {
        // 控制提交按钮
        if (self.model.phoneNo.length > 0 &&
             self.model.captcha.length > 0 &&
             self.model.account.length > 0 ) {
                self.submitBtn.enabled = YES;
                self.submitBtn.alpha = 1;
                return;
            }
        self.submitBtn.enabled = NO;
        self.submitBtn.alpha = 0.5;
    }else{
       // 控制提交按钮
        if (self.model.emailNo.length > 0 &&
             self.model.captcha.length > 0 &&
             self.model.account.length > 0 ) {
                self.submitBtn.enabled = YES;
                self.submitBtn.alpha = 1;
                return;
            }
        self.submitBtn.enabled = NO;
        self.submitBtn.alpha = 0.5;
    }
    
}

-(void)dealloc{
    if (self.model.type == RegisterPhoneType) {
        [self.model removeObserver:self forKeyPath:@"phoneNo"];
    }else{
        [self.model removeObserver:self forKeyPath:@"emailNo"];
    }
    [self.model removeObserver:self forKeyPath:@"captcha"];
    [self.model removeObserver:self forKeyPath:@"account"];
}

-(void)updateForLanguageChanged{
    
}


-(YYLabel* )tipLbl{
    if (!_tipLbl) {
        _tipLbl = [YYLabel new];
        NSString* str1 = @"点击“注册”按钮，表示同意";
        NSString* str2 = @"隐私条款和服务协议";
        NSString* str = [NSString stringWithFormat:@"%@%@",str1,str2];
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc]initWithString:str];
        [attr addFont:[UIFont font12] substring:str];
        [attr setTextHighlightRange:[str rangeOfString:str2] color:[UIColor moBlue] backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                NSLog(@"%@",text);
            [self login_get_version:@{@"type":@"2"} completion:^(id object, NSString *error) {
              if (object && [object isKindOfClass:[NSDictionary class]]) {
                  NSString* title = [object valueForKeyPath:@"title"];
                  NSString* content = [object valueForKeyPath:@"content"];
                  [MXRouter openURL:@"lcwl://MBWebVC" parameters:@{@"navTitle":title,@"content":content ?: @""}];
              }
            }];
              }];
        [attr setTextHighlightRange:[str rangeOfString:str1] color:[UIColor moBlack] backgroundColor:[UIColor clearColor] tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
            NSLog(@"%@",text);
          
              }];
        
        _tipLbl.attributedText = attr;
        
    }
    return _tipLbl;
}

@end
