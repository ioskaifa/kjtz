//
//  AuthorizationFailView.h
//  BOB
//
//  Created by mac on 2020/7/4.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AuthorizationFailType) {
    ///注册时候提示需要授权
    AuthorizationFailTypeRegist,
    ///登录时候提示需要授权
    AuthorizationFailTypeLogin,
};

NS_ASSUME_NONNULL_BEGIN

@interface AuthorizationFailView : UIView

@property (nonatomic, assign) AuthorizationFailType failType;

@end

NS_ASSUME_NONNULL_END
