//
//  TopBottomView.h
//  BOB
//
//  Created by mac on 2020/9/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, TopBottomViewType) {
    TopBottomViewTypeNormal       = 0,
    ///密码明暗文切换
    TopBottomViewTypeSecure       = 1,
};

@interface TopBottomView : UIView

@property (nonatomic, copy) NSString *value;

@property (nonatomic, strong) UITextField *tf;

- (instancetype)initWithViewType:(TopBottomViewType)viewType
                         tipsImg:(NSString *)img
                            tips:(NSString *)tips
                     placeholder:(NSString *)placeholder;

+ (UIButton *)tipsBtnWithtipsImg:(NSString *)img tips:(NSString *)tips;

@end

NS_ASSUME_NONNULL_END
