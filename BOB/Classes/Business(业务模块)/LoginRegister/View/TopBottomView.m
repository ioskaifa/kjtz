//
//  TopBottomView.m
//  BOB
//
//  Created by mac on 2020/9/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "TopBottomView.h"

@interface TopBottomView ()

@property (nonatomic, assign) TopBottomViewType viewType;

@property (nonatomic, copy) NSString *img;

@property (nonatomic, copy) NSString *tips;

@property (nonatomic, copy) NSString *placeholder;
///密码明暗文切换
@property (nonatomic, strong) UIButton *secureBtn;

@end

@implementation TopBottomView

- (instancetype)initWithViewType:(TopBottomViewType)viewType
                         tipsImg:(NSString *)img
                            tips:(NSString *)tips
                     placeholder:(NSString *)placeholder {
    if (self = [super init]) {
        self.viewType = viewType;
        self.img = img;
        self.tips = tips;
        self.placeholder = placeholder;
        [self createUI];
    }
    return self;
}

- (NSString *)value {
    return self.tf.text;
}

- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
        
    [layout addSubview:[self.class tipsBtnWithtipsImg:self.img tips:self.tips]];
    
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myLeft = 0;
    bottomLayout.myRight = 0;
    bottomLayout.myTop = 10;
    bottomLayout.myBottom = 10;
    bottomLayout.weight = 1;
    [layout addSubview:bottomLayout];
    
    [bottomLayout addSubview:self.tf];
    if (self.viewType == TopBottomViewTypeSecure) {
        self.tf.secureTextEntry = YES;
        [bottomLayout addSubview:self.secureBtn];
    }
    [self addBottomSingleLine:[UIColor colorWithHexString:@"#EBEBEB"]];
}

- (UITextField *)tf {
    if (!_tf) {
        _tf = [UITextField new];        
        _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
        _tf.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
        _tf.myLeft = 0;
        _tf.weight = 1;
        _tf.height = 25;
        _tf.myHeight = _tf.height;
        _tf.myCenterY = 0;
        NSArray *txtArr = @[self.placeholder?:@""];
        NSArray *colorArr = @[[UIColor colorWithHexString:@"#CCCCCC"]];
        NSArray *fontArr = @[[UIFont font15]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        _tf.attributedPlaceholder = att;
    }
    return _tf;
}

- (UIButton *)secureBtn {
    if (!_secureBtn) {
        _secureBtn = ({
            UIButton *object = [UIButton new];
            [object setImage:[UIImage imageNamed:@"密码隐藏"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"密码显示"] forState:UIControlStateSelected];
            object.mySize = CGSizeMake(30, 30);
            object.myCenterY = 0;
            object.myLeft = 10;
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                btn.selected = !btn.selected;
                self.tf.secureTextEntry = !btn.selected;
            }];
            object;
        });
    }
    return _secureBtn;
}

+ (UIButton *)tipsBtnWithtipsImg:(NSString *)img tips:(NSString *)tips {
    SPButton *btn = [SPButton new];
    btn.userInteractionEnabled = NO;
    btn.titleLabel.font = [UIFont boldFont13];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.imagePosition = SPButtonImagePositionLeft;
    btn.imageTitleSpace = 10;
    [btn setTitle:tips forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.mySize = btn.size;
    return btn;
}

@end
