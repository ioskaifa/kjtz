//
//  PhoneRegisterVC.m
//  BOB
//
//  Created by mac on 2019/12/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "PhoneRegisterVC.h"
#import "NSObject+LoginHelper.h"
#import "LoginRegModel.h"
#import "TopBottomView.h"
#import "VerityCodeView.h"
#import "ImageVerifyView.h"
#import "UIImage+Color.h"
#import "SettingPsdVC.h"
#import "TCAccountMgrModel.h"
#import "TCWechatInfoView.h"
#import "HUDHelper.h"

@interface PhoneRegisterVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MyLinearLayout *layout;

@property (nonatomic,strong) LoginRegModel  *model;

@property (nonatomic, strong) TopBottomView *nameView;

@property (nonatomic, strong) TopBottomView *invitationView;

@property (nonatomic, strong) TopBottomView *numView;

@property (nonatomic, strong) VerityCodeView  *codeView;

@property (nonatomic, strong) ImageVerifyView  *imgVerView;

@property (nonatomic, strong) UIButton *submitBtn;

@property (nonatomic, copy) NSString *img_id;

@property (nonatomic, copy) NSString *img_code;

@end

@implementation PhoneRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self reloadImgVerCode];
}

#pragma mark - IBAction

#pragma mark - Private Method
- (void)reloadImgVerCode {
    [self createImgCode:@{@"interface_type":@"createImgCode"}
             completion:^(id object, NSString *error) {
        if (object) {
            if (object[@"img_io"]) {
                [self.imgVerView reloadImage:[UIImage imageWithDataString:object[@"img_io"]]];
            }
            self.img_id = object[@"img_id"]?:@"";
        }
    }];
}

- (NSString *)phoneNo {
    NSString *phone = [StringUtil trim:self.numView.value];
    if ([StringUtil isEmpty:phone]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不能为空"];
        return @"";
    }
    if (![DCCheckRegular dc_checkTelNumber:phone]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不正确"];
        return @"";
    }
    return phone;
}

- (BOOL)valiParam {
    self.model.inviteCode = [StringUtil trim:self.invitationView.value];
    if ([StringUtil isEmpty:self.model.inviteCode]) {
        [NotifyHelper showMessageWithMakeText:@"邀请码不能为空"];
        return NO;
    }
    
    self.model.nickName = [StringUtil trim:self.nameView.value];
    if (self.model.nickName.length > 15) {
        [NotifyHelper showMessageWithMakeText:@"昵称长度不能超过15位"];
        return NO;
    }
    self.model.phoneNo = [StringUtil trim:self.numView.value];
    if ([StringUtil isEmpty:self.model.phoneNo]) {
        [NotifyHelper showMessageWithMakeText:@"手机不能为空"];
        return NO;
    }
    if (![DCCheckRegular dc_checkTelNumber:self.model.phoneNo]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不正确"];
        return NO;
    }
    if ([StringUtil isEmpty:self.img_code]) {
        [NotifyHelper showMessageWithMakeText:@"请输入图形验证码"];
        return NO;
    }
    self.model.captcha = [StringUtil trim:self.codeView.tf.text];
    if ([StringUtil isEmpty:self.model.captcha]) {
        [NotifyHelper showMessageWithMakeText:@"请输入验证码"];
        return NO;
    }
    return YES;
}

- (void)commit {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [self checkUserExist:self.model.phoneNo?:@"" completion:^(BOOL success, id object, NSString *error) {
        if (!success) {
            [NotifyHelper hideHUDForView:self.view animated:YES];
            [NotifyHelper showMessageWithMakeText:error];
            return;
        }
        NSDictionary *param = @{
            @"sms_code":self.model.captcha?:@"",
            @"nick_name":self.model.nickName?:@"",
            @"user_account":self.model.phoneNo?:@"",
            @"device_type":@"iOS",
            @"invite_code":self.model.inviteCode?:@"",
        };
        [self register1:param completion:^(id object, NSString *error) {
            [NotifyHelper hideHUDForView:self.view animated:YES];
            if([object[@"success"] boolValue]) {
                if (object[@"data"][@"valid_flag"]) {
                    self.model.valid_flag = [object[@"data"][@"valid_flag"] description];
                }
                UDetail.user.user_tel = self.model.phoneNo;
                SettingPsdVC *vc = [SettingPsdVC new];
                vc.model = self.model;
                [self.navigationController pushViewController:vc animated:YES];
            } else {
                [NotifyHelper showMessageWithMakeText:error];
            }
        }];
    }];
}

#pragma mark - InitUI
-(void)setupUI {
    [self.view addSubview:self.tableView];
    [self layoutUI];
    
    UILabel *tipLbl = [UILabel new];
    tipLbl.font = [UIFont boldSystemFontOfSize:30];
    tipLbl.textColor = [UIColor blackColor];
    tipLbl.text = [DeviceManager getAppName];
    tipLbl.myTop = 35;
    tipLbl.myLeft = 30;
    [tipLbl sizeToFit];
    tipLbl.mySize = tipLbl.size;
    [self.layout addSubview:tipLbl];
    
    [self.layout addSubview:self.invitationView];
    [self.layout addSubview:self.nameView];
    [self.layout addSubview:self.numView];
    
    UIButton *tipsBtn = [TopBottomView tipsBtnWithtipsImg:@"图形验证码" tips:@"图形验证码"];
    tipsBtn.myTop = 10;
    tipsBtn.myLeft = 30;
    [self.layout addSubview:tipsBtn];
    [self.layout addSubview:self.imgVerView];
    
    tipsBtn = [TopBottomView tipsBtnWithtipsImg:@"验证码" tips:@"验证码"];
    tipsBtn.myTop = 10;
    tipsBtn.myLeft = 30;
    [self.layout addSubview:tipsBtn];
    [self.layout addSubview:self.codeView];
    
    [self.layout addSubview:self.submitBtn];
    
    [self.layout layoutSubviews];
    self.tableView.tableHeaderView = self.layout;
}

- (void)layoutUI {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}

- (MyLinearLayout *)layout {
    if (!_layout) {
        _layout = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            object.backgroundColor = [UIColor whiteColor];
            object.myTop = 0;
            object.myLeft = 0;
            object.width = SCREEN_WIDTH;
            object.myWidth = object.width;
            object.myHeight = MyLayoutSize.wrap;
            object;
        });
    }
    return _layout;
}

-(TopBottomView* )invitationView{
    if (!_invitationView) {
        _invitationView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeNormal
                                                                    tipsImg:@"推荐码"
                                                                       tips:@"推荐码"
                                                                placeholder:@"请输入邀请码"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 50;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });

    }
    return _invitationView;
}

-(TopBottomView* )nameView{
    if (!_nameView) {
        _nameView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeNormal
                                                                    tipsImg:@"昵称"
                                                                       tips:@"昵称"
                                                                placeholder:@"请设置昵称（不超出15位）"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeDefault;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 10;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });

    }
    return _nameView;
}

-(TopBottomView* )numView{
    if (!_numView) {
        _numView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeNormal
                                                                    tipsImg:@"手机号"
                                                                       tips:@"手机号"
                                                                placeholder:@"请输入手机号码"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypePhonePad;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 10;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });

    }
    return _numView;
}

-(VerityCodeView* )codeView{
    if (!_codeView) {
        _codeView = [[VerityCodeView alloc] initWithFrame:CGRectZero];
        _codeView.backgroundColor = [UIColor whiteColor];
        _codeView.tf.textColor = [UIColor blackColor];
        _codeView.myTop = 0;
        _codeView.myLeft = 30;
        _codeView.myRight = 30;
        _codeView.myHeight = 60;
        
        @weakify(self)
        _codeView.sendCode = ^(id data) {
            @strongify(self)
            if ([StringUtil isEmpty:[self phoneNo]]) {
                return NO;
            }
            if ([StringUtil isEmpty:self.img_id]) {
                [NotifyHelper showMessageWithMakeText:@"请刷新图形验证码"];
                return NO;
            }
            if ([StringUtil isEmpty:self.img_code]) {
                [NotifyHelper showMessageWithMakeText:@"请输入图形验证码"];
                return NO;
            }
            [self send_code:@{@"user_account":self.phoneNo,
                              @"bus_type":@"FrontRegister",
                              @"img_id":self.img_id?:@"",
                              @"img_code":self.img_code?:@""}
                 completion:^(BOOL success, NSString *error) {
                
            }];
            return YES;
            };
    }
    return _codeView;
}

- (ImageVerifyView *)imgVerView {
    if (!_imgVerView) {
        _imgVerView = [[ImageVerifyView alloc] initWithFrame:CGRectZero];
        _imgVerView.backgroundColor = [UIColor whiteColor];
        _imgVerView.btn.backgroundColor = [UIColor whiteColor];
        _imgVerView.tf.textColor = [UIColor blackColor];
        _imgVerView.myTop = 0;
        _imgVerView.myLeft = 30;
        _imgVerView.myRight = 30;
        _imgVerView.myHeight = 60;
        @weakify(self)
        _imgVerView.getText = ^(NSString * data) {
            @strongify(self)
            self.img_code = data;
        };
        _imgVerView.picBlock = ^(id  _Nullable data) {
            @strongify(self)
            [self reloadImgVerCode];
        };
    }
    return _imgVerView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(SCREEN_WIDTH - 70, 55);
            [object setViewCornerRadius:5];
            [object setBackgroundColor:[UIColor blackColor]];
            object.titleLabel.font = [UIFont boldFont16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setTitle:Lang(@"注册") forState:UIControlStateNormal];
            object.myTop = 45;
            object.myLeft = 35;
            object.mySize = object.size;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
               @strongify(self);
               if ([self valiParam]) {
                   [self commit];
               }
            }];
            object;
        });
    }
    return _submitBtn;
}

- (LoginRegModel *)model {
    if (!_model) {
        _model = [LoginRegModel new];
    }
    return _model;
}

@end
