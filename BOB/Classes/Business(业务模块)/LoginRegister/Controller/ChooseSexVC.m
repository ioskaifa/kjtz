//
//  ChooseSexVC.m
//  BOB
//
//  Created by mac on 2020/9/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChooseSexVC.h"
#import "UIImage+Color.h"
#import "NSObject+Mine.h"

@interface ChooseSexVC ()
///男
@property (nonatomic, strong) SPButton *maleBtn;
///女
@property (nonatomic, strong) SPButton *femaleBtn;

@property (nonatomic, strong) UIButton *selectBtn;

@property (nonatomic, strong) UIButton *submitBtn;

@end

@implementation ChooseSexVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)sexBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        sender.layer.borderColor = [UIColor blackColor].CGColor;
    } else {
        sender.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    }
    if (sender.selected) {
        if (self.selectBtn != sender) {
            [self sexBtnClick:self.selectBtn];
            self.selectBtn = sender;
        }
    }
    UIImageView *imgView = [sender.superview viewWithTag:2];
    if (![imgView isKindOfClass:UIImageView.class]) {
        return;
    }
    imgView.visibility = sender.selected ? MyVisibility_Visible:MyVisibility_Gone;
}

- (NSString *)selectSex {
    NSString *string = @"1";
    if ([self.selectBtn.currentTitle isEqualToString:@"我是女森"]) {
        string = @"2";
    }
    return string;
}

- (void)commit {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [self modifyUserInfo:@{@"sex":[self selectSex]}
              completion:^(BOOL success, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        //更新性别 不管成功失败都跳转登录界面 进行登录
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

- (void)createUI {
    [self setNavBarTitle:@"选择性别"];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    
    MyFrameLayout *subLayout = [MyFrameLayout new];
    subLayout.mySize = self.maleBtn.size;
    subLayout.myCenterX = 0;
    subLayout.myTop = 60;
    [subLayout addSubview:self.maleBtn];
    UIImageView *imgView = [UIImageView new];
    imgView.tag = 2;
    imgView.image = [UIImage imageNamed:@"性别选中"];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myTop = self.maleBtn.height - imgView.height/2.0;
    imgView.myLeft = self.maleBtn.width - self.maleBtn.height/2;
    imgView.visibility = MyVisibility_Gone;
    [subLayout addSubview:imgView];
    
    [layout addSubview:subLayout];
    
    subLayout = [MyFrameLayout new];
    subLayout.mySize = self.femaleBtn.size;
    subLayout.myCenterX = 0;
    subLayout.myTop = 60;
    [subLayout addSubview:self.femaleBtn];
    imgView = [UIImageView new];
    imgView.tag = 2;
    imgView.image = [UIImage imageNamed:@"性别选中"];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myTop = self.femaleBtn.height - imgView.height/2.0;
    imgView.myLeft = self.femaleBtn.width - self.femaleBtn.height/2;
    imgView.visibility = MyVisibility_Gone;
    [subLayout addSubview:imgView];
    
    [layout addSubview:subLayout];
    
    [layout addSubview:self.submitBtn];
    
    self.selectBtn = self.maleBtn;
    [self sexBtnClick:self.maleBtn];
}

- (SPButton *)maleBtn {
    if (!_maleBtn) {
        _maleBtn = ({
            SPButton *object = [self.class factorySexBtn:@"我是男森" img:@"男_unSelect" selectImg:@"男_select"];
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                [self sexBtnClick:btn];
            }];
            object;
        });
    }
    return _maleBtn;
}

- (SPButton *)femaleBtn {
    if (!_femaleBtn) {
        _femaleBtn = ({
            SPButton *object = [self.class factorySexBtn:@"我是女森" img:@"女_unSelect" selectImg:@"女_select"];
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                [self sexBtnClick:btn];
            }];
            object;
        });
    }
    return _femaleBtn;
}

+ (SPButton *)factorySexBtn:(NSString *)title
                        img:(NSString *)img
                  selectImg:(NSString *)selectImg {
    SPButton *btn = [SPButton new];
    btn.size = CGSizeMake(260, 100);
    [btn setViewCornerRadius:5];
    btn.imagePosition = SPButtonImagePositionLeft;
    btn.imageTitleSpace = 40;
    btn.titleLabel.font = [UIFont font18];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:selectImg] forState:UIControlStateSelected];
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = [UIColor colorWithHexString:@"#C1C1C1"].CGColor;
    if ([title isEqualToString:@"我是男森"]) {
        btn.tag = 1;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    } else {
        [btn setTitleColor:[UIColor colorWithHexString:@"#DBDBDB"] forState:UIControlStateSelected];
    }
    [btn setTitleColor:[UIColor colorWithHexString:@"#DBDBDB"] forState:UIControlStateNormal];
    btn.mySize = btn.size;
    return btn;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(SCREEN_WIDTH - 70, 55);
            [object setViewCornerRadius:5];
            [object setBackgroundColor:[UIColor blackColor]];
            object.titleLabel.font = [UIFont boldFont16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setTitle:Lang(@"马上进入") forState:UIControlStateNormal];
            object.myTop = 60;
            object.myLeft = 35;
            object.mySize = object.size;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                [self commit];
            }];
            object;
        });
    }
    return _submitBtn;
}

@end
