//
//  CountryCodeEntity.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/1/29.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "CountryCodeEntity.h"
#import "CountryCodeCell.h"
#import "MXSeparatorLine.h"
#import "CountryCodeModel.h"
#import "StringUtil.h"
#define HeaderHeight 25
#define RowHeight 44
static CountryCodeEntity * helper =nil;
static dispatch_once_t onceToken;
@implementation CountryCodeEntity

+(CountryCodeEntity*)sharedInstance {
    dispatch_once(&onceToken, ^{
        helper=[[CountryCodeEntity alloc]init];
    });
    return helper;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier = @"Cell";
    CountryCodeCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CountryCodeCell" owner:self options:nil] safeObjectAtIndex:0];
        MXSeparatorLine* line1=[MXSeparatorLine initHorizontalLineWidth:CGRectGetWidth(SCREEN_SIZE) orginX:0 orginY:RowHeight-1];
        [cell addSubview:line1];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
    }
    MLog(@"%@ -- %@",@(indexPath.section),@(indexPath.row));
    CountryCodeModel* model = [[_countryCodeArray safeObjectAtIndex:indexPath.section] safeObjectAtIndex:indexPath.row];

   
    if (LanguageTypeChinese ==  [Language currentLanguageType]) {
         cell.countryName.text =  model.cs_name;
    }else{
         cell.countryName.text =  model.en_name;
    }

    if (![StringUtil isEmpty:model.code]) {
        
        cell.countryCode.text = [NSString stringWithFormat:@"%@", model.code];
    }else{
        cell.countryCode.text = @"";
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

#pragma mark - Table view delegate
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (title == UITableViewIndexSearch)
    {
        return -1;
    }
    return [ALPHA rangeOfString:title].location;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle=[self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle==nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label=[[UILabel alloc] init];
    label.frame=CGRectMake(12, 2, 300, 21);
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor whiteColor];
    label.font=[UIFont fontWithName:@"Helvetica-Bold" size:14];
    label.text=sectionTitle;
    
    // Create header view and add label as a subview
    UIView *sectionView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, HeaderHeight)];
    [sectionView setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:label];
    sectionView.backgroundColor  = [UIColor clearColor];
    MXSeparatorLine* line =[MXSeparatorLine initHorizontalLineWidth:tableView.bounds.size.width orginX:0 orginY:HeaderHeight-1];
//    [sectionView addSubview:line];
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSString *sectionTitle=[self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle==nil) {
        return 0;
    }
    return HeaderHeight;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{

    if ([[self.countryCodeArray safeObjectAtIndex:section] count] == 0) return nil;
    if (section == 0) {
        return   @"🌟";
    }else {
        return [NSString stringWithFormat:@"%@", [[ALPHA substringFromIndex:section] substringToIndex:1]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [[self.countryCodeArray safeObjectAtIndex:section] count];
        
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ALPHA.length;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray *indices = [NSMutableArray arrayWithObject:UITableViewIndexSearch];
    for (int i = 0; i < ALPHA.length; i++){
        if ([[self.countryCodeArray safeObjectAtIndex:i] count])
            [indices safeAddObj:[[ALPHA substringFromIndex:i] substringToIndex:1]];
    }
    return indices;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CountryCodeModel* model = [[self.countryCodeArray safeObjectAtIndex:indexPath.section] safeObjectAtIndex:indexPath.row];
    if (self.callBack) {
        self.callBack(@[model],NO);
    }
}
-(void)didSelect:(CountryCodeModel*)model {

}
-(void)didLocation:(CountryCodeModel*)model{
    if (self.callBack) {
        self.callBack(@[model],YES);
    }
}
@end
