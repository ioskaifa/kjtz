//
//  CountryCodeEntity.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/1/29.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CountryCodeModel.h"
typedef void(^CountryCodeDidSelectCallBack)(NSArray *countrys ,BOOL isSelectedLocation);

@interface CountryCodeEntity : NSObject<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,retain) NSMutableArray *countryCodeArray ;
@property (nonatomic,strong) CountryCodeDidSelectCallBack callBack;
@property (nonatomic) NSInteger type;

+(CountryCodeEntity*)sharedInstance;

@end
