//
//  CountryCodeVC.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/1/29.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CountryCodeEntity.h"
typedef enum
{
    MOBILE ,
    CITY   ,
    COUNTRY,
    AREA,
    STATE,
    Bussness,
    STREET
}CountryType;

typedef enum
{
    Current_And_Selected, //显示当前位置和已选地区两行
    Selected   ,  //只显示已选地区
}ShowType;


typedef enum: NSInteger
{
    fromLoginVC = 0,

}FromVC;



@interface CountryCodeVC : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *countryTable;

-(void)addDidSelectCallBack:(CountryCodeDidSelectCallBack)callback;

@property (nonatomic) CountryType type;

@property (nonatomic) ShowType showType;

@property (nonatomic) UIView* headView;

@property (strong, nonatomic) CountryCodeModel* selectedModel;
//如果是地区 请传此id
@property (nonatomic) NSInteger pid;

@property (assign , nonatomic) FromVC fromVC;

//是否选择市以后的信息 fly2015.11.04，默认是NO，只选中 国家省市，YES时可以选中的省市区
@property (assign , nonatomic) BOOL  shouldUseStreet;

@end
