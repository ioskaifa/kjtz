//
//  ShowForgetPwdSuccessVC.m
//  BOB
//
//  Created by mac on 2020/7/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ShowForgetPwdSuccessVC.h"
#import "ForgetPwdSuccessView.h"

@interface ShowForgetPwdSuccessVC ()

@property (nonatomic, strong) UIView *bgView;

@property (nonatomic, strong) MyLinearLayout *contentLayout;

@property (nonatomic, strong) ForgetPwdSuccessView *contentView;

@end

@implementation ShowForgetPwdSuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

#pragma mark - Public Method
+ (instancetype)showInViewController:(UIViewController *)vc {
    ShowForgetPwdSuccessVC *showVC = [ShowForgetPwdSuccessVC new];
    if (vc) {
        [vc addChildViewController:showVC];
        [vc.view addSubview:showVC.view];
        [showVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.insets(UIEdgeInsetsZero);
        }];
        [showVC show];
    }
    return showVC;
}


- (void)addContentViewAnimation {
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.calculationMode = kCAAnimationCubic;
    animation.values = @[@1.07,@1.06,@1.05,@1.03,@1.02,@1.01,@1.0];
    animation.duration = 0.2;
    [self.contentView.layer addAnimation:animation forKey:@"transform.scale"];
}

- (void)disMiss {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

#pragma mark - Private Method
- (void)show {
    self.bgView.alpha = 0.5;
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.contentView.alpha = 1;
    } completion:nil];
    
    [self addContentViewAnimation];
}

#pragma mark - InitUI
- (void)createUI {
    self.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.contentLayout];
    [self.contentLayout addSubview:self.contentView];
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor blackColor];
            object.alpha = 0;
            object;
        });
    }
    return _bgView;
}

- (MyLinearLayout *)contentLayout {
    if (!_contentLayout) {
        _contentLayout = ({
            MyLinearLayout *object = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            object.backgroundColor = [UIColor clearColor];
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.myBottom = 0;
            @weakify(self);
            [object addAction:^(UIView *view) {
                @strongify(self);
                [self disMiss];
            }];
            object;
        });
    }
    return _contentLayout;
}


- (ForgetPwdSuccessView *)contentView {
    if (!_contentView) {
        _contentView = ({
            ForgetPwdSuccessView *object = [ForgetPwdSuccessView new];
            object.myTop = 140;
            object.myLeft = 25;
            object.myRight = 25;
            object.myHeight = MyLayoutSize.wrap;
            object.alpha = 0;
            @weakify(self)
            [object.loginBtn addAction:^(UIButton *btn) {
                @strongify(self)
                [self disMiss];
                if (self.block) {
                    self.block(nil);
                }
            }];
            object;
        });
    }
    return _contentView;
}

@end
