//
//  ResetPwdVC.h
//  BOB
//
//  Created by mac on 2019/6/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "LoginRegModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ResetPwdVC : BaseViewController

@property (nonatomic , strong)LoginRegModel *model;

@end

NS_ASSUME_NONNULL_END
