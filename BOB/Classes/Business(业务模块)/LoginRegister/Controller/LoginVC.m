//
//  LoginVC.m
//  BOB
//
//  Created by mac on 2019/6/25.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "LoginVC.h"
#import "NSObject+LoginHelper.h"
#import "RealNameAuthVC.h"
#import "TopBottomView.h"
#import "UIImage+Color.h"
#import "VerityCodeView.h"
#import "ImageVerifyView.h"
#import "DCCheckRegular.h"

@interface LoginVC ()<UITextFieldDelegate>

@property (nonatomic,strong) TopBottomView *userView;

@property (nonatomic,strong) TopBottomView *psdView;

@property (nonatomic,strong) UIButton *submitBtn;

@property (nonatomic,strong) UIButton *forgetBtn;
@property (nonatomic,strong) UIButton *switchBtn;

@property (nonatomic,strong) UIButton *regBtn;

@property (nonatomic, strong) TopBottomView *numView;

@property (nonatomic, strong) VerityCodeView  *codeView;

@property (nonatomic, strong) ImageVerifyView  *imgVerView;

@property (nonatomic, strong) UIButton  *codeViewTipBnt;

@property (nonatomic, strong) UIButton  *imgVerTipBnt;

@property (nonatomic, copy) NSString *img_id;

@property (nonatomic, copy) NSString *img_code;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.userView.tf.text = UDetail.user.user_tel?:@"";
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - IBAction
-(void)login:(id)sender{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    NSDictionary* paramDict = nil;
    if(self.switchBtn.selected) {
        paramDict = @{@"login_type":@"account",
                      @"sys_user_account":self.userView.value?:@"",
                      @"login_password":self.psdView.value?:@""};
    } else {
        paramDict = @{@"login_type": @"code",
                      @"sys_user_account":self.numView.value?:@"",
                      @"sms_code":self.codeView.tf.text?:@""};
    }
    
    [self login:paramDict superView:self.view completion:^(BOOL success, NSString *error) {
        if (success) {
            MoApp.mallVC = [MainViewController new];
            [self.view.window setRootViewController:MoApp.mallVC];
        }
    }];
}

-(void)forget:(id)sender{
    MXRoute(@"ForgetPwdVC", nil);
}

- (void)privacyPolicy {
    MXRoute(@"MXWebViewVC", (@{@"urlString":@"https://dnf.wgzvip.com/privacy_hld.htm",
                               @"title":@"隐私政策"}))
}

#pragma mark - Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.userView.tf == textField) {
        return [self.psdView.tf becomeFirstResponder];
    }
    if (self.psdView.tf == textField) {
        return [self.psdView.tf resignFirstResponder];
    }
    return YES;
}

///通过登录密码还是通过验证码登录
- (void)switchLoginType:(BOOL)byPass {
    
    self.userView.visibility = byPass ? MyVisibility_Visible : MyVisibility_Gone;
    self.psdView.visibility = byPass ? MyVisibility_Visible : MyVisibility_Gone;
    
    self.numView.visibility = !byPass ? MyVisibility_Visible : MyVisibility_Gone;
    self.codeViewTipBnt.visibility = !byPass ? MyVisibility_Visible : MyVisibility_Gone;
    self.codeView.visibility = !byPass ? MyVisibility_Visible : MyVisibility_Gone;
    self.imgVerTipBnt.visibility = !byPass ? MyVisibility_Visible : MyVisibility_Gone;
    self.imgVerView.visibility = !byPass ? MyVisibility_Visible : MyVisibility_Gone;
}

#pragma mark - Private Method

#pragma mark - InitUI
-(void)initUI{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeAll;
    }    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    
    
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldSystemFontOfSize:30];
    lbl.textColor = [UIColor blackColor];
    lbl.text = [DeviceManager getAppName];
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 30;
    lbl.myTop = 125;
    [layout addSubview:lbl];
    //密码登录
    [layout addSubview:self.userView];
    [layout addSubview:self.psdView];
    
    //验证码登录
    [layout addSubview:self.numView];
    self.imgVerTipBnt = [TopBottomView tipsBtnWithtipsImg:@"图形验证码" tips:@"图形验证码"];
    self.imgVerTipBnt.myTop = 10;
    self.imgVerTipBnt.myLeft = 30;
    [layout addSubview:self.imgVerTipBnt];
    [layout addSubview:self.imgVerView];
    
    self.codeViewTipBnt = [TopBottomView tipsBtnWithtipsImg:@"验证码" tips:@"验证码"];
    self.codeViewTipBnt.myTop = 10;
    self.codeViewTipBnt.myLeft = 30;
    [layout addSubview:self.codeViewTipBnt];
    [layout addSubview:self.codeView];
    
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myTop = 10;
    bottomLayout.myHeight = 40;
    bottomLayout.myLeft = 35;
    bottomLayout.myRight = 35;
    [layout addSubview:bottomLayout];
    
    [bottomLayout addSubview:self.forgetBtn];
    [bottomLayout addSubview:[UIView placeholderHorzView]];
    [bottomLayout addSubview:self.regBtn];
    
    [layout addSubview:[UIView placeholderVertView]];
    
    [layout addSubview:self.submitBtn];
    
    [layout addSubview:self.switchBtn];
    
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"登录底部背景"];
    [imgView sizeToFit];
    CGSize size = imgView.size;
    imgView.size = CGSizeMake(SCREEN_WIDTH, SCREEN_WIDTH*size.height/size.width);
    imgView.mySize = imgView.size;
    imgView.myBottom = 0;
    imgView.myLeft = 0;
    [layout addSubview:imgView];
    
    [self switchLoginType:YES];
}

#pragma mark - Private Method
- (void)reloadImgVerCode {
    [self createImgCode:@{@"interface_type":@"createImgCode"}
             completion:^(id object, NSString *error) {
        if (object) {
            if (object[@"img_io"]) {
                [self.imgVerView reloadImage:[UIImage imageWithDataString:object[@"img_io"]]];
            }
            self.img_id = object[@"img_id"]?:@"";
        }
    }];
}

- (NSString *)phoneNo {
    NSString *phone = [StringUtil trim:self.numView.value];
    if ([StringUtil isEmpty:phone]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不能为空"];
        return @"";
    }
    if (![DCCheckRegular dc_checkTelNumber:phone]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不正确"];
        return @"";
    }
    return phone;
}

- (BOOL)valiParam {

    NSString *phoneNo = [StringUtil trim:self.numView.value];
    if ([StringUtil isEmpty:phoneNo]) {
        [NotifyHelper showMessageWithMakeText:@"手机不能为空"];
        return NO;
    }
    if (![DCCheckRegular dc_checkTelNumber:phoneNo]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不正确"];
        return NO;
    }
    if ([StringUtil isEmpty:self.img_code]) {
        [NotifyHelper showMessageWithMakeText:@"请输入图形验证码"];
        return NO;
    }
    NSString *captcha = [StringUtil trim:self.codeView.tf.text];
    if ([StringUtil isEmpty:captcha]) {
        [NotifyHelper showMessageWithMakeText:@"请输入验证码"];
        return NO;
    }
    return YES;
}

#pragma mark - Init
-(TopBottomView* )userView{
    if (!_userView) {
        _userView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeNormal
                                                                    tipsImg:@"手机号"
                                                                       tips:@"手机号"
                                                                placeholder:@"请输入手机号码"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 50;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });
    }
    return _userView;
}

-(TopBottomView* )psdView{
    if (!_psdView) {
        _psdView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeSecure
                                                                    tipsImg:@"登录密码"
                                                                       tips:@"登录密码"
                                                                placeholder:@"请输入登录密码"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            object.tf.returnKeyType = UIReturnKeyDone;
            object.myTop = 20;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });
    }
    return _psdView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(SCREEN_WIDTH - 70, 55);
            [object setViewCornerRadius:5];
            [object setBackgroundColor:[UIColor blackColor]];
            object.titleLabel.font = [UIFont font16];
            [object setTitleColor:[UIColor colorWithHexString:@"#DADAE6"] forState:UIControlStateNormal];
            [object setTitle:Lang(@"登录") forState:UIControlStateNormal];
            [object addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
            object.myTop = 30;
            object.myLeft = 35;
            object.mySize = object.size;
            object;
        });
    }
    return _submitBtn;
}

-(UIButton* )forgetBtn{
    if (!_forgetBtn) {
        _forgetBtn = ({
            UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.titleLabel.font = [UIFont font15];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn setTitle:Lang(@"忘记密码?") forState:UIControlStateNormal];
            [btn sizeToFit];
            btn;
        });
        [_forgetBtn addTarget:self action:@selector(forget:) forControlEvents:UIControlEventTouchUpInside];
        _forgetBtn.myCenterY = 0;
        _forgetBtn.myLeft = 0;
        _forgetBtn.mySize = _forgetBtn.size;
    }
    return _forgetBtn;
}

-(UIButton* )switchBtn{
    if (!_switchBtn) {
        _switchBtn = ({
            UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.titleLabel.font = [UIFont font15];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn setTitle:Lang(@"账号密码登录") forState:UIControlStateNormal];
            [btn setTitle:Lang(@"手机号登录") forState:UIControlStateSelected];
            btn.selected = YES;
            [btn sizeToFit];
            btn;
        });
        
        @weakify(self)
        [_switchBtn addAction:^(UIButton *btn) {
            @strongify(self)
            btn.selected = !btn.isSelected;
            [self switchLoginType:btn.isSelected];
        }];
        //[_switchBtn addTarget:self action:@selector(switchLoginType:) forControlEvents:UIControlEventTouchUpInside];
        _switchBtn.myTop = 20;
        _switchBtn.myCenterX = 0;
        _switchBtn.myWidth = MyLayoutSize.wrap;
        _switchBtn.myHeight = 17;
    }
    return _switchBtn;
}

-(UIButton* )regBtn{
    if (!_regBtn) {
        _regBtn = ({
            UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.titleLabel.font = [UIFont font14];
            [btn setTitleColor:[UIColor colorWithHexString:@"#585C6E"] forState:UIControlStateNormal];
            NSArray *txtArr = @[@"还没有账号？", @"注册"];
            NSArray *colorArr = @[[UIColor colorWithHexString:@"#AAAABB"], [UIColor moGreen]];
            NSArray *fontArr = @[[UIFont font15], [UIFont boldFont15]];
            NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
            [btn setAttributedTitle:att forState:UIControlStateNormal];
            [btn sizeToFit];
            btn;
        });
        [_regBtn addAction:^(UIButton *btn) {
            MXRoute(@"PhoneRegisterVC", nil);            
        }];
        _regBtn.myCenterY = 0;
        _regBtn.myRight = 0;
        _regBtn.mySize = _regBtn.size;
    }
    return _regBtn;
}

-(TopBottomView* )numView{
    if (!_numView) {
        _numView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeNormal
                                                                    tipsImg:@"手机号"
                                                                       tips:@"手机号"
                                                                placeholder:@"请输入手机号码"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypePhonePad;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 50;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });

    }
    return _numView;
}

-(VerityCodeView* )codeView{
    if (!_codeView) {
        _codeView = [[VerityCodeView alloc] initWithFrame:CGRectZero];
        _codeView.backgroundColor = [UIColor whiteColor];
        _codeView.tf.textColor = [UIColor blackColor];
        _codeView.myTop = 0;
        _codeView.myLeft = 30;
        _codeView.myRight = 30;
        _codeView.myHeight = 60;
        
        @weakify(self)
        _codeView.sendCode = ^(id data) {
            @strongify(self)
            if ([StringUtil isEmpty:[self phoneNo]]) {
                return NO;
            }
            if ([StringUtil isEmpty:self.img_id]) {
                [NotifyHelper showMessageWithMakeText:@"请刷新图形验证码"];
                return NO;
            }
            if ([StringUtil isEmpty:self.img_code]) {
                [NotifyHelper showMessageWithMakeText:@"请输入图形验证码"];
                return NO;
            }
            [self send_code:@{@"user_account":self.phoneNo,
                              @"bus_type":self.switchBtn.selected ? @"FrontRegister" : @"FrontUserLogin",
                              @"img_id":self.img_id?:@"",
                              @"img_code":self.img_code?:@""}
                 completion:^(BOOL success, NSString *error) {
                
            }];
            return YES;
            };
    }
    return _codeView;
}

- (ImageVerifyView *)imgVerView {
    if (!_imgVerView) {
        _imgVerView = [[ImageVerifyView alloc] initWithFrame:CGRectZero];
        _imgVerView.backgroundColor = [UIColor whiteColor];
        _imgVerView.btn.backgroundColor = [UIColor whiteColor];
        _imgVerView.tf.textColor = [UIColor blackColor];
        _imgVerView.myTop = 0;
        _imgVerView.myLeft = 30;
        _imgVerView.myRight = 30;
        _imgVerView.myHeight = 60;
        @weakify(self)
        _imgVerView.getText = ^(NSString * data) {
            @strongify(self)
            self.img_code = data;
        };
        _imgVerView.picBlock = ^(id  _Nullable data) {
            @strongify(self)
            [self reloadImgVerCode];
        };
        [self reloadImgVerCode];
    }
    return _imgVerView;
}
@end
