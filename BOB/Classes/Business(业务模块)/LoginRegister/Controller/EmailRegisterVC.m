//
//  PhoneRegisterVC.m
//  BOB
//
//  Created by mac on 2019/12/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "EmailRegisterVC.h"
#import "YYTextHelper.h"
#import "RegisterMainView.h"
#import "LoginRegModel.h"
static NSInteger leftEdgeInset = 35;
@interface EmailRegisterVC ()

@property (nonatomic,strong) UILabel           *titleLbl;

@property (nonatomic,strong) RegisterMainView  *mainView;

@property (nonatomic,strong) LoginRegModel  *model;

@end

@implementation EmailRegisterVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setNaviStyle];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self layout];
}

-(void)setupUI{
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.titleLbl];
    [self.view addSubview:self.mainView];
}

-(void)layout{
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(leftEdgeInset));
        make.top.equalTo(@(Status_Height+60));
    }];
    [self.mainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(leftEdgeInset));
        make.right.equalTo(@(-leftEdgeInset));
        make.top.equalTo(self.titleLbl.mas_bottom).offset(100);
        make.height.equalTo(@([RegisterMainView getHeight]));
    }];
}

-(void)setNaviStyle{
    self.navigationController.navigationBarHidden = YES;
}

-(UILabel*)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.textColor = [UIColor moBlack];
        _titleLbl.font = [UIFont boldSystemFontOfSize:27];
        _titleLbl.text = Lang(@"邮箱注册");
    }
    return _titleLbl;
}

+ (UINavigationController *)currentNC
{
    if (![[UIApplication sharedApplication].windows.lastObject isKindOfClass:[UIWindow class]]) {
        NSAssert(0, @"未获取到导航控制器");
        return nil;
    }
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    return [self getCurrentNCFrom:rootViewController];
}
//递归
+ (UINavigationController *)getCurrentNCFrom:(UIViewController *)vc
{
    if ([vc isKindOfClass:[UITabBarController class]]) {
        UINavigationController *nc = ((UITabBarController *)vc).selectedViewController;
        return [self getCurrentNCFrom:nc];
    }
    else if ([vc isKindOfClass:[UINavigationController class]]) {
        if (((UINavigationController *)vc).presentedViewController) {
            return [self getCurrentNCFrom:((UINavigationController *)vc).presentedViewController];
        }
        return [self getCurrentNCFrom:((UINavigationController *)vc).topViewController];
    }
    else if ([vc isKindOfClass:[UIViewController class]]) {
        if (vc.presentedViewController) {
            return [self getCurrentNCFrom:vc.presentedViewController];
        }
        else {
            return vc.navigationController;
        }
    }
    else {
        NSAssert(0, @"未获取到导航控制器");
        return nil;
    }
}

-(RegisterMainView*)mainView{
    if (!_mainView) {
        _mainView = [RegisterMainView new];
        if (!_model) {
            _model = [[LoginRegModel alloc]init];
            _model.type = RegisterEmailType;
        }
        _mainView.backgroundColor = [UIColor whiteColor];
        [_mainView configModel:self.model];
        @weakify(self)
        _mainView.regBlock = ^{
            @strongify(self)
            [self.navigationController popViewControllerAnimated:YES];
        };
        _mainView.loginBlock = ^{
           @strongify(self)
           [self.navigationController popToRootViewControllerAnimated:YES];
       };
        _mainView.nextBlock = ^(id data) {
            @strongify(self)
           [MXRouter openURL:@"lcwl://SetPwdVC" parameters:@{@"model":self.model}];
        };
    }
    return _mainView;
}
@end
