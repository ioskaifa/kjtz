//
//  PhoneRegisterVC.m
//  BOB
//
//  Created by mac on 2019/12/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "ForgetPwdVC.h"
#import "VerityCodeView.h"
#import "ImageVerifyView.h"
#import "NSObject+LoginHelper.h"
#import "TopBottomView.h"
#import "UIImage+Color.h"

@interface ForgetPwdVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) TopBottomView *numView;

@property (nonatomic, strong) VerityCodeView  *codeView;

@property (nonatomic, strong) ImageVerifyView  *imgVerView;

@property (nonatomic, strong) TopBottomView  *pwdView;
@property (nonatomic, strong) TopBottomView  *rePwdView;
@property (nonatomic, strong) UIButton        *submitBtn;
@property (nonatomic, copy) NSString *img_id;
@property (nonatomic, copy) NSString *img_code;
@end

@implementation ForgetPwdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self reloadImgVerCode];
}

#pragma mark - Private Method
- (void)reloadImgVerCode {
    [self createImgCode:@{@"interface_type":@"createImgCode"}
             completion:^(id object, NSString *error) {
        if (object) {
            if (object[@"img_io"]) {
                [self.imgVerView reloadImage:[UIImage imageWithDataString:object[@"img_io"]]];
            }
            self.img_id = object[@"img_id"]?:@"";
        }
    }];
}

- (NSString *)phoneNo {
    NSString *phone = [StringUtil trim:self.numView.value];
    if ([StringUtil isEmpty:phone]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不能为空"];
        return @"";
    }
    if (![DCCheckRegular dc_checkTelNumber:phone]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不正确"];
        return @"";
    }
    return phone;
}

- (BOOL)valiParam {
    NSString *phoneNo = [StringUtil trim:self.numView.value];
    if ([StringUtil isEmpty:phoneNo]) {
        [NotifyHelper showMessageWithMakeText:@"手机不能为空"];
        return NO;
    }
    if (![DCCheckRegular dc_checkTelNumber:phoneNo]) {
        [NotifyHelper showMessageWithMakeText:@"手机号不正确"];
        return NO;
    }
    if ([StringUtil isEmpty:self.img_code]) {
        [NotifyHelper showMessageWithMakeText:@"请输入图形验证码"];
        return NO;
    }
    if ([StringUtil isEmpty:self.codeView.tf.text]) {
        [NotifyHelper showMessageWithMakeText:@"请输入验证码"];
        return NO;
    }
    if ([StringUtil isEmpty:self.pwdView.value]) {
        [NotifyHelper showMessageWithMakeText:@"请输入密码"];
        return NO;
    }
    if (!(self.pwdView.value.length >= 6 && self.pwdView.value.length <=16)) {
        [NotifyHelper showMessageWithMakeText:@"登录密码长度应在6~16之间"];
        return NO;
    }
    if (![self.pwdView.value isEqualToString:self.rePwdView.value]) {
        [NotifyHelper showMessageWithMakeText:@"两次密码不一致"];
        return NO;
    }
    return YES;
}

- (void)commit {
    NSString *phoneNo = [StringUtil trim:self.numView.value];
    NSDictionary *param = @{@"login_password":self.pwdView.value?:@"",
                            @"user_account":phoneNo?:@"",
                            @"sys_user_account":phoneNo?:@"",
                            @"sms_code":self.codeView.tf.text?:@""};
    [self userForgetPass:param
              completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            UDetail.user.user_tel = self.numView.value;
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - InitUI
-(void)setupUI {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    MyLinearLayout *layout= [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 10;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self.view addSubview:layout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont30];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"忘记密码？";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 30;
    lbl.myTop = 35;
    [layout addSubview:lbl];
    
    [layout addSubview:self.numView];
    UIButton *tipsBtn = [TopBottomView tipsBtnWithtipsImg:@"图形验证码" tips:@"图形验证码"];
    tipsBtn.myTop = 10;
    tipsBtn.myLeft = 30;    
    [layout addSubview:tipsBtn];
    [layout addSubview:self.imgVerView];
    
    tipsBtn = [TopBottomView tipsBtnWithtipsImg:@"验证码" tips:@"验证码"];
    tipsBtn.myTop = 10;
    tipsBtn.myLeft = 30;
    [layout addSubview:tipsBtn];
    [layout addSubview:self.codeView];
    
    [layout addSubview:self.pwdView];
    [layout addSubview:self.rePwdView];
    
    [layout addSubview:self.submitBtn];
    
    [layout layoutSubviews];
    self.tableView.tableHeaderView = layout;
}

-(TopBottomView* )numView{
    if (!_numView) {
        _numView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeNormal
                                                                    tipsImg:@"手机号"
                                                                       tips:@"手机号"
                                                                placeholder:@"请输入手机号码"];
            NSString *phoneNo = UDetail.user.user_tel?:@"";
            if (![StringUtil isEmpty:phoneNo]) {
                object.tf.text = phoneNo;
            }
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 50;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });

    }
    return _numView;
}

-(VerityCodeView* )codeView{
    if (!_codeView) {
        _codeView = [[VerityCodeView alloc] initWithFrame:CGRectZero];
        _codeView.backgroundColor = [UIColor whiteColor];
        _codeView.myTop = 0;
        _codeView.myLeft = 30;
        _codeView.myRight = 30;
        _codeView.myHeight = 60;
        @weakify(self)
        _codeView.sendCode = ^(id data) {
            @strongify(self)
            if ([StringUtil isEmpty:[self phoneNo]]) {
                return NO;
            }
            if ([StringUtil isEmpty:self.img_id]) {
                [NotifyHelper showMessageWithMakeText:@"请刷新图形验证码"];
                return NO;
            }
            if ([StringUtil isEmpty:self.img_code]) {
                [NotifyHelper showMessageWithMakeText:@"请输入图形验证码"];
                return NO;
            }
            [self send_code:@{@"user_account":self.phoneNo,
                              @"sys_user_account":self.phoneNo,
                              @"bus_type":@"FrontForgetPass",
                              @"img_id":self.img_id?:@"",
                              @"img_code":self.img_code?:@""}
                 completion:^(BOOL success, NSString *error) {
                
            }];
            return YES;
            };
    }
    return _codeView;
}

- (ImageVerifyView *)imgVerView {
    if (!_imgVerView) {
        _imgVerView = [[ImageVerifyView alloc] initWithFrame:CGRectZero];
        _imgVerView.backgroundColor = [UIColor whiteColor];
        _imgVerView.myTop = 0;
        _imgVerView.myLeft = 30;
        _imgVerView.myRight = 30;
        _imgVerView.myHeight = 60;
        @weakify(self)
        _imgVerView.getText = ^(NSString * data) {
            @strongify(self)
            self.img_code = data;
        };
        _imgVerView.picBlock = ^(id  _Nullable data) {
            @strongify(self)
            [self reloadImgVerCode];
        };
    }
    return _imgVerView;
}

-(TopBottomView* )pwdView{
    if (!_pwdView) {
        _pwdView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeSecure
                                                                    tipsImg:@"登录密码"
                                                                       tips:@"登录密码"
                                                                placeholder:@"请设置新登录密码（6~16位字符组合）"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 10;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });
    }
    return _pwdView;
}

-(TopBottomView* )rePwdView{
    if (!_rePwdView) {
        _rePwdView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeSecure
                                                                    tipsImg:@"登录密码"
                                                                       tips:@"确认密码"
                                                                placeholder:@"请确认登录密码"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 10;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });
    }
    return _rePwdView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(SCREEN_WIDTH - 70, 55);
            [object setViewCornerRadius:5];
            [object setBackgroundColor:[UIColor blackColor]];
            object.titleLabel.font = [UIFont boldFont16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setTitle:Lang(@"确定") forState:UIControlStateNormal];
            object.myTop = 45;
            object.myLeft = 35;
            object.mySize = object.size;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
               @strongify(self);
               if ([self valiParam]) {
                   [self commit];
               }
            }];
            object;
        });
    }
    return _submitBtn;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}

@end
