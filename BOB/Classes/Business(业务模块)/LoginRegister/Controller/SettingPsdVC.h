//
//  SettingPsdVC.h
//  BOB
//
//  Created by mac on 2020/9/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "LoginRegModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SettingPsdVC : BaseViewController

@property (nonatomic,strong) LoginRegModel  *model;

@end

NS_ASSUME_NONNULL_END
