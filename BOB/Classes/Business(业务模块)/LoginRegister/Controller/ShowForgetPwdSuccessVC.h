//
//  ShowForgetPwdSuccessVC.h
//  BOB
//
//  Created by mac on 2020/7/2.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShowForgetPwdSuccessVC : UIViewController

@property (nonatomic, copy)FinishedBlock  block;

+ (instancetype)showInViewController:(UIViewController *)vc;

@end

NS_ASSUME_NONNULL_END
