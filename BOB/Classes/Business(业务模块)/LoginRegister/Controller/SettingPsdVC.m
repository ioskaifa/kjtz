//
//  SettingPsdVC.m
//  BOB
//
//  Created by mac on 2020/9/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SettingPsdVC.h"
#import "NSObject+LoginHelper.h"
#import "TopBottomView.h"
#import "UIImage+Color.h"

@interface SettingPsdVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) TopBottomView  *pwdView;
@property (nonatomic, strong) TopBottomView  *rePwdView;

@property (nonatomic, strong) TopBottomView  *payView;
@property (nonatomic, strong) TopBottomView  *rePayView;

@property (nonatomic, strong) UIButton        *submitBtn;

@end

@implementation SettingPsdVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (BOOL)valiParam {
    self.model.password = [StringUtil trim:self.pwdView.value];
    if ([StringUtil isEmpty:self.model.password]) {
        [NotifyHelper showMessageWithMakeText:@"请输入登录密码"];
        return NO;
    }
    if (!(self.pwdView.value.length >= 6 && self.pwdView.value.length <=16)) {
        [NotifyHelper showMessageWithMakeText:@"登录密码长度应在6~16之间"];
        return NO;
    }
    if (![self.pwdView.value isEqualToString:self.rePwdView.value]) {
        [NotifyHelper showMessageWithMakeText:@"两次登录密码不一致"];
        return NO;
    }
    
    self.model.pay_password = [StringUtil trim:self.payView.value];
//    if ([StringUtil isEmpty:self.model.pay_password]) {
//        [NotifyHelper showMessageWithMakeText:@"请输入支付密码"];
//        return NO;
//    }
//    if (![self.payView.value isEqualToString:self.rePayView.value]) {
//        [NotifyHelper showMessageWithMakeText:@"两次支付密码不一致"];
//        return NO;
//    }
//    if (self.model.pay_password.length != 6) {
//        [NotifyHelper showMessageWithMakeText:@"支付密码长度应为6位"];
//        return NO;
//    }
//    if (![StringUtil isNumberString:self.model.pay_password]) {
//        [NotifyHelper showMessageWithMakeText:@"支付密码只能为数字"];
//        return NO;
//    }
    return YES;
}

- (void)commit {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    @weakify(self)
    [self register2:@{@"login_password":self.model.password?:@"",
//                      @"pay_password":self.model.pay_password ?: @"",
                      @"user_account":self.model.phoneNo?:@"",
                      @"valid_flag":self.model.valid_flag?:@""}
         completion:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:self.view animated:YES];
        if (success) {
            @strongify(self)
            if (object[@"token"]) {
                UserModel *user = [UserModel new];
                user.token = object[@"token"];
                user.user_tel = self.model.phoneNo;
                [UDetail updateUserModel:user];
            }
            MXRoute(@"ChooseSexVC", nil);
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark - InitUI
-(void)setupUI {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self setNavBarTitle:@"密码设置"];
    MyLinearLayout *layout= [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self.view addSubview:layout];
    
    [layout addSubview:self.pwdView];
    [layout addSubview:self.rePwdView];
//    [layout addSubview:self.payView];
//    [layout addSubview:self.rePayView];
    
    [layout addSubview:self.submitBtn];
    
    [layout layoutSubviews];
    self.tableView.tableHeaderView = layout;
}

-(TopBottomView* )pwdView{
    if (!_pwdView) {
        _pwdView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeSecure
                                                                    tipsImg:@"登录密码"
                                                                       tips:@"登录密码"
                                                                placeholder:@"请设置登录密码（6~16位字符组合）"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 10;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });
    }
    return _pwdView;
}

-(TopBottomView* )rePwdView{
    if (!_rePwdView) {
        _rePwdView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeSecure
                                                                    tipsImg:@"登录密码"
                                                                       tips:@"确认密码"
                                                                placeholder:@"请确认登录密码"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 10;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });
    }
    return _rePwdView;
}

-(TopBottomView* )payView{
    if (!_payView) {
        _payView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeSecure
                                                                    tipsImg:@"支付密码"
                                                                       tips:@"支付密码"
                                                                placeholder:@"请设置支付密码（6位数字）"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumberPad;
            object.tf.returnKeyType = UIReturnKeyNext;
            object.myTop = 10;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });
    }
    return _payView;
}

-(TopBottomView* )rePayView{
    if (!_rePayView) {
        _rePayView = ({
            TopBottomView *object = [[TopBottomView alloc] initWithViewType:TopBottomViewTypeSecure
                                                                    tipsImg:@"支付密码"
                                                                       tips:@"确认密码"
                                                                placeholder:@"请确认支付密码"];
            object.tf.delegate = self;
            object.tf.keyboardType = UIKeyboardTypeNumberPad;
            object.tf.returnKeyType = UIReturnKeyDone;
            object.myTop = 10;
            object.myLeft = 30;
            object.myRight = 30;
            object.myHeight = 70;
            object;
        });
    }
    return _rePayView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(SCREEN_WIDTH - 70, 55);
            [object setViewCornerRadius:5];
            [object setBackgroundColor:[UIColor blackColor]];
            object.titleLabel.font = [UIFont boldFont16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setTitle:Lang(@"确定") forState:UIControlStateNormal];
            object.myTop = 45;
            object.myLeft = 35;
            object.mySize = object.size;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
               @strongify(self);
               if ([self valiParam]) {
                   [self commit];
               }
            }];
            object;
        });
    }
    return _submitBtn;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}

@end
