//
//  SetPwdVC.h
//  BOB
//
//  Created by mac on 2019/12/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "LoginRegModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SetPwdVC : BaseViewController

@property(nonatomic, strong) NSString *account;
@property(nonatomic, strong) NSString *phone;
@property (nonatomic,strong) LoginRegModel* model;

@end

NS_ASSUME_NONNULL_END
