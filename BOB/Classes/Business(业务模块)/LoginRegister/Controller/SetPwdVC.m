//
//  PhoneRegisterVC.m
//  BOB
//
//  Created by mac on 2019/12/4.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "SetPwdVC.h"
#import "YYTextHelper.h"
#import "SetPwdMainView.h"
#import "SPButton.h"
#import "NSObject+LoginHelper.h"

static NSInteger leftEdgeInset = 35;
@interface SetPwdVC ()

@property (nonatomic,strong) UILabel           *titleLbl;

@property (nonatomic,strong) SetPwdMainView  *mainView;

@property (nonatomic,strong) SPButton* backBtn;

@end

@implementation SetPwdVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setNaviStyle];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
    [self layout];
}

-(void)setupUI{
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.backBtn];
    [self.view addSubview:self.titleLbl];
    [self.view addSubview:self.mainView];
}

-(void)layout{
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLbl);
        make.top.equalTo(@(Status_Height+20));
        make.width.equalTo(@(40));
        make.height.equalTo(@(25));
    }];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(leftEdgeInset));
        make.top.equalTo(@(Status_Height+88));
    }];
    [self.mainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(leftEdgeInset));
        make.right.equalTo(@(-leftEdgeInset));
        make.top.equalTo(self.titleLbl.mas_bottom).offset(60);
        make.height.equalTo(@([SetPwdMainView getHeight]));
    }];
}

-(void)setNaviStyle{
    self.navigationController.navigationBarHidden = YES;
}

-(UILabel*)titleLbl{
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.numberOfLines = 0;
        NSString* str1 = Lang(@"设置密码");
        NSString* str2 = Lang(@"请输入8~20个非纯数字的字符");
        NSMutableAttributedString* attr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\n%@",str1,str2]];
        [attr addFont:[UIFont boldSystemFontOfSize:27] substring:str1];
        [attr addFont:[UIFont font12] substring:str2];
        [attr addColor:[UIColor moBlack] substring:str1];
        [attr addColor:[UIColor moBlack] substring:str2];
        [attr setLineSpacing:8];
        _titleLbl.attributedText = attr;
    }
    return _titleLbl;
}

- (SPButton *)backBtn{
    if (!_backBtn) {
        _backBtn = [[SPButton alloc] initWithImagePosition:SPButtonImagePositionLeft];
        _backBtn.frame = CGRectMake(0, 0, 40, 25);
        [_backBtn setTitle:Lang(@"<返回") forState:UIControlStateNormal];
        [_backBtn setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
        _backBtn.titleLabel.font = [UIFont systemFontOfSize:13];
//        [_backBtn setImage:[UIImage imageNamed:@"Arrow"] forState:UIControlStateNormal];
//        [_backBtn setImageTitleSpace:5];
        [_backBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}

-(void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

+ (UINavigationController *)currentNC
{
    if (![[UIApplication sharedApplication].windows.lastObject isKindOfClass:[UIWindow class]]) {
        NSAssert(0, @"未获取到导航控制器");
        return nil;
    }
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    return [self getCurrentNCFrom:rootViewController];
}
//递归
+ (UINavigationController *)getCurrentNCFrom:(UIViewController *)vc
{
    if ([vc isKindOfClass:[UITabBarController class]]) {
        UINavigationController *nc = ((UITabBarController *)vc).selectedViewController;
        return [self getCurrentNCFrom:nc];
    }
    else if ([vc isKindOfClass:[UINavigationController class]]) {
        if (((UINavigationController *)vc).presentedViewController) {
            return [self getCurrentNCFrom:((UINavigationController *)vc).presentedViewController];
        }
        return [self getCurrentNCFrom:((UINavigationController *)vc).topViewController];
    }
    else if ([vc isKindOfClass:[UIViewController class]]) {
        if (vc.presentedViewController) {
            return [self getCurrentNCFrom:vc.presentedViewController];
        }
        else {
            return vc.navigationController;
        }
    }
    else {
        NSAssert(0, @"未获取到导航控制器");
        return nil;
    }
}

-(SetPwdMainView*)mainView{
    if (!_mainView) {
        _mainView = [SetPwdMainView new];
        [_mainView configModel:self.model];
        _mainView.backgroundColor = [UIColor whiteColor];
        @weakify(self);
        _mainView.regBlock = ^(id data){
            @strongify(self)
            NSMutableDictionary *muDic = [data mutableCopy];
            [muDic setValue:self.account forKey:@"account"];
            [muDic setValue:self.phone forKey:@"phone"];
            [self register2:muDic completion:^(BOOL success,id object,NSString *error) {
                if(success) {
                    [self.navigationController popToRootViewControllerAnimated:YES];
//                    [self login:@{@"login_type":@"token",@"token":@""} completion:^(BOOL success, NSString *error) {
//                        if (success) {
//                            if (!MoApp.mainVC) {
//                                MoApp.mainVC = [[MainViewController alloc] init];
//                            }
//                            //通知语言更新
//                            [[[UIApplication sharedApplication] keyWindow] setRootViewController:MoApp.mainVC];
//                        }
//                    }];
                }
            }];
        };
    }
    return _mainView;
}
@end
