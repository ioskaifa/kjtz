//
//  RealNameAuthVC.m
//  BOB
//
//  Created by mac on 2020/7/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "RealNameAuthVC.h"
#import "PhotoBrowser.h"
#import "QNManager.h"

@interface RealNameAuthVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *defultImgArr;

@property (nonatomic, strong) NSMutableDictionary *dataImgMDic;

@end

@implementation RealNameAuthVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self setWhiteNavStyle];
}

- (void)navBarRightBtnAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)configCurrentVC {
    
}

#pragma mark - Private Method
- (BOOL)valiParam {
    if (self.dataImgMDic.count != self.defultImgArr.count) {
        [NotifyHelper showMessageWithMakeText:@"请上传证件照"];
        return NO;
    }
    return YES;
}

#pragma mark - IBAction
- (void)imgViewClick:(UIView *)sender {
    if (![sender isKindOfClass:UIImageView.class]) {
        return;
    }
    UIImageView *imgView = (UIImageView *)sender;
    [[PhotoBrowser shared] showSelectSinglePhotoLibrary:self completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
            if([images isKindOfClass:[NSArray class]]) {
                if([[images firstObject] isKindOfClass:[UIImage class]]) {
                    UIImage *image = [images firstObject];
                    imgView.image = image;
                    [NotifyHelper showHUDAddedTo:self.view animated:YES];
                    [[QNManager shared] uploadImage:image completion:^(id data) {
                        [NotifyHelper hideHUDForView:self.view animated:YES];
                        if(data && [NSURL URLWithString:data]) {
                            [self.dataImgMDic setValue:data forKey:[imgView description]];
                        }
                    }];
                }
            }
        }];
}

- (void)commit:(UIButton *)sender {
    NSString *url = StrF(@"%@/%@", LcwlServerRoot, @"api/user/info/submitRealnameInfo");
    NSArray *urlArr = self.dataImgMDic.allValues;
    NSDictionary *param = @{@"realname_photo":[urlArr componentsJoinedByString:@","]};
    [self request:url param:param completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            [self navBarRightBtnAction:nil];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"实名认证"];
    [self.view addSubview:self.tableView];
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    [self initHeaderView];
}

- (void)initHeaderView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"#2A2931"];
    lbl.text = @"请确保照片的清晰度";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 20;
    lbl.myTop = 20;
    [layout addSubview:lbl];
    
    MyTableLayout *table = [MyTableLayout tableLayoutWithOrientation:MyOrientation_Vert];
    table.myTop = 15;
    table.myLeft = 20;
    table.myRight = 20;
    table.myHeight = MyLayoutSize.wrap;
    table.subviewSpace = 15;
    CGFloat with = (SCREEN_WIDTH - 40 - 15) / 2.0;
    [table addRow:MyLayoutSize.wrap colSize:MyLayoutSize.wrap];
    for (NSInteger i = 0; i < self.defultImgArr.count; i++) {
        NSString *imgName = self.defultImgArr[i];
        UIImage *img = [UIImage imageNamed:imgName];
        CGFloat height = img.size.height * with / img.size.width;
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
        [imgView setViewCornerRadius:3];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.size = CGSizeMake(with, height);
        imgView.tag = i;
        @weakify(self)
        [imgView addAction:^(UIView *view) {
            @strongify(self)
            [self imgViewClick:view];
        }];
        [table addSubview:imgView];
        if ((i+1) % 2 == 0) {
            [table addRow:MyLayoutSize.wrap colSize:MyLayoutSize.wrap];
        }
    }
    [layout addSubview:table];
    
    UIButton *commitBtn = [UIButton new];
    commitBtn.backgroundColor = [UIColor colorWithHexString:@"#0088FF"];
    [commitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    commitBtn.titleLabel.font = [UIFont font15];
    [commitBtn setTitle:@"完成" forState:UIControlStateNormal];
    commitBtn.height = 34;
    [commitBtn setViewCornerRadius:commitBtn.height/2.0];
    commitBtn.myHeight = commitBtn.height;
    commitBtn.myTop = 40;
    commitBtn.myLeft = 15;
    commitBtn.myRight = 15;
    @weakify(self)
    [commitBtn addAction:^(UIButton *btn) {
        @strongify(self)
        if ([self valiParam]) {
            [self commit:btn];
        }
    }];
    [layout addSubview:commitBtn];
    
    [layout layoutSubviews];
    self.tableView.tableHeaderView = layout;
}

#pragma mark - Init
- (NSArray *)defultImgArr {
    if (!_defultImgArr) {
        _defultImgArr = @[@"身份证人像面", @"身份证国徽面", @"手持身份证"];
    }
    return _defultImgArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        AdjustTableBehavior(_tableView);
    }
    return _tableView;
}

- (NSMutableDictionary *)dataImgMDic {
    if (!_dataImgMDic) {
        _dataImgMDic = [NSMutableDictionary dictionaryWithCapacity:self.defultImgArr.count];
    }
    return _dataImgMDic;
}

@end
