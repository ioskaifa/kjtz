//
//  CountryCodeVC.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/1/29.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "CountryCodeVC.h"
#import "MXSeparatorLine.h"
#import "UINavigationBar+Alpha.h"
#import "CountryHelper.h"
#import "UIImage+Color.h"
#import "MXBackButton.h"
#define HeaderHeight 64

/// 有商圈
static const NSInteger haveBusiness = 1;
/// 选择的是中国
static const NSInteger chinese = 1;
/// 有下一级地址
static const NSInteger haveMore = 0;

@interface CountryCodeVC ()

@property (strong, nonatomic) CountryCodeEntity *entity;

@property (nonatomic,strong) UIButton         *backBtn;

@end
#define personal_details_backbutton_width 60
#define personal_details_backbutton_height 20
@implementation CountryCodeVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.entity = [[CountryCodeEntity alloc]init];
        self.pid = -1;
        self.title =  Lang(@"选择国家和地区");
        UIImageView *viewblu = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        NSString *imagePath = nil;

        imagePath = @"图层 3";
        UIImage *img = [UIImage imageNamed:imagePath];
        viewblu.image = [UIImage blurryImage:img withBlurLevel:1.5];
        [self.view insertSubview:viewblu belowSubview:self.countryTable];
        
        self.countryTable.sectionIndexBackgroundColor = [UIColor clearColor];
        self.countryTable.sectionIndexColor = [UIColor whiteColor];
        self.countryTable.backgroundColor = [UIColor clearColor];
    }
    return self;
}
-(void)addDidSelectCallBack:(CountryCodeDidSelectCallBack)callback{
    @weakify(self)
    self.entity.callBack = ^(NSArray *countrys ,BOOL isSelectedLocation){
        @strongify(self)
        CountryCodeModel* model = countrys[0];
        NSInteger selectType;
        
        switch (self.entity.type) {
            case MOBILE:
                [self.navigationController popViewControllerAnimated:YES];
                callback(countrys,NO);
                return;
            case CITY:
            {
//                CountryCodeModel *tmpModel =[CountryHelper sharedDataBase].selectedCountrys.firstObject;
                if ((model.currentId == chinese && model.level == 3) || (model.currentId != chinese && model.bussniss == haveBusiness)) {
                    [self back:callback model:model];
                    return;
                } else if (model.hasNext != haveMore && model.bussniss == haveBusiness) {
                    selectType = Bussness;
                    [self next:model selectType:selectType callback:callback];
                    return;
                } else {
                    selectType = AREA;
                }
                
                break;
            }
            case COUNTRY:
                selectType = CITY;
                break;
            case AREA:
                selectType = STREET;
                break;
            case STATE:
                selectType = CITY;
                break;
            case STREET:
                selectType = Bussness;
                if (model.bussniss != haveBusiness) {
                    [self back:callback model:model];
                    return;
                }
                break;
            case Bussness:
                [self back:callback model:model];
                return;
            default:
                return;
        }
        
        if(isSelectedLocation){
            [self back:callback model:model];
            return;
        }
        
        //fly 2015.11.04 排除街道的数据比较特殊，是否使用街道需要外界shouldUseStreet控制
        if (model.hasNext != haveMore ||
            (selectType == STREET && !self.shouldUseStreet)) {
            [self back:callback model:model];
        } else {
            [self next:model selectType:selectType callback:callback];
        }
    };
}

- (void)back:(CountryCodeDidSelectCallBack)callback model:(CountryCodeModel *)model
{
   
}

- (void)next:(CountryCodeModel *)model selectType:(NSInteger)type callback:(CountryCodeDidSelectCallBack)callback
{
//    [[CountryHelper sharedDataBase].selectedCountrys safeAddObj:model];
//    CountryCodeVC *countryVC = [[CountryCodeVC alloc] initWithNibName:@"CountryCodeVC" bundle:nil];
//    countryVC.title = MXLang(@"Country_CountryCodeVC_Title_2", @"地区");
//    countryVC.type = (CountryType)type;
//    //fly 2015.11.04 排除街道的数据比较特殊，是否使用街道需要外界shouldUseStreet控制
//    countryVC.shouldUseStreet = self.shouldUseStreet;
//    countryVC.pid = model.currentId;
//    [countryVC addDidSelectCallBack:callback];
//    [self.navigationController pushViewController:countryVC animated:YES];
}

-(void)backAction:(id)sender{
    [super backAction:sender];
}
-(void)popToRootViewController{
    NSArray* controllers = self.navigationController.viewControllers;
    int rootIndex = 0;
    for (int i=0 ; i<controllers.count ; i++) {
        UIViewController* subController = (UIViewController*)[controllers safeObjectAtIndex:i];
        if ([subController isKindOfClass:[CountryCodeVC class]]) {
            rootIndex = i-1;
            break;
        }
    }
    [self.navigationController popToViewController:controllers[rootIndex] animated:YES];
}

-(void)initDelegate{
    self.countryTable.dataSource = self.entity;
    self.countryTable.delegate = self.entity;
    self.countryTable.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)initTableData{
    BOOL needSimpleHeaderView = YES;
    if (self.selectedModel == nil ||
        [StringUtil isEmpty:self.selectedModel.cs_name] ||
        [StringUtil isEmpty:self.selectedModel.en_name]) {
        needSimpleHeaderView = NO;
    }
    
    self.entity.countryCodeArray = [[CountryHelper sharedDataBase]readMobileCountryDataFromDB];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeAll;
    }
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar barReset];
    [self.navigationController.navigationBar setNavBarCurrentColor:[UIColor clearColor] titleTextColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    [self initTableData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.entity.type = self.type;
    [self.countryTable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@(88));
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    AdjustTableBehavior(self.countryTable)
    self.countryTable.backgroundColor = [UIColor clearColor];
    self.isShowBackButton=YES;
    // 此返回按钮是白色的
    
    UIBarButtonItem *barBtnItem = [[UIBarButtonItem alloc] initWithCustomView:self.backBtn];
    self.navigationItem.leftBarButtonItem = barBtnItem;
   [self initDelegate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIButton*)backBtn{
    if (!_backBtn) {
        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backBtn setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateNormal];
        [_backBtn setTitle:Lang(@" 返回") forState:UIControlStateNormal];
        [_backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _backBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backBtn;
}
@end
