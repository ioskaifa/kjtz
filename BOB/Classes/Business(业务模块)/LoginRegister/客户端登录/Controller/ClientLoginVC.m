//
//  ClientLoginVC.m
//  BOB
//
//  Created by mac on 2020/8/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ClientLoginVC.h"
#import "ChatSendHelper.h"

@interface ClientLoginVC ()

@end

@implementation ClientLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setWhiteNavStyle];
    [ChatSendHelper sendClientMessageBySubType:kMXClientTypeIdentify];
}

- (void)configCurrentVC {
    
}

#pragma mark - IBAction
- (void)loginBtnClick:(UIButton *)sender {
    UDetail.user.client_status = YES;
    [ChatSendHelper sendClientMessageBySubType:kMXClientTypeConfirmLogin];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)cancelBtnClick:(UIButton *)sender {
    UDetail.user.client_status = NO;
    UDetail.user.client_type = @"";
    [ChatSendHelper sendClientMessageBySubType:kMXClientTypeCancelLogin];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - InitUI
- (void)createUI {
    [self initNavLeftItem];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"icon_client_logo"];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myCenterX = 0;
    imgView.myTop = 150;
    [layout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font20];
    lbl.textColor = [UIColor blackColor];
    lbl.text = StrF(@"%@ %@%@", UDetail.user.client_type?:@"Windows", [DeviceManager getAppName],@"登录确认");
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myTop = 20;
    [layout addSubview:lbl];
    
    UIView *view = [UIView new];
    view.weight = 1;
    view.myWidth = 1;
    view.myCenterX = 0;
    [layout addSubview:view];
    
    UIButton *btn = [UIButton new];
    btn.size = CGSizeMake(200, 40);
    [btn setViewCornerRadius:btn.height/2.0];
    btn.titleLabel.font = [UIFont font17];
    [btn setTitleColor:[UIColor colorWithHexString:@"#0088FF"] forState:UIControlStateNormal];
    [btn setTitle:@"登录" forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor moBackground]];
    btn.mySize = btn.size;
    btn.myCenterX = 0;
    @weakify(self)
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self loginBtnClick:btn];
    }];
    [layout addSubview:btn];
    
    btn = [UIButton new];
    btn.titleLabel.font = [UIFont font17];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitle:@"取消登录" forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.mySize = btn.size;
    btn.myCenterX = 0;
    btn.myTop = 50;
    btn.myBottom = 100;
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self cancelBtnClick:btn];
    }];
    [layout addSubview:btn];
}

- (void)initNavLeftItem {
    UIButton *btn = [UIButton new];
    @weakify(self)
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }];
    btn.titleLabel.font = [UIFont font17];
    [btn setTitleColor:[UIColor colorWithHexString:@"#0088FF"] forState:UIControlStateNormal];
    [btn setTitle:@"关闭" forState:UIControlStateNormal];
    [btn sizeToFit];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = item;
}

@end
