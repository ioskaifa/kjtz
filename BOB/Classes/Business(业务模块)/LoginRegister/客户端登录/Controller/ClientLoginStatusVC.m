//
//  ClientLoginStatusVC.m
//  BOB
//
//  Created by mac on 2020/8/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ClientLoginStatusVC.h"
#import "UIActionSheet+MKBlockAdditions.h"
#import "ChatSendHelper.h"

@interface ClientLoginStatusVC ()

@property (nonatomic, strong) UIButton *loginStatusBtn;

@property (nonatomic, strong) UIButton *slienceBtn;

@end

@implementation ClientLoginStatusVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setWhiteNavStyle];
}

- (void)configCurrentVC {
    
}

#pragma mark - IBAction
- (void)slienceBtnClick:(UIButton *)sender {
    NSString *title = UDetail.user.client_silenceStatus ? @"关闭“手机静音”后，手机新消息通知将恢复正常":@"启用“手机静音”后，手机上将不再接收新消息通知";
    NSString *text = UDetail.user.client_silenceStatus ? @"恢复手机通知":@"停止手机通知";
    [UIActionSheet actionSheetWithTitle:title
                                message:nil
                                buttons:@[text]
                             showInView:self.view
                              onDismiss:^(NSInteger buttonIndex) {
        sender.selected = !sender.selected;
        self.loginStatusBtn.selected = !self.loginStatusBtn.selected;
        UDetail.user.client_silenceStatus = sender.selected;
    } onCancel:nil];
}

- (void)loginOut:(UIButton *)sender {
    NSString *title = StrF(@"%@%@%@", @"是否退出",UDetail.user.client_type?:@"",[DeviceManager getAppName]);
    [UIActionSheet actionSheetWithTitle:title
                                message:nil
                                buttons:@[@"退出"]
                             showInView:self.view
                              onDismiss:^(NSInteger buttonIndex) {
        UDetail.user.client_status = NO;
        UDetail.user.client_silenceStatus = NO;
        [ChatSendHelper sendClientMessageBySubType:kMXClientTypeCancelLogin];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    } onCancel:nil];
}

#pragma mark - Init
- (void)createUI {
    [self initNavLeftItem];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    
    UIButton *btn = [UIButton new];
    [btn setImage:[UIImage imageNamed:@"icon_client_login_dissilence"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"icon_client_login_silence"] forState:UIControlStateSelected];
    [btn sizeToFit];
    btn.mySize = btn.size;
    btn.myCenterX = 0;
    btn.myTop = 150;
    btn.selected = UDetail.user.client_silenceStatus;
    self.loginStatusBtn = btn;
    [layout addSubview:btn];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont20];
    lbl.textColor = [UIColor blackColor];
    lbl.text = StrF(@"%@ %@%@", UDetail.user.client_type?:@"Windows", [DeviceManager getAppName],@"登录确认");
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myTop = 20;
    [layout addSubview:lbl];
    
    SPButton *slienceBtn = [SPButton new];
    slienceBtn.imagePosition = SPButtonImagePositionTop;
    slienceBtn.imageTitleSpace = 10;
    slienceBtn.titleLabel.font = [UIFont font14];
    [slienceBtn setTitleColor:[UIColor colorWithHexString:@"#AEAEAE"] forState:UIControlStateNormal];
    [slienceBtn setImage:[UIImage imageNamed:@"icon_client_dissilence"] forState:UIControlStateNormal];
    [slienceBtn setImage:[UIImage imageNamed:@"icon_client_silence"] forState:UIControlStateSelected];
    [slienceBtn setTitle:@"手机静音" forState:UIControlStateNormal];
    [slienceBtn sizeToFit];
    slienceBtn.mySize = CGSizeMake(slienceBtn.width + 10, slienceBtn.height);
    slienceBtn.myCenterX = 0;
    slienceBtn.myTop = 55;
    @weakify(self)
    [slienceBtn addAction:^(UIButton *btn) {
        @strongify(self)
        [self slienceBtnClick:btn];
    }];
    slienceBtn.selected = UDetail.user.client_silenceStatus;
    self.slienceBtn = slienceBtn;
    [layout addSubview:slienceBtn];
    
    btn = [UIButton new];
    btn.titleLabel.font = [UIFont font17];
    [btn setTitleColor:[UIColor colorWithHexString:@"#0088FF"] forState:UIControlStateNormal];
    [btn setBackgroundColor:[UIColor moBackground]];
    btn.size = CGSizeMake(200, 40);
    [btn setViewCornerRadius:btn.height/2.0];
    NSString *text = StrF(@"%@ %@ %@", @"退出", UDetail.user.client_type?:@"", [DeviceManager getAppName]);
    [btn setTitle:text forState:UIControlStateNormal];
    btn.mySize = btn.size;
    btn.myCenterX = 0;
    btn.myTop = 40;
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self loginOut:btn];
    }];
    [layout addSubview:btn];
    
    UIView *view = [UIView new];
    view.weight = 1;
    view.myWidth = 1;
    view.myCenterX = 0;
    [layout addSubview:view];
}

- (void)initNavLeftItem {
    UIButton *btn = [UIButton new];
    @weakify(self)
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }];
    [btn setImage:[UIImage imageNamed:@"icon_public_close"] forState:UIControlStateNormal];
    [btn sizeToFit];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = item;
}

@end
