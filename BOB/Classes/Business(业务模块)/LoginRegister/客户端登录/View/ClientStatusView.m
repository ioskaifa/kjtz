//
//  ClientStatusView.m
//  BOB
//
//  Created by mac on 2020/8/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ClientStatusView.h"

@interface ClientStatusView ()

@property (nonatomic, strong) SPButton *contentBtn;

@end

@implementation ClientStatusView

- (instancetype)init {
    if (self = [super init]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 40;
}

- (void)configureView {
    NSString *notiStatus = UDetail.user.client_silenceStatus ? @"手机通知已关闭":@"手机通知已打开";
    NSString *content = StrF(@"%@ %@%@ %@", UDetail.user.client_type, [DeviceManager getAppName],@"已登录,", notiStatus);
    [self.contentBtn setTitle:content forState:UIControlStateNormal];
    [self.contentBtn sizeToFit];
    self.contentBtn.mySize = self.contentBtn.size;
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:self.contentBtn];
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor moBackground];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0.5);
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.bottom.mas_equalTo(self.mas_bottom);
    }];    
}

- (SPButton *)contentBtn {
    if (!_contentBtn) {
        _contentBtn = ({
            SPButton *object = [SPButton new];
            object.userInteractionEnabled = NO;
            object.imageTitleSpace = 20;
            object.titleLabel.font = [UIFont font13];
            [object setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"icon_client_status"] forState:UIControlStateNormal];
            NSString *notiStatus = UDetail.user.client_silenceStatus ? @"手机通知已关闭":@"手机通知已打开" ;
            NSString *content = StrF(@"%@ %@%@ %@", UDetail.user.client_type?:@"", [DeviceManager getAppName],@"已登录,", notiStatus);
            [object setTitle:content forState:UIControlStateNormal];
            [object sizeToFit];
            object.mySize = object.size;
            object.myLeft = 30;
            object.myCenterY = 0;
            object;
        });
    }
    return _contentBtn;
}

@end
