//
//  FollowLiveListVC.m
//  BOB
//
//  Created by mac on 2020/10/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FollowLiveListVC.h"
#import "LiveListCell.h"
#import "LiveListObj.h"

@interface FollowLiveListVC ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation FollowLiveListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)fetchData {
    if ([self.last_id isEqualToString:@""]) {
        [self.dataSourceMArr removeAllObjects];
    }
    NSString *url = @"api/live/userLiveProgress/getFollowUserLiveProgressList";
    url = StrF(@"%@/%@", LcwlServerRoot, url);
    NSDictionary *param = @{@"last_id":self.last_id};
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url);
        net.params(param);
        net.finish(^(id data){
            [self colViewEndRefreshing];
            NSDictionary *dataDic = (NSDictionary *)data[@"data"];
            if ([data isSuccess]) {
                NSArray *dataArr = [LiveListObj modelListParseWithArray:dataDic[@"followUserLiveProgressList"]];
                [self.dataSourceMArr addObjectsFromArray:dataArr];
                LiveListObj *lastObj = (LiveListObj *)[self.dataSourceMArr lastObject];
                if (lastObj) {
                    self.last_id = lastObj.ID;
                }
                [self.colView reloadData];
            } else {
                [NotifyHelper showMessageWithMakeText:dataDic[@"msg"]];
            }
        }).failure(^(id error){
            [self colViewEndRefreshing];
            [NotifyHelper showMessageWithMakeText:[error description]];
            NSArray *dataArr = [LiveListObj liveListObj];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            LiveListObj *lastObj = (LiveListObj *)[self.dataSourceMArr lastObject];
            if (lastObj) {
                self.last_id = lastObj.ID;
            }
            [self.colView reloadData];
        })
        .execute();
    }];
}

- (void)colViewEndRefreshing {
    [self.colView.mj_header endRefreshing];
    [self.colView.mj_footer endRefreshing];
}

#pragma mark - Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = LiveListCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    LiveListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:[LiveListObj class]]) {
        return;
    }
    if (![cell isKindOfClass:[LiveListCell class]]) {
        return;
    }
    LiveListCell *listCell = (LiveListCell *)cell;
    listCell.cellType = LiveListCellTypeFllowed;
    [listCell configureView:obj];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    LiveListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:LiveListObj.class]) {
        return;
    }
    
}

- (void)createUI {    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    layout.backgroundColor = [UIColor moBackground];
    [self.view addSubview:layout];
    
    [layout addSubview:self.colView];
}

#pragma mark - Init
- (UICollectionView *)colView {
    if (!_colView) {
        _colView = ({
            UICollectionView *object = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
            object.backgroundColor = [UIColor moBackground];
            object.delegate = self;
            object.dataSource = self;
            object.weight = 1;
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            Class cls = LiveListCell.class;
            [object registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
            @weakify(self);
            object.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
                @strongify(self);
                self.last_id = @"";
                [self fetchData];
            }];
            object.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
                @strongify(self);
                [self fetchData];
            }];
            object;
        });
    }
    return _colView;
}

- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = ({
            UICollectionViewFlowLayout *object = [UICollectionViewFlowLayout new];
            CGFloat width = (SCREEN_WIDTH - 40) / 2.0;
            object.itemSize = CGSizeMake(width, [LiveListCell viewHeight]);
            object.minimumLineSpacing = 10;
            object.minimumInteritemSpacing = 10;
            object.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
            object;
        });
    }
    return _flowLayout;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

@end
