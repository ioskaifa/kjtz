//
//  ApplyShowResultVC.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

typedef NS_ENUM(NSInteger, ApplyShowResultStatus) {
    ///审核中
    ApplyShowResultStatusIng    = 0,
    ///审核通过
    ApplyShowResultStatusPass   = 1,
    ///审核失败
    ApplyShowResultStatusFail   = 2,
};

NS_ASSUME_NONNULL_BEGIN

@interface ApplyShowResultVC : BaseViewController

@property (nonatomic, assign)ApplyShowResultStatus applyStatus;

@end

NS_ASSUME_NONNULL_END
