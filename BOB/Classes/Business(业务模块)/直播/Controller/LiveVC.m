//
//  LiveVC.m
//  BOB
//
//  Created by mac on 2020/10/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveVC.h"
//#import <TXLiteAVSDK.h>

///推流地址
static NSString  * const rtmpUrl = @"rtmp://tl.tpshwl.com/live/CSLA?txSecret=d54c9973afc4cf1b7b29a26387c1059d&txTime=5FAF9A9E";


@interface LiveVC ()
//@interface LiveVC ()<TXLivePushListener>
//
//@property (nonatomic, strong) TXLivePushConfig *txPushConfig;
//
//@property (nonatomic, strong) TXLivePush *txPush;
//
//@property (nonatomic, strong) TXView *liveView;
/////推流
//@property (nonatomic, strong) UILabel *startLbl;
/////结束推流
//@property (nonatomic, strong) UILabel *stopLbl;

@end

@implementation LiveVC

//- (void)viewDidLoad {
//    [super viewDidLoad];
//    [self createUI];
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    [self stopPush];
//}
//
//- (void)navBarRightBtnAction:(id)sender {
//    MXRoute(@"LivePlayerVC", (@{@"bfUrl":@"http://bf.tpshwl.com/live/CSLA.flv", @"bfType":@(PLAY_TYPE_LIVE_FLV)}))
//}
//
//#pragma mark - IBAction
//- (void)startPush {
//    [self.txPush startPush:rtmpUrl];
//}
//
//- (void)stopPush {
//    [self.txPush stopPreview];
//    [self.txPush stopPush];
//}
//
//#pragma mark - Delegate
///**
// * 事件通知
// * @param EvtID 参见 TXLiveSDKEventDef.h
// * @param param 参见 TXLiveSDKTypeDef.h
// */
//- (void)onPushEvent:(int)EvtID withParam:(NSDictionary *)param {
//    MLog(@"")
//}
//
///**
// * 状态通知
// * @param param 参见 TXLiveSDKTypeDef.h
// */
//- (void)onNetStatus:(NSDictionary *)param {
//    
//}
//
//- (void)createUI {
//    [self setNavBarTitle:@"直播"];
//    [self setNavBarRightBtnWithTitle:@"拉流" andImageName:nil];
//    MyFrameLayout *layout = [MyFrameLayout new];
//    layout.myTop = 0;
//    layout.myLeft = 0;
//    layout.myRight = 0;
//    layout.myBottom = 0;
//    [self.view addSubview:layout];
//    
//    [layout addSubview:self.liveView];
//    [self.txPush startPreview:self.liveView];
//    
//    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
//    subLayout.myTop = 200;
//    subLayout.myCenterX = 0;
//    subLayout.myWidth = 200;
//    subLayout.myHeight = 50;
//    subLayout.gravity = MyGravity_Horz_Around;
//    [subLayout addSubview:self.startLbl];
//    [subLayout addSubview:self.stopLbl];
//    [layout addSubview:subLayout];
//}
//
//#pragma mark - Init
//- (TXLivePushConfig *)txPushConfig {
//    if (!_txPushConfig) {
//        _txPushConfig = [TXLivePushConfig new];
//    }
//    return _txPushConfig;
//}
//
//- (TXLivePush *)txPush {
//    if (!_txPush) {
//        _txPush = [[TXLivePush alloc] initWithConfig:self.txPushConfig];
//        _txPush.delegate = self;
//    }
//    return _txPush;
//}
//
//- (TXView *)liveView {
//    if (!_liveView) {
//        _liveView = [TXView new];
//        _liveView.myTop = 0;
//        _liveView.myLeft = 0;
//        _liveView.myRight = 0;
//        _liveView.myBottom = 0;
//    }
//    return _liveView;
//}
//
//- (UILabel *)startLbl {
//    if (!_startLbl) {
//        _startLbl = ({
//            UILabel *object = [UILabel new];
//            object.backgroundColor = [UIColor whiteColor];
//            object.textColor = [UIColor moBlack];
//            object.font = [UIFont font15];
//            object.textAlignment = NSTextAlignmentCenter;
//            object.size = CGSizeMake(70, 40);
//            object.mySize = object.size;
//            object.myCenterY = 0;
//            object.text = @"推流";
//            [object setViewCornerRadius:5];
//            @weakify(self)
//            [object addAction:^(UIView *view) {
//                @strongify(self)
//                [self startPush];
//            }];
//            object;
//        });
//    }
//    return _startLbl;
//}
//
//- (UILabel *)stopLbl {
//    if (!_stopLbl) {
//        _stopLbl = ({
//            UILabel *object = [UILabel new];
//            object.backgroundColor = [UIColor whiteColor];
//            object.textColor = [UIColor moBlack];
//            object.font = [UIFont font15];
//            object.textAlignment = NSTextAlignmentCenter;
//            object.size = CGSizeMake(70, 40);
//            object.mySize = object.size;
//            object.myCenterY = 0;
//            object.text = @"结束推流";
//            [object setViewCornerRadius:5];
//            @weakify(self)
//            [object addAction:^(UIView *view) {
//                @strongify(self)
//                [self stopPush];
//            }];
//            object;
//        });
//    }
//    return _stopLbl;
//}

@end
