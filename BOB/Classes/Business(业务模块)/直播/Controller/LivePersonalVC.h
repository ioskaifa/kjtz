//
//  LivePersonalVC.h
//  BOB
//
//  Created by colin on 2020/11/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "LivePersonalObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface LivePersonalVC : BaseViewController

@property (nonatomic, strong) LivePersonalObj *personalObj;

@end

NS_ASSUME_NONNULL_END
