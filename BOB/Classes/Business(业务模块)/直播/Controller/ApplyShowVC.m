//
//  ApplyShowVC.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ApplyShowVC.h"
#import "SelectLiveTimeListView.h"

@interface ApplyShowVC ()

@property (nonatomic, strong) UILabel        *showTimeLbl;

@property (nonatomic, strong) UIButton        *submitBtn;

@property (nonatomic, copy) NSString *time;

@end

@implementation ApplyShowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
//    [self fetchData];
}

- (void)navBarRightBtnAction:(id)sender {
    MXRoute(@"UserApplyLiveTimeListVC", nil)
}

- (void)fetchData {
    [self request:@"api/live/userApplyLiveTime/getUserApplyLiveTimeList"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        NSArray *dataArr = object[@"data"][@"userApplyLiveTimeList"];
        NSDictionary *dataDic = dataArr.firstObject;
        if (dataDic) {
            //status：记录状态 00-待审核 08-审核失败 09-审核成功
            NSString *status = dataDic[@"status"];
            if ([@"00" isEqualToString:status]) {
                MXRoute(@"ApplyShowResultVC", @{@"applyStatus":@(0)})
            } else if ([@"08" isEqualToString:status]) {
                MXRoute(@"ApplyShowResultVC", @{@"applyStatus":@(2)})
            }
        }
    }];
}

- (BOOL)valiParam {
    if ([StringUtil isEmpty:self.time]) {
        [NotifyHelper showMessageWithMakeText:@"请选择时长"];
        return NO;
    }
    return YES;
}

- (void)commit {
    [self request:@"api/live/userApplyLiveTime/userApplyLiveTime"
            param:@{@"time":self.time}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            MXRoute(@"ApplyShowResultVC", @{@"applyStatus":@(0)})
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)createUI {
    [self setNavBarTitle:@"申请开播"];
    [self setNavBarRightBtnWithTitle:@"历史记录" andImageName:nil];
    self.time = @"";
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.view addSubview:baseLayout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont17];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"申请时长";
    [lbl autoMyLayoutSize];
    lbl.myLeft = 15;
    lbl.myTop = 20;
    [baseLayout addSubview:lbl];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = 50;
    layout.myTop = 20;
    layout.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEE"].CGColor;
    layout.layer.borderWidth = 1;
    [baseLayout addSubview:layout];
    
    lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor colorWithHexString:@"#DADAE6"];
    lbl.text = @"请选择时长";
    self.showTimeLbl = lbl;
    [lbl autoMyLayoutSize];
    lbl.myLeft = 20;
    lbl.myCenterY = 0;
    lbl.weight = 1;
    lbl.myRight = 10;
    [layout addSubview:lbl];
    
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"Arrow"];
    imgView.myCenterY = 0;
    imgView.myRight = 20;
    [layout addSubview:imgView];
    [layout addAction:^(UIView *view) {
        SelectLiveTimeListView *timeView = [SelectLiveTimeListView new];
        timeView.size = CGSizeMake(SCREEN_WIDTH - 40, [SelectLiveTimeListView viewHeight]);
        MXCustomAlertVC *alertVC = [MXCustomAlertVC showInViewController:[[MXRouter sharedInstance] getTopNavigationController] contentView:timeView];
        alertVC.confirmFinish = ^(NSDictionary *data) {
            if (data[@"time"]) {
                self.time = data[@"time"];
                lbl.text = StrF(@"%@h", self.time);
            }
        };
    }];
    
    [baseLayout addSubview:self.submitBtn];
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"提交" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont15];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myTop = 20;
            object.myLeft = 15;
            object.myRight = 15;
            object.myBottom = 20 + safeAreaInsetBottom();
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                if ([self valiParam]) {
                    [self commit];
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

@end
