//
//  LiveGoodsListVC.m
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveGoodsListVC.h"
#import "AddLiveGoodsCell.h"

@interface LiveGoodsListVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation LiveGoodsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];    
}

- (void)configureGoods:(NSArray *)dataArr {
    NSArray *goodsIDArr = [self.dataSourceMArr valueForKeyPath:@"@distinctUnionOfObjects.ID"];
    if (goodsIDArr.count == 0) {
        [self.dataSourceMArr addObjectsFromArray:dataArr];
    } else {
        for (LiveGoodsObj *obj in dataArr) {
            if (![obj isKindOfClass:LiveGoodsObj.class]) {
                continue;
            }
            if ([goodsIDArr containsObject:obj.ID]) {
                continue;
            }
            [self.dataSourceMArr addObject:obj];
        }
    }
    [self.tableView reloadData];
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    if ([@"LiveGoodsListVCDel" isEqualToString:eventName]) {
        LiveGoodsObj *obj = userInfo[@"cellObj"];
        if ([obj isKindOfClass:LiveGoodsObj.class]) {
            [MXAlertViewHelper showAlertViewWithMessage:@"是否要删除商品？" completion:^(BOOL cancelled, NSInteger buttonIndex) {
                if (buttonIndex == 1) {
                    [self delGood:userInfo[@"cellObj"]];
                }
            }];
        }
    } else {
        [super routerEventWithName:eventName userInfo:userInfo];
    }
}

#pragma mark - 删除商品
- (void)delGood:(LiveGoodsObj *)obj {
    if (![obj isKindOfClass:LiveGoodsObj.class]) {
        return;
    }
    if ([self.dataSourceMArr containsObject:obj]) {
        [self.dataSourceMArr removeObject:obj];
        [self.tableView reloadData];
    }
}

- (void)endRefreshing {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = AddLiveGoodsCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:AddLiveGoodsCell.class]) {
        return;
    }
    AddLiveGoodsCell *listCell = (AddLiveGoodsCell *)cell;
    listCell.cellType = AddLiveGoodsCellTypeEdit;
    [listCell configureView:self.dataSourceMArr[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    LiveGoodsObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:LiveGoodsObj.class]) {
        return;
    }
//    MXRoute(@"GoodInfoVC", @{@"goods_id":obj.ID})
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"橱窗"];
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.view addSubview:baseLayout];
        
    [baseLayout addSubview:self.tableView];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [AddLiveGoodsCell viewHeight];
        Class cls = AddLiveGoodsCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:   NSStringFromClass(cls)];
//        @weakify(self);
//        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
//            @strongify(self);
//            self.last_id = @"";
//            [self fetchData];
//        }];
//        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
//            @strongify(self);
//            [self fetchData];
//        }];
        _tableView.myTop = 5;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

@end
