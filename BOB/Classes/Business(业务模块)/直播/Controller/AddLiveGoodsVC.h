//
//  AddLiveGoodsVC.h
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "AddLiveGoodsBottomView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddLiveGoodsVC : BaseViewController

@property (nonatomic, strong) AddLiveGoodsBottomView *bottomView;
///是否单选 默认全选
@property (nonatomic, assign)BOOL isSingleChoice;

- (BOOL)valiParam;
/// 获取选中的商品
- (NSArray *)selectedGoods;

@end

NS_ASSUME_NONNULL_END
