//
//  LiveListVC.m
//  BOB
//
//  Created by mac on 2020/10/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveListVC.h"
#import "RecommendLiveListVC.h"
#import "FollowLiveListVC.h"
#import "LiveListTopView.h"
#import "UIViewController+CWLateralSlide.h"
#import "LivePersonalVC.h"
#import "LivePersonalObj.h"

@interface LiveListVC ()<LiveListTopViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) LiveListTopView *topView;

@property (nonatomic, strong) RecommendLiveListVC *recommendVC;

@property (nonatomic, strong) FollowLiveListVC *followVC;

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@property (nonatomic, strong) LivePersonalObj *personalObj;

@end

@implementation LiveListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)rightBtnAction {
    LivePersonalVC *vc = [LivePersonalVC new];
    vc.personalObj = self.personalObj;
    CWLateralSlideConfiguration *conf = [CWLateralSlideConfiguration defaultConfiguration];
    conf.direction = CWDrawerTransitionFromRight;
    [self cw_showDrawerViewController:vc animationType:CWDrawerAnimationTypeMask configuration:conf];
}

- (void)fetchData {
    [self request:@"api/live/userApplyAnchor/getUserApplyAnchorRecord"
            param:@{} completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *dataDic = object[@"data"][@"userApplyAnchor"];
            if (dataDic) {
                //记录状态 00-待审核 08-审核失败 09-审核成功
                NSString *status = dataDic[@"status"];
                if ([@"00" isEqualToString:status]) {
                    
                } else if ([@"08" isEqualToString:status]) {
                    
                } else if ([@"09" isEqualToString:status]) {
                    self.personalObj.isAnchor = YES;
                }
            }
        }
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = UICollectionViewCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item >= self.dataSourceArr.count) {
        return;
    }
    UIView *view = [cell.contentView viewWithTag:100];
    if (view) {
        [view removeFromSuperview];
    }
    UIView *contentView = self.dataSourceArr[indexPath.item];
    contentView.tag = 100;
    [cell.contentView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(SCREEN_WIDTH, collectionView.height);
}

- (void)liveListTopView:(LiveListTopView *)sliderView didSelectItemAtIndex:(NSInteger)index {
    if (index >= self.dataSourceArr.count) {
        return;
    }
    [self.colView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]
                         atScrollPosition:UICollectionViewScrollPositionNone
                                 animated:YES];
}

- (void)createUI {
    [self setNavBarTitle:@"直播广场"];
    UIImageView *imgView = [UIImageView new];
    @weakify(self)
    [imgView addAction:^(UIView *view) {
        @strongify(self)
        [self rightBtnAction];
    }];
    imgView.backgroundColor = [UIColor moBackground];
    imgView.size = CGSizeMake(30, 30);
    imgView.clipsToBounds = YES;
    [imgView setViewCornerRadius:imgView.height/2.0];
    NSString *url = UDetail.user.head_photo;
    url = StrF(@"%@/%@", UDetail.user.qiniu_domain, url);
    [imgView sd_setImageWithURL:[NSURL URLWithString:AssignSize(url, 30, 30)] completed:nil];
    UIView *view = [UIView new];
    view.size = imgView.size;
    [view addSubview:imgView];
    UIBarButtonItem *barBtn = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.rightBarButtonItem = barBtn;
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    
    [layout addSubview:self.topView];
    [layout addSubview:self.colView];
}

- (LiveListTopView *)topView {
    if (!_topView) {
        _topView = [[LiveListTopView alloc] initWithData:@[@"精选", @"关注"]];
        _topView.size = CGSizeMake(SCREEN_WIDTH, [LiveListTopView viewHeight]);
        _topView.mySize = _topView.size;
        _topView.delegate = self;
        @weakify(self)
        [_topView.searchBtn addAction:^(UIButton *btn) {
            @strongify(self)
//            [self.viewerPanVC show];
        }];
    }
    return _topView;
}

- (FollowLiveListVC *)followVC {
    if (!_followVC) {
        _followVC = [FollowLiveListVC new];
        [self addChildViewController:_followVC];
    }
    return _followVC;
}

- (RecommendLiveListVC *)recommendVC {
    if (!_recommendVC) {
        _recommendVC = [RecommendLiveListVC new];
        [self addChildViewController:_recommendVC];
    }
    return _recommendVC;
}

- (NSArray *)dataSourceArr {
    if (!_dataSourceArr) {
        _dataSourceArr = @[self.recommendVC.view, self.followVC.view];
    }
    return _dataSourceArr;
}

- (UICollectionView *)colView {
    if (!_colView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        
        _colView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _colView.delegate = self;
        _colView.dataSource = self;
        _colView.backgroundColor = [UIColor moBackground];
//        _colView.pagingEnabled = YES;
        _colView.scrollEnabled = NO;
        _colView.myTop = 0;
        _colView.myLeft = 0;
        _colView.myRight = 0;
        _colView.height = SCREEN_HEIGHT - NavigationBar_Bottom - self.topView.height;
        _colView.myHeight = _colView.height;
        Class cls = UICollectionViewCell.class;
        [_colView registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
    }
    return _colView;
}

- (LivePersonalObj *)personalObj {
    if (!_personalObj) {
        _personalObj = [LivePersonalObj new];
        _personalObj.name = UDetail.user.nickname;
        _personalObj.photo = UDetail.user.head_photo;
        _personalObj.fansNum = 0;
        _personalObj.followNum = 200;
        _personalObj.liveNum = 999;
        _personalObj.level = @"1";
        _personalObj.introduction = @"";
        _personalObj.isAnchor = NO;
    }
    return _personalObj;
}

@end
