//
//  LiveRoomReportVC.m
//  BOB
//
//  Created by colin on 2020/11/24.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveRoomReportVC.h"
#import "MQImageDragView.h"
#import "PhotoBrowser.h"
#import <Photos/Photos.h>
#import "QNManager.h"

@interface LiveRoomReportVC ()<MQImageDragViewDelegate>

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) NSMutableArray *btnMArr;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic,strong) NSMutableArray<PHAsset *> *curSelectAssets;

@property (nonatomic,strong) NSMutableArray<UIImage *> *curSelectImages;

@property (nonatomic, strong) MQImageDragView *dragView;

@property (nonatomic, strong) UIButton        *submitBtn;

@end

@implementation LiveRoomReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)btnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        sender.layer.borderColor = [UIColor moGreen].CGColor;
    } else {
        sender.layer.borderColor = [UIColor colorWithHexString:@"#AAAABB"].CGColor;
    }
}

- (NSArray *)selectBtn {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"selected == YES"];
    NSArray *tempArr = [self.btnMArr filteredArrayUsingPredicate:predicate];
    return tempArr;
}

- (NSString *)selectType {
    NSArray *tempArr = [self selectBtn];
    if (tempArr.count == 0) {
        return @"";
    }
    tempArr = [tempArr valueForKeyPath:@"@distinctUnionOfObjects.currentTitle"];
    return [tempArr componentsJoinedByString:@","];
}

- (BOOL)valiParam {
    NSArray *tempArr = [self selectBtn];
    if (tempArr.count == 0) {
        [NotifyHelper showMessageWithMakeText:@"请选择举报内容"];
        return NO;
    }
    if (self.curSelectImages.count == 0) {
        [NotifyHelper showMessageWithMakeText:@"请上传举报截图"];
        return NO;
    }
    return YES;
}

- (void)commit {
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    [[QNManager shared] uploadImages:self.curSelectImages
                          completion:^(id data) {
        [NotifyHelper hideAllHUDsForView:self.view animated:YES];
        NSArray *array = data;
        if(array && array.count > 0) {
            [self request:@"api/user/feedBack/addUserFeedBack"
                    param:@{@"feedback_type":[self selectType]?:@"",
                            @"feedback_img":[array componentsJoinedByString:@","]
                    }
               completion:^(BOOL success, id object, NSString *error) {                
                if(success) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }];
}

- (void)updateDragView:(NSArray *)images assets:(NSArray *)assets {
    if([images isKindOfClass:[NSArray class]]) {
        [self.dragView removeAll];
        [images enumerateObjectsUsingBlock:^(UIImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            PHAsset *asset = [assets safeObjectAtIndex:idx];
//            self.curSelectVideo = NO;
            if(asset.mediaType == PHAssetMediaTypeVideo) {
//                self.dragView.kMaxCount = 1;
//                self.curSelectVideo = YES;
            } else if([obj isKindOfClass:[UIImage class]]) {
//                self.dragView.kMaxCount = 9;
            }
            
            [self.dragView addImage:obj];
        }];
        
//        [self layoutHeader];
        self.dragView.myHeight = [self.dragView getHeightThatFit];
//        [self configureScrollViewContevtSize];
        self.curSelectAssets = [NSMutableArray arrayWithArray:assets];
        self.curSelectImages = [NSMutableArray arrayWithArray:images];
    }
}

#pragma mark - MQImageDragViewDelegate
- (void)imageDragViewAddButtonClicked {
    NSLog(@"imageDragView---点击了添加按钮");
    @weakify(self)
    [[PhotoBrowser shared] dynamicShowPhotoLibrary:self allowSelectVideo:NO lastSelectAssets:self.curSelectAssets completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
        @strongify(self)
        [self updateDragView:images assets:assets];
    }];
}

- (void)imageDragViewDeleteButtonClickedAtIndex:(NSInteger)index{
    NSLog(@"imageDragView---删除了%ld",index);
    
    //刷新frame
    [self.curSelectAssets safeRemoveObjectAtIndex:index];
    [self.curSelectImages safeRemoveObjectAtIndex:index];
    self.dragView.myHeight = [self.dragView getHeightThatFit];
}

- (void)imageDragViewButtonClickedAtIndex:(NSInteger)index{
    NSLog(@"imageDragView---点击了%ld",index);
    [[PhotoBrowser shared] dynamicPreviewSelectPhotos:self didSelectItemAtIndex:index completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
        [self updateDragView:images assets:assets];
    }];
}

- (void)imageDragViewDidMoveButtonFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{
    NSLog(@"imageDragView---移动：从%ld至%ld",fromIndex,toIndex);
    [self.curSelectAssets exchangeObjectAtIndex:toIndex withObjectAtIndex:fromIndex];
    [self.curSelectImages exchangeObjectAtIndex:toIndex withObjectAtIndex:fromIndex];
}

- (MyBaseLayout *)headerView {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myHeight = MyLayoutSize.wrap;
    [baseLayout addSubview:[UIView fullLine]];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"请选择举报内容";
    [lbl autoMyLayoutSize];
    lbl.myLeft = 15;
    lbl.myTop = 10;
    [baseLayout addSubview:lbl];
    
    MyLinearLayout *layout;
    for (NSInteger i = 0; i < self.dataArr.count; i++) {
        if (i % 3 == 0) {
            layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
            layout.myTop = 10;
            layout.myLeft = 0;
            layout.myRight = 0;
            layout.gravity = MyGravity_Horz_Around;
            layout.myHeight = 50;
            [baseLayout addSubview:layout];
        }
        [layout addSubview:[self factoryBtn:self.dataArr[i]]];
    }
    
    lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"上传举报截图";
    [lbl autoMyLayoutSize];
    lbl.myLeft = 15;
    lbl.myTop = 10;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:self.dragView];
    [baseLayout addSubview:self.submitBtn];
    
    SPButton *btn = [SPButton new];
    btn.userInteractionEnabled = NO;
    btn.imagePosition = SPButtonImagePositionLeft;
    btn.imageTitleSpace = 10;
    [btn setImage:[UIImage imageNamed:@"温馨提示"] forState:UIControlStateNormal];
    [btn setTitle:@"温馨提示：" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont font12];
    [btn setTitleColor:[UIColor colorWithHexString:@"#FF4757"] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.mySize = btn.size;
    btn.myLeft = 20;
    btn.myTop = 20;
    [baseLayout addSubview:btn];
    
    lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.myLeft = 20;
    lbl.myTop = 10;
    NSString *tip1 = @"1）如您需举报有害信息，请按页面提示填写相应信息，我们将尽快核实处理 ";
    NSString *tip2 = @"\n2）请不要恶意举报，经查实为恶意举报，平台将进行封号处理";
    NSArray *txtArr = @[tip1, tip2];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#AAAABB"], [UIColor colorWithHexString:@"#AAAABB"]];
    NSArray *fontArr = @[[UIFont font12], [UIFont font12]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 5; // 调整行间距
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    lbl.mySize = size;
    [baseLayout addSubview:lbl];
    
    baseLayout.myWidth = SCREEN_WIDTH;
    [baseLayout layoutIfNeeded];
    return baseLayout;
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"举报"];
    
    MyLinearLayout *layout = [self.view addMyLinearLayout:MyOrientation_Vert];
    [self.view addSubview:layout];
    
    [layout addSubview:self.tableView];
    
    self.tableView.tableHeaderView = [self headerView];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.myBottom = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (UIButton *)factoryBtn:(NSString *)title {
    UIButton *btn = [UIButton new];
    CGFloat width = (SCREEN_WIDTH - 90)/3;
    btn.size = CGSizeMake(width, 40);
    btn.titleLabel.font = [UIFont font15];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#7F7F8E"] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor moGreen] forState:UIControlStateSelected];
    btn.layer.borderWidth = 0.5;
    [btn setViewCornerRadius:2];
    btn.layer.borderColor = [UIColor colorWithHexString:@"#AAAABB"].CGColor;
    btn.mySize = btn.size;
    btn.myCenterY = 0;
    @weakify(self)
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self btnClick:btn];
    }];
    [self.btnMArr addObject:btn];
    return btn;
}

- (NSArray *)dataArr {
    if (!_dataArr) {
        _dataArr = @[@"色情内容", @"垃圾广告", @"人身攻击", @"敏感信息", @"诈骗", @"其它"];
    }
    return _dataArr;
}

- (MQImageDragView *)dragView {
    if (!_dragView) {
        _dragView = ({
            MQImageDragView *object = [[MQImageDragView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 30, 100)];
            object.backgroundColor = [UIColor whiteColor];
            object.kMarginLRTB = 5;
            object.kMarginB = 4;
            object.kMaxCount = 8;
            object.kCountInRow = 4;
            object.dragViewDelegete = self;
            object.myTop = 10;
            object.myLeft = 0;
            object.myRight = 0;
            object.mySize = object.size;
            object;
        });
    }
    return _dragView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"提交" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myTop = 50;
            object.myLeft = 15;
            object.myRight = 15;
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                if ([self valiParam]) {
                    [self commit];
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

- (NSMutableArray *)btnMArr {
    if (!_btnMArr) {
        _btnMArr = [NSMutableArray arrayWithCapacity:self.dataArr.count];
    }
    return _btnMArr;
}

@end
