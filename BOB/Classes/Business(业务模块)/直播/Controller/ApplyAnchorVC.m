//
//  ApplyAnchorVC.m
//  BOB
//
//  Created by colin on 2020/11/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ApplyAnchorVC.h"
#import "LeftRightTextFieldView.h"
#import <Photos/Photos.h>
#import "QNManager.h"
#import "PhotoBrowser.h"
#import "ApplyAnchorCommitView.h"

@interface ApplyAnchorVC ()

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) LeftRightTextFieldView  *nameView;
@property (nonatomic, strong) LeftRightTextFieldView  *IDView;
@property (nonatomic, strong) LeftRightTextFieldView  *phoneView;
@property (nonatomic, strong) ApplyAnchorCommitView  *commitView;
@property (nonatomic, strong) UIButton        *submitBtn;

@property (nonatomic, strong) UIImageView        *IDCardOneImgView;

@property (nonatomic, strong) UIImageView        *IDCardTwoImgView;

@property (nonatomic, copy) NSString        *IDCardOneImgString;

@property (nonatomic, copy) NSString        *IDCardTwoImgString;
///是否重新申请
@property (nonatomic, assign) BOOL isReapply;

@end

@implementation ApplyAnchorVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)fetchData {
    [self request:@"api/live/userApplyAnchor/getUserApplyAnchorRecord"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *dataDic = object[@"data"][@"userApplyAnchor"];
            if (dataDic) {
                NSString *remark = dataDic[@"remark"]?:@"";
                //记录状态 00-待审核 08-审核失败 09-审核成功
                NSString *status = dataDic[@"status"];
                if ([@"00" isEqualToString:status]) {
                    [self.commitView configureView:@"提交申请成为主播" tips:@[@"提交成功", @"请耐心等待3~5个工作日，我们会尽快审核"]];
                } else if ([@"08" isEqualToString:status]) {
                    [self.commitView configureView:@"主播审核不通过" tips:@[@"审核不通过", remark]];
                    self.commitView.commitLbl.visibility = MyVisibility_Visible;
                } else if ([@"09" isEqualToString:status]) {
                    [self.commitView configureView:@"提交申请成为主播" tips:@[@"审核成功", remark]];
                }
                self.nameView.rightTF.text = dataDic[@"real_name"]?:@"";
                self.IDView.rightTF.text = dataDic[@"id_card"]?:@"";
                self.phoneView.rightTF.text = dataDic[@"tel"]?:@"";
                [self showResult];
            }
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)showResult {
    self.commitView.visibility = MyVisibility_Visible;
    self.tableView.visibility = MyVisibility_Invisible;
    self.submitBtn.visibility = MyVisibility_Invisible;
}

- (BOOL)valiParam {
    NSString *string = [StringUtil trim:self.nameView.value];
    if ([StringUtil isEmpty:string]) {
        [NotifyHelper showMessageWithMakeText:@"请填写姓名"];
        return NO;
    }
    string = [StringUtil trim:self.IDView.value];
    if ([StringUtil isEmpty:string]) {
        [NotifyHelper showMessageWithMakeText:@"请填写身份证号"];
        return NO;
    }
    string = [StringUtil trim:self.phoneView.value];
    if ([StringUtil isEmpty:string]) {
        [NotifyHelper showMessageWithMakeText:@"请填写手机号"];
        return NO;
    }
    if (![DCCheckRegular dc_checkTelNumber:string]) {
        [NotifyHelper showMessageWithMakeText:@"手机号有误"];
        return NO;
    }
    if ([StringUtil isEmpty:self.IDCardOneImgString] ||
        [StringUtil isEmpty:self.IDCardTwoImgString]) {
        [NotifyHelper showMessageWithMakeText:@"请上传证件照"];
        return NO;
    }
    return YES;
}

- (void)commit {
    NSString *name = [StringUtil trim:self.nameView.value]?:@"";
    NSString *cardID = [StringUtil trim:self.IDView.value]?:@"";
    NSString *tel = [StringUtil trim:self.phoneView.value]?:@"";
    [self request:@"api/live/userApplyAnchor/userApplyAnchor"
            param:@{@"id_card":cardID,
                    @"photo":StrF(@"%@,%@", self.IDCardOneImgString, self.IDCardTwoImgString),
                    @"tel":tel,
                    @"real_name":name,
            }
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            [self showResult];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

///重新申请
- (void)reapply {
    NSString *name = [StringUtil trim:self.nameView.value]?:@"";
    NSString *cardID = [StringUtil trim:self.IDView.value]?:@"";
    NSString *tel = [StringUtil trim:self.phoneView.value]?:@"";
    [self request:@"api/live/userApplyAnchor/reapplyUserAnchor"
            param:@{@"id_card":cardID,
                    @"photo":StrF(@"%@,%@", self.IDCardOneImgString, self.IDCardTwoImgString),
                    @"tel":tel,
                    @"real_name":name,
            }
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            [self.commitView configureView:@"提交申请成为主播" tips:@[@"提交成功", @"请耐心等待3~5个工作日，我们会尽快审核"]];
            [self showResult];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)configurePhoto:(UIImageView *)imgView withImg:(UIImage *)img {
    imgView.image = img;
    imgView.visibility = MyVisibility_Visible;
}

#pragma mark 上传图片
- (void)uploadPhoto:(UIImageView *)imgView {
    [[PhotoBrowser shared] showSelectSinglePhotoLibrary:self completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
            if([images isKindOfClass:[NSArray class]]) {
                if([[images firstObject] isKindOfClass:[UIImage class]]) {
                    UIImage *image = [images firstObject];
                    [NotifyHelper showHUDAddedTo:self.view animated:YES];
                    [[QNManager shared] uploadImage:image completion:^(id data) {
                        [NotifyHelper hideHUDForView:self.view animated:YES];
                        if(data && [NSURL URLWithString:data]) {
                            [self configurePhoto:imgView withImg:image];
                            if (imgView == self.IDCardOneImgView) {
                                self.IDCardOneImgString = data;
                            } else if (imgView == self.IDCardTwoImgView) {
                                self.IDCardTwoImgString = data;
                            }
                        }
                    }];
                }
            }
    }];
}

- (void)createUI {
    [self setNavBarTitle:@"申请成为主播"];
    MyFrameLayout *baseLayout = [MyFrameLayout new];
    baseLayout.backgroundColor = [UIColor moBackground];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.view addSubview:baseLayout];
    
    MyLinearLayout *layout= [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor moBackground];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [baseLayout addSubview:layout];
    [baseLayout addSubview:self.commitView];
    
    [layout addSubview:self.tableView];
    [layout addSubview:self.submitBtn];
    
}

- (MyBaseLayout *)headerView {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor colorWithHexString:@"#00DBD9"];
    lbl.backgroundColor = [UIColor colorWithHexString:@"#46474D"];
    lbl.text = @"*提示：根据国家相关法律法规，开启直播必须要进行实名认证审核！";
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    lbl.myCenter = CGPointZero;
    
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.backgroundColor = [UIColor colorWithHexString:@"#46474D"];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = lbl.height + 20;
    [layout addSubview:lbl];
    [baseLayout addSubview:layout];
    
    [baseLayout addSubview:self.nameView];
    [baseLayout addSubview:[UIView line]];
    
    [baseLayout addSubview:self.IDView];
    [baseLayout addSubview:[UIView line]];
    
    [baseLayout addSubview:self.phoneView];
    [baseLayout addSubview:[UIView line]];
    
    layout = [MyFrameLayout new];
    layout.backgroundColor = [UIColor colorWithHexString:@"#46474D"];
    layout.myTop = 20;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = 145;
    [layout setViewCornerRadius:5];
    layout.backgroundColor = [UIColor colorWithHexString:@"#F7F7FA"];
    [layout addSubview:[self.class factoryIDCardLayout:@"头像面\n" describe:@"上传身份证头像面"]];
    UIImageView *imgView = [UIImageView new];
    imgView.visibility = MyVisibility_Invisible;
    imgView.backgroundColor = [UIColor colorWithHexString:@"#F7F7FA"];
    imgView.myLeft = (SCREEN_WIDTH - 30) / 2.0;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    imgView.myTop = 15;
    imgView.myRight = 15;
    imgView.myBottom = 15;
    self.IDCardOneImgView = imgView;
    [layout addSubview:imgView];
    @weakify(self)
    [layout addAction:^(UIView *view) {
        @strongify(self)
        [self uploadPhoto:imgView];
    }];
    [baseLayout addSubview:layout];
    
    layout = [MyFrameLayout new];
    layout.backgroundColor = [UIColor colorWithHexString:@"#46474D"];
    layout.myTop = 15;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = 145;
    layout.myBottom = 20;
    [layout setViewCornerRadius:5];
    layout.backgroundColor = [UIColor colorWithHexString:@"#F7F7FA"];
    [layout addSubview:[self.class factoryIDCardLayout:@"国徽面\n" describe:@"上传身份证国徽面"]];
    imgView = [UIImageView new];
    imgView.visibility = MyVisibility_Invisible;
    imgView.backgroundColor = [UIColor colorWithHexString:@"#F7F7FA"];
    imgView.myLeft = (SCREEN_WIDTH - 30) / 2.0;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    imgView.myTop = 15;
    imgView.myRight = 15;
    imgView.myBottom = 15;
    self.IDCardTwoImgView = imgView;
    [layout addSubview:imgView];
    [layout addAction:^(UIView *view) {
        @strongify(self)
        [self uploadPhoto:imgView];
    }];
    [baseLayout addSubview:layout];
    
    [baseLayout layoutIfNeeded];
    return baseLayout;;
}

-(LeftRightTextFieldView* )nameView{
    if (!_nameView) {
        _nameView = ({
            LeftRightTextFieldView *object = [LeftRightTextFieldView leftRightView:@"真实姓名"
                                                                   withPlaceholder:@"请填写真实姓名"];
            object.backgroundColor = [UIColor whiteColor];
            object.myTop = 0;
            object.myLeft = 0;
            object.myWidth = SCREEN_WIDTH;
            object.myHeight = 52;
            object;
        });
    }
    return _nameView;
}

-(LeftRightTextFieldView* )IDView{
    if (!_IDView) {
        _IDView = ({
            LeftRightTextFieldView *object = [LeftRightTextFieldView leftRightView:@"身份证号"
                                                                   withPlaceholder:@"请填写身份证号码"];
            object.backgroundColor = [UIColor whiteColor];
            object.myTop = 0;
            object.myLeft = 0;
            object.myWidth = SCREEN_WIDTH;
            object.myHeight = 52;
            object;
        });
    }
    return _IDView;
}

-(LeftRightTextFieldView* )phoneView{
    if (!_phoneView) {
        _phoneView = ({
            LeftRightTextFieldView *object = [LeftRightTextFieldView leftRightView:@"手机号码"
                                                                   withPlaceholder:@"请填写手机号码"];
            object.rightTF.keyboardType = UIKeyboardTypePhonePad;
            object.backgroundColor = [UIColor whiteColor];
            object.myTop = 0;
            object.myLeft = 0;
            object.myWidth = SCREEN_WIDTH;
            object.myHeight = 52;
            object;
        });
    }
    return _phoneView;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            [object setTitle:@"提交" forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont boldFont15];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            object.height = 46;
            [object setViewCornerRadius:5];
            object.myHeight = object.height;
            object.myTop = 20;
            object.myLeft = 15;
            object.myRight = 15;
            object.myBottom = 20 + safeAreaInsetBottom();
            @weakify(self);
            [object addAction:^(UIButton *btn) {
                @strongify(self);
                if ([self valiParam]) {
                    if (self.isReapply) {
                        [self reapply];
                    } else {
                        [self commit];
                    }
                }
            }];
            object;
        });
    }
    return _submitBtn;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [UIView new];
        _tableView.tableHeaderView = [self headerView];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.myTop = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

+ (MyBaseLayout *)factoryIDCardLayout:(NSString *)title describe:(NSString *)describe {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 15;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myBottom = 15;
    layout.gravity = MyGravity_Fill;
    
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 2;
    NSArray *txtArr = @[title, describe];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#151419"], [UIColor colorWithHexString:@"#737380"]];
    NSArray *fontArr = @[[UIFont boldFont20], [UIFont font12]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 10; // 调整行间距
    paragraphStyle.alignment = NSTextAlignmentLeft;
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
//    [lbl autoMyLayoutSize];
    [layout addSubview:lbl];
    
    UIButton *btn = [UIButton new];
    btn.userInteractionEnabled = NO;
    btn.layer.borderColor = [UIColor colorWithHexString:@"#DADAE6"].CGColor;
    btn.layer.borderWidth = 0.7;
    [btn setViewCornerRadius:2];
    
    [btn setImage:[UIImage imageNamed:@"icon_add"] forState:UIControlStateNormal];
    [layout addSubview:btn];
    
    return layout;
}

- (ApplyAnchorCommitView *)commitView {
    if (!_commitView) {
        _commitView = [ApplyAnchorCommitView new];
        _commitView.size = CGSizeMake(SCREEN_WIDTH, [ApplyAnchorCommitView viewHeight]);
        _commitView.mySize = _commitView.size;
        _commitView.myTop = 0;
        _commitView.myLeft = 0;
        _commitView.visibility = MyVisibility_Gone;
        @weakify(self)
        [_commitView.commitLbl addAction:^(UIView *view) {
            @strongify(self)
            self.isReapply = YES;
            self->_commitView.visibility = MyVisibility_Gone;
            self.tableView.visibility = MyVisibility_Visible;
            self.submitBtn.visibility = MyVisibility_Visible;
        }];
    }
    return _commitView;
}

@end
