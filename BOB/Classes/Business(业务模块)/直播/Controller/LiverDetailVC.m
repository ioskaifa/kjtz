//
//  LiverDetailVC.m
//  BOB
//
//  Created by colin on 2020/11/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiverDetailVC.h"


@interface LiverDetailVC ()

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation LiverDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)fetchData {
    [self request:@"" param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            
        }
    }];
    self.tableView.tableHeaderView = [self headerView];
}

- (MyBaseLayout *)headerView {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myWidth = SCREEN_WIDTH;
    baseLayout.myHeight = MyLayoutSize.wrap;
    baseLayout.backgroundColor = [UIColor whiteColor];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 15;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = MyLayoutSize.wrap;
    [baseLayout addSubview:layout];
    
    UIImageView *imgView = [UIImageView new];
    imgView.size = CGSizeMake(60, 60);
    [imgView setViewCornerRadius:imgView.height/2.0];
    imgView.mySize = imgView.size;
    [imgView sd_setImageWithURL:[NSURL URLWithString:self.personalObj.photo] placeholderImage:[UIImage imageNamed:@"直播_默认头像"]];
    [layout addSubview:imgView];
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.weight = 1;
    subLayout.myLeft = 10;
    subLayout.myRight = 0;
    subLayout.height = imgView.height;
    subLayout.myHeight = imgView.height;
    [layout addSubview:subLayout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont18];
    lbl.textColor = [UIColor blackColor];
    lbl.myHeight = 25;
    lbl.myLeft = 0;
    lbl.myRight = 0;
    lbl.text = self.personalObj.name;
    [subLayout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor colorWithHexString:@"#7F7F8E"];
    lbl.myHeight = 20;
    lbl.myTop = 3;
    lbl.myLeft = 0;
    lbl.myRight = 0;
    lbl.text = StrF(@"直播标题：%@", self.personalObj.liveTitle);
    [subLayout addSubview:lbl];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = 80;
    layout.myTop = 20;
    layout.myBottom = 20;
    layout.gravity = MyGravity_Fill;
    [baseLayout addSubview:layout];
    [layout addSubview:[self.class factoryLbl:@"直播时长" value:self.detailObj.duration]];
    [layout addSubview:[self.class factoryLbl:@"直播时间" value:self.detailObj.liveTime]];
    
    [baseLayout addSubview:[UIView gapLine]];
    
    lbl = [UILabel new];
    lbl.font = [UIFont boldFont17];
    lbl.textColor = [UIColor colorWithHexString:@"#151419"];
    lbl.myHeight = 20;
    lbl.myTop = 10;
    lbl.myLeft = 15;
    lbl.myBottom = 10;
    lbl.text = @"互动总结";
    [lbl autoMyLayoutSize];
    [baseLayout addSubview:lbl];
    
    [baseLayout addSubview:[UIView fullLine]];
    
    MyFrameLayout *frameLayout = [MyFrameLayout new];
    frameLayout.height = 160;
    frameLayout.myHeight = frameLayout.height;
    frameLayout.myLeft = 0;
    frameLayout.myRight = 0;
    
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:@"#F0F0F5"];
    [frameLayout addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(frameLayout.mas_left);
        make.right.mas_equalTo(frameLayout.mas_right);
        make.top.mas_equalTo(frameLayout.height/2.0);
    }];
    
    view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:@"#F0F0F5"];
    [frameLayout addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.top.mas_equalTo(frameLayout.mas_top);
        make.bottom.mas_equalTo(frameLayout.mas_bottom);
        make.left.mas_equalTo(SCREEN_WIDTH/2.0);
    }];
    
    [baseLayout addSubview:frameLayout];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.height = frameLayout.height/2.0;
    layout.gravity = MyGravity_Fill;
    [frameLayout addSubview:layout];
    
    [layout addSubview:[self.class factoryLblEx:@"累计观看（人）" value:self.detailObj.watchNum]];
    [layout addSubview:[self.class factoryLblEx:@"新增粉丝（人）" value:self.detailObj.addFans]];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.height = frameLayout.height/2.0;
    layout.myTop = layout.height;
    layout.gravity = MyGravity_Fill;
    [frameLayout addSubview:layout];
    
    [layout addSubview:[self.class factoryLblEx:@"点赞次数" value:self.detailObj.watchNum]];
    [layout addSubview:[self.class factoryLblEx:@"直播间分享（次）" value:self.detailObj.addFans]];
    
    [baseLayout addSubview:[UIView gapLine]];
    
    lbl = [UILabel new];
    lbl.font = [UIFont boldFont17];
    lbl.textColor = [UIColor colorWithHexString:@"#151419"];
    lbl.myHeight = 20;
    lbl.myTop = 10;
    lbl.myLeft = 15;
    lbl.myBottom = 10;
    lbl.text = @"成交总结";
    [lbl autoMyLayoutSize];
    [baseLayout addSubview:lbl];
    
    [baseLayout addSubview:[UIView fullLine]];
    
    frameLayout = [MyFrameLayout new];
    frameLayout.height = 160;
    frameLayout.myHeight = frameLayout.height;
    frameLayout.myLeft = 0;
    frameLayout.myRight = 0;
    [baseLayout addSubview:frameLayout];
    
    view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:@"#F0F0F5"];
    [frameLayout addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(frameLayout.mas_left);
        make.right.mas_equalTo(frameLayout.mas_right);
        make.top.mas_equalTo(frameLayout.height/2.0);
    }];
    
    view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:@"#F0F0F5"];
    [frameLayout addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.top.mas_equalTo(frameLayout.mas_top);
        make.bottom.mas_equalTo(frameLayout.mas_bottom);
        make.left.mas_equalTo(SCREEN_WIDTH/2.0);
    }];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.height = frameLayout.height/2.0;
    layout.gravity = MyGravity_Fill;
    [frameLayout addSubview:layout];
    
    [layout addSubview:[self.class factoryLblEx:@"支付订单（笔）" value:self.detailObj.payOrderNum]];
    [layout addSubview:[self.class factoryLblEx:@"支付金额（元）" value:StrF(@"%0.2f", self.detailObj.payAmout)]];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.height = frameLayout.height/2.0;
    layout.myTop = layout.height;
    layout.gravity = MyGravity_Fill;
    [frameLayout addSubview:layout];
    
    [layout addSubview:[self.class factoryLblEx:@"观看用户下单率" value:StrF(@"%0.1f%%", (self.detailObj.viewerRate *100))]];
    [layout addSubview:[self.class factoryLblEx:@"观看次数下单率" value:StrF(@"%0.1f%%", (self.detailObj.timesRate *100))]];
    
    [baseLayout layoutIfNeeded];
    
    return baseLayout;
}

- (void)createUI {
    [self setNavBarTitle:@"直播数据"];
    [self.view addSubview:self.tableView];
    FullInSuperView(self.tableView);
    [self fetchData];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView new];
        _tableView.backgroundColor = [UIColor moBackground];
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}

- (LiverDetailObj *)detailObj {
    if (!_detailObj) {
        _detailObj = [LiverDetailObj new];
        _detailObj.watchNum = @"803";
        _detailObj.thumbsNum = @"6666";
        _detailObj.addFans = @"30009";
        _detailObj.orderNum = @"999";
        _detailObj.beginTime = @"2020-11-01 19:30";
        _detailObj.endTime = @"2020-11-01 23:30";
        _detailObj.orderAmout = 10091.56;
        _detailObj.totalWatch = @"9000";
        _detailObj.shareNum = @"10";
        _detailObj.payOrderNum = @"100";
        _detailObj.payAmout = 9890033;
        _detailObj.viewerRate = 0.24;
        _detailObj.timesRate = 0.7;
        _detailObj.liveTime = @"10/19 19:13-19:56";
    }
    return _detailObj;
}

- (LivePersonalObj *)personalObj {
    if (!_personalObj) {
        _personalObj = [LivePersonalObj new];
        _personalObj.name = @"较为英俊的男子";
        _personalObj.fansNum = 0;
        _personalObj.followNum = 200;
        _personalObj.liveNum = 999;
        _personalObj.level = @"1";
        _personalObj.introduction = @"";
        _personalObj.isAnchor = NO;
        _personalObj.liveTitle = @"来都来了，进来聊聊";
    }
    return _personalObj;
}

+ (MyBaseLayout *)factoryLbl:(NSString *)key value:(NSString *)value {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myLeft = 3;
    layout.myRight = 0;
    layout.myTop = 0;
    layout.myBottom = 0;
    layout.backgroundImage = [UIImage imageNamed:@"直播数据bg"];
    if ([StringUtil isEmpty:value]) {
        value = @"—";
    }
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 0;
    NSArray *txtArr = @[StrF(@"%@\n", key), value];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#AAAABB"], [UIColor colorWithHexString:@"#151419"]];
    NSArray *fontArr = @[[UIFont font13], [UIFont font17]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 10; // 调整行间距
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
    lbl.myTop = 10;
    lbl.myLeft = 10;
    lbl.myRight = 10;
    lbl.myBottom = 10;
    [layout addSubview:lbl];
    
    return layout;
}

+ (MyBaseLayout *)factoryLblEx:(NSString *)key value:(NSString *)value {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myTop = 0;
    layout.myBottom = 0;
    
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 0;
    if ([StringUtil isEmpty:key]) {
        key = @"—";
    }
    NSArray *txtArr = @[StrF(@"%@\n", key), value];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#AAAABB"], [UIColor colorWithHexString:@"#151419"]];
    NSArray *fontArr = @[[UIFont font13], [UIFont font17]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 10; // 调整行间距
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
    lbl.myTop = 15;
    lbl.myLeft = 15;
    lbl.myRight = 15;
    lbl.myBottom = 15;
    [layout addSubview:lbl];
    
    return layout;
}

@end
