//
//  StartLiveVC.m
//  BOB
//
//  Created by AlphaGo on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "StartLiveVC.h"
#import "TXLiteAVSDKManage.h"
#import "StartLiveOverlayerVC.h"

@interface StartLiveVC ()
@property(nonatomic,strong) StartLiveOverlayerVC *startLiveOverlayerVC;
@end

@implementation StartLiveVC

- (void)dealloc {
    [[TXLiteAVSDKManage shared] stopPreview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [[TXLiteAVSDKManage shared] startPush];
    
    [self.view addSubview:self.startLiveOverlayerVC.view];
    FullInSuperView(self.startLiveOverlayerVC.view);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[TXLiteAVSDKManage shared] startPreview:self.view];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    //[[TXLiteAVSDKManage shared] hideBeautyPanel];
}

- (StartLiveOverlayerVC *)startLiveOverlayerVC{
    if(!_startLiveOverlayerVC){
        _startLiveOverlayerVC = ({
            StartLiveOverlayerVC * object = [[StartLiveOverlayerVC alloc]init];
            object;
       });
    }
    return _startLiveOverlayerVC;
}
@end
