//
//  LiverDetailVC.h
//  BOB
//
//  Created by colin on 2020/11/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"
#import "LivePersonalObj.h"
#import "LiverDetailObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiverDetailVC : BaseViewController

@property (nonatomic, strong) LivePersonalObj *personalObj;

@property (nonatomic, strong) LiverDetailObj *detailObj;

@end

NS_ASSUME_NONNULL_END
