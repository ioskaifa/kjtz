//
//  LiveGoodsListVC.h
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveGoodsListVC : BaseViewController

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

- (void)configureGoods:(NSArray *)dataArr;

@end

NS_ASSUME_NONNULL_END
