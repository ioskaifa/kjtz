//
//  ApplyShowResultVC.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ApplyShowResultVC.h"

@interface ApplyShowResultVC ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *tipLbl;

@property (nonatomic, strong) UIButton *btn;

@end

@implementation ApplyShowResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.applyStatus != ApplyShowResultStatusFail) {
        NSArray *vcArr = self.navigationController.viewControllers;
        NSMutableArray *vcMArr = [NSMutableArray array];
        for (UIViewController *vc in vcArr) {
            if (![vc isKindOfClass:NSClassFromString(@"ApplyShowVC")]) {
                [vcMArr addObject:vc];
            }
        }
        self.navigationController.viewControllers = vcMArr;
    }
}

- (NSString *)imgName {
    NSString *string = @"开播审核中";
    switch (self.applyStatus) {
        case ApplyShowResultStatusIng:{
            string = @"开播审核中";
            break;
        }
        case ApplyShowResultStatusPass:{
            string = @"开播审核通过";
            break;
        }
        case ApplyShowResultStatusFail:{
            string = @"开播审核不通过";
            break;
        }
        default:
            break;
    }
    return string;
}

- (NSString *)tipString {
    NSString *string = @"审核中";
    switch (self.applyStatus) {
        case ApplyShowResultStatusIng:{
            string = @"审核中";
            break;
        }
        case ApplyShowResultStatusPass:{
            string = @"已通过";
            break;
        }
        case ApplyShowResultStatusFail:{
            string = @"不通过";
            break;
        }
        default:
            break;
    }
    return string;
}

- (NSString *)btnString {
    NSString *string = @"";
    switch (self.applyStatus) {
        case ApplyShowResultStatusIng:{
            string = @"";
            break;
        }
        case ApplyShowResultStatusPass:{
            string = @"发起直播";
            break;
        }
        case ApplyShowResultStatusFail:{
            string = @"重新申请";
            break;
        }
        default:
            break;
    }
    return string;
}

- (void)btnClick:(UIButton *)sender {
    switch (self.applyStatus) {
        case ApplyShowResultStatusPass:{
            [self launchLiveBroadcast];
            break;
        }
        case ApplyShowResultStatusFail:{
            [self applyAgain];
            break;
        }
        default:
            break;
    }
}

#pragma mark - 发起直播
- (void)launchLiveBroadcast {
    MLog(@"发起直播")
}

#pragma mark - 重新申请
- (void)applyAgain {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)createUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myHeight = MyLayoutSize.wrap;
    baseLayout.backgroundColor = [UIColor whiteColor];
    
    [baseLayout addSubview:self.imgView];
    [baseLayout addSubview:self.tipLbl];
    if (self.applyStatus != ApplyShowResultStatusIng) {
        [baseLayout addSubview:self.btn];
    }
    [baseLayout layoutIfNeeded];
    
    [self.view addSubview:baseLayout];
    self.view.backgroundColor = [UIColor moBackground];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView autoLayoutImgView:[self imgName]];
        _imgView.myTop = 25;
        _imgView.myCenterX = 0;
    }
    return _imgView;
}

- (UILabel *)tipLbl {
    if (!_tipLbl) {
        _tipLbl = [UILabel new];
        _tipLbl.font = [UIFont boldFont18];
        _tipLbl.textColor = [UIColor blackColor];
        _tipLbl.text = [self tipString];
        [_tipLbl autoMyLayoutSize];
        _tipLbl.myCenterX = 0;
        _tipLbl.myTop = 15;
        _tipLbl.myBottom = 30;
    }
    return _tipLbl;
}

- (UIButton *)btn {
    if (!_btn) {
        _btn = [UIButton new];
        _btn.size = CGSizeMake(180, 44);
        _btn.titleLabel.font = [UIFont font15];
        [_btn setTitleColor:[UIColor colorWithHexString:@"#46474D"] forState:UIControlStateNormal];
        _btn.layer.borderWidth = 1;
        _btn.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
        [_btn setViewCornerRadius:2];
        [_btn setTitle:[self btnString] forState:UIControlStateNormal];
        _btn.mySize = _btn.size;
        _btn.myBottom = 25;
        _btn.myCenterX = 0;
        @weakify(self)
        [_btn addAction:^(UIButton *btn) {
            @strongify(self)
            [self btnClick:btn];
        }];
    }
    return _btn;
}

@end
