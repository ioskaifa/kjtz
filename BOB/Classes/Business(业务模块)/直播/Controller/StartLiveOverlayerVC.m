//
//  StartLiveOverlayerVC.m
//  BOB
//
//  Created by AlphaGo on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "StartLiveOverlayerVC.h"
#import "PhotoBrowser.h"
#import "QNManager.h"
#import "TXLiteAVSDKManage.h"
#import "ManageLiveGoodsView.h"
#import "HWPanModelVC.h"
#import "MLVBLiveRoom.h"
#import "SPBadgeButton.h"
#import "GenerateTestUserSig.h"
#import "TCUserProfileModel.h"
#import "TCAnchorViewController.h"
#import "TCUploadHelper.h"
#import "HUDHelper.h"
#import "TCAccountMgrModel.h"

@interface StartLiveOverlayerVC ()
@property(nonatomic,strong) UIView *topView;
@property(nonatomic,strong) UIView *bottomView;

@property(nonatomic,strong) UIView *coverView;
@property(nonatomic,strong) UIView *toolView;

@property(nonatomic,strong) UILabel *titleLbl;

@property(nonatomic,strong) UIImageView *coverImgView;

@property (nonatomic, strong) ManageLiveGoodsView *manageGoodsView;

@property (nonatomic, strong) HWPanModelVC *panVC;

@property (nonatomic, strong) SPBadgeButton *badgeBtn;

@property (nonatomic, strong) MLVBLiveRoom *liveRoom;


@property (strong) UIImage *selectedCoverImage;
@property(nonatomic,strong) TCRoomInfo *liveInfo;
@property (copy) NSString *coverPic;
@property(nonatomic,assign) BOOL isLogining;;
@end

@implementation StartLiveOverlayerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self.view addMyLinearLayout:MyOrientation_Vert].gravity = MyGravity_Vert_Between;
    [self.view addSubviewOnLinearLayout:self.topView];
    [self.view addSubviewOnLinearLayout:self.bottomView];
//    [self.view addSubview:self.badgeBtn];
//    [self.badgeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(self.badgeBtn.size);
//        make.right.mas_equalTo(self.view.mas_right).mas_offset(-15);
//        make.bottom.mas_equalTo(self.view.mas_bottom).mas_offset(-(safeAreaInsetBottom()+30));
//    }];
    
    [self getNewUserLiveProgress];
}

- (void)getNewUserLiveProgress {
    [self request:@"api/live/userLiveProgress/getNewUserLiveProgress" param:nil completion:^(BOOL success, id object, NSString *error) {
        if(success) {
            NSString *cover_photo = [object valueForKeyPath:@"data.userLiveProgress.cover_photo"];
            if(![StringUtil isEmpty:cover_photo]) {
                [self.coverImgView sd_setImageWithURL:[NSURL URLWithString:cover_photo] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    self.selectedCoverImage = image;
                    self.coverPic = cover_photo;
                }];
            }
            NSString *title = [object valueForKeyPath:@"data.userLiveProgress.title"];
            if(![StringUtil isEmpty:title]) {
                self.titleLbl.text = title;
                self.titleLbl.custom = title;
            }
            
            
        }
    }];
}

- (void)addCoverImage {
    [[PhotoBrowser shared] showPhotoLibrary:self completion:^(NSArray<UIImage *> * _Nullable images, NSArray<PHAsset *> * _Nullable assets, BOOL isOriginal) {
            if([images isKindOfClass:[NSArray class]]) {
                if([[images firstObject] isKindOfClass:[UIImage class]]) {
                    UIImage *image = [images firstObject];
                    //[NotifyHelper showHUDAddedTo:self.view animated:YES];
                    self.selectedCoverImage = image;
                    self.coverImgView.image = image;
                    [self uploadImage];
//                    [[QNManager shared] uploadImage:image completion:^(id data) {
//                        [NotifyHelper hideHUDForView:self.view animated:YES];
//                        if(data && [NSURL URLWithString:data]) {
//                            self.coverImgView.image = image;
//                            self.coverImgView.custom = data;
//                        }
//                    }];
                }
            }
    }];
}

- (UIView *)topView{
    if(!_topView){
        _topView = ({
            UIView * object = [[UIView alloc]init];
            //object.frame = CGRectMake(18, Status_Height, SCREEN_WIDTH-18*2, 130);
            object.myTop = Status_Height;
            object.myLeft = 0;
            object.myRight = 0;
            object.myHeight = 130;
            
            [object addMyLinearLayout:MyOrientation_Horz].gravity = MyGravity_Horz_Between;
            UIImageView *imgView = [object addImage:[UIImage imageNamed:@"关闭"] imageSize:CGSizeMake(24, 24) inset:UIEdgeInsetsMake(0, 18, 0, 0)];
            [imgView addAction:^(UIView *view) {
                [[MXRouter sharedInstance] popViewController];
            }];
            [object addSubviewOnLinearLayout:self.coverView];
            [object addSubviewOnLinearLayout:self.toolView];
            object;
       });
    }
    return _topView;
}

#pragma mark - 通用事件回调
/// @name 通用事件回调
/// @{
/**
 * 错误回调
 *
 * SDK 不可恢复的错误，一定要监听，并分情况给用户适当的界面提示
 *
 * @param errCode     错误码
 * @param errMsg     错误信息
 * @param extraInfo 额外信息，如错误发生的用户，一般不需要关注，默认是本地错误
 */
- (void)onError:(int)errCode errMsg:(NSString*)errMsg extraInfo:(NSDictionary *)extraInfo {
    
}

#pragma mark - Login
//- (void)login {
//    NSString *userID = UDetail.user.user_uid;//[[ProfileManager shared] curUserID];
//    NSString *userSig = [GenerateTestUserSig genTestUserSig:userID];//[[ProfileManager shared] curUserSig];
//
//    MLVBLoginInfo *loginInfo = [MLVBLoginInfo new];
//    loginInfo.sdkAppID = SDKAPPID;
//    loginInfo.userID = userID;
//    loginInfo.userName = UDetail.user.nickname;
//    loginInfo.userAvatar = StrF(@"%@/%@",[QNManager shared].qnHost,UDetail.user.head_photo);
//    loginInfo.userSig = userSig;
//    //_userID = userID;
//    // 初始化LiveRoom
//    //_createBtn.enabled = NO;
//    __weak __typeof(self) weakSelf = self;
//    [self.liveRoom loginWithInfo:loginInfo completion:^(int errCode, NSString *errMsg) {
//        __strong __typeof(weakSelf) self = weakSelf; if (nil == self) return;
////        dispatch_async(dispatch_get_main_queue(), ^{
////            self->_createBtn.enabled = YES;
////        });
//        NSLog(@"init LiveRoom errCode[%d] errMsg[%@]", errCode, errMsg);
//        if (errCode == 0) {
//            [self onLoginSucceed];
//            //weakSelf.initSucc = YES;
//        } else {
//            [self onLoginFailed];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [NotifyHelper showMessageWithMakeText:@"LiveRoom init失败"];
//            });
//
//            //[weakSelf alertTips:@"LiveRoom init失败" msg:errMsg];
//        }
//    }];
//}

#pragma mark -
- (void)onLoginSucceed {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [_createBtn setTitle:@"新建直播间" forState:UIControlStateNormal];
//        _createBtn.enabled = YES;
//        [self requestRoomList];
//    });
}

- (void)onLoginFailed {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [_createBtn setTitle:@"登录" forState:UIControlStateNormal];
//        _createBtn.enabled = YES;
//    });
}

#pragma mark - 上传图片
- (void)uploadImage {
    TCUserProfileData  *profile = [[TCUserProfileModel sharedInstance] getUserProfile ];
    if (self.selectedCoverImage) {
        [[HUDHelper sharedInstance] syncLoading];
        [[TCUploadHelper shareInstance] upload:profile.identifier image:self.selectedCoverImage completion:^(int errCode, NSString *imageSaveUrl) {
            [[HUDHelper sharedInstance] syncStopLoading];
            if (errCode != 0) {
                [[HUDHelper sharedInstance] tipMessage:@"上传图片失败"];
                return;
            }
            
            self.coverPic = imageSaveUrl;
            [[TCUserProfileModel sharedInstance] saveUserCover:imageSaveUrl handler:^(int errCode, NSString *strMsg) {
                if (errCode != ERROR_SUCESS)
                {
                    [[HUDHelper sharedInstance] tipMessage:@"保存图片失败"];
                }
            }];
        }];
    } else {
        [[HUDHelper sharedInstance] tipMessage:@"请选择图片"];
    }
}

#pragma 启动推流界面
- (void)startPush {
    if (self.isLogining) {
        return;
    }
    self.isLogining = YES;
    __block CFTimeInterval start = CFAbsoluteTimeGetCurrent();
    __weak __typeof(self) weakSelf = self;
    [[TCAccountMgrModel sharedInstance] reLoginIfNeeded:^(NSString* username, NSString* pwd){
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong __typeof(weakSelf) self = weakSelf;
            if (self == nil) {
                return;
            }
            
            NSString *myLocation = @"";//self.locationSwitch.isOn ? self.locationLabel.text : @"";
            NSString *myTitle = @"";//self.titleTextView.text;
            //self.publishBtn.enabled = NO;
          
            NSString *userId = username;
            
            TCRoomInfo *publishInfo = [[TCRoomInfo alloc] init];
            publishInfo.userinfo = [[TCUserInfo alloc] init];
            publishInfo.userinfo.location = myLocation;
            publishInfo.title = self.titleLbl.custom ?: UDetail.user.nickname;
            publishInfo.userinfo.username = UDetail.user.nickname;
            publishInfo.userid = userId;
            publishInfo.userinfo.frontcover = (self.coverPic == nil ? @"" : self.coverPic);
            publishInfo.userinfo.headpic = UDetail.user.head_photo;//[[TCUserProfileModel sharedInstance] getUserProfile].faceURL;
            publishInfo.userinfo.nickname = UDetail.user.nickname;//[[TCUserProfileModel sharedInstance] getUserProfile].nickName;
            publishInfo.slaRoomID = [TXLiteAVSDKManage shared].userLiveModel._id;
            self.liveInfo = publishInfo;
            NSLog(@"[TIME] Login time: %.6f", CFAbsoluteTimeGetCurrent() - start);
            [self showPushController];
            self.isLogining = NO;
        });
    } fail:^(int code, NSString *msg) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *err = [NSString stringWithFormat:@"%@(%d)", msg, code];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"登录失败"
                                                            message:err
                                                           delegate:self
                                                  cancelButtonTitle:@"确认"
                                                  otherButtonTitles:nil];
            
            __strong __typeof(weakSelf) self = weakSelf;
            if (self == nil) {
                return;
            }
            
            [alert show];
            self.isLogining = NO;
        });
    }];
    
}

- (IBAction)onSelectSharePlatform:(UIButton *)sender {
//    _selectShare.highlighted = NO;
//    if (_selectShare == sender) {
//        _selectShare = nil;
//        return;
//    }
//    _selectShare = sender;
//    [self performSelector:@selector(doHighlight) withObject:nil afterDelay:0];
}

- (void)doHighlight {
    //[_selectShare setHighlighted:YES];
}

- (void)showPushController {
    CFTimeInterval start = CFAbsoluteTimeGetCurrent();

    TCAnchorViewController *pubVC = [[TCAnchorViewController alloc] initWithPublishInfo:self.liveInfo];
    pubVC.modalPresentationStyle = UIModalPresentationFullScreen;
    //NSString *str = _selectShare.titleLabel.text;
    
    NSLog(@"[TIME] TCPushViewController init time: %.6f", CFAbsoluteTimeGetCurrent() - start);
    start = CFAbsoluteTimeGetCurrent();
    UIViewController *startLiveVC = self.nextResponder.nextResponder;
    [startLiveVC presentViewController:pubVC animated:YES completion:^{
            NSLog(@"[TIME] presentViewController time: %.6f", CFAbsoluteTimeGetCurrent() - start);
        //[startLiveVC removeLastVC];
        
    }];
    self.liveInfo = nil;
}

///开播
- (void)startApplyLiveProgress:(NSDictionary *)param {
    [[TXLiteAVSDKManage shared] applyLiveProgress:param finish:^(id data) {
        if([data isSuccess]) {
            [self startPush];
        } else if([[data valueForKey:@"code"] isEqualToString:@"code_997999"]) { //您已经正在直播，请关闭后再开播
            [self closeUserLiveProgress:param];
        }
    }];
}

///关闭直播
- (void)closeUserLiveProgress:(NSDictionary *)param {
    [[TXLiteAVSDKManage shared] closeUserLiveProgress:^(id data) {
        if([data boolValue]) {
            [self startApplyLiveProgress:param];
        }
    }];
}

- (UIView *)bottomView{
    if(!_bottomView){
        _bottomView = ({
            UIView * object = [[UIView alloc]init];
            object.myBottom = safeAreaInsetBottom()+20;
            object.myLeft = 0;
            object.myRight = 0;
            object.myHeight = 80;
            
            [object addMyLinearLayout:MyOrientation_Vert].gravity = MyGravity_Center;
            UIButton *bnt = [object addButtonWithIcon:nil size:CGSizeMake(210, 50) content:[NSMutableAttributedString initWithTitles:@[@"开始直播"] colors:@[[UIColor whiteColor]] fonts:@[[UIFont boldSystemFontOfSize:16]] placeHolder:@""] inset:UIEdgeInsetsZero];
            bnt.backgroundColor = [UIColor colorWithHexString:@"#02C1C4"];
            [bnt setViewCornerRadius:5];
            [bnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
            [bnt addAction:^(UIButton *btn) {
                if(self.coverPic == nil) {
                    [NotifyHelper showMessageWithMakeText:@"请先设置封面"];
                    return;
                }
                NSDictionary *param = @{@"cover_photo":self.coverPic ?: @"",
                                        @"title":(self.titleLbl.custom ?: UDetail.user.nickname) ?: @"",
                                        @"goods_id_list":self.manageGoodsView.selectGoodsId
                };
                [self startApplyLiveProgress:param];
            }];
            
            UILabel *lbl = [object addOneHorLabel:[NSMutableAttributedString initWithTitles:@[@"开播默认已阅读并同意",@"《CSLA直播规范》"] colors:@[[UIColor whiteColor],[UIColor colorWithHexString:@"#1DB1FF"]] fonts:@[[UIFont systemFontOfSize:11],[UIFont systemFontOfSize:11]] placeHolder:@""] inset:UIEdgeInsetsMake(15, 0, 0, 0)];
            lbl.textAlignment = NSTextAlignmentCenter;
            [lbl addAction:^(UIView *view) {
                NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
                NSString *filePath = [resourcePath stringByAppendingPathComponent:@"zhiborule.html"];
                NSString*htmlstring = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
                MXRoute(@"MBWebVC", (@{@"content":htmlstring ?: @"",@"baseUrl":filePath ?: @"",@"navTitle":@"CSLA直播规范"}));
            }];
            object;
       });
    }
    return _bottomView;
}

- (UIView *)coverView{
    if(!_coverView){
        _coverView = ({
            CGFloat width = SCREEN_WIDTH-(18+24+20)*2;
            
            UIView * object = [[UIView alloc]init];
            object.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
            [object setViewCornerRadius:5];
            
            object.myTop = 15;
            object.myLeft = 0;
            object.myWidth = width;
            object.myHeight = 90;
            
            [object addMyLinearLayout:MyOrientation_Horz].gravity = MyGravity_Horz_Between | MyGravity_Vert_Center;
            UIView *addView = [UIView topImageBottomLabel:[UIImage imageNamed:@"联合"] imageSize:CGSizeMake(20, 20) title:[NSMutableAttributedString initWithTitles:@[@"添加封面"] colors:@[[UIColor whiteColor]] fonts:@[[UIFont systemFontOfSize:10]] placeHolder:@""] inset:UIEdgeInsetsZero action:^(id data) {
                [self addCoverImage];
            }];
            [object addSubviewOnLinearLayout:addView];
            [addView findLinearLayout].gravity = MyGravity_Vert_Center;
            addView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
            [addView setViewCornerRadius:3];
            [addView resetMyLayoutSetting];
            addView.myTop = 0;
            addView.myLeft = 10;
            addView.myWidth = 70;
            addView.myHeight = 70;
            [addView addSubview:self.coverImgView];
            
            UIView *rightView = [object addUIViewMyLinearLayout:MyOrientation_Vert];
            [rightView resetMyLayoutSetting];
            rightView.myTop = 0;
            rightView.myLeft = 0;
            rightView.myWidth = width - 20 - 70;
            rightView.myHeight = 60;
            [rightView findLinearLayout].gravity = MyGravity_Vert_Between;
            self.titleLbl = [rightView addOneHorLabel:[NSMutableAttributedString initWithTitles:@[@"6字以上的标题更能提高人气"] colors:@[[UIColor colorWithWhite:1 alpha:0.7]] fonts:@[[UIFont boldSystemFontOfSize:14]] placeHolder:@""] inset:UIEdgeInsetsZero];
            self.titleLbl.numberOfLines = 0;
            @weakify(self)
            [self.titleLbl addAction:^(UIView *view) {
                @strongify(self)
                [MXAlertViewHelper showAlertViewWithTextFiled:@"6字以上的标题更能提高人气" title:@"请输入标题" okTitle:@"确定" cancelTitle:@"取消" completion:^(NSString *text, NSInteger buttonIndex) {
                    if(buttonIndex == 0) {
                        if(![StringUtil isEmpty:text]) {
                            self.titleLbl.text = text;
                            self.titleLbl.custom = text;
                        }
                    }
                }];
            }];
            UIButton *bnt = [rightView addOneButtonWithIcon:@"待选" selectedIcon:@"选中" content:[NSMutableAttributedString initWithTitles:@[@"  开播通知粉丝    "] colors:@[[UIColor colorWithWhite:1 alpha:0.7]] fonts:@[[UIFont systemFontOfSize:10]] placeHolder:@""] inset:UIEdgeInsetsZero];
            [bnt addAction:^(UIButton *btn) {
                btn.selected = !btn.isSelected;
            }];
            object;
       });
    }
    return _coverView;
}

- (UIView *)toolView{
    if(!_toolView){
        _toolView = ({
            UIView * object = [[UIView alloc]init];
            object.myTop = 0;
            object.myWidth = 27;
            object.myRight = 18;
            object.myHeight = 110;
            object.visibility = MyVisibility_Invisible;
            
            [object addMyLinearLayout:MyOrientation_Vert].gravity = MyGravity_Vert_Between;
            [object addSubviewOnLinearLayout:[UIView topImageBottomLabel:[UIImage imageNamed:@"翻转"] imageSize:CGSizeMake(27, 27) title:[NSMutableAttributedString initWithTitles:@[@"翻转"] colors:@[[UIColor whiteColor]] fonts:@[[UIFont systemFontOfSize:11]] placeHolder:@""] inset:UIEdgeInsetsMake(0, 0, 48, 0) action:^(id data) {
                [[TXLiteAVSDKManage shared] switchCamera];
            }]];
            
            [object addSubviewOnLinearLayout:[UIView topImageBottomLabel:[UIImage imageNamed:@"美化"] imageSize:CGSizeMake(27, 27) title:[NSMutableAttributedString initWithTitles:@[@"美化"] colors:@[[UIColor whiteColor]] fonts:@[[UIFont systemFontOfSize:11]] placeHolder:@""] inset:UIEdgeInsetsMake(0, 0, 48, 0) action:^(id data) {
                [[TXLiteAVSDKManage shared] showBeautyPanel];
                //self.bottomView.visibility = MyVisibility_Invisible;
            }]];

            object;
       });
    }
    return _toolView;
}

- (UIImageView *)coverImgView{
    if(!_coverImgView){
        _coverImgView = ({
            UIImageView * object = [[UIImageView alloc]init];
            object.frame = CGRectMake(0, 0, 70, 70);
            object.contentMode = UIViewContentModeScaleAspectFill;
            object;
       });
    }
    return _coverImgView;
}

- (ManageLiveGoodsView *)manageGoodsView {
    if (!_manageGoodsView) {
        _manageGoodsView = [ManageLiveGoodsView new];
        _manageGoodsView.size = CGSizeMake(SCREEN_WIDTH, [ManageLiveGoodsView viewHeight]);
        @weakify(self)
        [_manageGoodsView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.panVC dismiss];
            self.badgeBtn.badge.badgeText = StrF(@"%zd", self->_manageGoodsView.selectCount);
        }];
    }
    return _manageGoodsView;
}

- (HWPanModelVC *)panVC {
    if (!_panVC) {
        _panVC = [HWPanModelVC showInVC:[[MXRouter sharedInstance] getTopNavigationController] customView:self.manageGoodsView];
        @weakify(self)
        _panVC.willDismissblock = ^(id data) {
            @strongify(self)
            self.badgeBtn.badge.badgeText = StrF(@"%zd", self.manageGoodsView.selectCount);
        };
        
    }
    return _panVC;
}

- (SPBadgeButton *)badgeBtn {
    if (!_badgeBtn) {
        _badgeBtn = [SPBadgeButton new];
        _badgeBtn.imagePosition = SPButtonImagePositionTop;
        _badgeBtn.imageTitleSpace = 5;
        _badgeBtn.titleLabel.font = [UIFont font11];
        [_badgeBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
        [_badgeBtn setTitle:@"带货" forState:UIControlStateNormal];
        [_badgeBtn setImage:[UIImage imageNamed:@"带货"] forState:UIControlStateNormal];
        [_badgeBtn sizeToFit];        
        _badgeBtn.badge.badgeText = @"0";
        @weakify(self)
        [_badgeBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self.panVC show];
        }];
    }
    return _badgeBtn;
}

@end
