//
//  LivePlayerVC.h
//  BOB
//
//  Created by mac on 2020/10/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LivePlayerVC : BaseViewController
///拉流地址
@property (nonatomic, copy) NSString *bfUrl;
///拉流类型
@property (nonatomic, assign) NSInteger bfType;

@end

NS_ASSUME_NONNULL_END
