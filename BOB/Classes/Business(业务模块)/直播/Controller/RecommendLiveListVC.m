//
//  RecommendLiveListVC.m
//  BOB
//
//  Created by mac on 2020/10/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "RecommendLiveListVC.h"
#import "LiveListCell.h"
#import "LiveListObj.h"
#import "MLVBLiveRoomDef.h"
#import "TCRoomListModel.h"
#import "TCUtil.h"
#import "GenerateTestUserSig.h"
#import "NSArray+BlocksKit.h"
#import "TCAudienceViewController.h"

@interface RecommendLiveListVC ()<UICollectionViewDelegate, UICollectionViewDataSource,MLVBLiveRoomDelegate>

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, strong) MLVBLiveRoom *liveRoom;
@property (nonatomic, strong) TCRoomListMgr *liveListMgr;
@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL hasEnterplayVC;

@property (nonatomic, assign) VideoType videoType;

@end

@implementation RecommendLiveListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    //[self fetchData];
    
    _liveListMgr = [TCRoomListMgr sharedMgr];
    _liveRoom = [MLVBLiveRoom sharedInstance];
    [_liveRoom setCameraMuteImage:[UIImage imageNamed:@"pause_publish.jpg"]];
    _liveRoom.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [TCRoomListMgr sharedMgr].liveRoom.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newDataAvailable:) name:kTCRoomListNewDataAvailable object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(listDataUpdated:) name:kTCRoomListUpdated object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(svrError:) name:kTCRoomListSvrError object:nil];
    
    self.playVC = nil;
    self.hasEnterplayVC = NO;
//    if(self.dataSourceMArr.count > 0) {
//        return;
//    }
    [self setup:VideoType_LIVE_Online];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTCRoomListNewDataAvailable object:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:kTCLiveListUpdated object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTCRoomListSvrError object:nil];
}

- (void)fetchData {
    if ([self.last_id isEqualToString:@""]) {
        [self.dataSourceMArr removeAllObjects];
    }
    NSString *url = @"api/live/userLiveProgress/getUserLiveProgressList";
    url = StrF(@"%@/%@", LcwlServerRoot, url);
    NSDictionary *param = @{@"last_id":self.last_id};
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url);
        net.params(param);
        net.finish(^(id data){
            [self colViewEndRefreshing];
            NSDictionary *dataDic = (NSDictionary *)data[@"data"];
            if ([data isSuccess]) {
                NSArray *dataArr = [LiveListObj modelListParseWithArray:dataDic[@"userLiveProgressList"]];
                [self.dataSourceMArr addObjectsFromArray:dataArr];
                LiveListObj *lastObj = (LiveListObj *)[self.dataSourceMArr lastObject];
                if (lastObj) {
                    self.last_id = lastObj.ID;
                }
                [self.colView reloadData];
            } else {
                [NotifyHelper showMessageWithMakeText:dataDic[@"msg"]];
            }
        }).failure(^(id error){
            [self colViewEndRefreshing];
            [NotifyHelper showMessageWithMakeText:[error description]];
            NSArray *dataArr = [LiveListObj liveListObj];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            LiveListObj *lastObj = (LiveListObj *)[self.dataSourceMArr lastObject];
            if (lastObj) {
                self.last_id = lastObj.ID;
            }
            [self.colView reloadData];
        })
        .execute();
    }];
}

- (void)colViewEndRefreshing {
    [self.colView.mj_header endRefreshing];
    [self.colView.mj_footer endRefreshing];
}

#pragma mark - Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = LiveListCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    TCRoomInfo *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:[TCRoomInfo class]]) {
        return;
    }
    if (![cell isKindOfClass:[LiveListCell class]]) {
        return;
    }
    LiveListCell *listCell = (LiveListCell *)cell;
    listCell.cellType = LiveListCellTypeRecommend;
    [listCell configureViewEx:obj];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    TCRoomInfo *info = self.dataSourceMArr[indexPath.row];
    if (![info isKindOfClass:TCRoomInfo.class]) {
        return;
    }
    @weakify(self)
    void(^onPlayError)(void) = ^{
        @strongify(self)
        if (self == nil) return;
        //加房间失败后，刷新列表，不需要刷新动画
        self.dataSourceMArr = [NSMutableArray array];
        self.isLoading = YES;
        [self.liveListMgr queryVideoList:VideoType_LIVE_Online getType:GetType_Up];
    };
    void(^onSheild)(TCRoomInfo *) = ^(TCRoomInfo *roomInfo) {
        @strongify(self)
        if (self == nil) return;
        [self.dataSourceMArr removeObject:roomInfo];
        // 加入黑名单
        [self.liveListMgr updateBlackList:roomInfo];
        [self.colView reloadData];
    };
    self.playVC = [[TCAudienceViewController alloc] initWithPlayInfo:info videoIsReady:^{
        @strongify(self)
        if (!self.hasEnterplayVC) {
            self.hasEnterplayVC = YES;
        }
    }];
    [(TCAudienceViewController*)_playVC setOnPlayError:onPlayError];
    [(TCAudienceViewController*)_playVC setOnShield:onSheild];
    [self.navigationController pushViewController:self.playVC animated:YES];
    // 视图跳转
//    LiveRoomPlayerViewController *vc = [[LiveRoomPlayerViewController alloc] init];
//    vc.roomID = roomInfo.roomID;
//    vc.roomName = roomInfo.roomInfo;
//    vc.userName = @"";
//    vc.liveRoom = self.liveRoom;
//    self.liveRoom.delegate = vc;
//
//    [self.navigationController pushViewController:vc animated:YES];
    
//    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
//    [param setValue:roomInfo.roomID forKey:@"roomID"];
//    [param setValue:roomInfo.roomInfo forKey:@"roomName"];
//    [param setValue:@"" forKey:@"userName"];
//    [param setValue:self.liveRoom forKey:@"liveRoom"];
//
//    MXRoute(@"LiveRoomPlayerViewController", param);
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    layout.backgroundColor = [UIColor moBackground];
    [self.view addSubview:layout];
    
    UIImage *img = [UIImage imageNamed:@"申请金牌主播"];
    CGFloat width = SCREEN_WIDTH - 30;
    UIImageView *imgView = [UIImageView new];
    imgView.image = img;
    imgView.size = CGSizeMake(width, img.size.height * width / img.size.width);
    imgView.mySize = imgView.size;
    imgView.myTop = 10;
    imgView.myLeft = 15;
    imgView.myRight = 15;
    [imgView addAction:^(UIView *view) {
        MXRoute(@"ApplyAnchorVC", nil);
    }];
    
    [layout addSubview:imgView];
    
    UIButton *closeBtn = [UIButton new];
    [closeBtn setImage:[UIImage imageNamed:@"金牌主播关闭"] forState:UIControlStateNormal];
    closeBtn.size = CGSizeMake(30, 30);
    closeBtn.mySize = closeBtn.size;
    [imgView addSubview:closeBtn];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(closeBtn.size);
        make.right.mas_equalTo(imgView.mas_right).mas_offset(-8);
        make.top.mas_equalTo(imgView.mas_top).mas_offset(0);
    }];
    [closeBtn addAction:^(UIButton *btn) {
        imgView.visibility = MyVisibility_Gone;
    }];
    
    [layout addSubview:self.colView];
}

/// @name 通用事件回调
/// @{
/**
 * 错误回调
 *
 * SDK 不可恢复的错误，一定要监听，并分情况给用户适当的界面提示
 *
 * @param errCode     错误码
 * @param errMsg     错误信息
 * @param extraInfo 额外信息，如错误发生的用户，一般不需要关注，默认是本地错误
 */
- (void)onError:(int)errCode errMsg:(NSString*)errMsg extraInfo:(NSDictionary *)extraInfo {
    
}

#pragma mark - Init
- (UICollectionView *)colView {
    if (!_colView) {
        _colView = ({
            UICollectionView *object = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
            object.backgroundColor = [UIColor moBackground];
            object.delegate = self;
            object.dataSource = self;
            object.weight = 1;
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            Class cls = LiveListCell.class;
            [object registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
            @weakify(self);
            object.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
                @strongify(self);
                self.isLoading = YES;
                [self.liveListMgr queryVideoList:self.videoType getType:GetType_Up];
            }];
            object.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
                @strongify(self);
                self.isLoading = YES;
                [self.liveListMgr queryVideoList:self.videoType getType:GetType_Down];
            }];
            [(MJRefreshHeader *)_colView.mj_header endRefreshingWithCompletionBlock:^{
                self.isLoading = NO;
            }];
            [(MJRefreshHeader *)_colView.mj_footer endRefreshingWithCompletionBlock:^{
                self.isLoading = NO;
            }];
            object;
        });
    }
    return _colView;
}

- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = ({
            UICollectionViewFlowLayout *object = [UICollectionViewFlowLayout new];
            CGFloat width = (SCREEN_WIDTH - 40) / 2.0;
            object.itemSize = CGSizeMake(width, [LiveListCell viewHeight]);
            object.minimumLineSpacing = 10;
            object.minimumInteritemSpacing = 10;
            object.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
            object;
        });
    }
    return _flowLayout;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (void)endRefresh {
    [self.colView.mj_header endRefreshing];
    [self.colView.mj_footer endRefreshing];}

- (void)setup:(VideoType)type {
    self.videoType = type;
    [self endRefresh];
    // 先加载缓存的数据，然后再开始网络请求，以防用户打开是看到空数据
    [self.liveListMgr loadLivesFromArchive:type];
    [self doFetchList];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.colView.mj_header beginRefreshing];
    });
}

#pragma mark - Net fetch
/**
 * 拉取直播列表。TCLiveListMgr在启动是，会将所有数据下载下来。在未全部下载完前，通过loadLives借口，
 * 能取到部分数据。通过finish接口，判断是否已取到最后的数据
 *
 */
- (void)doFetchList {
    if ([NSThread isMainThread]) {
        [self _doFetchList];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self _doFetchList];
        });
    }
}

- (void)_doFetchList {
    if(self.liveListMgr.currentPage == 1) {
        [self.dataSourceMArr removeAllObjects];
    }
    NSRange range = NSMakeRange(self.dataSourceMArr.count, 20);
    BOOL finish;
    NSArray *result = [self.liveListMgr readRoomList:range finish:&finish];
    if (result.count) {
        result = [self mergeResult:result];
        [self.dataSourceMArr addObjectsFromArray:result];
    } else {
        if (!finish) {
            return; // 等待新数据的通知过来
        }
    }
    [self.colView reloadData];
    [self endRefresh];
}

/**
 *  将取到的数据于已存在的数据进行合并。
 *
 *  @param result 新拉取到的数据
 *
 *  @return 新数据去除已存在记录后，剩余的数据
 */
- (NSArray *)mergeResult:(NSArray *)result {
    
    // 每个直播的播放地址不同，通过其进行去重处理
    NSArray *existArray = [self.dataSourceMArr bk_map:^id(TCRoomInfo *obj) {
        return obj.playurl;
    }];
    NSArray *newArray = [result bk_reject:^BOOL(TCRoomInfo *obj) {
        return [existArray containsObject:obj.playurl];
    }];
    
    return newArray;
}

/**
 *  TCLiveListMgr有新数据过来
 *
 *  @param noti
 */
- (void)newDataAvailable:(NSNotification *)noti {
    [self doFetchList];
}

/**
 *  TCLiveListMgr数据有更新
 *
 *  @param noti
 */
- (void)listDataUpdated:(NSNotification *)noti {
    NSDictionary* dict = noti.userInfo;
    NSString* userId = nil;
    NSString* fileId = nil;
    int type = 0;
    if (dict[@"userid"])
        userId = dict[@"userid"];
    if (dict[@"type"])
        type = [dict[@"type"] intValue];
    if (dict[@"fileid"])
        fileId = dict[@"fileid"];
    
    TCRoomInfo* info = [_liveListMgr readRoom:type userId:userId fileId:fileId];
    if (nil == info)
        return;
    
    for (TCRoomInfo* item in self.dataSourceMArr)
    {
        if ([info.userid isEqualToString:item.userid])
        {
            item.viewercount = info.viewercount;
            item.likecount = info.likecount;
            break;
        }
    }
    
    [self.colView reloadData];
}

/**
 *  TCLiveListMgr内部出错
 *
 *  @param noti
 */
- (void)svrError:(NSNotification *)noti {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSError *e = noti.object;
        NSDictionary *userInfo = e.userInfo;
        if ([userInfo isKindOfClass:[NSDictionary class]]) {
            int errCode = [userInfo[@"errCode"] intValue];
            //直播 errCode：-1   点播 errCode ： 498
            if (errCode == 498 || errCode == -1) {
                //token 验证失败，请重新登录
            }else{
                [NotifyHelper showMessageWithMakeText:StrF(@"errCode : %@ des: %@",userInfo[@"errCode"] , userInfo[@"description"])];
            }
        }else{
            [NotifyHelper showMessageWithMakeText:@"拉取列表失败"];
        }

        // 如果还在加载，停止加载动画
        if (self.isLoading) {
            [self endRefresh];
            self.isLoading = NO;
        }
    });
}

@end
