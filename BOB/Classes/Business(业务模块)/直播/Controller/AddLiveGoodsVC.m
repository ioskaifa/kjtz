//
//  AddLiveGoodsVC.m
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AddLiveGoodsVC.h"
#import "AddLiveGoodsCell.h"
#import "IQKeyboardManager.h"

@interface AddLiveGoodsVC ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, strong) UITextField *searchTF;

@property (nonatomic, copy) NSString *key_word;

@end

@implementation AddLiveGoodsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
}

#pragma mark - Private Method
- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    if ([@"AddLiveGoodsCellSelect" isEqualToString:eventName]) {
        [self goodsDidSelect:userInfo[@"cellObj"]];
    } else {
        [super routerEventWithName:eventName userInfo:userInfo];
    }
}

#pragma mark 全选/反选
- (void)allSelectClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    [self setAllGoodsSelected:sender.selected];
}

#pragma mark 设置所有商品选中与否
- (void)setAllGoodsSelected:(BOOL)selected {
    for (LiveGoodsObj *obj in self.dataSourceMArr) {
        if (![obj isKindOfClass:LiveGoodsObj.class]) {
            continue;
        }
        obj.isSelected = selected;
    }
    [self.tableView reloadData];
    [self configureBottomView];
}

- (void)configureBottomView {
    NSArray *tempArr = [self selectedGoods];
    [self.bottomView configureView:tempArr.count];
    if (tempArr.count == self.dataSourceMArr.count && self.dataSourceMArr.count != 0) {
        self.bottomView.selectBtn.selected = YES;
    } else {
        self.bottomView.selectBtn.selected = NO;
    }
}

#pragma mark 商品选中
- (void)goodsDidSelect:(LiveGoodsObj *)obj {
    if (![obj isKindOfClass:LiveGoodsObj.class]) {
        return;
    }
    NSArray *selectArr = [self selectedGoods];
    if (self.isSingleChoice) {
        LiveGoodsObj *selectObj = selectArr.firstObject;
        if ([selectObj isKindOfClass:LiveGoodsObj.class]) {
            if (obj == selectObj) {
                if (obj.isSelected) {
                    return;
                }
            } else {
                selectObj.isSelected = NO;
                obj.isSelected = YES;
            }
        } else {
            obj.isSelected = !obj.isSelected;
        }
    } else {
        obj.isSelected = !obj.isSelected;
    }
    [self.tableView reloadData];
    [self configureBottomView];
}

#pragma mark 获取选中的商品
- (NSArray *)selectedGoods {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected == YES"];
    NSArray *tempArr = [self.dataSourceMArr filteredArrayUsingPredicate:predicate];
    return tempArr;
}

- (BOOL)valiParam {
    if (self.dataSourceMArr.count == 0) {
        return NO;
    }
    
    if ([self selectedGoods].count == 0) {
        [NotifyHelper showMessageWithMakeText:@"请选择要添加的商品"];
        return NO;
    }
    
    return YES;
}

- (void)commit {
    
}

- (void)fetchData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [self request:@"api/live/userLiveGoods/getLiveGoodsList"
            param:@{@"last_id":self.last_id,
                    @"key_word":self.key_word,
                    @"anchor_user_id": UDetail.user.user_id
            }
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefreshing];
        if (success) {
            NSArray *dataArr = object[@"data"][@"liveGoodsList"];
            dataArr = [LiveGoodsObj modelListParseWithArray:dataArr];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            LiveGoodsObj *obj = [self.dataSourceMArr lastObject];
            if ([obj isKindOfClass:LiveGoodsObj.class]) {
                self.last_id = obj.order_num;
            }
            [self.tableView reloadData];
            [self configureBottomView];
        }
    }];
}

- (void)endRefreshing {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = AddLiveGoodsCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:AddLiveGoodsCell.class]) {
        return;
    }
    AddLiveGoodsCell *listCell = (AddLiveGoodsCell *)cell;
    listCell.cellType = AddLiveGoodsCellTypeShow;
    [listCell configureView:self.dataSourceMArr[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    LiveGoodsObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:LiveGoodsObj.class]) {
        return;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.searchTF) {
        self.key_word = [StringUtil trim:self.searchTF.text];
        if ([StringUtil isEmpty:self.key_word]) {
            return YES;
        }
        self.last_id = @"";
        [self.dataSourceMArr removeAllObjects];
        [self fetchData];
    }
    return YES;
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        NSString *keyWord = [StringUtil trim:self.searchTF.text];
        if ([self.key_word isEqualToString:keyWord] && ![StringUtil isEmpty:keyWord]) {
            return;
        }
        self.key_word = keyWord;
        self.last_id = @"";
        [self.dataSourceMArr removeAllObjects];
        [self fetchData];
    }
}

#pragma mark - InitUI
- (void)createUI {
    [self setNavBarTitle:@"添加商品"];
    self.last_id = @"";
    self.key_word = @"";
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.view addSubview:baseLayout];
    
    [baseLayout addSubview:self.searchTF];
    [baseLayout addSubview:self.tableView];
//    [baseLayout addSubview:[UIView addBottomSafeAreaWithContentView:self.bottomView]];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [AddLiveGoodsCell viewHeight];
        Class cls = AddLiveGoodsCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:   NSStringFromClass(cls)];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
        _tableView.myTop = 5;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = [UITextField new];
        _searchTF.delegate = self;
        [_searchTF addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
        [_searchTF setViewCornerRadius:5];
        _searchTF.font = [UIFont font15];
        _searchTF.backgroundColor = [UIColor colorWithHexString:@"#F6F6F9"];
        UIView *leftView = [UIView new];
        leftView.size = CGSizeMake(10, 30);
        
        UIView *rightView = [UIView new];
        rightView.size = CGSizeMake(40, 30);
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
        [leftBtn sizeToFit];
        leftBtn.x = 10;
        leftBtn.y = 5;
        [rightView addSubview:leftBtn];
        _searchTF.rightView = rightView;
        _searchTF.leftView = leftView;
        _searchTF.returnKeyType = UIReturnKeySearch;
        _searchTF.leftViewMode = UITextFieldViewModeAlways;
        _searchTF.rightViewMode = UITextFieldViewModeAlways;
        _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索商品"]
                                                                     
                                                                     colors:@[[UIColor colorWithHexString:@"#B2B2C1"]]
                                                                      
                                                                      fonts:@[[UIFont font14]]];
        _searchTF.attributedPlaceholder = att;
        _searchTF.myLeft = 15;
        _searchTF.myRight = 15;
        _searchTF.myHeight = 35;
        _searchTF.myTop = 10;
    }
    return _searchTF;
}

- (AddLiveGoodsBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [AddLiveGoodsBottomView new];
        _bottomView.size = CGSizeMake(SCREEN_WIDTH, [AddLiveGoodsBottomView viewHeight]);
        _bottomView.mySize = _bottomView.size;
        @weakify(self)
        [_bottomView.selectBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self allSelectClick:btn];
        }];
    }
    return _bottomView;
}

@end
