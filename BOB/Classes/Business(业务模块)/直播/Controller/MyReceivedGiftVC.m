//
//  MyReceivedGiftVC.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyReceivedGiftVC.h"
#import "MyReceivedGiftCell.h"
#import "MyReceivedGiftObj.h"

@interface MyReceivedGiftVC ()

@end

@implementation MyReceivedGiftVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNavBarTitle:@"我的礼物"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - Override Super Class Method
- (HTTPRequestMethod)httpMethod {
    return HTTPRequestMethodPost;
}

- (NSDictionary *)requestArgument {
    return @{};
}

- (NSString *)requestUrl {
    return @"api/wallet/info/getWalletRecordList";
}

///请求时传入last id的key
- (NSString *)httpRequstLastIdKey {
    return @"last_id";
}

///上拉刷新时从数据源数组中最后一个元素取值的key
- (NSString *)getValueFromItemLastIdKey {
    return @"ID";
}

///后台返回数据中数组的keypath
- (NSString *)dataListKeyPath {
    return @"data.walletRecordList";
}

///数据源中的model,需要继承BaseObject
- (Class)model {
    return [MyReceivedGiftObj class];
}

///是否需要上拉刷新
- (BOOL)needPullUpRefresh {
    return YES;
}

///是否需要下拉刷新
- (BOOL)needPullDownRefresh {
    return YES;
}

#pragma mark - setup
///配置tableVeiw
- (void)configTableView:(UITableView *)tableView {
    tableView.backgroundColor = [UIColor moBackground];
    [tableView std_registerCellClass:[MyReceivedGiftCell class]];
    
    STDTableViewSection *sectionData = [[STDTableViewSection alloc] initWithCellClass:[MyReceivedGiftCell class]];
    [tableView std_addSection:sectionData];
}

- (void)setupTableViewDataSource
{
    
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [MyReceivedGiftCell viewHeight];
}

@end
