//
//  MyFollowListVC.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyFollowListVC.h"
#import "MayInterestedView.h"
#import "MyFollowListObj.h"
#import "MyFansListCell.h"

@interface MyFollowListVC ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) MayInterestedView *mayInterestedView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UITextField *searchTF;

@property (nonatomic, copy) NSString *key_word;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, strong) NSArray *mayInterestedArr;

@end

@implementation MyFollowListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    if ([eventName isEqualToString:@"MayInterestedCellFollow"]) {
        [self followAnchor:userInfo[@"cellObj"]];
    } else {
        [super routerEventWithName:eventName userInfo:userInfo];
    }
}

- (void)fetchData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [self request:@"/api/live/userLiveFollow/getUserFollowList"
            param:@{@"last_id":self.last_id?:@"", @"key_word":self.key_word?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        [self endRefreshing];
        if (success) {
            NSArray *arr = [MyFollowListObj modelListParseWithArray:object[@"data"][@"userFollowList"]];
            MyFollowListObj *obj = arr.lastObject;
            if ([obj isKindOfClass:MyFollowListObj.class]) {
                self.last_id = obj.ID;
            }
            [self.dataSourceMArr addObjectsFromArray:arr];
            [self.tableView reloadData];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
    [self fetchMayInterestData];
}

- (void)fetchMayInterestData {
    [self request:@"/api/live/userLiveFollow/getAnchorUserList"
            param:@{@"last_id":@""}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSArray *arr = [MayInterestedObj modelListParseWithArray:object[@"data"][@"anchorUserList"]];
            self.mayInterestedArr = arr;
            if (arr.count > 0) {
                if (!self.tableView.tableHeaderView) {
                    self.tableView.tableHeaderView = self.mayInterestedView;
                }
                [self.mayInterestedView configureView:arr];
            }
        }
    }];
}

#pragma mark - 关注主播
- (void)followAnchor:(MayInterestedObj *)obj {
    if (![obj isKindOfClass:MayInterestedObj.class]) {
        return;
    }
    [self request:@"api/live/userLiveFollow/followAnchor"
            param:@{@"anchor_user_id":obj.ID?:@""}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            obj.isFollow = YES;
            [self.mayInterestedView reloadData];
        } else {
            [NotifyHelper showMessageWithMakeText:error];
        }
    }];
}

- (void)endRefreshing {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MyFansListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:MyFansListCell.class]) {
        return;
    }
    MyFansListCell *listCell = (MyFansListCell *)cell;
    [listCell configureView:self.dataSourceMArr[indexPath.row]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.searchTF) {
        self.key_word = [StringUtil trim:self.searchTF.text];
        if ([StringUtil isEmpty:self.key_word]) {
            return YES;
        }
        self.last_id = @"";
        [self.dataSourceMArr removeAllObjects];
        [self fetchData];
    }
    return YES;
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        self.key_word = [StringUtil trim:self.searchTF.text];
        self.last_id = @"";
        [self.dataSourceMArr removeAllObjects];
        [self fetchData];
    }
}

- (void)createUI {
    [self setNavBarTitle:@"我的关注"];
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.view addSubview:baseLayout];
    
    [baseLayout addSubview:self.searchTF];
    [baseLayout addSubview:self.tableView];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [MyFansListCell viewHeight];
        Class cls = MyFansListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:   NSStringFromClass(cls)];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = [UITextField new];
        _searchTF.delegate = self;
        [_searchTF addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
        [_searchTF setViewCornerRadius:5];
        _searchTF.font = [UIFont font15];
        _searchTF.backgroundColor = [UIColor colorWithHexString:@"#F6F6F9"];
        UIView *leftView = [UIView new];
        leftView.size = CGSizeMake(10, 30);
        
        UIView *rightView = [UIView new];
        rightView.size = CGSizeMake(40, 30);
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
        [leftBtn sizeToFit];
        leftBtn.x = 10;
        leftBtn.y = 5;
        [rightView addSubview:leftBtn];
        _searchTF.rightView = rightView;
        _searchTF.leftView = leftView;
        _searchTF.returnKeyType = UIReturnKeySearch;
        _searchTF.leftViewMode = UITextFieldViewModeAlways;
        _searchTF.rightViewMode = UITextFieldViewModeAlways;
        _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索已关注的人"]
                                                                     
                                                                     colors:@[[UIColor colorWithHexString:@"#B2B2C1"]]
                                                                      
                                                                      fonts:@[[UIFont font14]]];
        _searchTF.attributedPlaceholder = att;
        _searchTF.myLeft = 15;
        _searchTF.myRight = 15;
        _searchTF.myHeight = 35;
        _searchTF.myTop = 10;
    }
    return _searchTF;
}

- (MayInterestedView *)mayInterestedView {
    if (!_mayInterestedView) {
        _mayInterestedView = [MayInterestedView new];
        _mayInterestedView.size = CGSizeMake(SCREEN_WIDTH, [MayInterestedView viewHeight]);
    }
    return _mayInterestedView;
}

@end
