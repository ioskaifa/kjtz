//
//  LivePersonalVC.m
//  BOB
//
//  Created by colin on 2020/11/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LivePersonalVC.h"
#import "MyListCell.h"

@interface LivePersonalVC ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@end

@implementation LivePersonalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)fetchData {
    
}

#pragma mark - Delegate
#pragma mark UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = [MyListCell class];
    MyListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    NSDictionary *obj = self.dataSourceArr[indexPath.row];
    MyListCell *listCell = (MyListCell *)cell;
    [listCell configureView:obj];
    if (indexPath.row == self.dataSourceArr.count - 1) {
        listCell.line.hidden = YES;
    } else {
        listCell.line.hidden = NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    NSDictionary *dic = self.dataSourceArr[indexPath.row];
    if (![dic isKindOfClass:[NSDictionary class]]) {
        return;
    }
    if ([StringUtil isEmpty:dic[@"title"]]) {
        return;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        if([StringUtil isEmpty:dic[@"openUrl"]]) {
            return;
        }
        MXRoute(dic[@"openUrl"], nil);
    }];
}

- (void)createUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.view addSubview:baseLayout];
    
    [baseLayout addSubview:self.tableView];
}

- (MyBaseLayout *)headerView {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 50;
    layout.myLeft = 25;
    layout.myRight = 25;
    layout.myHeight = MyLayoutSize.wrap;
    [baseLayout addSubview:layout];
    
    UIImageView *imgView = [UIImageView new];
    imgView.size = CGSizeMake(60, 60);
    imgView.mySize = imgView.size;
    imgView.backgroundColor = [UIColor moBackground];
    [imgView sd_setImageWithURL:[NSURL URLWithString:self.personalObj.photo]];
    [layout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont font12];
    lbl.backgroundColor = [UIColor colorWithHexString:@"#151419"];
    lbl.text = StrF(@"V%@", self.personalObj.level);
    [lbl setViewCornerRadius:2];
    lbl.size = CGSizeMake(25, 15);
    [imgView addSubview:lbl];
    lbl.hidden = YES;
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(lbl.size);
        make.bottom.mas_equalTo(imgView.mas_bottom);
        make.right.mas_equalTo(imgView.mas_right);
    }];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.myTop = 0;
    subLayout.myLeft = 15;
    subLayout.myRight = 0;
    subLayout.weight = 1;
    subLayout.myHeight = imgView.height;
    [layout addSubview:subLayout];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#151419"];
    lbl.font = [UIFont boldFont18];
    lbl.text = self.personalObj.name;
    lbl.myTop = 0;
    lbl.myLeft = 0;
    lbl.myRight = 0;
    lbl.myHeight = 25;
    [subLayout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    lbl.font = [UIFont font12];
    NSString *string = self.personalObj.introduction;
    if ([StringUtil isEmpty:string]) {
        string = @"点击添加个人介绍，更好的展示自己…";
    }
    lbl.text = string;
    lbl.numberOfLines = 2;
    lbl.myTop = 5;
    lbl.myLeft = 0;
    lbl.myRight = 0;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH*0.75 - imgView.width - 65, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    [subLayout addSubview:lbl];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 20;
    layout.myLeft = 25;
    layout.myRight = 25;
    layout.myBottom = 30;
    layout.gravity = MyGravity_Horz_Fill;
    layout.myHeight = MyLayoutSize.wrap;
    [baseLayout addSubview:layout];
    
    UILabel *fansLbl = [self.class factoryLbl:self.personalObj.fansNum describe:@"粉丝"];
    [fansLbl addAction:^(UIView *view) {
        [self dismissViewControllerAnimated:YES completion:^{
            view.userInteractionEnabled = NO;
            MXRoute(@"MyFansListVC", nil)
            view.userInteractionEnabled = YES;
        }];
    }];
    UILabel *followLbl = [self.class factoryLbl:self.personalObj.followNum describe:@"关注"];
    [followLbl addAction:^(UIView *view) {
        [self dismissViewControllerAnimated:YES completion:^{
            view.userInteractionEnabled = NO;
            MXRoute(@"MyFollowListVC", nil)
            view.userInteractionEnabled = YES;
        }];
    }];
    UILabel *liveLbl = [self.class factoryLbl:self.personalObj.liveNum describe:@"直播总数"];
    [layout addSubview:fansLbl];
    [layout addSubview:followLbl];
    [layout addSubview:liveLbl];
    
    SPButton *btn = [SPButton new];
    btn.imagePosition = SPButtonImagePositionLeft;
    btn.imageTitleSpace = 10;
    btn.titleLabel.font = [UIFont font14];
    [btn setTitleColor:[UIColor colorWithHexString:@"#151419"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"申请成为主播"] forState:UIControlStateNormal];
    [btn setTitle:@"申请成为主播" forState:UIControlStateNormal];
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
    [btn setViewCornerRadius:2];
    [btn addAction:^(UIButton *btn) {
        [self dismissViewControllerAnimated:YES completion:^{
            MXRoute(@"ApplyAnchorVC", nil);
        }];
    }];
    btn.myLeft = 25;
    btn.myRight = 25;
    btn.myHeight = 42;
    btn.myBottom = 40;
    [baseLayout addSubview:btn];

    if (self.personalObj.isAnchor) {
        btn.visibility = MyVisibility_Gone;
    } else {
        btn.visibility = MyVisibility_Visible;
    }
    
    [baseLayout layoutIfNeeded];
    return baseLayout;
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = [self headerView];
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [MyListCell viewHeight];
        _tableView.sectionFooterHeight = 0.01;
        Class cls = [MyListCell class];
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (NSArray *)dataSourceArr {
    if (!_dataSourceArr) {
        NSDictionary *apply = @{@"title":@"申请开播", @"leftImg":@"申请开播", @"rightImg":@"Arrow",@"openUrl":@"ApplyShowVC"};
        if (self.personalObj.isAnchor) {
            apply = @{@"title":@"申请开播", @"leftImg":@"申请开播_Sel", @"rightImg":@"Arrow",@"openUrl":@"ApplyShowVC"};
        }
        NSDictionary *launch = @{@"title":@"发起直播", @"leftImg":@"发起直播", @"rightImg":@"Arrow",@"openUrl":@"StartLiveVC"};
        if (self.personalObj.isAnchor) {
            launch = @{@"title":@"发起直播", @"leftImg":@"发起直播_Sel", @"rightImg":@"Arrow",@"openUrl":@"StartLiveVC"};
        }
        NSDictionary *reward = @{@"title":@"打赏记录", @"leftImg":@"打赏记录_Sel", @"rightImg":@"Arrow",@"openUrl":@"MySendGiftVC"};
        NSDictionary *gift = @{@"title":@"我的礼物", @"leftImg":@"我的礼物", @"rightImg":@"Arrow",@"openUrl":@"MyReceivedGiftVC"};
        if (self.personalObj.isAnchor) {
            gift = @{@"title":@"我的礼物", @"leftImg":@"我的礼物_Sel", @"rightImg":@"Arrow",@"openUrl":@"MyReceivedGiftVC"};
        }
        _dataSourceArr = @[apply, launch, reward, gift];
    }
    return _dataSourceArr;
}

+ (UILabel *)factoryLbl:(NSInteger)number describe:(NSString *)describe {
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 2;
    NSArray *txtArr = @[StrF(@"%zd\n", number), describe];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#151419"], [UIColor colorWithHexString:@"#7A7A7A"]];
    NSArray *fontArr = @[[UIFont boldFont13], [UIFont font12]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 3; // 调整行间距
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
    [lbl autoMyLayoutSize];
    return lbl;
}

@end
