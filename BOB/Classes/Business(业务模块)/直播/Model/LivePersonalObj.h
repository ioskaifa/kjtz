//
//  LivePersonalObj.h
//  BOB
//
//  Created by colin on 2020/11/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface LivePersonalObj : BaseObject
///直播ID
@property (nonatomic, strong) NSString *ID;
///主播头像
@property (nonatomic, strong) NSString *photo;
///主播名称
@property (nonatomic, strong) NSString *name;
///主播等级
@property (nonatomic, strong) NSString *level;
///个性签名
@property (nonatomic, strong) NSString *introduction;
///粉丝数
@property (nonatomic, assign) NSInteger fansNum;
///关注数
@property (nonatomic, assign) NSInteger followNum;
///直播总数
@property (nonatomic, assign) NSInteger liveNum;
///是否为主播
@property (nonatomic, assign) BOOL isAnchor;
///直播标题
@property (nonatomic, copy) NSString *liveTitle;

@end

NS_ASSUME_NONNULL_END
