//
//  ApplyLiveTimeListObj.m
//  BOB
//
//  Created by colin on 2020/11/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ApplyLiveTimeListObj.h"

@implementation ApplyLiveTimeListObj

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"ID":@"id"};
}

- (NSString *)formatStatus {
    if ([@"00" isEqualToString:self.status]) {
        return @"待审核";
    } else if ([@"08" isEqualToString:self.status]) {
        return @"审核失败";
    } else if ([@"09" isEqualToString:self.status]) {
        return @"审核成功";
    } else {
        return @"";
    }
}

- (NSString *)formateDate {
    NSString *date = [StringUtil formatDayString:self.cre_date unit:@"."];
    NSString *time = [StringUtil formatTimeString:self.cre_time];
    return StrF(@"%@ %@", date, time);
}

@end
