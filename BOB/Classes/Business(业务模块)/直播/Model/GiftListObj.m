//
//  GiftListObj.m
//  BOB
//
//  Created by colin on 2020/11/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GiftListObj.h"

@implementation GiftListObj

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"ID":@"id",
//             @"price":@"gift_sla_num",
//             @"name":@"gift_name",
//             @"photo":@"gift_show",
    };
}

- (NSString *)gift_show {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _gift_show);
}

+ (NSArray *)giftListObj {
    GiftListObj *obj = [GiftListObj new];
    obj.gift_name = @"香吻";
    obj.gift_sla_num = 1;
    
    GiftListObj *obj1 = [GiftListObj new];
    obj1.gift_name = @"香吻";
    obj1.gift_sla_num = 1;
    
    GiftListObj *obj2 = [GiftListObj new];
    obj2.gift_name = @"香吻";
    obj2.gift_sla_num = 1;
    
    GiftListObj *obj3 = [GiftListObj new];
    obj3.gift_name = @"香吻";
    obj3.gift_sla_num = 1;
    
    GiftListObj *obj4 = [GiftListObj new];
    obj4.gift_name = @"香吻";
    obj4.gift_sla_num = 1;
    
    GiftListObj *obj5 = [GiftListObj new];
    obj5.gift_name = @"香吻";
    obj5.gift_sla_num = 1;
    
    GiftListObj *obj6 = [GiftListObj new];
    obj6.gift_name = @"香吻";
    obj6.gift_sla_num = 1;
    
    GiftListObj *obj7 = [GiftListObj new];
    obj7.gift_name = @"香吻";
    obj7.gift_sla_num = 1;
    
    GiftListObj *obj8 = [GiftListObj new];
    obj8.gift_name = @"香吻";
    obj8.gift_sla_num = 1;
    
    GiftListObj *obj9 = [GiftListObj new];
    obj9.gift_name = @"香吻";
    obj9.gift_sla_num = 1;
    
    return @[obj, obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9];
}

@end
