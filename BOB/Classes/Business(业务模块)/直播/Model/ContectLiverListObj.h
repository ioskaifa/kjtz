//
//  ContectLiverListObj.h
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContectLiverListObj : BaseObject

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *photo;
///粉丝数
@property (nonatomic, copy) NSString *fansNum;

+ (NSArray *)contectLiverListObj;

@end

NS_ASSUME_NONNULL_END
