//
//  ContectLiverListObj.m
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ContectLiverListObj.h"

@implementation ContectLiverListObj

- (NSString *)photo {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _photo);
}

+ (NSArray *)contectLiverListObj {
    ContectLiverListObj *obj = [ContectLiverListObj new];
    obj.name = @"周杰伦";
    obj.fansNum = @"100000000";
    
    ContectLiverListObj *obj1 = [ContectLiverListObj new];
    obj1.name = @"周杰伦1";
    obj1.fansNum = @"200000000";
    
    ContectLiverListObj *obj2 = [ContectLiverListObj new];
    obj2.name = @"周杰伦2";
    obj2.fansNum = @"300000000";
    
    return @[obj, obj1, obj2];
}

@end
