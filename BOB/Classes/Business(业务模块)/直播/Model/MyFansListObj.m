//
//  MyFansListObj.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyFansListObj.h"

@implementation MyFansListObj

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"ID":@"user_id",
             @"photo":@"head_photo",
             @"name":@"nick_name",
    };
}

- (NSString *)photo {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _photo);
}

@end
