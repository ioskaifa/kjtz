//
//  MyReceivedGiftObj.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyReceivedGiftObj.h"

@implementation MyReceivedGiftObj

- (NSString *)giftImg {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _giftImg);
}

- (NSString *)senderName {
    return StrF(@"来自'%@'", _senderName);
}

- (NSString *)formatGiftPrice {
    return StrF(@"金额：%0.2f", self.giftPrice);;
}

- (NSString *)describeString {
    return StrF(@"礼物：%@x%@", self.giftName, self.giftNum);
}

- (NSString *)time {
    return StrF(@"时间：%@", _time);
}

+ (NSArray *)myReceivedGiftObj {
    MyReceivedGiftObj *obj = [MyReceivedGiftObj new];
    obj.senderName = @"周杰伦";
    obj.time = @"2020年11月14日15:49:17";
    obj.giftNum = @"10";
    obj.giftName = @"火箭";
    
    MyReceivedGiftObj *obj1 = [MyReceivedGiftObj new];
    obj1.senderName = @"周杰伦1";
    obj1.time = @"2020年11月14日15:49:17";
    obj1.giftNum = @"10";
    obj1.giftName = @"火箭";
    
    MyReceivedGiftObj *obj2 = [MyReceivedGiftObj new];
    obj2.senderName = @"周杰伦2";
    obj2.time = @"2020年11月14日15:49:17";
    obj2.giftNum = @"10";
    obj2.giftName = @"火箭";
    
    
    return @[obj, obj1, obj2];
}

@end
