//
//  OnLineViewerObj.m
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "OnLineViewerObj.h"

@implementation OnLineViewerObj

- (NSString *)photo {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _photo);
}

+ (NSArray *)onLineViewerObj {
    OnLineViewerObj *obj = [OnLineViewerObj new];
    obj.name = @"飞进我房间二维if";
    obj.amount = 9877;
    
    OnLineViewerObj *obj1 = [OnLineViewerObj new];
    obj1.name = @"飞进我房间二维if蜂王浆偶就发我发文件诶哦附加额外氛围分未付金额WiFi额外缴费文件费文件费欧文范围文件费蜂王浆哦";
    obj1.amount = 987;
    
    OnLineViewerObj *obj2 = [OnLineViewerObj new];
    obj2.name = @"11111飞进我房间二维if";
    obj2.amount = 98;
    
    return @[obj, obj1, obj2];
}

@end
