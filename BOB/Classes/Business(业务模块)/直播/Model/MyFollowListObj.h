//
//  MyFollowListObj.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyFollowListObj : BaseObject

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *photo;
//关注状态 已关注03  未关注04
@property (nonatomic, copy) NSString *status;

+ (NSArray *)myFollowListObj;

@end

NS_ASSUME_NONNULL_END
