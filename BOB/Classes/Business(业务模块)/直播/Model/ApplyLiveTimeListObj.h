//
//  ApplyLiveTimeListObj.h
//  BOB
//
//  Created by colin on 2020/11/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApplyLiveTimeListObj : BaseObject

@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , assign) NSInteger              user_id;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , copy) NSString              *ID;
@property (nonatomic , copy) NSString              *time;
@property (nonatomic , copy) NSString              * update_by;
@property (nonatomic , copy) NSString              * status;

@property (nonatomic , copy) NSString              *formateDate;
@property (nonatomic , copy) NSString              *formatStatus;

@end

NS_ASSUME_NONNULL_END
