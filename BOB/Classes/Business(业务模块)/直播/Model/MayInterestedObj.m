//
//  MayInterestedObj.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MayInterestedObj.h"

@implementation MayInterestedObj

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"ID":@"id",
             @"photo":@"head_photo",
             @"name":@"nick_name",
    };
}

- (NSString *)photo {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _photo);
}

+ (NSArray *)mayInterestedObj {
    MayInterestedObj *obj = [MayInterestedObj new];
    obj.name = @"LolaRose";
    
    MayInterestedObj *obj1 = [MayInterestedObj new];
    obj1.name = @"LolaRoseLolaRoseLolaRoseLolaRoseLolaRose";
    
    MayInterestedObj *obj2 = [MayInterestedObj new];
    obj2.name = @"LolaRoseLolaRoseLolaRoseLolaRoseLolaRose";
    
    MayInterestedObj *obj3 = [MayInterestedObj new];
    obj3.name = @"LolaRoseLolaRoseLolaRoseLolaRoseLolaRose";
    
    return @[obj, obj1, obj2, obj3];
}

@end
