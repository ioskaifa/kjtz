//
//  MyFansListObj.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyFansListObj : BaseObject

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *live_id;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *photo;
//关注状态 互相关注01  单关注02
@property (nonatomic, copy) NSString *status;

@end

NS_ASSUME_NONNULL_END
