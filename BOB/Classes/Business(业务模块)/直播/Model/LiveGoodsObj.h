//
//  LiveGoodsObj.h
//  BOB
//
//  Created by colin on 2020/11/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveGoodsObj : BaseObject

@property (nonatomic , copy) NSString              * goods_navigation;
///商品状态 00-待上架 08-已下架 09-已上架
@property (nonatomic , copy) NSString              * goods_status;
@property (nonatomic , copy) NSString              * goods_name;
@property (nonatomic , assign) NSInteger              goods_sales_num;
@property (nonatomic , copy) NSString              * goods_describe;
@property (nonatomic , assign) NSInteger              goods_stock_num;
///删除状态 0-否  1-是
@property (nonatomic , copy) NSString              * del_status;
@property (nonatomic , copy) NSString              * remark;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , assign) CGFloat              goods_market_cash;
@property (nonatomic , assign) CGFloat              express_cash;
///展示价格
@property (nonatomic , assign) CGFloat              cash_min;
@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , copy) NSString              *ID;
@property (nonatomic , copy) NSString              *order_num;
@property (nonatomic , copy) NSString              * goods_show;

@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isExplain;

@end

NS_ASSUME_NONNULL_END
