//
//  MyReceivedGiftObj.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyReceivedGiftObj : BaseObject

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *giftName;

@property (nonatomic, copy) NSString *giftImg;

@property (nonatomic, copy) NSString *giftNum;

@property (nonatomic, assign) CGFloat giftPrice;

@property (nonatomic, copy) NSString *formatGiftPrice;

@property (nonatomic, copy) NSString *time;

@property (nonatomic, copy) NSString *senderName;

@property (nonatomic, copy) NSString *describeString;

+ (NSArray *)myReceivedGiftObj;

@end

NS_ASSUME_NONNULL_END
