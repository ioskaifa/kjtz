//
//  MayInterestedObj.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MayInterestedObj : BaseObject

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *photo;
//关注状态 YES已关注 NO未关注
@property (nonatomic, assign) BOOL isFollow;

+ (NSArray *)mayInterestedObj;

@end

NS_ASSUME_NONNULL_END
