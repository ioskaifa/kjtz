//
//  MySendGiftObj.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MySendGiftObj : BaseObject

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *giftName;

@property (nonatomic, copy) NSString *giftImg;

@property (nonatomic, copy) NSString *giftNum;

@property (nonatomic, assign) CGFloat giftPrice;

@property (nonatomic, copy) NSString *time;

@property (nonatomic, copy) NSString *senderName;

@end

NS_ASSUME_NONNULL_END
