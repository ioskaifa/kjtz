//
//  MyFollowListObj.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyFollowListObj.h"

@implementation MyFollowListObj

- (instancetype)init {
    self = [super init];
    if (self) {
        self.status = @"03";
    }
    return self;
}

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"ID":@"id",
             @"photo":@"head_photo",
             @"name":@"nick_name",
    };
}

- (NSString *)photo {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _photo);
}

+ (NSArray *)myFollowListObj {
    MyFollowListObj *obj = [MyFollowListObj new];
    obj.name = @"lisa";
    obj.status = @"01";
    
    MyFollowListObj *obj1 = [MyFollowListObj new];
    obj1.name = @"lisa1";
    obj1.status = @"02";
    
    MyFollowListObj *obj2 = [MyFollowListObj new];
    obj2.name = @"lisa2";
    obj2.status = @"01";
    
    return @[obj, obj1, obj2];
}

@end
