//
//  LiverDetailObj.h
//  BOB
//
//  Created by colin on 2020/11/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiverDetailObj : BaseObject
///直播时长
@property (nonatomic, copy) NSString *duration;
///直播时间
@property (nonatomic, copy) NSString *liveTime;
///直播开始
@property (nonatomic, copy) NSString *beginTime;
///直播结束
@property (nonatomic, copy) NSString *endTime;
///观看人数
@property (nonatomic, copy) NSString *watchNum;
///点赞次数
@property (nonatomic, copy) NSString *thumbsNum;
///新增粉丝数
@property (nonatomic, copy) NSString *addFans;
///订单数
@property (nonatomic, copy) NSString *orderNum;
///支付订单数
@property (nonatomic, copy) NSString *payOrderNum;
///订单总额
@property (nonatomic, assign) double orderAmout;
///累计观看
@property (nonatomic, copy) NSString *totalWatch;
///直播间分享
@property (nonatomic, copy) NSString *shareNum;
///支付金额
@property (nonatomic, assign) double payAmout;
///观看用户下单率
@property (nonatomic, assign) CGFloat viewerRate;
///观看次数下单率
@property (nonatomic, assign) CGFloat timesRate;

@end

NS_ASSUME_NONNULL_END
