//
//  GiftListObj.h
//  BOB
//
//  Created by colin on 2020/11/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface GiftListObj : BaseObject

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *gift_name;

@property (nonatomic, copy) NSString *gift_show;
///单价
@property (nonatomic, assign) CGFloat gift_sla_num;

///购买数量
@property (nonatomic, assign) NSInteger gift_buy_num;

///礼物排序
@property (nonatomic, copy) NSString *order_num;
///是否选中 - 单选
@property (nonatomic, assign) BOOL isSelected;
///主播名称
@property (nonatomic, copy) NSString *liverName;

+ (NSArray *)giftListObj;

@end

NS_ASSUME_NONNULL_END
