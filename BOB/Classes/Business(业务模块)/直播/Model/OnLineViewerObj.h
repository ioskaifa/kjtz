//
//  OnLineViewerObj.h
//  BOB
//  在线观众
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface OnLineViewerObj : BaseObject

@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *photo;

@property (nonatomic, assign) CGFloat amount;

@property (nonatomic, assign) NSInteger index;

+ (NSArray *)onLineViewerObj;

@end

NS_ASSUME_NONNULL_END
