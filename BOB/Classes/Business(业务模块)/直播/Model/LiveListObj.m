//
//  LiveListObj.m
//  BOB
//
//  Created by mac on 2020/10/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveListObj.h"

@implementation LiveListObj

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"ID":@"id",
             @"cover_photo":@"img",
             @"head_photo":@"photo",
             @"room_no":@"roomID",
    };
}

- (NSString *)formatWatch {
    if (!_formatWatch) {
        NSString *string = StrF(@"%zd观看", self.watchNum);
        _formatWatch = string;
    }
    return _formatWatch;
}

- (NSString *)formatThumbs {
    if (!_formatThumbs) {
        NSString *string = StrF(@"%0.2fK", self.thumbsNum%1000*0.1);
        _formatThumbs = string;
    }
    return _formatThumbs;
}

+ (NSArray *)liveListObj {
    LiveListObj *obj = [LiveListObj new];
    obj.thumbsNum = 300000;
    obj.watchNum = 1200;
    obj.title = @"分为肌肤微积分为分为非未我";
    obj.name = @"直播中";
    
    LiveListObj *obj1 = [LiveListObj new];
    obj1.thumbsNum = 300001;
    obj1.watchNum = 1201;
    obj1.title = @"分为肌肤微积分为分为非未我1";
    obj1.name = @"直播中1";
    
    LiveListObj *obj2 = [LiveListObj new];
    obj2.thumbsNum = 300002;
    obj2.watchNum = 1202;
    obj2.title = @"分为肌肤微积分为分为非未我2";
    obj2.name = @"直播中2";
    
    LiveListObj *obj3 = [LiveListObj new];
    obj3.thumbsNum = 300003;
    obj3.watchNum = 1203;
    obj3.title = @"分为肌肤微积分为分为非未我3";
    obj3.name = @"直播中3";
    
    LiveListObj *obj4 = [LiveListObj new];
    obj4.thumbsNum = 300004;
    obj4.watchNum = 1204;
    obj4.title = @"分为肌肤微积分为分为非未我4";
    obj4.name = @"直播中4";
    
    return @[obj, obj1, obj2, obj3, obj4];
}

@end
