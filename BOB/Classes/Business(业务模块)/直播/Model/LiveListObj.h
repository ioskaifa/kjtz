//
//  LiveListObj.h
//  BOB
//
//  Created by mac on 2020/10/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveListObj : BaseObject

@property (nonatomic, copy) NSString *ID;
///直播封面
@property (nonatomic, copy) NSString *img;
///观看人数
@property (nonatomic, assign) NSInteger watchNum;
@property (nonatomic, copy) NSString *formatWatch;
///点赞次数
@property (nonatomic, assign) NSInteger thumbsNum;
@property (nonatomic, copy) NSString *formatThumbs;
///直播标题
@property (nonatomic, copy) NSString *title;
///主播名称
@property (nonatomic, copy) NSString *name;
///主播头像
@property (nonatomic, copy) NSString *photo;
///房间号
@property (nonatomic, copy) NSString *roomID;
///开播状态 0-已关播 1-正在直播
@property (nonatomic, assign) BOOL status;

+ (NSArray *)liveListObj;

@end

NS_ASSUME_NONNULL_END
