//
//  LiveGoodsObj.m
//  BOB
//
//  Created by colin on 2020/11/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveGoodsObj.h"

@implementation LiveGoodsObj

+ (nullable NSDictionary<NSString *, id> *)modelCustomPropertyMapper {
    return @{@"ID":@"id"};
}

- (NSString *)goods_show {
    if([_goods_show hasPrefix:@"http:"]) {
        return _goods_show;
    }
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _goods_show);
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    self.isExplain = isSelected;
}

@end
