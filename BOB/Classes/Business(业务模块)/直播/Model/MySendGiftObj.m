//
//  MySendGiftObj.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MySendGiftObj.h"

@implementation MySendGiftObj

- (NSString *)giftImg {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _giftImg);
}

- (NSString *)senderName {
    return StrF(@"打赏给'%@'", _senderName);
}

- (NSString *)formatGiftPrice {
    return StrF(@"金额：%0.2f", self.giftPrice);;
}

- (NSString *)describeString {
    return StrF(@"礼物：%@x%@", self.giftName, self.giftNum);
}

- (NSString *)time {
    return StrF(@"时间：%@", _time);
}

@end
