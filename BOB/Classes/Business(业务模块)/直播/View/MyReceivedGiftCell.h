//
//  MyReceivedGiftCell.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "STDTableViewCell.h"
#import "MyReceivedGiftObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyReceivedGiftCell : STDTableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(MyReceivedGiftObj *)obj;

@end

NS_ASSUME_NONNULL_END
