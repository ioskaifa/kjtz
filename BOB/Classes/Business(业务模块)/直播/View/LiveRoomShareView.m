//
//  LiveRoomShareView.m
//  BOB
//
//  Created by colin on 2020/11/24.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveRoomShareView.h"

@implementation LiveRoomShareView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 260;
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [baseLayout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font18];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"分享到";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 15;
    lbl.myBottom = 15;
    lbl.myCenterX = 0;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:[UIView fullLine]];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.gravity = MyGravity_Horz_Among;
    layout.weight = 1;
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    [baseLayout addSubview:layout];
    SPButton *btn = [self.class factoryBtnWithTitle:@"微信" withImg:@"weixin"];
    [layout addSubview:btn];
    
    btn = [self.class factoryBtnWithTitle:@"朋友圈" withImg:@"pengyouquan"];
    [layout addSubview:btn];
    
    UIView *view = [UIView gapLine];
    [baseLayout addSubview:view];
    lbl = [UILabel new];
    lbl.font = [UIFont boldFont17];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"举报";
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myTop = 10;
    lbl.myBottom = 10 + safeAreaInsetBottom();
    self.reportLbl = lbl;    
    [baseLayout addSubview:lbl];
    
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.closeImgView.size);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

+ (SPButton *)factoryBtnWithTitle:(NSString *)title withImg:(NSString *)img {
    SPButton *btn = [SPButton new];
    btn.imagePosition = SPButtonImagePositionTop;
    btn.imageTitleSpace = 10;
    btn.titleLabel.font = [UIFont font12];
    [btn setTitleColor:[UIColor colorWithHexString:@"#AAAABB"] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#AAAABB"] forState:UIControlStateSelected];
    [btn setTitleColor:[UIColor colorWithHexString:@"#AAAABB"] forState:UIControlStateHighlighted];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.mySize = btn.size;
    btn.myCenterY = 0;
    return btn;
}

@end
