//
//  MayInterestedCell.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MayInterestedObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MayInterestedCell : UICollectionViewCell

+ (CGSize)itemSize;

- (void)configureView:(MayInterestedObj *)obj;

@end

NS_ASSUME_NONNULL_END
