//
//  LiveListCell.m
//  BOB
//
//  Created by mac on 2020/10/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveListCell.h"
#import "NSString+NumFormat.h"

@interface LiveListCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UIImageView *headImgView;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) SPButton *thumbsBtn;

@property (nonatomic, strong) SPButton *watchBtn;

@property (nonatomic, strong) UILabel *followLbl;

@end

@implementation LiveListCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 295;
}

- (void)setCellType:(LiveListCellType)cellType {
    _cellType = cellType;
    if (cellType == LiveListCellTypeFllowed) {
        self.followLbl.visibility = MyVisibility_Visible;
        self.thumbsBtn.visibility = MyVisibility_Gone;
    } else {
        self.followLbl.visibility = MyVisibility_Gone;
        self.thumbsBtn.visibility = MyVisibility_Visible;
    }
}

- (void)configureView:(LiveListObj *)obj {
    if (![obj isKindOfClass:LiveListObj.class]) {
        return;
    }
    NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, obj.img);
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, obj.photo);
    [self.headImgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    self.titleLbl.text = obj.title;
    
    self.nameLbl.text = obj.name;
    [self.nameLbl sizeToFit];
    self.nameLbl.mySize = self.nameLbl.size;
    
    [self.thumbsBtn setTitle:obj.formatThumbs forState:UIControlStateNormal];
    [self.thumbsBtn sizeToFit];
    self.thumbsBtn.mySize = self.thumbsBtn.size;
    
    [self.watchBtn setTitle:obj.formatWatch forState:UIControlStateNormal];
    [self.watchBtn sizeToFit];
    self.watchBtn.mySize = CGSizeMake(self.watchBtn.width + 10, self.watchBtn.height + 10);
}

- (void)configureViewEx:(TCRoomInfo *)obj {
    if (![obj isKindOfClass:TCRoomInfo.class]) {
        return;
    }
    NSString *imgUrl = obj.userinfo.frontcover;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
    }];
    
    imgUrl = obj.userinfo.headpic;
    [self.headImgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    self.titleLbl.text = obj.title?:@"";
    
    self.nameLbl.text = obj.userinfo.username;
    [self.nameLbl sizeToFit];
    self.nameLbl.mySize = self.nameLbl.size;
    
    [self.thumbsBtn setTitle:[NSString getFormatBrowseCount:obj.likecount] forState:UIControlStateNormal];
    [self.thumbsBtn sizeToFit];
    self.thumbsBtn.mySize = self.thumbsBtn.size;

    [self.watchBtn setTitle:StrF(@"%zd观看", obj.viewercount) forState:UIControlStateNormal];
    [self.watchBtn sizeToFit];
    self.watchBtn.mySize = CGSizeMake(self.watchBtn.width + 10, self.watchBtn.height + 10);
}

#pragma mark - InitUI
- (void)createUI {
    MyFrameLayout *baseLayout = [MyFrameLayout new];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.contentView addSubview:baseLayout];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.imgView];
    [layout addSubview:self.titleLbl];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myLeft = 10;
    subLayout.myRight = 10;
    subLayout.myHeight = MyLayoutSize.wrap;
    subLayout.myBottom = 10;
    [layout addSubview:subLayout];
    
    [subLayout addSubview:self.headImgView];
    [subLayout addSubview:self.nameLbl];
    [subLayout addSubview:[UIView placeholderHorzView]];
    [subLayout addSubview:self.thumbsBtn];
    [subLayout addSubview:self.followLbl];
    
    [baseLayout addSubview:self.watchBtn];
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.contentMode = UIViewContentModeScaleAspectFill;
            object.backgroundColor = [UIColor whiteColor];
            object.clipsToBounds = YES;
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.weight = 1;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.backgroundColor = [UIColor whiteColor];
            object.textColor = [UIColor colorWithHexString:@"#151419"];
            object.font = [UIFont boldFont14];
            object.myTop = 5;
            object.myLeft = 10;
            object.myRight = 10;
            object.myHeight = 20;
            object.myBottom = 10;
            object;
        });
    }
    return _titleLbl;
}

- (UIImageView *)headImgView {
    if (!_headImgView) {
        _headImgView = ({
            UIImageView *object = [UIImageView new];
//            object.contentMode = UIViewContentModeScaleAspectFill;
            object.size = CGSizeMake(20, 20);
            [object setViewCornerRadius:object.height/2.0];
            object.mySize = object.size;
            object.backgroundColor = [UIColor moBackground];
            object.myCenterY = 0;
            object.myLeft = 0;
            object.myRight = 10;
            object;
        });
    }
    return _headImgView;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.backgroundColor = [UIColor whiteColor];
            object.textColor = [UIColor colorWithHexString:@"#303033"];
            object.font = [UIFont font12];
            object.myCenterY = 0;
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)followLbl {
    if (!_followLbl) {
        _followLbl = ({
            UILabel *object = [UILabel new];
            object.backgroundColor = [UIColor colorWithHexString:@"#00DBD9"];
            object.textColor = [UIColor whiteColor];
            object.font = [UIFont systemFontOfSize:10];
            object.size = CGSizeMake(30, 20);
            [object setViewCornerRadius:2];
            object.textAlignment = NSTextAlignmentCenter;
            object.text = @"关注";
            object.myCenterY = 0;
            object.myRight = 0;
            object;
        });
    }
    return _followLbl;
}

- (SPButton *)thumbsBtn {
    if (!_thumbsBtn) {
        _thumbsBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionLeft;
            object.imageTitleSpace = 5;
            [object setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font11];
            [object setImage:[UIImage imageNamed:@"直播点赞"] forState:UIControlStateNormal];
            [object setTitle:@"21.2K" forState:UIControlStateNormal];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _thumbsBtn;
}

- (SPButton *)watchBtn {
    if (!_watchBtn) {
        _watchBtn = ({
            SPButton *object = [SPButton new];
            [object setViewCornerRadius:2];
            [object setBackgroundColor:[UIColor colorWithHexString:@"#25292E"]];
            object.imagePosition = SPButtonImagePositionLeft;
            object.imageTitleSpace = 5;
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font11];
            [object setImage:[UIImage imageNamed:@"直播中"] forState:UIControlStateNormal];
            object.myTop = 5;
            object.myLeft = 5;
            object;
        });
    }
    return _watchBtn;
}

@end
