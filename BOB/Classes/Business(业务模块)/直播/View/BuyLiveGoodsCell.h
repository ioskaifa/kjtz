//
//  BuyLiveGoodsCell.h
//  BOB
//
//  Created by colin on 2020/11/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuyGoodsListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface BuyLiveGoodsCell : UITableViewCell

@property (nonatomic, strong) UIView *line;

+ (CGFloat)viewHeight;

- (void)configureView:(BuyGoodsListObj *)obj;

@end

NS_ASSUME_NONNULL_END
