//
//  ManageLiveGoodsView.m
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ManageLiveGoodsView.h"
#import "YJSliderView.h"
#import "AddLiveGoodsVC.h"
#import "LiveGoodsListVC.h"

@interface ManageLiveGoodsView ()<YJSliderViewDelegate>

@property (nonatomic, strong) YJSliderView *sliderView;

@property (nonatomic, strong) NSArray *sliderTitleArr;

@property (nonatomic, strong) NSArray *sliderViewArr;

@property (nonatomic, strong) AddLiveGoodsVC *addVC;

@property (nonatomic, strong) LiveGoodsListVC *goodsVC;

@end

@implementation ManageLiveGoodsView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return SCREEN_HEIGHT * 0.75;
}

- (NSInteger)selectCount {
    return self.goodsVC.dataSourceMArr.count;
}

- (LiveGoodsObj *)selectGoodObj {
    return self.addVC.selectedGoods.firstObject;
}

- (NSString *)selectGoodsId {
    NSArray *dataArr = self.goodsVC.dataSourceMArr;
    if (dataArr.count == 0) {
        return @"";
    } else {
        dataArr = [dataArr valueForKeyPath:@"@distinctUnionOfObjects.ID"];
        return [dataArr componentsJoinedByString:@","];
    }
}

#pragma mark YJSliderViewDelegate
- (NSInteger)numberOfItemsInYJSliderView:(YJSliderView *)sliderView {
    return self.sliderTitleArr.count;
}

- (UIView *)yj_SliderView:(YJSliderView *)sliderView viewForItemAtIndex:(NSInteger)index {
    if (index >= self.sliderViewArr.count) {
        return [UIView new];
    }
    return self.sliderViewArr[index];
}

- (NSString *)yj_SliderView:(YJSliderView *)sliderView titleForItemAtIndex:(NSInteger)index {
    if (index >= self.sliderTitleArr.count) {
        return @"";
    }
    return self.sliderTitleArr[index];
}

- (NSInteger)initialzeIndexFoYJSliderView:(YJSliderView *)sliderView {
    return 0;
}

#pragma makr - InitUI
- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.mySize = layout.size;
    [self addSubview:layout];
    [layout addSubview:self.sliderView];
    
    [layout setBorderWithCornerRadius:10
                    byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.top.mas_equalTo(self.mas_top).offset(10);
        make.right.mas_equalTo(self.mas_right).offset(-10);
    }];
}

#pragma mark - Init
- (YJSliderView *)sliderView {
    if (!_sliderView) {
        _sliderView = [[YJSliderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, [self.class viewHeight])];
        _sliderView.themeBGColor = [UIColor whiteColor];
        _sliderView.delegate = self;
        _sliderView.themeColor = [UIColor blackColor];
        _sliderView.defultColor = [UIColor blackColor];
        _sliderView.lineColor = [UIColor blackColor];
        _sliderView.selectFont = [UIFont boldFont18];
        _sliderView.myTop = 0;
        _sliderView.myLeft = 0;
        _sliderView.myRight = 0;
        _sliderView.weight = 1;
    }
    return _sliderView;
}

- (NSArray *)sliderTitleArr {
    if (!_sliderTitleArr) {
        _sliderTitleArr = @[@"橱窗"];
    }
    return _sliderTitleArr;
}

- (NSArray *)sliderViewArr {
    if (!_sliderViewArr) {
        _sliderViewArr = @[self.addVC.view];
    }
    return _sliderViewArr;
}

- (AddLiveGoodsVC *)addVC {
    if (!_addVC) {
        _addVC = [AddLiveGoodsVC new];
        _addVC.isSingleChoice = YES;
        @weakify(self)
        [_addVC.bottomView.buyBtn addAction:^(UIButton *btn) {
            @strongify(self)
            if ([self->_addVC valiParam]) {
                NSArray *goodsArr = [self->_addVC selectedGoods];
                [self.goodsVC configureGoods:goodsArr];
                [self.sliderView configureCurrentInde:1];
            }
        }];
    }
    return _addVC;
}

- (LiveGoodsListVC *)goodsVC {
    if (!_goodsVC) {
        _goodsVC = [LiveGoodsListVC new];
    }
    return _goodsVC;
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

@end
