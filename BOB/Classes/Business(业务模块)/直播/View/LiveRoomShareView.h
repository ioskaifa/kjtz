//
//  LiveRoomShareView.h
//  BOB
//
//  Created by colin on 2020/11/24.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomShareView : UIView

@property (nonatomic, strong) UIImageView *closeImgView;

@property (nonatomic, strong) UILabel *reportLbl;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
