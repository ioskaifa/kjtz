//
//  LiverMoreView.m
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiverMoreView.h"

@implementation LiverMoreView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 220 + safeAreaInsetBottom();
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [baseLayout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font18];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"更多";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 15;
    lbl.myBottom = 15;
    lbl.myCenterX = 0;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:[UIView fullLine]];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myHeight = 80;
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.gravity = MyGravity_Horz_Among;
    [baseLayout addSubview:layout];
    [layout addSubview:self.giftBtn];
    [layout addSubview:self.contentBtn];
    [layout addSubview:self.stopBtn];
    UIButton *btn = [self.class factoryBtn:@"礼物" img:@"主播礼物"];
    btn.visibility = MyVisibility_Invisible;
    [layout addSubview:btn];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myHeight = 80;
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.gravity = MyGravity_Horz_Among;
    [baseLayout addSubview:layout];
    [layout addSubview:self.switchBtn];
    [layout addSubview:self.shareBtn];
    [layout addSubview:self.notiBtn];
    [layout addSubview:self.setBtn];
    
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.closeImgView.size);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

- (SPButton *)giftBtn {
    if (!_giftBtn) {
        _giftBtn = [self.class factoryBtn:@"礼物" img:@"主播礼物"];
    }
    return _giftBtn;
}

- (SPButton *)contentBtn {
    if (!_contentBtn) {
        _contentBtn = [self.class factoryBtn:@"连麦" img:@"连麦_black"];
    }
    return _contentBtn;
}

- (SPButton *)stopBtn {
    if (!_stopBtn) {
        _stopBtn = [self.class factoryBtn:@"暂停直播" img:@"暂停直播"];
    }
    return _stopBtn;
}

- (SPButton *)switchBtn {
    if (!_switchBtn) {
        _switchBtn = [self.class factoryBtn:@"镜头切换" img:@"翻转_black"];
    }
    return _switchBtn;
}

- (SPButton *)shareBtn {
    if (!_shareBtn) {
        _shareBtn = [self.class factoryBtn:@"分享" img:@"直播间分享"];
    }
    return _shareBtn;
}

- (SPButton *)notiBtn {
    if (!_notiBtn) {
        _notiBtn = [self.class factoryBtn:@"通知粉丝" img:@"通知粉丝"];
        [_notiBtn setImage:[UIImage imageNamed:@"通知粉丝_Sel"] forState:UIControlStateSelected];
    }
    return _notiBtn;
}

- (SPButton *)setBtn {
    if (!_setBtn) {
        _setBtn = [self.class factoryBtn:@"直播设置" img:@"直播设置"];
    }
    return _setBtn;
}

+ (SPButton *)factoryBtn:(NSString *)title img:(NSString *)img {
    SPButton *btn = [SPButton new];
    btn.imagePosition = SPButtonImagePositionTop;
    btn.imageTitleSpace = 5;
    btn.titleLabel.font = [UIFont font12];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.myCenterY = 0;
    btn.mySize = CGSizeMake(btn.width + 20, btn.height);
    return btn;
}

@end
