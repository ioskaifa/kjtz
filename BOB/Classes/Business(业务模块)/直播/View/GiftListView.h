//
//  GiftListView.h
//  BOB
//
//  Created by colin on 2020/11/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiftListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface GiftListView : UIView

@property (nonatomic, strong) UIImageView *closeImgView;
///打赏
@property (nonatomic, strong) UILabel *commitLbl;
///选中的礼物
@property (nonatomic, strong) GiftListObj *selectGiftObj;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
