//
//  ManageLiveGoodsView.h
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveGoodsObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface ManageLiveGoodsView : UIView

@property (nonatomic, assign) NSInteger selectCount;

@property (nonatomic, copy) NSString *selectGoodsId;
///当前讲解的商品
@property (nonatomic, strong) LiveGoodsObj *selectGoodObj;

@property (nonatomic, strong) UIImageView *closeImgView;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
