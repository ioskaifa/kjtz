//
//  GiftListView.m
//  BOB
//
//  Created by colin on 2020/11/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GiftListView.h"
#import "GiftListCollectionView.h"

@interface GiftListView () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) NSMutableArray *dataMArr;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UICollectionView *colView;

@end

@implementation GiftListView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
        [self fetchData];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return [GiftListCollectionView viewHeight] + 100 + safeAreaInsetBottom();
}

- (void)fetchData {
    NSString *url = @"api/live/userLiveGift/getLiveGiftList";
    url = StrF(@"%@/%@", LcwlServerRoot, url);
    NSDictionary *param = @{};
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url);
        net.params(param);
        net.finish(^(id data){
            NSDictionary *dataDic = (NSDictionary *)data[@"data"];
            if ([data isSuccess]) {
                NSArray *dataArr = [GiftListObj modelListParseWithArray:dataDic[@"liveGiftList"]];
                [dataArr sortedArrayUsingComparator:^NSComparisonResult(GiftListObj *obj1, GiftListObj *obj2) {
                    return [obj1.order_num compare:obj2.order_num] == NSOrderedDescending;
                }];
                [self.dataMArr addObjectsFromArray:dataArr];
                [self configureDataSourceMArr];   
                [self.colView reloadData];
            } else {
                [NotifyHelper showMessageWithMakeText:dataDic[@"msg"]];
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

- (GiftListObj *)selectGiftObj {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected == YES"];
    NSArray *tempArr = [self.dataMArr filteredArrayUsingPredicate:predicate];
    return tempArr.firstObject;
}

- (void)configureDataSourceMArr {
    [self.dataSourceMArr removeAllObjects];
    NSMutableArray *tempMArr;
    for (NSInteger i = 0; i < self.dataMArr.count; i++) {
        if (i % 8 == 0) {
            if (tempMArr) {
                [self.dataSourceMArr addObject:tempMArr];
            }
            tempMArr = [NSMutableArray arrayWithCapacity:8];
        }
        [tempMArr addObject:self.dataMArr[i]];
    }
    if (tempMArr.count > 0) {
        [self.dataSourceMArr addObject:tempMArr];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = UICollectionViewCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    GiftListCollectionView *view = [cell.contentView viewWithTag:1000];
    if (!view) {
        view = [GiftListCollectionView new];
        view.tag = 1000;
        
        [cell.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(cell.contentView);
        }];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item >= self.dataSourceMArr.count) {
        return;
    }
    NSArray *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:NSArray.class]) {
        return;
    }
    GiftListCollectionView *view = [cell.contentView viewWithTag:1000];
    if (![view isKindOfClass:GiftListCollectionView.class]) {
        return;
    }
    
    [view configureView:obj withAllData:self.dataMArr];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    
}

#pragma mark - InitUI
- (void)createUI {
    MyLinearLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [baseLayout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font18];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"打赏";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 15;
    lbl.myBottom = 15;
    lbl.myCenterX = 0;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:[UIView fullLine]];
    [baseLayout addSubview:self.colView];
    [baseLayout addSubview:[UIView fullLine]];
    UIView *bottomView = [UIView addBottomSafeAreaWithContentView:[self bottomView]];
    [baseLayout addSubview:bottomView];
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.closeImgView.size);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}

#pragma mark - Init
- (UICollectionView *)colView {
    if (!_colView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = CGSizeMake(SCREEN_WIDTH, [GiftListCollectionView viewHeight]);
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _colView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _colView.pagingEnabled = YES;
        _colView.showsVerticalScrollIndicator = NO;
        _colView.showsHorizontalScrollIndicator = NO;
        _colView.backgroundColor = [UIColor whiteColor];
        _colView.delegate = self;
        _colView.dataSource = self;
        Class cls = UICollectionViewCell.class;
        [_colView registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
        _colView.myTop = 0;
        _colView.myRight = 0;
        _colView.myLeft = 0;
        _colView.weight = 1;
    }
    return _colView;
}

- (MyBaseLayout *)bottomView {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    baseLayout.size = CGSizeMake(SCREEN_WIDTH, 50);
    baseLayout.mySize = baseLayout.size;
    
    NSArray *txtArr = @[@"剩余：", StrF(@"%0.4f SLA", UDetail.user.sla_num)];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#AAAABB"], [UIColor blackColor]];
    NSArray *fontArr = @[[UIFont lightFont13], [UIFont fontWithName:@"DINAlternate-Bold" size:13]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    
    UILabel *lbl = [UILabel new];
    lbl.myCenterY = 0;
    lbl.myLeft = 15;
    lbl.weight = 1;
    lbl.myHeight = 25;
    lbl.attributedText = att;
    [baseLayout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.size = CGSizeMake(80, 35);
    [lbl setViewCornerRadius:3];
    lbl.backgroundColor = [UIColor moGreen];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor whiteColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.text = @"打赏";
    lbl.myCenterY = 0;
    lbl.myRight = 15;
    lbl.myLeft = 10;
    lbl.mySize = lbl.size;
    self.commitLbl = lbl;
    [baseLayout addSubview:lbl];
    
    return baseLayout;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (NSMutableArray *)dataMArr {
    if (!_dataMArr) {
        _dataMArr = [NSMutableArray array];
    }
    return _dataMArr;
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

@end
