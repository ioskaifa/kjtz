//
//  GiftListCollectionView.m
//  BOB
//
//  Created by colin on 2020/11/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GiftListCollectionView.h"
#import "GiftListCell.h"

@interface GiftListCollectionView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) NSArray *allDataArr;

@property (nonatomic, strong) NSArray *dataSourceArr;

@property (nonatomic, strong) UICollectionView *colView;

@end

@implementation GiftListCollectionView

+ (CGFloat)viewHeight {
    return [GiftListCell itemSize].height * 2 + 35;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)configureView:(NSArray *)dataArr withAllData:(NSArray *)allData {
    if (![dataArr isKindOfClass:NSArray.class]) {
        return;
    }
    self.dataSourceArr = dataArr;
    [self.colView reloadData];
    self.allDataArr = allData;
}

- (void)configureDidSelectItemByObj:(GiftListObj *)selecObj {
    if (![selecObj isKindOfClass:GiftListObj.class]) {
        return;
    }
    
    for (GiftListObj *obj in self.allDataArr) {
        if (![obj isKindOfClass:GiftListObj.class]) {
            continue;
        }
        if (obj == selecObj) {
            obj.isSelected = YES;
        } else {
            obj.isSelected = NO;
        }
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = GiftListCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item >= self.dataSourceArr.count) {
        return;
    }
    if (![cell isKindOfClass:GiftListCell.class]) {
        return;
    }
    GiftListCell *listCell = (GiftListCell *)cell;
    [listCell configureView:self.dataSourceArr[indexPath.item]];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    GiftListObj *obj = self.dataSourceArr[indexPath.row];
    if (![obj isKindOfClass:GiftListObj.class]) {
        return;
    }
    [self configureDidSelectItemByObj:obj];
    [collectionView reloadData];
}

#pragma mark - InitUI
- (void)createUI {
    MyLinearLayout *layout = [self addMyLinearLayout:MyOrientation_Vert];
    
    [layout addSubview:self.colView];
}

#pragma mark - Init
- (UICollectionView *)colView {
    if (!_colView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = [GiftListCell itemSize];
        layout.minimumInteritemSpacing = 10;
        layout.minimumLineSpacing = 15;
        layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _colView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _colView.showsVerticalScrollIndicator = NO;
        _colView.showsHorizontalScrollIndicator = NO;
        _colView.backgroundColor = [UIColor whiteColor];
        _colView.delegate = self;
        _colView.dataSource = self;
        Class cls = GiftListCell.class;
        [_colView registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
        _colView.weight = 1;
        _colView.myTop = 0;
        _colView.myLeft = 0;
        _colView.myRight = 0;
    }
    return _colView;
}

@end
