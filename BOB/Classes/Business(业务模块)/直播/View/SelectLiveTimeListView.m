//
//  SelectLiveTimeListView.m
//  BOB
//
//  Created by colin on 2020/11/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SelectLiveTimeListView.h"

@interface SelectLiveTimeListView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@end

@implementation SelectLiveTimeListView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
        [self fetchData];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 300;
}

- (void)fetchData {
    NSString *url = @"api/live/userApplyLiveTime/getLiveTimeList";
    url = StrF(@"%@/%@", LcwlServerRoot, url);
    NSDictionary *param = @{};
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url);
        net.params(param);
        net.finish(^(id data){
            if ([data isSuccess]) {
                NSArray *dataArr = data[@"data"][@"liveTimeList"];
                for (NSDictionary *dic in dataArr) {
                    if (dic[@"time"]) {
                        [self.dataSourceMArr addObject:[dic[@"time"] description]];
                    }
                }
                [self.tableView reloadData];
            } else {
                [NotifyHelper showMessageWithMakeText:data[@"msg"]];
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = [UITableViewCell class];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell addBottomSingleLine:[UIColor moBackground]];
    
    UILabel *lbl = [cell.contentView viewWithTag:1001];
    if (!lbl) {
        lbl = [UILabel new];
        lbl.tag = 1001;
        lbl.font = [UIFont font15];
        lbl.textColor = [UIColor blackColor];
        [cell.contentView addSubview:lbl];
        [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(30);
            make.left.mas_equalTo(cell.contentView.mas_left).mas_offset(15);
            make.right.mas_equalTo(cell.contentView.mas_right).mas_offset(-15);
            make.centerY.mas_equalTo(cell.contentView.mas_centerY);
        }];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    UILabel *lbl = [cell.contentView viewWithTag:1001];
    if (lbl) {
        lbl.text = self.dataSourceMArr[indexPath.row];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    [tableView routerEventWithName:@"MXCustomAlertVCconfirm" userInfo:@{@"time":self.dataSourceMArr[indexPath.row]}];
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    [baseLayout setViewCornerRadius:5];
    baseLayout.backgroundColor = [UIColor whiteColor];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"请选择时长(h)";
    [lbl autoMyLayoutSize];
    lbl.myTop = 10;
    lbl.myBottom = 10;
    lbl.myCenterX = 0;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:[UIView fullLine]];
    
    [baseLayout addSubview:self.tableView];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        Class cls = [UITableViewCell class];
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        _tableView.rowHeight = 60;
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

@end
