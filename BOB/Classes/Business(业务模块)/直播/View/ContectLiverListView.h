//
//  ContectLiverListView.h
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContectLiverListView : UIView

@property (nonatomic, strong) UIImageView *closeImgView;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
