//
//  LiveListTopView.h
//  BOB
//
//  Created by mac on 2020/10/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LiveListTopView;

NS_ASSUME_NONNULL_BEGIN

@protocol LiveListTopViewDelegate <NSObject>

@optional
- (void)liveListTopView:(LiveListTopView *)sliderView didSelectItemAtIndex:(NSInteger)index;

@end

@interface LiveListTopView : UIView

@property (nonatomic, weak) id<LiveListTopViewDelegate> delegate;

@property (nonatomic, strong) SPButton *searchBtn;

+ (CGFloat)viewHeight;

- (instancetype)initWithData:(NSArray *)dataArr;

@end

NS_ASSUME_NONNULL_END
