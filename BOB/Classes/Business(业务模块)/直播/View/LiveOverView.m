//
//  LiveOverView.m
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveOverView.h"
#import "TXLiteAVSDKManage.h"
#import "UserApplyAnchorModel.h"

@interface LiveOverView ()

@property(nonatomic,strong) UserApplyAnchorModel *model;

@end

@implementation LiveOverView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[TXLiteAVSDKManage shared] startPreview:self];
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return SCREEN_HEIGHT;
}

- (void)setLiveID:(NSString *)liveID {
    [self fetchData:liveID];
}

- (void)fetchData:(NSString *)liveID {
    
    NSString *url = @"api/live/userLiveProgress/getOverLiveSummary";
    url = StrF(@"%@/%@", LcwlServerRoot, url);
    NSDictionary *param = @{@"live_id":liveID};
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url);
        net.params(param);
        net.finish(^(id data){
            NSDictionary *dataDic = (NSDictionary *)data[@"data"];
            self.model = [UserApplyAnchorModel modelParseWithDict:dataDic];
            if ([data isSuccess]) {
                [self initUI];
            } else {
                [NotifyHelper showMessageWithMakeText:dataDic[@"msg"]];
            }
        }).failure(^(id error){
            [NotifyHelper showMessageWithMakeText:[error description]];
        })
        .execute();
    }];
}

- (void)createUI {
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
    UIVisualEffectView *effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
    effectview.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [self addSubview:effectview];
    
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"icon_public_close"];
    imgView.myLeft = SCREEN_WIDTH - imgView.width - 15;
    imgView.myTop = NavigationBar_Bottom-20;
    [baseLayout addSubview:imgView];
    [imgView addAction:^(UIView *view) {
        [self removeFromSuperview];
        Block_Exec(self.closeBlock,nil);
    }];
}

- (void)initUI {    
    MyBaseLayout *baseLayout = [self viewWithTag:100001];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldSystemFontOfSize:24];
    lbl.textColor = [UIColor whiteColor];
    lbl.text = @"直播已结束";
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myTop = 25;
    
    [baseLayout addSubview:lbl];
    
    lbl = [UILabel new];
    lbl.font = [UIFont font14];
    lbl.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
    lbl.text = StrF(@"直播时长：%@", self.model.liveTimeString);
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myTop = 5;
    
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:[self contentLayout]];
}

- (MyBaseLayout *)contentLayout {
    MyFrameLayout *frameLayout = [MyFrameLayout new];
    frameLayout.size = CGSizeMake(SCREEN_WIDTH - 40, 280);
    frameLayout.mySize = frameLayout.size;
    frameLayout.myCenterX = 0;
    frameLayout.myTop = 60;
    frameLayout.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    frameLayout.layer.cornerRadius = 5;
    
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [baseLayout setViewCornerRadius:5];
    [frameLayout addSubview:baseLayout];
    
    UIImageView *imgView = [UIImageView new];
    imgView.size = CGSizeMake(60, 60);
    imgView.backgroundColor = [UIColor moBackground];
    [imgView setViewCornerRadius:imgView.height/2.0];
    imgView.mySize = imgView.size;
    imgView.myTop = -(imgView.height/2.0);
    imgView.myCenterX = 0;
    imgView.layer.borderWidth = 1;
    imgView.layer.borderColor = [UIColor whiteColor].CGColor;
    [imgView sd_setImageWithURL:[NSURL URLWithString:self.model.head_photo]];
    [frameLayout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor whiteColor];
    lbl.text = self.model.nick_name;
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myTop = imgView.height/2 + 5;
    [baseLayout addSubview:lbl];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 30;
    layout.myLeft = 20;
    layout.myRight = 20;
    layout.myHeight = 50;
    layout.gravity = MyGravity_Fill;
    [baseLayout addSubview:layout];
    //观看人数
    [layout addSubview:[self.class facotoryLbl:toStr(self.model.view_num) key:@"观看人数"]];
    //点赞次数
    [layout addSubview:[self.class facotoryLbl:toStr(self.model.view_num) key:@"点赞次数"]];
    //新增粉丝
    [layout addSubview:[self.class facotoryLbl:toStr(self.model.add_fans_num) key:@"新增粉丝"]];
    
    layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 20;
    layout.myLeft = 20;
    layout.myRight = 20;
    layout.myHeight = 50;
    layout.gravity = MyGravity_Fill;
    [baseLayout addSubview:layout];
    //订单数
    [layout addSubview:[self.class facotoryLbl:self.model.order_num key:@"订单数"]];
    //订单总额(￥)
    [layout addSubview:[self.class facotoryLbl:self.model.cash_num key:@"订单总额(￥)"]];
    
    lbl = [self.class facotoryLbl:toStr(self.model.add_fans_num) key:@"新增粉丝"];
    lbl.visibility = MyVisibility_Invisible;
    [layout addSubview:lbl];
    
    [baseLayout addSubview:[UIView placeholderVertView]];
    
    SPButton *btn = [SPButton new];
    btn.imagePosition = SPButtonImagePositionLeft;
    btn.imageTitleSpace = 5;
    btn.size = CGSizeMake(135, 35);
    btn.mySize = btn.size;
    btn.titleLabel.font = [UIFont font14];
    [btn setTitle:@"直播数据" forState:UIControlStateNormal];
    [btn setTitleColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"直播结束"] forState:UIControlStateNormal];
    //btn.layer.borderWidth = 1;
    //btn.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8].CGColor;
    btn.myCenterX = 0;
    [baseLayout addSubview:btn];
    [btn addAction:^(UIButton *btn) {
        //MXRoute(@"LiverDetailVC", nil)
    }];
    btn.myBottom = 20;
    
    
    return frameLayout;
}

+ (UILabel *)facotoryLbl:(NSString *)value key:(NSString *)key {
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 0;
    NSArray *txtArr = @[StrF(@"%@\n", value), key];
    NSArray *colorArr = @[[UIColor whiteColor], [[UIColor whiteColor] colorWithAlphaComponent:0.8]];
    NSArray *fontArr = @[[UIFont font18], [UIFont font12]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 10; // 调整行间距
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    lbl.attributedText = att;
    return lbl;
}

- (LiverDetailObj *)detailObj {
    if (!_detailObj) {
        _detailObj = [LiverDetailObj new];
        _detailObj.watchNum = @"803";
        _detailObj.thumbsNum = @"6666";
        _detailObj.addFans = @"30009";
        _detailObj.orderNum = @"999";
        _detailObj.orderAmout = 10091.56;
    }
    return _detailObj;
}

@end
