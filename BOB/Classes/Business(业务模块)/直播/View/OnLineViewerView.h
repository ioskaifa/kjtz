//
//  OnLineViewerView.h
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCRoomListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OnLineViewerView : UIView

@property (nonatomic, weak) MLVBLiveRoom* liveRoom;
@property (nonatomic, weak) TCRoomInfo  *liveInfo;

@property (nonatomic, strong) UIImageView *closeImgView;

- (void)fetchData;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
