//
//  AddLiveGoodsCell.h
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveGoodsObj.h"

typedef NS_ENUM(NSInteger, AddLiveGoodsCellType) {
    ///添加商品
    AddLiveGoodsCellTypeAdd     = 0,
    ///橱窗
    AddLiveGoodsCellTypeShow    = 1,
    ///橱窗编辑
    AddLiveGoodsCellTypeEdit    = 2,
};

NS_ASSUME_NONNULL_BEGIN

@interface AddLiveGoodsCell : UITableViewCell

@property (nonatomic, strong) UIView *line;

@property (nonatomic, assign) AddLiveGoodsCellType cellType;

+ (CGFloat)viewHeight;

- (void)configureView:(LiveGoodsObj *)obj;

@end

NS_ASSUME_NONNULL_END
