//
//  ContectLiverListCell.m
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ContectLiverListCell.h"

@interface ContectLiverListCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UILabel *fansLbl;

@end

@implementation ContectLiverListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
        [self addBottomSingleLine:[UIColor colorWithHexString:@"#EBEBEB"]];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 70;
}

- (void)configureView:(ContectLiverListObj *)obj {
    if (![obj isKindOfClass:ContectLiverListObj.class]) {
        return;
    }
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.photo]];
    
    self.nameLbl.text = obj.name;
    self.fansLbl.text = StrF(@"粉丝数：%@", obj.fansNum);
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self.contentView addMyLinearLayout:MyOrientation_Horz];
    [baseLayout addSubview:self.imgView];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myHeight = self.imgView.height;
    layout.myCenterY= 0;
    layout.weight = 1;
    layout.myLeft = 10;
    layout.myRight = 10;
    [baseLayout addSubview:layout];
    
    [layout addSubview:self.nameLbl];
    [layout addSubview:self.fansLbl];
    
    UILabel *lbl = [UILabel new];
    lbl.size = CGSizeMake(56, 25);
    [lbl setViewCornerRadius:2];
    lbl.textColor = [UIColor whiteColor];
    lbl.backgroundColor = [UIColor moGreen];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myRight = 15;
    lbl.text = @"邀请";
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont font12];
    [baseLayout addSubview:lbl];
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor colorWithHexString:@"#151419"];
            object.myLeft = 0;
            object.myRight = 0;
            object.myHeight = 25;
            object;
        });
    }
    return _nameLbl;
}

- (UILabel *)fansLbl {
    if (!_fansLbl) {
        _fansLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myLeft = 0;
            object.myRight = 0;
            object.myHeight = 20;
            object.myTop = 5;
            object;
        });
    }
    return _fansLbl;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            object.myLeft = 0;
            object.myRight = 0;
            object.size = CGSizeMake(45, 45);
            [object setViewCornerRadius:object.height/2.0];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 15;
            object;
        });
    }
    return _imgView;
}

@end
