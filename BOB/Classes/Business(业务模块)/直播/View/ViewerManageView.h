//
//  ViewerManageView.h
//  BOB
//
//  Created by colin on 2020/11/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPBadgeButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface ViewerManageView : UIView

@property (nonatomic, strong) MyBaseLayout *editView;

@property (nonatomic, strong) SPBadgeButton *goodsBtn;

@property (nonatomic, strong) UIButton *giftBtn;

@property (nonatomic, strong) UIButton *shareBtn;

@property (nonatomic, strong) UIButton *likeBtn;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
