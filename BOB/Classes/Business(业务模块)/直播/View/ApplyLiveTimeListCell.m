//
//  ApplyLiveTimeListCell.m
//  BOB
//
//  Created by colin on 2020/11/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ApplyLiveTimeListCell.h"

@interface ApplyLiveTimeListCell ()

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *statusLbl;

@property (nonatomic, strong) UILabel *timeLbl;

@end

@implementation ApplyLiveTimeListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
        [self addBottomSingleLine:[UIColor colorWithHexString:@"#EBEBEB"]];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 70;
}

- (void)loadContent {
    [self configureView:self.data];
}

- (void)configureView:(ApplyLiveTimeListObj *)obj {
    if (![obj isKindOfClass:ApplyLiveTimeListObj.class]) {
        return;
    }
    self.titleLbl.text = StrF(@"申请时长：%@h",obj.time);
    self.statusLbl.text = obj.formatStatus;
    [self.statusLbl autoMyLayoutSize];
    self.timeLbl.text = obj.formateDate;
}

- (void)createUI {
    self.contentView.backgroundColor = [UIColor moBackground];
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.myTop = 5;
    baseLayout.myLeft = 5;
    baseLayout.myRight = 5;
    baseLayout.myBottom = 5;
    [baseLayout setViewCornerRadius:5];
    [self.contentView addSubview:baseLayout];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myHeight = 22;
    layout.myTop = 10;
    [layout addSubview:self.titleLbl];
    [layout addSubview:self.statusLbl];
    [baseLayout addSubview:layout];
    [baseLayout addSubview:[UIView placeholderVertView]];
    [baseLayout addSubview:self.timeLbl];
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor blackColor];
            object.myLeft = 0;
            object.myRight = 10;
            object.myHeight = 22;
            object.myCenterY = 0;
            object.weight = 1;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)statusLbl {
    if (!_statusLbl) {
        _statusLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font15];
            object.textColor = [UIColor blackColor];
            object.myRight = 0;
            object.myHeight = 22;
            object.myCenterY = 0;
            object;
        });
    }
    return _statusLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#C1C1C1"];
            object.myLeft = 10;
            object.myRight = 10;
            object.myHeight = 22;
            object.myBottom = 10;
            object;
        });
    }
    return _timeLbl;
}

@end
