//
//  LiverManageView.m
//  BOB
//
//  Created by colin on 2020/11/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiverManageView.h"

@implementation LiverManageView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60;
}

- (void)createUI {
    MyLinearLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Horz];
    baseLayout.gravity = MyGravity_Horz_Around;
    [baseLayout addSubview:self.moreBtn];
    [baseLayout addSubview:self.beautifyBtn];
    [baseLayout addSubview:self.musicBtn];
    SPButton *btn = [self.class factoryBtn:@"带货" title:@"带货"];
    btn.visibility = MyVisibility_Invisible;
    [baseLayout addSubview:btn];
    //[baseLayout addSubview:self.contactBtn];
    [baseLayout addSubview:self.goodsBtn];
}

- (SPBadgeButton *)moreBtn {
    if (!_moreBtn) {
        _moreBtn = [self.class factoryBtn:@"直播更多" title:@"更多"];
    }
    return _moreBtn;
}

- (SPBadgeButton *)beautifyBtn {
    if (!_beautifyBtn) {
        _beautifyBtn = [self.class factoryBtn:@"直播美化" title:@"美化"];
    }
    return _beautifyBtn;
}

- (SPBadgeButton *)musicBtn {
    if (!_musicBtn) {
        _musicBtn = [self.class factoryBtn:@"直播音乐" title:@"音乐"];
    }
    return _musicBtn;
}

- (SPBadgeButton *)contactBtn {
    if (!_contactBtn) {
        _contactBtn = [self.class factoryBtn:@"直播连麦" title:@"连麦"];
    }
    return _contactBtn;
}

- (SPBadgeButton *)goodsBtn {
    if (!_goodsBtn) {
        _goodsBtn = [self.class factoryBtn:@"带货" title:@"带货"];
    }
    return _goodsBtn;
}

+ (SPBadgeButton *)factoryBtn:(NSString *)img title:(NSString *)title {
    SPBadgeButton *btn = [SPBadgeButton new];
    btn.imagePosition = SPButtonImagePositionTop;
    btn.imageTitleSpace = 5;
    btn.titleLabel.font = [UIFont font11];
    [btn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.badge.alpha = 0;
    if (![@"带货" isEqualToString:title]) {
        btn.badge.automaticHidden = YES;
        btn.badge.badgeText = @"";
    } else {
        btn.badge.badgeText = @"";
    }
    btn.mySize = btn.size;
    btn.myCenterY = 0;
    return btn;
}

@end
