//
//  MyFansListCell.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyFansListCell.h"
#import "MyFansListObj.h"

@interface MyFansListCell ()

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *statusLbl;

@end

@implementation MyFansListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
        [self addBottomSingleLine:[UIColor colorWithHexString:@"#EBEBEB"]];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 70;
}

- (void)loadContent {
    MyFansListObj *obj = self.data;
    [self configureContent:obj];
}

- (void)configureContent:(MyFansListObj *)obj {
    if (![obj isKindOfClass:MyFansListObj.class]) {
        return;
    }
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.photo]];
    self.nameLbl.text = obj.name;
    if ([@"01" isEqualToString:obj.status]) {
        [self configureStatusLblEach];
    } else if ([@"02" isEqualToString:obj.status]) {
        [self configureStatusLblSingle];
    }
}

- (void)configureView:(MyFollowListObj *)obj {
    if (![obj isKindOfClass:MyFollowListObj.class]) {
        return;
    }
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.photo]];
    self.nameLbl.text = obj.name;
    if ([@"01" isEqualToString:obj.status]) {
        [self configureStatusLblFollowed];
    } else if ([@"02" isEqualToString:obj.status]) {
        [self configureStatusLblEnFollow];
    }
}

///互粉
- (void)configureStatusLblEach {
    self.statusLbl.text = @"互相关注";
    self.statusLbl.textColor = [UIColor colorWithHexString:@"#9999A8"];
    self.statusLbl.layer.borderColor = [UIColor colorWithHexString:@"#DADAE6"].CGColor;
}

///回粉
- (void)configureStatusLblSingle {
    self.statusLbl.text = @"回粉";
    self.statusLbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
    self.statusLbl.layer.borderColor = [UIColor colorWithHexString:@"#FF4757"].CGColor;
}

///已关注
- (void)configureStatusLblFollowed {
    self.statusLbl.text = @"已关注";
    self.statusLbl.textColor = [UIColor colorWithHexString:@"#9999A8"];
    self.statusLbl.layer.borderColor = [UIColor colorWithHexString:@"#DADAE6"].CGColor;
}

///未关注
- (void)configureStatusLblEnFollow {
    self.statusLbl.text = @"未关注";
    self.statusLbl.textColor = [UIColor colorWithHexString:@"#9999A8"];
    self.statusLbl.layer.borderColor = [UIColor colorWithHexString:@"#DADAE6"].CGColor;
}

- (void)createUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self.contentView addSubview:baseLayout];
    
    [baseLayout addSubview:self.imgView];
    [baseLayout addSubview:self.nameLbl];
    [baseLayout addSubview:self.statusLbl];
    
    [self configureStatusLblFollowed];
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor blackColor];
            object.myLeft = 10;
            object.myRight = 10;
            object.myHeight = 22;
            object.weight = 1;
            object.myCenterY = 0;
            object;
        });
    }
    return _nameLbl;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            object.myLeft = 0;
            object.myRight = 0;
            object.size = CGSizeMake(40, 40);
            [object setViewCornerRadius:object.height/2.0];
            object.mySize = object.size;
            object.myLeft = 15;
            object.myCenterY = 0;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)statusLbl {
    if (!_statusLbl) {
        _statusLbl = ({
            UILabel *object = [UILabel new];
            object.size = CGSizeMake(60, 25);
            object.textAlignment = NSTextAlignmentCenter;
            object.font = [UIFont font12];
            object.layer.borderWidth = 1;
            object.myRight = 15;
            object.mySize = object.size;
            object.myCenterY = 0;
            object;
        });
    }
    return _statusLbl;
}

@end
