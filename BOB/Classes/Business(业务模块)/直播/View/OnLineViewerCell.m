//
//  OnLineViewerCell.m
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "OnLineViewerCell.h"

@interface OnLineViewerCell ()

@property (nonatomic, strong) UILabel *indexLbl;

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) SPButton *amoutBtn;

@end

@implementation OnLineViewerCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
        //[self addBottomSingleLine:[UIColor colorWithHexString:@"#EBEBEB"]];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60;
}

- (void)configureView:(MLVBAudienceInfo *)obj {
    if (![obj isKindOfClass:MLVBAudienceInfo.class]) {
        return;
    }
    self.indexLbl.text = StrF(@"%@", obj.custom);
    [self.indexLbl autoMyLayoutSize];
            
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.userAvatar]];
    
    self.nameLbl.text = obj.userName;
    [self.nameLbl autoMyLayoutSize];
    
    [self.amoutBtn setTitle:StrF(@"%0.0f", 10.0) forState:UIControlStateNormal];
    [self.amoutBtn sizeToFit];
    self.amoutBtn.mySize = self.amoutBtn.size;
    
    self.indexLbl.font = [UIFont boldSystemFontOfSize:20];
    if([obj.custom integerValue] == 1) {
        UIColor *color = [UIColor colorWithHexString:@"#FFC400"];
        self.imgView.layer.borderWidth = 1;
        self.imgView.layer.borderColor = color.CGColor;
        self.indexLbl.textColor = color;
    } else if([obj.custom integerValue] == 2) {
        UIColor *color = [UIColor colorWithHexString:@"#AAAABB"];
        self.imgView.layer.borderWidth = 1;
        self.imgView.layer.borderColor = color.CGColor;
        self.imgView.layer.borderColor = color.CGColor;
        self.indexLbl.textColor = color;
    } else if([obj.custom integerValue] == 3) {
        UIColor *color = [UIColor colorWithHexString:@"#C96A24"];
        self.imgView.layer.borderWidth = 1;
        self.imgView.layer.borderColor = color.CGColor;
        self.indexLbl.textColor = color;
    } else {
        self.imgView.layer.borderWidth = 0;
        self.indexLbl.textColor = [UIColor colorWithHexString:@"#46474D"];
        self.indexLbl.font = [UIFont font20];
    }
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self.contentView addMyLinearLayout:MyOrientation_Horz];
    [baseLayout addSubview:self.indexLbl];
    [baseLayout addSubview:self.imgView];
    [baseLayout addSubview:self.nameLbl];    
    [baseLayout addSubview:self.amoutBtn];
}

- (UILabel *)indexLbl {
    if (!_indexLbl) {
        _indexLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font20];
            object.textColor = [UIColor colorWithHexString:@"#46474D"];
            object.myLeft = 15;
            object.myCenterY = 0;
            object;
        });
    }
    return _indexLbl;
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor colorWithHexString:@"#151419"];
            object.myLeft = 10;
            object.myRight = 10;
            object.weight = 1;
            object.myCenterY = 0;
            object;
        });
    }
    return _nameLbl;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            object.myLeft = 0;
            object.myRight = 0;
            object.size = CGSizeMake(36, 36);
            [object setViewCornerRadius:18];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 15;
            object.layer.borderWidth = 1;
            object;
        });
    }
    return _imgView;
}

- (SPButton *)amoutBtn {
    if (!_amoutBtn) {
        _amoutBtn = [SPButton new];
        _amoutBtn.imagePosition = SPButtonImagePositionLeft;
        _amoutBtn.imageTitleSpace = 10;
        _amoutBtn.titleLabel.font = [UIFont font15];
        [_amoutBtn setTitleColor:[UIColor colorWithHexString:@"#7F7F8E"] forState:UIControlStateNormal];
        [_amoutBtn setImage:[UIImage imageNamed:@"观众打赏"] forState:UIControlStateNormal];
        _amoutBtn.myCenterY = 0;
        _amoutBtn.myRight = 15;
    }
    return _amoutBtn;
}

@end
