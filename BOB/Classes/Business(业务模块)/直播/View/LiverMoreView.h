//
//  LiverMoreView.h
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiverMoreView : UIView

@property (nonatomic, strong) UIImageView *closeImgView;
///礼物
@property (nonatomic, strong) SPButton *giftBtn;
///连麦
@property (nonatomic, strong) SPButton *contentBtn;
///暂停直播
@property (nonatomic, strong) SPButton *stopBtn;
///镜头翻转
@property (nonatomic, strong) SPButton *switchBtn;
///分享
@property (nonatomic, strong) SPButton *shareBtn;
///通知粉丝
@property (nonatomic, strong) SPButton *notiBtn;
///直播间设置
@property (nonatomic, strong) SPButton *setBtn;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
