//
//  GiftListCollectionView.h
//  BOB
//
//  Created by colin on 2020/11/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GiftListCollectionView : UIView

+ (CGFloat)viewHeight;

- (void)configureView:(NSArray *)dataArr withAllData:(NSArray *)allData;

@end

NS_ASSUME_NONNULL_END
