//
//  BuyLiveGoodsCell.m
//  BOB
//
//  Created by colin on 2020/11/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyLiveGoodsCell.h"

@interface BuyLiveGoodsCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *stockLbl;

@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) BuyGoodsListObj *cellObj;

@end

@implementation BuyLiveGoodsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 130;
}

- (void)configureView:(BuyGoodsListObj *)obj {
    if (![obj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    self.cellObj = obj;
    NSString *imgUrl = obj.goods_show;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    self.titleLbl.text = obj.title;
    CGFloat width = SCREEN_WIDTH - 50 - self.imgView.width;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    self.stockLbl.text = StrF(@"库存 %@", obj.goods_stock_num);
    
    self.priceLbl.text = StrF(@"￥%0.2f", obj.cash_min);
}

- (void)createUI {
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    [layout setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:layout];
        
    [layout addSubview:self.imgView];
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.weight = 1;
    rightLayout.myLeft = 10;
    rightLayout.myRight = 10;
    rightLayout.myHeight = self.imgView.height;
    rightLayout.myCenterY = 0;
    [layout addSubview:rightLayout];
    
    [rightLayout addSubview:self.titleLbl];
    [rightLayout addSubview:self.stockLbl];
    
    [rightLayout addSubview:[UIView placeholderVertView]];
    
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myLeft = 0;
    bottomLayout.myRight = 0;
    bottomLayout.myHeight = MyLayoutSize.wrap;
    [rightLayout addSubview:bottomLayout];
    
    [bottomLayout addSubview:self.priceLbl];
    
    UILabel *lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor whiteColor];
    lbl.backgroundColor = [UIColor moGreen];
    lbl.font = [UIFont font13];
    lbl.text = @"立即购买";
    lbl.size = CGSizeMake(72, 30);
    lbl.mySize = lbl.size;
    lbl.myLeft = 5;
    lbl.myRight = 0;
    lbl.myCenterY = 0;
    [bottomLayout addSubview:lbl];
    
    [self.contentView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(rightLayout.mas_right);
        make.bottom.mas_equalTo(layout.mas_bottom);
        make.left.mas_equalTo(rightLayout.mas_left);
    }];
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            [object setViewCornerRadius:5];
            object.size = CGSizeMake(100, 100);
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 5;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont lightFont14];
            object.numberOfLines = 2;
            object.textColor = [UIColor colorWithHexString:@"#151419"];
            object.myLeft = 0;
            object.myBottom = 5;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)stockLbl {
    if (!_stockLbl) {
        _stockLbl = [UILabel new];
        _stockLbl.myTop = 5;
        _stockLbl.myLeft = 0;
        _stockLbl.myRight = 0;
        _stockLbl.myHeight = 20;
        _stockLbl.font = [UIFont font12];
        _stockLbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    }
    return _stockLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.font = [UIFont boldFont17];
        _priceLbl.textColor = [UIColor blackColor];
        _priceLbl.myLeft = 0;
        _priceLbl.myRight = 5;
        _priceLbl.myHeight = 25;
        _priceLbl.weight = 1;
    }
    return _priceLbl;
}

- (UIView *)line {
    if (!_line) {
        _line = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor moBlack];
            object;
        });
    }
    return _line;
}

@end
