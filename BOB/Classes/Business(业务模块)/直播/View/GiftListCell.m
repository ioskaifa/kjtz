//
//  GiftListCell.m
//  BOB
//
//  Created by colin on 2020/11/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GiftListCell.h"

@interface GiftListCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UIButton *priceBtn;

@property (nonatomic, strong) UIButton *nameBtn;

@end

@implementation GiftListCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGSize)itemSize {
    CGFloat width = (SCREEN_WIDTH - 50)/4.0;
    return CGSizeMake(width, width + 60);
}

- (void)configureView:(GiftListObj *)obj {
    if (![obj isKindOfClass:GiftListObj.class]) {
        return;
    }
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.gift_show]];
    
    [self.priceBtn setTitle:StrF(@"(￥%0.2f)", obj.gift_sla_num) forState:UIControlStateNormal];
    [self.priceBtn sizeToFit];
    self.priceBtn.mySize = self.priceBtn.size;
    
    [self.nameBtn setTitle:obj.gift_name forState:UIControlStateNormal];
    [self.nameBtn sizeToFit];
    self.nameBtn.mySize = self.nameBtn.size;
    
    self.priceBtn.selected = obj.isSelected;
    self.nameBtn.selected = obj.isSelected;
    
    if (obj.isSelected) {
        self.imgView.layer.borderWidth = 1;
        
    } else {
        self.imgView.layer.borderWidth = 0;
    }
}

#pragma mark - InitUI
- (void)createUI {
    MyBaseLayout *baseLayout = [self.contentView addMyLinearLayout:MyOrientation_Vert];
    [baseLayout addSubview:self.imgView];
    [baseLayout addSubview:self.priceBtn];
    [baseLayout addSubview:self.nameBtn];
}

#pragma mark - Init
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.size = CGSizeMake(80, 80);
        _imgView.mySize = _imgView.size;
        _imgView.myTop = 15;
        _imgView.backgroundColor = [UIColor moBackground];
        _imgView.layer.borderColor = [UIColor moGreen].CGColor;
    }
    return _imgView;
}

- (UIButton *)priceBtn {
    if (!_priceBtn) {
        _priceBtn = [UIButton new];
        [_priceBtn setTitleColor:[UIColor colorWithHexString:@"#AAAABB"] forState:UIControlStateNormal];
        [_priceBtn setTitleColor:[UIColor moGreen] forState:UIControlStateSelected];
        _priceBtn.titleLabel.font = [UIFont font12];
        _priceBtn.myCenterX = 0;
        _priceBtn.myTop = 0;
    }
    return _priceBtn;
}

- (UIButton *)nameBtn {
    if (!_nameBtn) {
        _nameBtn = [UIButton new];
        [_nameBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_nameBtn setTitleColor:[UIColor moGreen] forState:UIControlStateSelected];
        _nameBtn.titleLabel.font = [UIFont font13];
        _nameBtn.myCenterX = 0;
        _nameBtn.myTop = 0;
    }
    return _nameBtn;
}

@end
