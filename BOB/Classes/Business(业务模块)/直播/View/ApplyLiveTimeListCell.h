//
//  ApplyLiveTimeListCell.h
//  BOB
//
//  Created by colin on 2020/11/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "STDTableViewCell.h"
#import "ApplyLiveTimeListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApplyLiveTimeListCell : STDTableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(ApplyLiveTimeListObj *)obj;

@end

NS_ASSUME_NONNULL_END
