//
//  MayInterestedView.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MayInterestedView.h"
#import "MayInterestedCell.h"

@interface MayInterestedView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@end

@implementation MayInterestedView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 20 + [MayInterestedCell itemSize].height;
}

- (void)configureView:(NSArray *)dataArr {
    if (![dataArr isKindOfClass:NSArray.class]) {
        return;
    }
    self.dataSourceArr = dataArr;
    [self.colView reloadData];
}

- (void)reloadData {
    [self.colView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = MayInterestedCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    if (![cell isKindOfClass:MayInterestedCell.class]) {
        return;
    }
    MayInterestedCell *listCell = (MayInterestedCell *)cell;
    [listCell configureView:self.dataSourceArr[indexPath.row]];
}

- (void)createUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    baseLayout.backgroundColor = [UIColor whiteColor];
    [self addSubview:baseLayout];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font12];
    lbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    lbl.text = @"你可能感兴趣的人";
    [lbl autoMyLayoutSize];
    lbl.myTop = 10;
    lbl.myLeft = 15;
    lbl.myBottom = 10;
    [baseLayout addSubview:lbl];
    
    [baseLayout addSubview:self.colView];
}

- (UICollectionView *)colView {
    if (!_colView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = [MayInterestedCell itemSize];
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
        
        _colView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _colView.showsVerticalScrollIndicator = NO;
        _colView.showsHorizontalScrollIndicator = NO;
        _colView.backgroundColor = [UIColor whiteColor];
        _colView.delegate = self;
        _colView.dataSource = self;
        Class cls = MayInterestedCell.class;
        [_colView registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
        _colView.myTop = 0;
        _colView.myLeft = 0;
        _colView.myRight = 0;
        _colView.weight = 1;
    }
    return _colView;
}

@end
