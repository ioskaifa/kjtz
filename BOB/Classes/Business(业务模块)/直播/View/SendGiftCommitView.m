//
//  SendGiftCommitView.m
//  BOB
//
//  Created by colin on 2020/11/24.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SendGiftCommitView.h"
#import "PPNumberButton.h"

@interface SendGiftCommitView ()

@property (nonatomic, strong) PPNumberButton *numberBtn;

@property (nonatomic,strong) UIButton *submitBtn;                                   

@property (nonatomic, strong) GiftListObj *obj;

@end

@implementation SendGiftCommitView

- (instancetype)initWithObj:(GiftListObj *)obj {
    if (self = [super init]) {
        self.obj = obj;
        [self createUI];
    }
    return self;
}

+ (CGSize)viewSize {
    return CGSizeMake(SCREEN_WIDTH - 80, 350);
}

- (BOOL)valiParam {
    return YES;
}

- (void)createUI {
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    [baseLayout setViewCornerRadius:10];
    [baseLayout addSubview:self.closeImgView];
    
    UIImageView *imgView = [UIImageView new];
    imgView.size = CGSizeMake(80, 80);
    imgView.myCenterX = 0;
    imgView.myTop = 5;
    imgView.backgroundColor = [UIColor moBackground];
    [imgView sd_setImageWithURL:[NSURL URLWithString:self.obj.gift_show]];
    [baseLayout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.textColor = [UIColor blackColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.myHeight = 25;
    lbl.font = [UIFont font14];
    lbl.myLeft = 10;
    lbl.myRight = 10;
    lbl.myTop = 10;
    lbl.myBottom = 30;
    lbl.text = StrF(@"打赏给'%@'", self.obj.liverName?:@"");
    [lbl autoMyLayoutSize];
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:self.numberBtn];
    UIView *line = [UIView line];
    line.myLeft = 20;
    line.myRight = 20;
    line.myTop = 10;
    [baseLayout addSubview:line];
    [baseLayout addSubview:[UIView placeholderVertView]];
    [baseLayout addSubview:self.submitBtn];
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object.myLeft = [self.class viewSize].width - object.width - 10;
            object.myTop = 10;
            [object addAction:^(UIView *view) {
                [self routerEventWithName:@"MXCustomAlertVCCancel" userInfo:nil];
            }];
            object;
        });
    }
    return _closeImgView;
}

- (PPNumberButton *)numberBtn {
    if (!_numberBtn) {
        _numberBtn = ({
            PPNumberButton *object = [PPNumberButton numberButtonWithFrame:CGRectZero];
            object.minValue = 1;
            object.increaseTitle = @"";
            object.decreaseTitle = @"";
            object.increaseImage = [UIImage imageNamed:@"加"];
            object.decreaseImage = [UIImage imageNamed:@"减"];
            object.inputFieldFont = 15;
            CGFloat width = [self.class viewSize].width - 40;
            object.mySize = CGSizeMake(width, 30);
            object.myCenterX = 0;
            object.myTop = 5;
            object;
        });
    }
    return _numberBtn;
}

-(UIButton* )submitBtn{
    if (!_submitBtn) {
        _submitBtn = ({
            UIButton *object = [UIButton new];
            CGFloat width = [self.class viewSize].width - 40;
            object.size = CGSizeMake(width, 40);
            [object setViewCornerRadius:5];
            [object setBackgroundColor:[UIColor moGreen]];
            object.titleLabel.font = [UIFont font16];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [object setTitle:Lang(@"确认") forState:UIControlStateNormal];
            object.myCenterX = 0;
            object.mySize = object.size;
            object.myBottom = 20;
            [object addAction:^(UIButton *btn) {
                [self routerEventWithName:@"MXCustomAlertVCconfirm" userInfo:@{@"number":[NSNumber numberWithInteger:self.numberBtn.currentNumber]}];
            }];
            object;
        });
    }
    return _submitBtn;
}

@end
