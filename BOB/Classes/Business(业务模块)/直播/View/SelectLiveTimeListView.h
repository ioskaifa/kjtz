//
//  SelectLiveTimeListView.h
//  BOB
//
//  Created by colin on 2020/11/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectLiveTimeListView : UIView

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
