//
//  LiverManageView.h
//  BOB
//
//  Created by colin on 2020/11/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiverManageView : UIView
///更多
@property (nonatomic, strong) SPBadgeButton *moreBtn;
///美化
@property (nonatomic, strong) SPBadgeButton *beautifyBtn;
///音乐
@property (nonatomic, strong) SPBadgeButton *musicBtn;
///连麦
@property (nonatomic, strong) SPBadgeButton *contactBtn;
///带货
@property (nonatomic, strong) SPBadgeButton *goodsBtn;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
