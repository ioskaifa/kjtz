//
//  LiveListCell.h
//  BOB
//
//  Created by mac on 2020/10/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveListObj.h"
#import "TCRoomListModel.h"

typedef NS_ENUM(NSInteger, LiveListCellType) {
    ///精选
    LiveListCellTypeRecommend  = 0,
    ///关注
    LiveListCellTypeFllowed    = 1,
};

NS_ASSUME_NONNULL_BEGIN

@interface LiveListCell : UICollectionViewCell

@property (nonatomic, assign) LiveListCellType cellType;

+ (CGFloat)viewHeight;

- (void)configureView:(LiveListObj *)obj;

- (void)configureViewEx:(TCRoomInfo *)obj;

@end

NS_ASSUME_NONNULL_END
