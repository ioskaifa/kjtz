//
//  MyReceivedGiftCell.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MyReceivedGiftCell.h"

@interface MyReceivedGiftCell ()

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *timeLbl;

@property (nonatomic, strong) UILabel *describeLbl;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *amountLbl;

@end

@implementation MyReceivedGiftCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
        [self addBottomSingleLine:[UIColor colorWithHexString:@"#EBEBEB"]];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 110;
}

- (void)loadContent {
    MyReceivedGiftObj *obj = self.data;
    [self configureView:obj];
}

- (void)configureView:(MyReceivedGiftObj *)obj {
    if (![obj isKindOfClass:MyReceivedGiftObj.class]) {
        return;
    }
    self.titleLbl.text = obj.senderName;
    self.timeLbl.text = obj.time;
    self.describeLbl.text = obj.describeString;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.giftImg]];
    self.amountLbl.text = obj.formatGiftPrice;
}

- (void)createUI {
    MyFrameLayout *baseLayout = [MyFrameLayout new];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    baseLayout.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:baseLayout];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myCenterY = 0;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = MyLayoutSize.wrap;
    
    [baseLayout addSubview:layout];
    
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.weight = 1;
    subLayout.myLeft = 0;
    subLayout.myRight = 10;
    subLayout.myHeight = MyLayoutSize.wrap;
    subLayout.myCenterY = 0;
    [subLayout addSubview:self.titleLbl];
    [subLayout addSubview:self.timeLbl];
    [subLayout addSubview:self.describeLbl];
    [layout addSubview:subLayout];
    
    subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    subLayout.myLeft = 0;
    subLayout.myRight = 10;
    subLayout.myHeight = MyLayoutSize.wrap;
    subLayout.myWidth = 75;
    subLayout.myCenterY = 0;
    [subLayout addSubview:self.imgView];
    [subLayout addSubview:self.amountLbl];
    [layout addSubview:subLayout];
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor blackColor];
            object.myLeft = 0;
            object.myRight = 0;
            object.myHeight = 22;
            object.myBottom = 10;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)timeLbl {
    if (!_timeLbl) {
        _timeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myLeft = 0;
            object.myRight = 0;
            object.myHeight = 20;
            object.myBottom = 5;
            object;
        });
    }
    return _timeLbl;
}

- (UILabel *)describeLbl {
    if (!_describeLbl) {
        _describeLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myLeft = 0;
            object.myRight = 0;
            object.myHeight = 20;
            object;
        });
    }
    return _describeLbl;
}

- (UILabel *)amountLbl {
    if (!_amountLbl) {
        _amountLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont font12];
            object.textColor = [UIColor colorWithHexString:@"#AAAABB"];
            object.myLeft = 0;
            object.myRight = 0;
            object.myHeight = 20;
            object;
        });
    }
    return _amountLbl;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            object.myLeft = 0;
            object.myRight = 0;
            object.size = CGSizeMake(50, 50);
            object.mySize = object.size;
            object.myRight = 0;
            object.myBottom = 5;
            object;
        });
    }
    return _imgView;
}


@end
