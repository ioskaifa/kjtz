//
//  BuyLiveGoodsListView.h
//  BOB
//
//  Created by colin on 2020/11/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BuyLiveGoodsListView : UIView

@property (nonatomic, strong) UIImageView *closeImgView;

@property (nonatomic, copy) NSString *anchor_user_id;

@property (nonatomic, copy) NSString *live_id;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
