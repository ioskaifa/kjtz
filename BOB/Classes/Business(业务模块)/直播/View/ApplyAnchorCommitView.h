//
//  ApplyAnchorCommitView.h
//  BOB
//
//  Created by colin on 2020/11/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApplyAnchorCommitView : UIView

@property (nonatomic, strong) UILabel *commitLbl;

+ (CGFloat)viewHeight;

- (void)configureView:(NSString *)img tips:(NSArray *)tipsArr;

@end

NS_ASSUME_NONNULL_END
