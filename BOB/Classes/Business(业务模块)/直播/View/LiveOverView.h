//
//  LiveOverView.h
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LivePersonalObj.h"
#import "LiverDetailObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveOverView : UIView

@property(nonatomic,copy) FinishedBlock closeBlock;

@property(nonatomic,strong) NSString *liveID;

@property (nonatomic, strong) LivePersonalObj *personalObj;

@property (nonatomic, strong) LiverDetailObj *detailObj;

@end

NS_ASSUME_NONNULL_END
