//
//  ContectLiverListCell.h
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContectLiverListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContectLiverListCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(ContectLiverListObj *)obj;

@end

NS_ASSUME_NONNULL_END
