//
//  MayInterestedView.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MayInterestedObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MayInterestedView : UIView

+ (CGFloat)viewHeight;

- (void)configureView:(NSArray *)dataArr;

- (void)reloadData;

@end

NS_ASSUME_NONNULL_END
