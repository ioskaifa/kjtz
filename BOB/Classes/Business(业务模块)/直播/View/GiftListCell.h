//
//  GiftListCell.h
//  BOB
//
//  Created by colin on 2020/11/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiftListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface GiftListCell : UICollectionViewCell

+ (CGSize)itemSize;

- (void)configureView:(GiftListObj *)obj;

@end

NS_ASSUME_NONNULL_END
