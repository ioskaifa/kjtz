//
//  ViewerManageView.m
//  BOB
//
//  Created by colin on 2020/11/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ViewerManageView.h"

@implementation ViewerManageView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 60;
}

- (void)createUI {
    MyLinearLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Horz];
    baseLayout.gravity = MyGravity_Horz_Around;
    [baseLayout addSubview:self.editView];
    [baseLayout addSubview:self.goodsBtn];
    [baseLayout addSubview:self.giftBtn];
    //[baseLayout addSubview:self.shareBtn];
    [baseLayout addSubview:self.likeBtn];
}

#pragma mark - Init
- (MyBaseLayout *)editView {
    if(!_editView) {
        MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
        [layout setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.3]];
        layout.size = CGSizeMake(150, 35);
        layout.mySize = layout.size;
        layout.myCenterY = 0;
        
        UIImageView *imgView = [UIImageView autoLayoutImgView:@"观众输入"];
        imgView.myLeft = 10;
        imgView.myCenterY = 0;
        [layout addSubview:imgView];
        
        UILabel *lbl = [UILabel new];
        lbl.font = [UIFont font12];
        lbl.textColor = [UIColor whiteColor];
        lbl.text = @"说点什么";
        [lbl autoMyLayoutSize];
        lbl.myLeft = 10;
        lbl.myCenterY = 0;
        [layout addSubview:lbl];
        _editView = layout;
    }
    
    
    return _editView;
}

- (SPBadgeButton *)goodsBtn {
    if (!_goodsBtn) {
        SPBadgeButton *object = [SPBadgeButton new];
        [object setImage:[UIImage imageNamed:@"带货"] forState:UIControlStateNormal];
        object.badge.automaticHidden = YES;
        object.badge.badgeText = @"";
        [object sizeToFit];
        object.mySize = object.size;
        object.myCenterY = 0;
        _goodsBtn = object;
    }
    return _goodsBtn;
}

- (UIButton *)giftBtn {
    if (!_giftBtn) {
        UIButton *object = [UIButton new];
        [object setImage:[UIImage imageNamed:@"直播礼物"] forState:UIControlStateNormal];
        [object sizeToFit];
        object.mySize = object.size;
        object.myCenterY = 0;
        _giftBtn = object;
    }
    return _giftBtn;
}

- (UIButton *)shareBtn {
    if (!_shareBtn) {
        UIButton *object = [UIButton new];
        [object setImage:[UIImage imageNamed:@"直播分享"] forState:UIControlStateNormal];
        [object sizeToFit];
        object.mySize = object.size;
        object.myCenterY = 0;
        _shareBtn = object;
    }
    return _shareBtn;
}

- (UIButton *)likeBtn {
    if (!_likeBtn) {
        UIButton *object = [UIButton new];
        [object setImage:[UIImage imageNamed:@"直播喜欢"] forState:UIControlStateNormal];
        [object sizeToFit];
        object.mySize = object.size;
        object.myCenterY = 0;
        _likeBtn = object;
    }
    return _likeBtn;
}

@end
