//
//  ContectLiverListView.m
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ContectLiverListView.h"
#import "ContectLiverListCell.h"

@interface ContectLiverListView ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@property (nonatomic, strong) UITextField *searchTF;

@property (nonatomic, copy) NSString *key_word;

@end

@implementation ContectLiverListView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
        [self fetchData];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return SCREEN_HEIGHT * 0.7;
}

- (void)fetchData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    NSString *url = @"";
    url = StrF(@"%@/%@", LcwlServerRoot, url);
    NSDictionary *param = @{@"last_id":self.last_id};
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url);
        net.params(param);
        net.finish(^(id data){
            [self endRefresh];
            NSDictionary *dataDic = (NSDictionary *)data[@"data"];
            if ([data isSuccess]) {
                NSArray *dataArr = [ContectLiverListObj modelListParseWithArray:dataDic[@"goodsList"]];
                [self.dataSourceMArr addObjectsFromArray:dataArr];
                ContectLiverListObj *lastObj = (ContectLiverListObj *)[self.dataSourceMArr lastObject];
                if ([lastObj isKindOfClass:ContectLiverListObj.class]) {
                    self.last_id = lastObj.ID;
                }
                [self.tableView reloadData];
            } else {
                [NotifyHelper showMessageWithMakeText:dataDic[@"msg"]];
            }
        }).failure(^(id error){
            [self endRefresh];
            [NotifyHelper showMessageWithMakeText:[error description]];
            NSArray *dataArr = [ContectLiverListObj contectLiverListObj];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            ContectLiverListObj *lastObj = (ContectLiverListObj *)[self.dataSourceMArr lastObject];
            if (lastObj) {
                self.last_id = lastObj.ID;
            }
            [self.tableView reloadData];
        })
        .execute();
    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma makr - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = ContectLiverListCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:ContectLiverListCell.class]) {
        return;
    }
    ContectLiverListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:ContectLiverListObj.class]) {
        return;
    }
    [(ContectLiverListCell *)cell configureView:obj];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.searchTF) {
        self.key_word = [StringUtil trim:self.searchTF.text];
        if ([StringUtil isEmpty:self.key_word]) {
            return YES;
        }
        self.last_id = @"";
        [self.dataSourceMArr removeAllObjects];
        [self fetchData];
    }
    return YES;
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        self.key_word = [StringUtil trim:self.searchTF.text];
        self.last_id = @"";
        [self.dataSourceMArr removeAllObjects];
        [self fetchData];
    }
}

#pragma mark - InitUI
- (void)createUI {
    self.last_id = @"";
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [baseLayout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font18];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"连麦主播";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 15;
    lbl.myBottom = 15;
    lbl.myCenterX = 0;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:[UIView fullLine]];
    [baseLayout addSubview:self.searchTF];
    [baseLayout addSubview:self.tableView];
    
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.closeImgView.size);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.rowHeight = [ContectLiverListCell viewHeight];
        Class cls = ContectLiverListCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
        @weakify(self)
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self)
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self)
            [self fetchData];
        }];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = [UITextField new];
        _searchTF.delegate = self;
        [_searchTF addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
        [_searchTF setViewCornerRadius:5];
        _searchTF.font = [UIFont font15];
        _searchTF.backgroundColor = [UIColor colorWithHexString:@"#F6F6F9"];
        UIView *leftView = [UIView new];
        leftView.size = CGSizeMake(10, 30);
        
        UIView *rightView = [UIView new];
        rightView.size = CGSizeMake(40, 30);
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
        [leftBtn sizeToFit];
        leftBtn.x = 10;
        leftBtn.y = 5;
        [rightView addSubview:leftBtn];
        _searchTF.rightView = rightView;
        _searchTF.leftView = leftView;
        _searchTF.returnKeyType = UIReturnKeySearch;
        _searchTF.leftViewMode = UITextFieldViewModeAlways;
        _searchTF.rightViewMode = UITextFieldViewModeAlways;
        _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"搜索主播"]
                                                                     
                                                                     colors:@[[UIColor colorWithHexString:@"#B2B2C1"]]
                                                                      
                                                                      fonts:@[[UIFont font14]]];
        _searchTF.attributedPlaceholder = att;
        _searchTF.myLeft = 15;
        _searchTF.myRight = 15;
        _searchTF.myHeight = 35;
        _searchTF.myTop = 10;
    }
    return _searchTF;
}

@end
