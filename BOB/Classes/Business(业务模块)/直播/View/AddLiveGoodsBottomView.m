//
//  AddLiveGoodsBottomView.m
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AddLiveGoodsBottomView.h"

@implementation AddLiveGoodsBottomView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 50;
}

- (void)configureView:(NSInteger)num {
    [self.selectBtn setTitle:StrF(@"全选 (已选%zd件)", num) forState:UIControlStateNormal];
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:self.selectBtn];
    [layout addSubview:[UIView placeholderHorzView]];
    [layout addSubview:self.buyBtn];
    
    UIView *line = [UIView fullLine];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(line.height);
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.mas_top);
    }];
}

#pragma mark - Init
- (SPButton *)selectBtn {
    if (!_selectBtn) {
        _selectBtn = ({
            SPButton *object = [SPButton new];
            object.imageTitleSpace = 10;
            object.imagePosition = SPButtonImagePositionLeft;
            [object setImage:[UIImage imageNamed:@"icon_red_unselect"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"icon_green_select"] forState:UIControlStateSelected];
            [object setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font13];
            [object setTitle:@"全选 (已选0件)" forState:UIControlStateNormal];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 10;
            object;
        });
    }
    return _selectBtn;
}

- (UIButton *)buyBtn {
    if (!_buyBtn) {
        _buyBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(120, 42);
            [object setViewCornerRadius:5];
            object.titleLabel.font = [UIFont boldFont15];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            [object setTitle:@"确定添加" forState:UIControlStateNormal];
            object.myCenterY = 0;
            object.mySize = object.size;
            object.myRight = 10;
            object;
        });
    }
    return _buyBtn;
}

@end
