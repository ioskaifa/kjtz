//
//  MayInterestedCell.m
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MayInterestedCell.h"

@interface MayInterestedCell ()

@property (nonatomic, strong) UILabel *nameLbl;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *followLbl;

@property (nonatomic, strong) MayInterestedObj *cellObj;

@end

@implementation MayInterestedCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGSize)itemSize {
    return CGSizeMake(140, 200);
}

- (void)configureView:(MayInterestedObj *)obj {
    if (![obj isKindOfClass:MayInterestedObj.class]) {
        return;
    }
    self.cellObj = obj;
    self.nameLbl.text = obj.name;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.photo]];
    
    if (obj.isFollow) {
        self.followLbl.text = @"已关注";
        self.followLbl.userInteractionEnabled = NO;
    } else {
        self.followLbl.text = @"关注";
        self.followLbl.userInteractionEnabled = YES;
    }
}

- (void)createUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 10;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 10;
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.layer.borderWidth = 1;
    baseLayout.layer.borderColor = [UIColor moBackground].CGColor;
    [self.contentView addSubview:baseLayout];
    
    [baseLayout addSubview:self.imgView];
    [baseLayout addSubview:self.nameLbl];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font11];
    lbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    lbl.text = @"你可能感兴趣";
    [lbl autoMyLayoutSize];
    lbl.myCenterX = 0;
    lbl.myTop = 5;
    [baseLayout addSubview:lbl];
    
    [baseLayout addSubview:[UIView placeholderVertView]];
    
    lbl = [UILabel new];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont font11];
    lbl.textColor = [UIColor whiteColor];
    lbl.backgroundColor = [UIColor moGreen];
    lbl.text = @"关注";
    lbl.myLeft = 10;
    lbl.myRight = 10;
    lbl.myHeight = 30;
    lbl.myBottom = 10;
    self.followLbl = lbl;
    [lbl addAction:^(UIView *view) {
        [self routerEventWithName:@"MayInterestedCellFollow" userInfo:@{@"cellObj":self.cellObj?:@""}];
    }];
    [baseLayout addSubview:lbl];
    
    
    UIButton *closeBtn = [UIButton new];
    closeBtn.size = CGSizeMake(30, 30);
    [closeBtn setImage:[UIImage imageNamed:@"金牌主播关闭"] forState:UIControlStateNormal];
    [self.contentView addSubview:closeBtn];
    [closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(closeBtn.size);
        make.top.mas_equalTo(self.contentView.mas_top).mas_offset(15);
        make.right.mas_equalTo(self.contentView.mas_right).mas_offset(0);
    }];
    
    
}

- (UILabel *)nameLbl {
    if (!_nameLbl) {
        _nameLbl = ({
            UILabel *object = [UILabel new];
            object.textAlignment = NSTextAlignmentCenter;
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor blackColor];
            object.myLeft = 10;
            object.myRight = 10;
            object.myHeight = 22;
            object.myCenterX = 0;
            object.myTop = 10;
            object;
        });
    }
    return _nameLbl;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            object.size = CGSizeMake(50, 50);
            [object setViewCornerRadius:object.height/2.0];
            object.mySize = object.size;
            object.myTop = 15;
            object.myCenterX = 0;
            object;
        });
    }
    return _imgView;
}

@end
