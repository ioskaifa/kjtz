//
//  ApplyAnchorCommitView.m
//  BOB
//
//  Created by colin on 2020/11/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ApplyAnchorCommitView.h"

@interface ApplyAnchorCommitView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *tipsLbl;

@end

@implementation ApplyAnchorCommitView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 285;
}

- (void)configureView:(NSString *)img tips:(NSArray *)tipsArr {
    self.imgView.image = [UIImage imageNamed:img];
    self.tipsLbl.attributedText = [self.class tipsAttriString:tipsArr.firstObject two:tipsArr.lastObject];
    [self.tipsLbl autoMyLayoutSize];    
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    self.backgroundColor = [UIColor whiteColor];
    
    [layout addSubview:self.imgView];
    [layout addSubview:[UIView placeholderVertView]];
    [layout addSubview:self.tipsLbl];
    [layout addSubview:self.commitLbl];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"提交申请成为主播"];
            object.myCenterX = 0;
            object.myTop = 30;
            object;
        });
    }
    return _imgView;
}

- (UILabel *)tipsLbl {
    if (!_tipsLbl) {
        _tipsLbl = ({
            UILabel *object = [UILabel new];
            object.numberOfLines = 0;
            NSAttributedString *att = [self.class tipsAttriString:@"提交成功" two:@"请耐心等待3~5个工作日，我们会尽快审核"];
            object.attributedText = att;
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myBottom = 20;
            object;
        });
    }
    return _tipsLbl;
}

- (UILabel *)commitLbl {
    if (!_commitLbl) {
        _commitLbl =({
            UILabel *object = [UILabel new];
            object.visibility = MyVisibility_Gone;
            object.textAlignment = NSTextAlignmentCenter;
            object.font = [UIFont font15];
            object.textColor = [UIColor whiteColor];
            object.text = @"重新申请";
            object.backgroundColor = [UIColor moGreen];
            [object setViewCornerRadius:3];
            object.size = CGSizeMake(100, 35);
            object.mySize = object.size;
            object.myCenterX = 0;
            object.myTop = 5;
            object.myBottom = 10;
            object;
        });
    }
    return _commitLbl;
}

+ (NSAttributedString *)tipsAttriString:(NSString *)one two:(NSString *)two {
    NSArray *txtArr = @[one, StrF(@"\n%@", two)];
    NSArray *colorArr = @[[UIColor colorWithHexString:@"#151419"], [UIColor colorWithHexString:@"#AAAABB"]];
    NSArray *fontArr = @[[UIFont boldFont18], [UIFont font13]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineSpacing = 5; // 调整行间距
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSRange range = [att.string rangeOfString:att.string];
    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    return att;
}

@end
