//
//  OnLineViewerCell.h
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCRoomListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OnLineViewerCell : UITableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(MLVBAudienceInfo *)obj;

@end

NS_ASSUME_NONNULL_END
