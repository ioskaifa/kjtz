//
//  OnLineViewerView.m
//  BOB
//
//  Created by colin on 2020/11/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "OnLineViewerView.h"
#import "OnLineViewerCell.h"

@interface OnLineViewerView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation OnLineViewerView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
        //[self fetchData];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return SCREEN_HEIGHT * 0.7;
}

- (void)fetchData {
    [self.liveRoom getAudienceList:self.liveRoom.roomInfo.roomID completion:^(int errCode, NSString *errMsg, NSArray<MLVBAudienceInfo *> *audienceInfoArray) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (errCode == 0) {
                [audienceInfoArray enumerateObjectsWithOptions:NSEnumerationConcurrent usingBlock:^(MLVBAudienceInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    obj.custom = @(idx+1);
                }];
                [self.dataSourceMArr removeAllObjects];
                [self.dataSourceMArr addObjectsFromArray:audienceInfoArray];
                [self.tableView reloadData];
            } else {
                [NotifyHelper showMessageWithMakeText:errMsg];
            }
        });
    }];
    
    return;;
    

//    NSString *url = @"";
//    url = StrF(@"%@/%@", LcwlServerRoot, url);
//    NSDictionary *param = @{@"last_id":self.last_id};
//    [MXNet Post:^(MXNet *net) {
//        net.apiUrl(url);
//        net.params(param);
//        net.finish(^(id data){
//            [self endRefresh];
//            NSDictionary *dataDic = (NSDictionary *)data[@"data"];
//            if ([data isSuccess]) {
//                NSArray *dataArr = [OnLineViewerObj modelListParseWithArray:dataDic[@"goodsList"]];
//                [self.dataSourceMArr addObjectsFromArray:dataArr];
//                OnLineViewerObj *lastObj = (OnLineViewerObj *)[self.dataSourceMArr lastObject];
//                if ([lastObj isKindOfClass:OnLineViewerObj.class]) {
//                    self.last_id = lastObj.ID;
//                }
//                [self.tableView reloadData];
//            } else {
//                [NotifyHelper showMessageWithMakeText:dataDic[@"msg"]];
//            }
//        }).failure(^(id error){
//            [self endRefresh];
//            [NotifyHelper showMessageWithMakeText:[error description]];
//            NSArray *dataArr = [OnLineViewerObj onLineViewerObj];
//            [self.dataSourceMArr addObjectsFromArray:dataArr];
//            OnLineViewerObj *lastObj = (OnLineViewerObj *)[self.dataSourceMArr lastObject];
//            if (lastObj) {
//                self.last_id = lastObj.ID;
//            }
//            [self.tableView reloadData];
//        })
//        .execute();
//    }];
}

- (void)endRefresh {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

#pragma makr - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = OnLineViewerCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:OnLineViewerCell.class]) {
        return;
    }
    MLVBAudienceInfo *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:MLVBAudienceInfo.class]) {
        return;
    }
    
    [(OnLineViewerCell *)cell configureView:obj];
}

#pragma mark - InitUI
- (void)createUI {
    self.last_id = @"";
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [baseLayout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font18];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"在线观众";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 15;
    lbl.myBottom = 15;
    lbl.myCenterX = 0;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:[UIView fullLine]];
    [baseLayout addSubview:self.tableView];
    
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.closeImgView.size);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.rowHeight = [OnLineViewerCell viewHeight];
        Class cls = OnLineViewerCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:NSStringFromClass(cls)];
//        @weakify(self)
//        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
//            @strongify(self)
//            self.last_id = @"";
//            [self fetchData];
//        }];
//        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
//            @strongify(self)
//            [self fetchData];
//        }];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

@end
