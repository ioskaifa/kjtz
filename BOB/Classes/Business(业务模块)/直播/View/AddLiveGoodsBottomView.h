//
//  AddLiveGoodsBottomView.h
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddLiveGoodsBottomView : UIView
///全选
@property (nonatomic, strong) SPButton *selectBtn;
///确定添加
@property (nonatomic, strong) UIButton *buyBtn;

+ (CGFloat)viewHeight;

- (void)configureView:(NSInteger)num;

@end

NS_ASSUME_NONNULL_END
