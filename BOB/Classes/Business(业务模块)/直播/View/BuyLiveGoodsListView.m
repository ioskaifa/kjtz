//
//  BuyLiveGoodsListView.m
//  BOB
//
//  Created by colin on 2020/11/16.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyLiveGoodsListView.h"
#import "BuyLiveGoodsCell.h"

@interface BuyLiveGoodsListView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, copy) NSString *last_id;

@end

@implementation BuyLiveGoodsListView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return SCREEN_HEIGHT * 0.7;
}

- (void)setAnchor_user_id:(NSString *)anchor_user_id {
    _anchor_user_id = anchor_user_id;
    [self fetchData];
}

- (void)fetchData {
    if ([@"" isEqualToString:self.last_id]) {
        [self.dataSourceMArr removeAllObjects];
    }
    NSString *url = @"api/live/userLiveGoods/getLiveGoodsList";
    [self request:url param:@{
        @"anchor_user_id":self.anchor_user_id?:@"",
        @"last_id":self.last_id
    } completion:^(BOOL success, id object, NSString *error) {
        [self endRefreshing];
        NSArray *dataArr = [BuyGoodsListObj modelListParseWithArray:object[@"data"][@"liveGoodsList"]];
        [self.dataSourceMArr addObjectsFromArray:dataArr];
        BuyGoodsListObj *lastObj = self.dataSourceMArr.lastObject;
        if ([lastObj isKindOfClass:BuyGoodsListObj.class]) {
            self.last_id = lastObj.ID;
        }
        [self.tableView reloadData];
    }];
}

- (void)endRefreshing {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = BuyLiveGoodsCell.class;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    if (![cell isKindOfClass:BuyLiveGoodsCell.class]) {
        return;
    }
    BuyLiveGoodsCell *listCell = (BuyLiveGoodsCell *)cell;
    [listCell configureView:self.dataSourceMArr[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    BuyGoodsListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BuyLiveGoodsListViewBuy" object:nil];
    MXRoute(@"GoodInfoVC", (@{@"goods_id":obj.ID, @"isLive":@(1), @"live_id":self.live_id?:@""}))    
}

#pragma mark - InitUI
- (void)createUI {
    self.last_id = @"";
    MyBaseLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    baseLayout.backgroundColor = [UIColor whiteColor];
    baseLayout.size = CGSizeMake(SCREEN_WIDTH, [self.class viewHeight]);
    [baseLayout setBorderWithCornerRadius:10 byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font18];
    lbl.textColor = [UIColor moBlack];
    lbl.text = @"商品";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 15;
    lbl.myBottom = 15;
    lbl.myCenterX = 0;
    [baseLayout addSubview:lbl];
    [baseLayout addSubview:[UIView fullLine]];
    [baseLayout addSubview:self.tableView];
    
    [self addSubview:self.closeImgView];
    [self.closeImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.closeImgView.size);
        make.top.mas_equalTo(self.mas_top).offset(15);
        make.right.mas_equalTo(self.mas_right).offset(-15);
    }];
}

#pragma mark - Init
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.tableFooterView = [UIView new];
        _tableView.rowHeight = [BuyLiveGoodsCell viewHeight];
        Class cls = BuyLiveGoodsCell.class;
        [_tableView registerClass:cls forCellReuseIdentifier:   NSStringFromClass(cls)];
        @weakify(self);
        _tableView.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            @strongify(self);
            self.last_id = @"";
            [self fetchData];
        }];
        _tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self fetchData];
        }];
        _tableView.myTop = 0;
        _tableView.myLeft = 0;
        _tableView.myRight = 0;
        _tableView.weight = 1;
    }
    return _tableView;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UIImageView *)closeImgView {
    if (!_closeImgView) {
        _closeImgView = ({
            UIImageView *object = [UIImageView autoLayoutImgView:@"icon_public_close"];
            object;
        });
    }
    return _closeImgView;
}

@end
