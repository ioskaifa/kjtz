//
//  MyFansListCell.h
//  BOB
//
//  Created by colin on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "STDTableViewCell.h"
#import "MyFollowListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyFansListCell : STDTableViewCell

+ (CGFloat)viewHeight;

- (void)configureView:(MyFollowListObj *)obj;

@end

NS_ASSUME_NONNULL_END
