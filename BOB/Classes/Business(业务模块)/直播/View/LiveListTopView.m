//
//  LiveListTopView.m
//  BOB
//
//  Created by mac on 2020/10/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "LiveListTopView.h"

@interface LiveListTopView ()

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) NSMutableArray *btnMArr;

@property (nonatomic, strong) UIButton *selectedBtn;

@end

@implementation LiveListTopView

- (instancetype)initWithData:(NSArray *)dataArr {
    if (self = [super init]) {
        self.dataArr = dataArr;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 50;
}

#pragma mark - IBAction
- (void)btnClick:(UIButton *)sender {
    if (self.selectedBtn == sender) {
        return;
    } else {
        self.selectedBtn.selected = NO;
        [self configureBtnState:self.selectedBtn];
    }
    
    sender.selected = YES;
    self.selectedBtn = sender;
    [self configureBtnState:self.selectedBtn];
    [self updateBottomLinePosition:self.selectedBtn];
    if (self.delegate && [self.delegate respondsToSelector:@selector(liveListTopView:didSelectItemAtIndex:)]) {
        [self.delegate liveListTopView:self didSelectItemAtIndex:sender.tag];
    }
}

- (void)updateBottomLinePosition:(UIButton *)btn {
    CGRect btnFrame = [btn.superview.superview convertRect:btn.frame toView:self.lineView.superview];
    if (CGSizeEqualToSize(btnFrame.size, CGSizeZero)) {
        return;
    }
    CGFloat left = CGRectGetMinX(btnFrame) + (CGRectGetWidth(btnFrame) - self.lineView.width)/2.0;
    if (left == self.lineView.x) {
        return;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.lineView.x = left;
    }];
}

- (void)configureBtnState:(UIButton *)sender {
    if (sender.selected) {
        [self configureBtnSelected:sender];
    } else {
        [self configureBtnNormal:sender];
    }
}

- (void)configureBtnNormal:(UIButton *)sender {
    sender.titleLabel.font = [UIFont font14];
}

- (void)configureBtnSelected:(UIButton *)sender {
    sender.titleLabel.font = [UIFont boldFont18];
}

- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    for (NSInteger i = 0; i < self.dataArr.count; i++) {
        NSString *title = self.dataArr[i];
        UIButton *btn = [UIButton new];
        [btn setTitle:title forState:UIControlStateNormal];
        btn.size = CGSizeMake(40, 25);
        btn.mySize = btn.size;
        btn.myRight = 20;
        btn.tag = i;
        btn.myCenterY = 0;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [btn setTitleColor:[UIColor colorWithHexString:@"#7F7F8E"] forState:UIControlStateNormal];
        if (i == 0) {
            btn.myLeft = 15;
            btn.selected = YES;
            self.selectedBtn = btn;
        } else {
            btn.myLeft = 0;
            btn.selected = NO;
        }
        [self configureBtnState:btn];
        [self.btnMArr addObject:btn];
        @weakify(self)
        [btn addAction:^(UIButton *btn) {
            @strongify(self)
            [self btnClick:btn];
        }];
        [layout addSubview:btn];
    }
    [layout addSubview:[UIView placeholderHorzView]];
    [layout addSubview:self.searchBtn];
    [self addSubview:self.lineView];
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = [UIColor blackColor];
        _lineView.size = CGSizeMake(20, 3);
        _lineView.x = (40 - _lineView.width) / 2.0 + 15;
        _lineView.y = [self.class viewHeight] - _lineView.height;
    }
    return _lineView;
}

- (NSMutableArray *)btnMArr {
    if (!_btnMArr) {
        _btnMArr = [NSMutableArray array];
    }
    return _btnMArr;
}

- (SPButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = ({
            SPButton *object = [SPButton new];
            [object setViewCornerRadius:2];
            [object setBackgroundColor:[UIColor colorWithHexString:@"#F6F6F9"]];
            object.imagePosition = SPButtonImagePositionRight;
            object.imageTitleSpace = 35;
            [object setTitleColor:[UIColor colorWithHexString:@"#B2B2C1"] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font12];
            [object setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
            [object setTitle:@"试试搜商品/主播" forState:UIControlStateNormal];
            object.size = CGSizeMake(160, 30);
            object.mySize = object.size;
            object.myRight = 15;
            object.myCenterY = 0;
            object;
        });
    }
    return _searchBtn;
}

@end
