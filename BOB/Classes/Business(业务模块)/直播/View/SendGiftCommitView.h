//
//  SendGiftCommitView.h
//  BOB
//
//  Created by colin on 2020/11/24.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiftListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface SendGiftCommitView : UIView

@property (nonatomic, strong) UIImageView *closeImgView;

+ (CGSize)viewSize;

- (instancetype)initWithObj:(GiftListObj *)obj;

@end

NS_ASSUME_NONNULL_END
