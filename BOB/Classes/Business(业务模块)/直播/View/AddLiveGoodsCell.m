//
//  AddLiveGoodsCell.m
//  BOB
//
//  Created by colin on 2020/11/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AddLiveGoodsCell.h"

@interface AddLiveGoodsCell ()

@property (nonatomic, strong) UIButton *selectBtn;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *stockLbl;

@property (nonatomic, strong) UILabel *priceLbl;
///失效
@property (nonatomic, strong) UIImageView *invalidImgView;

@property (nonatomic, strong) LiveGoodsObj *cellObj;

@property (nonatomic, strong) SPButton *explainBtn;

@property (nonatomic, strong) UIButton *delBtn;

@end

@implementation AddLiveGoodsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 130;
}

- (void)setCellType:(AddLiveGoodsCellType)cellType {
    _cellType = cellType;
    switch (cellType) {
        case AddLiveGoodsCellTypeAdd: {
            self.selectBtn.visibility = MyVisibility_Visible;
            self.explainBtn.visibility = MyVisibility_Gone;
            self.delBtn.visibility = MyVisibility_Gone;
            break;
        }
        case AddLiveGoodsCellTypeShow: {
            self.selectBtn.visibility = MyVisibility_Gone;
            self.explainBtn.visibility = MyVisibility_Visible;
            self.delBtn.visibility = MyVisibility_Gone;
            break;
        }
        case AddLiveGoodsCellTypeEdit: {
            self.selectBtn.visibility = MyVisibility_Gone;
            self.explainBtn.visibility = MyVisibility_Gone;
            self.delBtn.visibility = MyVisibility_Visible;
            break;
        }
        default:
            break;
    }
}

- (void)configureView:(LiveGoodsObj *)obj {
    if (![obj isKindOfClass:LiveGoodsObj.class]) {
        return;
    }
    self.cellObj = obj;
    self.selectBtn.selected = obj.isSelected;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.goods_show]];
    
    self.titleLbl.text = obj.goods_name;
    CGFloat width = SCREEN_WIDTH - 50 - self.imgView.width - self.selectBtn.width;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    self.stockLbl.text = StrF(@"库存：%zd", obj.goods_stock_num);
    
    self.priceLbl.text = StrF(@"%@%0.2f", @"￥", obj.cash_min);
//    if (obj.isInvalid) {
//        [self configureViewInvalidState];
//    } else {
//        [self configureViewValidState];
//    }
    [self configureViewValidState];
    if (obj.isExplain) {
        [self configureExplainBtnIng];
    } else {
        [self configureExplainBtn];
    }
}

#pragma mark - 失效产品
- (void)configureViewInvalidState {
    self.invalidImgView.hidden = NO;
    self.imgView.alpha = 0.7;
    self.titleLbl.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
    self.priceLbl.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
}

#pragma mark - 正常产品
- (void)configureViewValidState {
    self.invalidImgView.hidden = YES;
    self.imgView.alpha = 1;
    self.titleLbl.textColor = [UIColor moBlack];
    self.priceLbl.textColor = [UIColor blackColor];
}

#pragma mark - 开始讲解
- (void)configureExplainBtn {
    self.explainBtn.selected = NO;
    self.explainBtn.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
}

#pragma mark - 正在讲解
- (void)configureExplainBtnIng {
    self.explainBtn.selected = YES;
    self.explainBtn.layer.borderColor = [UIColor colorWithHexString:@"#02C1C4"].CGColor;
}

- (void)createUI {
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    [layout setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.selectBtn];
    [layout addSubview:self.imgView];
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.weight = 1;
    rightLayout.myLeft = 10;
    rightLayout.myRight = 10;
    rightLayout.myHeight = self.imgView.height;
    rightLayout.myCenterY = 0;
    [layout addSubview:rightLayout];
    
    [rightLayout addSubview:self.titleLbl];
    [rightLayout addSubview:self.stockLbl];
    
    [rightLayout addSubview:[UIView placeholderVertView]];
    
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myLeft = 0;
    bottomLayout.myRight = 0;
    bottomLayout.myHeight = MyLayoutSize.wrap;
    [rightLayout addSubview:bottomLayout];
    
    [bottomLayout addSubview:self.priceLbl];
    [bottomLayout addSubview:self.explainBtn];
    [bottomLayout addSubview:self.delBtn];
    
    [self.contentView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(rightLayout.mas_right);
        make.bottom.mas_equalTo(layout.mas_bottom);
        make.left.mas_equalTo(rightLayout.mas_left);
    }];
    
    [self.imgView addSubview:self.invalidImgView];
    [self.invalidImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.invalidImgView.size);
        make.center.mas_equalTo(self.imgView);
    }];
}

#pragma mark - Init
- (UIButton *)selectBtn {
    if (!_selectBtn) {
        _selectBtn = ({
            UIButton *object = [UIButton new];
            [object setImage:[UIImage imageNamed:@"icon_public_unselect"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"icon_green_select"] forState:UIControlStateSelected];
            object.size = CGSizeMake(30, 30);
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 5;
            [object addAction:^(UIButton *btn) {
                [self.nextResponder routerEventWithName:@"AddLiveGoodsCellSelect" userInfo:@{@"cellObj":self.cellObj}];
            }];
            object;
        });
    }
    return _selectBtn;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            [object setViewCornerRadius:5];
            object.size = CGSizeMake(100, 100);
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 5;
            object;
        });
    }
    return _imgView;
}

- (UIImageView *)invalidImgView {
    if (!_invalidImgView) {
        _invalidImgView = ({
            UIImageView *object = [UIImageView new];
            object.image = [UIImage imageNamed:@"购物车已失效"];
            [object sizeToFit];
            object;
        });
    }
    return _invalidImgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont lightFont14];
            object.numberOfLines = 2;
            object.textColor = [UIColor colorWithHexString:@"#151419"];
            object.myLeft = 0;
            object.myBottom = 5;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)stockLbl {
    if (!_stockLbl) {
        _stockLbl = [UILabel new];
        _stockLbl.myTop = 5;
        _stockLbl.myLeft = 0;
        _stockLbl.myRight = 0;
        _stockLbl.myHeight = 20;
        _stockLbl.font = [UIFont font12];
        _stockLbl.textColor = [UIColor colorWithHexString:@"#AAAABB"];
    }
    return _stockLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.font = [UIFont boldFont17];
        _priceLbl.textColor = [UIColor blackColor];
        _priceLbl.myLeft = 0;
        _priceLbl.myRight = 5;
        _priceLbl.myHeight = 25;
        _priceLbl.weight = 1;
    }
    return _priceLbl;
}

- (UIView *)line {
    if (!_line) {
        _line = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor moBackground];
            object;
        });
    }
    return _line;
}

- (SPButton *)explainBtn {
    if (!_explainBtn) {
        _explainBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionLeft;
            object.imageTitleSpace = 5;
            object.titleLabel.font = [UIFont font12];
            [object setImage:[UIImage imageNamed:@"开始讲解"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"正在讲解"] forState:UIControlStateSelected];
            [object setTitle:@"开始讲解" forState:UIControlStateNormal];
            [object setTitle:@"正在讲解" forState:UIControlStateSelected];
            [object setTitleColor:[UIColor colorWithHexString:@"#737380"] forState:UIControlStateNormal];
            [object setTitleColor:[UIColor colorWithHexString:@"#02C1C4"] forState:UIControlStateSelected];
            object.layer.borderWidth = 1;
            object.size = CGSizeMake(85, 30);
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myRight = 0;
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                if (btn.selected) {
                    return;
                }
                
                btn.selected = !btn.selected;
                if (btn.selected) {
                    [self configureExplainBtnIng];
                } else {
                    [self configureExplainBtn];
                }
                [self.nextResponder routerEventWithName:@"AddLiveGoodsCellSelect" userInfo:@{@"cellObj":self.cellObj?:@""}];
            }];
            object;
        });
    }
    return _explainBtn;
}

- (UIButton *)delBtn {
    if (!_delBtn) {
        _delBtn = ({
            UIButton *object = [UIButton buttonWithType:UIButtonTypeCustom];
            object.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
            object.titleLabel.font = [UIFont font12];
            [object setTitle:@"删除" forState:UIControlStateNormal];
            [object setTitle:@"删除" forState:UIControlStateSelected];
            [object setTitleColor:[UIColor colorWithHexString:@"#737380"] forState:UIControlStateNormal];
            [object setTitleColor:[UIColor colorWithHexString:@"#737380"] forState:UIControlStateSelected];
            object.layer.borderWidth = 1;
            object.size = CGSizeMake(85, 30);
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myRight = 0;
            @weakify(self)
            [object addAction:^(UIButton *btn) {
                @strongify(self)
                [self.nextResponder routerEventWithName:@"LiveGoodsListVCDel" userInfo:@{@"cellObj":self.cellObj?:@""}];
            }];
            object;
        });
    }
    return _delBtn;
}

@end
