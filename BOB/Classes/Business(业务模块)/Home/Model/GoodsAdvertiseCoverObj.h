//
//  GoodsAdvertiseCoverObj.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsAdvertiseCoverObj : BaseObject

@property (nonatomic , copy) NSString              * create_by;
@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , copy) NSString              * up_date;
@property (nonatomic , assign) NSInteger              goods_id;
@property (nonatomic , copy) NSString              * up_time;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , assign) NSInteger              ID;
@property (nonatomic , assign) NSInteger              order_num;
@property (nonatomic , copy) NSString              * advertise_type;
@property (nonatomic , copy) NSString              * update_by;
@property (nonatomic , copy) NSString              * advertise_cover;

+ (NSString *)advertise_typeToCustom:(NSString *)type;

@end

NS_ASSUME_NONNULL_END
