//
//  CategoryObj.m
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "CategoryObj.h"

@implementation CategoryObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

+ (instancetype)allCategoryObj {
    CategoryObj *obj = [CategoryObj new];
    obj.category_name = @"全部频道";
    obj.category_icon = @"全部频道";
    return obj;
}

@end
