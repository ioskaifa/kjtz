//
//  GoodsAdvertiseCoverObj.m
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodsAdvertiseCoverObj.h"

@implementation GoodsAdvertiseCoverObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id"};
}

+ (NSString *)advertise_typeToCustom:(NSString *)type {
    if ([@"01" isEqualToString:type]) {
        return @"聚划算";
    } else if ([@"02" isEqualToString:type]) {
        return @"天天特卖";
    } else if ([@"03" isEqualToString:type]) {
        return @"热销美妆";
    } else if ([@"04" isEqualToString:type]) {
        return @"品牌购";
    } else if ([@"05" isEqualToString:type]) {
        return @"有好货";
    } else if ([@"06" isEqualToString:type]) {
        return @"直播";
    } else if ([@"07" isEqualToString:type]) {
        return @"1SLA专区";
    }
    return @"";
}

@end
