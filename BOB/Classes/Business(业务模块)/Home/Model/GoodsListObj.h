//
//  GoodsListObj.h
//  BOB
//
//  Created by mac on 2020/9/9.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsListObj : BaseObject

///是否是自营商品
@property (nonatomic, assign) BOOL isSelfSupport;

@property (nonatomic, copy) NSString *ID;
///商品名称
@property (nonatomic, copy) NSString *title;
///商品类型 01：自营商品 02：供应链商品
@property (nonatomic, copy) NSString *goods_type;
///价格
@property (nonatomic, assign) CGFloat price;
///图片
@property (nonatomic, copy) NSString *photo;
///销量
@property (nonatomic, assign) NSInteger sales;
///排序 加载下一页的时候传过来该值
@property (nonatomic , copy) NSString *order_num;

@property (nonatomic, assign) CGFloat itemHeight;

@property (nonatomic, assign) BOOL only_sla_pay;

@end

NS_ASSUME_NONNULL_END
