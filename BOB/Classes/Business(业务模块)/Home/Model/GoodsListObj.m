//
//  GoodsListObj.m
//  BOB
//
//  Created by mac on 2020/9/9.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodsListObj.h"

@implementation GoodsListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"goods_id",
             @"title" : @"goods_name",
             @"photo" : @"goods_show",
             @"sales" : @"goods_sales_num",
             @"price" : @"cash_min"};
}

- (BOOL)isSelfSupport {
    if ([@"01" isEqualToString:self.goods_type]) {
        return YES;
    }
    return NO;
}

- (NSString *)photo {
    if (self.isSelfSupport) {
        return StrF(@"%@/%@", UDetail.user.qiniu_domain, _photo);
    } else if ([@"02" isEqualToString:self.goods_type]) {
        return _photo;
    }
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _photo);
}

@end
