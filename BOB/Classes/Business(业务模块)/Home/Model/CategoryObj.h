//
//  CategoryObj.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface CategoryObj : BaseObject

@property (nonatomic , copy) NSString              * category_name;
@property (nonatomic , copy) NSString              * category_icon;
@property (nonatomic , copy) NSString              * tab_icon;
@property (nonatomic , copy) NSString              *ID;
///单选
@property (nonatomic , assign) BOOL isSelected;

+ (instancetype)allCategoryObj;

@end

NS_ASSUME_NONNULL_END
