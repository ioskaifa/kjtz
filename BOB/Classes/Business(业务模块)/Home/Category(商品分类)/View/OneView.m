//
//  OneView.m
//  LinkageMenu
//
//  Created by XXX on 2017/3/10.
//  Copyright © 2017年 EmotionV. All rights reserved.
//

#import "OneView.h"
#import "OneCollectionViewCell.h"
#import "SecondClassModel.h"
#import "GKCycleScrollView.h"

@interface OneView()<GKCycleScrollViewDataSource, GKCycleScrollViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

///轮播图
@property (nonatomic, strong) GKCycleScrollView *cycleScrollView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UIPageControl *pageControl;

@end

@implementation OneView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.frame = frame;
        self.backgroundColor = [UIColor whiteColor];
        [self.collectionView registerClass:[OneCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        layout.myTop = 0;
        layout.myLeft = 0;
        layout.myRight = 0;
        layout.myBottom = 0;
        [self addSubview:layout];
//        [layout addSubview:self.cycleScrollView];
        [layout addSubview:self.collectionView];
        self.collectionView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)setDataArray:(NSArray *)dataArray{
    _dataArray = dataArray;
    [self.collectionView reloadData];
}

- (void)configureCycleView:(NSArray *)dataArr {
    if (![dataArr isKindOfClass:NSArray.class]) {
        self.cycleScrollView.visibility = MyVisibility_Gone;
        return;
    }
    if (dataArr.count == 0) {self.cycleScrollView.visibility = MyVisibility_Gone;
        return;
    }
    self.cycleScrollView.visibility = MyVisibility_Visible;
    [self.dataSourceMArr removeAllObjects];
    [self.dataSourceMArr addObjectsFromArray:dataArr];
    [self.cycleScrollView reloadData];
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.minimumInteritemSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.myTop = 0;
        _collectionView.myLeft = 0;
        _collectionView.myRight = 0;
        _collectionView.weight = 1;
    }
    return _collectionView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellWidth = (self.frame.size.width - 5.0) / 3.0;
    CGFloat cellHeight = cellWidth + 20;
    return CGSizeMake(cellWidth, cellHeight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    OneCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    SecondClassModel *model = [_dataArray objectAtIndex:indexPath.row];
    [cell.backView sd_setImageWithURL:[NSURL URLWithString:model.categoryImg] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
    }];
    cell.titleLabel.text = model.categoryName;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    SecondClassModel *model = [_dataArray objectAtIndex:indexPath.row];
    Block_Exec(self.clickBlck,model);
}

#pragma mark GKCycleScrollViewDataSource
- (NSInteger)numberOfCellsInCycleScrollView:(GKCycleScrollView *)cycleScrollView {
    return self.dataSourceMArr.count;
}

- (GKCycleScrollViewCell *)cycleScrollView:(GKCycleScrollView *)cycleScrollView cellForViewAtIndex:(NSInteger)index {
    GKCycleScrollViewCell *cell = [cycleScrollView dequeueReusableCell];
    if (!cell) {
        cell = [GKCycleScrollViewCell new];
    }
    if (index < self.dataSourceMArr.count) {
        NSDictionary *dict = self.dataSourceMArr[index];
        NSString *img_url = dict[@"img_url"];
        if ([img_url isKindOfClass:NSString.class]) {
            img_url = StrF(@"%@/%@", UDetail.user.qiniu_domain, img_url);
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                
            }];
        }
    }
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    return cell;
}

- (void)cycleScrollView:(GKCycleScrollView *)cycleScrollView didSelectCellAtCell:(GKCycleScrollViewCell *)cell
            atIndex:(NSInteger)index {
    if (index >= self.dataSourceMArr.count) {
        return;
    }
    NSDictionary *dataDic = self.dataSourceMArr[index];
    if (![dataDic isKindOfClass:NSDictionary.class]) {
        return;
    }
    NSString *img_url = dataDic[@"img_url"];
    if (![img_url isKindOfClass:NSString.class]) {
        return;
    }
}

#pragma mark - 轮播图
- (GKCycleScrollView *)cycleScrollView {
    if (!_cycleScrollView) {
        _cycleScrollView.height = 150;
        _cycleScrollView = [[GKCycleScrollView alloc] initWithFrame:CGRectMake(15, 10, self.width - 30, 88)];
        [_cycleScrollView setViewCornerRadius:5];
        _cycleScrollView.pageControl = self.pageControl;
        _cycleScrollView.isAutoScroll = YES;
        _cycleScrollView.dataSource = self;
        _cycleScrollView.delegate = self;
        _cycleScrollView.backgroundColor = [UIColor whiteColor];
        _cycleScrollView.myTop = 10;
        _cycleScrollView.myLeft = 15;
        _cycleScrollView.myRight = 15;
        _cycleScrollView.myBottom = 15;
        _cycleScrollView.myHeight = _cycleScrollView.height;
        _cycleScrollView.visibility = MyVisibility_Gone;
        [_cycleScrollView addSubview:self.pageControl];
        _cycleScrollView.backgroundColor = [UIColor moBackground];
    }
    return _cycleScrollView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [UIPageControl new];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.numberOfPages = self.dataSourceMArr.count;
        _pageControl.frame = CGRectMake(20, self.cycleScrollView.height - 20, SCREEN_WIDTH - 40 - 30, 20);
    }
    return _pageControl;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

@end
