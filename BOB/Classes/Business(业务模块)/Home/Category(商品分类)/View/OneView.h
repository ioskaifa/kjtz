//
//  OneView.h
//  LinkageMenu
//
//  Created by XXX on 2017/3/10.
//  Copyright © 2017年 EmotionV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OneView : UIView<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSArray *dataArray;
@property(nonatomic,copy) FinishedBlock clickBlck;

- (void)configureCycleView:(NSArray *)dataArr;

@end
