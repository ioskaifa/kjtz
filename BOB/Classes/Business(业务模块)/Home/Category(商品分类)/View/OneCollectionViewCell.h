//
//  OneCollectionViewCell.h
//  LinkageMenu
//
//  Created by XXX on 2017/3/10.
//  Copyright © 2017年 EmotionV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OneCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *backView;
@property (nonatomic, strong) UILabel *titleLabel;

@end
