//
//  CategoryVC.m
//  RatelBrother
//
//  Created by XXX on 2020/4/9.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "CategoryVC.h"
#import "OneView.h"
#import "LinkageMenuView.h"
#import "OneCollectionViewCell.h"
#import "FirstClassModel.h"
#import "SecondClassModel.h"

@interface CategoryVC ()

@property (strong,nonatomic) OneView *oneView;
/* 顶部工具View */
@property (nonatomic, strong) NSMutableArray<FirstClassModel *> *firstClassArr;
@property (nonatomic, strong) NSArray<SecondClassModel *> *secondClassArr;

@property (nonatomic, strong) UIView *navTitleView;

@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation CategoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.firstClassArr = [NSMutableArray arrayWithCapacity:1];
    
    _oneView = [[OneView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 101, SCREEN_HEIGHT - TabbarHeight - NavigationBar_Bottom)];
    _oneView.clickBlck = ^(SecondClassModel * data) {
        if (![data isKindOfClass:SecondClassModel.class]) {
            return;
        }
        MXRoute(@"GoodsSortListVC", @{@"category_id":@(data._id)})
    };
    
    [self firstClass:^(id data) {
        FirstClassModel *model = [self.firstClassArr firstObject];
        [self secondClass:model._id];
    }];
    [self setUpNavTopView];
    [self fetchAppImgList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)fetchAppImgList {
    [self request:@"api/system/appImg/getAppImgList" param:@{@"img_type":@"02"}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *dataArr = dataDic[@"data"][@"appImgList"];
            [self.oneView configureCycleView:dataArr];
        }
    }];
}

- (void)firstClass:(FinishedBlock)block {
    [self request:@"api/shop/goods/getCategoryList"
            param:@{@"category_level":@"1"}
       completion:^(BOOL success, id object, NSString *error) {
        if(success) {
            NSArray *arr = [FirstClassModel modelListParseWithArray:object[@"data"][@"categoryList"]];
            if(arr.count > 0) {
                [self.firstClassArr addObjectsFromArray:arr];
            }
            [self refreshFirstClass];
        }
        Block_Exec(block,nil);
    }];
}

- (void)secondClass:(NSInteger)g_id {
    [self request:@"api/shop/goods/getCategoryList"
            param:@{@"category_level":@"2", @"parent_id":@(g_id)}
       completion:^(BOOL success, id object, NSString *error) {
        if(success) {
            NSArray *arr = [SecondClassModel modelListParseWithArray:object[@"data"][@"categoryList"]];
            self.secondClassArr = arr;
            self.oneView.dataArray = self.secondClassArr;
        }
    }];
}

- (void)refreshFirstClass {
    if(self.firstClassArr.count == 0) {
        return;
    }
    [[self.view viewWithTag:100010] removeFromSuperview];
    
    NSArray *arr = [self.firstClassArr valueForKeyPath:@"@unionOfObjects.categoryName"];
    LinkageMenuView *lkMenu = [[LinkageMenuView alloc] initWithFrame:CGRectMake(0, NavigationBar_Bottom,SCREEN_WIDTH , SCREEN_HEIGHT - TabbarHeight) WithMenu:arr andViews:@[_oneView]];
    lkMenu.backgroundColor = [UIColor moBackground];
    lkMenu.selectViewColor = [UIColor whiteColor];
    lkMenu.selectTextColor = [UIColor colorWithHexString:@"#16A5AF"];
    lkMenu.colorsArr = @[[UIColor whiteColor],[UIColor whiteColor]];
    lkMenu.tag = 100010;
    [self.view addSubview:lkMenu];
    lkMenu.clickBlock = ^(id data) {
        FirstClassModel *model = [self.firstClassArr safeObjectAtIndex:[data integerValue]-1];
        [self secondClass:model._id];
    };
}

- (void)setUpNavTopView {
    [self.view addSubview:self.navTitleView];
}

#pragma mark - init
- (UIView *)navTitleView {
    if (!_navTitleView) {
        _navTitleView = ({
            UIView *view = [UIView new];
            view.frame = CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom);
            view.backgroundColor = [UIColor whiteColor];
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateNormal];
            [leftBtn setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateHighlighted];
            leftBtn.imageView.tintColor = [UIColor blackColor];
            [leftBtn addAction:^(UIButton *btn) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            UIButton *messageBtn = [UIButton new];
            [messageBtn setImage:[UIImage imageNamed:@"导航栏消息"] forState:UIControlStateNormal];
            [messageBtn sizeToFit];
            [messageBtn addAction:^(UIButton *btn) {
                MXRoute(@"MessageCenterVC", nil);
            }];
            [view addSubview:leftBtn];
            [view addSubview:self.searchTF];
            [view addSubview:messageBtn];
            
            [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(30, 30));
                make.left.mas_equalTo(view.mas_left).offset(10);
                make.bottom.mas_equalTo(view.mas_bottom).offset(-5);
            }];
            
            [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(30);
                make.left.mas_equalTo(leftBtn.mas_right).offset(5);
                make.right.mas_equalTo(messageBtn.mas_left).offset(-10);
                make.centerY.mas_equalTo(leftBtn.mas_centerY);
            }];
            
            [messageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(messageBtn.size);
                make.right.mas_equalTo(view.mas_right).offset(-10);
                make.centerY.mas_equalTo(leftBtn.mas_centerY);
            }];
            [view addAction:^(UIView *view) {
                MXRoute(@"GoodsSortListVC", nil)
            }];
            view;
        });
    }
    return _navTitleView;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = [UITextField new];
        _searchTF.userInteractionEnabled = NO;
        [_searchTF setViewCornerRadius:5];
        _searchTF.font = [UIFont font15];
        _searchTF.backgroundColor = [UIColor colorWithHexString:@"#F6F6F9"];
        UIView *leftView = [UIView new];
        leftView.size = CGSizeMake(10, 30);
        
        UIView *rightView = [UIView new];
        rightView.size = CGSizeMake(40, 30);
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
        [leftBtn sizeToFit];
        leftBtn.x = 10;
        leftBtn.y = 5;
        [rightView addSubview:leftBtn];
        _searchTF.rightView = rightView;
        _searchTF.leftView = leftView;
        _searchTF.returnKeyType = UIReturnKeySearch;
        _searchTF.leftViewMode = UITextFieldViewModeAlways;
        _searchTF.rightViewMode = UITextFieldViewModeAlways;
        _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[@"服饰鞋靴"]
                                                                     colors:@[[UIColor colorWithHexString:@"#C1C1C1"]]
                                                                      fonts:@[[UIFont font14]]];
        _searchTF.attributedPlaceholder = att;
    }
    return _searchTF;
}

@end
