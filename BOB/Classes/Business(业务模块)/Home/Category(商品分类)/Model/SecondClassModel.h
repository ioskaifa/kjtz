//
//  SecondClassModel.h
//  RatelBrother
//
//  Created by AlphaGo on 2020/5/7.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface SecondClassModel : BaseObject
@property (nonatomic , assign) NSInteger              _id;
@property (nonatomic , copy) NSString              * categoryName;
@property (nonatomic , assign) NSInteger              categoryType;
@property (nonatomic , assign) NSInteger              superior;
@property (nonatomic , copy) NSString              * categoryImg;
@end

NS_ASSUME_NONNULL_END
