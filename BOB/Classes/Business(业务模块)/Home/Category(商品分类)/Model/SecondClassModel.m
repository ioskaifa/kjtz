//
//  SecondClassModel.m
//  RatelBrother
//
//  Created by AlphaGo on 2020/5/7.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SecondClassModel.h"

@implementation SecondClassModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"_id" : @"id",
             @"categoryName":@"category_name",
             @"categoryType":@"",
             @"categoryImg":@"category_icon"};
}
@end
