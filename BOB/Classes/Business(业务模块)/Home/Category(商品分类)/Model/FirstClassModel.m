//
//  FirstClassModel.m
//  RatelBrother
//
//  Created by AlphaGo on 2020/5/7.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "FirstClassModel.h"

@implementation FirstClassModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"_id" : @"id",
             @"categoryName":@"category_name",
             @"categoryType":@"",
             @"categoryImg":@"category_icon"};
}
@end
