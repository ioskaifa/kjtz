//
//  GoodsSortListVC.h
//  BOB
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsSortListVC : BaseViewController
///分类编号
@property (nonatomic, copy) NSString *category_id;
///聚划算状态 0-否  1-是
@property (nonatomic, copy) NSString *bargain_status;
///天天特卖状态 0-否  1-是
@property (nonatomic, copy) NSString *day_sale_status;
///热销美妆状态 0-否  1-是
@property (nonatomic, copy) NSString *product_status_new;
///品牌购状态 0-否  1-是
@property (nonatomic, copy) NSString *brand_buy_status;
///有好货状态 0-否  1-是
@property (nonatomic, copy) NSString *have_good_status;
///1SLA专区 0-否 1-是
@property (nonatomic, copy) NSString *one_sla_status;

@property (nonatomic, assign) BOOL isFree;

@property (nonatomic, copy) NSString *key_word;

@end

NS_ASSUME_NONNULL_END
