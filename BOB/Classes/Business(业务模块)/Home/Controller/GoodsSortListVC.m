//
//  GoodsSortListVC.m
//  BOB
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "GoodsSortListVC.h"
#import "HomeFlowLayout.h"
#import "HomeGoodsCell.h"
#import "GoodsListObj.h"
#import "TopSortView.h"

@interface GoodsSortListVC ()<UICollectionViewDelegate, UICollectionViewDataSource, UITextViewDelegate>

@property (nonatomic, strong) TopSortView *topSortView;

@property (nonatomic, assign) NSInteger page_num;

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) HomeFlowLayout *flowLayout;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UIView *navTitleView;

@property (nonatomic, strong) UITextField *searchTF;

@end

@implementation GoodsSortListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (NSDictionary *)paramDic {
    NSMutableDictionary *MDic = [NSMutableDictionary dictionaryWithDictionary:[self.topSortView sortType]];
    [MDic setValue:self.key_word?:@"" forKey:@"key_word"];
    [MDic setValue:self.category_id?:@"" forKey:@"category_id"];
    if (![StringUtil isEmpty:self.bargain_status]) {
        [MDic setValue:@"1" forKey:@"bargain_status"];
    }
    if (![StringUtil isEmpty:self.day_sale_status]) {
        [MDic setValue:@"1" forKey:@"day_sale_status"];
    }
    
    if (![StringUtil isEmpty:self.product_status_new]) {
        [MDic setValue:@"1" forKey:@"new_product_status"];
    }
    
    if (![StringUtil isEmpty:self.brand_buy_status]) {
        [MDic setValue:@"1" forKey:@"brand_buy_status"];
    }
    
    if (![StringUtil isEmpty:self.have_good_status]) {
        [MDic setValue:@"1" forKey:@"have_good_status"];
    }
    
    if (![StringUtil isEmpty:self.one_sla_status]) {
        [MDic setValue:@"1" forKey:@"one_sla_status"];
    }
    
    [MDic setValue:@(self.page_num) forKey:@"page_num"];
    return MDic;
}

- (NSString *)urlString {
    if (self.isFree) {
        return @"api/freeshop/goods/getKeyWordGoodsList";
    } else {
        return @"api/shop/goods/getKeyWordGoodsList";
    }
}

- (void)fetchData {
    [self request:[self urlString]
            param:[self paramDic]
       completion:^(BOOL success, id object, NSString *error) {
        [self colViewEndRefreshing];
        if (success) {
            if (self.page_num == 0) {
                [self.dataSourceMArr removeAllObjects];
            }
            NSDictionary *dataDic = (NSDictionary *)object[@"data"];
            NSArray *dataArr = [GoodsListObj modelListParseWithArray:dataDic[@"goodsList"]];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            [self.colView reloadData];
        }
    }];
}

- (void)colViewEndRefreshing {
    [self.colView.mj_header endRefreshing];
    [self.colView.mj_footer endRefreshing];
}

#pragma mark - Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = HomeGoodsCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    GoodsListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:[GoodsListObj class]]) {
        return;
    }
    if (![cell isKindOfClass:[HomeGoodsCell class]]) {
        return;
    }
    
    HomeGoodsCell *listCell = (HomeGoodsCell *)cell;
    [listCell configureView:obj];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    GoodsListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    if (self.isFree) {
        MXRoute(@"GoodInfoVC", (@{@"goods_id":obj.ID, @"isFree":@(1)}))
    } else {
        MXRoute(@"GoodInfoVC", @{@"goods_id":obj.ID})
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return CGSizeZero;
    }
    return CGSizeMake((SCREEN_WIDTH - 40) / 2.0, [HomeGoodsCell viewHeight]);
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (self.isFree) {
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }
    return YES;
}

- (void)textFieldTextChange:(UITextField *)textField {
    if (textField == self.searchTF) {
        self.key_word = [StringUtil trim:self.searchTF.text];
        self.page_num = 0;
        [self.dataSourceMArr removeAllObjects];
        [self fetchData];
    }
}

#pragma mark - InitUI
- (void)createUI {
    self.view.backgroundColor = [UIColor moBackground];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self.view addSubview:layout];
    [layout addSubview:self.navTitleView];
    [layout addSubview:self.topSortView];
    [layout addSubview:self.colView];
}

#pragma mark - Init
- (TopSortView *)topSortView {
    if (!_topSortView) {
        _topSortView = [TopSortView new];
        _topSortView.size = CGSizeMake(SCREEN_WIDTH, [TopSortView viewHeight]);
        _topSortView.mySize = _topSortView.size;
        _topSortView.myTop = 1;
        _topSortView.myLeft = 0;
        @weakify(self)
        _topSortView.block = ^(NSDictionary *data) {
            @strongify(self)
            self.page_num = 0;
            [self fetchData];
        };
    }
    return _topSortView;
}

- (UICollectionView *)colView {
    if (!_colView) {
        _colView = ({
            UICollectionView *object = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
            object.backgroundColor = [UIColor moBackground];
            object.delegate = self;
            object.dataSource = self;
            Class cls = HomeGoodsCell.class;
            [object registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
//            AdjustTableBehavior(object);
            object.myTop = 10;
            object.myLeft = 0;
            object.myRight = 0;
            object.weight = 1;
            @weakify(self);
            object.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
                @strongify(self);
                self.page_num = 0;
                [self fetchData];
            }];
            object.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
                @strongify(self);
                self.page_num++;
                [self fetchData];
            }];
            object;
        });
    }
    return _colView;
}

- (HomeFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = ({
            HomeFlowLayout *object = [HomeFlowLayout new];
            object.minimumLineSpacing = 10;
            object.minimumInteritemSpacing = 10;
            object.sectionInset = UIEdgeInsetsMake(0, 15, 10, 15);
            object;
        });
    }
    return _flowLayout;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (UIView *)navTitleView {
    if (!_navTitleView) {
        _navTitleView = ({
            UIView *view = [UIView new];
            view.frame = CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom);
            view.backgroundColor = [UIColor whiteColor];
            UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [leftBtn setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateNormal];
            [leftBtn setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateHighlighted];
            leftBtn.imageView.tintColor = [UIColor blackColor];
            [leftBtn addAction:^(UIButton *btn) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            UIButton *messageBtn = [UIButton new];
            [messageBtn setImage:[UIImage imageNamed:@"导航栏消息"] forState:UIControlStateNormal];
            [messageBtn sizeToFit];
            [messageBtn addAction:^(UIButton *btn) {
                MXRoute(@"MessageCenterVC", nil);
            }];
            [view addSubview:leftBtn];
            [view addSubview:self.searchTF];
//            [view addSubview:messageBtn];
            
            [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(30, 30));
                make.left.mas_equalTo(view.mas_left).offset(10);
                make.bottom.mas_equalTo(view.mas_bottom).offset(-5);
            }];
            
            [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(30);
                make.left.mas_equalTo(leftBtn.mas_right).offset(5);
                make.right.mas_equalTo(view.mas_right).offset(-10);
                make.centerY.mas_equalTo(leftBtn.mas_centerY);
            }];
            
//            [messageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.size.mas_equalTo(messageBtn.size);
//                make.right.mas_equalTo(view.mas_right).offset(-10);
//                make.centerY.mas_equalTo(leftBtn.mas_centerY);
//            }];
            
            view;
        });
    }
    return _navTitleView;
}

- (UITextField *)searchTF {
    if (!_searchTF) {
        _searchTF = [UITextField new];
        _searchTF.delegate = self;
        [_searchTF addTarget:self action:@selector(textFieldTextChange:) forControlEvents:UIControlEventEditingChanged];
        [_searchTF setViewCornerRadius:5];
        _searchTF.font = [UIFont font15];
        _searchTF.backgroundColor = [UIColor colorWithHexString:@"#F6F6F9"];
        UIView *leftView = [UIView new];
        leftView.size = CGSizeMake(10, 30);
        
//        UIView *rightView = [UIView new];
//        rightView.size = CGSizeMake(40, 30);
//        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [leftBtn setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
//        [leftBtn sizeToFit];
//        leftBtn.x = 10;
//        leftBtn.y = 5;
//        [rightView addSubview:leftBtn];
//        _searchTF.rightView = rightView;
        _searchTF.leftView = leftView;
        _searchTF.returnKeyType = UIReturnKeySearch;
        _searchTF.leftViewMode = UITextFieldViewModeAlways;
        _searchTF.rightViewMode = UITextFieldViewModeAlways;
        _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        NSString *string = @"服饰鞋靴";
        if (![StringUtil isEmpty:self.key_word]) {
            string = self.key_word;
        }
        NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[string]
                                                                     colors:@[[UIColor colorWithHexString:@"#C1C1C1"]]
                                                                      fonts:@[[UIFont font14]]];
        _searchTF.attributedPlaceholder = att;
    }
    return _searchTF;
}

@end
