//
//  HomeVC.m
//  BOB
//
//  Created by mac on 2019/6/24.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "HomeVC.h"
#import "HomeHeaderView.h"
#import "HomeFlowLayout.h"
#import "HomeGoodsCell.h"
#import "GoodsListObj.h"
#import "CategoryObj.h"
#import "GoodsAdvertiseCoverObj.h"
#import "NoviceGiftBagView.h"

@interface HomeVC ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) HomeHeaderView *headerView;

@property (nonatomic, strong) HomeFlowLayout *flowLayout;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) NSMutableDictionary *goodsAdvertiseMDic;

@property (nonatomic, copy) NSString *seat;

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self.colView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!UDetail.user.give_status) {
        NoviceGiftBagView *giftView = [NoviceGiftBagView new];
        giftView.size = [NoviceGiftBagView viewSize];
        [MXCustomAlertVC showInViewController:MoApp.mallVC contentView:giftView];
    }
}

- (void)fetchData {
    if ([self.seat isEqualToString:@""]) {
        [self.dataSourceMArr removeAllObjects];
    }
    [self request:@"api/shop/goods/getCategoryGoodsList"
            param:@{@"last_id":self.seat}
       completion:^(BOOL success, id object, NSString *error) {
        [self colViewEndRefreshing];
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object[@"data"];
            NSArray *dataArr = [GoodsListObj modelListParseWithArray:dataDic[@"goodsList"]];
            [self.dataSourceMArr addObjectsFromArray:dataArr];
            GoodsListObj *lastObj = (GoodsListObj *)[self.dataSourceMArr lastObject];
            if (lastObj) {
                self.seat = lastObj.order_num;
            }
            [self.colView reloadData];
        }
    }];
    if ([@"" isEqualToString:self.seat]) {
        [self fetchLatestData];
    }
}

///下拉
- (void)fetchLatestData {
    //首页轮播图
    [self request:@"api/system/appImg/getAppImgList"
            param:@{@"img_type":@"01"}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *dataArr = dataDic[@"data"][@"appImgList"];
            [self.headerView configureCycleScrollView:dataArr];
        }
    }];
    //首页分类
    [self request:@"api/shop/goods/getCategoryList"
            param:@{@"category_level":@"1",
                    @"is_tab":@"1"}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            NSDictionary *dataDic = (NSDictionary *)object;
            NSArray *tempArr = dataDic[@"data"][@"categoryList"];
            NSArray *dataArr = [CategoryObj modelListParseWithArray:tempArr];
            [self.headerView configureCategoryView:dataArr];
            self.headerView.height = [self.headerView viewHeight];
            [self.colView reloadData];
        }
    }];
    
    //首页分类
    [self request:@"api/freeshop/goods/getIndexHotGoodsList"
            param:@{}
       completion:^(BOOL success, id object, NSString *error) {
        if (success) {            
            NSArray *dataArr = [GoodsListObj modelListParseWithArray:object[@"data"][@"goodsList"]];
            [self.headerView configureFreeShopListView:dataArr];
        }
    }];
    
    [self fetchAdvertiseData];
}

- (void)fetchAdvertiseData {
    [self.goodsAdvertiseMDic removeAllObjects];
    NSArray *paramArr = @[@"01", @"02", @"03", @"04", @"05", @"07"];
    dispatch_group_t group = dispatch_group_create();
    for (NSString *param in paramArr) {
        dispatch_group_enter(group);
        [self request:@"api/shop/goods/getGoodsAdvertiseCoverList"
                param:@{@"advertise_type":param}
           completion:^(BOOL success, id object, NSString *error) {
            dispatch_group_leave(group);
            if (success) {
                NSDictionary *dataDic = (NSDictionary *)object;
                NSArray *tempArr = dataDic[@"data"][@"goodsAdvertiseCoverList"];
                NSArray *dataArr = [GoodsAdvertiseCoverObj modelListParseWithArray:tempArr];
                if (dataArr) {
                    [self.goodsAdvertiseMDic setValue:dataArr
                                               forKey:[GoodsAdvertiseCoverObj advertise_typeToCustom:param]];
                }
            }
        }];
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self.headerView configureAdvertise:self.goodsAdvertiseMDic];
        self.headerView.height = [self.headerView viewHeight];
        [self.colView reloadData];
    });
}

- (void)colViewEndRefreshing {
    [self.colView.mj_header endRefreshing];
    [self.colView.mj_footer endRefreshing];
}

#pragma mark - 偶数
+ (BOOL)isEvenNumber:(NSInteger)index {
    if (index % 2 == 0) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = HomeGoodsCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    GoodsListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:[GoodsListObj class]]) {
        return;
    }
    if (![cell isKindOfClass:[HomeGoodsCell class]]) {
        return;
    }
    
    HomeGoodsCell *listCell = (HomeGoodsCell *)cell;
    [listCell configureView:obj];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        return self.headerView;
    }
    return [UICollectionReusableView new];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    GoodsListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    MXRoute(@"GoodInfoVC", @{@"goods_id":obj.ID})
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return CGSizeZero;
    }
    return CGSizeMake((SCREEN_WIDTH - 40) / 2.0, [HomeGoodsCell viewHeight]);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {    
    return CGSizeMake(SCREEN_WIDTH, [HomeHeaderView viewHeight]);
}

#pragma mark - InitUI
- (void)createUI {
    self.seat = @"";
    [self.view addSubview:self.colView];
    [self layoutUI];
}

- (void)layoutUI {
    [self.colView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (UICollectionView *)colView {
    if (!_colView) {
        _colView = ({
            UICollectionView *object = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
            object.showsVerticalScrollIndicator = NO;
            object.showsHorizontalScrollIndicator = NO;
            object.backgroundColor = [UIColor moBackground];
            object.delegate = self;
            object.dataSource = self;
            Class cls = HomeGoodsCell.class;
            [object registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
            cls = HomeHeaderView.class;
            [object registerClass:cls forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
              withReuseIdentifier:NSStringFromClass(cls)];
            AdjustTableBehavior(object);
            @weakify(self);
            object.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
                @strongify(self);
                self.seat = @"";
                [self fetchData];
            }];
            object.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
                @strongify(self);
                [self fetchData];
            }];
            object;
        });
    }
    return _colView;
}

- (HomeFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = ({
            HomeFlowLayout *object = [HomeFlowLayout new];
            object.minimumLineSpacing = 10;
            object.minimumInteritemSpacing = 10;
            object.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
            object;
        });
    }
    return _flowLayout;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (HomeHeaderView *)headerView {
    if (!_headerView) {
        Class cls = HomeHeaderView.class;
        HomeHeaderView *view = [self.colView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                withReuseIdentifier:NSStringFromClass(cls)
                                                                       forIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        view.size = CGSizeMake(SCREEN_WIDTH, [view viewHeight]);
        _headerView = view;
    }
    return _headerView;
}

- (NSMutableDictionary *)goodsAdvertiseMDic {
    if (!_goodsAdvertiseMDic) {
        _goodsAdvertiseMDic = [NSMutableDictionary dictionary];
    }
    return _goodsAdvertiseMDic;
}

@end
