//
//  TopSortView.h
//  BOB
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopSortView : UIView

@property (nonatomic, copy) FinishedBlock block;

+ (CGFloat)viewHeight;

///获取当前排序类型
///sort_type 01-综合默认排序（后台指定的排序） 02—销量 03—价格
///asc_type  1-升序 2-降序
///eg: @{@"sort_type:@"01", @"asc_type":@"1"}
- (NSDictionary *)sortType;

@end

NS_ASSUME_NONNULL_END
