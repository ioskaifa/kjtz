//
//  GoodListView.h
//  BOB
//
//  Created by mac on 2020/9/10.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodListView : UIView

- (void)configureView:(GoodsListObj *)obj;

@end

NS_ASSUME_NONNULL_END
