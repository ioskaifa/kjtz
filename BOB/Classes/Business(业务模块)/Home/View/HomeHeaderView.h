//
//  HomeHeaderView.h
//  BOB
//
//  Created by mac on 2020/9/8.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeHeaderView : UICollectionReusableView

+ (CGFloat)viewHeight;

- (CGFloat)viewHeight;

- (void)configureView;

- (void)configureCycleScrollView:(NSArray *)dataArr;

- (void)configureCategoryView:(NSArray *)dataArr;

- (void)configureAdvertise:(NSDictionary *)dataDic;

- (void)configureFreeShopListView:(NSArray *)dataArr;

@end

NS_ASSUME_NONNULL_END
