//
//  HomeHeaderSubView.m
//  BOB
//
//  Created by mac on 2020/9/9.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "HomeHeaderSubView.h"
#import "GoodsAdvertiseCoverObj.h"

@interface HomeHeaderSubView ()

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSArray *dataArr;

@property (nonatomic, strong) UIImageView *leftImgView;

@property (nonatomic, strong) UIImageView *rightImgView;

@end

@implementation HomeHeaderSubView

#pragma mark - Lifecycle
- (instancetype)initWithTitle:(NSString *)title {
    if (self = [super init]) {
        self.title = title;        
        [self createUI];
    }
    return self;
}

- (void)configureView:(NSArray *)dataArr {
    if (![dataArr isKindOfClass:NSArray.class]) {
        return;
    }
    self.dataArr = dataArr;
    NSArray *imgViewArr = @[self.leftImgView, self.rightImgView];
    for (NSInteger i = 0; i < dataArr.count; i++) {
        GoodsAdvertiseCoverObj *obj = dataArr[i];
        if (![obj isKindOfClass:GoodsAdvertiseCoverObj.class]) {
            continue;
        }
        if (i >= imgViewArr.count) {
            continue;
        }
        UIImageView *imgView = imgViewArr[i];
        NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, obj.advertise_cover);
        [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    }
}

- (void)imgViewClick:(UIView *)sender {
    if (![sender isKindOfClass:UIView.class]) {
        return;
    }
    if (sender.tag >= self.dataArr.count) {
        return;
    }
    GoodsAdvertiseCoverObj *obj = self.dataArr[sender.tag];
    
}

#pragma mark - InitUI
- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 10;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    [self addSubview:layout];
//    if ([self.title isEqualToString:@"聚划算"]) {
//        [self initJuhuasuanUI:layout];
//    } else if ([self.title isEqualToString:@"直播"]) {
//        [self initLive:layout];
//    } else if ([self.title isEqualToString:@"有好货"]) {
//        [self initGoods:layout];
//    } else if ([self.title isEqualToString:@"品牌购"]) {
//        [self initBrand:layout];
//    }  else {
        [self initNormalUI:layout title:self.title];
//    }
    MyLinearLayout *dataLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    dataLayout.myTop = 5;
    dataLayout.weight = 1;
    dataLayout.myLeft = 0;
    dataLayout.myRight = 0;
    dataLayout.subviewHSpace = 10;
    dataLayout.gravity = MyGravity_Horz_Stretch;
    [layout addSubview:dataLayout];
    
    [dataLayout addSubview:self.leftImgView];
//    [dataLayout addSubview:self.rightImgView];
}

#pragma mark - 聚划算
- (void)initJuhuasuanUI:(MyBaseLayout *)layout {
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"聚划算"]];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    [layout addSubview:imgView];
}

#pragma mark - 直播
- (void)initLive:(MyBaseLayout *)layout {
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 0;
    subLayout.myLeft = 0;
    subLayout.myHeight = 15;
    subLayout.myWidth = MyLayoutSize.wrap;
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"首页直播"]];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    [subLayout addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Live"]];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myLeft = 10;
    imgView.myCenterY = 2;
    [subLayout addSubview:imgView];
    [layout addSubview:subLayout];
    
    UILabel *lbl = [UILabel new];
    lbl.text = @"看直播推好物";
    lbl.textColor = [UIColor colorWithHexString:@"#00DBD9"];
    lbl.font = [UIFont font12];
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 5;
    [layout addSubview:lbl];
}

#pragma mark - 有好货
- (void)initGoods:(MyBaseLayout *)layout {
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 0;
    subLayout.myLeft = 0;
    subLayout.myHeight = 20;
    subLayout.myWidth = MyLayoutSize.wrap;
    
    UILabel *lbl = [UILabel new];
    lbl.text = @"有好货";
    lbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    lbl.font = [UIFont boldFont15];
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 0;
    lbl.myCenterY = 0;
    [subLayout addSubview:lbl];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Live_有好货"]];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myLeft = 10;
    imgView.myCenterY = 2;
    [subLayout addSubview:imgView];
    [layout addSubview:subLayout];
    
    lbl = [UILabel new];
    lbl.text = @"推荐品质好物";
    lbl.textColor = [UIColor colorWithHexString:@"#FF4757"];
    lbl.font = [UIFont font12];
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 5;
    [layout addSubview:lbl];
}

#pragma mark - 品牌购
- (void)initBrand:(MyBaseLayout *)layout {
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 0;
    subLayout.myLeft = 0;
    subLayout.myHeight = 20;
    subLayout.myWidth = MyLayoutSize.wrap;
    
    UILabel *lbl = [UILabel new];
    lbl.text = @"品牌购";
    lbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    lbl.font = [UIFont boldFont15];
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myLeft = 0;
    lbl.myCenterY = 0;
    [subLayout addSubview:lbl];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dapai"]];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myLeft = 10;
    imgView.myCenterY = 2;
    [subLayout addSubview:imgView];
    [layout addSubview:subLayout];
}

- (void)initNormalUI:(MyBaseLayout *)layout title:(NSString *)title {
    UILabel *lbl = [UILabel new];
    lbl.text = title;
    lbl.textColor = [UIColor colorWithHexString:@"#1A1A1A"];
    lbl.font = [UIFont boldFont16];
    if ([title containsString:@"1SLA专区"]) {
        NSArray *txtArr = @[@"1", @"SLA专区"];
        NSArray *colorArr = @[[UIColor redColor], [UIColor blackColor]];
        NSArray *fontArr = @[[UIFont boldFont20], [UIFont boldFont16]];
        NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
        lbl.attributedText = att;
    }
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myTop = 0;
    [layout addSubview:lbl];
}

#pragma mark - Init
- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        _leftImgView = [UIImageView new];
        [_leftImgView setViewCornerRadius:7];
        _leftImgView.backgroundColor = [UIColor moBackground];
        _leftImgView.myWidth = MyLayoutSize.wrap;
        _leftImgView.myHeight = MyLayoutSize.fill;
        _leftImgView.contentMode = UIViewContentModeScaleToFill;
        _leftImgView.tag = 0;
//        @weakify(self)
//        [_leftImgView addAction:^(UIView *view) {
//            @strongify(self)
//            [self imgViewClick:view];
//        }];
    }
    return _leftImgView;
}

- (UIImageView *)rightImgView {
    if (!_rightImgView) {
        _rightImgView = [UIImageView new];
        [_rightImgView setViewCornerRadius:7];
        _rightImgView.backgroundColor = [UIColor moBackground];
        _rightImgView.myWidth = MyLayoutSize.wrap;
        _rightImgView.myHeight = MyLayoutSize.fill;
        _rightImgView.contentMode = UIViewContentModeScaleAspectFill;
        _rightImgView.tag = 1;
//        @weakify(self)
//        [_rightImgView addAction:^(UIView *view) {
//            @strongify(self)
//            [self imgViewClick:view];
//        }];
    }
    return _rightImgView;
}

@end
