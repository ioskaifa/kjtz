//
//  HomeHeaderView.m
//  BOB
//
//  Created by mac on 2020/9/8.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "HomeHeaderView.h"
#import "GKCycleScrollView.h"
#import "HomeHeaderSubView.h"
#import "CategoryObj.h"
#import "UIButton+WebCache.h"
#import "UIImage+Color.h"
#import "HomeFreeShopListView.h"

@interface HomeHeaderView ()<GKCycleScrollViewDataSource, GKCycleScrollViewDelegate>

@property (nonatomic, strong) MyBaseLayout *baseLayout;

@property (nonatomic, strong) SPButton *searchBtn;
///轮播图
@property (nonatomic, strong) GKCycleScrollView *cycleScrollView;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, strong) MyTableLayout *categoryLayout;

@property (nonatomic, strong) NSMutableArray *categoryMArr;
///聚划算
@property (nonatomic, strong) HomeHeaderSubView *juhuasuanView;
///直播
@property (nonatomic, strong) HomeHeaderSubView *liveView;
///天天特卖
@property (nonatomic, strong) HomeHeaderSubView *saleView;
///有好货
@property (nonatomic, strong) HomeHeaderSubView *goodView;
///热销美妆
@property (nonatomic, strong) HomeHeaderSubView *newView;
///品牌购
@property (nonatomic, strong) HomeHeaderSubView *bandView;
///免单商品
@property (nonatomic, strong) HomeFreeShopListView *freeShopListView;

@end

@implementation HomeHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

#pragma mark - Public Method
+ (CGFloat)viewHeight {
    return 888;
}

- (CGFloat)viewHeight {
    [self.baseLayout layoutIfNeeded];
    return self.baseLayout.height;
}

- (void)configureView {
    
}

- (void)configureCycleScrollView:(NSArray *)dataArr {
    if (![dataArr isKindOfClass:NSArray.class]) {
        return;
    }
    [self.dataSourceMArr removeAllObjects];
    [self.dataSourceMArr addObjectsFromArray:dataArr];
    [self.cycleScrollView reloadData];
}

- (void)configureCategoryView:(NSArray *)dataArr {
    if (![dataArr isKindOfClass:NSArray.class]) {
        return;
    }
    
    [self.categoryMArr removeAllObjects];
    [self.categoryMArr addObjectsFromArray:dataArr];
    ///全部频道
    [self.categoryMArr addObject:[CategoryObj allCategoryObj]];
    [self reloadCategoryLayout];
}

- (void)configureAdvertise:(NSDictionary *)dataDic {
    if (![dataDic isKindOfClass:NSDictionary.class]) {
        return;
    }
    [self.juhuasuanView configureView:dataDic[@"聚划算"]];
    [self.liveView configureView:dataDic[@"1SLA专区"]];
    [self.saleView configureView:dataDic[@"天天特卖"]];
    [self.goodView configureView:dataDic[@"有好货"]];
    [self.newView configureView:dataDic[@"热销美妆"]];
    [self.bandView configureView:dataDic[@"品牌购"]];
}

- (void)configureFreeShopListView:(NSArray *)dataArr {
    if (![dataArr isKindOfClass:NSArray.class]) {
        return;
    }
    [self.freeShopListView configureView:dataArr];
}

#pragma mark - Delegate
#pragma mark GKCycleScrollViewDataSource
- (NSInteger)numberOfCellsInCycleScrollView:(GKCycleScrollView *)cycleScrollView {
    return self.dataSourceMArr.count;
}

- (GKCycleScrollViewCell *)cycleScrollView:(GKCycleScrollView *)cycleScrollView cellForViewAtIndex:(NSInteger)index {
    GKCycleScrollViewCell *cell = [cycleScrollView dequeueReusableCell];
    if (!cell) {
        cell = [GKCycleScrollViewCell new];
    }
    if (index < self.dataSourceMArr.count) {
        NSDictionary *dict = self.dataSourceMArr[index];
        NSString *img_url = dict[@"img_url"];
        if ([img_url isKindOfClass:NSString.class]) {
            img_url = StrF(@"%@/%@", UDetail.user.qiniu_domain, img_url);
            [cell.imageView sd_setImageWithURL:[NSURL URLWithString:img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                
            }];
        }
    }
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    return cell;
}

- (void)cycleScrollView:(GKCycleScrollView *)cycleScrollView didSelectCellAtIndex:(NSInteger)index {
    if (index >= self.dataSourceMArr.count) {
        return;
    }
    NSDictionary *dataDic = self.dataSourceMArr[index];
    if (![dataDic isKindOfClass:NSDictionary.class]) {
        return;
    }
    NSString *goods_id = [dataDic[@"goods_id"] description];
    if ([StringUtil isEmpty:goods_id]) {
        return;
    }
    NSString *mall_type = dataDic[@"mall_type"];
    if ([@"01" isEqualToString:mall_type]) {
        MXRoute(@"GoodInfoVC", (@{@"goods_id":goods_id, @"isFree":@(0)}))
    } else if ([@"02" isEqualToString:mall_type]) {
        MXRoute(@"GoodInfoVC", (@{@"goods_id":goods_id, @"isFree":@(1)}))
    }
}

#pragma mark - InitUI
- (void)createUI {
    [self addSubview:self.baseLayout];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];    
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    [self.baseLayout addSubview:layout];
    [layout addSubview:[self messageLayout]];
    [layout addSubview:[self searchLayout]];
    [layout addSubview:self.cycleScrollView];
    [layout addSubview:self.categoryLayout];
    [self reloadCategoryLayout];
    [layout addSubview:[self invitationView]];
//    [layout addSubview:[UIView gapLine]];
//    [layout addSubview:self.freeShopListView];
    [layout addSubview:[UIView gapLine]];
    [layout addSubview:[self juhuasuanLayout]];
}

#pragma mark - Init
- (MyBaseLayout *)baseLayout {
    if (!_baseLayout) {
        _baseLayout = [MyFrameLayout new];
        _baseLayout.myTop = 0;
        _baseLayout.myLeft = 0;
        _baseLayout.myRight = 0;
        _baseLayout.myHeight = MyLayoutSize.wrap;
        _baseLayout.backgroundColor = [UIColor whiteColor];
    }
    return _baseLayout;
}

#pragma mark - 消息
- (MyBaseLayout *)messageLayout {
    SPButton *appNameBtn = [SPButton new];
    appNameBtn.titleLabel.font = [UIFont boldFont20];
    [appNameBtn setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
    [appNameBtn setTitle:[DeviceManager getAppName] forState:UIControlStateNormal];
//    [appNameBtn setImage:[UIImage imageNamed:@"CSLA_logo"] forState:UIControlStateNormal];
    [appNameBtn sizeToFit];
    appNameBtn.mySize = appNameBtn.size;
    appNameBtn.myCenterY = 0;
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 20 + safeAreaInset().top;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.myHeight = MyLayoutSize.wrap;
    [layout addSubview:appNameBtn];
    [layout addSubview:[UIView placeholderHorzView]];
    UIButton *messageBtn = [UIButton new];
    [messageBtn setImage:[UIImage imageNamed:@"导航栏消息"] forState:UIControlStateNormal];
    [messageBtn sizeToFit];
    [messageBtn addAction:^(UIButton *btn) {
        MXRoute(@"MessageCenterVC", nil);        
    }];
    [layout addSubview:messageBtn];
    return layout;
}

#pragma mark - 搜索栏
- (MyBaseLayout *)searchLayout {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor colorWithHexString:@"#F6F6F9"];
    layout.myTop = 10;
    layout.myLeft = 15;
    layout.myRight = 15;
    layout.height = 32;
    layout.myHeight = layout.height;
    [layout setViewCornerRadius:5];
    [layout addSubview:self.searchBtn];
    
    UIView *view = [UIView new];
    view.weight = 1;
    view.myCenterY = 0;
    [layout addSubview:view];
        
    UIButton *btn = [UIButton new];
    btn.userInteractionEnabled = NO;
    btn.size = CGSizeMake(70, layout.height);
    btn.titleLabel.font = [UIFont font14];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIImage* image = [UIImage gradientColorImageFromColors:@[[UIColor colorWithHexString:@"#7F7F8E"],[UIColor colorWithHexString:@"#3B3B42"]] gradientType:GradientTypeLeftToRight imgSize:btn.size];
    [btn setBackgroundImage:image forState:UIControlStateNormal];
    [btn setTitle:@"搜索" forState:UIControlStateNormal];
    [layout addSubview:btn];
    [layout addAction:^(UIView *view) {
        MXRoute(@"GoodsSortListVC", nil)
    }];
    return layout;
}

- (SPButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = ({
            SPButton *object = [SPButton new];
            object.userInteractionEnabled = NO;
            object.imagePosition = SPButtonImagePositionLeft;
            object.imageTitleSpace = 20;
            [object setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font14];
            [object setTitleColor:[UIColor colorWithHexString:@"#C1C1C1"] forState:UIControlStateNormal];
            [object setTitle:@"无线耳机、洗衣液低至5折" forState:UIControlStateNormal];
            [object sizeToFit];
            object.mySize = object.size;
            object.myLeft = 15;
            object.myCenterY = 0;
            object;
        });
    }
    return _searchBtn;
}
#pragma mark - 轮播图
- (GKCycleScrollView *)cycleScrollView {
    if (!_cycleScrollView) {
        _cycleScrollView.height = 150;
        _cycleScrollView = [[GKCycleScrollView alloc] initWithFrame:CGRectMake(15, 0, SCREEN_WIDTH - 30, 150)];
        [_cycleScrollView setViewCornerRadius:5];
        _cycleScrollView.pageControl = self.pageControl;
        _cycleScrollView.isAutoScroll = YES;
        _cycleScrollView.dataSource = self;
        _cycleScrollView.delegate = self;
        _cycleScrollView.myTop = 15;
        _cycleScrollView.myLeft = 15;
        _cycleScrollView.myRight = 15;
        _cycleScrollView.myBottom = 15;
        _cycleScrollView.myHeight = _cycleScrollView.height;
        [_cycleScrollView addSubview:self.pageControl];
        _cycleScrollView.backgroundColor = [UIColor moBackground];
    }
    return _cycleScrollView;
}

- (UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [UIPageControl new];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.numberOfPages = self.dataSourceMArr.count;
        _pageControl.frame = CGRectMake(20, self.cycleScrollView.height - 20, SCREEN_WIDTH - 40 - 30, 20);
    }
    return _pageControl;
}

#pragma mark - 类别
- (void)categoryLayoutClick:(UIView *)layout {
    if (layout.tag >= self.categoryMArr.count) {
        return;
    }
    CategoryObj *obj = self.categoryMArr[layout.tag];
    if (![obj isKindOfClass:CategoryObj.class]) {
        return;
    }
    if ([@"全部频道" isEqualToString:obj.category_name]) {
//        if ([MoApp.mallVC isKindOfClass:UITabBarController.class]) {
//            if (MoApp.mallVC.selectedIndex == 1) {
//                [self.viewController.navigationController popToRootViewControllerAnimated:YES];
//            } else {
//                [self.viewController.navigationController popToRootViewControllerAnimated:NO];
//                MoApp.mallVC.selectedIndex = 1;
//            }
//        }
        MXRoute(@"CategoryVC", nil)
        return;
    }
    MXRoute(@"GoodsSortListVC", @{@"category_id":obj.ID})
}


- (void)reloadCategoryLayout {
    if (self.categoryMArr.count == 0) {
        self.categoryLayout.visibility = MyVisibility_Gone;
    } else {
        self.categoryLayout.visibility = MyVisibility_Visible;
    }
    NSInteger countOfRow = self.categoryLayout.countOfRow;
    for (NSInteger i = countOfRow - 1; i >= 0; i--) {
        [self.categoryLayout removeRowAt:i];
    }
    if (self.categoryMArr.count == 0) {
        return;
    }
    CGFloat btnWidth = (SCREEN_WIDTH - 30 - 10*5)/5;
    [self.categoryLayout addRow:MyLayoutSize.wrap colSize:MyLayoutSize.wrap];
    for (NSInteger i = 0; i < self.categoryMArr.count; i++) {
        CategoryObj *obj = self.categoryMArr[i];
        if (![obj isKindOfClass:CategoryObj.class]) {
            continue;
        }
        MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
        layout.tag = i;
        layout.mySize = CGSizeMake(btnWidth, btnWidth);
        UIImageView *imgView = [UIImageView new];
        imgView.mySize = CGSizeMake(50, 50);
        imgView.myCenterX = 0;
        NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, obj.category_icon);
        if ([@"全部频道" isEqualToString:obj.category_name]) {
            imgView.image = [UIImage imageNamed:@"more"];
        } else {
            [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
        }
        
        UILabel *lbl = [UILabel new];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.font = [UIFont boldFont12];
        lbl.textColor = [UIColor blackColor];
        lbl.text = obj.category_name;
        [lbl sizeToFit];
        lbl.mySize = lbl.size;
        lbl.myCenterX = 0;
        
        [layout addSubview:imgView];
        [layout addSubview:[UIView placeholderVertView]];
        [layout addSubview:lbl];
                
        [self.categoryLayout addSubview:layout];
        if (i == 0 || i == 5) {
            layout.myLeft = 10;
        }
        if ((i+1) % 5 == 0) {
            [self.categoryLayout addRow:MyLayoutSize.wrap colSize:MyLayoutSize.wrap];
        }
        @weakify(self)
        [layout addAction:^(UIView *view) {
            @strongify(self)
            [self categoryLayoutClick:view];
        }];
    }
}

- (MyTableLayout *)categoryLayout {
    if (!_categoryLayout) {
        _categoryLayout = ({
            MyTableLayout *layout = [MyTableLayout tableLayoutWithOrientation:MyOrientation_Vert];
            layout.myBottom = 15;
            layout.myLeft = 15;
            layout.myRight = 15;
            layout.myHeight = MyLayoutSize.wrap;
            layout.subviewVSpace = 10;
            layout.subviewHSpace = 10;
            layout;
        });
    }
    return _categoryLayout;
}
    
#pragma mark - 邀请好友
- (UIView *)invitationView {
    UIImageView *imgView = [UIImageView new];
    UIImage *img = [UIImage imageNamed:@"邀请好友banner"];
    imgView.image = img;
    CGSize size = img.size;
    CGFloat width = SCREEN_WIDTH - 30;
    imgView.size = CGSizeMake(width, width*size.height/size.width);
    imgView.mySize = imgView.size;
    imgView.myLeft = 15;
    imgView.myBottom = 15;
    [imgView addAction:^(UIView *view) {
        MXRoute(@"InvitationExVC", nil)
    }];
    return imgView;
}

- (HomeHeaderSubView *)juhuasuanView {
    if (!_juhuasuanView) {
        _juhuasuanView = [[HomeHeaderSubView alloc] initWithTitle:@"聚划算"];
        [_juhuasuanView addAction:^(UIView *view) {
            MXRoute(@"GoodsSortListVC", @{@"bargain_status":@"1"})
        }];
    }
    return _juhuasuanView;
}

- (HomeHeaderSubView *)liveView {
    if (!_liveView) {
        _liveView = [[HomeHeaderSubView alloc] initWithTitle:@"1SLA专区"];
        [_liveView addAction:^(UIView *view) {
            MXRoute(@"GoodsSortListVC", @{@"one_sla_status":@"1"})
        }];
    }
    return _liveView;
}

- (HomeHeaderSubView *)saleView {
    if (!_saleView) {
        _saleView = [[HomeHeaderSubView alloc] initWithTitle:@"天天特卖"];
        [_saleView addAction:^(UIView *view) {
            MXRoute(@"GoodsSortListVC", @{@"day_sale_status":@"1"})
        }];
    }
    return _saleView;
}

- (HomeHeaderSubView *)goodView {
    if (!_goodView) {
        _goodView = [[HomeHeaderSubView alloc] initWithTitle:@"有好货"];
        [_goodView addAction:^(UIView *view) {
            MXRoute(@"GoodsSortListVC", @{@"have_good_status":@"1"})
        }];
    }
    return _goodView;
}

- (HomeHeaderSubView *)newView {
    if (!_newView) {
        _newView = [[HomeHeaderSubView alloc] initWithTitle:@"热销美妆"];
        [_newView addAction:^(UIView *view) {
            MXRoute(@"GoodsSortListVC", @{@"new_product_status":@"1"})
        }];
        _newView.myBottom = 10;
    }
    return _newView;
}

- (HomeHeaderSubView *)bandView {
    if (!_bandView) {
        _bandView = [[HomeHeaderSubView alloc] initWithTitle:@"品牌购"];
        [_bandView addAction:^(UIView *view) {
            MXRoute(@"GoodsSortListVC", @{@"brand_buy_status":@"1"})
        }];
        _bandView.myBottom = 10;
    }
    return _bandView;
}

#pragma mark - 聚划算
- (MyBaseLayout *)juhuasuanLayout {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    NSArray *subViewArr = @[self.liveView, self.juhuasuanView, self.saleView,
                            self.goodView, self.bandView, self.newView];
    MyTableLayout *table = [MyTableLayout tableLayoutWithOrientation:MyOrientation_Vert];
    table.backgroundColor = [UIColor whiteColor];
    table.myTop = 15;
    table.myLeft = 15;
    table.myRight = 15;
    table.myHeight = MyLayoutSize.wrap;
    table.myBottom = 15;
    table.subviewVSpace = 0;
    table.subviewHSpace = 0;
    [layout addSubview:table];
    CGFloat btnWidth = (SCREEN_WIDTH - 30)/3;
    [table addRow:MyLayoutSize.wrap colSize:MyLayoutSize.wrap];
    for (NSInteger i = 0; i < subViewArr.count; i++) {
        UIView *view = subViewArr[i];
        if (![view isKindOfClass:UIView.class]) {
            continue;
        }
        view.mySize = CGSizeMake(btnWidth, 120);
        [table addSubview:view];
        if ((i+1) % 3 == 0) {
            [table addRow:MyLayoutSize.wrap colSize:MyLayoutSize.wrap];
        }
    }
    
    UIView *line1 = [UIView new];
    line1.backgroundColor = [UIColor moBackground];
    line1.width = 1;
    
    UIView *line2 = [UIView new];
    line2.backgroundColor = [UIColor moBackground];
    line2.width = 1;
    
    [layout addSubview:line1];
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(line1.width);
        make.top.mas_equalTo(layout.mas_top).mas_offset(15);
        make.bottom.mas_equalTo(layout.mas_bottom).mas_offset(-15);
        make.left.mas_equalTo(layout.mas_left).mas_offset(btnWidth + 15);
    }];
    [layout addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(line2.width);
        make.top.mas_equalTo(layout.mas_top).mas_offset(15);
        make.bottom.mas_equalTo(layout.mas_bottom).mas_offset(-15);
        make.right.mas_equalTo(layout.mas_right).mas_offset(-(btnWidth+15));
    }];
    
    UIView *line3 = [UIView new];
    line3.backgroundColor = [UIColor moBackground];
    line3.height = 1;
    
    [layout addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(line3.height);
        make.left.mas_equalTo(layout.mas_left).mas_offset(15);
        make.right.mas_equalTo(layout.mas_right).mas_offset(-15);
        make.top.mas_equalTo(layout.mas_top).mas_offset(120 + 20);
    }];
    
    return layout;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

- (NSMutableArray *)categoryMArr {
    if (!_categoryMArr) {
        _categoryMArr = [NSMutableArray array];
    }
    return _categoryMArr;
}

- (HomeFreeShopListView *)freeShopListView {
    if (!_freeShopListView) {
        _freeShopListView = [HomeFreeShopListView new];
        _freeShopListView.size = CGSizeMake(SCREEN_WIDTH, [HomeFreeShopListView viewHeight]);
    }
    return _freeShopListView;
}

@end
