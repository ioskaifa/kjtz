//
//  HomeGoodsCell.m
//  BOB
//
//  Created by mac on 2020/9/9.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "HomeGoodsCell.h"
#import "GoodListView.h"

@interface HomeGoodsCell ()

@property (nonatomic, strong) GoodListView *goodView;

@end

@implementation HomeGoodsCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 306;
}

- (void)configureView:(GoodsListObj *)obj {
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    [self.goodView configureView:obj];
}

#pragma mark - InitUI
- (void)createUI {    
    [self.contentView addSubview:self.goodView];
    
    [self.goodView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
}

#pragma mark - Init
- (GoodListView *)goodView {
    if (!_goodView) {
        _goodView = [GoodListView new];
    }
    return _goodView;
}

@end
