//
//  NoviceGiftBagView.m
//  BOB
//
//  Created by colin on 2021/1/4.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import "NoviceGiftBagView.h"

@implementation NoviceGiftBagView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGSize)viewSize {
    return CGSizeMake(SCREEN_WIDTH, 450);
}

- (void)commit {
    [self request:@"api/user/info/getFirstLoginReward"
            param:@{} completion:^(BOOL success, id object, NSString *error) {
        if (success) {
            UDetail.user.give_status = YES;
            [NotifyHelper showMessageWithMakeText:@"领取成功"];
            [self routerEventWithName:@"MXCustomAlertVCconfirm" userInfo:nil];
        }
    }];
}

- (void)createUI {
    MyLinearLayout *baseLayout = [self addMyLinearLayout:MyOrientation_Vert];
    UIImageView *imgView = [UIImageView autoLayoutImgView:@"新人专享红包"];
    CGFloat width = SCREEN_WIDTH - 50;
    imgView.size = CGSizeMake(width, width*imgView.height/imgView.width);
    imgView.mySize = imgView.size;
    imgView.myCenterX = 0;
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.size = imgView.size;
    layout.mySize = layout.size;
    layout.myCenterX = 0;
    layout.backgroundImage = imgView.image;
    [baseLayout addSubview:layout];
    
    UILabel *lbl = [UILabel new];
    lbl.text = StrF(@"%@ SLA", UDetail.user.give_sla_num?:@"");
    lbl.textColor = [UIColor colorWithHexString:@"#FFDD82"];
    lbl.font = [UIFont boldSystemFontOfSize:35];
    [lbl autoMyLayoutSize];
    lbl.myCenter = CGPointMake(0, -20);
    [layout addSubview:lbl];
    
    
    imgView = [UIImageView autoLayoutImgView:@"立即领取"];
    imgView.myTop = -(imgView.height + 10);
    imgView.myCenterX = 0;
    [imgView addAction:^(UIView *view) {
        [self commit];
    }];
    [baseLayout addSubview:imgView];
    
    imgView = [UIImageView autoLayoutImgView:@"关闭-红色"];
    imgView.myCenterX = 0;
    [baseLayout addSubview:imgView];
    [imgView addAction:^(UIView *view) {
        [self routerEventWithName:@"MXCustomAlertVCCancel" userInfo:nil];
    }];
}

@end
