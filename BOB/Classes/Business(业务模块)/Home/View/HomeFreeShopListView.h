//
//  HomeFreeShopListView.h
//  BOB
//
//  Created by Colin on 2020/10/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeFreeShopListView : UIView

+ (CGFloat)viewHeight;

- (void)configureView:(NSArray *)dataArr;

@end

NS_ASSUME_NONNULL_END
