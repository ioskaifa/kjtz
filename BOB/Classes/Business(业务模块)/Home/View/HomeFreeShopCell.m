//
//  HomeFreeShopCell.m
//  BOB
//
//  Created by Colin on 2020/10/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "HomeFreeShopCell.h"

@interface HomeFreeShopCell ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) UILabel *priceLbl;

@end

@implementation HomeFreeShopCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

+ (CGSize)itemSize {
    return CGSizeMake(120, 160);
}

- (void)configureView:(GoodsListObj *)obj {
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.photo]];
    self.titleLbl.text = obj.title;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake([self.class itemSize].width - 20, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    self.priceLbl.text = StrF(@"￥%0.2f", obj.price);
    [self.priceLbl autoMyLayoutSize];
}

- (void)createUI {
    MyLinearLayout *baseLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    baseLayout.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:baseLayout];
    
    [baseLayout addSubview:self.imgView];
    [baseLayout addSubview:self.titleLbl];
    [baseLayout addSubview:[UIView placeholderVertView]];
    [baseLayout addSubview:self.priceLbl];
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.contentMode = UIViewContentModeScaleAspectFit;
        _imgView.myLeft = 0;
        _imgView.myRight = 0;
        _imgView.myTop = 0;
        _imgView.myHeight = 85;
    }
    return _imgView;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = [UILabel new];
        _titleLbl.font = [UIFont lightFont12];
        _titleLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _titleLbl.numberOfLines = 2;
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        _titleLbl.myLeft = 10;
        _titleLbl.myRight = 10;
        _titleLbl.myTop = 10;
    }
    return _titleLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.font = [UIFont boldFont13];
        _priceLbl.textColor = [UIColor colorWithHexString:@"#151419"];
        _priceLbl.myCenterX = 0;
        _priceLbl.myBottom = 5;
    }
    return _priceLbl;
}

@end
