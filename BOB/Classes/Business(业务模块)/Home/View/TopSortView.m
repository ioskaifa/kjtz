//
//  TopSortView.m
//  BOB
//
//  Created by mac on 2020/9/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "TopSortView.h"

@interface TopSortView ()

@property (nonatomic, strong) NSArray *sortArr;
///单选  当前排序
@property (nonatomic, strong) UIButton *selectBtn;

@end

@implementation TopSortView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return  40;
}

///获取当前排序类型
///sort_type 01-综合默认排序（后台指定的排序） 02—销量 03—价格
///asc_type  1-升序 2-降序
///eg: @{@"sort_type:@"01", @"asc_type":@"1"}
- (NSDictionary *)sortType {
    return @{@"sort_type":[self sort_type],
             @"asc_type":[self asc_type]
    };
}

- (NSString *)sort_type {
    NSString *title = self.selectBtn.currentTitle;
    if ([@"综合" isEqualToString:title]) {
        return @"01";
    } else if ([@"销量" isEqualToString:title]) {
        return @"02";
    } else {
        return @"03";
    }
}

- (NSString *)asc_type {
    if (self.selectBtn.selected && !self.selectBtn.highlighted) {
        return @"1";
    } else if (!self.selectBtn.selected && self.selectBtn.highlighted) {
        return @"2";
    }
    return @"1";
}

- (void)sortBtnClick:(UIButton *)sender {
    if (self.selectBtn != sender) {
        self.selectBtn.selected = NO;
        self.selectBtn.custom = @(0);
        [self.selectBtn setImage:[UIImage imageNamed:@"排序normal"] forState:UIControlStateNormal];
    }
    self.selectBtn = sender;
    if (!sender.selected && ![sender.custom boolValue]) {
        sender.selected = YES;
    } else if (sender.selected && ![sender.custom boolValue]) {
        sender.selected = NO;
        sender.custom = @(1);
        [self.selectBtn setImage:[UIImage imageNamed:@"排序des"] forState:UIControlStateNormal];
    } else if (!sender.selected && [sender.custom boolValue]) {
        sender.selected = YES;
        sender.custom = @(0);
    }
    if (self.block) {
        self.block([self sortType]);
    }
}

- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    layout.gravity = MyGravity_Horz_Fill;
    for (NSString *title in self.sortArr) {
        SPButton *btn = [self factoryBtn:title];
        if ([@"综合" isEqualToString:btn.currentTitle]) {
            btn.selected = YES;
            self.selectBtn = btn;
        }
        [layout addSubview:btn];
    }
    [self addSubview:layout];
}

- (NSArray *)sortArr {
    if (!_sortArr) {
        _sortArr = @[@"综合", @"销量", @"价格"];
    }
    return _sortArr;
}

- (SPButton *)factoryBtn:(NSString *)title {
    SPButton *btn = [SPButton new];
    btn.imagePosition = SPButtonImagePositionRight;
    btn.imageTitleSpace = 10;
    btn.titleLabel.font = [UIFont font13];
    [btn setTitleColor:[UIColor colorWithHexString:@"#AAAABB"] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#16A5AF"] forState:UIControlStateSelected];
    [btn setImage:[UIImage imageNamed:@"排序normal"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"排序asc"] forState:UIControlStateSelected];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.myTop = 0;
    btn.myBottom = 0;
    @weakify(self)
    [btn addAction:^(UIButton *btn) {
        @strongify(self)
        [self sortBtnClick:btn];
    }];
    return btn;
}

@end
