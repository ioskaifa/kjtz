//
//  HomeFreeShopListView.m
//  BOB
//
//  Created by Colin on 2020/10/31.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "HomeFreeShopListView.h"
#import "HomeFreeShopCell.h"

@interface HomeFreeShopListView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) NSArray *dataSourceArr;

@end

@implementation HomeFreeShopListView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 210;
}

- (void)configureView:(NSArray *)dataArr {
    if (![dataArr isKindOfClass:NSArray.class]) {
        return;
    }
    self.dataSourceArr = dataArr;
    [self.colView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = HomeFreeShopCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.item >= self.dataSourceArr.count) {
        return;
    }
    if (![cell isKindOfClass:HomeFreeShopCell.class]) {
        return;
    }
    HomeFreeShopCell *listCell = (HomeFreeShopCell *)cell;
    [listCell configureView:self.dataSourceArr[indexPath.item]];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceArr.count) {
        return;
    }
    
    GoodsListObj *obj = self.dataSourceArr[indexPath.row];
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    MXRoute(@"GoodInfoVC", (@{@"goods_id":obj.ID, @"isFree":@(1)}))
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    [layout addSubview:[self topView]];
    [layout addSubview:self.colView];
}

- (MyBaseLayout *)topView {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myHeight = MyLayoutSize.wrap;
    layout.myTop = 10;
    layout.myLeft = 12;
    layout.myRight = 12;
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"免单商品";
    [lbl autoMyLayoutSize];
    lbl.myCenterY = 0;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView placeholderHorzView]];
    
    SPButton *btn = [SPButton new];
    btn.userInteractionEnabled = NO;
    btn.imagePosition = SPButtonImagePositionRight;
    btn.imageTitleSpace = 5;
    btn.titleLabel.font = [UIFont lightFont12];
    [btn setTitle:@"更多" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#AAAABB"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"Arrow"] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.mySize = btn.size;
    btn.myRight = 0;
    btn.myCenterY = 0;
    [layout addSubview:btn];
    [layout addAction:^(UIView *view) {
        MXRoute(@"FreeShopVC", nil);
    }];
    
    [layout layoutIfNeeded];
    return layout;
}

- (UICollectionView *)colView {
    if (!_colView) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.itemSize = [HomeFreeShopCell itemSize];
        layout.minimumInteritemSpacing = 5;
        layout.sectionInset = UIEdgeInsetsMake(10, 12, 10, 12);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _colView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _colView.showsVerticalScrollIndicator = NO;
        _colView.showsHorizontalScrollIndicator = NO;
        _colView.backgroundColor = [UIColor whiteColor];
        _colView.delegate = self;
        _colView.dataSource = self;
        Class cls = HomeFreeShopCell.class;
        [_colView registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
        _colView.weight = 1;
        _colView.myTop = 0;
        _colView.myLeft = 0;
        _colView.myRight = 0;
    }
    return _colView;
}

@end
