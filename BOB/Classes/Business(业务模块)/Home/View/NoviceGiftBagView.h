//
//  NoviceGiftBagView.h
//  BOB
//
//  Created by colin on 2021/1/4.
//  Copyright © 2021 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoviceGiftBagView : UIView

+ (CGSize)viewSize;

@end

NS_ASSUME_NONNULL_END
