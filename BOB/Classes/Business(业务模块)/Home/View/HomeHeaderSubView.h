//
//  HomeHeaderSubView.h
//  BOB
//
//  Created by mac on 2020/9/9.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeHeaderSubView : UIView

- (instancetype)initWithTitle:(NSString *)title;

- (void)configureView:(NSArray *)dataArr;

@end

NS_ASSUME_NONNULL_END
