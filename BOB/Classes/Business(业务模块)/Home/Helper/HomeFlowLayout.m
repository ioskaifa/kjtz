//
//  HomeFlowLayout.m
//  BOB
//
//  Created by mac on 2020/9/9.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "HomeFlowLayout.h"

@interface HomeFlowLayout ()

@property (nonatomic, assign) CGFloat topEdge;

@end

@implementation HomeFlowLayout

-(NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    return [super layoutAttributesForElementsInRect:rect];
//    NSArray *array = [super layoutAttributesForElementsInRect:rect];
//    for (UICollectionViewLayoutAttributes *currentAtt in array) {
//        if (![currentAtt isKindOfClass:UICollectionViewLayoutAttributes.class]) {
//            continue;
//        }
//        if (currentAtt.representedElementCategory != UICollectionElementCategoryCell) {
//            continue;
//        }
//        if (self.topEdge == 0) {
//            self.topEdge = 980;
//        }
//        if (currentAtt.indexPath.row % 2 != 0) {
//            continue;
//        }
//        NSInteger row = floorf(currentAtt.indexPath.row / 2);
//        CGRect frame = currentAtt.frame;
//        CGFloat y = CGRectGetMinY(frame);        
//        CGFloat standY = self.topEdge + row * CGRectGetHeight(frame) + self.minimumLineSpacing*row;
//        if (y != standY) {
//            CGFloat x = CGRectGetMinX(frame);
//            CGFloat width = CGRectGetWidth(frame);
//            CGFloat height = CGRectGetHeight(frame);
//            frame = CGRectMake(x, standY, width, height);
//            currentAtt.frame = frame;
//        }
//    }
//    return array;
}

@end
