//
//  BuyGoodsListCell.m
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyGoodsListCell.h"
#import "BuyGoodsListView.h"

@interface BuyGoodsListCell ()

@property (nonatomic, strong) BuyGoodsListView *goodView;

@end

@implementation BuyGoodsListCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 260;
}

- (void)configureView:(GoodsListObj *)obj {
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    [self.goodView configureView:obj];
}

#pragma mark - InitUI
- (void)createUI {    
    [self.contentView addSubview:self.goodView];
    
    [self.goodView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
}

#pragma mark - Init
- (BuyGoodsListView *)goodView {
    if (!_goodView) {
        _goodView = [BuyGoodsListView new];
    }
    return _goodView;
}

@end
