//
//  BuyNullHeaderView.m
//  BOB
//  购物车为空
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyNullHeaderView.h"

@implementation BuyNullHeaderView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 300;
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"购物车为空"];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myCenterX = 0;
    imgView.myTop = 30;
    [layout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"#333333"];
    lbl.text = @"购物车空空如也~";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterX = 0;
    lbl.myTop = 20;
    [layout addSubview:lbl];
    [layout addSubview:[UIView placeholderVertView]];
    
    UIButton *btn = [UIButton new];
    btn.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
    btn.layer.borderWidth = 1;
    btn.titleLabel.font = [UIFont font15];
    [btn setTitle:@"立即购物" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:@"#46474D"] forState:UIControlStateNormal];
    btn.size = CGSizeMake(180, 44);
    [btn setViewCornerRadius:5];
    btn.mySize = btn.size;
    btn.myCenterX = 0;
    btn.myBottom = 20;
    [btn addAction:^(UIButton *btn) {
        if ([MoApp.mallVC isKindOfClass:UITabBarController.class]) {
            MoApp.mallVC.selectedIndex = 0;
        }
    }];
    [layout addSubview:btn];
}

@end
