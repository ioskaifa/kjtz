//
//  CartGoodsListObjCell.h
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BuyGoodsListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface CartGoodsListObjCell : UITableViewCell

@property (nonatomic, strong) UIView *line;

+ (CGFloat)viewHeight;

- (void)configureView:(BuyGoodsListObj *)obj;

@end

NS_ASSUME_NONNULL_END
