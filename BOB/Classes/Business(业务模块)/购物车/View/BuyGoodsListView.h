//
//  BuyGoodsListView.h
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsListObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface BuyGoodsListView : UIView

+ (CGFloat)viewHeight;

- (void)configureView:(GoodsListObj *)obj;

@end

NS_ASSUME_NONNULL_END
