//
//  BuyGoodsListView.m
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyGoodsListView.h"
#import "BuyGoodListBottomView.h"

@interface BuyGoodsListView ()

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) BuyGoodListBottomView *bottomView;

@property (nonatomic, strong) MyBaseLayout *layout;

@end

@implementation BuyGoodsListView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 130;
}

- (void)configureView:(GoodsListObj *)obj {
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    NSString *imgUrl = obj.photo;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    [self.bottomView configureView:obj];
}

#pragma mark - InitUI
- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.layout];
    [self.layout addSubview:self.imgView];
    [self.layout addSubview:self.bottomView];
}

#pragma mark - Init
- (MyBaseLayout *)layout {
    if (!_layout) {
        _layout = ({
            MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            layout.myTop = 0;
            layout.myLeft = 0;
            layout.myRight = 0;
            layout.myBottom = 0;
            layout;
        });
    }
    return _layout;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            [object setViewCornerRadius:1];
            object.contentMode = UIViewContentModeScaleAspectFill;
            object.backgroundColor = [UIColor whiteColor];
            object.myTop = 0;
            object.myLeft = 0;
            object.myRight = 0;
            object.weight = 1;
            object;
        });
    }
    return _imgView;
}

- (BuyGoodListBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = ({
            BuyGoodListBottomView *object = [BuyGoodListBottomView new];
            object.myTop = 0;
            object.myLeft = 5;
            object.myRight = 5;
            object.myHeight = [BuyGoodListBottomView viewHeight];
            object;
        });
    }
    return _bottomView;
}

@end
