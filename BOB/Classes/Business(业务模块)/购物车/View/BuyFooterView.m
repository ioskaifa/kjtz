//
//  BuyFooterView.m
//  BOB
//  购物车底部为您推荐
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyFooterView.h"
#import "BuyGoodsListCell.h"
#import "GoodsListObj.h"
#import "BuyFooterTopView.h"

@interface BuyFooterView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *colView;

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) NSMutableArray *dataSourceMArr;

@property (nonatomic, assign) NSInteger page_num;

@end

@implementation BuyFooterView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)fetchLastData:(void(^)(CGFloat))complete {
    [self fetchLastDataByCategory:@"" complete:complete];
}

- (void)fetchMoreData:(void(^)(CGFloat))complete {
    [self fetchMoreDataByCategory:@"" complete:complete];
}

- (void)fetchLastDataByCategory:(NSString *)categoryID
                       complete:(void(^)(CGFloat))complete {
    self.page_num = 0;
    [self fetchDataByCategory:categoryID complete:complete];
}

- (void)fetchMoreDataByCategory:(NSString *)categoryID
                       complete:(void(^)(CGFloat))complete {
    self.page_num++;
    [self fetchDataByCategory:categoryID complete:complete];
}

- (void)fetchDataByCategory:(NSString *)categoryID
                   complete:(void(^)(CGFloat))complete {
    if (self.page_num == 0) {
        [self.dataSourceMArr removeAllObjects];
    }
    NSString *url = @"api/shop/goods/getKeyWordGoodsList";
    NSString *fullUrl = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,url];
    NSDictionary *param = @{@"page_num":@(self.page_num),
                            @"category_id":categoryID
    };
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(fullUrl);
        net.params(param);
        net.finish(^(id data){
            if ([data isSuccess]) {
                NSDictionary *dataDic = (NSDictionary *)data[@"data"];
                NSArray *dataArr = [GoodsListObj modelListParseWithArray:dataDic[@"goodsList"]];
                [self.dataSourceMArr addObjectsFromArray:dataArr];
                [self.colView reloadData];
                [self.colView layoutIfNeeded];
                if (complete) {
                    complete(self.colView.contentSize.height);
                }
            }
        }).failure(^(id error){            
            [self.colView reloadData];
            [self.colView layoutIfNeeded];
            if (complete) {
                complete(self.colView.contentSize.height);
            }
        })
        .execute();
    }];
}

- (void)colViewEndRefreshing {
    [self.colView.mj_header endRefreshing];
    [self.colView.mj_footer endRefreshing];
}

#pragma mark - Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSourceMArr.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Class cls = BuyGoodsListCell.class;
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    GoodsListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:[GoodsListObj class]]) {
        return;
    }
    if (![cell isKindOfClass:[BuyGoodsListCell class]]) {
        return;
    }
    
    BuyGoodsListCell *listCell = (BuyGoodsListCell *)cell;
    [listCell configureView:obj];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return;
    }
    GoodsListObj *obj = self.dataSourceMArr[indexPath.row];
    if (![obj isKindOfClass:[GoodsListObj class]]) {
        return;
    }
    MXRoute(@"GoodInfoVC", @{@"goods_id":obj.ID})
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        Class cls = BuyFooterTopView.class;
        BuyFooterTopView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:NSStringFromClass(cls) forIndexPath:indexPath];
        return view;
    }
    return [UICollectionReusableView new];
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= self.dataSourceMArr.count) {
        return CGSizeZero;
    }
    return CGSizeMake((SCREEN_WIDTH - 40) / 2.0, [BuyGoodsListCell viewHeight]);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(SCREEN_WIDTH, [BuyFooterTopView viewHeight]);
}

- (void)createUI {
    self.page_num = 0;
    [self addSubview:self.colView];
    [self.colView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}

#pragma mark - Init
- (UICollectionView *)colView {
    if (!_colView) {
        _colView = ({
            UICollectionView *object = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
            object.backgroundColor = [UIColor moBackground];
            object.delegate = self;
            object.dataSource = self;
            Class cls = BuyGoodsListCell.class;
            [object registerClass:cls forCellWithReuseIdentifier:NSStringFromClass(cls)];
            cls = BuyFooterTopView.class;
            [object registerClass:cls forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
              withReuseIdentifier:NSStringFromClass(cls)];
            object.scrollEnabled = NO;
            object;
        });
    }
    return _colView;
}

- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout = ({
            UICollectionViewFlowLayout *object = [UICollectionViewFlowLayout new];
            object.minimumLineSpacing = 10;
            object.minimumInteritemSpacing = 10;
            object.sectionInset = UIEdgeInsetsMake(0, 15, 10, 15);
            object;
        });
    }
    return _flowLayout;
}

- (NSMutableArray *)dataSourceMArr {
    if (!_dataSourceMArr) {
        _dataSourceMArr = [NSMutableArray array];
    }
    return _dataSourceMArr;
}

@end
