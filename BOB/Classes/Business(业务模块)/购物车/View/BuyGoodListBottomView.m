//
//  BuyGoodListBottomView.m
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyGoodListBottomView.h"

@interface BuyGoodListBottomView ()

@property (nonatomic, strong) UILabel *titleLbl;
///价格
@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) MyBaseLayout *layout;

@end

@implementation BuyGoodListBottomView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 86;
}

- (void)configureView:(GoodsListObj *)obj {
    if (![obj isKindOfClass:GoodsListObj.class]) {
        return;
    }
    self.titleLbl.text = obj.title;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(((SCREEN_WIDTH - 40) / 2.0) - 10, CGFLOAT_MAX)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    NSArray *txtArr = @[@"￥", StrF(@"%0.2f", obj.price)];
    NSArray *colorArr = @[[UIColor blackColor], [UIColor blackColor]];
    NSArray *fontArr = @[[UIFont font11], [UIFont boldFont15]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
    self.priceLbl.attributedText = att;
    [self.priceLbl sizeToFit];
    self.priceLbl.mySize = self.priceLbl.size;
}

#pragma mark - InitUI
- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.layout];
    [self.layout addSubview:self.titleLbl];
    MyLinearLayout *subLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    subLayout.myTop = 10;
    subLayout.myLeft = 0;
    subLayout.myRight = 0;
    subLayout.myHeight = 25;
    subLayout.myBottom = 10;
    [subLayout addSubview:self.priceLbl];
    UIView *view = [UIView new];
    view.weight = 1;
    [subLayout addSubview:view];
    UIImageView *imgView = [UIImageView new];
    imgView.image = [UIImage imageNamed:@"加入购物车"];
    [imgView sizeToFit];
    imgView.mySize = imgView.size;
    imgView.myCenterY = 0;
    imgView.myRight = 5;
    [subLayout addSubview:imgView];
    [self.layout addSubview:subLayout];
}

#pragma mark - Init
- (MyBaseLayout *)layout {
    if (!_layout) {
        _layout = ({
            MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
            layout.myTop = 0;
            layout.myLeft = 0;
            layout.myRight = 0;
            layout.myHeight = MyLayoutSize.wrap;
            layout;
        });
    }
    return _layout;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.textColor = [UIColor colorWithHexString:@"#151419"];
            object.font = [UIFont lightFont13];
            object.numberOfLines = 2;
            object.myTop = 10;
            object.myLeft = 5;
            object.myRight = 5;
            object;
        });
    }
    return _titleLbl;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = ({
            UILabel *object = [UILabel new];
            object.myLeft = 5;
            object.myCenterY = 0;
            object;
        });
    }
    return _priceLbl;
}

@end
