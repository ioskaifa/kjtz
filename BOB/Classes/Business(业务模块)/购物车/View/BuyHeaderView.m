//
//  BuyHeaderView.m
//  BOB
//
//  Created by mac on 2020/9/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyHeaderView.h"

@interface BuyHeaderView ()

@property (nonatomic, strong) SPButton *addressBtn;

@end

@implementation BuyHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 30;
}

- (void)configureView:(NSString *)address {
    address = address ?:@"";
    address = StrF(@"%@ %@", @"配送至:", address);
    [self.addressBtn setTitle:address forState:UIControlStateNormal];
    [self.addressBtn sizeToFit];
    CGSize size = self.addressBtn.size;
    if (size.width > SCREEN_WIDTH - 30) {
        size = CGSizeMake(SCREEN_WIDTH - 30, size.height);
        self.addressBtn.size = size;
    }
    self.addressBtn.mySize = self.addressBtn.size;
}

- (void)createUI {
    self.backgroundColor = [UIColor colorWithHexString:@"#F3F2F7"];
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [layout addSubview:self.addressBtn];
    [self addSubview:layout];
}

- (SPButton *)addressBtn {
    if (!_addressBtn) {
        _addressBtn = ({
            SPButton *object = [SPButton new];
            object.imagePosition = SPButtonImagePositionLeft;
            object.imageTitleSpace = 10;
            object.titleLabel.font = [UIFont font12];
            [object setTitleColor:[UIColor colorWithHexString:@"#7F7F8E"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"购物车地址"] forState:UIControlStateNormal];
            object.myCenter = CGPointZero;
            object;
        });
    }
    return _addressBtn;
}

@end
