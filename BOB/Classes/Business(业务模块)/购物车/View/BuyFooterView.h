//
//  BuyFooterView.h
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BuyFooterView : UIView

- (void)fetchLastData:(void(^)(CGFloat))complete;

- (void)fetchMoreData:(void(^)(CGFloat))complete;

- (void)fetchLastDataByCategory:(NSString *)categoryID
                       complete:(void(^)(CGFloat))complete;

- (void)fetchMoreDataByCategory:(NSString *)categoryID
                       complete:(void(^)(CGFloat))complete;

@end

NS_ASSUME_NONNULL_END
