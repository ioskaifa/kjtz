//
//  CartGoodsListObjCell.m
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "CartGoodsListObjCell.h"
#import "PPNumberButton.h"

@interface CartGoodsListObjCell () <PPNumberButtonDelegate>

@property (nonatomic, strong) UIButton *selectBtn;

@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UILabel *titleLbl;

@property (nonatomic, strong) SPButton *specsBtn;

@property (nonatomic, strong) UILabel *priceLbl;

@property (nonatomic, strong) PPNumberButton *numberBtn;
///失效
@property (nonatomic, strong) UIImageView *invalidImgView;

@property (nonatomic, strong) BuyGoodsListObj *cellObj;

@end

@implementation CartGoodsListObjCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 130;
}

- (void)configureView:(BuyGoodsListObj *)obj {
    if (![obj isKindOfClass:BuyGoodsListObj.class]) {
        return;
    }
    self.cellObj = obj;
    self.selectBtn.selected = obj.isSelected;
    NSString *imgUrl = obj.photo;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    
    self.titleLbl.text = obj.title;
    CGFloat width = SCREEN_WIDTH - 50 - self.imgView.width - self.selectBtn.width;
    CGSize size = [self.titleLbl sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    self.titleLbl.size = size;
    self.titleLbl.mySize = self.titleLbl.size;
    
    [self.specsBtn setTitle:obj.specs forState:UIControlStateNormal];
    [self.specsBtn sizeToFit];
    CGFloat btnWidth = self.specsBtn.width > (width - 10) ? (width - 10) : self.specsBtn.width;
    self.specsBtn.myWidth = btnWidth + 10;
    
    self.priceLbl.text = StrF(@"%@%0.2f", @"￥", obj.price);
    [self.priceLbl sizeToFit];
    self.priceLbl.mySize = self.priceLbl.size;
    self.numberBtn.currentNumber = obj.number;
    if (obj.isInvalid) {
        [self configureViewInvalidState];
    } else {
        [self configureViewValidState];
    }
}

#pragma mark - Delegate
- (void)pp_numberButton:(__kindof UIView *)numberButton number:(NSInteger)number increaseStatus:(BOOL)increaseStatus {    
    [self.nextResponder routerEventWithName:@"CartGoodsListObjCellNumberChange"
                                   userInfo:@{@"status":@(increaseStatus),
                                              @"cellObj":self.cellObj}];
}

#pragma mark - 失效产品
- (void)configureViewInvalidState {
    self.invalidImgView.hidden = NO;
    self.imgView.alpha = 0.7;
    self.titleLbl.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
    self.specsBtn.alpha = 0.7;
    self.priceLbl.textColor = [UIColor colorWithHexString:@"#A8A8A8"];
    self.numberBtn.alpha = 0.7;
    self.numberBtn.userInteractionEnabled = NO;
}
#pragma mark - 正常产品
- (void)configureViewValidState {
    self.invalidImgView.hidden = YES;
    self.imgView.alpha = 1;
    self.titleLbl.textColor = [UIColor moBlack];
    self.specsBtn.alpha = 1;
    self.priceLbl.textColor = [UIColor blackColor];
    self.numberBtn.alpha = 1;
    self.numberBtn.userInteractionEnabled = YES;
}

- (void)createUI {
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 10;
    layout.myRight = 10;
    layout.myBottom = 0;
    [layout setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:layout];
    
    [layout addSubview:self.selectBtn];
    [layout addSubview:self.imgView];
    
    MyLinearLayout *rightLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightLayout.weight = 1;
    rightLayout.myLeft = 10;
    rightLayout.myRight = 10;
    rightLayout.myHeight = self.imgView.height;
    rightLayout.myCenterY = 0;
    [layout addSubview:rightLayout];
    
    [rightLayout addSubview:self.titleLbl];
    [rightLayout addSubview:self.specsBtn];
    [rightLayout addSubview:[UIView placeholderVertView]];
    
    MyLinearLayout *bottomLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    bottomLayout.myLeft = 0;
    bottomLayout.myRight = 0;
    bottomLayout.myHeight = MyLayoutSize.wrap;
    [rightLayout addSubview:bottomLayout];
    
    [bottomLayout addSubview:self.priceLbl];
    [bottomLayout addSubview:[UIView placeholderHorzView]];
    [bottomLayout addSubview:self.numberBtn];
    
    [self.contentView addSubview:self.line];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(rightLayout.mas_right);
        make.bottom.mas_equalTo(layout.mas_bottom);
        make.left.mas_equalTo(rightLayout.mas_left);
    }];
    
    [self.imgView addSubview:self.invalidImgView];
    [self.invalidImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.invalidImgView.size);
        make.center.mas_equalTo(self.imgView);
    }];
}

#pragma mark - Init
- (UIButton *)selectBtn {
    if (!_selectBtn) {
        _selectBtn = ({
            UIButton *object = [UIButton new];
            [object setImage:[UIImage imageNamed:@"icon_public_unselect"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"icon_green_select"] forState:UIControlStateSelected];
            object.size = CGSizeMake(30, 30);
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 5;
            [object addAction:^(UIButton *btn) {
                [self.nextResponder routerEventWithName:@"CartGoodsListObjCellSelect" userInfo:@{@"cellObj":self.cellObj}];
            }];
            object;
        });
    }
    return _selectBtn;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = ({
            UIImageView *object = [UIImageView new];
            object.backgroundColor = [UIColor moBackground];
            [object setViewCornerRadius:5];
            object.size = CGSizeMake(100, 100);
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 5;
            object;
        });
    }
    return _imgView;
}

- (UIImageView *)invalidImgView {
    if (!_invalidImgView) {
        _invalidImgView = ({
            UIImageView *object = [UIImageView new];
            object.image = [UIImage imageNamed:@"购物车已失效"];
            [object sizeToFit];
            object;
        });
    }
    return _invalidImgView;
}

- (PPNumberButton *)numberBtn {
    if (!_numberBtn) {
        _numberBtn = ({
            PPNumberButton *object = [PPNumberButton numberButtonWithFrame:CGRectZero];
            object.minValue = 1;
            object.increaseTitle = @"＋";
            object.decreaseTitle = @"－";
            object.increaseImage = [UIImage imageNamed:@"加"];
            object.decreaseImage = [UIImage imageNamed:@"减"];
            object.inputFieldFont = 15;
            object.delegate = self;
            object.mySize = CGSizeMake(70, 30);
            object.myCenterY = 0;
            object;
        });
    }
    return _numberBtn;
}

- (UILabel *)titleLbl {
    if (!_titleLbl) {
        _titleLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont lightFont14];
            object.numberOfLines = 2;
            object.textColor = [UIColor colorWithHexString:@"#151419"];
            object.myLeft = 0;
            object.myBottom = 5;
            object;
        });
    }
    return _titleLbl;
}

- (SPButton *)specsBtn {
    if (!_specsBtn) {
        _specsBtn = ({
            SPButton *object = [SPButton new];
            object.backgroundColor = [UIColor colorWithHexString:@"#F5F5FA"];
            object.imagePosition = SPButtonImagePositionRight;
            object.imageTitleSpace = 10;
            object.titleLabel.font = [UIFont font11];
            object.height = 20;
            [object setViewCornerRadius:object.height/2.0];
//            [object setImage:[UIImage imageNamed:@"购物车更改"] forState:UIControlStateNormal];
            object.userInteractionEnabled = NO;
            [object setTitleColor:[UIColor colorWithHexString:@"#AAAABB"] forState:UIControlStateNormal];
            object.myHeight = object.height;
            object.myTop = 5;
            object;
        });
    }
    return _specsBtn;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.font = [UIFont boldFont17];
        _priceLbl.textColor = [UIColor blackColor];
        _priceLbl.myCenterY = 0;
    }
    return _priceLbl;
}

- (UIView *)line {
    if (!_line) {
        _line = ({
            UIView *object = [UIView new];
            object.backgroundColor = [UIColor moBackground];
            object;
        });
    }
    return _line;
}



@end
