//
//  BuyFooterTopView.m
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyFooterTopView.h"

@implementation BuyFooterTopView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 50;
}

- (void)createUI {
    self.backgroundColor = [UIColor moBackground];
    MyFrameLayout *baseLayout = [MyFrameLayout new];
    baseLayout.myTop = 0;
    baseLayout.myLeft = 0;
    baseLayout.myRight = 0;
    baseLayout.myBottom = 0;
    [self addSubview:baseLayout];
    
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myHeight = MyLayoutSize.wrap;
    layout.myWidth = MyLayoutSize.wrap;
    layout.myCenter = CGPointZero;
    [baseLayout addSubview:layout];
    
//    UIImageView *imgView = [UIImageView autoLayoutImgView:@"section左边"];
//    imgView.myCenterY = 0;
//    [layout addSubview:imgView];
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont font13];
    lbl.textColor = [UIColor blackColor];
    lbl.text = @"——      为你推荐      ——";
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myCenterY = 0;
    lbl.myLeft = 15;
    lbl.myRight = 15;
    [layout addSubview:lbl];
    
//    imgView = [UIImageView autoLayoutImgView:@"section右边"];
//    imgView.myCenterY = 0;
//    [layout addSubview:imgView];
}

@end
