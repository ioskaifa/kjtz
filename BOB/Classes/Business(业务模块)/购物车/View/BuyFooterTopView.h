//
//  BuyFooterTopView.h
//  BOB
//  为您推荐
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BuyFooterTopView : UICollectionReusableView

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
