//
//  BuyHeaderView.h
//  BOB
//
//  Created by mac on 2020/9/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BuyHeaderView : UIView

+ (CGFloat)viewHeight;

- (void)configureView:(NSString *)address;

@end

NS_ASSUME_NONNULL_END
