//
//  BuySettlementView.h
//  BOB
//
//  Created by mac on 2020/9/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, BuySettlementViewType) {
    ///结算UI
    BuySettlementViewTypeNormal    = 0,
    ///删除UI
    BuySettlementViewTypeDel       = 1,
};

@interface BuySettlementView : UIView
///全选
@property (nonatomic, strong) SPButton *selectBtn;
///去结算
@property (nonatomic, strong) UIButton *buyBtn;

@property (nonatomic, assign) BuySettlementViewType viewType;

+ (CGFloat)viewHeight;

- (void)configureView:(CGFloat)price number:(NSInteger)num;

@end

NS_ASSUME_NONNULL_END
