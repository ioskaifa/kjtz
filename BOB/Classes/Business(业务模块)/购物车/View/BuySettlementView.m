//
//  BuySettlementView.m
//  BOB
//  去结算
//  Created by mac on 2020/9/12.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuySettlementView.h"
#import "UIImage+Color.h"

@interface BuySettlementView ()
///合计
@property (nonatomic, strong) UILabel *priceLbl;
///一键删除所有
@property (nonatomic, strong) SPButton *delAllBtn;
///删除
@property (nonatomic, strong) UIButton *delBtn;

@end

@implementation BuySettlementView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

+ (CGFloat)viewHeight {
    return 50;
}

- (void)setViewType:(BuySettlementViewType)viewType {
    _viewType = viewType;
    if (viewType == BuySettlementViewTypeNormal) {
        [self configureViewNormal];
    } else if (viewType == BuySettlementViewTypeDel) {
        [self configureViewDel];
    }
}

- (void)configureViewNormal {
    self.priceLbl.visibility = MyVisibility_Visible;
    self.buyBtn.visibility = MyVisibility_Visible;
    
    self.delAllBtn.visibility = MyVisibility_Gone;
    self.delBtn.visibility = MyVisibility_Gone;
}

- (void)configureViewDel {
    self.delAllBtn.visibility = MyVisibility_Visible;
    self.delBtn.visibility = MyVisibility_Visible;
    
    self.priceLbl.visibility = MyVisibility_Gone;
    self.buyBtn.visibility = MyVisibility_Gone;
}

- (void)configureView:(CGFloat)price number:(NSInteger)num {
    self.priceLbl.size = CGSizeZero;
    self.priceLbl.mySize = self.priceLbl.size;
    NSArray *txtArr = @[@"合计: ", StrF(@"￥%0.2f", price)];
    NSArray *colorArr = @[[UIColor blackColor], [UIColor blackColor]];
    NSArray *fontArr = @[[UIFont font13], [UIFont boldFont16]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr colors:colorArr fonts:fontArr];
//    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
//    paragraphStyle.lineSpacing = 3; // 调整行间距
//    paragraphStyle.alignment = NSTextAlignmentRight;
//    NSRange range = [att.string rangeOfString:att.string];
//    [att addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    self.priceLbl.attributedText = att;
    [self.priceLbl sizeToFit];
    self.priceLbl.mySize = self.priceLbl.size;
    NSString *title = StrF(@"%@(%ld)", @"去结算", (long)num);
    [self.buyBtn setTitle:title forState:UIControlStateNormal];
    title = StrF(@"%@(%ld)", @"删除", (long)num);
    [self.delBtn setTitle:title forState:UIControlStateNormal];
}

- (void)createUI {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    [layout addSubview:self.selectBtn];
    [layout addSubview:[UIView placeholderHorzView]];
    [layout addSubview:self.priceLbl];
    [layout addSubview:self.delAllBtn];
    [layout addSubview:self.delBtn];
    [layout addSubview:self.buyBtn];
    [self configureView:0 number:0];
    
    UIView *line = [UIView fullLine];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(line.height);
        make.left.mas_equalTo(self.mas_left);
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.mas_top);
    }];
}

#pragma mark - Init
- (SPButton *)selectBtn {
    if (!_selectBtn) {
        _selectBtn = ({
            SPButton *object = [SPButton new];
            object.imageTitleSpace = 10;
            object.imagePosition = SPButtonImagePositionLeft;
            [object setImage:[UIImage imageNamed:@"icon_red_unselect"] forState:UIControlStateNormal];
            [object setImage:[UIImage imageNamed:@"icon_green_select"] forState:UIControlStateSelected];
            [object setTitleColor:[UIColor colorWithHexString:@"#1A1A1A"] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font13];
            [object setTitle:@"全选" forState:UIControlStateNormal];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myLeft = 10;
            object;
        });
    }
    return _selectBtn;
}

- (UILabel *)priceLbl {
    if (!_priceLbl) {
        _priceLbl = [UILabel new];
        _priceLbl.numberOfLines = 0;
        _priceLbl.myCenterY = 0;
        _priceLbl.myRight = 10;
    }
    return _priceLbl;
}

- (UIButton *)buyBtn {
    if (!_buyBtn) {
        _buyBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(120, 42);
            [object setViewCornerRadius:5];
            object.titleLabel.font = [UIFont boldFont15];
            [object setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            object.backgroundColor = [UIColor moGreen];
            [object setTitle:@"去结算" forState:UIControlStateNormal];
            object.myCenterY = 0;
            object.mySize = object.size;
            object.myRight = 10;
            object;
        });
    }
    return _buyBtn;
}

- (SPButton *)delAllBtn {
    if (!_delAllBtn) {
        _delAllBtn = ({
            SPButton *object = [SPButton new];
            object.imageTitleSpace = 5;
            object.imagePosition = SPButtonImagePositionLeft;
            [object setImage:[UIImage imageNamed:@"一键删除所有"] forState:UIControlStateNormal];
            [object setTitleColor:[UIColor colorWithHexString:@"#EF0F00"] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont font12];
            [object setTitle:@"一键删除所有" forState:UIControlStateNormal];
            [object sizeToFit];
            object.mySize = object.size;
            object.myCenterY = 0;
            object.myRight = 20;
            [object addAction:^(UIButton *btn) {
                [self.nextResponder routerEventWithName:@"BuySettlementViewDelAll" userInfo:nil];
            }];
            object;
        });
    }
    return _delAllBtn;
}

- (UIButton *)delBtn {
    if (!_delBtn) {
        _delBtn = ({
            UIButton *object = [UIButton new];
            object.size = CGSizeMake(120, 42);
            [object setViewCornerRadius:5];
            object.titleLabel.font = [UIFont boldFont15];
            [object setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
            object.layer.borderColor = [UIColor colorWithHexString:@"#E3E3EB"].CGColor;
            object.layer.borderWidth = 1;
            [object setTitle:@"删除" forState:UIControlStateNormal];
            object.myCenterY = 0;
            object.mySize = object.size;
            object.myRight = 10;
            [object addAction:^(UIButton *btn) {
                [self.nextResponder routerEventWithName:@"BuySettlementViewDelSelected" userInfo:nil];
            }];
            object;
        });
    }
    return _delBtn;
}

@end
