//
//  BuyGoodsListObj.m
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BuyGoodsListObj.h"

@implementation BuyGoodsListObj

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id",
             @"title" : @"goods_name",
             @"specs" : @"character_value",
             @"photo" : @"goods_detail_show",
             @"number" : @"goods_num",
             @"price" : @"cash"};
}

- (BOOL)isInvalid {
    return self.is_change;
}

- (BOOL)isSelfSupport {
    if ([@"01" isEqualToString:self.goods_type]) {
        return YES;
    }
    return NO;
}

- (NSString *)photo {
    if (self.isSelfSupport) {
        NSString *imgUrl = StrF(@"%@/%@", UDetail.user.qiniu_domain, _photo);
        return imgUrl;
    } else {
        return _photo;
    }
}

- (NSString *)goods_show {
    return StrF(@"%@/%@", UDetail.user.qiniu_domain, _goods_show);
}

+ (NSArray *)buyGoodsListObj {
    BuyGoodsListObj *obj = [BuyGoodsListObj new];
    obj.title = @"狮子歌歌芽茶梅酒茶酒青熟梅酒荔枝果酒甜酒柚子梅酒…";
    obj.number = 1;
    obj.specs = @"芽茶梅酒；600ML";
    obj.price = 129.6;
    
    BuyGoodsListObj *obj1 = [BuyGoodsListObj new];
    obj1.title = @"【郑爽同款】Lola Rose手表女ins风轻奢复古小方盘正品";
    obj1.number = 1;
    obj1.specs = @"LR2R34；香槟金";
    obj1.price = 1060;
    obj1.isExplain = YES;
    
    BuyGoodsListObj *obj2 = [BuyGoodsListObj new];
    obj2.title = @"Haier/海尔 BCD-470WDPG风冷变频一级节能家用冰箱";
    obj2.number = 1;
    obj2.specs = @"星动-12头-全铜材质 -【一室两厅B3套装】- 不带光源";
    obj2.price = 4799;
    
    BuyGoodsListObj *obj3 = [BuyGoodsListObj new];
    obj3.title = @"狮子歌歌芽茶梅酒茶酒青熟梅酒荔枝果酒甜酒柚子梅酒…";
    obj3.number = 1;
    obj3.specs = @"芽茶梅酒；600ML";
    obj3.price = 129.6;
    obj3.isInvalid = YES;
    
    return @[obj, obj1, obj2, obj3];
}

@end
