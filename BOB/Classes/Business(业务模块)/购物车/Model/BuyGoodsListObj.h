//
//  BuyGoodsListObj.h
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface BuyGoodsListObj : BaseObject

///是否是自营商品
@property (nonatomic, assign) BOOL isSelfSupport;
///购物车编号
@property (nonatomic, copy) NSString *ID;

@property (nonatomic, copy) NSString *goods_id;
///商品类别
@property (nonatomic, copy) NSString *goods_type;

@property (nonatomic, copy) NSString *user_id;
///供应商ID
@property (nonatomic, copy) NSString *supplier_id;
///供应商名称
@property (nonatomic, copy) NSString *supplier_name;
///供应商logo
@property (nonatomic, copy) NSString *supplier_logo;
///商品图片
@property (nonatomic, copy) NSString *photo;
///商品名称
@property (nonatomic, copy) NSString *title;
///商品价格
@property (nonatomic, assign) CGFloat price;
///商品数量
@property (nonatomic, assign) NSInteger number;
///商品库存
@property (nonatomic, assign) NSInteger stock;
///商品规格ID
@property (nonatomic, copy) NSString *goods_detail_id;
///商品规格
@property (nonatomic, copy) NSString *specs;
///快递费
@property (nonatomic, assign) CGFloat express_cash;
///商品信息是否变更 0-未变更 1-已变更 注意：对于已经变更的不能结算购买 失效
@property (nonatomic, assign) BOOL is_change;
///是否失效
@property (nonatomic, assign) BOOL isInvalid;
///是否选中
@property (nonatomic, assign) BOOL isSelected;
///是否正在讲解
@property (nonatomic, assign) BOOL isExplain;
///：商品库存
@property (nonatomic, copy) NSString *goods_stock_num;
///：展示价格
@property (nonatomic, assign) CGFloat cash_min;
///：商品展示图
@property (nonatomic, copy) NSString *goods_show;
///是否只能SLA支付
@property (nonatomic, assign) BOOL only_sla_pay;

+ (NSArray *)buyGoodsListObj;

@end

NS_ASSUME_NONNULL_END
