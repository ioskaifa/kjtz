//
//  UIFont+MoFont.h
//  MoPromo_Develop
//
//  Created by yang.xiangbao on 15/10/21.
//  Copyright © 2015年 MoPromo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (MoFont)

+ (UIFont *)lightFont26;
+ (UIFont *)lightFont24;
+ (UIFont *)lightFont20;
+ (UIFont *)lightFont19;
+ (UIFont *)lightFont18;
+ (UIFont *)lightFont17;
+ (UIFont *)lightFont16;
+ (UIFont *)lightFont15;
+ (UIFont *)lightFont14;
+ (UIFont *)lightFont13;
+ (UIFont *)lightFont12;
+ (UIFont *)lightFont11;

+ (UIFont *)font26;
+ (UIFont *)font24;
+ (UIFont *)font20;
+ (UIFont *)font19;
+ (UIFont *)font18;
+ (UIFont *)font17;
+ (UIFont *)font16;
+ (UIFont *)font15;
+ (UIFont *)font14;
+ (UIFont *)font13;
+ (UIFont *)font12;
+ (UIFont *)font11;
+ (UIFont *)font10;

+ (UIFont *)boldFont30;
+ (UIFont *)boldFont20;
+ (UIFont *)boldFont19;
+ (UIFont *)boldFont18;
+ (UIFont *)boldFont17;
+ (UIFont *)boldFont16;
+ (UIFont *)boldFont15;
+ (UIFont *)boldFont14;
+ (UIFont *)boldFont13;
+ (UIFont *)boldFont12;
+ (UIFont *)boldFont11;


@end
