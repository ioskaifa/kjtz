//
//  UIFont+MoFont.m
//  MoPromo_Develop
//
//  Created by yang.xiangbao on 15/10/21.
//  Copyright © 2015年 MoPromo. All rights reserved.
//

#import "UIFont+MoFont.h"
#import "MXCache.h"

@implementation UIFont (MoFont)

+ (UIFont *)lightFont26 {
    static NSString *fontKey = @"lightFont26";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:26];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont24 {
    static NSString *fontKey = @"lightFont24";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:24];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont20 {
    static NSString *fontKey = @"lightFont20";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:20];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont19 {
    static NSString *fontKey = @"lightFont19";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:19];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont18 {
    static NSString *fontKey = @"lightFont18";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:18];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont17 {
    static NSString *fontKey = @"lightFont17";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:17];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont16 {
    static NSString *fontKey = @"lightFont16";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:16];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont15 {
    static NSString *fontKey = @"lightFont15";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:15];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont14 {
    static NSString *fontKey = @"lightFont14";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:14];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont13 {
    static NSString *fontKey = @"lightFont13";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:13];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont12 {
    static NSString *fontKey = @"lightFont12";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:12];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)lightFont11 {
    static NSString *fontKey = @"lightFont11";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont fontWithName:@"PingFangSC-Thin" size:11];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}


+ (UIFont *)font26
{
    static NSString *fontKey = @"font26";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:26.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font24
{
    static NSString *fontKey = @"font24";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:24.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font20
{
    static NSString *fontKey = @"font20";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:20.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font19
{
    static NSString *fontKey = @"font19";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:19.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font18
{
    static NSString *fontKey = @"font18";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:18.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font17
{
    static NSString *fontKey = @"font17";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:17.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font16
{
    static NSString *fontKey = @"font16";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:16.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font15
{
    static NSString *fontKey = @"font15";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:15.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font14
{
    static NSString *fontKey = @"font14";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:14.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font13
{
    static NSString *fontKey = @"font13";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:13.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}


+ (UIFont *)font12
{
    static NSString *fontKey = @"font12";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:12.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font11
{
    static NSString *fontKey = @"font11";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:11.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)font10
{
    static NSString *fontKey = @"font10";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont systemFontOfSize:10.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont30
{
    static NSString *fontKey = @"boldFont30";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:30.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont20
{
    static NSString *fontKey = @"boldFont20";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:20.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont19
{
    static NSString *fontKey = @"boldFont19";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:19.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont18
{
    static NSString *fontKey = @"boldFont18";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:18.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont17
{
    static NSString *fontKey = @"boldFont17";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:17.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont16
{
    static NSString *fontKey = @"boldFont16";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:16.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont15
{
    static NSString *fontKey = @"boldFont15";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:15.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont14
{
    static NSString *fontKey = @"boldFont14";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:14.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont13
{
    static NSString *fontKey = @"boldFont13";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:13.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont12
{
    static NSString *fontKey = @"boldFont12";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:12.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

+ (UIFont *)boldFont11
{
    static NSString *fontKey = @"boldFont11";
    UIFont *font = [MXCache valueForKey:fontKey];
    if (!font) {
        font = [UIFont boldSystemFontOfSize:11.f];
        [MXCache setValue:font forKey:fontKey];
    }
    
    return font;
}

@end
