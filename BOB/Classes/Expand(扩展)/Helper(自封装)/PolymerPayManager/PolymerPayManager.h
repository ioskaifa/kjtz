//
//  PolymerPayManager.h
//  RatelBrother
//
//  Created by mac on 2019/9/14.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * _Nonnull const kWechatAppid = @"wxde55f88d1e27d947";
static NSString * _Nonnull const kWechatUniversalLink = @"https://com.kjtz.mall/";

typedef NS_ENUM(NSInteger,PolymerPayType) {
    PolymerPayTypeExchange,
    PolymerPayTypeAliPay,
    PolymerPayTypeWePay,
};

NS_ASSUME_NONNULL_BEGIN

@interface PolymerPayManager : NSObject
+ (instancetype)shared;
- (void)pay:(PolymerPayType)type param:(NSDictionary *)param viewController:(UIViewController *)viewController block:(FinishedBlock)block;

- (void)pay:(PolymerPayType)type url:(NSString *)url param:(NSDictionary *)param viewController:(UIViewController *)viewController block:(FinishedBlock)block;

///调起微信支付
- (void)wePay:(NSDictionary *)param viewController:(UIViewController *)viewController block:(FinishedBlock)block;

///调起支付宝支付
- (void)aliPay:(NSDictionary *)param block:(FinishedBlock)block;

///支付回调
- (void)payCallBack:(NSURL *)url;
@end

NS_ASSUME_NONNULL_END
