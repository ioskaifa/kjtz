//
//  PolymerPayManager.m
//  RatelBrother
//
//  Created by mac on 2019/9/14.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "PolymerPayManager.h"
#import "WXApiManager.h"
#import <AlipaySDK/AlipaySDK.h>
#import "NSString+Ext.h"

@interface PolymerPayManager ()<WXApiDelegate>

@property(nonatomic,strong) FinishedBlock payCompletionBlock;

@end

@implementation PolymerPayManager

+(instancetype)shared {
    static dispatch_once_t onceToken;
    static PolymerPayManager *instance;
    dispatch_once(&onceToken, ^{
        instance = [[PolymerPayManager alloc] init];
    });
    return instance;
}

- (instancetype)init {
    if(self == [super init]) {
        [WXApiManager sharedManager].delegate = self;
    }
    return self;
}

- (void)pay:(PolymerPayType)type param:(NSDictionary *)param viewController:(UIViewController *)viewController block:(FinishedBlock)block {
    self.payCompletionBlock = block;
    NSString *fullUrl = [NSString stringWithFormat:@"%@/cartSettlePayment",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(fullUrl);
        //net.useMsg();
        net.useEncrypt();
        net.params(param);
        net.finish(^(id data){
            if(type == PolymerPayTypeWePay) {
                [self wePay:[data valueForKeyPath:@"data.wechat_sdk"] viewController:viewController block:block];
            } else if(type == PolymerPayTypeAliPay) {
                [self aliPay:[data valueForKey:@"data"] block:block];
            } else {
                Block_Exec(block,data);
            }
            if(![data isSuccess]) {
                [NotifyHelper showMessageWithMakeText:data[@"msg"]];
            }
        }).failure(^(id error){
            Block_Exec(block,[error description]);
        })
        .execute();
    }];
}

- (void)pay:(PolymerPayType)type url:(NSString *)url param:(NSDictionary *)param viewController:(UIViewController *)viewController block:(FinishedBlock)block {
    self.payCompletionBlock = block;
    NSString *fullUrl = [NSString stringWithFormat:@"%@%@",LcwlServerRoot, url];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(fullUrl);
        //net.useMsg();
        net.useEncrypt();
        net.params(param);
        net.finish(^(id data){
            if(type == PolymerPayTypeWePay) {
                [self wePay:[data valueForKeyPath:@"data.wechat_sdk"] viewController:viewController block:block];
            } else if(type == PolymerPayTypeAliPay) {
                [self aliPay:data[@"data"] block:block];
            } else {
                Block_Exec(block,data);
            }
            if(![data isSuccess]) {
                [NotifyHelper showMessageWithMakeText:data[@"msg"]];
                Block_Exec(block,data);
            }
        }).failure(^(id error){
            Block_Exec(block,[error description]);
        })
        .execute();
    }];
}

///调起微信支付
- (void)wePay:(NSDictionary *)param viewController:(UIViewController *)viewController block:(FinishedBlock)block {
    if(![param isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    PayReq *req             = [[PayReq alloc] init];
    req.partnerId           = [param valueForKey:@"partnerid"];
    req.prepayId            = [param valueForKey:@"prepayid"];
    req.nonceStr            = [param valueForKey:@"noncestr"];
    req.timeStamp           = [[param valueForKey:@"timestamp"] intValue];
    req.package             = [param valueForKey:@"package"];
    req.sign                = [param valueForKey:@"sign"];
    [WXApi sendAuthReq:req viewController:viewController delegate:self completion:nil];
}

///调起支付宝支付
- (void)aliPay:(NSDictionary *)param block:(FinishedBlock)block {
    NSString *sign = [param valueForKey:@"ali_sdk"];
    [[AlipaySDK defaultService] payOrder:sign fromScheme:@"KJTZAliPay" callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
    }];
}

///15余额支付，20兑换券支付（兑换商城）
- (void)balanceExchangePay:(NSDictionary *)param block:(FinishedBlock)block {
    
//    NSString *fullUrl = [NSString stringWithFormat:@"%@/api/order/pay",LcwlServerRoot];
//    [MXNet Post:^(MXNet *net) {
//        net.apiUrl(fullUrl);
//        net.useMsg();
//        net.params(param);
//        net.finish(^(id data){
//            Block_Exec(block,data);
//        }).failure(^(id error){
//            Block_Exec(block,[error description]);
//        })
//        .execute();
//    }];
}

#pragma mark - WXApiDelegate
- (void)onResp:(BaseResp *)resp {
//    if([resp isKindOfClass:[PayResp class]]){
//        //支付返回结果，实际支付结果需要去微信服务器端查询
//        NSString *strMsg,*strTitle = [NSString stringWithFormat:@"支付结果"];
//
//        switch (resp.errCode) {
//            case WXSuccess:
//                strMsg = @"支付结果：成功！";
//                NSLog(@"支付成功－PaySuccess，retcode = %d", resp.errCode);
//                break;
//
//            default:
//                strMsg = [NSString stringWithFormat:@"支付结果：失败！retcode = %d, retstr = %@", resp.errCode,resp.errStr];
//                NSLog(@"错误，retcode = %d, retstr = %@", resp.errCode,resp.errStr);
//                break;
//        }
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }else {
//    }
}

- (void)onReq:(BaseReq *)req {
    
}

///支付宝支付回调
- (void)payCallBack:(NSURL *)url {
    __block NSDictionary *result = nil;
    if ([url.host isEqualToString:@"safepay"] && [url.absoluteString hasPrefix:@"KJTZAliPay"]) {
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSDictionary *info = [[resultDic valueForKey:@"result"] dictionaryWithJsonString];
            if([[info valueForKeyPath:@"alipay_trade_app_pay_response.code"] isEqualToString:@"10000"]) {
                result = @{@"success":@"1",@"type":@"alipay",@"error":@""};
            } else {
                result = @{@"success":@"0",@"type":@"alipay",@"msg":@"支付未完成"};
            }
        }];
    } else if([url.absoluteString hasPrefix:kWechatAppid]) {
        NSDictionary *param = [self paramerWithURL:url];
        BOOL status = [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
        if (param[@"ret"]) {
            if (![param[@"ret"] isEqualToString:@"0"]) {
                status = NO;
            }
        }
        result = @{@"success":status ? @"1" : @"0",@"type":@"wepay",@"msg":@"支付未完成"};
    }
    Block_Exec(self.payCompletionBlock,result);
}

/**

获取url的所有参数

@param url 需要提取参数的url

@return NSDictionary

*/

-(NSDictionary *)paramerWithURL:(NSURL *) url {
    NSMutableDictionary *paramer = [[NSMutableDictionary alloc]init];
    //创建url组件类
    NSURLComponents *urlComponents = [[NSURLComponents alloc] initWithString:url.absoluteString];
    //遍历所有参数，添加入字典
    [urlComponents.queryItems enumerateObjectsUsingBlock:^(NSURLQueryItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [paramer setObject:obj.value forKey:obj.name];
    }];
    return paramer;
}

@end
