//
//  Language.h
//
//  Created by mac on 2019/6/3.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LanguageChangedNotification @"LanguageChangedNotification"

#define Lang(key) [Language getValue:key value:nil]

///头部Placeholder
#define LangLPH(key,placeholder) [NSString stringWithFormat:@"%@%@",placeholder,Lang(key)]

///尾部Placeholder
#define LangRPH(key,placeholder) [NSString stringWithFormat:@"%@%@",Lang(key),placeholder]

///两头Placeholder
#define LangLRPH(key,placeholder) [NSString stringWithFormat:@"%@%@%@",placeholder,Lang(key),placeholder]

//尾部带:
#define LangColon(key) [NSString stringWithFormat:@"%@:",Lang(key),gap]



@protocol LanguageDelegate <NSObject>
@required // 必须实现的方法
-(void)updateForLanguageChanged;
@end


typedef NS_ENUM(NSInteger,LanguageType) {
    LanguageTypeChinese,
    LanguageTypeEnglish,
    LanguageTypeVietnam,
    LanguageTypeIndonesia,
    LanguageTypeKorea,
};

@interface Language : NSObject

+ (void)initialize;
+ (void)setLanguage:(LanguageType)language;
+ (LanguageType)currentLanguageType;
+ (NSString*)currentLanguageName;
+ (void)saveUserSelectedLanguage:(LanguageType)selectedLanguage;
+ (NSString *)getValue:(NSString *)key value:(NSString *)value;

@end
