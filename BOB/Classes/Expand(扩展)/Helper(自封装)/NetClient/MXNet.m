//
//  MXNet.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 16/7/20.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "MXNet.h"
#import "CommonDefine.h"
#import "MXRequest+RequestError.h"
#import "MXBaseRequestApi.h"
#import "MXReachabilityManager.h"
#import "MD5Util.h"
#import "NSString+Ext.h"
#import "RSAManage.h"
#import "AFURLSessionManager.h"
#import "UIAlertView+MKBlockAdditions.h"
#import "AppDelegate+Helper.h"
#import "SecurityUtil.h"
#import "NSString+Common.h"

static NSString* key = @"NHqKBZjgv6MB0C8dmOLecwEowJ4cLVfFb7m9ZA1EWzHtyzSGXTSXzJdY3NxQXPmv";
static NSString* key2 = @"Ylm1bDfcfHXhFy5xNX04PqwaGX61fPU2FOtaTtOGZAc7iEk03x315hxK72DA72NVeWbCFUbF";

static BOOL isLogined;

@interface MXApi : MXBaseRequestApi

@property (nonatomic, strong) NSString        *url;
@property (nonatomic, assign) NSTimeInterval timeoutInterval;
@property (nonatomic, assign) MXRequestMethod method;
@property (nonatomic, strong) NSDictionary    *param;

@end

@implementation MXApi

- (instancetype)init {
    self = [super init];
    if (self) {
        _timeoutInterval = 30;
    }
    return self;
}

- (NSString *)requestUrl {
    return self.url ?: @"";
}

- (MXRequestMethod)requestMXMethod {
    return self.method;
}

- (id)requestArgument {
    return self.param;
}

- (CGFloat)requestTimeoutInterval {
    return self.timeoutInterval;
}

@end

@interface MXNet ()

@property (nonatomic, strong) MXApi         *api;
@property (nonatomic, assign) BOOL          errorRawData;
///是否需要加密
@property (nonatomic, assign) BOOL          needEncrypt;
@property (nonatomic, assign) BOOL          needMsg;
@property (nonatomic, assign) BOOL          needHud;
@property (nonatomic, weak) id showInView; ///hud显示所在的视图，空则显示在window上

@property (nonatomic, strong) id<MXHandleData> dataHandle;
@property (nonatomic, strong) void(^apiFinish)(id);
@property (nonatomic, strong) void(^apiFailure)(id);
@property (nonatomic, strong) void(^apiTimeOut)(void);
@end

@implementation MXNet

- (instancetype)init {
    self = [super init];
    if (self) {
        _api = [[MXApi alloc] init];
    }
    return self;
}

+ (void)Post:(void(^)(MXNet *net))block {
    MXNet *api = [[MXNet alloc] init];
    api.api.method = MXRequestMethodPost;
    
    Block_Exec(block, api);
}

+ (void)Get:(void(^)(MXNet *net))block {
    MXNet *api = [[MXNet alloc] init];
    api.api.method = MXRequestMethodGet;
    
    Block_Exec(block, api);
}

+ (void)Put:(void(^)(MXNet *net))block {
    MXNet *api = [[MXNet alloc] init];
    api.api.method = MXRequestMethodPut;
    
    Block_Exec(block, api);
}

- (MXNet* (^)(NSString *))apiUrl {
    return ^id(NSString *url) {
        self.api.url = url;
        return self;
    };
}

- (MXNet* (^)(void))cache {
    return ^id() {
        self.api.ignoreCache = NO;
        return self;
    };
}

- (MXNet* (^)(void))useMsg {
    return ^id() {
        self.needMsg = YES;
        return self;
    };
}

- (MXNet* (^)(id))useHud {
    return ^id(id superView) {
        self.showInView = superView;
        self.needHud = YES;
        return self;
    };
}

- (MXNet* (^)(void))useEncrypt {
    return ^id() {
        self.needEncrypt = YES;
        return self;
    };
}

- (MXNet* (^)(void))errorRamData {
    return ^id() {
        self.errorRawData = YES;
        return self;
    };
}

- (MXNet* (^)(NSInteger))page {
    return ^id(NSInteger page) {
        self.api.page = @(page);
        return self;
    };
}

- (MXNet* (^)(NSInteger))pageSize {
    return ^id(NSInteger size) {
        self.api.pageSize = @(size);
        return self;
    };
}

static NSString *extracted(MXNet *object, NSMutableDictionary *tmpParam) {
    return [StringUtil calcMD5forString:[object md5Str:tmpParam key:[object.api.url hasPrefix:LcwlServerRoot] ? key : key2]].uppercaseString;
}

- (MXNet* (^)(NSDictionary *))params {
    return ^id(NSDictionary *param) {
        
        NSMutableDictionary* tmpParam = [[NSMutableDictionary alloc]initWithDictionary:param];
        if (![[param allKeys] containsObject:@"token"]) {
            if ([self.api.url hasSuffix:@"createImgCode"] ||
                [self.api.url hasSuffix:@"set_code"] ||
                [self.api.url hasSuffix:@"userForgetPass"] ||
                [self.api.url hasSuffix:@"sendSmsCodeOnly"]) {
                ///不需要token
            } else {
                if (tmpParam[@"login_type"]) {
                    if ([@"account" isEqualToString:tmpParam[@"login_type"]]) {
                        ///不需要token
                    } else {
                        if ([self.api.url containsString:LcwlServerRoot2]) {
                            [tmpParam setValue:UDetail.user.chatToken forKey:@"chat_token"];
                        } else {
                            [tmpParam setValue:UDetail.user.token forKey:@"token"];
                        }
                    }
                } else {
                    if ([self.api.url containsString:LcwlServerRoot2]) {
                        [tmpParam setValue:UDetail.user.chatToken forKey:@"chat_token"];
                    } else {
                        [tmpParam setValue:UDetail.user.token forKey:@"token"];
                    }
                }
            }
        }
        //如果有密码 进行MD5加密
        NSString *key = @"login_password";
        if ([[param allKeys] containsObject:key]) {
            NSString *password = param[key];
            password = [StringUtil calcMD5forString:password];
            [tmpParam setValue:password forKey:key];
        }
        //交易密码
        key = @"pay_password";
        if ([[param allKeys] containsObject:key]) {
            NSString *password = param[key];
            password = [StringUtil calcMD5forString:password];
            [tmpParam setValue:password forKey:key];
        }
        key = @"sys_system_type";
        if (![[param allKeys] containsObject:key]) {
            [tmpParam setValue:@"mallLive" forKey:key];
        }
        if ([StringUtil needSignApi:self.api.url]) {
            [tmpParam setValue:extracted(self, tmpParam) forKey:@"sign"];
        }
        self.api.param = tmpParam;
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tmpParam options:NSJSONWritingPrettyPrinted error:&error];
        NSString* jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
#if DEBUG
        MLog(@"api=%@ 请求参数列表%@", self.api.url,jsonString);
    
        //在header中添加明文请求参数
        NSString *jsonStr = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if([NSString isContainChinese:jsonStr]) {
            jsonStr = [jsonStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
        [self.api.passThroughParam setValue:jsonStr forKey:@"Custom-Param"];
        
#endif
        NSString *privatekey = [SecurityUtil randomlyGenerated16BitString];
        self.api.param = [RSAManage encryptString:jsonString privateKey:privatekey] ?: @"";
        if ([self.api.url containsString:LcwlServerRoot2]) {
            self.api.privateKey = [RSAManage encryptChatString:privatekey];
        } else {
            self.api.privateKey = [RSAManage encryptString:privatekey];
        }                
        return self;
    };
}
-(NSString* )md5Str:(NSDictionary* )dict key:(NSString *)signKey{
    NSArray *keys = [dict allKeys];
    //按字母顺序排序
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSMutableString *contentString = [NSMutableString string];
    //拼接字符串
    for (NSString *categoryId in sortedArray) {
        NSString *value = [dict valueForKey:categoryId];
        if([StringUtil isEmpty:value]) {
            continue;
        }
        [contentString appendFormat:@"%@=%@&", categoryId, [value removeSpaceAndNewline]];
    }
    [contentString appendFormat:@"key=%@", signKey];
    return contentString;
}


- (MXNet* (^)(id<MXHandleData>))parse {
    return ^id(id<MXHandleData> parseData) {
        self.dataHandle = parseData;
        return self;
    };
}

- (MXNet* (^)(void(^)(id)))finish {
    return ^id(void(^f)(id)) {
        self.apiFinish = f;
        return self;
    };
}

- (MXNet* (^)(void(^)(id)))failure {
    return ^id(void(^error)(id)) {
        self.apiFailure = error;
        return self;
    };
}

- (MXNet* (^)(void(^)(void)))timeOut {
    return ^id(void(^time)(void)) {
        self.apiTimeOut = time;
        return self;
    };
}

- (MXNet* (^)(NSTimeInterval))timeoutInterval {
    return ^id(NSTimeInterval time) {
        self.api.timeoutInterval = time;
        return self;
    };
}

- (void (^)(void))execute {
    return ^(void){
        //        if ([MXReachabilityManager isNetWorkReachable]) {
        //            Block_Exec(self.apiFailre, MXLang(@"", @"网络不给力，请再试试啦~"));
        //            return;
        //        }
        [self showHud];
        [self.api startWithCompletionBlockWithSuccess:^(MXRequest *request) {
            isLogined = NO;
            [self hideHud];
            NSDictionary *dict = request.responseJSONObject;
            if(self.needMsg) {
                [NotifyHelper showMessageWithMakeText:Lang([dict valueForKey:@"msg"])];
            }
//            if(dict != nil && [self.api.url hasPrefix:LcwlServerRoot]) {
//                dict = [NSMutableDictionary dictionaryWithDictionary:dict];
//                [dict setValue:@(![[dict valueForKey:@"status"] boolValue]) forKey:@"success"];
//            }
            #if DEBUG
            MLog(@"api = %@ 接口返回数据-----%@", self.api.url, dict);
            #endif
            Block_Exec(self.apiFinish, self.dataHandle ?[self.dataHandle parseData:dict] :dict);
        } failure:^(MXRequest *request) {
            [self hideHud];
            if (![self.api.url containsString:@"modifyUserOnline"]) {
                [NotifyHelper showMessageWithMakeText:[request requestError]];
            }
            // 超时
            if (request.responseStatusCode == -1001 && self.apiTimeOut) {
                self.apiTimeOut();
            } else {
                Block_Exec(self.apiFailure, [request requestError]);
            }

            NSString* responseString = request.responseString;
            if (responseString && [responseString rangeOfString:@"登录已失效"].location != NSNotFound) {
                
                NSString *msg = nil;
    #ifdef DEBUG
                msg = @"当前账户信息：";
                msg = [msg stringByAppendingString:[NSString stringWithFormat:@"account: %@\n",UDetail.user.account]];
                msg = [msg stringByAppendingString:[NSString stringWithFormat:@"token: %@\n",UDetail.user.token]];
                msg = [msg stringByAppendingString:[NSString stringWithFormat:@"chatToken: %@\n",UDetail.user.chatToken]];
                
                msg = [msg stringByAppendingString:@"MXNet failure:\n"];
                msg = [msg stringByAppendingString:[NSString stringWithFormat:@"%@",request.requestUrl]];
                msg = [msg stringByAppendingString:@"\n"];
                
                if(request.custom != nil) {
                    msg = [msg stringByAppendingString:@"参数："];
                    msg = [msg stringByAppendingString:request.custom];
                    msg = [msg stringByAppendingString:@"\n"];
                }
                
                if(request.responseString != nil) {
                    msg = [msg stringByAppendingString:@"返回值："];
                    msg = [msg stringByAppendingString:request.responseString];
                    msg = [msg stringByAppendingString:@"\n"];
                }
    #endif
                
                [MoApp kickOff:msg];
            }
            if (responseString &&
                ([responseString rangeOfString:@"商城系统暂时关闭，请等待开放"].location != NSNotFound)) {
                [MoApp closeAPP];
            }
        }];
    };
}

- (void)showHud {
    if(self.needHud) {
        if([self.showInView isKindOfClass:[UIViewController class]]) {
            [NotifyHelper showHUDAddedTo:[self.showInView view] animated:YES];
        } else if([self.showInView isKindOfClass:[UIView class]]) {
            [NotifyHelper showHUDAddedTo:self.showInView animated:YES];
        } else {
            [NotifyHelper showHUD];
        }
    }
}

- (void)hideHud {
    if(self.needHud) {
        if([self.showInView isKindOfClass:[UIViewController class]]) {
            [NotifyHelper hideHUDForView:[self.showInView view] animated:YES];
        } else if([self.showInView isKindOfClass:[UIView class]]) {
            [NotifyHelper hideHUDForView:self.showInView animated:YES];
        } else {
            [NotifyHelper hideHUD];
        }
    }
}

- (void (^)(void))cancle {
    return ^(void) {
        [self.api stop];
    };
}

@end

