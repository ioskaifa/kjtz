//
//  MBWebVC.m
//  MoPromo_Develop
//
//  Created by yang.xiangbao on 15/5/13.
//  Copyright (c) 2015年 MoPromo. All rights reserved.
//

#import "MBWebVC.h"
#import "AmplifyRespondButton.h"
#import <WebKit/WebKit.h>
#import "FBKVOController.h"
#import "UIImage+Utils.h"
#import "UINavigationBar+Alpha.h"

@interface MBWebVC ()<WKUIDelegate, WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *wkWebView;
///进度条
@property (nonatomic, strong) UIProgressView *progressBar;

@property (nonatomic, copy  ) NSTimer *progressTimer;

///加载完毕
@property (nonatomic, assign) BOOL isDidLoad;

///带关闭的返回按钮
@property (nonatomic, strong) UIView *customNavLeftBtnView;

///关闭按钮
@property (nonatomic, strong) UIButton *closeBtn;

@property (nonatomic, strong) FBKVOController *kvoController;

@end

@implementation MBWebVC

static const NSString *CompanyFirstDomainByWeChatRegister = @"flight.ztlvx.com";
static NSString *endPayRedirectURL = nil;

- (void)dealloc {
    if (self.wkWebView) {
        [self.wkWebView removeObserver:self forKeyPath:@"estimatedProgress" context:nil];
    }
}

#pragma mark - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];            
    [self setNavBarTitle:self.navTitle];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];
//    [request addValue:CompanyFirstDomainByWeChatRegister forHTTPHeaderField:@"referer"];
    self.wkWebView = [[WKWebView alloc] initWithFrame:CGRectZero];
    self.wkWebView.UIDelegate = self;
    self.wkWebView.navigationDelegate = self;
    [self.wkWebView addSubview:self.progressBar];
    self.wkWebView.backgroundColor = [UIColor clearColor];
    self.wkWebView.scrollView.backgroundColor = [UIColor clearColor];
    
    [self.wkWebView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    
    self.wkWebView.hidden = YES;
    [self.view addSubview:self.wkWebView];
    [self.wkWebView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    AdjustTableBehavior(self.wkWebView.scrollView);
    
    if (_content) {
        [self.wkWebView loadHTMLString:_content baseURL:self.baseUrl];
    } else if(self.urlString) {
        [self.wkWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:self.urlString isDirectory:NO]]];
    } else {
        [self.wkWebView loadRequest:request];
    }
    
    self.currentUrlStr = self.urlString;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    if(self.lucencyNavi) {
        self.edgesForExtendedLayout = UIRectEdgeAll;
        [self.navigationController.navigationBar barReset];
        //[self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
        self.navigationController.navigationBar.translucent = YES;
        //[self.navigationController.navigationBar setNavBarCurrentColor:[UIColor gameBackgroundColor] titleTextColor:[UIColor whiteColor]];
        [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                          NSForegroundColorAttributeName : self.navTitle ? [UIColor whiteColor] : [UIColor clearColor],
                                                                          NSFontAttributeName : [UIFont font19]
                                                                          }];
        [self.navigationController.navigationBar setBackgroundImage:[UIImage createImageWithColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
        [self setNavBarLeftBtnImg:@"public_white_back"];
        [self setIsShowBackButton:YES];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.progressTimer) {
        [self.progressTimer invalidate];
        self.progressTimer = nil;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.wkWebView.hidden = NO;
}

- (FBKVOController *)kvoController {
    if (!_kvoController) {
        _kvoController = [FBKVOController controllerWithObserver:self];
    }
    
    return _kvoController;
}

#pragma mark - SuperEvent
- (void)backAction:(id)sender {
    if (self.wkWebView.canGoBack) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.customNavLeftBtnView];
        self.closeBtn.hidden = NO;
        [self.wkWebView goBack];
    } else {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.backBut];
        self.closeBtn.hidden = YES;
        [super backAction:sender];
    }
}

- (void)navBarRightBtnAction:(UIView *)sender {
    
}

#pragma mark - Public Method
- (id)getCurrentWebView {
    return self.wkWebView;
}

- (NSString *)currentUrlStr {
    return self.wkWebView.URL.absoluteString;
}

#pragma mark - Private
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        self.progressBar.alpha = 1.0;
        [self.progressBar setProgress:self.wkWebView.estimatedProgress animated:YES];
        
        if (self.wkWebView.estimatedProgress >= 1.0) {
            [UIView animateWithDuration:0.5 animations:^{
                self.progressBar.alpha = 0.0;
            } completion:^(BOOL finished) {
                if (finished) {
                    self.progressBar.progress = 0.0;
                }
            }];
        }
    }
}

- (void)progressCallBack {
    if (self.isDidLoad) {
        if (self.progressBar.progress >= 1) {
            [self.progressTimer setFireDate:[NSDate distantFuture]];
            [UIView animateWithDuration:0.5 animations:^{
                self.progressBar.hidden = YES;
            } completion:^(BOOL finished) {
                if (finished) {
                    self.progressBar.progress = 0.0;
                }
            }];
        } else {
            self.progressBar.progress += 0.1;
        }
    } else {
        self.progressBar.progress += 0.05;
        if (self.progressBar.progress >= 0.95) {
            self.progressBar.progress = 0.95;
        }
    }
}

#pragma mark - IBActions
- (void)closeBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Delegate
#pragma mark WKNavigationDelegate
- (void)webView:(WKWebView *)webView didFinishLoadingNavigation:(WKNavigation *)navigation {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    if (!navigationAction.targetFrame.isMainFrame) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    self.navBarRightBtn.enabled = NO;
}

- (void)webView:(WKWebView *)webView didFinishNavigation: (WKNavigation *)navigation{
    if (![StringUtil isEmpty:webView.title]) {
        if (![StringUtil isEmpty:self.title]) {
            self.title = self.title;
        } else {
            
            self.title = webView.title;
        }
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.navBarRightBtn.enabled = YES;
//    [self.wkWebView evaluateJavaScript:self.jsContent completionHandler:nil];
    
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    if (decisionHandler) {
        decisionHandler(WKNavigationResponsePolicyAllow);
    }
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    MLog(@"decisionHandler ---- %@", navigationAction.request.URL.absoluteString)
    WKNavigationActionPolicy actionPolicy = WKNavigationActionPolicyAllow;
    NSString*urlString = navigationAction.request.URL.absoluteString;
    urlString = [urlString stringByRemovingPercentEncoding];
    NSURLRequest *request        = navigationAction.request;
    NSString     *scheme         = [request.URL scheme];
    if ([urlString hasPrefix:@"https://wx.tenpay.com/cgi-bin/mmpayweb-bin/checkmweb"] &&
        ![urlString hasSuffix:StrF(@"redirect_url=com.%@://", CompanyFirstDomainByWeChatRegister)]) {
        decisionHandler(WKNavigationActionPolicyCancel);
        NSString *redirectUrl = nil;
        if ([urlString containsString:@"redirect_url="]) {
            NSRange redirectRange = [urlString rangeOfString:@"redirect_url"];
            endPayRedirectURL =  [urlString substringFromIndex:redirectRange.location+redirectRange.length+1];
            redirectUrl = [[urlString substringToIndex:redirectRange.location] stringByAppendingString:[NSString stringWithFormat:@"redirect_url=com.%@://",CompanyFirstDomainByWeChatRegister]];
        }else {
            redirectUrl = [urlString stringByAppendingString:[NSString stringWithFormat:@"&redirect_url=com.%@://",CompanyFirstDomainByWeChatRegister]];
        }
        
        NSMutableURLRequest *newRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:redirectUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
        newRequest.allHTTPHeaderFields = request.allHTTPHeaderFields;
        newRequest.URL = [NSURL URLWithString:redirectUrl];
        [webView loadRequest:newRequest];
        return;
    }
    
    if([urlString containsString:@"about:blank"]) {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    } else if (![scheme isEqualToString:@"https"] && ![scheme isEqualToString:@"http"]) {
        decisionHandler(WKNavigationActionPolicyCancel);
        if ([scheme isEqualToString:@"weixin"]) {
            if (endPayRedirectURL) {
                [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:endPayRedirectURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10]];
                endPayRedirectURL = nil;
            }
        }
        BOOL canOpen = [[UIApplication sharedApplication] canOpenURL:request.URL];
        if (canOpen) {
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:request.URL
                                                   options:@{UIApplicationOpenURLOptionUniversalLinksOnly: @NO}
                                         completionHandler:^(BOOL success) {
                }];
            } else {
                [[UIApplication sharedApplication] openURL:request.URL];
            }
        }
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

#pragma mark WKUIDelegate
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (completionHandler) {
            completionHandler();
        }
    }]];
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(NO);
    }])];
    [alertController addAction:([UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(YES);
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = defaultText;
    }];
    [alertController addAction:([UIAlertAction actionWithTitle:@"完成" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(alertController.textFields[0].text?:@"");
    }])];
    
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Init
- (UIProgressView *)progressBar {
    if (!_progressBar) {
        _progressBar = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
        _progressBar.tintColor = [UIColor moOrange];
        _progressBar.progress = 0.0f;
    }
    
    return _progressBar;
}

- (NSTimer *)progressTimer {
    if (!_progressTimer) {
        _progressTimer = [NSTimer scheduledTimerWithTimeInterval:1/6 target:self selector:@selector(progressCallBack) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_progressTimer forMode:NSDefaultRunLoopMode];
    }
    
    return _progressTimer;
}

- (UIView *)customNavLeftBtnView {
    if (!_customNavLeftBtnView) {
        _customNavLeftBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 40)];
        
        AmplifyRespondButton *backBtn = [[AmplifyRespondButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [backBtn setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
        [backBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -15, 0, 0)];
        backBtn.imageView.tintColor = [UIColor blackColor];
        [_customNavLeftBtnView addSubview:backBtn];
        [_customNavLeftBtnView addSubview:self.closeBtn];
    }
    
    return _customNavLeftBtnView;
}

- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(25, 0, 40, 40)];
        [_closeBtn setImage:[UIImage imageNamed:@"icon_public_close"] forState:UIControlStateNormal];
        [_closeBtn setImage:[UIImage imageNamed:@"icon_public_close"] forState:UIControlStateHighlighted];
        _closeBtn.hidden = YES;
        [_closeBtn addTarget:self action:@selector(closeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _closeBtn;
}

@end
