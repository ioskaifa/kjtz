//
//  MBWebVC+Helper.m
//  MoPal_Developer
//
//  Created by lhy on 16/10/25.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "MBWebVC+Helper.h"
#import <WebKit/WebKit.h>

#import <objc/runtime.h>

static char shareUrlKey;

static char shareTypeKey;

#import "StringUrl.h"

typedef NS_ENUM(NSInteger, OpenUrlType){
    OpenUrlTypeStoreHome        =1, //店铺首页
    OpenUrlTypePersonalDetail,      //个人中心
    OpenUrlTypeMoGrabInfoPerfect    //魔抢订单信息完善
};


static NSString * const storeHomeUrl         = @"mo_page/mo_biz/shop-preview";
static NSString * const personalDetailUrl    = @"mo_grab/type=userCenter";
static NSString * const moGrabInfoPerfectUrl = @"mo_grab/type=orderdetail?";

@implementation MBWebVC (Helper)

#pragma mark - Setter/Getter Method
- (void)setShareUrl:(NSString *)shareUrl {
    objc_setAssociatedObject(self, &shareUrlKey, shareUrl, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)shareUrl {
    return objc_getAssociatedObject(self, &shareUrlKey);
}

- (void)setShareType:(MoGrabShareType)type {
    objc_setAssociatedObject(self, &shareTypeKey, @(type), OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (MoGrabShareType)shareType {
    return (MoGrabShareType)[objc_getAssociatedObject(self, &shareTypeKey) integerValue];
}


#pragma mark 拦截webview跳转 跳转到app原生页
- (BOOL)handleOpenUrlOnApp:(NSString *)url {
    BOOL result = NO;
    if ([StringUtil isEmpty:url]) {
        return NO;
    }
    
    return result;
}

#pragma mark - 分享
- (void)shareWeb:(NSString *)urlStr {
//    MLog()
//    if ([StringUtil isEmpty:urlStr]) {
//        return;
//    }
//
//    self.shareUrl = [urlStr copy];
//
//    MoGrabShareType moGrabShareType = MoGrabShareTypeGrabing;
//    if ([urlStr rangeOfString:moGrab_grabing].length > 0) {
//        moGrabShareType = MoGrabShareTypeGrabing;
//    } else if ([urlStr rangeOfString:moGrab_coming].length > 0){
//        moGrabShareType = MoGrabShareTypeComing;
//    } else if ([urlStr rangeOfString:moGrab_winner].length > 0){
//        moGrabShareType = MoGrabShareTypeWinner;
//    } else if ([urlStr rangeOfString:moGrab_detail].length > 0){
//        moGrabShareType = MoGrabShareTypeDetail;
//    }
//
//    self.shareType = moGrabShareType;
//    switch (moGrabShareType) {
//        case MoGrabShareTypeGrabing:
//        case MoGrabShareTypeComing:
//        case MoGrabShareTypeWinner:{
//            [self shareWebUrl:urlStr
//                        title:self.title
//                        image:[UIImage imageNamed:@"moGrabShare"]
//                      content:@""];
//            break;
//        }
//
//        case MoGrabShareTypeDetail:{
//            [self getShareImageByElementId:@"shareImage"];
//            break;
//        }
//        default:{
//            break;
//        }
//    }
}

//处理魔抢的分享的URL
//- (NSString *)handleGrabShareURL:(NSString *)shareUrl shareType:(ShareType)type{
//    if ([shareUrl rangeOfString:@"moxian.com/mo_grab"].location == NSNotFound) {
//        return shareUrl;
//    }
//    NSInteger mxShareType = [self getMXShareType:type];
//    NSString *shareEntityId = [self getInfoIdFromUrl:shareUrl];
//    NSString *joinStr = ([shareUrl rangeOfString:@"?"].location == NSNotFound) ? @"?" : @"&";
//
//    NSString *resultUrl = [NSString stringWithFormat:@"%@%@shareEntityId=%@&shareType=%@&userId=%@",shareUrl,joinStr,shareEntityId,@(mxShareType),AppUserModel.userId];
//    return resultUrl;
//}


#pragma mark - Share
- (void)shareWebUrl:(NSString *)shareUrl
              title:(NSString *)shareTitle
              image:(id)shareImage
            content:(NSString *)shareContent {
    if ([StringUtil isEmpty:shareUrl]) {
        return;
    }
    
//    [MXSharedMenu showSharedMenuInView:self.view completion:^(ShareType type, NSInteger rowIndex) {
//        if (type==ShareTypeWeixiSession || type==ShareTypeWeixiTimeline) {
//            NSString *resultUrl = [self handleGrabShareURL:shareUrl shareType:type];
//            
//            [MXShareManager shareContentToWeiXin:shareContent imagePath:shareImage title:shareTitle url:resultUrl type:type result:^(SSResponseState state, id<ICMErrorInfo> error) {
//                if (state == SSResponseStateSuccess) {
//                    [self shareMoGrabGiveAwardWithGrabShareUrl:shareUrl shareType:type];
//                } else if (state == SSResponseStateFail) {
//                    NSString *errorStr = error.errorDescription;
//                    MLog(@"%@", errorStr)
//                }
//            }];
//        } else if(type == copyKey) {
//            //复制到粘贴板
//            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
//            NSString *resultUrl = [self handleGrabShareURL:shareUrl shareType:type];
//            pasteboard.string = resultUrl;
//            [NotifyHelper showMessageWithMakeText:MXLang(@"", @"链接复制成功~")];
//        } else {
//            if (type == ShareTypeSinaWeibo) {
//                //微博分享urlStr不会分享出去，只能拼到shareContent
//                NSString *contentAndUrl = [NSString stringWithFormat:@"%@%@%@",shareTitle,(shareContent?:@""),shareUrl];
//                contentAndUrl = [self handleGrabShareURL:contentAndUrl shareType:type];
//                //判断是否安装微博
//                if ([WeiboSDK isWeiboAppInstalled]) {
//                    [MXShareManager shareForClientContent:contentAndUrl imagePath:shareImage title:shareTitle url:shareUrl type:type result:^(SSResponseState readCount) {
//                        if (readCount == SSResponseStateSuccess) {
//                            [self shareMoGrabGiveAwardWithGrabShareUrl:shareUrl shareType:type];
//                        }
//                    }];
//                } else {
//                    [MXShareManager clientShareContent:contentAndUrl imagePath:shareImage title:shareTitle url:shareUrl type:type result:^(SSResponseState readCount) {
//                        if (readCount == SSResponseStateSuccess) {
//                            [self shareMoGrabGiveAwardWithGrabShareUrl:shareUrl shareType:type];
//                        }
//                    }];
//                }
//            } else {
//                NSString *resultUrl = [self handleGrabShareURL:shareUrl shareType:type];
//                [MXShareManager shareForClientContent:shareContent imagePath:shareImage title:shareTitle url:resultUrl type:type result:^(SSResponseState readCount) {
//                    if (readCount == SSResponseStateSuccess) {
//                        [self shareMoGrabGiveAwardWithGrabShareUrl:shareUrl shareType:type];
//                    }
//                }];
//            }
//        }
//    }];
}

#pragma mark - 根据id名称获取图片元素
- (void)getShareImageByElementId:(NSString *)idStr {
    if ([StringUtil isEmpty:idStr]) {
        return;
    }
    
//    NSString *documentStr = [NSString stringWithFormat:@"var address = document.getElementById(\"%@\");", idStr];
//    documentStr = [NSString stringWithFormat:@"%@%@", documentStr, @"address.src"];
//
//    @weakify(self)
//    if ([[self getCurrentWebView] isKindOfClass:[WKWebView class]]) {
//        [(WKWebView *)[self getCurrentWebView] evaluateJavaScript:documentStr completionHandler:^(id sender, NSError * _Nullable error) {
//            @strongify(self)
//            //分享图片
//            id shareImage= sender;
//
//            if ([StringUtil isEmpty:shareImage]) {
//                shareImage = [UIImage imageNamed:@"moGrabShare"];
//            }
//
//            [self shareWebUrl:self.shareUrl
//                        title:self.title
//                        image:shareImage
//                      content:@""];
//        }];
//    } else {
//        //分享图片
//        id shareImage= [(UIWebView *)[self getCurrentWebView] stringByEvaluatingJavaScriptFromString:documentStr];
//
//        if (!shareImage) {
//            shareImage = [UIImage imageNamed:@"moGrabShare"];
//        }
//
//        [self shareWebUrl:self.shareUrl
//                    title:self.title
//                    image:shareImage
//                  content:@""];
//    }
}

///获取url中的infoId
//- (NSString *)getInfoIdFromUrl:(NSString *)url {
//    NSString *infoId;
//    if ([StringUtil isEmpty:url]) {
//        return infoId;
//    }
//    if ([url rangeOfString:@"?infoId="].location == NSNotFound) {
//        return @"0";
//    }
//    infoId = [[url componentsSeparatedByString:@"?infoId="] lastObject];
//    return infoId;
//}

///分享目标平台,1微信好友,2微信朋友圈 3微博,4 手机QQ,5 QQ空间,6facebook ,7 twitter
//- (NSInteger)getMXShareType:(ShareType)shareType{
//    NSInteger mxShareType;
//    switch (shareType) {
//        case ShareTypeWeixiSession:{
//            mxShareType = 1;
//            break;
//        }
//        case ShareTypeWeixiTimeline:{
//            mxShareType = 2;
//            break;
//        }
//        case ShareTypeSinaWeibo:{
//            mxShareType = 3;
//            break;
//        }
//        case ShareTypeQQ:{
//            mxShareType = 4;
//            break;
//        }
//        case ShareTypeFacebook:{
//            mxShareType = 6;
//            break;
//        }
//        case ShareTypeTwitter:{
//            mxShareType = 7;
//            break;
//        }
//        default:
//            mxShareType = 1;
//            break;
//    }
//    
//    return mxShareType;
//}
//
//#pragma mark - NetWork
////WIKI http://wiki2.moxian.com/index.php?title=%E9%AD%94%E6%8A%A2%E5%BA%97%E9%93%BA%E7%82%B9%E5%87%BB
/////魔抢店铺点击统计
//- (void)clickMoGrabShop:(NSString *)shopId {
//    if ([StringUtil isEmpty:shopId]) {
//        MLog(@"参数有误")
//        return;
//    }
//    
//    //special by lhy 2017年02月27日09:48:05  店铺ID 需要long
////    NSInteger shopidl = [shopId integerValue];
//    NSString *url = MOXIAN_URL_STR_NEW(@"grab", @"/mo_grab/m2/shop/recommend");
//    [MXNet Post:^(MXNet *net) {
//        net.apiUrl(url).params(@{@"shopId":shopId})
//        .finish(^(id data){
//            MLog(@"魔抢店铺点击统计sucess")
//        }).failure(^(id error){
//            MLog(@"魔抢店铺点击统计failure")
//        })
//        .execute();
//    }];
//}
//
////WIKI http://wiki2.moxian.com/index.php?title=%E9%AD%94%E6%8A%A2%E5%88%86%E4%BA%AB%E9%80%81%E5%88%86
/////魔抢分享送积分
//- (void)shareMoGrabGiveAwardWithGrabShareUrl:(NSString *)url shareType:(ShareType) shareType{
//    if ([StringUtil isEmpty:url]) {
//        MLog(@"参数有误")
//        return;
//    }
//    
//    NSString *apiUrl = MOXIAN_URL_STR_NEW(@"grab", @"/mo_grab/m2/grabshareactivity/award");
//    NSInteger sourceType;
//    NSString *shareEntityId = @"";
//    NSInteger type = 1;
//    //分享的途径,1 APP分享产品,2 APP分享列表,3网页分享产品,4网页分享列表
//    if (self.shareType == MoGrabShareTypeDetail) {
//        sourceType = 1;
//        shareEntityId = [self getInfoIdFromUrl:url];
//    } else {
//        sourceType = 2;
//    }
//    
//    type = [self getMXShareType:shareType];
//    
//    NSDictionary *parems = @{@"sourceType":@(sourceType),@"shareEntityId":shareEntityId, @"type":@(type)};
//    
//    [MXNet Post:^(MXNet *net) {
//        net.apiUrl(apiUrl).params(parems)
//        .finish(^(id data){
//            if (data && [data isKindOfClass:[NSDictionary class]]) {
//                BOOL result = [data[@"result"] boolValue];
//                NSInteger points = [data[@"data"] integerValue];
//                if (result && points > 0) {
//                    NSString *title = [NSString stringWithFormat:@"%@%@", MXLang(@"", @"积分+"), @(points)];
//                    NSString *tip = MXLang(@"", @"分享成功");
//                    MXDailyTaskRewardView *customView = [[MXDailyTaskRewardView alloc] initWithFrame:CGRectMake(0, 0, 100, 100) withTitle:title withTip:tip];
//                    [NotifyHelper showMXCustomView:customView animated:YES];
//                }
//            }
//        }).failure(^(id error){
//            MLog(@"分享魔抢送积分failure")
//        })
//        .execute();
//    }];
//}
@end
