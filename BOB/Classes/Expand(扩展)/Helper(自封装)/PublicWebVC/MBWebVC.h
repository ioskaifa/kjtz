//
//  MBWebVC.h
//  MoPromo_Develop
//
//  Created by yang.xiangbao on 15/5/13.
//  Copyright (c) 2015年 MoPromo. All rights reserved.
//

#import "BaseViewController.h"

@interface MBWebVC : BaseViewController

// 标题
@property (nonatomic,   copy) NSString *navTitle;
// 网址
@property (nonatomic, copy) NSString *urlString;

@property (nonatomic, strong) NSURL *baseUrl;
// html 内容
@property (nonatomic,   copy) NSString *content;

@property (nonatomic,   copy) NSString *jsContent;

// 网页中所有图片
@property (nonatomic, strong) NSArray *picUrlsArr;

@property (nonatomic, assign) BOOL webTitleWillDynamicChange;

//导航栏透明
@property (nonatomic, assign) BOOL lucencyNavi;

///当前web的Url字符串
@property (nonatomic, copy) NSString *currentUrlStr;

- (id)getCurrentWebView;

@end
