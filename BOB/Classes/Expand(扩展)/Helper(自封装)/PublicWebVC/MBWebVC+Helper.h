//
//  MBWebVC+Helper.h
//  MoPal_Developer
//
//  Created by lhy on 16/10/25.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "MBWebVC.h"

typedef NS_ENUM(NSInteger, MoGrabShareType){
    MoGrabShareTypeGrabing      =1, //进行中
    MoGrabShareTypeComing,          //即将开始
    MoGrabShareTypeWinner,          //已开奖
    MoGrabShareTypeDetail           //详情
};

static NSString * const moGrab_grabing = @"mo_grab/grabing";
static NSString * const moGrab_coming  = @"mo_grab/coming";
static NSString * const moGrab_winner  = @"mo_grab/winner";
static NSString * const moGrab_detail  = @"mo_grab/detail?infoId";

@interface MBWebVC (Helper)

@property (nonatomic, copy) NSString *shareUrl;

@property (nonatomic, assign) MoGrabShareType shareType;

///拦截webview跳转 跳转到app原生页
- (BOOL)handleOpenUrlOnApp:(NSString *)url;

///分享
- (void)shareWeb:(NSString *)urlStr;

@end
