//
//  MXRemotePush.h
//  MoPal_Developer
//
//  个推推送统一的入口使用步骤:
//  1. 调用 start 方法
//  2. 调用 registerRemoteNotification 方法
//
//  Created by yang.xiangbao on 15/4/8.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MXPushModel;

@interface MXRemotePush : NSObject

@property (nonatomic, copy, readonly) NSString *deviceToken;
@property (nonatomic, copy, readonly) NSString *voipToken;

// 单例
+ (MXRemotePush *)sharedInstance;

// 注册推送通知
// 在 - (BOOL)application:didFinishLaunchingWithOptions: 中调用
- (void)registerRemoteNotification:(UIApplication *)application;
// 注册VoIP push
- (void)registerVoIPPush:(UIApplication *)application;

// 向服务器注册deviceToken
// 在 application:didRegisterForRemoteNotificationsWithDeviceToken: 中调用
- (void) registerForRemoteNotificationsForGeTuiWithDeviceToken:(NSData*)deviceToken;

- (void)registerDeviceTokenToServer;

// APNS注册失败
// 在 application:didFailToRegisterForRemoteNotificationsWithError: 中调用
- (void) registerGeTuiError;

///告诉服务器本地已离线,消息走离线推送
///applicationWillTerminate中调用
+ (void)userOffline:(NSString *)on;
///APP进入后台后，收到消息,做本地推送
+ (void)didReceiveRemoteNotificationBackground:(MessageModel *)obj;
///设置APP角标
+ (void)setApplicationIconBadgeNumber;

- (void)registerDeviceTokenToServer;

@end
