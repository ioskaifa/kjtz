//
//  MXRemotePush.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/4/8.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "MXRemotePush.h"
#import "MXPushModel.h"
#import "NSObject+Property.h"
#import <YYKit/NSObject+YYModel.h>
#import "BaseNavigationController.h"
#import "GVUserDefaults+Properties.h"
#import "UserModel.h"
#import <UserNotifications/UserNotifications.h>
#import "LcwlChat.h"
#import <PushKit/PushKit.h>
//#import <CallKit/CallKit.h>
#import "AudioVibrateManager.h"
#import <AudioToolbox/AudioToolbox.h>

API_AVAILABLE(ios(10.0))//CXProviderDelegate
@interface MXRemotePush () <UNUserNotificationCenterDelegate, PKPushRegistryDelegate >

@property (nonatomic, copy) NSString *deviceToken;
@property (nonatomic, copy) NSString *voipToken;

@property (nonatomic, strong) NSUUID * uuid;
//@property (nonatomic, strong) CXProvider * provider;
//@property (nonatomic, strong) CXCallController * cxCallController;
//@property (nonatomic, readonly) CXProviderConfiguration * providerConfig;

@end

@implementation MXRemotePush

+ (MXRemotePush *)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (NSString *)deviceToken {
    if ([StringUtil isEmpty:_deviceToken]) {
        _deviceToken = [MXCache valueForKey:@"deviceToken"];
    }
    return _deviceToken;
}

//#pragma mark - 注册推送通知
- (void)registerRemoteNotification:(UIApplication *)application {
    if (@available(iOS 10.0, *)) {
        //iOS10特有
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        // 必须写代理，不然无法监听通知的接收与点击
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (granted) {
                // 点击允许
                [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
                    MLog(@"%@", settings);
                }];
            } else {
                MLog(@"注册失败");
            }
        }];
    } else if (@available(iOS 8.0, *)){
        //iOS8 - iOS10
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeSound | UIUserNotificationTypeBadge categories:nil]];

    } else {
        //iOS8系统以下
        [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
    }
    // 注册获得device Token
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)registerVoIPPush:(UIApplication *)application {
    PKPushRegistry * pushRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    pushRegistry.delegate = self;
    pushRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}

#pragma mark - PKPushRegistryDelegate
-(void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(PKPushType)type{
    if ([credentials.token length] == 0) {
        MLog(@"voip token NULL");
        return;
    }
    NSMutableString *deviceTokenString = [NSMutableString string];
    const char *bytes = credentials.token.bytes;
    NSInteger count = credentials.token.length;
    for (int i = 0; i < count; i++) {
        [deviceTokenString appendFormat:@"%02x", bytes[i]&0x000000FF];
    }
    MLog(@"VoIP token ....%@", deviceTokenString)
    self.voipToken = deviceTokenString;
//    [self registerDeviceToken:self.deviceToken voipToken:self.voipToken];
}

-(void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(PKPushType)type{
    if (type != PKPushTypeVoIP) {
        return;
    }
        
    NSDictionary *dataDic = payload.dictionaryPayload;
    NSDictionary *apsDic = dataDic[@"aps"];
//    NSInteger msgType = [apsDic[@"msgType"] integerValue];
    NSString *alert = apsDic[@"alert"];
//    BOOL isCallKit = (msgType == kMXVoiceChatTypeAsk);
    UIApplicationState applicationState = [UIApplication sharedApplication].applicationState;
    switch (applicationState) {
        case UIApplicationStateActive:
        case UIApplicationStateInactive:{
            //不处理,显示app接听界面
            if (self.uuid) {
//                [self endCall];
            }
            break;
        }
        case UIApplicationStateBackground:{
            if (@available(iOS 10.0, *)) {
                if (self.uuid) {
                    return;
                }
                self.uuid = [NSUUID UUID];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PKPushIncomingCallReportedNotification" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AVAudioSessionPickableRouteChangeNotification" object:nil];
                [self meetingLocalNotifi:alert];
//                CXCallUpdate * callUpdate = [[CXCallUpdate alloc] init];
//                callUpdate.remoteHandle = [[CXHandle alloc] initWithType:CXHandleTypeGeneric value:alert];
//                callUpdate.hasVideo = NO;
//                [self.provider reportNewIncomingCallWithUUID:self.uuid update:callUpdate completion:^(NSError * _Nullable error) {
//                    if (error) {
//                        MLog(@"report error:%@",error.description);
//                    } else {
//                        MLog(@"report success:----");
//                        [self performSelector:@selector(endCall) withObject:nil afterDelay:60];
//                    }
//                }];
            } else {
                [self meetingLocalNotifi:alert];
            }
            
        }
        default:
            break;
    }

}

-(void)meetingLocalNotifi:(NSString *)fromUserName{
    MLog(@"VoIP 推送.....%@", fromUserName)
    if (@available(iOS 10.0, *)) {
        UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
        UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
        content.body = fromUserName;
//        UNNotificationSound *customSound = [UNNotificationSound soundNamed:@"voip_call.caf"];
        content.sound = [UNNotificationSound defaultSound];
        UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"Voip_Push"  content:content trigger:trigger];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            [AudioVibrateManager playContinuedVibrate];
        }];
    } else {
        UILocalNotification *callNotification = [[UILocalNotification alloc] init];
        callNotification.alertBody = fromUserName;
        
        callNotification.soundName = UILocalNotificationDefaultSoundName;
        callNotification.alertAction = @"查看";
        [[UIApplication sharedApplication]
         presentLocalNotificationNow:callNotification];
        [AudioVibrateManager playContinuedVibrate];
    }
}

#pragma mark - CXProviderDelegate
//- (void)providerDidReset:(CXProvider *)provider{
//    MLog(@"callkit - reset");
//    [self endCall];
//}
//
//-(void)provider:(CXProvider *)provider timedOutPerformingAction:(CXAction *)action{
//    MLog(@"callkit - timeout");
//    [self endCall];
//}
//
//- (void)provider:(CXProvider *)provider performAnswerCallAction:(CXAnswerCallAction *)action{
//    MLog(@"callkit - answercallaction");
//    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(endCall) object:nil];
//
//    [action fulfill];
//}
//
//- (void)provider:(CXProvider *)provider performEndCallAction:(CXEndCallAction *)action{
//    MLog(@"callkit - callend");
////    [g_notify postNotificationName:kCallEndNotification object:nil];
//    [self endCall];
//    [action fulfill];
//}
//
//- (void)provider:(CXProvider *)provider didActivateAudioSession:(AVAudioSession *)audioSession{
//    MLog(@"callkit - callanswer");
////    [g_notify postNotificationName:kCallAnswerNotification object:nil];
//}
//- (void)provider:(CXProvider *)provider performSetMutedCallAction:(CXSetMutedCallAction *)action{
//    MLog(@"callkit - setmuted");
////    [g_notify postNotificationName:kCallSetMutedNotification object:[NSNumber numberWithBool:action.muted]];
//}
//
//
//- (void)provider:(CXProvider *)provider didDeactivateAudioSession:(AVAudioSession *)audioSession{
//    NSLog(@"callkit - deactivate");
//    switch ([UIApplication sharedApplication].applicationState) {
//        case UIApplicationStateActive:
//        case UIApplicationStateInactive:{
//            break;
//        }
//        case UIApplicationStateBackground:
//        default:{
////            [self applicationDidEnterBackground:[UIApplication sharedApplication]];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                exit(0);
//            });
//            break;
//        }
//    }
//}

//#pragma mark - 向服务器注册deviceToken
- (void)registerForRemoteNotificationsForGeTuiWithDeviceToken:(NSData*)deviceToken {
    NSMutableString *deviceTokenString = [NSMutableString string];
    const char *bytes = deviceToken.bytes;
    NSInteger count = deviceToken.length;
    for (int i = 0; i < count; i++) {
        [deviceTokenString appendFormat:@"%02x", bytes[i]&0x000000FF];
    }
    self.deviceToken = deviceTokenString;
    MLog(@"deviceToken:%@", self.deviceToken);

    [MXCache setValue:self.deviceToken forKey:@"deviceToken"];

    // [3]:向服务器注册deviceToken
//    [self registerDeviceToken:self.deviceToken voipToken:self.voipToken];
}

- (void)registerDeviceTokenToServer {
    [self registerDeviceToken:self.deviceToken voipToken:self.voipToken];
}

- (void)registerDeviceToken:(NSString *)deviceToken voipToken:(NSString *)voipToken {
    if ([StringUtil isEmpty:UDetail.user.chatToken]) {
        return;
    }
    NSString *url = StrF(@"%@%@", LcwlServerRoot2, @"/api/user/login/updateDeviceInfo");
    NSDictionary *param = @{@"device_type":@"iOS",
                            @"device_no":deviceToken?:@"",
                            @"device_no2":voipToken?:@"",
                            @"version_no":StrF(@"%0.2f", [DeviceManager getIOSVersion]),
#ifdef DEBUG
                            @"envir_type":@"debug",
#else
                            @"envir_type":@"release",
#endif
                            @"app_version":[DeviceManager getAppBundleVersion]?:@""};
    NSLog(@"token = %@", deviceToken);
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
            
        }).failure(^(id error){
            
        })
        .execute();
    }];
}

#pragma mark - 如果APNS注册失败
- (void)registerGeTuiError {
 
}

- (void)didReceiveRemoteNotificationBackground:(NSDictionary *)userInfo {
    [[UIApplication sharedApplication] cancelAllLocalNotifications];    
}

+ (void)userOffline:(NSString *)on {
    if ([StringUtil isEmpty:UDetail.user.chatToken]) {
        return;
    }
    NSString *url = StrF(@"%@%@", LcwlServerRoot2, @"/api/user/info/modifyUserOnline");
    NSDictionary *param = @{@"is_online":on};
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(param)
        .finish(^(id data){
        }).failure(^(id error){
        })
        .execute();
    }];
}

/// 判断用户是否允许接收通知
+ (void)checkUserNotificationEnable:(FinishedBlock)complete {
    if (@available(iOS 10.0, *)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            if (settings.notificationCenterSetting == UNNotificationSettingEnabled) {
                if (complete) {
                    complete(@(1));
                }
            } else {
               complete(@(0));
            }
        }];
    } else {
        if ([[UIApplication sharedApplication] currentUserNotificationSettings].types == UIUserNotificationTypeNone){
            complete(@(0));
        } else {
            complete(@(1));
        }
    }
}

+ (void)CheckPushPermissions {
    
}

+ (void)didReceiveRemoteNotificationBackground:(MessageModel *)obj {
    if (![obj isKindOfClass:MessageModel.class]) {
        return;
    }
    [self checkUserNotificationEnable:^(id data) {
        BOOL result = [data boolValue];
        if (result) {
            if (@available(iOS 10.0, *)) {
                UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
                UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
                // 标题
                content.title = @"测试标题";
                // 内容
                content.body = @"发来一条信息";
                // 声音
//               // 默认声音
//                 content.sound = [UNNotificationSound defaultSound];
//             // 添加自定义声音
//                content.sound = [UNNotificationSound soundNamed:@"Alert_ActivityGoalAttained_Salient_Haptic.caf"];
                // 角标 （我这里测试的角标无效，暂时没找到原因）
//                content.badge = @0;
                // 多少秒后发送,可以将固定的日期转化为时间
                NSTimeInterval time = [[NSDate dateWithTimeIntervalSinceNow:1] timeIntervalSinceNow];
                // repeats，是否重复，如果重复的话时间必须大于60s，要不会报错
                UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:time repeats:NO];
                
                // 添加通知的标识符，可以用于移除，更新等操作
                NSString *identifier = @"noticeId";
                UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:content trigger:trigger];
                [center addNotificationRequest:request withCompletionHandler:^(NSError *_Nullable error) {
                    NSLog(@"成功添加推送");
                }];
            } else {
                UILocalNotification *notif = [[UILocalNotification alloc] init];
                // 发出推送的日期
                notif.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                // 推送的内容
                notif.alertBody = @"你已经10秒没出现了";
                // 可以添加特定信息
                notif.userInfo = @{@"noticeId":@"00001"};
                // 角标
                notif.applicationIconBadgeNumber = 1;
                // 提示音
                notif.soundName = UILocalNotificationDefaultSoundName;
                // 每周循环提醒
                notif.repeatInterval = NSCalendarUnitWeekOfYear;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:notif];
            }
        }
    }];
}

+ (void)setApplicationIconBadgeNumber {
    NSInteger nums = [[LcwlChat shareInstance].chatManager getAllUnReadNums];
    Block_Exec_Main_Async_Safe(^{
       [[UIApplication sharedApplication] setApplicationIconBadgeNumber:nums];
    });
}

- (void)endCall {
    if (self.uuid) {
//        CXEndCallAction *endAction = [[CXEndCallAction alloc] initWithCallUUID:self.uuid];
//        CXTransaction *trans = [[CXTransaction alloc] initWithAction:endAction];
//        CXCallController* callVC = [[CXCallController alloc] initWithQueue:dispatch_get_main_queue()];
//        [callVC requestTransaction:trans completion:^(NSError * _Nullable error) {
//            if (error) {
//                [self.provider reportCallWithUUID:self.uuid endedAtDate:nil reason:CXCallEndedReasonUnanswered];
//            }
////            if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
////                [self applicationDidEnterBackground:[UIApplication sharedApplication]];
////            }
//            self.uuid = nil;
//        }];
    }
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
//        [self applicationDidEnterBackground:[UIApplication sharedApplication]];
    }
}

#pragma mark - Init
//-(CXProvider *)provider API_AVAILABLE(ios(10.0)){
//    if (!_provider) {
//        _provider = [[CXProvider alloc] initWithConfiguration:self.providerConfig];
//        [_provider setDelegate:self queue:nil];
//    }
//    return _provider;
//}
//
//-(CXCallController *)cxCallController API_AVAILABLE(ios(10.0)){
//    if (!_cxCallController) {
//        _cxCallController = [[CXCallController alloc] initWithQueue:dispatch_get_main_queue()];
//    }
//    return _cxCallController;
//}
//
//- (CXProviderConfiguration *)providerConfig{
//    static CXProviderConfiguration* configInternal = nil;
//
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//
//        configInternal = [[CXProviderConfiguration alloc] initWithLocalizedName:@"9聊"];
//        configInternal.supportsVideo = NO;
//        configInternal.maximumCallsPerCallGroup = 1;
//        configInternal.maximumCallGroups = 1;
//        configInternal.supportedHandleTypes = [NSSet setWithObject:@(CXHandleTypePhoneNumber)];
//        UIImage* iconMaskImage = [UIImage imageNamed:@"avatar_default"];
//        configInternal.iconTemplateImageData = UIImagePNGRepresentation(iconMaskImage);
//        configInternal.ringtoneSound = @"MuteChecker.caf";
//    });
//
//    return configInternal;
//}

@end
