//
//  CommonDefine.h
//  MoPromo_Develop
//
//  Created by yang.xiangbao on 15/5/21.
//  Copyright (c) 2015年 MoPromo. All rights reserved.
//

#ifndef MoPromo_Develop_CommonDefine_h
#define MoPromo_Develop_CommonDefine_h
#import "FBKVOController.h"
#import "NSObject+Additions.h"

typedef void(^FinishedBlock)(id data);

#define EdgesForExtendedLayoutNone() do {\
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {\
                self.edgesForExtendedLayout = UIRectEdgeNone;\
                self.automaticallyAdjustsScrollViewInsets = NO;\
            }\
        } while (0)

typedef void (^rac_cleanupBlock_t1)();

static inline void rac_executeCleanupBlock1 (__strong rac_cleanupBlock_t1 *block) {
    (*block)();
}

#define StrF(...)   [NSString stringWithFormat:__VA_ARGS__]

//数字转字符串
#define toStr(num)  [@(num) description]

//#define Block_Exec(block, ...) if (block) { block(__VA_ARGS__); };

#define Block_Exec(block, ...) do {\
                if (block) { block(__VA_ARGS__); } \
            } while (0)

#define Array_Add(array, obj) do {\
if (obj) {\
    [array addObject:obj];\
} else {\
    MLog(@"addObject for nil");\
}\
} while (0);\


/*! 主线程同步队列 */
#define Block_Exec_Main_Sync_Safe(block)          \
    if ([NSThread isMainThread]) {                  \
        block();                                        \
    } else {                                        \
        dispatch_sync(dispatch_get_main_queue(), block);\
}

/*! 主线程异步队列 */
#define Block_Exec_Main_Async_Safe(block)        \
    if ([NSThread isMainThread]) {                 \
        block();                                       \
    } else {                                       \
        dispatch_async(dispatch_get_main_queue(), block);\
}

#define metamacro_concat_(A, B) A ## B

#define metamacro_concat(A, B) \
metamacro_concat_(A, B)

#if DEBUG
#define rac_keywordify autoreleasepool {}
#else
#define rac_keywordify try {} @catch (...) {}
#endif

//#define Padding 10
#define sizef40in(size40) floorf(size40*([[UIScreen mainScreen] applicationFrame].size.width/320))
#define sizef55in(size55) floorf(size55*([[UIScreen mainScreen] applicationFrame].size.width/414))

#define onExit \
rac_keywordify \
__strong rac_cleanupBlock_t1 metamacro_concat(rac_exitBlock_, __LINE__) __attribute__((cleanup(rac_executeCleanupBlock1), unused)) = ^


typedef enum : NSUInteger {
    Help_MLVBLiveRoom,
    Help_录屏直播,
    Help_超级播放器,
    Help_视频录制,
    Help_特效编辑,
    Help_视频拼接,
    Help_图片转场,
    Help_视频上传,
    Help_双人音视频,
    Help_多人音视频,
    Help_rtmp推流,
    Help_直播播放器,
    Help_点播播放器,
    Help_webrtc,
    Help_TRTC,
} HelpTitle;

#define  HelpBtnUI(NAME) \
UIButton *helpbtn = [UIButton buttonWithType:UIButtonTypeCustom]; \
helpbtn.tag = Help_##NAME; \
[helpbtn setFrame:CGRectMake(0, 0, 60, 25)]; \
[helpbtn setBackgroundImage:[UIImage imageNamed:@"help_small"] forState:UIControlStateNormal]; \
[helpbtn addTarget:[[UIApplication sharedApplication] delegate] action:@selector(clickHelp:) forControlEvents:UIControlEventTouchUpInside]; \
[helpbtn sizeToFit]; \
UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:helpbtn]; \
self.navigationItem.rightBarButtonItems = @[rightItem];

#define HelpBtnConfig(helpbtn, x) \
helpbtn.tag = Help_##x; \
[helpbtn addTarget:[[UIApplication sharedApplication] delegate] action:@selector(clickHelp:) forControlEvents:UIControlEventTouchUpInside];

///指定图片尺寸
#define AssignSize(url,w,h) [NSString stringWithFormat:@"%@?imageView2/1/w/%d/h/%d",url,((int)(w*[[UIScreen mainScreen] scale])),((int)(h*[[UIScreen mainScreen] scale]))]

#endif


