//
//  SPBadgeButton.m
//  BOB
//
//  Created by colin on 2020/11/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SPBadgeButton.h"

@implementation SPBadgeButton

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    [self addSubview:self.badge];
    [self.badge mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.badge.size);
        make.right.mas_equalTo(self.mas_right);
        make.top.mas_equalTo(self.mas_top);
    }];
    self.badge.badgeText = @"";
}

- (AxcAE_TabBarBadge *)badge {
    if (!_badge) {
        _badge = [AxcAE_TabBarBadge new];
        _badge.badgeText = @"0";
    }
    return _badge;
}

@end
