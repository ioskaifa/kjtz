//
//  SPBadgeButton.h
//  BOB
//
//  Created by colin on 2020/11/13.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "SPButton.h"
#import "AxcAE_TabBarBadge.h"

NS_ASSUME_NONNULL_BEGIN

@interface SPBadgeButton : SPButton

@property (nonatomic, strong) AxcAE_TabBarBadge *badge;

@end

NS_ASSUME_NONNULL_END
