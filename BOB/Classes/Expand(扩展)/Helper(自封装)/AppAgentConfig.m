//
//  AppAgentConfig.m
//  MoPal_Developer
//
//  Created by fly on 15/11/30.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "AppAgentConfig.h"
#import "GVUserDefaults+Properties.h"
#import "MXRemotePush.h"
#import "MXMapConfig.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "WXApi.h"
#import "PolymerPayManager.h"

#ifdef OpenDebugVC
#import "Xtrace.h"
#import "AppDelegate.h"
#endif

@implementation AppAgentConfig

+ (void)configApplicationForCapacity{
    // 初始化 定位
//    [[MXLocationManager shareManager] startUpdatingLocation:nil];
    // 注册推送通知
//    [[MXRemotePush sharedInstance] registerRemoteNotification];
    //语音权限
    //[[MXDeviceManager sharedInstance] audioSessionAmbient];
    
//    [NBSAppAgent startWithAppID:@"789d419bdea7461b99df48d8eb1fabec"];
    [WXApi registerApp:kWechatAppid universalLink:kWechatUniversalLink];
}

+ (void)configApplicationForThirdLibrary{
    
#ifdef OpenDebugVC
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"serverType"]) {
        [GVUserDefaults standardUserDefaults].serverType = MXMoXianServerDomainTest;
    }

    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"debugLocation"]) {
        [GVUserDefaults standardUserDefaults].debugLocation = DebugLocationCurrent;
    }

#else
//    [GVUserDefaults standardUserDefaults].serverType = MXMoXianServerDomainOnline;
#endif
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"systemStatusIsValid"]) {
        [GVUserDefaults standardUserDefaults].systemStatusIsValid = YES;
    }

    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
}

#pragma mark - 类方法调用追踪打印
#ifdef OpenDebugVC
+ (void)mx_xtrace {
#ifdef DEBUG
    [Xtrace describeValues:NO];
    [Xtrace showCaller:YES];
    [Xtrace showActual:YES];
    [Xtrace showArguments:YES];
    [Xtrace showReturns:NO];
    [AppDelegate xtrace];
    [LoginVC xtrace];
    [LoginManager xtrace];
#endif
}
#endif

+ (void)configApplicationForDefaultSetting{   
    // add by yang.xiangbao 2015/10/8
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName : [UIColor blackColor],
                                                           NSFontAttributeName : [UIFont font19]
                                                           }];
    [GVUserDefaults standardUserDefaults].lastCheckAppVersion = 0;
}

/**
 *  检测APP更新了版本后，是否需要修改本地的一些配置或者缓存
 *
 *  @return Yes表示已做更新 NO表示还
 */
+ (BOOL)checkApplicationNeedModifyConfig{
    NSString* oldVersion = [GVUserDefaults standardUserDefaults].lastAppVersion;
    if ( oldVersion && [oldVersion compare:APPSHORTVERSION] != NSOrderedAscending) {
        return NO;
    }
    return YES;
}

@end
