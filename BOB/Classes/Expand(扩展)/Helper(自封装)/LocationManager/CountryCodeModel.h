//
//  CountryCodeModel.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/2/7.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryCodeModel : NSObject<NSCoding>
//手机国家码
@property (nonatomic, copy  ) NSString  *code;
//1 热门 默认0
@property (nonatomic, copy  ) NSString  *isHotStr;
@property (nonatomic, assign) NSInteger isHot;
@property (nonatomic, copy  ) NSString  *cs_name;
@property (nonatomic, copy  ) NSString  *en_name;
//城市码 简写
@property (nonatomic, copy  ) NSString  *countryCode;
// 城市拼音
@property (nonatomic, copy  ) NSString  *pinYin;

@property (nonatomic, assign) NSInteger currentId;
@property (nonatomic, assign) NSInteger pid;
@property (nonatomic, assign) NSInteger level;//1 国 2省（州） 3 市
@property (nonatomic, assign) NSInteger hasNext;
@property (nonatomic, assign) NSInteger bussniss;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) NSInteger coutryId;
//城市简写
//@property (nonatomic, copy) NSString* countryCodeShortName;

- (void)dictionaryToModel:(NSDictionary *)dic ;
@end
