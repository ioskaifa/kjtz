//
//  SearchAutoPlacesModel.m
//  MapDemo
//
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "SearchAutoPlacesModel.h"
#import "CommonDefine.h"

@implementation SearchAutoPlacesModel


/**
 * 将字典数据转换成实体对象
 */
- (void)dictionaryToModel:(NSDictionary *)dic  {
    
    if ([dic isKindOfClass:[NSDictionary class]]) {
        @onExit {
            [self checkPropertyAndSetDefaultValue];
        };
        self.addressDesc=dic[@"description"];
        self.reference=dic[@"reference"];
        
        NSArray *tempArray=dic[@"terms"];
        
        if ([tempArray isKindOfClass:[NSArray class]]) {
            if (tempArray.count > 0) {
                
                NSDictionary *tempDic=tempArray[0];
                self.name=tempDic[@"value"];
                
            }
            
        }
    }
}

@end
