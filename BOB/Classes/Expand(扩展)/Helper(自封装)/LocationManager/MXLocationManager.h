//
//  MXLocationManager.h
//  MoPal_Developer
//
//  使用高德定位,其内容已做了坐标修正
//  Created by aken on 15/11/9.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
   #import <CoreLocation/CoreLocation.h>
extern NSString * _Nullable const AuthorizationStatus_toString[];

typedef NS_ENUM(NSUInteger, LocationManagerStatus) {
    LocationManagerStatusStop       = 0,
    LocationManagerStatusReady      = 1,
    LocationManagerStatusUpdating   = 2,
};

@class MXAddressModel;

// 定位回调函数
typedef void(^MXAmapLocationCallback)(CLLocation * _Nullable location,MXAddressModel * _Nullable model,NSError * _Nullable error);

@interface MXLocationManager : NSObject<NSCoding>

// 定位相关信息
@property (nonatomic, assign) CLLocationCoordinate2D location; // 经纬度
@property (nonatomic, copy) NSString * _Nullable currentAddress;          // 当前地址
@property (nonatomic, copy) NSString * _Nullable province;                // 省/直辖市
@property (nonatomic, copy) NSString * _Nullable currentCity;             // 当前城市
@property (nonatomic, copy) NSString * _Nullable district;                // 区

@property (nonatomic, copy) NSString * _Nullable ISOcountryCode;          // 区域编码
@property (nonatomic, copy) NSString * _Nullable country;                 // 区域编码
/// 数据库国家编号
@property (nonatomic, assign) NSInteger countryId;
// 定位结果保留 
@property (nonatomic) BOOL locateResult;
// 回调变量
@property (nonatomic,strong) MXAmapLocationCallback _Nullable callback;


+ (MXLocationManager*_Nullable)shareManager;

// 启动定位，马上进入定位
- (void)startUpdatingLocation;
- (void)startUpdatingLocation:(MXAmapLocationCallback _Nullable )completion;

- (NSString*_Nullable)distanceFromLatitude:(CLLocationDegrees)latitude
                        longitude:(CLLocationDegrees)longitude;

// 停止定位
- (void)stopLocation;

//定时开启定位，程序在几种模式下的定位,程序进入前台 程序的几个首页切换时候
//这种方式的定位 会延时几秒进行
- (void)startScheduleUpdateLocation;

- (void)storeLocationCache:(MXAddressModel*_Nullable)cache;

/**
 *  当前城市码，手动选择的城市优先
 *
 *  @return 城市Id
 */
- (nonnull NSString *)currentCityCode;

/**
 *  当前国家码，手动选择的国家优先
 */
- (nonnull NSString *)currentCountryCode;

@end
