//
//  MXFixCllocation2DHelper.m
//  Moxian
//
//  Created by litiankun on 14-1-22.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import "MXFixCllocation2DHelper.h"
#import <AMapLocationKit/AMapLocationKit.h>

#define pi 3.14159265358979324 // pi
#define ee 0.00669342162296594323
#define a 6378245.0


//矩形
@interface Rectangle : NSObject

@property(assign, nonatomic) double west;
@property(assign, nonatomic) double north;
@property(assign, nonatomic) double east;
@property(assign, nonatomic) double south;

@end

@implementation Rectangle

+ (Rectangle *)rectangleWithLa1:(double)latitude1 lo1:(double)longitude1 la2:(double)latitude2 lo2:(double)longitude2 {
    return [[Rectangle alloc]
            initWithLa1:latitude1 lo1:longitude1 la2:latitude2 lo2:longitude2];
}

- (instancetype)initWithLa1:(double)latitude1 lo1:(double)longitude1 la2:(double)latitude2 lo2:(double)longitude2 {
    
    self = [super init];
    if (self) {
        self.west = MIN(longitude1, longitude2);
        self.north = MAX(latitude1, latitude2);
        self.east = MAX(longitude1, longitude2);
        self.south = MIN(latitude1, latitude2);
    }
    return self;
}

@end

@interface MXFixCllocation2DHelper()

@property(nonatomic, strong) NSArray *regionRectangle;
@property(nonatomic, strong) NSArray *excludeRectangle;

@end

@implementation MXFixCllocation2DHelper

static id _shared;
+ (instancetype)shared {
    static dispatch_once_t _onceToken;
    dispatch_once(&_onceToken, ^{
        _shared = [[self alloc] init];
    });
    return _shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        //范围矩形列表
        self.regionRectangle = [[NSArray alloc] initWithObjects:
                                [Rectangle rectangleWithLa1:49.220400 lo1:079.446200 la2:42.889900 lo2:096.330000],
                                [Rectangle rectangleWithLa1:54.141500 lo1:109.687200 la2:39.374200 lo2:135.000200],
                                [Rectangle rectangleWithLa1:42.889900 lo1:073.124600 la2:29.529700 lo2:124.143255],
                                [Rectangle rectangleWithLa1:29.529700 lo1:082.968400 la2:26.718600 lo2:097.035200],
                                [Rectangle rectangleWithLa1:29.529700 lo1:097.025300 la2:20.414096 lo2:124.367395],
                                [Rectangle rectangleWithLa1:20.414096 lo1:107.975793 la2:17.871542 lo2:111.744104],
                                nil];
        
        //范围内排除的矩形列表
        self.excludeRectangle = [[NSArray alloc] initWithObjects:
                                 [Rectangle rectangleWithLa1:25.398623 lo1:119.921265 la2:21.785006 lo2:122.497559],
                                 [Rectangle rectangleWithLa1:22.284000 lo1:101.865200 la2:20.098800 lo2:106.665000],
                                 [Rectangle rectangleWithLa1:21.542200 lo1:106.452500 la2:20.487800 lo2:108.051000],
                                 [Rectangle rectangleWithLa1:55.817500 lo1:109.032300 la2:50.325700 lo2:119.127000],
                                 [Rectangle rectangleWithLa1:55.817500 lo1:127.456800 la2:49.557400 lo2:137.022700],
                                 [Rectangle rectangleWithLa1:44.892200 lo1:131.266200 la2:42.569200 lo2:137.022700],
                                 nil];
    }
    return self;
}
//判断location是否在中国
- (BOOL)isInsideChina:(CLLocationCoordinate2D)location {
    
    for (int i = 0; i < self.regionRectangle.count; i++) {
        
        if([self inRectangle:self.regionRectangle[i] location:location]) {
            
            for (int j = 0; j < self.excludeRectangle.count; j++) {
                
                if ([self inRectangle:self.excludeRectangle[j] location:location]) {
                    return false;
                }
            }
            return true;
        }
    }
    return false;
}

//点是否在矩形内
- (BOOL)inRectangle:(Rectangle *)rect location:(CLLocationCoordinate2D)location {
    
    return rect.west <= location.longitude && rect.east >= location.longitude && rect.north >= location.latitude && rect.south <= location.latitude;
}

+ (CLLocationCoordinate2D)transformFromWGSToGCJ:(CLLocationCoordinate2D)wgsLoc {
    
    CLLocationCoordinate2D adjustLoc;
    
    if(![[self shared] isInsideChina:wgsLoc]){
        
        adjustLoc = wgsLoc;
        
    }else{
        double adjustLat = [self transformLatWithX:wgsLoc.longitude - 105.0 withY:wgsLoc.latitude - 35.0];
        double adjustLon = [self transformLonWithX:wgsLoc.longitude - 105.0 withY:wgsLoc.latitude - 35.0];
        double radLat = wgsLoc.latitude / 180.0 * pi;
        double magic = sin(radLat);
        magic = 1 - ee * magic * magic;
        double sqrtMagic = sqrt(magic);
        adjustLat = (adjustLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
        adjustLon = (adjustLon * 180.0) / (a / sqrtMagic * cos(radLat) * pi);
        adjustLoc.latitude = wgsLoc.latitude + adjustLat;
        adjustLoc.longitude = wgsLoc.longitude + adjustLon;
    }
    return adjustLoc;
}

+ (BOOL)isLocationChina {
    return YES;
    //返回是否在大陆或以外地区，返回YES为大陆地区，NO为非大陆。
    BOOL flag= AMapLocationDataAvailableForCoordinate([MXLocationManager shareManager].location);
//    return ![[self shared] isInsideChina:[MXLocationManager shareManager].location];
    
    return flag;
}

+ (BOOL)isLocationChinaWithLocation:(CLLocationCoordinate2D)coordinate {
    return  YES;
    //返回是否在大陆或以外地区，返回YES为大陆地区，NO为非大陆。
    BOOL flag= AMapLocationDataAvailableForCoordinate(coordinate);
    
    return flag;
}

+ (double)transformLatWithX:(double)x withY:(double)y {
    
    double lat = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * sqrt(fabs(x));
    lat += (20.0 * sin(6.0 * x * pi) + 20.0 *sin(2.0 * x * pi)) * 2.0 / 3.0;
    lat += (20.0 * sin(y * pi) + 40.0 * sin(y / 3.0 * pi)) * 2.0 / 3.0;
    lat += (160.0 * sin(y / 12.0 * pi) + 3320 * sin(y * pi / 30.0)) * 2.0 / 3.0;
    return lat;
}

+ (double)transformLonWithX:(double)x withY:(double)y {
    
    double lon = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * sqrt(fabs(x));
    lon += (20.0 * sin(6.0 * x * pi) + 20.0 * sin(2.0 * x * pi)) * 2.0 / 3.0;
    lon += (20.0 * sin(x * pi) + 40.0 * sin(x / 3.0 * pi)) * 2.0 / 3.0;
    lon += (150.0 * sin(x / 12.0 * pi) + 300.0 * sin(x / 30.0 * pi)) * 2.0 / 3.0;
    return lon;
}

@end
