//
//  MXAddressManager.h
//  MoPal_Developer
//
//  地理位置管理类:地址反编译、POI搜索(高德、Google)
//  Created by aken on 15/3/11.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MXMapConfig.h"

@class MXAddressModel;

// 地理位置反编译回调
typedef void (^GetAddressModelCallBack)(MXAddressModel *model);

// POI搜索回调
typedef void (^SearchPOICallBack)(NSMutableArray *array);

@interface MXAddressManager : NSObject


@property (nonatomic,assign) MXMapViewType mapViewType;

// 当前经纬度
@property (nonatomic) CLLocationCoordinate2D coordinate2D;

+ (MXAddressManager*)sharedInstance;
/**
 * 清除地址查询缓存: 此函数在内存报警时调用
 */
+ (void)clearCache ;

/**
 *  开始搜索
 */
- (void)startSearch;
- (void)clearSearch;


/**
 * 转化物理地址 : 此函数使用苹果自带的API实现反地理编码
 * 如果是在中国,则此经纬度为百度坐标系的标准,在实现中会先将百度坐标转换成GPS坐标再进行反地理编码;
 * 如果不是在中国,则此经纬度为标准GPS坐标系,实现中直接反地理编码
 */
+(void)parseAddressByLng:(double)lng withLat:(double)lat completion:(GetAddressModelCallBack)completion;

/**
 *  根据经纬度反编译地理位置消息
 *
 *  @param coor       经纬度
 *  @param completion 回调
 */
- (void)reverseGeocodingWithCoordinate:(CLLocationCoordinate2D)coor completion:(GetAddressModelCallBack)completion;

/**
 *  根据经纬度进行POI搜索
 *
 *  @param location 经纬度
 *  @param radius   地理范围
 *  @param completion 回调函数
 */
- (void)searchPOIWithCoordinate:(CLLocationCoordinate2D)location andRadius:(NSInteger)radius completion:(SearchPOICallBack)completion;

/**
 *  根据关键字进行POI搜索
 *
 *  @param keyword    关键字
 *  @param adcode     城市名
 *  @param completion 回调函数
 */
- (void)searchPOIWithKey:(NSString *)keyword adcode:(NSString *)adcode completion:(SearchPOICallBack)completion;

/**
 *  根据关键字进行自动提示搜索.
 *
 *  @param keyword    关键字
 *  @param cityname   城市
 *  @param completion 回调函数
 */
- (void)searchTipsWithKeyword:(NSString *)keyword cityname:(NSString*)cityname completion:(SearchPOICallBack)completion;


/**
 *  根据关键字进行POI搜索
 *
 *  @param keyword    关键字
 *  @param completion 回调函数
 */
- (void)searchPOIWithKeywork:(NSString *)keyword completion:(SearchPOICallBack)completion;

@end
