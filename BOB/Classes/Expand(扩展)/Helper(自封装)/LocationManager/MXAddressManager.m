//
//  MXAddressManager.m
//  MoPal_Developer
//
//  Created by aken on 15/3/11.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "MXAddressManager.h"
//#import "StringUtil.h"
#import "MXAddressModel.h"
#import "MXFixCllocation2DHelper.h"

// 高德
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
//#import <AMapSearchKit/AMapSearchServices.h>

#import "RegionAnnotation.h"
#import "SearchAutoPlacesModel.h"
//#import "MXGooglePlaceManager.h"

//#define GaodeKey @"1b4aa259adc650365039012eb093e10d"

@interface MXAddressManager()<AMapSearchDelegate>
{
    
    GetAddressModelCallBack _callBack;
    
    SearchPOICallBack  _searchCallBack;
}

@property (nonatomic, strong) AMapSearchAPI *search;;

@end


@implementation MXAddressManager

+ (MXAddressManager*)sharedInstance {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (instancetype)init {
    self=[super init];
    if (self) {
        
        NSString *key=nil;
#if DEBUG
    key = GaodeKey;
#else
    key = GaodeKey;
#endif
        
        [AMapServices sharedServices].apiKey = key;
    }
    
    return self;
}

- (void)startSearch {
    if (self.search.delegate==nil) {
       self.search.delegate=self;
    }
}

- (void)clearSearch
{
    self.search.delegate = nil;
}

static NSMutableDictionary *addressCacheDic;
+ (void)clearCache {
    @synchronized(addressCacheDic){
        if (addressCacheDic) {
            [addressCacheDic removeAllObjects];
            addressCacheDic = nil;
        }
    }
}

- (AMapSearchAPI*)search {
    if (_search==nil) {
        _search = [[AMapSearchAPI alloc] init];
        _search.delegate = self;
    }
    return _search;
}


#pragma mark - 解析详细地址
+(void)parseAddressByLng:(double)lng withLat:(double)lat completion:(GetAddressModelCallBack)completion {

    CLLocation* location = [[CLLocation alloc]initWithLatitude:lat longitude:lng];
    CLGeocoder*  _geocoder=[[CLGeocoder alloc] init] ;
    
    [_geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
        NSString* currentAddress = @"";
        MXAddressModel *model = [[MXAddressModel alloc] init];
        @autoreleasepool {
            for (CLPlacemark *placeMark in placemarks) {
                currentAddress=[[NSString alloc] initWithFormat:@"%@%@%@%@",placeMark.locality == nil ? @"":placeMark.locality,
                                placeMark.subLocality == nil ? @"":placeMark.subLocality,
                                placeMark.thoroughfare == nil ? @"":placeMark.thoroughfare,
                                placeMark.subThoroughfare == nil ? @"":placeMark.subThoroughfare];
                model.address = currentAddress;
                NSString* city = [[NSString alloc] initWithFormat:@"%@",placeMark.locality == nil ? @"":placeMark.locality];
                model.cityName = city;
            }
        }
        
        completion(model);
    }];
}

#pragma mark - public

// 根据经纬度反编译地理位置消息
- (void)reverseGeocodingWithCoordinate:(CLLocationCoordinate2D)coordinate completion:(GetAddressModelCallBack)completion
{
    [self startSearch];
    //    _completeLocate = YES;
    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];

    regeo.location = [AMapGeoPoint locationWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    regeo.requireExtension = YES;

    [self.search AMapReGoecodeSearch:regeo];
    _callBack=completion;
    
   
}


// 根据经纬度进行附近POI搜索
- (void)searchPOIWithCoordinate:(CLLocationCoordinate2D)location andRadius:(NSInteger)radius completion:(SearchPOICallBack)completion {
    

    _searchCallBack=completion;
    
//    if ([MXFixCllocation2DHelper isLocationChina]) {
        [self startSearch];
          AMapPOIAroundSearchRequest*request = [[AMapPOIAroundSearchRequest alloc] init];
          
          request.location = [AMapGeoPoint locationWithLatitude:location.latitude  longitude:location.longitude];

          request.radius   = 1000;
          request.types = @"住宅";
          request.sortrule = 0;
          request.page     = 1;
          
          [self.search AMapPOIAroundSearch:request];

//        AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
//
//        request.location            = [AMapGeoPoint locationWithLatitude:location.latitude longitude:location.longitude];
//        /* 按照距离排序. */
//        request.sortrule            = 1;
//        request.requireExtension    = YES;
//        request.keywords    = @"";
//        request.radius=radius;
//        [self.search AMapPOIAroundSearch:request];
        
//    }else {
//        MLog(@"Google POI搜索");
//        // Google搜索
////        [MXGooglePlaceManager searchNearbyWithKeyWord:@"" location:location completion:^(NSArray *array, NSString *error) {
////
////            _searchCallBack([array mutableCopy]);
////        }];
//    }
}

// 根据关键字进行POI搜索
- (void)searchPOIWithKey:(NSString *)keyword adcode:(NSString *)adcode completion:(SearchPOICallBack)completion
{
    if (keyword.length == 0) {
        return;
    }
    
    _searchCallBack=completion;
    
    if ([MXFixCllocation2DHelper isLocationChina]) {
        
        [self startSearch];
        MLog(@"高德POI搜索");
        
        AMapPOIKeywordsSearchRequest *request = [[AMapPOIKeywordsSearchRequest alloc] init];
        
        request.keywords            = keyword;
        request.city                = adcode;
        request.requireExtension    = YES;
        [self.search AMapPOIKeywordsSearch:request];
        
        
    }else {
        
        MLog(@"Google POI搜索");
        
//        [MXGooglePlaceManager googlePlaceDetails:keyword completion:^(NSArray *array, NSString *error) {
//            if (_searchCallBack) {
//                _searchCallBack([array mutableCopy]);
//            }
//        }];
    }
}

// 根据关键字进行POI搜索
- (void)searchPOIWithKeywork:(NSString *)keyword completion:(SearchPOICallBack)completion {
    
    if (keyword.length == 0) {
        return;
    }
    
    _searchCallBack=completion;
    
    
    CLGeocoder*  geocoder=[[CLGeocoder alloc] init] ;
    
    [geocoder geocodeAddressString:keyword completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
        NSString* currentAddress = @"";
        MXAddressModel *model = [[MXAddressModel alloc] init];
        NSMutableArray *array = [NSMutableArray array];
        @autoreleasepool {
            
            if (placemarks && placemarks.count > 0) {
                
                CLPlacemark *placeMark = placemarks.firstObject;
                currentAddress=[[NSString alloc] initWithFormat:@"%@%@%@%@",placeMark.locality == nil ? @"":placeMark.locality,
                                placeMark.subLocality == nil ? @"":placeMark.subLocality,
                                placeMark.thoroughfare == nil ? @"":placeMark.thoroughfare,
                                placeMark.subThoroughfare == nil ? @"":placeMark.subThoroughfare];
                model.address = currentAddress;
                NSString* city = [[NSString alloc] initWithFormat:@"%@",placeMark.locality == nil ? @"":placeMark.locality];
                model.cityName = city;
                model.location = placeMark.location;
                model.ISOcountryCode = placeMark.ISOcountryCode;
                model.country = placeMark.country;
//                model.countryId = [[CountryHelper sharedDataBase] readCountryIdWithCountryName:model.country];
                
                [array addObject:model];
                
           }

        }
        
        _searchCallBack(array);
    } ];
    
}

// 自动提示搜索.
- (void)searchTipsWithKeyword:(NSString *)keyword cityname:(NSString*)cityname completion:(SearchPOICallBack)completion {
    
    if (keyword.length == 0)
    {
        return;
    }
    
    _searchCallBack=completion;
    
    if ([MXFixCllocation2DHelper isLocationChina]) {
        [self startSearch];
        AMapInputTipsSearchRequest *tips = [[AMapInputTipsSearchRequest alloc] init];
        tips.keywords = keyword;
        tips.city=cityname;
        [_search AMapInputTipsSearch:tips];
    }else {
        
        // google auto place搜索
//        [MXGooglePlaceManager searchAutocompletePlaceLists:keyword location:self.coordinate2D completion:^(NSMutableArray *array, NSString *error) {
//            
//            _searchCallBack(array);
//        }];
    }
    
}

/******************************************************delegate**********************************************************/
#pragma mark - AMapReGeocodeSearchRequest delegate
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    MLog(@"AMapReGeocodeSearchRequest_response :%@", response);

    MXAddressModel *model = [[MXAddressModel alloc] init];
    model.address=response.regeocode.formattedAddress;
    model.cityName=response.regeocode.addressComponent.city;
    if (_callBack) {
        _callBack(model);
    }
    
}
- (void)AMapSearchRequest:(id)request didFailWithError:(NSError *)error
{
    
}
/* 输入提示回调. */
- (void)onInputTipsSearchDone:(AMapInputTipsSearchRequest *)request response:(AMapInputTipsSearchResponse *)response
{
    
    if (_searchCallBack) {
        
        NSMutableArray *poiAnnotations = [NSMutableArray arrayWithCapacity:response.tips.count];
        
        [response.tips enumerateObjectsUsingBlock:^(AMapTip *obj, NSUInteger idx, BOOL *stop) {
            
            SearchAutoPlacesModel *region=[[SearchAutoPlacesModel alloc] init];
            region.name=obj.name;
            region.addressDesc=obj.district;
            region.adcode=obj.adcode;
            region.reference=obj.name;
            [poiAnnotations addObject:region];
        }];
        
        _searchCallBack(poiAnnotations);
    }
}


/* POI 搜索回调. */
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    if (response.pois.count == 0) {
        return;
    }
    
    NSMutableArray *poiAnnotations = [NSMutableArray arrayWithCapacity:response.pois.count];
    
    [response.pois enumerateObjectsUsingBlock:^(AMapPOI *obj, NSUInteger idx, BOOL *stop) {
        RegionAnnotation *region = [[RegionAnnotation alloc] init];
        region.title = obj.name;
        region.subtitle = [NSString stringWithFormat:@"%@%@%@", obj.city?:@"", obj.district?:@"", obj.address?:@""];
        region.coordinate = CLLocationCoordinate2DMake(obj.location.latitude, obj.location.longitude);
        region.city = obj.city;
        [poiAnnotations addObject:region];
    }];
    
    if (_searchCallBack) {
        _searchCallBack(poiAnnotations);
    }
}

@end
