//
//  CountryCodeModel.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/2/7.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "CountryCodeModel.h"
#import "CommonDefine.h"

@implementation CountryCodeModel
@synthesize code,isHot,cs_name,en_name,currentId,pid,hasNext,level,countryCode;
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.code forKey:@"code"];
    [aCoder encodeObject:self.cs_name forKey:@"cs_name"];
    [aCoder encodeObject:self.en_name forKey:@"en_name"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.currentId] forKey:@"currentId"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.isHot] forKey:@"isHot"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.pid] forKey:@"pid"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.hasNext] forKey:@"hasNext"];
    [aCoder encodeObject:self.countryCode forKey:@"countryCode"];
    [aCoder encodeObject:self.isHotStr forKey:@"isHotStr"];
    [aCoder encodeObject:self.pinYin forKey:@"pinYin"];
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.code = [aDecoder decodeObjectForKey:@"code"];
        self.cs_name = [aDecoder decodeObjectForKey:@"cs_name"];
        self.en_name = [aDecoder decodeObjectForKey:@"en_name"];
        self.currentId = [[aDecoder decodeObjectForKey:@"currentId"] intValue];
        self.isHot = [[aDecoder decodeObjectForKey:@"isHot"] integerValue];
        self.pid = [[aDecoder decodeObjectForKey:@"pid"] intValue];
        self.hasNext = [[aDecoder decodeObjectForKey:@"hasNext"] intValue];
        self.countryCode = [aDecoder decodeObjectForKey:@"countryCode"];
        self.isHotStr = [aDecoder decodeObjectForKey:@"isHotStr"];
        _pinYin = [[aDecoder decodeObjectForKey:@"pinYin"] copy];
    }
    return self;
}
- (void)dictionaryToModel:(NSDictionary *)dic {
    if (![dic isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    @onExit {
        [self checkPropertyAndSetDefaultValue];
    };
    
    NSString* codeString = [NSString stringWithFormat:@"%@",dic[@"cityCode"]];
    NSString* cs_nameString = [NSString stringWithFormat:@"%@",dic[@"csName"]];
    NSString* en_nameString = [NSString stringWithFormat:@"%@",dic[@"enName"]];
    
    NSString *hotStr=[NSString stringWithFormat:@"%@",dic[@"hot"]];
    self.isHotStr=hotStr;
    self.isHot =[StringUtil isEmpty:hotStr]?0:[hotStr integerValue];
    self.code = codeString;
    self.cs_name = cs_nameString;
    self.en_name = [StringUtil isEmpty:en_nameString]?cs_nameString:en_nameString;
}

@end
