//
//  MXLocationManager.m
//  MoPal_Developer
//
//  Created by aken on 15/11/9.
//  Copyright © 2015年 MoXian. All rights reserved.
//

#import "MXLocationManager.h"
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import "NSObject+CapacityAuthorize.h"
#import "GVUserDefaults+Properties.h"
#import "NSString+HandleCityName.h"
#import "MXLocationManager+Util.h"
#import "NSString+Distance.h"
#import "MXAddressModel.h"
#import "CountryHelper.h"
#import "MXCache.h"
#import "MXFixCllocation2DHelper.h"
//#import "MXCityChangeHelper.h"

static NSString * const ManagerCacheKey = @"MXLocationManagerCache";

static NSUInteger const minUpdateTimeInterval = (30 * 60);


NSString * const AuthorizationStatus_toString[] = {
    [kCLAuthorizationStatusNotDetermined] = @"kCLAuthorizationStatusNotDetermined",
    [kCLAuthorizationStatusRestricted]     = @"kCLAuthorizationStatusRestricted",
    [kCLAuthorizationStatusDenied]    = @"kCLAuthorizationStatusDenied",
    [kCLAuthorizationStatusAuthorizedAlways]   = @"kCLAuthorizationStatusAuthorizedAlways",
    [kCLAuthorizationStatusAuthorizedWhenInUse]   = @"kCLAuthorizationStatusAuthorizedWhenInUse",
};

@interface MXLocationManager()<AMapLocationManagerDelegate>

@property (nonatomic, assign) LocationManagerStatus      loctionStatus;

@property (nonatomic, strong) AMapLocationManager         *locationManager;
@property (nonatomic, copy  ) AMapLocatingCompletionBlock completionBlock;

@end

@implementation MXLocationManager

+ (instancetype)shareManager {
    
    static id sharedInstanceNH;
    static dispatch_once_t onceNH;
    dispatch_once(&onceNH, ^{
        sharedInstanceNH = [[[self class] alloc] init];
    });
    return sharedInstanceNH;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSString *key = nil;
#if DEBUG
    key = GaodeKey;
#else
    key = GaodeKey;
#endif
        [self fillDataFromCache];

        self.loctionStatus = LocationManagerStatusStop;
        
        [AMapServices sharedServices].apiKey = key;
        
        self.locationManager = [[AMapLocationManager alloc] init];
        self.locationManager.delegate = self;
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
        [self.locationManager setPausesLocationUpdatesAutomatically:NO];
        
    }
    return self;
}

#pragma mark - Public

// 启动定位
- (void)startUpdatingLocation {
    
    
    [self startUpdatingLocation:nil];
}

- (void)startUpdatingLocation:(MXAmapLocationCallback)completion {
    if (![[UIApplication sharedApplication] isAuthorizedLocationStatus]) {
      
        [self setDefaultLocation];
        return;
    }
    self.loctionStatus = LocationManagerStatusUpdating;
    self.callback = completion;
    [self cteateCompleteBlock];
    [self.locationManager requestLocationWithReGeocode:YES completionBlock:self.completionBlock];
    [self.locationManager startUpdatingLocation];
}

//启动自动定位
- (void)startScheduleUpdateLocation{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status == kCLAuthorizationStatusDenied ) {
        [self setDefaultLocation];
        return;
    }
    if (self.loctionStatus != LocationManagerStatusStop) {
        return;
    }
    if ([self isShouldStartUpdateLocation]) {
        self.loctionStatus  = LocationManagerStatusReady;
        [self startUpdatingLocation:^(CLLocation *location, MXAddressModel *model, NSError *error) {
            [GVUserDefaults standardUserDefaults].lastUpdateLocation = [[NSDate date] timeIntervalSince1970];
//            [GetFriendsManager saveFansLocation:nil completion:nil];
        }];
    }
}

- (NSString*)distanceFromLatitude:(CLLocationDegrees)latitude
                        longitude:(CLLocationDegrees)longitude {
    if ((self.location.longitude == 0 && self.location.latitude == 0)||
        (latitude == 0 && longitude == 0)) {
        return @"";
    }
    CLLocation* myLocation = [[CLLocation alloc] initWithLatitude:self.location.latitude longitude:self.location.longitude];
    CLLocation *fromLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    CLLocationDistance distance = [myLocation distanceFromLocation:fromLocation];
    return [[NSString stringWithFormat:@"%f",distance] distance];
}

#pragma mark - Private

- (void)fillDataFromCache{
    NSData *data = [MXCache valueForKey:ManagerCacheKey];
    if (!data || ![data isKindOfClass:[NSData class]]) {
        return;
    }
    id obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (![obj isKindOfClass:[self class]]) {
        return;
    }
    MXLocationManager *cacheObj = (MXLocationManager*)obj;
    self.currentCity = cacheObj.currentCity;
    self.location = cacheObj.location;
    self.currentAddress = cacheObj.currentAddress;
    self.province = cacheObj.province;
    self.district = cacheObj.district;
    self.ISOcountryCode = cacheObj.ISOcountryCode;
    self.country = cacheObj.country;
    self.countryId = cacheObj.countryId;
}

// 停止定位
- (void)stopLocation {
    self.loctionStatus = LocationManagerStatusStop;
    [self.locationManager stopUpdatingLocation];
    
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    [self.locationManager setPausesLocationUpdatesAutomatically:YES];
    
    self.locationManager.delegate = nil;
    self.completionBlock = nil;
}

// 单次定位完成回调
- (void)cteateCompleteBlock
{

    @weakify(self);
    self.completionBlock = ^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        @strongify(self);

        
        [self stopLocation];
        
        
        BOOL isChinaLocation = [MXFixCllocation2DHelper isLocationChinaWithLocation:location.coordinate];

        // 国外
        if (!isChinaLocation) {
                CLGeocoder *geocoder = [[CLGeocoder alloc] init];
                [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *array, NSError *geocoderError){
                    @strongify(self);
                    
                    if(geocoderError) {
                        [self setDefaultLocation];
                       
                    } else if (array.count > 0){                        
                        CLPlacemark *placemark = [array firstObject];
                        
                        self.currentCity = placemark.locality;
                       
                        //fly 2016.2.29 如果定位出来的城市不存在本地的DB中，认定为定位失败
                        self.country = placemark.country;
                        self.countryId = [[CountryHelper sharedDataBase] readCountryIdWithCountryName:self.country];
                        
                        self.location = location.coordinate;
                        
                        CountryCodeModel *cityModel = [[CountryHelper sharedDataBase] getCountryCodeByAbbreviation:self.currentCity];
                        
                        if ([StringUtil isEmpty:self.currentCity] || !cityModel) {
                           
                            [self setDefaultLocation];
                            return;
                        }
                        
                        if (![MXCache valueForKey:kCurrentCityNameKey]) {
//                            [MXCityChangeHelper changeCityWithCityName:self.currentCity cityCode:[[CountryHelper sharedDataBase] readCurrentCityId]];
                        }
                        
                        self.countryId = [[CountryHelper sharedDataBase] readCountryIdWithCountryName:self.country];
                        self.currentAddress = [placemark.addressDictionary[@"FormattedAddressLines"] firstObject];
                        self.province = placemark.subAdministrativeArea ? : placemark.administrativeArea;
                        self.district = placemark.subLocality;
                        self.ISOcountryCode = placemark.ISOcountryCode;
                        self.country = placemark.country;
                        
                        [self handleLocationSuccess:location withError:geocoderError];
                    }
                }];
        }else{
            
            if (error && error.code == AMapLocationErrorLocateFailed) {

             
                [self setDefaultLocation];
                self.locateResult = NO;
                return;
            }
            
            self.locateResult = YES;
            if (!location || !regeocode)  {
                return;
            }
            self.location = location.coordinate;
            
            NSString *currentCity = (!regeocode.city ? regeocode.province : regeocode.city);
            currentCity = [currentCity handleSuffixString];
            
            //fly 2016.2.29 如果定位出来的城市不存在本地的DB中，认定为定位失败
            //要先修改 countryId
            self.country = regeocode.country;
            self.countryId = [[CountryHelper sharedDataBase] readCountryIdWithCountryName:self.country];
           
            CountryCodeModel *cityModel = [[CountryHelper sharedDataBase] getCountryCodeByAbbreviation:currentCity];
           
            
            if ([StringUtil isEmpty:currentCity] || !cityModel) {
                [self setDefaultLocation];
                return;
            }
           
            self.currentCity = currentCity;
            
            if (![MXCache valueForKey:kCurrentCityNameKey]) {
//                [MXCityChangeHelper changeCityWithCityName:currentCity cityCode:[[CountryHelper sharedDataBase] readCurrentCityId]];
            }
            
            // add by yang.xiangbao 2015/12/24 返回的数据会出现数组的情况
            // 定位失败的情况下会是NSArray
            if (![self.currentCity isKindOfClass:[NSString class]]) {
                return;
            }else{
                self.currentCity = [self.currentCity handleSuffixString];
            }
            
            self.province = regeocode.province;
            self.currentAddress = regeocode.formattedAddress;
            self.district = regeocode.district;
            
            [self saveReverseGeocodeInfo:location withError:error];
        }
    };
}

- (void)saveReverseGeocodeInfo:(CLLocation *)location withError:(NSError *)error{

    [self handleLocationSuccess:location withError:error];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *array, NSError *error){
        if (array.count > 0){

            CLPlacemark *placemark = [array firstObject];
            
            self.ISOcountryCode = placemark.ISOcountryCode;
            self.country = placemark.country;
            self.countryId = [[CountryHelper sharedDataBase] readCountryIdWithCountryName:self.country];
            
            [self recordLocationAddress];
            [self cacheMyself];
        }
    }];
}

- (void)handleLocationSuccess:(CLLocation *)location withError:(NSError *)error{
    
    if (error) {
        MLog(@"error = %@",error);
        [self setDefaultLocation];
        return;
    }

    [MXCache setValue:self.currentCity forKey:@"city"];
    MXAddressModel *model = [[MXAddressModel alloc] init];
    model.address = self.currentAddress;
    model.province = self.province;
    model.cityName = self.currentCity;
    model.district = self.district;
    
    [self configLocationNBSAppAgent];
    [self recordLocationAddress];
    [self cacheMyself];
    
    if (self.callback) {
        self.callback(location,model,error);
    }
}

- (void)cacheMyself{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    if (data) {
        [MXCache setValue:data forKey:ManagerCacheKey];
    }
}

- (void)storeLocationCache:(MXAddressModel*)cache {
    
    self.currentAddress= cache.address;
    self.province= cache.province;
    self.currentCity= cache.cityName;
    self.district= cache.district;
    self.location = cache.location.coordinate;
    self.ISOcountryCode = cache.ISOcountryCode;
    self.country = cache.country;
    self.countryId = cache.countryId;
    
    [self cacheMyself];
}

- (BOOL)isShouldStartUpdateLocation{
    NSTimeInterval currentTimeInt = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval lastTimeInt = [GVUserDefaults standardUserDefaults].lastUpdateLocation;
    return (currentTimeInt - lastTimeInt) > minUpdateTimeInterval;
}

- (instancetype)initWithCoder:(NSCoder *)coder{
    self = [super init];
    if (self) {
        self.currentCity = [coder decodeObjectForKey:@"currentCity"];
        CLLocationDegrees latitude = [coder decodeDoubleForKey:@"location.latitude"];
        CLLocationDegrees longitude = [coder decodeDoubleForKey:@"location.longitude"];
        self.location = CLLocationCoordinate2DMake(latitude, longitude);
        self.currentAddress = [coder decodeObjectForKey:@"currentAddress"];
        self.province = [coder decodeObjectForKey:@"province"];
        self.district = [coder decodeObjectForKey:@"district"];
        self.ISOcountryCode = [coder decodeObjectForKey:@"ISOcountryCode"];
        self.country = [coder decodeObjectForKey:@"country"];
        self.countryId = [coder decodeIntegerForKey:@"countryId"];
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.currentCity forKey:@"currentCity"];
    [aCoder encodeDouble:self.location.latitude forKey:@"location.latitude"];
    [aCoder encodeDouble:self.location.longitude forKey:@"location.longitude"];
    [aCoder encodeObject:self.currentAddress forKey:@"currentAddress"];
    [aCoder encodeObject:self.province forKey:@"province"];
    [aCoder encodeObject:self.district forKey:@"district"];
    [aCoder encodeObject:self.ISOcountryCode forKey:@"ISOcountryCode"];
    [aCoder encodeObject:self.country forKey:@"country"];
    [aCoder encodeInteger:self.countryId forKey:@"countryId"];
    
}

- (nonnull NSString *)currentCityCode {
    NSString *currentId;
    if ([StringUtil isEmpty:kCurrentCityCodeValue]) {
        CountryCodeModel *current = [[CountryHelper sharedDataBase] getCountryCodeByAbbreviation:AppCurrentCity];
        currentId = current ? [NSString stringWithFormat:@"%@",@(current.currentId)] : @"";
    } else {
        currentId = kCurrentCityCodeValue;
    }
    
    return currentId;
}

- (nonnull NSString *)currentCountryCode {
    NSString *countryCode = [MXCache valueForKey:kCurrentCountryCodeKey];
    if ([StringUtil isEmpty:countryCode]) {
        CountryCodeModel *current = [[CountryHelper sharedDataBase] getCountryCodeByAbbreviation:AppCurrentCity];
        countryCode = current ? current.countryCode : @"";
    }
    return countryCode;
}

- (void)amapLocationManager:(AMapLocationManager *)manager doRequireLocationAuth:(CLLocationManager*)locationManager {
    [locationManager requestAlwaysAuthorization];
}

@end
