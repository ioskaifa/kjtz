//
//  RegionAnnotation.m
//  MapDemo
//
//  Created by aken on 15/4/22.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import "RegionAnnotation.h"

@implementation RegionAnnotation



- (void)dictionaryToModel:(NSDictionary *)dic {
    
    
    if ([dic isKindOfClass:[NSDictionary class]]) {
        
        self.title=dic[@"name"];
        self.subtitle=dic[@"vicinity"];
        
        NSDictionary *temp=dic[@"geometry"];
        
        if ([temp isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *tempLoc=temp[@"location"];
            self.coordinate=CLLocationCoordinate2DMake([tempLoc[@"lat"] doubleValue], [tempLoc[@"lng"] doubleValue]);
            
        }
        
    }
    
}

@end
