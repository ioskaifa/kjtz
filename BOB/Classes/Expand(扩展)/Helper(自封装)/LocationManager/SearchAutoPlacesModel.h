//
//  SearchAutoPlacesModel.h
//  MapDemo
//
//  Created by aken on 15/4/24.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchAutoPlacesModel : NSObject

@property (nonatomic, copy) NSString *addressDesc;          // 详情地址1
@property (nonatomic, copy) NSString *name;               // 名称
@property (nonatomic, copy) NSString *adcode;             // 地区码
@property (nonatomic, copy) NSString *reference;        // google address的地址对象

/**
 * 将字典数据转换成实体对象
 */
- (void)dictionaryToModel:(NSDictionary *)dic;

@end
