//
//  MXLocationManager+Util.h
//  MoPal_Developer
//
//  Created by fly on 16/5/12.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "MXLocationManager.h"

typedef NS_ENUM(NSUInteger, DebugLocation) {
    DebugLocationCurrent    = 0,
    DebugLocationNewYork    = 1,
    DebugLocationMalaysia   = 2,
    DebugLocationSingapore  = 3,
    DebugLocationGuangZhou  = 4,
};

@interface MXLocationManager (Util)

/**
 *  配置Debug模式下的地理位置信息
 */
- (void)configDebugLocation;

/**
 *  配置听云上的一些账号信息
 */
- (void)configLocationNBSAppAgent;

/**
 *  记录Debug模式下定位信息
 */
- (void)recordLocationAddress;

/**
 *  在定位不到或未开启定位时，所有的异常情况下的默认位置
 */
- (void)setDefaultLocation;

///**
// *  设置摇一摇的城市位置
// *
// */
//- (void)fetchShakeCityCode:(CLLocationCoordinate2D)coordinate2D;

@end
