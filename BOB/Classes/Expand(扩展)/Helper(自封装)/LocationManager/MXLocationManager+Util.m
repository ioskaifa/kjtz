//
//  MXLocationManager+Util.m
//  MoPal_Developer
//
//  Created by fly on 16/5/12.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "MXLocationManager+Util.h"
#import "GVUserDefaults+Properties.h"
#import "MXAddressModel.h"
//#import "ShakeManager.h"
//#import "ShakeCommon.h"
#import "MXFixCllocation2DHelper.h"
#import "MXCache.h"
@implementation MXLocationManager (Util)


- (void)configLocationNBSAppAgent{
    //获取听云所需的cityInfo
//    [NBSAppAgent setCustomerData:[NSString stringWithFormat:@"lat=%@&lon=%@&fullAddress=%@",@(self.location.latitude),@(self.location.longitude),self.currentAddress] forKey:@"cityInfo"];
//    [NBSAppAgent setCustomerData:[StringUtil isEmpty:UDetail.user.userId]?@"loginStatus=未登录":@"loginStatus=已登录" forKey:@"loginStatus"];
//    NSString *serverNetType = ServerDomain_toString[[GVUserDefaults standardUserDefaults].serverType];
//    [NBSAppAgent setCustomerData:serverNetType forKey:@"serverNetType"];
//    NSString *loactionAuthorizationStatus = AuthorizationStatus_toString[[CLLocationManager authorizationStatus]];
//    [NBSAppAgent setCustomerData:loactionAuthorizationStatus forKey:@"loactionAuthorizationStatus"];
}

- (void)recordLocationAddress{
#ifdef OpenDebugVC
    
    NSString *locationString = $str(@"longitude：%.6f latitude：%.6f \n国家：%@ \n国家编码：%@ \n详细地址： %@ \n省：%@ \n市：%@ \n区：%@",self.location.longitude,self.location.latitude,self.country,self.ISOcountryCode,self.currentAddress,self.province,self.currentCity,self.district);
    MLog(@"locationString ->%@",locationString);
    [GVUserDefaults standardUserDefaults].locationAddress = locationString;
#endif
}


- (void)setDefaultLocation
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLocationDidUpdateFailer object:nil];
    
    
    return;
    
    // 以下默认数据暂时不要，如果定位失败，则通过手动选择城市，通过城市名反地理编码，取第一条数据
    
#ifdef OpenDebugVC
    if ([GVUserDefaults standardUserDefaults].debugLocation != DebugLocationCurrent) {
        [self configDebugLocation];
        return;
    }
#endif
    if (![StringUtil isEmpty:self.currentCity]) {
   
        return;
    }
    CLLocationDegrees latitude = 22.53425392324238;
    CLLocationDegrees longitude = 114.0631012344598;
    //中文 zhang.tai
    MXAddressModel *model = [[MXAddressModel alloc] init];
        self.currentCity = @"深圳";
        self.ISOcountryCode = @"CN";
        self.country = @"中国";
        model.address = @"深圳市福田区";
        model.province = @"广东";
    
    
    model.cityName =self.currentCity;
    self.countryId = 1;
    self.location = CLLocationCoordinate2DMake(latitude, longitude);
    [MXCache setValue:self.currentCity forKey:@"city"];
    CLLocation *location =[[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    
    if (self.callback) {
        self.callback(location,model,nil);
    }
}

- (void)configDebugLocation{
    DebugLocation debuglocation ; //[GVUserDefaults standardUserDefaults].debugLocation;
    CLLocationDegrees latitude = 22.53425392324238;
    CLLocationDegrees longitude = 114.0631012344598;
    switch (debuglocation) {
        case DebugLocationNewYork:
            latitude = 40.7127837;
            longitude = 74.0059;
            
            self.currentCity = @"纽约";
            self.location = CLLocationCoordinate2DMake(latitude, longitude);
            self.currentAddress = @"230 Broadway, New York";
            self.province = @"纽约";
            self.district = @"纽约";
            self.ISOcountryCode = @"US";
            self.country = @"美国";
            
            break;
            
        case DebugLocationMalaysia:
            latitude = 3.139003;
            longitude = 101.6868;
            
            self.currentCity = @"吉隆坡";
            self.location = CLLocationCoordinate2DMake(latitude, longitude);
            self.currentAddress = @"230 Broadway, New York";
            self.province = @"吉隆坡";
            self.district = @"吉隆坡";
            self.ISOcountryCode = @"MY";
            self.country = @"马拉西亚";
            
            break;
        case DebugLocationSingapore:
            latitude = 1.3553794;
            longitude = 103.867744;
            
            self.currentCity = @"新加坡市";
            self.location = CLLocationCoordinate2DMake(latitude, longitude);
            self.currentAddress = @"230 Broadway, New York";
            self.province = @"新加坡市";
            self.district = @"新加坡市";
            self.ISOcountryCode = @"SG";
            self.country = @"新加坡";
            break;
        case DebugLocationGuangZhou:
        {
            latitude = 23.142172;
            longitude = 113.325090;
            
            self.currentCity = @"广州市";
            self.location = CLLocationCoordinate2DMake(latitude, longitude);
            self.currentAddress = @"230 Broadway, New York";
            self.province = @"广东省";
            self.district = @"天河区";
            self.ISOcountryCode = @"CN";
            self.country = @"中国";
            self.currentAddress = @"广东省广州市天河区林和街道中信广场国际公寓";
            break;
        }
        default:
        {
            latitude = 22.530252;
            longitude = 114.069314;
            
            self.currentCity = @"深圳市";
            self.location = CLLocationCoordinate2DMake(latitude, longitude);
            self.province = @"广东省";
            self.district = @"福田区";
            self.ISOcountryCode = @"CN";
            self.country = @"中国";
            self.currentAddress = @"广东省深圳市福田区福田街道华景花园（万景花园西南）";
            break;
        }
    }
    
    self.countryId = @"";//[[CountryHelper sharedDataBase] readCountryIdWithCountryName:self.country];
    
    MXAddressModel *model = [[MXAddressModel alloc] init];
    model.address = self.currentAddress;
    model.province = self.province;
    model.cityName = self.currentCity;
    model.district = self.district;
    
    NSData* data =  [NSKeyedArchiver archivedDataWithRootObject:self];
    if (data) {
        [MXCache setValue:data forKey:@"MXLocationManagerCache"];
    }
    CLLocation *clLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    if (self.callback) {
        self.callback(clLocation,model,nil);
    }
}



@end
