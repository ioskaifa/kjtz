//
//  RegionAnnotation.h
//  MapDemo
//
//  地理位置消息对象，注解
//  Created by aken on 15/4/22.
//  Copyright (c) 2015年 MapDemo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface RegionAnnotation : NSObject<MKAnnotation>

// 标题
@property (nonatomic, copy) NSString *title;

// 详细描述
@property (nonatomic, copy) NSString *subtitle;

@property (nonatomic, copy) NSString *city;

// 地理位置
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;


- (void)dictionaryToModel:(NSDictionary *)dic;

@end
