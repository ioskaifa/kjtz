//
//  MXAddressModel.h
//  MoPal_Developer
//
//  地址对象信息
//  Created by aken on 15/3/11.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MXAddressModel : NSObject

// 省/直辖市
@property (nonatomic, copy) NSString *province;
// 城市名称
@property (copy) NSString *cityName;

// 地址名称
@property (copy) NSString *address;

// 地区
@property (copy) NSString *district;

// 经纬度
@property (nonatomic, copy) CLLocation *location;

// 区域编码
@property (nonatomic, copy) NSString *ISOcountryCode;

// 国家
@property (nonatomic, copy) NSString *country;

/// 数据库国家编号
@property (nonatomic, assign) NSInteger countryId;

@end
