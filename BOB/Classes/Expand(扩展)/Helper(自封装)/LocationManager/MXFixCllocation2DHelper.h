//
//  MXFixCllocation2DHelper.h
//  Moxian
//
//  Created by litiankun on 14-1-22.
//  Copyright (c) 2014年 Moxian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <math.h>

@interface MXFixCllocation2DHelper : NSObject

+ (CLLocationCoordinate2D)transformFromWGSToGCJ:(CLLocationCoordinate2D)wgsLoc;

+ (instancetype)shared;

// 判断location是否在中国
- (BOOL)isInsideChina:(CLLocationCoordinate2D)location;

+ (BOOL)isLocationChina;

+ (BOOL)isLocationChinaWithLocation:(CLLocationCoordinate2D)coordinate;

@end
