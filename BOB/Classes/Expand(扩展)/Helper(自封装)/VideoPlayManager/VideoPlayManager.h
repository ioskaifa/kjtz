//
//  VideoPlayManager.h
//  BOB
//
//  Created by mac on 2020/1/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoPlayManager : NSObject
+ (instancetype)share;
///全屏播放
- (void)play:(NSString *)url superView:(UIView *)superView;
- (void)playLocal:(NSString *)url superView:(UIView *)superView;
///父视图播放
- (void)play:(NSString *)url onSuperView:(UIView *)superView;
///父视图播放本地
- (void)playLocal:(NSString *)url onSuperView:(UIView *)superView;
- (void)playStop;
@end

NS_ASSUME_NONNULL_END
