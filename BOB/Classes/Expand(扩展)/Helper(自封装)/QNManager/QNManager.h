//
//  QNManager.h
//  Lcwl
//
//  Created by mac on 2018/12/11.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QiniuSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface QNManager : NSObject
+ (QNManager *)shared;
@property(nonatomic, copy) NSString *qnHost;

- (void)uploadImage:(UIImage *)image completion:(FinishedBlock)block;
- (void)uploadImages:(NSArray *)images assets:(NSArray *)assets completion:(FinishedBlock)block;
- (void)uploadAssets:(NSArray *)assets completion:(FinishedBlock)block;
- (void)uploadVideo:(PHAsset *)asset completion:(FinishedBlock)block;
- (void)uploadAudio:(NSString *)filePath completion:(FinishedBlock)block;
- (void)uploadFilePath:(NSString *)filePath completion:(FinishedBlock)block;
- (void)uploadImages:(NSArray *)images  completion:(FinishedBlock)block ;

@end

NS_ASSUME_NONNULL_END
