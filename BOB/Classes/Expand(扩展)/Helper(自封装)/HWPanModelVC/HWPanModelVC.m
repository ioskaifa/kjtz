//
//  HWPanModelVC.m
//  RatelBrother
//
//  Created by AlphaGo on 2020/5/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "HWPanModelVC.h"
#import <HWPanModal/HWPanModal.h>

@interface HWPanModelVC () <HWPanModalPresentable>

@property(nonatomic,strong) UIView *customView;

@property(nonatomic,strong) UIViewController *superVC;

@end

@implementation HWPanModelVC

+ (instancetype)showInVC:(UIViewController *)superVC customView:(UIView *)customView {
    if(customView == nil) {
        return nil;
    }
    HWPanModelVC *vc = [[HWPanModelVC alloc]init];
    vc.superVC = superVC;
    [vc setValue:customView forKey:@"customView"];
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.customView];
    [self.customView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom);
        make.height.equalTo(@(self.customView.height));
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismiss) name:@"HWPanModelVCDismiss" object:nil];
}

- (void)show {
    [self.superVC presentPanModal:self];
}

- (void)dismiss {
    Block_Exec(self.willDismissblock,nil);
    [self dismissViewControllerAnimated:YES completion:^{
        Block_Exec(self.didDismissblock,nil);
    }];
}

#pragma mark - HWPanModalPresentable

- (PanModalHeight)shortFormHeight {
    if ([self isLandScape]) {
        return [self longFormHeight];
    }
    return PanModalHeightMake(PanModalHeightTypeMax, 200);
}
- (PanModalHeight)longFormHeight{
    return [self shortFormHeight];
}

// 当转屏且为横屏时，为全屏幕模式。
- (CGFloat)topOffset {
    if ([self isLandScape]) {
        return 0;
    } else {
        return 40;
    }
}

- (BOOL)shouldRespondToPanModalGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer{
    return NO;
}


- (UIViewAnimationOptions)transitionAnimationOptions {
    return UIViewAnimationOptionCurveLinear;
}

- (BOOL)isLandScape {
    if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight ||
        [UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft) {
        return YES;
    }
    return NO;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    CGPoint point = [[touches anyObject] locationInView:self.customView];
    if (![self.customView.layer containsPoint:point]) {
        [self dismiss];
    }
}

/**
 * 是否顶部圆角
 * 齐刘海不需要
 */
- (BOOL)showDragIndicator {
    if(safeAreaInset().top > 0) {
        return NO;
    } else {
        return YES;
    }
}
@end
