//
//  HWPanModelVC.h
//  RatelBrother
//
//  Created by XXX on 2020/5/1.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HWPanModelVC : UIViewController
///点击空白 页面dismiss时候的回调
@property (nonatomic, copy) FinishedBlock willDismissblock;

@property (nonatomic, copy) FinishedBlock didDismissblock;

+ (instancetype)showInVC:(UIViewController *)superVC customView:(UIView *)customView;

- (void)show;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
