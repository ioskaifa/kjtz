//
//  UIView+SpeedyView.h
//  RatelBrother
//
//  Created by AlphaGo on 2020/4/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaceholderTextView.h"
#import "TextCaptchaView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIView (SpeedyView)

- (MyLinearLayout *)findLinearLayout;

///创建一个MyLinearLayout
+ (MyLinearLayout *)linearLayout:(MyOrientation)orientation inset:(UIEdgeInsets)inset;

///添加一个子视图，前提条件是该视图已经存在mylayout容器
- (MyLinearLayout *)addSubviewOnLinearLayout:(UIView *)subview;

- (MyLinearLayout *)addMyLinearLayout:(MyOrientation)orientation inset:(UIEdgeInsets)inset;
///添加一个自动增长的layout，可使scrollview自定调整contentsize
- (MyLinearLayout *)addAutoMyLinearLayout:(MyOrientation)orientation;

- (MyLinearLayout *)removeSubviewOnLinearLayout;

///添加一个线性布局容器
- (MyLinearLayout *)addMyLinearLayout:(MyOrientation)orientation;

///添加一个UIView，再在此View上添加一个线性布局容器，返回值UIView以供外界进一步设置大小等属性
///注意⚠️：属性定义似乎不能重叠，比如里面设置了myBottom,外面设置myHeight就会无效，此方法待改进
- (UIView *)addUIViewMyLinearLayout:(MyOrientation)orientation;

- (UIView *)addUIViewMyLinearLayout:(UIEdgeInsets)inset orientation:(MyOrientation)orientation;

///添加一个view到水平布局容器中，并且填充剩余空间
- (UIView *)addHorUIViewForFill:(UIEdgeInsets)inset orientation:(MyOrientation)orientation;
///先添加一个UIScrollView,再在此基础上添加一个宽度或者高度自适应子视图的线性布局容器，以保证ScrollView能正常接管contentSize
- (MyLinearLayout *)addScrollViewMyLinearLayout:(MyOrientation)orientation;
- (UIScrollView *)addScrollViewMyLinearLayout:(MyOrientation)orientation inset:(UIEdgeInsets)inset;

///添加一个线性布局容器，可以直接带上子视图
- (MyLinearLayout *)addSubviewsOnLinearLayout:(NSArray *)subviews orientation:(MyOrientation)orientation gravity:(MyGravity)gravity;


+ (UIView *)line;
+ (UIView *)cellLine;
+ (UIView *)fullLine;
+ (UIView *)gapLine;
+ (UIView *)line:(UIColor *)lieColor;
+ (UIView *)gapLineWithHeight:(CGFloat)height;
+ (UIView *)gapLineWithHeight:(CGFloat)height color:(UIColor *)color;
+ (UIView *)cellWithArrow:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset;
+ (UIView *)cellStringWithArrow:(NSString *)title height:(CGFloat)height;
+ (UIView *)cellAttributedStringWithArrow:(NSMutableAttributedString *)title height:(CGFloat)height;
+ (UIView *)cellBgName:(NSString *)imgName AttributedStringWithArrow:(NSMutableAttributedString *)title height:(CGFloat)height;
+ (UIView *)cellWithIconTitleArrow:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset;
+ (UIView *)cellWithIconTitlePointArrow:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset;
///左边title，右边一个头像和arrow
+ (UIView *)cellWithRightIconArrow:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset;
///标题+detail+arrow
+ (UIView *)cellWithDetailArrow:(NSMutableAttributedString *)title detail:(NSMutableAttributedString *)detail inset:(UIEdgeInsets)inset;
///title和detail各占据两端
+ (UIView *)cellWithDetail:(NSMutableAttributedString *)title detail:(NSMutableAttributedString *)detail inset:(UIEdgeInsets)inset action:(FinishedBlock)action;
///添加N个格子的布局---只有一行
+ (MyBaseLayout *)aequilateHorGrid:(NSArray<UIView *> *)arr;
///添加N个格子的布局---只有一行,外界一定要对该返回的layout进行宽高等设置
+ (MyBaseLayout *)aequilateHorGrid:(NSArray<UIView *> *)arr inset:(UIEdgeInsets)inset;
///添加四个格子的布局
+ (MyBaseLayout *)fourGrid:(NSArray<UIView *> *)arr itemSize:(CGSize)size;
///添加四个格子的布局,需要传入每个格子的size
+ (MyBaseLayout *)fourGrid:(NSArray<UIView *> *)arr itemSizes:(NSArray<NSValue *> *)sizes;

///添加N个格子的布局---多行
+ (MyBaseLayout *)aequilateMultiLineGrid:(NSArray<UIView *> *)arr rowHeight:(CGFloat)rowHeight colNum:(NSInteger)colNum action:(FinishedBlock)action;
///添加N个格子的布局---多行,多了一个inset参数
+ (MyBaseLayout *)aequilateMultiLineGrid:(NSArray<UIView *> *)arr rowHeight:(CGFloat)rowHeight colNum:(NSInteger)colNum inset:(UIEdgeInsets)inset action:(FinishedBlock)action;

///上下两行文字，传入的是上下两行的NSMutableAttributedString
+ (MyBaseLayout *)aequilateLabelMultiLineGrid:(NSArray<NSArray<NSMutableAttributedString *> *> *)arr rowHeight:(CGFloat)rowHeight colNum:(NSInteger)colNum inset:(UIEdgeInsets)inset action:(FinishedBlock)action;

///添加一个由文本决定size的label
- (UILabel *)addLabel:(NSMutableAttributedString *)att inset:(UIEdgeInsets)inset;

///添加一个圆形的头像,image可以是一个UIImage类型的，也可以是一个url
- (UIImageView *)addHeadImg:(id)image inset:(UIEdgeInsets)inset size:(CGFloat)size;

///注意⚠️：仅用于垂直布局，添加一个水平label，inset用于设置myTop,myLeft,myRight,高度默认使用MyLayoutSize.wrap
- (UILabel *)addOneHorLabel:(NSMutableAttributedString *)att inset:(UIEdgeInsets)inset;
- (UILabel *)addOneHorLabel:(NSMutableAttributedString *)att numberOfLines:(NSInteger)numberOfLines textAlignmentN:(NSTextAlignment)textAlignment inset:(UIEdgeInsets)inset;

///左边一个label，右边一个label, 并且位两端对齐
- (UIView *)addBothLabel:(NSMutableAttributedString *)leftAtt rightAtt:(NSMutableAttributedString *)rightAtt inset:(UIEdgeInsets)inset;

///添加个带icon的title
- (UIButton *)addOneButtonWithIcon:(NSString *)icon content:(NSMutableAttributedString *)content inset:(UIEdgeInsets)inset;

///添加一个按钮，左边文字右边icon
- (UIButton *)addOneButtonWithRightIcon:(NSString *)icon size:(CGSize)iconSize content:(NSMutableAttributedString *)content inset:(UIEdgeInsets)inset action:(FinishedBlock)action;

///添加一个指定大小的button
- (UIButton *)addButtonWithIcon:(NSString *)icon size:(CGSize)size content:(NSMutableAttributedString *)content inset:(UIEdgeInsets)inset;

///添加个带normal selected两种状态的icon以及title
- (UIButton *)addOneButtonWithIcon:(NSString *)normalIcon selectedIcon:(NSString *)selectedIcon content:(NSMutableAttributedString *)content inset:(UIEdgeInsets)inset;

///注意⚠️：仅用于垂直布局，添加一个水平的横线
- (UIImageView *)addOneHorLine:(UIColor *)color inset:(UIEdgeInsets)inset;

- (UIImageView *)addOneVerLine:(CGFloat)height inset:(UIEdgeInsets)inset;

///添加一个全屏的scrollView
- (UIScrollView *)addScrollView;
- (UIImageView *)addBackgroundImage:(id)image;
- (UIImageView *)addCenterImage:(id)image size:(CGSize)size;
///添加一张高度自适应的图片
- (UIImageView *)addAutoImage:(id)image width:(CGFloat)width;
///添加一张图片,如果imageSize为0，则高度自适应
- (UIImageView *)addImage:(id)image imageSize:(CGSize)imageSize inset:(UIEdgeInsets)inset;
///添加一个简单的TextField
- (UITextField *)addTextField:(id)placeHolder inset:(UIEdgeInsets)inset;
- (UITextField *)addTextField:(UIFont *)font placeHolder:(id)placeHolder inset:(UIEdgeInsets)inset;
- (PlaceholderTextView *)addTextView:(UIFont *)font placeHolder:(id)placeHolder inset:(UIEdgeInsets)inset;
///获取验证码
- (TextCaptchaView *)addCaptchaView:(FinishedBlock)action sendBlock:(FinishedBlock)sendBlock;
+ (UIButton *)bottomBnt:(NSString *)title inset:(UIEdgeInsets)inset action:(FinishedBlock)block;
+ (UIView *)inputView:( NSString * _Nullable)imgName title:( NSString * _Nullable)title placeHolder:( NSString * _Nullable)placeHolder leftWeight:(CGFloat)leftWeight;
///添加四张图片到四个角,数组顺序为@[topleft,topright,bottomleft,bottomright]
- (void)addCornerImage:(NSArray *)imageArr inset:(CGFloat)inset;
- (void)addBorder:(UIColor *)color;

+ (UIView *)functionView:(NSArray<NSDictionary *> *)contentArr imageSize:(CGSize)imageSize titleColor:(UIColor *)titleColor titleFont:(UIFont *)titleFont rowHeight:(CGFloat)rowHeight colNum:(NSInteger)colNum acition:(FinishedBlock)tapBlock;

///左边一个头像，右边上下两个label
+ (UIView *)personalInfo:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title subTitle:(NSMutableAttributedString *)subTitle action:(FinishedBlock)block;

///左边一个头像，右边上面一个labe，下面为button，用于显示规格，该方法快速展示商品
+ (UIView *)productInfo:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title subTitle:(NSMutableAttributedString *)subTitle action:(FinishedBlock)block;

+ (UIView *)topImageBottomLabel:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset action:(FinishedBlock)block;

+ (UIView *)topImageTwoBottomLabel:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title bottomTitle:(NSMutableAttributedString *)bottomTitle inset:(UIEdgeInsets)inset action:(FinishedBlock)block;

+ (UIView *)topBottomLabel:(NSMutableAttributedString *)title bottomTitle:(NSMutableAttributedString *)bottomTitle inset:(UIEdgeInsets)inset action:(FinishedBlock)block;

///左边一个头像，右边上面一个labe,label后面紧接着可以携带一个箭头
+ (MyLinearLayout *)headTitle:(id)image imageSize:(CGSize)imageSize isCircle:(BOOL)isCircle needTitleArrow:(BOOL)needTitleArrow title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset action:(FinishedBlock)action;

+ (UIView *)placeholderHorzView;
///垂直占位view
+ (UIView *)placeholderVertView;

- (void)addMyLayoutProperty:(CGFloat)top left:(CGFloat)left bottom:(CGFloat)bottom right:(CGFloat)right;

+ (MyBaseLayout *)stdCellView:(NSString *)left right:(NSString *)right arrow:(NSString *)arrow rowHeight:(CGFloat)height;

+ (UIView *)addBottomSafeAreaWithContentView:(UIView *)contentView;

+ (MyBaseLayout *)factoryLeft:(NSString *)left rightText:(NSString *)right;

+ (MyBaseLayout *)factoryLeft:(NSString *)left rightText:(NSString *)right topEdge:(CGFloat)top bottomEdge:(CGFloat)bottom;

@end

NS_ASSUME_NONNULL_END



