//
//  SpeedyMacro.h
//  RatelBrother
//
//  Created by AlphaGo on 2020/4/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#ifndef SpeedyMacro_h
#define SpeedyMacro_h

#define FullInSuperView(view) \
[view mas_makeConstraints:^(MASConstraintMaker *make) { \
    make.edges.equalTo(@(0)); \
}]; \

#endif /* SpeedyMacro_h */
