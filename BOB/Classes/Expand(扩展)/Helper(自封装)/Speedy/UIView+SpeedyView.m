//
//  UIView+SpeedyView.m
//  RatelBrother
//
//  Created by AlphaGo on 2020/4/20.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UIView+SpeedyView.h"
#import "UIView+Utils.h"

@implementation UIView (SpeedyView)

- (MyLinearLayout *)findLinearLayout {
    return [self viewWithTag:100001];
}

- (MyLinearLayout *)addSubviewOnLinearLayout:(UIView *)subview {
    MyLinearLayout *linearLayout = [self viewWithTag:100001];
    [linearLayout addSubview:subview];
    return linearLayout;
}

- (MyLinearLayout *)removeSubviewOnLinearLayout {
    MyLinearLayout *linearLayout = [self viewWithTag:100001];
    [linearLayout removeAllSubviews];
    return linearLayout;
}

+ (MyLinearLayout *)linearLayout:(MyOrientation)orientation inset:(UIEdgeInsets)inset {
    MyLinearLayout *linearLayout = [MyLinearLayout linearLayoutWithOrientation:orientation];
    linearLayout.myTop = inset.top;
    linearLayout.myLeft = inset.left;
    linearLayout.myRight = inset.right;
    linearLayout.myHeight = inset.bottom;
    linearLayout.tag = 100001;
    return linearLayout;
}

- (MyLinearLayout *)addMyLinearLayout:(MyOrientation)orientation {
    MyLinearLayout *linearLayout = [MyLinearLayout linearLayoutWithOrientation:orientation];
    linearLayout.myTop = 0;
    linearLayout.myLeft = 0;
    linearLayout.myRight = 0;
    linearLayout.myBottom = 0;
    linearLayout.gravity = MyGravity_None;
    linearLayout.tag = 100001;
    [self addSubview:linearLayout];

    return linearLayout;
}

- (MyLinearLayout *)addMyLinearLayout:(MyOrientation)orientation inset:(UIEdgeInsets)inset {
    MyLinearLayout *linearLayout = [MyLinearLayout linearLayoutWithOrientation:orientation];
    linearLayout.myTop = inset.top;
    linearLayout.myLeft = inset.left;
    linearLayout.myRight = inset.right;
    linearLayout.myHeight = inset.bottom;
//    if(orientation == MyOrientation_Horz) {
//        linearLayout.myWidth = MyLayoutSize.wrap;
//    } else {
//        linearLayout.myRight = 0;
//    }
    
    linearLayout.tag = 100001;
    [self addSubview:linearLayout];

    return linearLayout;
}

- (MyLinearLayout *)addAutoMyLinearLayout:(MyOrientation)orientation {
    MyLinearLayout *linearLayout = [MyLinearLayout linearLayoutWithOrientation:orientation];
    linearLayout.myTop = 0;
    linearLayout.myLeft = 0;
    if(orientation == MyOrientation_Horz) {
        linearLayout.myWidth = MyLayoutSize.wrap;
    } else {
        linearLayout.myRight = 0;
    }
    
    if(orientation == MyOrientation_Vert) {
        linearLayout.myHeight = MyLayoutSize.wrap;
    } else {
        linearLayout.myBottom = 0;
    }
    linearLayout.gravity = MyGravity_None;
    linearLayout.tag = 100001;
    [self addSubview:linearLayout];

    return linearLayout;
}

- (MyLinearLayout *)addScrollViewMyLinearLayout:(MyOrientation)orientation {
    UIScrollView *scrollView = [UIScrollView new];
    scrollView.tag = 999999;
    [self addSubview:scrollView];
    //此处用约束会造成在tableview嵌套的话，在ios11上面向上滑动手势会冲突，用frame就不会了，这里先记录一下，后续解决
    FullInSuperView(scrollView);

    return [scrollView addAutoMyLinearLayout:orientation];
}

- (UIScrollView *)addScrollViewMyLinearLayout:(MyOrientation)orientation inset:(UIEdgeInsets)inset {
    UIScrollView *scrollView = [UIScrollView new];
    scrollView.tag = 999999;
    scrollView.myTop = inset.top;
    scrollView.myLeft = inset.left;
    scrollView.myRight = inset.right;
    scrollView.myHeight = inset.bottom;
    [self addSubviewOnLinearLayout:scrollView];
    [scrollView addAutoMyLinearLayout:orientation];
    return scrollView;
}

- (UIView *)addUIViewMyLinearLayout:(MyOrientation)orientation {
    UIView *view = [UIView new];
    view.tag = 888888;
    view.myTop = 0;
    view.myLeft = 0;
    view.myRight = 0;
    view.myBottom = 0;
    //view.myHeight = MyLayoutSize.fill;
    [self addSubviewOnLinearLayout:view];
    [view addMyLinearLayout:orientation];
    return view;
}

- (UIView *)addUIViewMyLinearLayout:(UIEdgeInsets)inset orientation:(MyOrientation)orientation {
    UIView *view = [UIView new];
    view.tag = 888888;
    view.myTop = inset.top;
    view.myLeft = inset.left;
    view.myRight = inset.right;
    view.myHeight = inset.bottom;
    [self addSubviewOnLinearLayout:view];
    [view addMyLinearLayout:orientation];
    return view;
}

- (UIView *)addHorUIViewForFill:(UIEdgeInsets)inset orientation:(MyOrientation)orientation {
    UIView *view = [UIView new];
    view.tag = 888888;
    view.myTop = inset.top;
    view.myLeft = inset.left;
    view.weight = 1;
    view.myHeight = inset.bottom;
    [self addSubviewOnLinearLayout:view];
    [view addMyLinearLayout:orientation];
    return view;
}

- (MyLinearLayout *)addSubviewsOnLinearLayout:(NSArray *)subviews orientation:(MyOrientation)orientation gravity:(MyGravity)gravity {
    MyLinearLayout *linearLayout = [MyLinearLayout linearLayoutWithOrientation:orientation];
    linearLayout.myTop = 0;
    linearLayout.myLeft = 0;
    linearLayout.myRight = 0;
    linearLayout.myBottom = 0;
    linearLayout.gravity = gravity;
    linearLayout.tag = 100001;
    [self addSubview:linearLayout];
    
    for(UIView *view in subviews) {
        [linearLayout addSubview:view];
    }
    return linearLayout;
}

- (UIImageView *)addHeadImg:(id)image inset:(UIEdgeInsets)inset size:(CGFloat)size {
    UIImageView *imgView = [[UIImageView alloc] init];
    [imgView setViewCornerRadius:size/2];
    imgView.myTop = inset.top;
    imgView.myLeft = inset.left;
    imgView.myWidth = size;
    imgView.myHeight = size;
    if([image isKindOfClass:[UIImage class]]) {
        imgView.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:image]];
    }
    [self addSubviewOnLinearLayout:imgView];
    return imgView;
}

- (UILabel *)addLabel:(NSMutableAttributedString *)att inset:(UIEdgeInsets)inset {
    UILabel *label = [[UILabel alloc] init];
    label.attributedText = att;
    label.tag = 200002;
    label.myTop = inset.top;
    label.myLeft = inset.left;
    label.myWidth = MyLayoutSize.wrap;
    label.myHeight = MyLayoutSize.wrap;
    [self addSubviewOnLinearLayout:label];
    return label;
}

- (UILabel *)addOneHorLabel:(NSMutableAttributedString *)att inset:(UIEdgeInsets)inset {
    UILabel *label = [[UILabel alloc] init];
    label.attributedText = att;
    label.myTop = inset.top;
    label.myLeft = inset.left;
    label.myRight = inset.right; //这种写法只能用于垂直布局下有效，水平布局下宽度会为0
    label.myHeight = MyLayoutSize.wrap;
    label.tag = 200002;
    [self addSubviewOnLinearLayout:label];
    return label;
}

- (UILabel *)addOneHorLabel:(NSMutableAttributedString *)att numberOfLines:(NSInteger)numberOfLines textAlignmentN:(NSTextAlignment)textAlignment inset:(UIEdgeInsets)inset {
    UILabel *label = [[UILabel alloc] init];
    label.attributedText = att;
    label.numberOfLines = numberOfLines;
    label.textAlignment = textAlignment;
    
    label.myTop = inset.top;
    label.myLeft = inset.left;
    label.myRight = inset.right; //这种写法只能用于垂直布局下有效，水平布局下宽度会为0
    label.myHeight = MyLayoutSize.wrap;
    [self addSubviewOnLinearLayout:label];
    return label;
}

///左边一个label，右边一个label, 并且位两端对齐
- (UIView *)addBothLabel:(NSMutableAttributedString *)leftAtt rightAtt:(NSMutableAttributedString *)rightAtt inset:(UIEdgeInsets)inset {
    UIView *object = [UIView bothEndsButton:leftAtt rightTitle:rightAtt action:nil];;
    object.myTop = inset.top;
    object.myLeft = inset.left;
    object.myRight = inset.right;
    object.myHeight = inset.bottom;
    [self addSubviewOnLinearLayout:object];
    return object;
}

///添加个带icon的title
- (UIButton *)addOneButtonWithIcon:(NSString *)icon content:(NSMutableAttributedString *)content inset:(UIEdgeInsets)inset {
    UIButton *object = [UIButton new];
    [object setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [object setAttributedTitle:content forState:UIControlStateNormal];
    if(icon) {
        [object setImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    }
    
    object.myTop = inset.top;
    object.myLeft = inset.left;
    object.myRight = inset.right; //这种写法只能用于垂直布局下有效，水平布局下宽度会为0
    object.myHeight = MyLayoutSize.wrap;
    [self addSubviewOnLinearLayout:object];
    return object;
}

///添加一个按钮，左边文字右边icon
- (UIButton *)addOneButtonWithRightIcon:(NSString *)icon size:(CGSize)iconSize content:(NSMutableAttributedString *)content inset:(UIEdgeInsets)inset action:(FinishedBlock)action {
    UIButton *object = [UIButton new];
    [object addAction:action];
    [object addMyLinearLayout:MyOrientation_Horz].gravity = MyGravity_Horz_Between | MyGravity_Vert_Center;
    [object addLabel:content inset:UIEdgeInsetsZero];
    [object addImage:[UIImage imageNamed:icon] imageSize:iconSize inset:UIEdgeInsetsZero];
    
    object.myTop = inset.top;
    object.myLeft = inset.left;
    object.myRight = inset.right;
    object.myHeight = inset.bottom;
    [self addSubviewOnLinearLayout:object];
    return object;
}

///添加一个指定大小的button
- (UIButton *)addButtonWithIcon:(NSString *)icon size:(CGSize)size content:(NSMutableAttributedString *)content inset:(UIEdgeInsets)inset {
    UIButton *object = [UIButton new];
    [object setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [object setAttributedTitle:content forState:UIControlStateNormal];
    [object setImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    
    object.myTop = inset.top;
    object.myLeft = inset.left;
    object.myWidth = size.width;
    object.myHeight = size.height;
    [self addSubviewOnLinearLayout:object];
    return object;
}

///添加个带normal selected两种状态的icon以及title
- (UIButton *)addOneButtonWithIcon:(NSString *)normalIcon selectedIcon:(NSString *)selectedIcon content:(NSMutableAttributedString *)content inset:(UIEdgeInsets)inset {
    UIButton *object = [UIButton new];
    [object setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [object setAttributedTitle:content forState:UIControlStateNormal];
    [object setImage:[UIImage imageNamed:normalIcon] forState:UIControlStateNormal];
    [object setImage:[UIImage imageNamed:selectedIcon] forState:UIControlStateSelected];
    
    object.myTop = inset.top;
    object.myLeft = inset.left;
    object.myWidth = MyLayoutSize.wrap;
    object.myHeight = MyLayoutSize.wrap;
    [self addSubviewOnLinearLayout:object];
    return object;
}

- (UIImageView *)addOneHorLine:(UIColor *)color inset:(UIEdgeInsets)inset {
    UIImageView *line = [[UIImageView alloc] init];
    line.backgroundColor = color;
    line.myTop = inset.top;
    line.myLeft = inset.left;
    line.myRight = inset.right;
    line.myHeight = inset.bottom;
    [self addSubviewOnLinearLayout:line];
    return line;
}

- (UIImageView *)addOneVerLine:(CGFloat)width inset:(UIEdgeInsets)inset {
    UIImageView *line = [[UIImageView alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#EDEDED"];
    line.myTop = inset.top;
    line.myBottom = inset.bottom;
    line.myLeft = inset.left;
    line.myWidth = width;
    [self addSubviewOnLinearLayout:line];
    return line;
}

+ (UIView *)gapLine:(CGFloat)height {
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor clearColor];
    line.myTop = 0;
    line.myLeft = 0;
    line.myRight = 0;
    line.myHeight = 10;
    return line;
}

+ (UIView *)line {
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
    line.myTop = 0;
    line.myLeft = 15;
    line.myRight = 15;
    line.myHeight = 1;
    return line;
}

+ (UIView *)cellLine {
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
    line.myTop = 0;
    line.myLeft = 12;
    line.myRight = 12;
    line.myHeight = 1;
    return line;
}

+ (UIView *)fullLine {
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
    line.myTop = 0;
    line.myLeft = 0;
    line.myRight = 0;
    line.myHeight = 1;
    return line;
}

+ (UIView *)gapLine {
    UIView *gapLine = [UIView new];
    gapLine.backgroundColor = [UIColor moBackground];
    gapLine.myTop = 0;
    gapLine.myLeft = 0;
    gapLine.myRight = 0;
    gapLine.myHeight = 10;
    return gapLine;
}

+ (UIView *)line:(UIColor *)lieColor {
    UIView *line = [UIView new];
    line.backgroundColor = lieColor;
    line.myTop = 0;
    line.myLeft = 15;
    line.myRight = 15;
    line.myHeight = SINGLE_LINE_HEIGHT;
    return line;
}

+ (UIView *)gapLineWithHeight:(CGFloat)height {
    UIView *gapLine = [UIView new];
    gapLine.backgroundColor = [UIColor clearColor];
    gapLine.myTop = 0;
    gapLine.myLeft = 0;
    gapLine.myRight = 0;
    gapLine.myHeight = height;
    return gapLine;
}

+ (UIView *)gapLineWithHeight:(CGFloat)height color:(UIColor *)color {
    UIView *gapLine = [UIView new];
    gapLine.backgroundColor = color;
    gapLine.myTop = 0;
    gapLine.myLeft = 0;
    gapLine.myRight = 0;
    gapLine.myHeight = height;
    return gapLine;
}


+ (UIView *)cellWithArrow:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset {
    UIView *baseView = [UIView new];
    
    UILabel *lbl = [UILabel new];
    lbl.attributedText = title;
    [baseView addSubview:lbl];
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(@(0));
    }];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
    [baseView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-15));
        make.centerY.equalTo(baseView);
        //make.width.height.equalTo(@(10));
        make.width.equalTo(@6);
        make.height.equalTo(@12);
    }];
    baseView.myTop = inset.top;
    baseView.myLeft = inset.left;
    baseView.myRight = inset.right;
    baseView.myHeight = inset.bottom;
    return baseView;
}

+ (UIView *)cellStringWithArrow:(NSString *)title height:(CGFloat)height {
    UIView *baseView = [UIView new];
    baseView.backgroundColor = [UIColor whiteColor];
    
    UILabel *lbl = [UILabel new];
    lbl.text = title;
    lbl.font = [UIFont systemFontOfSize:15];
    [baseView addSubview:lbl];
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(@(0));
        make.left.equalTo(@(15));
    }];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
    [baseView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-15));
        make.centerY.equalTo(baseView);
        make.width.equalTo(@6);
        make.height.equalTo(@12);
    }];
    
    baseView.myTop = 0;
    baseView.myLeft = 0;
    baseView.myRight = 0;
    baseView.myHeight = height;
    return baseView;
}

+ (UIView *)cellAttributedStringWithArrow:(NSMutableAttributedString *)title height:(CGFloat)height {
    UIView *baseView = [UIView new];
    baseView.backgroundColor = [UIColor whiteColor];
    
    UILabel *lbl = [UILabel new];
    lbl.attributedText = title;//lbl.text = title;
    lbl.font = [UIFont systemFontOfSize:15];
    [baseView addSubview:lbl];
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(@(0));
        make.left.equalTo(@(15));
    }];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
    [baseView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-15));
        make.centerY.equalTo(baseView);
        //make.width.height.equalTo(@(10));
        make.width.equalTo(@6);
        make.height.equalTo(@12);
    }];
    
    baseView.myTop = 0;
    baseView.myLeft = 0;
    baseView.myRight = 0;
    baseView.myHeight = height;
    return baseView;
}

+ (UIView *)cellBgName:(NSString *)imgName AttributedStringWithArrow:(NSMutableAttributedString *)title height:(CGFloat)height {
    UIView *baseView = [UIView new];
    baseView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *bg = [UIImageView new];
    bg.image = [UIImage imageNamed:imgName];
    [baseView addSubview:bg];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@5);
        make.bottom.equalTo(@(-5));
        make.left.equalTo(@15);
        make.right.equalTo(@(-15));
    }];
    
    UILabel *lbl = [UILabel new];
    lbl.attributedText = title;
    lbl.font = [UIFont systemFontOfSize:15];
    [baseView addSubview:lbl];
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(@(0));
        make.left.equalTo(@(39.5));
    }];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"goldArrow"]];
    [baseView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-45));
        make.centerY.equalTo(baseView);
        make.width.equalTo(@20);
        make.height.equalTo(@6);
    }];
    
    baseView.myTop = 0;
    baseView.myLeft = 0;
    baseView.myRight = 0;
    baseView.myHeight = height;
    return baseView;
}

+ (UIView *)cellWithIconTitleArrow:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset {
    UIView *baseView = [UIView new];
    [baseView addMyLinearLayout:MyOrientation_Horz].gravity = MyGravity_Vert_Center | MyGravity_Horz_Between;
    
    baseView.myTop = inset.top;
    baseView.myLeft = inset.left;
    baseView.myRight = inset.right;
    baseView.myHeight = inset.bottom;
    
    MyLinearLayout *righLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    righLayout.myWidth = MyLayoutSize.wrap;
    righLayout.myHeight = MyLayoutSize.wrap;
    
    UIImageView *icon = [[UIImageView alloc] init];
    icon.myLeft = 0;
    icon.mySize = imageSize;
    [righLayout addSubview:icon];
    if([image isKindOfClass:[UIImage class]]) {
        icon.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [icon sd_setImageWithURL:[NSURL URLWithString:image]];
    }
    
    UILabel *lbl = [UILabel new];
    lbl.attributedText = title;
    lbl.myLeft = 10;
    lbl.myRight = 0;
    lbl.myWidth = MyLayoutSize.wrap;
    lbl.myHeight = MyLayoutSize.wrap;
    [righLayout addSubview:lbl];
    
    [baseView addSubviewOnLinearLayout:righLayout];

    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
    arrow.myWidth = 6;
    arrow.myHeight = 11;
    [baseView addSubviewOnLinearLayout:arrow];

    return baseView;
}

+ (UIView *)cellWithIconTitlePointArrow:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset {
    UIView *baseView = [UIView new];
    [baseView addMyLinearLayout:MyOrientation_Horz].gravity = MyGravity_Vert_Center | MyGravity_Horz_Between;
    
    baseView.myTop = inset.top;
    baseView.myLeft = inset.left;
    baseView.myRight = inset.right;
    baseView.myHeight = inset.bottom;
    
    MyLinearLayout *righLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    righLayout.myWidth = MyLayoutSize.wrap;
    righLayout.myHeight = MyLayoutSize.wrap;
    
    UIImageView *icon = [[UIImageView alloc] init];
    icon.myLeft = 0;
    icon.mySize = imageSize;
    [righLayout addSubview:icon];
    if([image isKindOfClass:[UIImage class]]) {
        icon.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [icon sd_setImageWithURL:[NSURL URLWithString:image]];
    }
    
    UILabel *lbl = [UILabel new];
    lbl.attributedText = title;
    lbl.myLeft = 10;
    lbl.myRight = 0;
    lbl.myWidth = MyLayoutSize.wrap;
    lbl.myHeight = MyLayoutSize.wrap;
    [righLayout addSubview:lbl];
    
    UIView *readRedPoint = [UIView new];
    readRedPoint.tag = 20201026;
    readRedPoint.myWidth = 7;
    readRedPoint.myHeight = 7;
    readRedPoint.myTop = -2;
    readRedPoint.myLeading = 3;
    [readRedPoint setViewCornerRadius:3.5];
    readRedPoint.backgroundColor = UIColor.redColor;
    [righLayout addSubview:readRedPoint];
    
    [baseView addSubviewOnLinearLayout:righLayout];

    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
    arrow.myWidth = 6;
    arrow.myHeight = 11;
    [baseView addSubviewOnLinearLayout:arrow];

    return baseView;
}


+ (UIView *)cellWithRightIconArrow:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset {
    UIView *baseView = [UIView new];
    [baseView addMyLinearLayout:MyOrientation_Horz].gravity = MyGravity_Vert_Center | MyGravity_Horz_Between;
    
    baseView.myTop = inset.top;
    baseView.myLeft = inset.left;
    baseView.myRight = inset.right;
    baseView.myHeight = inset.bottom;
    
    UILabel *lbl = [UILabel new];
    lbl.attributedText = title;
    lbl.myLeft = 0;
    lbl.myCenterX = 0;
    lbl.myWidth = MyLayoutSize.wrap;
    lbl.myHeight = MyLayoutSize.wrap;
    [baseView addSubviewOnLinearLayout:lbl];
    
    MyLinearLayout *righLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    righLayout.gravity = MyGravity_Horz_Right | MyGravity_Vert_Center;
    righLayout.myWidth = MyLayoutSize.wrap;
    righLayout.myHeight = MyLayoutSize.wrap;
    
    UIImageView *icon = [[UIImageView alloc] init];
    icon.tag = 999999;
    icon.mySize = imageSize;
    [righLayout addSubview:icon];
    if([image isKindOfClass:[UIImage class]]) {
        icon.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [icon sd_setImageWithURL:[NSURL URLWithString:image]];
    }
    
    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
    arrow.myLeft = 10;
    arrow.myWidth = 6;
    arrow.myHeight = 11;
    [righLayout addSubview:arrow];
    
    [baseView addSubviewOnLinearLayout:righLayout];

    return baseView;
}

+ (UIView *)cellWithDetailArrow:(NSMutableAttributedString *)title detail:(NSMutableAttributedString *)detail inset:(UIEdgeInsets)inset {
    UIView *baseView = [UIView new];
    [baseView addMyLinearLayout:MyOrientation_Horz].gravity = MyGravity_Vert_Center | MyGravity_Horz_Between;
    
    baseView.myTop = inset.top;
    baseView.myLeft = inset.left;
    baseView.myRight = inset.right;
    baseView.myHeight = inset.bottom;
    
    MyLinearLayout *leftLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    leftLayout.myWidth = MyLayoutSize.wrap;
    leftLayout.myHeight = MyLayoutSize.wrap;
    
    MyLinearLayout *righLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    righLayout.gravity = MyGravity_Horz_Right | MyGravity_Vert_Center;
    righLayout.myWidth = MyLayoutSize.wrap;
    righLayout.myHeight = MyLayoutSize.wrap;
    
    [baseView addSubviewOnLinearLayout:leftLayout];
    [baseView addSubviewOnLinearLayout:righLayout];
    
    UILabel *lbl = [UILabel new];
    lbl.attributedText = title;
    lbl.myLeft = 0;
    lbl.myRight = 0;
    lbl.myWidth = MyLayoutSize.wrap;
    lbl.myHeight = MyLayoutSize.wrap;
    [leftLayout addSubview:lbl];
    
    UILabel *rbl = [UILabel new];
    rbl.attributedText = detail;
    rbl.tag = 20201023;
    rbl.myWidth = MyLayoutSize.wrap;
    rbl.myHeight = MyLayoutSize.wrap;
    [righLayout addSubview:rbl];

    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
    arrow.myLeft = 10;
    arrow.myWidth = 6;//10;
    arrow.myHeight = 11;//10;
    [righLayout addSubview:arrow];

    return baseView;
}

+ (UIView *)cellWithDetail:(NSMutableAttributedString *)title detail:(NSMutableAttributedString *)detail inset:(UIEdgeInsets)inset action:(FinishedBlock)action {
    UIView *baseView = [UIView new];
    [baseView addMyLinearLayout:MyOrientation_Horz].gravity = MyGravity_Vert_Center | MyGravity_Horz_Between;
    
    baseView.myTop = inset.top;
    baseView.myLeft = inset.left;
    baseView.myRight = inset.right;
    baseView.myHeight = inset.bottom;
    
    UILabel *lbl = [UILabel new];
    lbl.attributedText = title;
    lbl.myLeft = 0;
    lbl.myRight = 0;
    lbl.myWidth = MyLayoutSize.wrap;
    lbl.myHeight = MyLayoutSize.wrap;
    
    UILabel *rbl = [UILabel new];
    rbl.attributedText = detail;
    rbl.myWidth = MyLayoutSize.wrap;
    rbl.myHeight = MyLayoutSize.wrap;
    
    [baseView addSubviewOnLinearLayout:lbl];
    [baseView addSubviewOnLinearLayout:rbl];
    
    [baseView addAction:action];

    return baseView;
}

///添加N个格子的布局---只有一行
+ (MyBaseLayout *)aequilateHorGrid:(NSArray<UIView *> *)arr {
    
    MyGridLayout *gridLayout = [MyGridLayout new];
    gridLayout.myTop = 0;
    gridLayout.myHeight = MyLayoutSize.wrap;
    gridLayout.myLeft = 15;
    gridLayout.myRight = 15;
    
    [arr enumerateObjectsUsingBlock:^(UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [gridLayout addCol:1.0/(CGFloat)arr.count];
        [gridLayout addSubview:obj];
    }];
    return gridLayout;
}

///添加N个格子的布局---只有一行
+ (MyBaseLayout *)aequilateHorGrid:(NSArray<UIView *> *)arr inset:(UIEdgeInsets)inset {
    
    MyGridLayout *gridLayout = [MyGridLayout new];
    gridLayout.myTop = inset.top;
    gridLayout.myHeight = MyLayoutSize.wrap;
    gridLayout.myLeft = inset.left;
    gridLayout.myRight = inset.right;
    
    [arr enumerateObjectsUsingBlock:^(UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [gridLayout addCol:MyLayoutSize.fill]; //使用1.0/(CGFloat)arr.count，当数组个数等于1的时候会有出现布局问题
        [gridLayout addSubview:obj];
    }];
    return gridLayout;
}

///添加N个格子的布局---多行
+ (MyBaseLayout *)aequilateMultiLineGrid:(NSArray<UIView *> *)arr rowHeight:(CGFloat)rowHeight colNum:(NSInteger)colNum action:(FinishedBlock)action {
    return [self aequilateMultiLineGrid:arr rowHeight:rowHeight colNum:colNum inset:UIEdgeInsetsMake(0, 15, 0, 15) action:action];
}

+ (MyBaseLayout *)aequilateMultiLineGrid:(NSArray<UIView *> *)arr rowHeight:(CGFloat)rowHeight colNum:(NSInteger)colNum inset:(UIEdgeInsets)inset action:(FinishedBlock)action {
    
    MyGridLayout *gridLayout = [MyGridLayout new];
    gridLayout.myTop = inset.top;
    gridLayout.myHeight = MyLayoutSize.wrap;
    gridLayout.myLeft = inset.left;
    gridLayout.myRight = inset.right;
    
    id<MyGrid> grid = nil;
    for(NSInteger i = 0; i < arr.count;i++) {
        if(i%colNum == 0) {
            grid = [gridLayout addRow:rowHeight];
        }
        [grid addCol:1.0/(CGFloat)colNum]; //使用MyLayoutSize.fill可以均分
    }
    
    //NSInteger totalRowNum = ceil(((CGFloat)arr.count/colNum));
    
    [arr enumerateObjectsUsingBlock:^(UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(action) {
            if(obj.custom == nil) {
                obj.custom = @(idx);
            }
            [obj addAction:action];
        }
        [gridLayout addSubview:obj];
    }];
    return gridLayout;
}

+ (MyBaseLayout *)aequilateLabelMultiLineGrid:(NSArray<NSArray<NSMutableAttributedString *> *> *)arr rowHeight:(CGFloat)rowHeight colNum:(NSInteger)colNum inset:(UIEdgeInsets)inset action:(FinishedBlock)action {
    
    NSMutableArray *muArr = [NSMutableArray arrayWithCapacity:1];
    [arr enumerateObjectsUsingBlock:^(NSArray<NSMutableAttributedString *> * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIView *item = [UIView new];
        [item addMyLinearLayout:MyOrientation_Vert].gravity = MyGravity_Vert_Center;
        UILabel *topLbl = [item addOneHorLabel:[obj firstObject] inset:UIEdgeInsetsZero];
        UILabel *bottomLbl = [item addOneHorLabel:[obj lastObject] inset:UIEdgeInsetsZero];
        topLbl.numberOfLines = 1;
        bottomLbl.numberOfLines = 1;
        topLbl.adjustsFontSizeToFitWidth = YES;
        bottomLbl.adjustsFontSizeToFitWidth = YES;
        bottomLbl.myTop = 5;
        [muArr addObject:item];
    }];
    return [UIView aequilateMultiLineGrid:muArr rowHeight:rowHeight colNum:colNum inset:inset action:action];
}

///添加四个格子的布局
+ (MyBaseLayout *)fourGrid:(NSArray<UIView *> *)arr itemSize:(CGSize)size {
    
    MyFrameLayout *frameLayout = [MyFrameLayout new];
    frameLayout.myTop = 0;
    frameLayout.myBottom = 0;
    frameLayout.myLeft = 15;
    frameLayout.myRight = 15;
    
    NSMutableArray<NSValue *> *sizeArr = [NSMutableArray arrayWithCapacity:1];
    [arr enumerateObjectsUsingBlock:^(UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [sizeArr addObject:[NSValue valueWithCGSize:size]];
    }];
    
    return [self fourGrid:arr itemSizes:sizeArr];
}

///添加四个格子的布局,需要传入每个格子的size
+ (MyBaseLayout *)fourGrid:(NSArray<UIView *> *)arr itemSizes:(NSArray<NSValue *> *)sizes {
    
    MyFrameLayout *frameLayout = [MyFrameLayout new];
    frameLayout.myTop = 0;
    frameLayout.myBottom = 0;
    frameLayout.myLeft = 15;
    frameLayout.myRight = 15;
    
    [arr enumerateObjectsUsingBlock:^(UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(idx == 0) {
            obj.myTop = 14;
        } else if(idx == 1) {
            obj.myRight = 0;
            obj.myTop = 14;
        } else if(idx == 2) {
            obj.myBottom = 14;
        } else if(idx == 3) {
            obj.myRight = 0;
            obj.myBottom = 14;
        }
        obj.mySize = [[sizes objectAtIndex:idx] CGSizeValue];
        [frameLayout addSubview:obj];
    }];
    return frameLayout;
}

- (UIScrollView *)addScrollView {
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    [self addSubview:scrollView];
    FullInSuperView(scrollView);
    return scrollView;
}

- (UIImageView *)addBackgroundImage:(id)image {
    UIImageView *imgView = [[UIImageView alloc] init];
    //imgView.contentMode = UIViewContentModeScaleToFill;
    imgView.tag = 8102020;
    [self addSubview:imgView];
    FullInSuperView(imgView);
    if([image isKindOfClass:[UIImage class]]) {
        imgView.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:image]];
    }
    return imgView;
}

- (UIImageView *)addCenterImage:(id)image size:(CGSize)size {
    UIImageView *imgView = [[UIImageView alloc] init];
    //imgView.contentMode = UIViewContentModeScaleToFill;
    imgView.tag = 8102020;
    [self addSubview:imgView];
    if([image isKindOfClass:[UIImage class]]) {
        imgView.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:image]];
    }
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
        make.width.equalTo(@(size.width));
        make.height.equalTo(@(size.height));
    }];
    return imgView;
}

- (UIImageView *)addAutoImage:(id)image width:(CGFloat)width {
    return [self addImage:image imageSize:CGSizeMake(width, 0) inset:UIEdgeInsetsZero];
}

- (UIImageView *)addImage:(id)image imageSize:(CGSize)imageSize inset:(UIEdgeInsets)inset {
    UIImageView *imgView = [[UIImageView alloc] init];
    //imgView.backgroundColor = [UIColor moBackground];
    imgView.myLeft = inset.left;
    imgView.myTop = inset.top;
    
    if(imageSize.width <= 0) {
        imgView.myWidth = MyLayoutSize.wrap;
    } else {
        imgView.myWidth = imageSize.width;
    }
    
    if(imageSize.height <= 0) {
        imgView.myHeight = MyLayoutSize.wrap;
    } else {
        imgView.myHeight = imageSize.height;
    }
    
    [self addSubviewOnLinearLayout:imgView];
    
    if([image isKindOfClass:[UIImage class]]) {
        imgView.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:image] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            //[[imgView superview] layoutIfNeeded];
        }];
    }
    return imgView;
}


- (UITextField *)addTextField:(id)placeHolder inset:(UIEdgeInsets)inset {
    UITextField *textFiled = [[UITextField alloc] init];
    if([placeHolder isKindOfClass:[NSMutableAttributedString class]]) {
        textFiled.attributedPlaceholder = placeHolder;
    } else if([placeHolder isKindOfClass:[NSString class]]) {
        textFiled.placeholder = placeHolder;
    }
    textFiled.myTop = inset.top;
    textFiled.myLeft = inset.left;
    textFiled.myRight = inset.right;
    textFiled.myHeight = inset.bottom;
    [textFiled setPlaceholderColor:[UIColor colorWithHexString:@"#C5C5C5"]];
    [self addSubviewOnLinearLayout:textFiled];
    
    return textFiled;
}

- (UITextField *)addTextField:(UIFont *)font placeHolder:(id)placeHolder inset:(UIEdgeInsets)inset {
    UITextField *textFiled = [[UITextField alloc] init];
    if([placeHolder isKindOfClass:[NSMutableAttributedString class]]) {
        textFiled.attributedPlaceholder = placeHolder;
    } else if([placeHolder isKindOfClass:[NSString class]]) {
        textFiled.placeholder = placeHolder;
    }
    textFiled.font = font;
    textFiled.myTop = inset.top;
    textFiled.myLeft = inset.left;
    textFiled.myRight = inset.right;
    textFiled.myHeight = inset.bottom;
    [self addSubviewOnLinearLayout:textFiled];
    
    return textFiled;
}

///获取验证码
- (TextCaptchaView *)addCaptchaView:(FinishedBlock)action sendBlock:(FinishedBlock)sendBlock {
//    TextCaptchaView *captcha = [TextCaptchaView new];
//    [captcha setLeftPadding:15];
//    captcha.layer.borderColor = [UIColor colorWithHexString:@"#607288"].CGColor;
//    captcha.layer.borderWidth = .5;
//    captcha.layer.cornerRadius = 10;
//    captcha.tf.font = [UIFont font12];
//    captcha.tf.textColor = UIColor.whiteColor;
//    captcha.myTop = 20/2;
//    captcha.myLeft = captcha.myRight = 20;
//    captcha.myHeight = 40;
//    captcha.tf.returnKeyType = UIReturnKeyDone;
//    [captcha reloadPlaceholder:Lang(@"请输入验证码")];
//    captcha.getText = ^(id data) {
//        !action?:action(data);
//    };
//    captcha.sendCodeBlock = ^(id data) {
//        !sendBlock ?: sendBlock(data);
//    };
//    [self addSubviewOnLinearLayout:captcha];
//    return captcha;
    return nil;
}

- (PlaceholderTextView *)addTextView:(UIFont *)font placeHolder:(id)placeHolder inset:(UIEdgeInsets)inset {
    PlaceholderTextView *textView = [[PlaceholderTextView alloc] init];
    if([placeHolder isKindOfClass:[NSMutableAttributedString class]]) {
        textView.placeholder = ((NSMutableAttributedString *)placeHolder).string;
    } else if([placeHolder isKindOfClass:[NSString class]]) {
        textView.placeholder = placeHolder;
    }
    textView.textColor = [UIColor blackColor];
    textView.placeholderColor = [UIColor moPlaceHolder];
    textView.font = font;
    textView.myTop = inset.top;
    textView.myLeft = inset.left;
    textView.myRight = inset.right;
    textView.myHeight = inset.bottom;
    [self addSubviewOnLinearLayout:textView];
    
    return textView;
}

+ (UIButton *)bottomBnt:(NSString *)title inset:(UIEdgeInsets)inset action:(FinishedBlock)block {
    UIButton *bnt = [[UIButton alloc] init];
    bnt.backgroundColor = [UIColor colorWithHexString:@"#F70500"];
    [bnt setViewCornerRadius:5];
    [bnt setTitle:title forState:UIControlStateNormal];
    [bnt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    bnt.myTop = inset.top;
    bnt.myLeft = inset.left;
    bnt.myRight = inset.right;
    bnt.myHeight = inset.bottom > 0 ? inset.bottom : 44;
    [bnt addAction:block];
    return bnt;
}

+ (UIView *)inputView:( NSString * _Nullable)imgName title:( NSString * _Nullable)title placeHolder:( NSString * _Nullable)placeHolder leftWeight:(CGFloat)leftWeight {
    UIView *baseView = [UIView new];
    baseView.backgroundColor = [UIColor whiteColor];
    baseView.myTop = 0;
    baseView.myLeft = 0;
    baseView.myRight = 0;
    baseView.myHeight = 240;
    [baseView addMyLinearLayout:MyOrientation_Horz];
    
    UIButton *bnt = [UIButton new];
    bnt.titleLabel.font = [UIFont systemFontOfSize:15];
    [bnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    if(imgName) {
        [bnt setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    }
    
    if(title) {
        [bnt setTitle:title forState:UIControlStateNormal];
    }
    
    bnt.myTop = 0;
    bnt.myLeft = 0;
    bnt.weight = leftWeight;
    bnt.myBottom = 0;
    [baseView addSubviewOnLinearLayout:bnt];
    
    UITextField *textFiled = [[UITextField alloc] init];
    textFiled.font = [UIFont systemFontOfSize:15];
    textFiled.placeholder = placeHolder;
    textFiled.myTop = 0;
    textFiled.myLeft = 0;
    textFiled.weight = 1-leftWeight;
    textFiled.myBottom = 0;
    [baseView addSubviewOnLinearLayout:textFiled];
    
    baseView.custom = textFiled;
    
    return baseView;
}

- (void)addCornerImage:(NSArray *)imageArr inset:(CGFloat)inset {
    [imageArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIImage *image = [UIImage imageNamed:obj];
        UIImageView *cornerImgView = [UIImageView new];
        cornerImgView.image = image;
        [self addSubview:cornerImgView];
        [cornerImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(image.size.width));
            make.height.equalTo(@(image.size.height));
            if(idx == 0) { //top left
                make.top.equalTo(@(-inset));
                make.left.equalTo(@(-inset));
            } else if(idx == 1) { //top right
                make.top.equalTo(@(-inset));
                make.right.equalTo(@(inset));
            } else if(idx == 2) { //bottom left
                make.bottom.equalTo(@(inset));
                make.left.equalTo(@(-inset));
            } else { //bottom right
                make.bottom.equalTo(@(inset));
                make.right.equalTo(@(inset));
            }
        }];
    }];
}

- (void)addBorder:(UIColor *)color {
    self.layer.borderWidth = 1;
    self.layer.borderColor = color.CGColor;
}

+ (UIView *)functionView:(NSArray<NSDictionary *> *)contentArr imageSize:(CGSize)imageSize titleColor:(UIColor *)titleColor titleFont:(UIFont *)titleFont rowHeight:(CGFloat)rowHeight colNum:(NSInteger)colNum acition:(FinishedBlock)tapBlock {

    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:1];
    [contentArr enumerateObjectsUsingBlock:^(NSDictionary* _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *imageName = [obj valueForKey:@"imageName"];
        NSString *title = [obj valueForKey:@"title"];
        NSString *openUrl = [obj valueForKey:@"openUrl"];
        
        UIView *item = [UIView new];
        item.openUrl = openUrl;
        [item addMyLinearLayout:MyOrientation_Vert].gravity = MyGravity_Vert_Center;
        
        UIImageView *imgView = [UIImageView new];
        imgView.image = [UIImage imageNamed:imageName];
        imgView.myCenterX = 0;
        imgView.mySize = imageSize;
        
        UILabel *label = [UILabel new];
        label.text = title;
        label.textColor = titleColor;
        label.font = titleFont;
        label.textAlignment = NSTextAlignmentCenter;
        label.myTop = 5;
        label.myLeft = 0;
        label.myRight = 0;
        label.myHeight = MyLayoutSize.wrap;
        
        [item addSubviewOnLinearLayout:imgView];
        [item addSubviewOnLinearLayout:label];
        [arr addObject:item];
    }];
    
    return [UIView aequilateMultiLineGrid:arr rowHeight:rowHeight colNum:colNum inset:UIEdgeInsetsZero action:tapBlock];
}

+ (UIView *)personalInfo:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title subTitle:(NSMutableAttributedString *)subTitle action:(FinishedBlock)block {
    UIView *baseView = [UIView new];
    [baseView addMyLinearLayout:MyOrientation_Horz];
    
    UIImageView *imgView = [UIImageView new];
    imgView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    imgView.myCenterY = 0;
    imgView.myLeft = 0;
    imgView.mySize = imageSize;
    imgView.tag = 6699;
    //[imgView setViewCornerRadius:imageSize.width/2.0];
    if([image isKindOfClass:[UIImage class]]) {
        imgView.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:[UIImage imageNamed:@"minePortrait"]];
    }
    
    UIView *rightView = [UIView new];
    rightView.myLeft = 15;
    rightView.myTop = 0;
    rightView.myBottom = 0;
    rightView.weight = 1;
    rightView.tag = 300001;
    [rightView addMyLinearLayout:MyOrientation_Vert].gravity = MyGravity_Vert_Around;
    if(title) {
        [rightView addOneHorLabel:title inset:UIEdgeInsetsZero].numberOfLines = 1;
    }
    
    if(subTitle) {
        [rightView addOneHorLabel:subTitle inset:UIEdgeInsetsZero];
    }
    
    [baseView addSubviewOnLinearLayout:imgView];
    [baseView addSubviewOnLinearLayout:rightView];
    
    [baseView addAction:block];
    
    return baseView;
}

+ (UIView *)productInfo:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title subTitle:(NSMutableAttributedString *)subTitle action:(FinishedBlock)block {
    UIView *baseView = [UIView new];
    [baseView addMyLinearLayout:MyOrientation_Horz];
    
    UIImageView *imgView = [UIImageView new];
    imgView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    imgView.myCenterY = 0;
    imgView.myLeft = 0;
    imgView.mySize = imageSize;
    imgView.tag = 6699;
    //[imgView setViewCornerRadius:imageSize.width/2.0];
    if([image isKindOfClass:[UIImage class]]) {
        imgView.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:nil];
    }
    
    MyLinearLayout *rightView = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    rightView.tag = 100001;
    rightView.myLeft = 15;
    rightView.myCenterY = 0;
    rightView.weight = 1;
    rightView.myHeight = MyLayoutSize.wrap;
    [rightView addOneHorLabel:title inset:UIEdgeInsetsZero].numberOfLines = 1;
    
    UIButton *typeBnt = [UIButton new];
    [typeBnt setAttributedTitle:subTitle forState:UIControlStateNormal];
    [typeBnt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    typeBnt.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
    typeBnt.layer.cornerRadius = 3;
    typeBnt.tag = 100002;
    typeBnt.myTop = 10;
    typeBnt.myLeft = 0;
    typeBnt.myWidth = MyLayoutSize.wrap;
    typeBnt.myHeight = 16;
    [rightView addSubview:typeBnt];
    
    [baseView addSubviewOnLinearLayout:imgView];
    [baseView addSubviewOnLinearLayout:rightView];
    
    [baseView addAction:block];
    
    return baseView;
}

+ (UIView *)topImageBottomLabel:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset action:(FinishedBlock)block {
    UIView *item = [UIView new];
    [item addMyLinearLayout:MyOrientation_Vert];
    [item addAction:block];
    
    UIImageView *imgView = [UIImageView new];
    //imgView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    if([image isKindOfClass:[UIImage class]]) {
        imgView.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:nil];
    }
    imgView.myTop = 0;
    imgView.myCenterX = 0;
    imgView.mySize = imageSize;
    
    UILabel *label = [UILabel new];
    label.attributedText = title;
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 200002;
    label.myTop = 10;
    label.myLeft = 0;
    label.myRight = 0;
    label.myHeight = MyLayoutSize.wrap;
    
    [item addSubviewOnLinearLayout:imgView];
    [item addSubviewOnLinearLayout:label];
    
    item.myTop = inset.top;
    item.myLeft = inset.left;
    item.myRight = inset.right;
    item.myHeight = inset.bottom;
    return item;
}

+ (UIView *)topImageTwoBottomLabel:(id)image imageSize:(CGSize)imageSize title:(NSMutableAttributedString *)title bottomTitle:(NSMutableAttributedString *)bottomTitle inset:(UIEdgeInsets)inset action:(FinishedBlock)block {
    UIView *item = [UIView new];
    [item addMyLinearLayout:MyOrientation_Vert];
    [item addAction:block];
    
    UIImageView *imgView = [UIImageView new];
    imgView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    if([image isKindOfClass:[UIImage class]]) {
        imgView.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:nil];
    }
    imgView.myTop = 0;
    imgView.myCenterX = 0;
    imgView.mySize = imageSize;
    
    UILabel *label = [UILabel new];
    label.attributedText = title;
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 200002;
    label.myTop = 5;
    label.myLeft = 0;
    label.myRight = 0;
    label.myHeight = MyLayoutSize.wrap;
    
    UILabel *bottomlabel = [UILabel new];
    bottomlabel.attributedText = bottomTitle;
    bottomlabel.textAlignment = NSTextAlignmentCenter;
    bottomlabel.tag = 200003;
    bottomlabel.myTop = 5;
    bottomlabel.myLeft = 0;
    bottomlabel.myRight = 0;
    bottomlabel.myHeight = MyLayoutSize.wrap;
    
    [item addSubviewOnLinearLayout:imgView];
    [item addSubviewOnLinearLayout:label];
    [item addSubviewOnLinearLayout:bottomlabel];
    
    item.myTop = inset.top;
    item.myLeft = inset.left;
    item.myWidth = imageSize.width;
    item.myHeight = inset.bottom;
    return item;
}

+ (UIView *)topBottomLabel:(NSMutableAttributedString *)title bottomTitle:(NSMutableAttributedString *)bottomTitle inset:(UIEdgeInsets)inset action:(FinishedBlock)block {
    MyLinearLayout *linearLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Vert];
    linearLayout.tag = 100001;
    [linearLayout addAction:block];
    
    UILabel *label = [UILabel new];
    label.attributedText = title;
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 200002;
    label.myTop = 0;
    label.myLeft = 0;
    label.myWidth = MyLayoutSize.wrap;
    label.myHeight = MyLayoutSize.wrap;
    
    UILabel *bottomlabel = [UILabel new];
    bottomlabel.attributedText = bottomTitle;
    bottomlabel.textAlignment = NSTextAlignmentCenter;
    bottomlabel.tag = 200003;
    bottomlabel.myTop = 5;
    bottomlabel.myLeft = 0;
    bottomlabel.myWidth = MyLayoutSize.wrap;
    bottomlabel.myHeight = MyLayoutSize.wrap;
    
    [linearLayout addSubviewOnLinearLayout:label];
    [linearLayout addSubviewOnLinearLayout:bottomlabel];
    
    linearLayout.myTop = inset.top;
    linearLayout.myLeft = inset.left;
    linearLayout.myWidth = MyLayoutSize.wrap;
    linearLayout.myHeight = inset.bottom;
    return linearLayout;
}

+ (MyLinearLayout *)headTitle:(id)image imageSize:(CGSize)imageSize isCircle:(BOOL)isCircle needTitleArrow:(BOOL)needTitleArrow title:(NSMutableAttributedString *)title inset:(UIEdgeInsets)inset action:(FinishedBlock)action {

    MyLinearLayout *linearLayout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    linearLayout.myTop = inset.top;
    linearLayout.myLeft = inset.left;
    linearLayout.myRight = inset.right;
    linearLayout.myHeight = inset.bottom;
    linearLayout.gravity = MyGravity_Vert_Center;
    linearLayout.tag = 100001;

    UIImageView *imgView = [UIImageView new];
    imgView.tag = 888888;
    imgView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    if([image isKindOfClass:[UIImage class]]) {
        imgView.image = image;
    } else if([image isKindOfClass:[NSString class]]) {
        [imgView sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:nil];
    }
    if(isCircle) {
        imgView.clipsToBounds = YES;
        imgView.layer.cornerRadius = imageSize.width/2.0;
    }
    imgView.mySize = imageSize;
    [linearLayout addSubview:imgView];
    
    UILabel *titleLbl = [[UILabel alloc] init];
    titleLbl.myLeft = 10;
    titleLbl.myWidth = MyLayoutSize.wrap;
    titleLbl.myHeight = MyLayoutSize.wrap;
    titleLbl.attributedText = title;
    [linearLayout addSubview:titleLbl];
    
    if(needTitleArrow) {
        UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
        arrow.myLeft = 10;
        arrow.myWidth = 10;
        arrow.myHeight = 10;
        [linearLayout addSubview:arrow];
    }
    
    [linearLayout addAction:^(UIView *view) {
        Block_Exec(action,nil);
    }];
    
    return linearLayout;
}

- (void)addMyLayoutProperty:(CGFloat)top left:(CGFloat)left bottom:(CGFloat)bottom right:(CGFloat)right {
    self.myTop = top;
    self.myLeft = left;
    self.myRight = right;
    self.myHeight = bottom;
}

///垂直占位view
+ (UIView *)placeholderVertView {
    UIView *view = [UIView new];
    view.weight = 1;
    view.myWidth = 1;
    view.myCenterX = 0;
    return view;
}
///水平占位view
+ (UIView *)placeholderHorzView {
    UIView *view = [UIView new];
    view.weight = 1;
    view.myHeight = 1;
    view.myCenterY = 0;
    return view;
}

+ (MyBaseLayout *)stdCellView:(NSString *)left right:(NSString *)right arrow:(NSString *)arrow rowHeight:(CGFloat)height {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.backgroundColor = [UIColor whiteColor];
    layout.height = height;
    layout.myHeight = layout.height;
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    
    UILabel *lbl = [UILabel new];
    lbl.font = [UIFont boldFont15];
    lbl.textColor = [UIColor colorWithHexString:@"#1D1D1F"];
    lbl.myCenterY = 0;
    lbl.myLeft = 15;
    lbl.text = left;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    [layout addSubview:lbl];
    
    [layout addSubview:[UIView placeholderHorzView]];
    lbl = [UILabel new];
    lbl.tag = 100;
    lbl.font = [UIFont font15];
    lbl.textColor = [UIColor colorWithHexString:@"#737380"];
    lbl.myCenterY = 0;
    lbl.text = right;
    [lbl sizeToFit];
    lbl.mySize = lbl.size;
    lbl.myRight = 15;
    [layout addSubview:lbl];
    
    if (![StringUtil isEmpty:arrow]) {
        UIImageView *imgView = [UIImageView new];
        imgView.image = [UIImage imageNamed:arrow];
        imgView.myRight = 15;
        [imgView sizeToFit];
        imgView.mySize = imgView.size;
        imgView.myCenterY = 0;
        [layout addSubview:imgView];
    }
    return layout;
}

+ (UIView *)addBottomSafeAreaWithContentView:(UIView *)contentView {
    MyFrameLayout *layout = [MyFrameLayout new];
    layout.backgroundColor = contentView.backgroundColor;
    layout.height = contentView.height + safeAreaInsetBottom();
    contentView.myTop = 0;
    contentView.myLeft = 0;
    contentView.myRight = 0;
    contentView.myHeight = contentView.height;
    [layout addSubview:contentView];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = layout.height;
    return layout;
}

+ (MyBaseLayout *)factoryLeft:(NSString *)left rightText:(NSString *)right {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    
    UILabel *leftLbl = [UILabel new];
    leftLbl.myTop = 0;
    leftLbl.myLeft = 15;
    leftLbl.text = left;
    leftLbl.font = [UIFont font15];
    leftLbl.textColor = [UIColor colorWithHexString:@"#666666"];
    [leftLbl sizeToFit];
//    leftLbl.size = CGSizeMake(100, 20);
    leftLbl.mySize = leftLbl.size;
    [layout addSubview:leftLbl];
    
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.myTop = 0;
    lbl.myLeft = 15;
    lbl.myRight = 20;
    lbl.myBottom = 20;
    lbl.weight = 1;
    NSArray *txtArr = @[right];
    NSArray *colorArr = @[[UIColor moBlack]];
    NSArray *fontArr = @[[UIFont font15]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                        colors:colorArr
                                                                         fonts:fontArr];
    NSRange range = [att.string rangeOfString:att.string];
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineSpacing = 5.0;
    [att addAttributes:@{NSParagraphStyleAttributeName:paragraph} range:range];
    lbl.attributedText = att;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - leftLbl.width - 55, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    [layout addSubview:lbl];
        
    return layout;
}

+ (MyBaseLayout *)factoryLeft:(NSString *)left rightText:(NSString *)right topEdge:(CGFloat)top bottomEdge:(CGFloat)bottom {
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myHeight = MyLayoutSize.wrap;
    
    UILabel *leftLbl = [UILabel new];
    leftLbl.myTop = top;
    leftLbl.myLeft = 20;
    leftLbl.text = left;
    leftLbl.font = [UIFont font15];
    leftLbl.textColor = [UIColor colorWithHexString:@"#666666"];
    [leftLbl sizeToFit];
    leftLbl.mySize = leftLbl.size;
    [layout addSubview:leftLbl];
    
    UILabel *lbl = [UILabel new];
    lbl.numberOfLines = 0;
    lbl.myTop = top;
    lbl.myLeft = 15;
    lbl.myRight = 20;
    lbl.myBottom = bottom;
    lbl.weight = 1;
    NSArray *txtArr = @[right];
    NSArray *colorArr = @[[UIColor moBlack]];
    NSArray *fontArr = @[[UIFont font15]];
    NSMutableAttributedString *att = [NSMutableAttributedString initWithTitles:txtArr
                                                                        colors:colorArr
                                                                         fonts:fontArr];
    NSRange range = [att.string rangeOfString:att.string];
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineSpacing = 5.0;
    [att addAttributes:@{NSParagraphStyleAttributeName:paragraph} range:range];
    lbl.attributedText = att;
    CGSize size = [lbl sizeThatFits:CGSizeMake(SCREEN_WIDTH - leftLbl.width - 55, MAXFLOAT)];
    lbl.size = size;
    lbl.mySize = lbl.size;
    [layout addSubview:lbl];
        
    return layout;
}

@end



