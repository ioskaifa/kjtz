//
//  MXScrollView.m
//  AdvertisingMaster
//
//  Created by mac on 2020/6/4.
//  Copyright © 2020 mac. All rights reserved.
//

#import "MXScrollView.h"

@implementation ContentView

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    BOOL inSide = [super pointInside:point withEvent:event];
    if (!inSide) {
        UIView *superView = self.superview;
        if (superView) {
            point = [self convertPoint:point toView:superView];
            inSide = CGRectContainsPoint(superView.bounds, point);
        }
    }
    return inSide;
}

@end

@implementation MXScrollView

- (instancetype)init {
    if (self = [super init]) {
        [self createUI];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self createUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    [self addSubview:self.contentView];
}

- (ContentView *)contentView {
    if (!_contentView) {
        _contentView = [[ContentView alloc] initWithFrame:CGRectZero];
        _contentView.tag = 1;
    }
    return _contentView;
}

@end
