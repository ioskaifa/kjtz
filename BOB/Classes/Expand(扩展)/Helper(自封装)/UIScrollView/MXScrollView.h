//
//  MXScrollView.h
//  AdvertisingMaster
//
//  Created by mac on 2020/6/4.
//  Copyright © 2020 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContentView : UIView

@end

@interface MXScrollView : UIScrollView

@property (nonatomic, strong) ContentView *contentView;

@end

NS_ASSUME_NONNULL_END
