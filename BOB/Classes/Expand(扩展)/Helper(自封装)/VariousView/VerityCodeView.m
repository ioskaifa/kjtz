//
//  VerityCodeView.m
//  AdvertisingMaster
//
//  Created by XXX on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "VerityCodeView.h"
#import "LoginRegModel.h"
static NSInteger  interval =  60 ;// 发送短信间隔时间:秒
static NSInteger iconWidth = 23;
@interface VerityCodeView()<UITextFieldDelegate,MXCountdownViewDelegate>
{
    
}

@property (nonatomic,strong) LoginRegModel   *model;

@end

@implementation VerityCodeView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        
        [self initUI];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFiledEditChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:self.tf];
    }
    
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initUI{
    [self addSubview:self.img];
    [self addSubview:self.tf];
    [self addSubview:self.countDowView];
    self.line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self addSubview:self.line];
    [self layout];
}

-(void)layout{
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(-0);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(SINGLE_LINE_HEIGHT));
    }];
    
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    
    [self.tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.img.mas_centerY);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.countDowView.mas_left).offset(-5);;
    }];
    
    [self.countDowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.img.mas_centerY);
        make.size.mas_equalTo(self.countDowView.size);
        make.right.equalTo(self.line.mas_right).offset(-10);
    }];
}

- (void)hideIcon {
    [self.img mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(0));
        make.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.line.mas_left).offset(-2*5);
    }];
}

-(UIImageView*)img{
    if (!_img) {
        _img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_login_ver"]];
    }
    return _img;
}

-(UITextField* )tf{
    if (!_tf) {
        _tf = [[UITextField alloc] init];
        _tf.delegate = self;
        _tf.placeholder = @"请输入验证码";
        [_tf setPlaceholderColor:[UIColor moPlaceHolder]];
        //[_tf setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        _tf.textColor = [UIColor blackColor];
        _tf.keyboardType = UIKeyboardTypeDefault;
        _tf.returnKeyType = UIReturnKeyNext;
        [_tf setFont:[UIFont font14]];
         _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _tf;
}

- (void)textFiledEditChanged:(NSNotification *)obj {
    UITextField *textField = (UITextField *)obj.object;
    if (textField == self.tf) {
        if (self.getText) {
            self.getText(textField.text);
        }
    }
}

-(MXCountdownView *)countDowView{
    if (!_countDowView) {
        _countDowView = [[MXCountdownView alloc] initWithFrame:CGRectZero timeInterval:interval];
        _countDowView.size = CGSizeMake(80, 30);
        [_countDowView setViewCornerRadius:3];
        _countDowView.delegate = self;
        _countDowView.normalTitleColor = [UIColor whiteColor];
        _countDowView.buttonNormalBackColor = [UIColor moGreen];
    }
    return _countDowView;
}

-(void)configModel:(LoginRegModel*)model{
    self.model = model;
}

- (void)startToRequest {
    if (self.sendCode) {
        BOOL result = self.sendCode(nil);
        if (result) {
            [self.countDowView start];
        }
    }
}

-(void)setHiddenDownLine:(BOOL)isHidden{
    self.line.hidden = isHidden;
}

- (void)setNextTextField:(UITextField *)nextTextField {
    if(nextTextField == nil) {
        self.tf.returnKeyType = UIReturnKeyDone;
        return;
    }
    _nextTextField = nextTextField;
    self.tf.returnKeyType = UIReturnKeyNext;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(self.nextTextField != nil) {
        [self.nextTextField becomeFirstResponder];
    }
    return YES;
}
@end
