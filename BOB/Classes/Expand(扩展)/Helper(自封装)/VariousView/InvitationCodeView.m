//
//  ImageVerifyView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "InvitationCodeView.h"
static NSInteger iconWidth = 22;
@interface InvitationCodeView()<UITextFieldDelegate>

@property (nonatomic, strong) UILabel         *lbl;

@property (nonatomic, strong) UIImageView     *img;

@property (nonatomic, strong) MXSeparatorLine *line;

@end

@implementation InvitationCodeView


- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        self.tag = 998877;
        [self initUI];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFiledEditChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:self.tf];
    }
    
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initUI{
    self.backgroundColor = [UIColor moBackground];
    [self addSubview:self.img];
    [self addSubview:self.lbl];
    [self addSubview:self.tf];
    self.line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self addSubview:self.line];
    [self layout];
}

-(void)layout{
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(-0);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(0.5));
    }];
    
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    
    [self.lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(iconWidth));
        make.width.equalTo(@(40));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    
    [self.tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.lbl.mas_right).offset(2*5);
        make.left.equalTo(self.img.mas_right).offset(2*5).priority(300);
        make.right.equalTo(self.line.mas_right);
    }];
}

-(UIImageView*)img{
    if (!_img) {
        _img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"邀请"]];
    }
    return _img;
}

-(UILabel*)lbl{
    if (!_lbl) {
        _lbl = [UILabel new];
        [_lbl setFont:[UIFont font16]];
        _lbl.textColor = [UIColor moBlack];
    }
    return _lbl;
}

-(UITextField* )tf{
    if (!_tf) {
        _tf = [[UITextField alloc] init];
        _tf.delegate = self;
        _tf.textColor = [UIColor moBlack];
        _tf.keyboardType = UIKeyboardTypeDefault;
        _tf.returnKeyType = UIReturnKeyNext;
        [_tf setFont:[UIFont font16]];
        _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _tf;
}

- (void)textFiledEditChanged:(NSNotification *)obj {
    UITextField *textField = (UITextField *)obj.object;
    if (textField == self.tf) {
        if (self.getText) {
            self.getText(textField.text);
        }
    }
}

-(void)reloadTitle:(NSString *) title placeHolder:(NSString *)placeHolder{
    self.lbl.text = title;
    NSAttributedString *placeHolderAtt = [NSMutableAttributedString initWithTitles:@[placeHolder]
                                                                            colors:@[[UIColor moPlaceHolder]]
                                                                             fonts:@[[UIFont font16]]];
    self.tf.attributedPlaceholder = placeHolderAtt;
    [self.img removeFromSuperview];
    [self layoutIfNeeded];
}

-(void)reloadImg:(NSString *) image  placeHolder:(NSString *)placeHolder{
    self.img.image = [UIImage imageNamed:image];
    NSAttributedString *placeHolderAtt = [NSMutableAttributedString initWithTitles:@[placeHolder]
                                                                            colors:@[[UIColor moPlaceHolder]]
                                                                             fonts:@[[UIFont font16]]];
    self.tf.attributedPlaceholder = placeHolderAtt;
    [self.lbl removeFromSuperview];
    [self layoutIfNeeded];
}

-(void)isHiddenLine:(BOOL)isHidden{
    self.line.hidden = isHidden;
}

-(void)setTitleWidth:(NSInteger)width{
    [self.lbl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(width));
        make.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    [self layoutIfNeeded];
}
@end
