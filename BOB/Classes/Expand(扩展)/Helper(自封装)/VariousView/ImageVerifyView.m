//
//  ImageVerifyView.m
//  AdvertisingMaster
//
//  Created by XXX on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "ImageVerifyView.h"
static NSInteger iconWidth = 23;
@interface ImageVerifyView()<UITextFieldDelegate>


@end

@implementation ImageVerifyView


- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        
        [self initUI];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFiledEditChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:self.tf];
    }
    
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initUI{
    [self addSubview:self.img];
    [self addSubview:self.tf];
    [self addSubview:self.btn];
    [self layout];
    [self addBottomSingleLine:[UIColor colorWithHexString:@"#EBEBEB"]];
}

-(void)layout{
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left);
    }];
    
    [self.tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.img.mas_centerY);
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.btn.mas_left).offset(-5);;
    }];
    
    [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.img.mas_centerY);
        make.width.equalTo(@(100));
        make.height.equalTo(@(33));
        make.right.equalTo(self.mas_right).offset(-0);
    }];
}

-(UIImageView*)img{
    if (!_img) {
        _img = [UIImageView new];        
    }
    return _img;
}

-(UITextField* )tf{
    if (!_tf) {
        _tf = [[UITextField alloc] init];
        _tf.delegate = self;
        _tf.placeholder = @"输入图形验证码";
        [_tf setPlaceholderColor:[UIColor moPlaceHolder]];
        //[_tf setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        _tf.textColor = [UIColor blackColor];
        _tf.keyboardType = UIKeyboardTypeDefault;
        _tf.returnKeyType = UIReturnKeyNext;
        [_tf setFont:[UIFont systemFontOfSize:15]];
         _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _tf;
}

- (void)textFiledEditChanged:(NSNotification *)obj {
    UITextField *textField = (UITextField *)obj.object;
    if (textField == self.tf) {
        if (self.getText) {
            self.getText(textField.text);
        }
    }
}

-(UIButton*)btn{
    if (!_btn) {
        _btn = [[UIButton alloc]initWithFrame:CGRectZero];
        _btn.backgroundColor = [UIColor whiteColor];
        [_btn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [_btn setBackgroundColor:[UIColor moBackground]];
    }
    return _btn;
}

-(void)click:(id)sender{
    if (self.picBlock) {
        self.picBlock(nil);
    }
}

-(void)reloadImage:(UIImage* )img{
     [self.btn setBackgroundImage:img forState:UIControlStateNormal];
}

@end
