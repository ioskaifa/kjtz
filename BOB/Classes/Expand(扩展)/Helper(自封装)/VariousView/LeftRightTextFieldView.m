//
//  LeftRightTextFieldView.m
//  AdvertisingMaster
//
//  Created by mac on 2020/6/5.
//  Copyright © 2020 mac. All rights reserved.
//

#import "LeftRightTextFieldView.h"

@interface LeftRightTextFieldView ()

@property (nonatomic, strong) UILabel *leftLbl;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *placeholder;

@end

@implementation LeftRightTextFieldView

+ (instancetype)leftRightView:(NSString *)title withPlaceholder:(NSString *)placeholder {
    LeftRightTextFieldView *view = [[LeftRightTextFieldView alloc] initWithTitle:title withPlaceholder:placeholder];
    return view;
}

- (instancetype)initWithTitle:(NSString *)title withPlaceholder:(NSString *)placeholder {
    if (self = [super init]) {
        self.title = title;
        self.placeholder = placeholder;
        [self createUI];
    }
    return self;
}

- (instancetype)init {
    if (self = [super init]) {
        [self createUI];
    }
    return self;
}

#pragma mark - Public Method
+ (CGFloat)viewHeight {
    return 60;
}

- (NSString *)value {
    return self.rightTF.text;
}

- (void)setValue:(NSString *)value {
    self.rightTF.text = value;
}

#pragma mark - InitUI
- (void)createUI {
    self.backgroundColor = [UIColor whiteColor];
    MyLinearLayout *layout = [MyLinearLayout linearLayoutWithOrientation:MyOrientation_Horz];
    layout.myTop = 0;
    layout.myLeft = 0;
    layout.myRight = 0;
    layout.myBottom = 0;
    [self addSubview:layout];
    
    [layout addSubview:self.leftLbl];
    [layout addSubview:self.rightTF];
}

#pragma mark - Init
- (UILabel *)leftLbl {
    if (!_leftLbl) {
        _leftLbl = ({
            UILabel *object = [UILabel new];
            object.font = [UIFont boldFont15];
            object.textColor = [UIColor moBlack];
            object.text = self.title;
            object.myCenterY = 0;
            object.myLeft = 15;
            object.myHeight = 15;
            object.width = 100;
            object.myWidth = object.width;
            object;
        });
    }
    return _leftLbl;
}

- (UITextField *)rightTF {
    if (!_rightTF) {
        _rightTF = ({
            UITextField *object = [UITextField new];
            object.backgroundColor = [UIColor whiteColor];            
            object.font = [UIFont font15];
            NSAttributedString *att = [NSMutableAttributedString initWithTitles:@[self.placeholder]
                                                                         colors:@[[UIColor colorWithHexString:@"#999999"]]
                                                                          fonts:@[[UIFont font15]]];
            object.attributedPlaceholder = att;
            object.myCenterY = 0;
            object.myLeft = 15;
            object.myWidth = SCREEN_WIDTH - self.leftLbl.width - 30 - 15;
            object.myHeight = MyLayoutSize.fill;
            object;
        });
    }
    return _rightTF;
}

@end
