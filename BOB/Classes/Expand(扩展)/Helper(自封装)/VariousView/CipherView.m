//
//  ImageVerifyView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "CipherView.h"
static NSInteger iconWidth = 22;
@interface CipherView()<UITextFieldDelegate>
{
    
}

@property (nonatomic, strong) UIImageView     *img;

@property (nonatomic, strong) UILabel         *lbl;

@property (nonatomic, strong) UIButton        *btn;

@property (nonatomic, strong) MXSeparatorLine *line;

@end

@implementation CipherView


- (instancetype)init{
    
    self=[super init];
    if (self) {
        self.tag = 998877;
        [self initUI];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFiledEditChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:self.tf];
    }
    
    return self;
}

-(void)initUI{
    self.backgroundColor = [UIColor moBackground];
    [self addSubview:self.lbl];
    [self addSubview:self.img];
    [self addSubview:self.tf];
    [self addSubview:self.btn];
    self.line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self addSubview:self.line];
    [self layout];
}

-(void)layout{
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(-0);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(0.5));
    }];
    
    [self.lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(40));
        make.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    
    [self.tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.lbl.mas_right).offset(2*5);
        make.left.equalTo(self.img.mas_right).offset(2*5).priority(300);
        make.left.equalTo(self.line.mas_left).offset(2*5).priority(700);
        make.right.equalTo(self.btn.mas_left).offset(-5);;
    }];
    
    [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@(30));
        make.height.equalTo(@(30));
        make.right.equalTo(self.line.mas_right);
    }];
}



-(void)click:(id)sender{
    self.tf.secureTextEntry = !self.tf.secureTextEntry;
    self.btn.selected =  !self.btn.selected ;
}

- (void)textFiledEditChanged:(NSNotification *)obj {
    UITextField *textField = (UITextField *)obj.object;
    if (textField == self.tf) {
        if (self.getText) {
            self.getText(textField.text);
        }
    }
}


-(void)reloadTitle:(NSString *) title placeHolder:(NSString *)placeHolder{
    self.lbl.text = title;
     NSAttributedString *placeHolderAtt = [NSMutableAttributedString initWithTitles:@[placeHolder]
                                                                               colors:@[[UIColor moPlaceHolder]]
                                                                                fonts:@[[UIFont font16]]];
    self.tf.attributedPlaceholder = placeHolderAtt;
    [self.img removeFromSuperview];
    [self layoutIfNeeded];
}

-(void)reloadImg:(NSString *) image placeHolder:(NSString *)placeHolder{
    self.img.image = [UIImage imageNamed:image];
     NSAttributedString *placeHolderAtt = [NSMutableAttributedString initWithTitles:@[placeHolder]
                                                                               colors:@[[UIColor moPlaceHolder]]
                                                                                fonts:@[[UIFont font16]]];
    self.tf.attributedPlaceholder = placeHolderAtt;
    [self.lbl removeFromSuperview];
    [self layoutIfNeeded];
}

-(void)reloadPlaceHolder:(NSString *)placeHolder{
     self.tf.placeholder = placeHolder;
    [self.lbl removeFromSuperview];
    [self.img removeFromSuperview];
    [self layoutIfNeeded];
}

-(void)isHiddenLine:(BOOL)isHidden{
    self.line.hidden = isHidden;
}

-(UILabel* )lbl{
    if (!_lbl) {
        _lbl = [[UILabel alloc] init];
        _lbl.textColor = [UIColor blackColor];
        _lbl.text = Lang(@"新登录密码");
        [_lbl setFont:[UIFont font16]];
    }
    return _lbl;
}

-(UIImageView*)img{
    if (!_img) {
        _img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"密码"]];
    }
    return _img;
}

-(UIButton*)btn{
    if (!_btn) {
        _btn = [[UIButton alloc]initWithFrame:CGRectZero];
        _btn.backgroundColor = [UIColor clearColor];
        [_btn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [_btn setImage:[UIImage imageNamed:@"显示"] forState:UIControlStateNormal];
        [_btn setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateSelected];
        //        _btn.hidden = YES;
    }
    return _btn;
}


-(UITextField* )tf{
    if (!_tf) {
        _tf = [[UITextField alloc] init];
        _tf.delegate = self;
        _tf.secureTextEntry = YES;
        [_tf setPlaceholderFont:[UIFont font16]];
        _tf.placeholder = Lang(@"请输入登录密码");
        [_tf setPlaceholderColor: [UIColor moPlaceHolder]];
        _tf.textColor = [UIColor moBlack];
        _tf.keyboardType = UIKeyboardTypeDefault;
        _tf.returnKeyType = UIReturnKeyNext;        
        _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _tf;
}

-(void)setTitleWidth:(NSInteger)width{
    [self.lbl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(width));
        make.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    [self layoutIfNeeded];
}
@end
