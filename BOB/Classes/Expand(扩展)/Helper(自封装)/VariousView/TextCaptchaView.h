//
//  VerityCodeView2.h
//  JiuJiuEcoregion
//
//  Created by mac on 2019/6/3.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TextCaptchaView : UIView

@property (nonatomic, strong) UITextField     *tf;

@property (nonatomic , strong) FinishedBlock getText;

@property (nonatomic , strong) FinishedBlock sendCodeBlock;

-(void)isHiddenLine:(BOOL)hidden;

-(void)reloadTitle:(NSString *) title placeHolder:(NSString *)placeHolder;

-(void)reloadImg:(NSString *) image  placeHolder:(NSString *)placeHolder;

-(void)reloadPlaceHolder:(NSString *)placeHolder;

-(void)startCountDowView;

-(void)setTitleWidth:(NSInteger)width;

@end

NS_ASSUME_NONNULL_END
