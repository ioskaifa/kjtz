//
//  PhoneNumView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "PhoneNumView.h"
#import "UIViewController+FormatPhoneNumber.h"
@interface PhoneNumView()<UITextFieldDelegate>
{
    //手机格式344定义的变量
    NSString *previousTextFieldContent;
    UITextRange *previousSelection;
}

@end

@implementation PhoneNumView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self=[super initWithFrame:frame];
    if (self) {
        self.tag = 998877;
        [self initUI];
    }
    
    return self;
}

-(void)initUI{
    [self addSubview:self.lbl];
    [self addSubview:self.tf];
    self.line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self addSubview:self.line];
    [self layout];
}

-(void)layout{
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(@(0.5));
    }];
    [self.lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(40));
        make.centerY.equalTo(self);
        make.left.equalTo(self.mas_left).offset(15);
    }];
    [self.tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.centerY.equalTo(self);
        make.left.equalTo(self.lbl.mas_right).offset(2*5);
    }];
}

-(UILabel*)lbl{
    if (!_lbl) {
        _lbl = [UILabel new];
        _lbl.font = [UIFont font14];
        _lbl.textColor = [UIColor moBlack];
    }
    return _lbl;
}

-(NumberPadTextField* )tf{
    if (!_tf) {
        _tf = [[NumberPadTextField alloc] init];
        _tf.delegate = self;
        _tf.placeholder =  Lang(@"手机号");
        _tf.textColor = [UIColor moBlack];
        _tf.keyboardType = UIKeyboardTypeNumberPad;
        [_tf setFont:[UIFont font14]];
        //3-4-4格式加的事件
        [_tf addTarget:self action:@selector(formatPhoneNumber:) forControlEvents:UIControlEventEditingChanged];
        _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _tf;
}

- (void)textFiledEditChanged:(NSNotification *)obj {
    UITextField *textField = (UITextField *)obj.object;
    if (textField == self.tf) {
        if (self.getText) {
            self.getText(textField.text);
        }
    }
}
//3-4-4格式手机号输入
- (void)formatPhoneNumber:(UITextField *)textField {
    [self formatPhone:textField previousTextFieldContent:previousTextFieldContent previousSelection:previousSelection];
    if (textField == self.tf) {
        if (self.getText) {
            NSString* text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            self.getText(text);
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    previousTextFieldContent = textField.text;
    previousSelection = textField.selectedTextRange;
    return YES;
}

-(void)formatPhone:(UITextField *)textField previousTextFieldContent:(NSString *)previousTextFieldContent previousSelection:(UITextRange *)previousSelection{
    NSUInteger targetCursorPosition =
    [textField offsetFromPosition:textField.beginningOfDocument
                       toPosition:textField.selectedTextRange.start];
    // nStr表示不带空格的号码
    NSString *nStr = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *preTxt = [previousTextFieldContent stringByReplacingOccurrencesOfString:@" " withString:@""];
    char editFlag = 0;  // 正在执行删除操作时为0，否则为1
    
    if (nStr.length <= preTxt.length) {
        editFlag = 0;
    } else {
        editFlag = 1;
    }
    
    // textField设置text
    if (nStr.length > 11) {
        textField.text = previousTextFieldContent;
        textField.selectedTextRange = previousSelection;
        return;
    }
    
    // 空格
    NSString *spaceStr = @" ";
    
    NSMutableString *mStrTemp = [NSMutableString new];
    int spaceCount = 0;
    if (nStr.length < 3 && nStr.length > -1) {
        spaceCount = 0;
    } else if (nStr.length < 7 && nStr.length > 2) {
        spaceCount = 1;
        
    } else if (nStr.length < 12 && nStr.length > 6) {
        spaceCount = 2;
    }
    
    for (int i = 0; i < spaceCount; i++) {
        if (i == 0) {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(0, 3)], spaceStr];
        } else if (i == 1) {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(3, 4)], spaceStr];
        } else if (i == 2) {
            [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(7, 4)], spaceStr];
        }
    }
    
    if (nStr.length == 11) {
        [mStrTemp appendFormat:@"%@%@", [nStr substringWithRange:NSMakeRange(7, 4)], spaceStr];
    }
    
    if (nStr.length < 4) {
        [mStrTemp appendString:[nStr substringWithRange:NSMakeRange(nStr.length - nStr.length % 3,
                                                                    nStr.length % 3)]];
    } else if (nStr.length > 3) {
        NSString *str = [nStr substringFromIndex:3];
        [mStrTemp appendString:[str substringWithRange:NSMakeRange(str.length - str.length % 4,
                                                                   str.length % 4)]];
        if (nStr.length == 11) {
            [mStrTemp deleteCharactersInRange:NSMakeRange(13, 1)];
        }
    }
    textField.text = mStrTemp;
    // textField设置selectedTextRange
    NSUInteger curTargetCursorPosition = targetCursorPosition;  // 当前光标的偏移位置
    if (editFlag == 0) {
        //删除
        if (targetCursorPosition == 9 || targetCursorPosition == 4) {
            curTargetCursorPosition = targetCursorPosition - 1;
        }
    } else {
        //添加
        if (curTargetCursorPosition == 9 || curTargetCursorPosition == 4) {
            curTargetCursorPosition = targetCursorPosition + 1;
        }
    }
    
    UITextPosition *targetPosition = [textField positionFromPosition:[textField beginningOfDocument]
                                                              offset:curTargetCursorPosition];
    [textField setSelectedTextRange:[textField textRangeFromPosition:targetPosition
                                                          toPosition:targetPosition]];
}

-(void)reloadTitle:(NSString*)title placeholder:(NSString *)placeholder {
    self.lbl.text = title;
    self.tf.placeholder = placeholder;
}

@end
