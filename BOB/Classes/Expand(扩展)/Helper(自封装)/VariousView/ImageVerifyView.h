//
//  ImageVerifyView.h
//  AdvertisingMaster
//
//  Created by XXX on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageVerifyView : UIView

@property (nonatomic, strong) UIImageView     *img;

@property (nonatomic, strong) UITextField     *tf;

@property (nonatomic, strong) UIButton        *btn;

@property (nonatomic , strong) FinishedBlock getText;

@property (nonatomic , strong) FinishedBlock picBlock;

-(void)reloadImage:(UIImage* )img;

@end

NS_ASSUME_NONNULL_END
