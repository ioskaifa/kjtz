//
//  VerityCodeView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "TextCaptchaView.h"
#import "MXCountdownView.h"
#import "LoginRegModel.h"
static NSInteger  interval =  60 ;// 发送短信间隔时间:秒
static NSInteger iconWidth = 22;
@interface TextCaptchaView()<UITextFieldDelegate,MXCountdownViewDelegate>
{
    
}

@property (nonatomic, strong) UILabel         *lbl;

@property (nonatomic, strong) UIImageView     *img;

@property (nonatomic, strong) MXCountdownView *countDowView;

@property (nonatomic, strong) MXSeparatorLine *line;

@property (nonatomic, strong) MXSeparatorLine *horLine;

@property (nonatomic, strong) LoginRegModel   *model;

@end

@implementation TextCaptchaView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        [self initUI];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFiledEditChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:self.tf];
    }
    
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initUI{
    self.line = ({
        MXSeparatorLine* tmp = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
        tmp;
    });
    
    self.horLine = ({
        MXSeparatorLine* tmp = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
        tmp;
    });
    
    [self addSubview:self.lbl];
    [self addSubview:self.img];
    [self addSubview:self.tf];
    [self addSubview:self.countDowView];
    [self addSubview:self.line];
    [self addSubview:self.horLine];
    [self layout];
}

-(void)layout{
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(-0);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(0.5));
    }];
    
    [self.lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(40));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    
    [self.tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.lbl.mas_right).offset(2*5);
        make.left.equalTo(self.img.mas_right).offset(2*5).priority(300);
        make.left.equalTo(self.line).offset(2*5).priority(700);
        make.right.equalTo(self.countDowView.mas_left).offset(-5);;
    }];
    
    [self.countDowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@(80));
        make.height.equalTo(@(22));
        make.right.equalTo(self.line.mas_right);
    }];
    
    [self.horLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.countDowView.mas_left).offset(-0);
        make.centerY.equalTo(self.mas_centerY);
        make.height.equalTo(@(10));
        make.width.equalTo(@(1));
    }];
}

- (void)textFiledEditChanged:(NSNotification *)obj {
    UITextField *textField = (UITextField *)obj.object;
    if (textField == self.tf) {
        if (self.getText) {
            self.getText(textField.text);
        }
    }
}

- (void)startToRequest {
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    if (self.sendCodeBlock) {
        self.sendCodeBlock(nil);
    }
}

-(void)isHiddenLine:(BOOL)isHidden{
    self.line.hidden = isHidden;
}

-(UILabel* )lbl{
    if (!_lbl) {
        _lbl = [[UILabel alloc] init];
        _lbl.textColor = [UIColor moBlack];
        _lbl.text = Lang(@"短信验证码");
        [_lbl setFont:[UIFont systemFontOfSize:13]];
    }
    return _lbl;
}

-(UIImageView*)img{
    if (!_img) {
        _img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"验证码"]];
    }
    return _img;
}

-(MXCountdownView *)countDowView{
    if (!_countDowView) {
        _countDowView = [[MXCountdownView alloc] initWithFrame:CGRectZero timeInterval:interval];
        _countDowView.delegate = self;
        _countDowView.normalTitleColor = [UIColor themeColor];
        _countDowView.buttonNormalBackColor = [UIColor clearColor];
    }
    return _countDowView;
}

-(UITextField* )tf{
    if (!_tf) {
        _tf = [[UITextField alloc] init];
        _tf.delegate = self;
        _tf.placeholder = Lang(@"请输入验证码");
        [_tf setPlaceholderColor: [UIColor moPlaceHolder]];
        _tf.textColor = [UIColor moBlack];
        _tf.keyboardType = UIKeyboardTypeDefault;
        _tf.returnKeyType = UIReturnKeyNext;
        [_tf setFont:[UIFont systemFontOfSize:12]];
        _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _tf;
}

-(void)startCountDowView{
    [self.countDowView start];
}

-(void)reloadTitle:(NSString *) title placeHolder:(NSString *)placeHolder{
    self.lbl.text = title;
    self.tf.placeholder = placeHolder;
    [self.img removeFromSuperview];
    [self layoutIfNeeded];
}

-(void)reloadImg:(NSString *) image  placeHolder:(NSString *)placeHolder{
    self.img.image = [UIImage imageNamed:image];
    self.tf.placeholder = placeHolder;
    [self.lbl removeFromSuperview];
    [self layoutIfNeeded];
}

-(void)setTitleWidth:(NSInteger)width{
    [self.lbl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(width));
        make.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    [self layoutIfNeeded];
}

-(void)reloadPlaceHolder:(NSString *)placeHolder{
    self.tf.placeholder = placeHolder;
    [self.lbl removeFromSuperview];
    [self.img removeFromSuperview];
    [self layoutIfNeeded];
}
@end
