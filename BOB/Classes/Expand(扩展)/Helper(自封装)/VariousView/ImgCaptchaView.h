//
//  ImgCaptchaView.h
//  BOB
//
//  Created by mac on 2019/7/15.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImgCaptchaView : UIView

@property (nonatomic , strong) FinishedBlock getText;

@property (nonatomic , strong) FinishedBlock picBlock;
//标题
-(void)reloadTitle:(NSString *) title placeHolder:(NSString *)placeHolder;
//图片
-(void)reloadImg:(NSString *) img placeHolder:(NSString *)placeHolder;

-(void)reloadRightImg:(UIImage *)image;
@end

NS_ASSUME_NONNULL_END
