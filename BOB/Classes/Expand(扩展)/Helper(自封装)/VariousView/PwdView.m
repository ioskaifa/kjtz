//
//  ImageVerifyView.m
//  AdvertisingMaster
//
//  Created by XXX on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "PwdView.h"
static NSInteger iconWidth = 23;
@interface PwdView()<UITextFieldDelegate>
{
    
}

@end

@implementation PwdView


- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        
        [self initUI];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFiledEditChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:self.tf];
    }
    
    return self;
}

-(void)initUI{
    [self addSubview:self.img];
    [self addSubview:self.tf];
    [self addSubview:self.btn];
    self.line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self addSubview:self.line];
    [self layout];
}

-(void)layout{
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(-0);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(0.5));
    }];
    
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(@(0));
    }];
    
    [self.tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.img.mas_centerY);
        make.left.equalTo(self.img.mas_right).offset(2*5);
        make.right.equalTo(self.btn.mas_left).offset(-5);;
    }];
    
    [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.img.mas_centerY);
        make.width.equalTo(@(30));
        make.height.equalTo(@(30));
        make.right.equalTo(self.line.mas_right);
    }];
}

- (void)hideIcon {
    [self.img mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(0));
        make.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.line.mas_left).offset(-2*5);
    }];
}


-(UIImageView*)img{
    if (!_img) {
        _img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_login_psd"]];
    }
    return _img;
}

-(UITextField* )tf{
    if (!_tf) {
        _tf = [[UITextField alloc] init];
        _tf.delegate = self;
        _tf.secureTextEntry = YES;
        _tf.placeholder = @"输入密码";
        [_tf setPlaceholderColor:[UIColor whiteColor]];
        _tf.textColor = [UIColor whiteColor];
        _tf.keyboardType = UIKeyboardTypeDefault;
        _tf.returnKeyType = UIReturnKeyNext;
        [_tf setFont:[UIFont font14]];
         _tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _tf;
}

-(UIButton*)btn{
    if (!_btn) {
        _btn = [[UIButton alloc]initWithFrame:CGRectZero];
        [_btn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [_btn setImage:[UIImage imageNamed:@"隐藏"] forState:UIControlStateNormal];
        [_btn setImage:[UIImage imageNamed:@"显示"] forState:UIControlStateSelected];
    }
    return _btn;
}

-(void)click:(id)sender{
    self.tf.secureTextEntry = !self.tf.secureTextEntry;
    self.btn.selected =  !self.btn.selected ;
}

- (void)textFiledEditChanged:(NSNotification *)obj {
    UITextField *textField = (UITextField *)obj.object;
    if (textField == self.tf) {
        if (self.getText) {
            self.getText(textField.text);
        }
    }
}


-(void)reloadAttrPlaceHolder:(NSMutableAttributedString *) placeholder{
    self.tf.attributedPlaceholder = placeholder;
}

-(void)reloadPlaceHolder:(NSString *) placeholder{
    self.tf.placeholder = placeholder;
}

-(void)setHiddenDownLine:(BOOL)isHidden{
    self.line.hidden = isHidden;
}

- (void)setNextTextField:(UITextField *)nextTextField {
    if(nextTextField == nil) {
        self.tf.returnKeyType = UIReturnKeyDone;
        return;
    }
    _nextTextField = nextTextField;
    self.tf.returnKeyType = UIReturnKeyNext;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(self.nextTextField != nil) {
        [self.nextTextField becomeFirstResponder];
    }
    return YES;
}
@end
