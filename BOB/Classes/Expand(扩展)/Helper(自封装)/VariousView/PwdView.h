//
//  PwdView.h
//  AdvertisingMaster
//
//  Created by XXX on 2019/4/6.
//  Copyright © 2019年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PwdView : UIView

@property (nonatomic , strong) FinishedBlock getText;

@property (nonatomic, strong) UITextField     *tf;

@property (nonatomic, strong) MXSeparatorLine *line;

@property (nonatomic, strong) UIImageView     *img;

@property (nonatomic, strong) UIButton        *btn;

-(void)reloadPlaceHolder:(NSString *) placeholder;

-(void)reloadAttrPlaceHolder:(NSMutableAttributedString *) placeholder;

-(void)setHiddenDownLine:(BOOL)isHidden;

- (void)hideIcon;

///下一个UITextField，用于点击键盘next时光标移动到下个文本框
@property (nonatomic , strong) UITextField *nextTextField;
@end

NS_ASSUME_NONNULL_END
