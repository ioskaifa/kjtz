//
//  LeftRightTextFieldView.h
//  AdvertisingMaster
//
//  Created by mac on 2020/6/5.
//  Copyright © 2020 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeftRightTextFieldView : UIView

@property (nonatomic, copy) NSString *value;

@property (nonatomic, strong) UITextField *rightTF;

+ (instancetype)leftRightView:(NSString *)title withPlaceholder:(NSString *)placeholder;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
