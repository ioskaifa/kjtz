//
//  ImageVerifyView.m
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import "LanguageView.h"
static NSInteger iconWidth = 22;
@interface LanguageView()<UITextFieldDelegate>
{
    
}

@property (nonatomic, strong) UIImageView     *img;

@property (nonatomic, strong) UILabel     *titleLbl;

@property (nonatomic, strong) MXSeparatorLine *line;

@property (nonatomic, strong) UILabel     *rightLbl;
@end

@implementation LanguageView


- (instancetype)initWithFrame:(CGRect)frame {
    
    self=[super initWithFrame:frame];
    if (self) {
        self.tag = 998877;
        [self initUI];
    
    }
    
    return self;
}

-(void)initUI{
    [self addSubview:self.img];
    [self addSubview:self.titleLbl];
    [self addSubview:self.rightLbl];
    self.line = [MXSeparatorLine initHorizontalLineWidth:0 orginX:0 orginY:0];
    [self addSubview:self.line];
    [self layout];
}

-(void)layout{
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(0);
        make.right.equalTo(self.mas_right).offset(-0);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@(1));
    }];
    
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(@(iconWidth));
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.line.mas_left);
    }];
    
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.img.mas_centerY);
        make.left.equalTo(self.img.mas_right).offset(2*5);
        make.right.equalTo(self.rightLbl.mas_left).offset(-5);;
    }];
    
    [self.rightLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.img.mas_centerY);
//        make.width.equalTo(@(80));
        make.right.equalTo(self.mas_right).offset(-5);;
    }];
    
}

-(UIImageView*)img{
    if (!_img) {
        _img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"邀请"]];
    }
    return _img;
}

-(UILabel* )titleLbl{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] init];
        _titleLbl.textColor = [UIColor whiteColor];
        _titleLbl.text = Lang(@"中文");
        [_titleLbl setFont:[UIFont font15]];
    }
    return _titleLbl;
}

-(UILabel* )rightLbl{
    if (!_rightLbl) {
        _rightLbl = [[UILabel alloc] init];
        _rightLbl.textColor = [UIColor themeColor];
        _rightLbl.text = Lang(@"切换语言");
        [_rightLbl setFont:[UIFont font14]];
    }
    return _rightLbl;
}

-(void)reloadPlaceHolder:(NSString *) placeholder image:(NSString *)image desc:(NSString* )text{
    self.img.image = [UIImage imageNamed:image];
    _titleLbl.text = placeholder;
    _rightLbl.text = text;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (self.block) {
        self.block(nil);
    }
}
@end
