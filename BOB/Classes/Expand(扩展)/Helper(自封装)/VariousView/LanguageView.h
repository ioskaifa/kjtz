//
//  LanguageView.h
//  JiuJiuEcoregion
//
//  Created by mac on 2019/5/31.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LanguageView : UIView

@property (nonatomic ,strong) FinishedBlock block;

-(void)reloadPlaceHolder:(NSString *) placeholder image:(NSString *)image desc:(NSString* )text;


@end

NS_ASSUME_NONNULL_END
