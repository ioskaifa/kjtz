//
//  PhoneNumView.h
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NumberPadTextField.h"

NS_ASSUME_NONNULL_BEGIN
@class LoginRegModel;
@interface PhoneNumView : UIView

@property (nonatomic,strong) UILabel        *lbl;

@property (nonatomic,strong) NumberPadTextField *tf;

@property (nonatomic,strong) MXSeparatorLine    *line;

@property (nonatomic,strong) LoginRegModel*    model;

@property (nonatomic , strong) FinishedBlock getText;

-(void)reloadTitle:(NSString*)title placeholder:(NSString *)placeholder ;

@end

NS_ASSUME_NONNULL_END
