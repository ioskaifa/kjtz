//
//  VerityCodeView.h
//  AdvertisingMaster
//
//  Created by XXX on 2019/4/5.
//  Copyright © 2019年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXCountdownView.h"

NS_ASSUME_NONNULL_BEGIN

typedef BOOL(^VerityCodeFinishedBlock)(id _Nullable data);

@class LoginRegModel;
@interface VerityCodeView : UIView

@property (nonatomic, strong) UITextField     *tf;

@property (nonatomic, strong) MXCountdownView *countDowView;

@property (nonatomic, strong) MXSeparatorLine    *line;

@property (nonatomic, strong) UIImageView     *img;

@property (nonatomic , strong) FinishedBlock getText;

@property (nonatomic , strong) VerityCodeFinishedBlock sendCode;

@property (nonatomic,strong) NSString   *phoneNum;

///下一个UITextField，用于点击键盘next时光标移动到下个文本框
@property (nonatomic , strong) UITextField *nextTextField;

-(void)configModel:(LoginRegModel*)model;

-(void)setHiddenDownLine:(BOOL)isHidden;

- (void)hideIcon;
@end


NS_ASSUME_NONNULL_END
