//
//  InvitationCodeView.h
//  BOB
//
//  Created by mac on 2019/7/15.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InvitationCodeView : UIView

@property (nonatomic, strong) UITextField     *tf;

@property (nonatomic , strong) FinishedBlock getText;

- (instancetype)initWithFrame:(CGRect)frame ;

-(void)reloadTitle:(NSString *)title placeHolder:(NSString *)placeHolder;

-(void)reloadImg:(NSString *)image  placeHolder:(NSString *)placeHolder;

-(void)isHiddenLine:(BOOL)hidden;

-(void)setTitleWidth:(NSInteger)width;

@end

NS_ASSUME_NONNULL_END
