//
//  UserLiveProgressModel.m
//  BOB
//
//  Created by AlphaGo on 2020/11/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserLiveProgressModel.h"

@implementation UserLiveProgressModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"_id" : @"id"};
}
@end
