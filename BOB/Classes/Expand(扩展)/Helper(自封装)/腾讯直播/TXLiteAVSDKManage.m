//
//  TXLiteAVSDKManage.m
//  BOB
//
//  Created by AlphaGo on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "TXLiteAVSDKManage.h"
//#import "TCBeautyPanel.h"
#import "TCGlobalConfig.h"
#import "SDKHeader.h"
#import "TCAccountMgrModel.h"
#import "TCWechatInfoView.h"
#import "HUDHelper.h"

@interface TXLiteAVSDKManage ()<
                                TXLivePushListener
                                //BeautyLoadPituDelegate
//                                ScanQRDelegate,
//                                PushSettingDelegate,
//                                PushMoreSettingDelegate,
//                                AddressBarControllerDelegate,
//                                #ifdef ENABLE_CUSTOM_MODE_AUDIO_CAPTURE
//                                CustomAudioFileReaderDelegate,
//                                #endif
//                                PushBgmControlDelegate,
//                                AudioEffectViewDelegate
                                >
@property(nonatomic,strong) TXLivePush *livePusher;
//@property(nonatomic,strong) TCBeautyPanel *beautyPanel;    // 美颜控件
@end

@implementation TXLiteAVSDKManage

+ (void)load {
    [[self shared] txLiteInit];
}

+(instancetype)shared {
    static dispatch_once_t onceToken;
    static TXLiteAVSDKManage *instance;
    dispatch_once(&onceToken, ^{
        instance = [[TXLiteAVSDKManage alloc] init];
    });
    return instance;
}

- (void)txLiteInit {
    [TXLiveBase setLicenceURL:LICENCE_URL key:LICENCE_KEY];
    //MLog(@"TXSDK Version = %@", [TXLiveBase getSDKVersionStr]);

}

- (void)registerAccount:(NSString *)account password:(NSString *)password block:(FinishedBlock)block {
    // 用户名密码注册
    __weak typeof(self) weakSelf = self;
    [[HUDHelper sharedInstance] syncLoading];
    [[TCAccountMgrModel sharedInstance] registerWithUsername:account password:password succ:^(NSString *userName, NSString *md5pwd) {
        // 注册成功后直接登录
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TCAccountMgrModel sharedInstance] loginWithUsername:UDetail.user.zhibo_account password:UDetail.user.zhibo_password succ:^(NSString *userName, NSString *md5pwd) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[HUDHelper sharedInstance] syncStopLoading];
                    
//                    __strong typeof(weakSelf) self = weakSelf;
//                    if (self) {
//                        [self.delegate loginSuccess:userName hashedPwd:md5pwd];
//                    }
                });
            } fail:^(int errCode, NSString *errMsg) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[HUDHelper sharedInstance] syncStopLoading];
                    [HUDHelper alertTitle:@"登录失败" message:errMsg cancel:@"确定"];
                    NSLog(@"%s %d %@", __func__, errCode, errMsg);
                });
            }];
        });
        
    } fail:^(int errCode, NSString *errMsg) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[HUDHelper sharedInstance] syncStopLoading];
            //[HUDHelper alertTitle:@"注册失败" message:errMsg cancel:@"确定"];
            NSLog(@"%s %d %@", __func__, errCode, errMsg);
        });
    }];
}

- (void)applyLiveProgress:(NSDictionary *)param finish:(FinishedBlock)block {
    [self request:@"api/live/userLiveProgress/applyLiveProgress" param:param
       completion:^(BOOL success, id object, NSString *error) {
        if(success) {
            self.userLiveModel = [UserLiveProgressModel modelParseWithDict:[object valueForKeyPath:@"data.userLiveProgress"]];
        } else {
            [NotifyHelper showMessageWithMakeText:[object valueForKey:@"msg"]];
        }
        
        Block_Exec(block,object);
    }];
}

- (void)closeUserLiveProgress:(FinishedBlock)block {
    [self request:@"api/live/userLiveProgress/closeUserLiveProgress" param:nil completion:^(BOOL success, id object, NSString *error) {
        Block_Exec(block,@(success));
    }];
}

- (void)startPreview:(UIView *)baseView {
    //创建一个 view 对象，并将其嵌入到当前界面中
     UIView *_localView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
     [baseView insertSubview:_localView atIndex:0];

     //启动本地摄像头预览
     [self.livePusher startPreview:_localView];
}

- (void)stopPreview {
    [self.livePusher stopPreview];
}

/*
 拼装播放 URL
 播放地址主要由播放前缀、播放域名（domain）、应用名称（AppName）、流名称（StreamName）、播放协议后缀、鉴权参数以及其他自定义参数组成。
 */
- (NSString *)createUrl {
    //Md5(key+StreamName+hex(time))&txTime=hex(time)
//    NSString *txTime = toStr([[NSDate date] timeIntervalSince1970]);
//    NSString *txSecret = [NSString stringWithFormat:@"%@%@%@",licenceKey,UDetail.user.user_id,txTime];
//    NSString *url = [NSString stringWithFormat:@"rtmp://tl.tpshwl.com/live/%@?txSecret=%@&txTime=%@",UDetail.user.user_id,[StringUtil calcMD5forString:txSecret],txTime];
    return [NSString stringWithFormat:@"rtmp://tl.tpshwl.com/live/%@",UDetail.user.user_uid];
}

///启动推流
- (void)startPush {
    NSString* rtmpUrl = [self createUrl];    //此处填写您的 rtmp 推流地址
    int result = [self.livePusher startPush:rtmpUrl];
    if(result != 0) {
        [NotifyHelper showMessageWithMakeText:StrF(@"启动推流失败: [%d]",result)];
    }
}

///结束推流
- (void)stopPush {
    [self.livePusher stopPreview]; //如果已经启动了摄像头预览，请在结束推流时将其关闭。
    [self.livePusher stopPush];
}

///切换前后摄像头
- (void)switchCamera {
    [self.livePusher switchCamera];
}

///展示美颜工具条
//- (void)showBeautyPanel {
//    UIViewController *baseVC = [[MXRouter sharedInstance] getTopViewController];
//    if(baseVC) {
//        [baseVC.view addSubview:self.beautyPanel];
//    } else {
//        [MoApp.window addSubview:self.beautyPanel];
//    }
//    self.beautyPanel.hidden = NO;
//}
//
/////隐藏美颜工具条
//- (void)hideBeautyPanel {
//    self.beautyPanel.hidden = YES;
//}

#pragma mark - 控件响应函数

- (void)clickPush:(UIButton *)btn {
//    if (_btnPush.tag == 0) {
//        if (![self startPush]) {
//            return;
//        }
//        [_btnPush setImage:[UIImage imageNamed:@"stop2"] forState:UIControlStateNormal];
//        _btnPush.tag = 1;
//        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
//
//    } else {
//        [self stopPush];
//        [_logView clear];
//
//        [_btnPush setImage:[UIImage imageNamed:@"start2"] forState:UIControlStateNormal];
//        _btnPush.tag = 0;
//        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
//    }
}

#pragma mark - TXLivePushListener

- (void)onPushEvent:(int)evtID withParam:(NSDictionary *)param {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        if (evtID == PUSH_ERR_NET_DISCONNECT || evtID == PUSH_ERR_INVALID_ADDRESS) {
//            // 断开连接时，模拟点击一次关闭推流
//            [self clickPush:self->_btnPush];
//
//        } else if (evtID == PUSH_ERR_OPEN_CAMERA_FAIL) {
//            [self clickPush:self->_btnPush];
//            [self toastTip:@"获取摄像头权限失败，请前往隐私-相机设置里面打开应用权限"];
//
//        } else if (evtID == PUSH_EVT_OPEN_CAMERA_SUCC) {
//            [self.pusher toggleTorch:[PushMoreSettingViewController isOpenTorch]];
//
//        } else if (evtID == PUSH_ERR_OPEN_MIC_FAIL) {
//            [self clickPush:self->_btnPush];
//            [self toastTip:@"获取麦克风权限失败，请前往隐私-麦克风设置里面打开应用权限"];
//
//        } else if (evtID == PUSH_EVT_CONNECT_SUCC) {
//            [self.pusher setMute:[PushMoreSettingViewController isMuteAudio]];
//            [self.pusher showVideoDebugLog:[PushMoreSettingViewController isShowDebugLog]];
//            [self.pusher setMirror:[PushMoreSettingViewController isMirrorVideo]];
//            BOOL isWifi = [AFNetworkReachabilityManager sharedManager].reachableViaWiFi;
//            if (!isWifi) {
//                __weak __typeof(self) weakSelf = self;
//                [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
//                    if (weakSelf.pushUrl.length == 0) {
//                        return;
//                    }
//                    if (status == AFNetworkReachabilityStatusReachableViaWiFi) {
//                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
//                                                                                       message:@"您要切换到WiFi再推流吗?"
//                                                                                preferredStyle:UIAlertControllerStyleAlert];
//                        [alert addAction:[UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction *_Nonnull action) {
//                            [alert dismissViewControllerAnimated:YES completion:nil];
//
//                            // 先暂停，再重新推流
//                            [weakSelf.pusher stopPush];
//                            [weakSelf.pusher startPush:weakSelf.pushUrl];
//                        }]];
//                        [alert addAction:[UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleCancel handler:^(UIAlertAction *_Nonnull action) {
//                            [alert dismissViewControllerAnimated:YES completion:nil];
//                        }]];
//                        [weakSelf presentViewController:alert animated:YES completion:nil];
//                    }
//                }];
//            }
//        } else if (evtID == PUSH_WARNING_NET_BUSY) {
//            [self->_notification displayNotificationWithMessage:@"您当前的网络环境不佳，请尽快更换网络保证正常直播" forDuration:5];
//        }
//
//        // log
//        [self->_logView setPushEvent:evtID withParam:param];
//    });
}

- (void)onNetStatus:(NSDictionary *)param {
    // 这里可以上报相关推流信息到业务服务器
    // 比如：码率，分辨率，帧率，cpu使用，缓存等信息
    // 字段请在TXLiveSDKTypeDef.h中定义

//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self->_logView setNetStatus:param];
//    });
}

#pragma mark - BeautyLoadPituDelegate

- (void)onLoadPituStart {
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self showInProgressText:@"开始加载资源"];
    });
}

- (void)onLoadPituProgress:(CGFloat)progress {
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self showInProgressText:[NSString stringWithFormat:@"正在加载资源%d %%",(int)(progress * 100)]];
    });
}

- (void)onLoadPituFinished {
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self showText:@"资源加载成功"];
    });
}

- (void)onLoadPituFailed {
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self showText:@"资源加载失败"];
    });
}

- (TXLivePush *)livePusher{
    if(!_livePusher){
        _livePusher = ({
            TXLivePush * object = [[TXLivePush alloc]init];
            TXLivePushConfig *config = [[TXLivePushConfig alloc] init];
            config.pauseImg = [UIImage imageNamed:@"pause_publish.jpg"];
            config.pauseFps = 15;
            config.pauseTime = 300;

            object = [[TXLivePush alloc] initWithConfig:config];
            object.delegate = self;
            [object setVideoQuality:VIDEO_QUALITY_HIGH_DEFINITION adjustBitrate:NO adjustResolution:NO];
            [object setLogViewMargin:UIEdgeInsetsMake(120, 10, 60, 10)];
            config.videoEncodeGop = 2;
            [object setConfig:config];
            object;
       });
    }
    return _livePusher;
}

//- (TCBeautyPanel *)beautyPanel{
//    if(!_beautyPanel){
//        CGFloat bottom = safeAreaInsetBottom();// > 0 ? safeAreaInsetBottom() + 0 : 0;
//        _beautyPanel = [TCBeautyPanel beautyPanelWithFrame:CGRectMake(0, SCREEN_HEIGHT-[TCBeautyPanel getHeight]-bottom, SCREEN_WIDTH, [TCBeautyPanel getHeight] + bottom)
//                                                     SDKObject:self.livePusher];
//        _beautyPanel.bottomOffset = bottom;
//
//        TCBeautyPanelTheme *theme = [[TCBeautyPanelTheme alloc] init];
//        theme.beautyPanelSelectionColor = [UIColor colorWithRed:0
//                                                          green:109/255.0
//                                                           blue:1.0
//                                                          alpha:1.0];
//        theme.sliderValueColor = theme.beautyPanelSelectionColor;
//        theme.beautyPanelMenuSelectionBackgroundImage = [UIImage imageNamed:@"beautyPanelMenuSelectionBackgroundImage"];
//        theme.sliderThumbImage = [UIImage imageNamed:@"slider"];
//        _beautyPanel.theme = theme;
//
//        //_beautyPanel.hidden = YES;
//        _beautyPanel.pituDelegate = self;
//        [_beautyPanel resetAndApplyValues]; // 美颜设置初始值
//    }
//    return _beautyPanel;
//}
@end
