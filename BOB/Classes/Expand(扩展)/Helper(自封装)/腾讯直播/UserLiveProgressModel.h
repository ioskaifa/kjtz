//
//  UserLiveProgressModel.h
//  BOB
//
//  Created by AlphaGo on 2020/11/27.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserLiveProgressModel : BaseObject
@property (nonatomic , assign) NSInteger              start_time;
@property (nonatomic , copy) NSString              * cover_photo;
@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , copy) NSString              * user_id;
@property (nonatomic , copy) NSString              * room_no;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , copy) NSString              * push_url;
@property (nonatomic , assign) NSInteger              _id;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * status;
@end

NS_ASSUME_NONNULL_END
