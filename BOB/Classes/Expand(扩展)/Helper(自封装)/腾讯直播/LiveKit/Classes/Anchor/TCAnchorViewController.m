/**
 * Module: TCAnchorViewController
 *
 * Function: 主播推流模块主控制器，里面承载了渲染view，逻辑view，以及推流相关逻辑，同时也是SDK层事件通知的接收者
 */

#import "TCAnchorViewController.h"
#import "TCAudienceViewController.h"
#import <Foundation/Foundation.h>
#import <sys/types.h>
#import <sys/sysctl.h>
#import <UIKit/UIKit.h>
#import <mach/mach.h>
#import "TCMsgModel.h"
#import "TCUserProfileModel.h"
#import "TCGlobalConfig.h"
#import "NSString+Common.h"
#import <CWStatusBarNotification/CWStatusBarNotification.h>
#import "TCStatusInfoView.h"
#import "TCAccountMgrModel.h"
#import "UIView+Additions.h"
#import "TCBeautyPanel.h"
#import "AudioEffectSettingKit.h"
#import "LiverManageView.h"
#import "HWPanModelVC.h"
#import "LiverMoreView.h"
#import "OnLineViewerView.h"
#import "MyReceivedGiftView.h"
#import "ContectLiverListView.h"
#import "LiveOverView.h"
#import "LiveRoomMsgListTableView.h"
#import "LiveRoomAccPlayerView.h"
#import "UIColor+MLPFlatColors.h"
#import "TXLiteAVSDKManage.h"
#import "ProductView.h"
#import "ManageLiveGoodsView.h"

typedef NS_ENUM(NSInteger, PKStatus) {
    PKStatus_IDLE,         // 空闲状态
    PKStatus_REQUESTING,   // 请求PK中
    PKStatus_BEING,        // PK中
};

#if POD_PITU
//#import "MCTip.h"
//#import "MCCameraDynamicView.h"
//#import "MaterialManager.h"

@interface TCAnchorViewController () <
    AVCaptureVideoDataOutputSampleBufferDelegate,
    UITextFieldDelegate,
    MPMediaPickerControllerDelegate,
//    MCCameraDynamicDelegate,
    MLVBLiveRoomDelegate,
    TCAnchorToolbarDelegate,
    AudioEffectViewDelegate
>

@property (nonatomic, strong) UIButton *filterBtn;
@property (nonatomic, assign) NSInteger currentFilterIndex;
//@property (nonatomic, strong) MCCameraDynamicView *tmplBar;
@end

#else
@interface TCAnchorViewController ()
@property (strong, nonatomic) MLVBAnchorInfo *pkAnchor;

@property (nonatomic, strong) LiverManageView *liverManageView;

@property (nonatomic, strong) LiverMoreView *liverMoreView;

@property (nonatomic, strong) HWPanModelVC *liverMorePanVC;

@property (nonatomic, strong) OnLineViewerView *viewerView;

@property (nonatomic, strong) HWPanModelVC *viewerPanVC;

@property (nonatomic, strong) MyReceivedGiftView *giftView;

@property (nonatomic, strong) HWPanModelVC *giftPanVC;

@property (nonatomic, strong) ContectLiverListView *contectLiverView;

@property (nonatomic, strong) HWPanModelVC *contectLiverPanVC;

@property(nonatomic,strong) ProductView *productView;

@property (nonatomic, strong) ManageLiveGoodsView *manageGoodsView;

@property (nonatomic, strong) HWPanModelVC *goodsPanVC;

@property(nonatomic,strong) LiveOverView *overView;

@property(nonatomic,strong) dispatch_source_t timer;

@end
#endif

#define MAX_LINKMIC_MEMBER_SUPPORT  3

#define VIDEO_VIEW_WIDTH            100
#define VIDEO_VIEW_HEIGHT           150
#define VIDEO_VIEW_MARGIN_BOTTOM    56
#define VIDEO_VIEW_MARGIN_RIGHT     8

@implementation TCAnchorViewController
{
    BOOL _camera_switch;
    float  _beauty_level;
    float  _whitening_level;
    float  _ruddiness_level;
    float  _eye_level;
    float  _face_level;
    BOOL _torch_switch;
    
    NSString*       _testPath;
    BOOL            _isPreviewing;
    
    BOOL       _appIsInterrupt;
    
    TCRoomInfo *_liveInfo;
    
    BOOL        _firstAppear;
    
    TCAnchorToolbarView *_anchorToolbarView;
    UIView             *_videoParentView;
    
    CWStatusBarNotification *_notification;
    
    float _bgmVolume;
    float _micVolume;
    float _bgmPitch;
    float _bgmPosition;
    
    //link mic
    NSString*               _sessionId;
    NSString*               _userIdRequest;
    NSMutableArray*         _statusInfoViewArray;
    BOOL                    _isSupprotHardware;
    uint64_t                _beginTime;
    uint64_t                _endTime;
    
    
    ///PK
    //UIView                   *_pusherView;
    NSMutableDictionary      *_playerViewDic;  // 小主播的画面，[userID, view]
    
    TCBeautyPanel       *_vBeauty;  // 美颜界面组件
    
    UIButton                 *_btnChat;
    UIButton                 *_btnCamera;
    UIButton                 *_btnBeauty;
    UIButton                 *_btnMute;
    UIButton                 *_btnPK;
    UIButton                 *_btnLog;
    

    BOOL                     _beauty_switch;
    BOOL                     _mute_switch;
    
    BOOL                     _appIsInActive;
    BOOL                     _appIsBackground;
    BOOL                     _hasPendingRequest;
    
    UITextView               *_logView;
    UIView                   *_coverView;
    BOOL                     _log_switch;  // 0:隐藏log  1:显示SDK内部的log  2:显示业务层log
    
    // 消息列表展示和输入
    LiveRoomMsgListTableView *_msgListView;
    UIView                   *_msgListCoverView; // 用于盖在消息列表上以监听点击事件
    UIView                   *_msgInputView;
    UITextField              *_msgInputTextField;
    UIButton                 *_msgSendBtn;
    
    CGPoint                  _touchBeginLocation;
    UITextView               *_toastView;
    
    PKStatus                 _pkStatus;             // PK状态
    NSArray                  *_roomCreatorList;     // 所有的房间主播列表(不包括自己），用于作为PK的目标
    UITableView              *_roomCreatorListView; // 用于展示主播列表
}

- (instancetype)initWithPublishInfo:(TCRoomInfo *)liveInfo {
    if (self = [super init]) {
        
        _liveInfo = liveInfo;
        
        _playerViewDic = [[NSMutableDictionary alloc] init];
        
        //link mic
        _sessionId = [self getLinkMicSessionID];
        
        _statusInfoViewArray = [NSMutableArray array];
        
        _setLinkMemeber = [NSMutableSet set];
        
        _isSupprotHardware = ( [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0);
        
        _bgmVolume = 1.f;
        _micVolume = 1.f;
        _bgmPitch = 0.f;
        _bgmPosition = 0.f;
        
        _camera_switch   = NO;
        _beauty_level    = 6.3;
        _whitening_level = 6.0;
        _ruddiness_level = 2.7;
        _torch_switch    = NO;
        _log_switch      = NO;
        _firstAppear     = YES;
        
        _notification = [CWStatusBarNotification new];
        _notification.notificationLabelBackgroundColor = [UIColor redColor];
        _notification.notificationLabelTextColor = [UIColor whiteColor];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        _bgmVolume = 1.f;
        _micVolume = 1.f;
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_anchorToolbarView.vMusicPanel resetAudioSetting];
}

- (void)onAppDidEnterBackGround:(UIApplication*)app {
    // 暂停背景音乐
    [self.liveRoom pauseBGM];
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
    }];
}

- (void)onAppWillEnterForeground:(UIApplication*)app {
    [self.liveRoom resumeBGM];
}

- (void)onAppWillResignActive:(NSNotification*)notification {
    _appIsInterrupt = YES;
}

- (void)onAppDidBecomeActive:(NSNotification*)notification {
    if (_appIsInterrupt) {
        _appIsInterrupt = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIColor* bgColor = [UIColor blackColor];
    [self.view setBackgroundColor:bgColor];
    
    //视频画面的父view
    _videoParentView = [[UIView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:_videoParentView];
    
    //logicView
    _anchorToolbarView = [[TCAnchorToolbarView alloc] initWithFrame:self.view.frame];
    _anchorToolbarView.delegate = self;
    
//    @weakify(self)
//    _anchorToolbarView.updateOrderInfoBlock = ^(id data) {
//        @strongify(self)
//        [self getUserLiveProgressOrderTotal];
//    };
    [_anchorToolbarView setLiveInfo:_liveInfo];
    [self.view addSubview:_anchorToolbarView];
    
    //liveRoom
    _liveRoom = [MLVBLiveRoom sharedInstance];
    [_liveRoom setCameraMuteImage:[UIImage imageNamed:@"pause_publish.jpg"]];
    _liveRoom.delegate = self;
    [_liveRoom startLocalPreview:YES view:_videoParentView];

    _anchorToolbarView.vBeauty.actionPerformer = [TCBeautyPanelActionProxy proxyWithSDKObject:_liveRoom];
    [_anchorToolbarView.vMusicPanel setAudioEffectManager:[_liveRoom getAudioEffectManager]];
    _anchorToolbarView.vMusicPanel.delegate = self;
    [_anchorToolbarView.vBeauty resetAndApplyValues];
    _beauty_level = _anchorToolbarView.vBeauty.beautyLevel;
    _whitening_level = _anchorToolbarView.vBeauty.whiteLevel;
    _ruddiness_level = _anchorToolbarView.vBeauty.ruddyLevel;
    
    _anchorToolbarView.vBeauty.backgroundColor = [UIColor colorWithHexString:@"#19191E"];
    _anchorToolbarView.vMusicPanel.backgroundColor = [UIColor colorWithHexString:@"#19191E"];

    _liveInfo.timestamp = [[NSDate date] timeIntervalSince1970];
    [self startRtmp];
    
    //link mic
    //初始化连麦播放小窗口
    [self initStatusInfoView: 1];
    [self initStatusInfoView: 2];
    [self initStatusInfoView: 3];
    
    [self.view addSubview:self.productView];
    //logicView不能被连麦小窗口挡住
    [self.view bringSubviewToFront:self.anchorToolbarView];
    
    //初始化连麦播放小窗口里的logView
    for (TCStatusInfoView * statusInfoView in _statusInfoViewArray) {
        UIView * logView = [[UIView alloc] initWithFrame:statusInfoView.videoView.frame];
        logView.backgroundColor = [UIColor clearColor];
        logView.hidden = YES;
        logView.backgroundColor = [UIColor whiteColor];
        logView.alpha  = 0.5;
        [self.view addSubview:logView];
        statusInfoView.logView = logView;
    }
    
    //初始化连麦播放小窗口里的踢人Button
    CGFloat width = self.view.size.width;
    CGFloat height = self.view.size.height;
    int index = 1;
    for (TCStatusInfoView* statusInfoView in _statusInfoViewArray) {
        statusInfoView.btnKickout = [[UIButton alloc] initWithFrame:CGRectMake(width - BOTTOM_BTN_ICON_WIDTH/2 - VIDEO_VIEW_MARGIN_RIGHT - 5, height - VIDEO_VIEW_MARGIN_BOTTOM - VIDEO_VIEW_HEIGHT * index + 5, BOTTOM_BTN_ICON_WIDTH/2, BOTTOM_BTN_ICON_WIDTH/2)];
        [statusInfoView.btnKickout addTarget:self action:@selector(clickBtnKickout:) forControlEvents:UIControlEventTouchUpInside];
        [statusInfoView.btnKickout setImage:[UIImage imageNamed:@"kickout"] forState:UIControlStateNormal];
        statusInfoView.btnKickout.hidden = YES;
        [self.view addSubview:statusInfoView.btnKickout];
        ++index;
    }
    
#if POD_PITU
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(packageDownloadProgress:) name:kMC_NOTI_ONLINEMANAGER_PACKAGE_PROGRESS object:nil];
#endif
    
    [_anchorToolbarView triggeValue];
    
    [self.view addSubview:self.liverManageView];
    //self.productView.hidden = YES;

//    [self.liverManageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(self.liverManageView.size);
//        make.left.mas_equalTo(self.view.mas_left);
//        make.right.mas_equalTo(self.view.mas_right);
//        make.bottom.mas_equalTo(self.view.mas_bottom).mas_offset(-(safeAreaInsetBottom() + 20));
//    }];
    
    [self reFrameAudienceToolBarView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillTerminate) name:UIApplicationWillTerminateNotification object:nil];
    
    [self installOrderInfoCycleCheck];
}

- (void)installOrderInfoCycleCheck {
    // 启动心跳，向业务服务器发送心跳请求
    @weakify(self)
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
    dispatch_source_set_event_handler(_timer, ^{
        @strongify(self)
        [self getUserLiveProgressOrderTotal];
    });
    dispatch_source_set_timer(_timer, dispatch_walltime(NULL, 0), 30 * NSEC_PER_SEC, 0);
    dispatch_resume(_timer);
}

///查询直播间订单统计信息
- (void)getUserLiveProgressOrderTotal {
    [self request:@"api/live/liveOrder/getUserLiveProgressOrderTotal" param:@{@"live_id":toStr(_liveInfo.slaRoomID)} completion:^(BOOL success, id object, NSString *error) {
        if(success) {
            NSString *order_num = [object valueForKeyPath:@"data.userLiveProgressOrderTotal.order_num"];
            NSString *cash_num = [object valueForKeyPath:@"data.userLiveProgressOrderTotal.cash_num"];
            
            [self.anchorToolbarView setOrderInfo:order_num payMoney:cash_num];
        }
    }];
}

- (void)applicationWillTerminate {
    [[TXLiteAVSDKManage shared] closeUserLiveProgress:nil];
}

- (void)reFrameAudienceToolBarView {
    CGFloat height = self.liverManageView.y-20;
    if(self.productView.hidden) {
        _anchorToolbarView.height = height;
    } else {
        _anchorToolbarView.height = self.productView.y - 20;
    }
    [_anchorToolbarView reframe];
}


- (void)initStatusInfoView: (int)index {
    CGFloat width = self.view.size.width;
    CGFloat height = self.view.size.height;
    
    TCStatusInfoView* statusInfoView = [[TCStatusInfoView alloc] init];
    statusInfoView.videoView = [[UIView alloc] initWithFrame:CGRectMake(width - VIDEO_VIEW_WIDTH - VIDEO_VIEW_MARGIN_RIGHT, height - VIDEO_VIEW_MARGIN_BOTTOM - VIDEO_VIEW_HEIGHT * index, VIDEO_VIEW_WIDTH, VIDEO_VIEW_HEIGHT)];
    [self.view addSubview:statusInfoView.videoView];
    
    statusInfoView.pending = false;
    [_statusInfoViewArray addObject:statusInfoView];
    _beginTime = [[NSDate date] timeIntervalSince1970];
}

- (void)viewDidDisappear:(BOOL)animated {
    _endTime = [[NSDate date] timeIntervalSince1970];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
#if !TARGET_IPHONE_SIMULATOR
#if !POD_PITU
    if (!_firstAppear) {
        //是否有摄像头权限
        AVAuthorizationStatus statusVideo = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if (statusVideo == AVAuthorizationStatusDenied) {
            [_anchorToolbarView closeVCWithError:kErrorMsgOpenCameraFailed Alert:YES Result:NO];
            return;
        }
        
        if (!_isPreviewing) {
            [_liveRoom startLocalPreview:YES view:_videoParentView];
            _isPreviewing = YES;
        }
    } else {
        _firstAppear = NO;
    }
#endif
#endif
    
}

- (void)startRtmp{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{@"title":_liveInfo.title,
                                                                 @"frontcover":_liveInfo.userinfo.frontcover,
                                                                 @"location":_liveInfo.userinfo.location,
                                                                 @"headpic":_liveInfo.userinfo.headpic,
                                                                 @"username":_liveInfo.userinfo.username,
                                                                 @"slaRoomID":toStr(_liveInfo.slaRoomID)
                                                                 } options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *roomInfo = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    __weak typeof(self) weakSelf = self;
    [self.liveRoom createRoom:[TXLiteAVSDKManage shared].userLiveModel.room_no roomInfo:roomInfo completion:^(int errCode, NSString *errMsg) {
        NSLog(@"createRoom: errCode[%d] errMsg[%@]", errCode, errMsg);
        dispatch_async(dispatch_get_main_queue(), ^{
            if (errCode == 0) {
                [self.liveRoom setEyeScaleLevel:self->_eye_level];
                [self.liveRoom setFaceScaleLevel:self->_face_level];
                [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
                
                NSDictionary* params = @{@"userid": self->_liveInfo.userid,
                                         @"title": self->_liveInfo.title,
                                         @"frontcover" : self->_liveInfo.userinfo.frontcover,
                                         @"location" : self->_liveInfo.userinfo.location,
                                         @"slaRoomID" : toStr(self->_liveInfo.slaRoomID),
                                         @"headpic":self->_liveInfo.userinfo.headpic,
                                         };
                [TCUtil asyncSendHttpRequest:@"upload_room" token:[TCAccountMgrModel sharedInstance].token params:params handler:^(int resultCode, NSString *message, NSDictionary *resultDict) {
                    if (resultCode != 200) {
                        NSLog(@"uploadRoom: errCode[%d] errMsg[%@]", errCode, errMsg);
                    }
                }];
            }  else if (errCode == 10036) {
                UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"您当前使用的云通讯账号未开通音视频聊天室功能，创建聊天室数量超过限额，请前往腾讯云官网开通【IM音视频聊天室】"
                                                                                    message:nil
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *action = [UIAlertAction actionWithTitle:@"去开通" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [weakSelf.anchorToolbarView closeVCWithError:[NSString stringWithFormat:@"%@%d", errMsg, errCode] Alert:YES Result:NO];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://buy.cloud.tencent.com/avc"]];
                }];
                UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [weakSelf.anchorToolbarView closeVCWithError:[NSString stringWithFormat:@"%@%d", errMsg, errCode] Alert:YES Result:NO];
                }];
                [controller addAction:action];
                [controller addAction:confirm];
                [self presentViewController:controller animated:YES completion:nil];
            } else {
                [weakSelf.anchorToolbarView closeVCWithError:[NSString stringWithFormat:@"%@%d", errMsg, errCode] Alert:YES Result:NO];
            }
        });
    }];
}

- (void)stopRtmp {
    if (self.liveRoom) {
        [self.liveRoom stopLocalPreview];
        [self.liveRoom exitRoom:^(int errCode, NSString *errMsg) {
            NSLog(@"exitRoom: errCode[%d] errMsg[%@]", errCode, errMsg);
        }];
    }
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)appendSystemMsg:(NSString *)msg {
    LiveRoomMsgModel *msgMode = [[LiveRoomMsgModel alloc] init];
    msgMode.type = LiveRoomMsgModeTypeSystem;
    msgMode.userMsg = msg;
    [_msgListView appendMsg:msgMode];
}


#pragma mark - MLVBLiveRoomDelegate
- (void)onDebugLog:(NSString *)msg {
    NSLog(@"onDebugLog:%@", msg);
}

- (void)onRoomDestroy:(NSString *)roomID {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"onRoomDestroy, roomID:%@", roomID);
        [self->_anchorToolbarView closeVCWithError:kErrorMsgPushClosed Alert:YES Result:YES];
    });
    
}

- (void)onError:(int)errCode errMsg:(NSString *)errMsg extraInfo:(NSDictionary *)extraInfo {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"onError:%d, %@", errCode, errMsg);
        if(errCode != 0){
            [self->_anchorToolbarView closeVCWithError:errMsg Alert:YES Result:YES];
        }
    });
}

- (void)onAnchorEnter:(MLVBAnchorInfo *)anchor {
    NSString* userID = anchor.userID;
    NSString* strPlayUrl = anchor.accelerateURL;
    if (userID == nil || strPlayUrl == nil) {
        return;
    }
    
    DebugLog(@"onReceiveMemberJoinNotify: userID = %@ playUrl = %@", userID, strPlayUrl);
    
    if ([_setLinkMemeber containsObject:userID] == NO) {
        return;
    }
    
    TCStatusInfoView * statusInfoView = [self getStatusInfoViewFrom:userID];
    if (statusInfoView == nil) {
        return;
    }
    statusInfoView.anchor = anchor;
    __weak typeof(self) weakSelf = self;
    [self.liveRoom startRemoteView:anchor view:statusInfoView.videoView onPlayBegin:^{
        statusInfoView.pending = NO;
        statusInfoView.btnKickout.hidden = NO;
        [statusInfoView stopLoading];
    } onPlayError:^(int errCode, NSString *errMsg) {
        [weakSelf.setLinkMemeber removeObject:userID];
        [statusInfoView stopPlay];
        [statusInfoView emptyPlayInfo];
        if (errMsg != nil && errMsg.length > 0) {
            [TCUtil toastTip:errMsg parentView:weakSelf.view];
        }
    } playEvent:nil];
}

- (void)onAnchorExit:(MLVBAnchorInfo *)anchorInfo {
    NSString* userID = anchorInfo.userID;
    TCStatusInfoView* statusInfoView = [self getStatusInfoViewFrom:userID];
    if (statusInfoView == nil) {
        DebugLog(@"onReceiveMemberExitNotify: invalid notify");
        return;
    }
    
    //混流：减少一路
    [self.liveRoom stopRemoteView:anchorInfo];
    [statusInfoView stopPlay];
    [statusInfoView emptyPlayInfo];
    [_setLinkMemeber removeObject:userID];
}


- (void)onRequestJoinAnchor:(MLVBAnchorInfo *)anchorInfo reason:(NSString *)reason {
    NSString *userID = anchorInfo.userID;

    if ([_setLinkMemeber count] >= MAX_LINKMIC_MEMBER_SUPPORT) {
        [TCUtil toastTip:@"主播端连麦人数超过最大限制" parentView:self.view];
        [self.liveRoom responseJoinAnchor:userID agree:NO reason:@"主播端连麦人数超过最大限制"];
    }
    else if (_userIdRequest.length > 0) {
        if (![_userIdRequest isEqualToString:userID]) {
            [TCUtil toastTip:@"请稍后，主播正在处理其它人的连麦请求" parentView:self.view];
            [self.liveRoom responseJoinAnchor:userID agree:NO reason:@"请稍后，主播正在处理其它人的连麦请求"];
        }
    }
    else {
        TCStatusInfoView * statusInfoView = [self getStatusInfoViewFrom:userID];
        if (statusInfoView){
            [self.liveRoom kickoutJoinAnchor:statusInfoView.userID];
            [_setLinkMemeber removeObject:statusInfoView.userID];
            [statusInfoView stopLoading];
            [statusInfoView stopPlay];
            [statusInfoView emptyPlayInfo];
        }
        _userIdRequest = userID;
        UIAlertView* _alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:[NSString stringWithFormat:@"%@向您发起连麦请求", userID]  delegate:self cancelButtonTitle:@"拒绝" otherButtonTitles:@"接受", nil];
        
        [_alertView show];
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(handleTimeOutRequest:) object:_alertView];
        [self performSelector:@selector(handleTimeOutRequest:) withObject:_alertView afterDelay:20];
    }
}

- (void)onRecvRoomTextMsg:(NSString *)roomID userID:(NSString *)userID userName:(NSString *)userName userAvatar:(NSString *)userAvatar message:(NSString *)message {
    IMUserAble* info = [IMUserAble new];
    info.imUserId = userID;
    info.imUserName = userName.length > 0? userName : userID;
    info.imUserIconUrl = userAvatar;
    info.cmdType = TCMsgModelType_NormalMsg;
    [_anchorToolbarView handleIMMessage:info msgText:message];
}

- (void)onRecvRoomCustomMsg:(NSString *)roomID userID:(NSString *)userID userName:(NSString *)userName userAvatar:(NSString *)userAvatar cmd:(NSString *)cmd message:(NSString *)message {
    IMUserAble* info = [IMUserAble new];
    info.imUserId = userID;
    info.imUserName = userName.length > 0? userName : userID;
    info.imUserIconUrl = userAvatar;
    info.cmdType = [cmd integerValue];
    [_anchorToolbarView handleIMMessage:info msgText:message];
}

/**
   主播收到PK请求
 */
- (void)onRequestRoomPK:(MLVBAnchorInfo *)anchor {
    if (_hasPendingRequest || _pkStatus != PKStatus_IDLE || _playerViewDic.count) {
        [_liveRoom responseRoomPK:anchor agree:NO reason:@"请稍后，主播正忙"];
        return;
    }
    
    _hasPendingRequest = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *msg = [NSString stringWithFormat:@"[%@]请求PK", anchor.userName];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"拒绝" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self->_hasPendingRequest = NO;
            [self->_liveRoom responseRoomPK:anchor agree:NO reason:@"主播不同意您的PK"];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"接受" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.pkAnchor = anchor;
            self->_hasPendingRequest = NO;
            [self->_liveRoom responseRoomPK:anchor agree:YES reason:nil];
            [self addPKView:anchor];
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self->_hasPendingRequest = NO;
            [alertController dismissViewControllerAnimated:NO completion:nil];
        });
    });
}

/**
   主播收到结束PK的请求
 */
- (void)onQuitRoomPK {
    [self deletePKView];
    [self toastTip:@"对方结束PK" time:2];
}

// 播放PK主播的画面
- (void)addPKView:(MLVBAnchorInfo *)anchor {
    LiveRoomAccPlayerView *playerView = [self accPlayerViewForUID:anchor.userID];
    [self.view addSubview:playerView];
    
    _pkStatus = PKStatus_BEING;  // PK中
    [_btnPK setImage:[UIImage imageNamed:@"mlvb_pk_stop_button"] forState:UIControlStateNormal];
    
    // 重新布局
    _roomCreatorListView.hidden = YES;
    [self relayout];
    [_liveRoom startRemoteView:anchor
                          view:playerView
                   onPlayBegin:^{
                       playerView.loading = NO;
                   } onPlayError:^(int errCode, NSString *errMsg) {
                       [self->_liveRoom quitRoomPK:nil];
                       [self deletePKView];
                   } playEvent:nil];
}

// 删除PK主播的画面
- (void)deletePKView {
    [_liveRoom stopRemoteView:self.pkAnchor];

    for (id userID in _playerViewDic) {
        UIView *playerView = [_playerViewDic objectForKey:userID];
        [playerView removeFromSuperview];
    }
    
    [_playerViewDic removeAllObjects];
    
    _pkStatus = PKStatus_IDLE;  // 空闲状态
    [_btnPK setImage:[UIImage imageNamed:@"mlvb_pk"] forState:UIControlStateNormal];
    [self relayout];
}

// 连麦模式，大主播踢掉小主播
- (void)clickKickoutBtn:(UIButton *)btn {
    NSString *userID = btn.titleLabel.text;
    [self kickout:userID]; // TODO: FIx
}

- (void)kickout:(NSString *)userID {
    [_liveRoom kickoutJoinAnchor:userID];
    [self removeAccViewForUID:userID];
    [self relayout];
}

- (void)alertTips:(NSString *)title msg:(NSString *)msg completion:(void(^)())completion {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (completion) {
                completion();
            }
        }]];
        
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
    });
}

#pragma mark- LinkMic Func
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (_userIdRequest != nil && _userIdRequest.length > 0) {
        if (buttonIndex == 0) {
            //拒绝连麦
            [self.liveRoom responseJoinAnchor:_userIdRequest agree:NO reason:@"主播拒绝了您的连麦请求"];
        }
        else if (buttonIndex == 1) {
            //接受连麦
            [self.liveRoom responseJoinAnchor:_userIdRequest agree:YES reason:nil];
            //查找空闲的TCLinkMicSmallPlayer, 开始loading
            for (TCStatusInfoView * statusInfoView in _statusInfoViewArray) {
                if (statusInfoView.userID == nil || statusInfoView.userID.length == 0) {
                    statusInfoView.pending = YES;
                    statusInfoView.userID = _userIdRequest;
                    [statusInfoView startLoading];
                    break;
                }
            }
            
            //设置超时逻辑
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(onLinkMicTimeOut:) object:_userIdRequest];
            [self performSelector:@selector(onLinkMicTimeOut:) withObject:_userIdRequest afterDelay:20];
            
            //加入连麦成员列表
            [_setLinkMemeber addObject:_userIdRequest];
        }
    }
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(handleTimeOutRequest:) object:alertView];
    _userIdRequest = @"";
}

- (void)onLinkMicTimeOut:(NSString*)userID {
    if (userID) {
        TCStatusInfoView* statusInfoView = [self getStatusInfoViewFrom:userID];
        if (statusInfoView && statusInfoView.pending == YES){
            [self.liveRoom kickoutJoinAnchor:statusInfoView.userID];
            [_setLinkMemeber removeObject:userID];
            [statusInfoView stopPlay];
            [statusInfoView emptyPlayInfo];
            [TCUtil toastTip: [NSString stringWithFormat: @"%@连麦超时", userID] parentView:self.view];
        }
    }
}

- (void)handleTimeOutRequest:(UIAlertView*)alertView {
    _userIdRequest = @"";
    if (alertView) {
        [alertView dismissWithClickedButtonIndex:0 animated:NO];
    }
}

- (NSString*) getLinkMicSessionID {
    //说明：
    //1.sessionID是混流依据，sessionID相同的流，后台混流Server会混为一路视频流；因此，sessionID必须全局唯一
    
    //2.直播码频道ID理论上是全局唯一的，使用直播码作为sessionID是最为合适的
    //NSString* strSessionID = [TCLinkMicModel getStreamIDByStreamUrl:self.rtmpUrl];
    
    //3.直播码是字符串，混流Server目前只支持64位数字表示的sessionID，暂时按照下面这种方式生成sessionID
    //  待混流Server改造完成后，再使用直播码作为sessionID
    
    UInt64 timeStamp = [[NSDate date] timeIntervalSince1970] * 1000;
    
    UInt64 sessionID = ((UInt64)3891 << 48 | timeStamp); // 3891是bizid, timeStamp是当前毫秒值
    
    return [NSString stringWithFormat:@"%llu", sessionID];
}

- (TCStatusInfoView *)getStatusInfoViewFrom:(NSString*)userID {
    if (userID) {
        for (TCStatusInfoView* statusInfoView in _statusInfoViewArray) {
            if ([userID isEqualToString:statusInfoView.userID]) {
                return statusInfoView;
            }
        }
    }
    return nil;
}

- (void)clickBtnKickout:(UIButton *)btn {
    for (TCStatusInfoView* statusInfoView in _statusInfoViewArray) {
        if (statusInfoView.btnKickout == btn) {
            [self.liveRoom kickoutJoinAnchor:statusInfoView.userID];
            [_setLinkMemeber removeObject:statusInfoView.userID];
            [statusInfoView stopPlay];
            [statusInfoView emptyPlayInfo];
            break;
        }
    }
}

- (LiveRoomAccPlayerView *)accPlayerViewForUID:(NSString *)uid {
    LiveRoomAccPlayerView *view = _playerViewDic[uid];
    if (view == nil) {
        view = [[LiveRoomAccPlayerView alloc] initWithFrame:self.view.bounds];
        [view setBackgroundColor:UIColorFromRGB(0x262626)];
        _playerViewDic[uid] = view;
    }
    return view;
}

- (void)removeAccViewForUID:(NSString *)uid {
    LiveRoomAccPlayerView *view = _playerViewDic[uid];
    if (view) {
        [view removeFromSuperview];
        [_playerViewDic removeObjectForKey:uid];
    }
}

- (void)relayout {
    // 重新布局小主播的画面
    int index = 0;
    int originX = self.view.width - 110;
    int originY = NavigationBar_Bottom+34+54;//self.view.height - 250;
    int videoViewWidth = 100;
    int videoViewHeight = 150;
    
    // 区分PK和普通直播(连麦)
    if (_pkStatus == PKStatus_BEING) {
        originX = self.view.width / 2.0;
        //originY = 0;
        videoViewWidth = self.view.width / 2.0;
        videoViewHeight = videoViewWidth * 16.0 / 9.0;
        _videoParentView.frame = CGRectMake(0, originY, videoViewWidth, videoViewHeight); // 9:16
        _msgListView.frame = CGRectMake(10,
                                        videoViewHeight + 6,
                                        self.view.width - 20,
                                        self.view.height - videoViewHeight - self.view.width / 10 - 30);
 
    } else {
        _videoParentView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
        _msgListView.frame = CGRectMake(10, self.view.height/3, 300, self.view.height/2);
    }
    
    for (id userID in _playerViewDic) {
        UIView *playerView = [_playerViewDic objectForKey:userID];
        playerView.frame = CGRectMake(originX, originY/*originY - videoViewHeight * index*/, videoViewWidth, videoViewHeight);
        ++ index;
    }
}

- (void)_onEnterRoom {
    _vBeauty.actionPerformer = [TCBeautyPanelActionProxy proxyWithSDKObject:_liveRoom];
}

//- (void)initRoomLogic {
//    [_liveRoom createRoom:@"" roomInfo:_roomName completion:^(int errCode, NSString *errMsg) {
//        NSLog(@"createRoom: errCode[%d] errMsg[%@]", errCode, errMsg);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            if (errCode == 0) {
//                [self appendSystemMsg:@"连接成功"];
//            } else if (errCode == 10036) {
//                UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"您当前使用的云通讯账号未开通音视频聊天室功能，创建聊天室数量超过限额，请前往腾讯云官网开通【IM音视频聊天室】"
//                                                                                    message:nil
//                                                                             preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *action = [UIAlertAction actionWithTitle:@"去开通" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                    [self.navigationController popViewControllerAnimated:YES];
//                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://buy.cloud.tencent.com/avc"]];
//                }];
//                UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                    [self.navigationController popViewControllerAnimated:YES];
//                }];
//                [controller addAction:action];
//                [controller addAction:confirm];
//                [self presentViewController:controller animated:YES completion:nil];
//            } else {
//                [self alertTips:@"创建直播间失败" msg:errMsg completion:^{
//                    [self.navigationController popViewControllerAnimated:YES];
//                }];
//            }
//        });
//
//    }];
//}

// 聊天
- (void)clickChat:(UIButton *)btn {
    [_msgInputTextField becomeFirstResponder];
}

#pragma mark - AudioEffectViewDelegate
-(void)onEffectViewHidden:(BOOL)isHidden {
    self.liverManageView.hidden = !isHidden;
    [_anchorToolbarView setButtonHidden:!isHidden];
}

#pragma mark -  TCAnchorToolbarDelegate

- (void)closeRTMP {
    for (TCStatusInfoView* statusInfoView in _statusInfoViewArray) {
        [statusInfoView stopPlay];
    }
    [self stopRtmp];
}

- (void)closeVC {
    [MXAlertViewHelper showAlertViewWithMessage:nil title:@"一大波观众正在赶来，确定要关闭直播吗？" okTitle:@"我要关闭" cancelTitle:@"等等Ta们" baseVC:self completion:^(BOOL cancelled, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [[TXLiteAVSDKManage shared] closeUserLiveProgress:^(id data) {
                if([data boolValue]) {
                    [self showLiveResultView];
                }
            }];
        }
    }];
}

- (void)showLiveResultView {
    self.overView.liveID = toStr(_liveInfo.slaRoomID);
    [self.view addSubview:self.overView];
}

- (void)clickScreen:(UITapGestureRecognizer *)gestureRecognizer {
    _anchorToolbarView.vBeauty.hidden = YES;
    [_anchorToolbarView setButtonHidden:NO];
    self.liverManageView.hidden = NO;
    
    //手动聚焦
    CGPoint touchLocation = [gestureRecognizer locationInView:_videoParentView];;
    [self.liveRoom setFocusPosition:touchLocation];
}

- (void)clickCamera:(UIButton *)button {
    _camera_switch = !_camera_switch;
#if POD_PITU
    [self.liveRoom setMirror:!_camera_switch];
#endif
    [self.liveRoom switchCamera];
}

- (void)clickBeauty:(UIButton *)button {
    _anchorToolbarView.vBeauty.hidden = NO;
    [_anchorToolbarView setButtonHidden:YES];
}

- (void)clickMusic:(UIButton *)button {
    [_anchorToolbarView.vMusicPanel show];
}

- (void)clickTorch:(UIButton *)button {
    _torch_switch = !_torch_switch;
    BOOL result = [self.liveRoom enableTorch:_torch_switch];
    if (!result) {
         _torch_switch = !_torch_switch;
        return;
    }
    if (_torch_switch == YES) {
        [_anchorToolbarView.btnTorch setImage:[UIImage imageNamed:@"flash_hover"] forState:UIControlStateNormal];
    }
    else {
        [_anchorToolbarView.btnTorch setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateNormal];
    }
}

- (void)clickLog:(UIButton *)button {
    for (TCStatusInfoView * statusInfoView in _statusInfoViewArray) {
        [statusInfoView showLogView:self.log_switch];
    }
    _log_switch = !_log_switch;
    [self.liveRoom showVideoDebugLog:_log_switch];
}

- (void)clickMusicSelect:(UIButton *)button {
    //创建播放器控制器
    MPMediaPickerController *mpc = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeAnyAudio];
    mpc.delegate = self;
    mpc.editing = YES;
    [self presentViewController:mpc animated:YES completion:nil];
}

- (void)clickMusicClose:(UIButton *)button {
    [_anchorToolbarView.vMusicPanel hide];
    [self.liveRoom stopBGM];
}

- (void)clickVolumeSwitch:(UIButton *)button {
    // todo
}

- (void)sliderValueChange:(UISlider *)obj {
    if (obj.tag == 0) { //美颜
        _beauty_level = obj.value;
        [self.liveRoom setBeautyStyle:0 beautyLevel:_beauty_level whitenessLevel:_whitening_level ruddinessLevel:_ruddiness_level];
    } else if (obj.tag == 1) { //美白
        _whitening_level = obj.value;
        [self.liveRoom setBeautyStyle:0 beautyLevel:_beauty_level whitenessLevel:_whitening_level ruddinessLevel:_ruddiness_level];
    } else if (obj.tag == 2) { //大眼
        _eye_level = obj.value;
        [self.liveRoom setEyeScaleLevel:_eye_level];
    } else if (obj.tag == 3) { //瘦脸
        _face_level = obj.value;
        [self.liveRoom setFaceVLevel:_face_level];
    } else if (obj.tag == 4) {// 背景音乐音量
        _bgmVolume = obj.value/obj.maximumValue;
        [self.liveRoom setBGMVolume:(obj.value/obj.maximumValue)];
    } else if (obj.tag == 5) { // 麦克风音量
        _micVolume = obj.value/obj.maximumValue;
        [self.liveRoom setMicVolume:(obj.value/obj.maximumValue)];
    } else if (obj.tag == 6) { // bgm变调
        _bgmPitch =  obj.value/obj.maximumValue*2-1;
        [self.liveRoom setBGMPitch:_bgmPitch];
    } else if (obj.tag == 7) { // bgm seek
        _bgmPosition = obj.value/obj.maximumValue;
        [self.liveRoom setBGMPosition:_bgmPosition];
    }
}

- (void)sliderValueChangeEx:(UISlider*)obj {
    // todo
}

- (void)selectEffect:(NSInteger)index {
    [self.liveRoom setReverbType:index];
}

- (void)selectEffect2:(NSInteger)index {
    [self.liveRoom setVoiceChangerType:index];
}

- (void)requestPK:(NSInteger)index {
    [self.liveRoom requestRoomPK:(UDetail.user.nickname/*@"alphago2"*/) completion:^(int errCode, NSString *errMsg, NSString *streamUrl) {
        
    }];
}

- (void)clickAudienceList {
    self.viewerView.liveRoom = self.liveRoom;
    self.viewerView.liveInfo = _liveInfo;
    [self.viewerView fetchData];
    [self.viewerPanVC show];
}

#pragma mark - 特效设置
#if POD_PITU
//#warning step 1.3 切换动效素材
//- (void)motionTmplSelected:(NSString *)materialID {
//    if (materialID == nil) {
//        [MCTip hideText];
//    }
//    if ([MaterialManager isOnlinePackage:materialID]) {
//        [self.liveRoom selectMotionTmpl:materialID inDir:[MaterialManager packageDownloadDir]];
//    } else {
//        NSString *localPackageDir = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Resource"];
//        [self.liveRoom selectMotionTmpl:materialID inDir:localPackageDir];
//    }
//}
//
//- (void)packageDownloadProgress:(NSNotification *)notification {
//    if ([[notification object] isKindOfClass:[NSDictionary class]]) {
//        NSDictionary *progressDic = [notification object];
//        CGFloat progress = [progressDic[kMC_USERINFO_ONLINEMANAGER_PACKAGE_PROGRESS] floatValue];
//        if (progress <= 0.f) {
//            [MCTip showText:@"素材下载失败" inView:self.view afterDelay:2.f];
//        }
//    }
//}
#endif

#pragma mark - BGM
//选中后调用
- (void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection{
    NSArray *items = mediaItemCollection.items;
    MPMediaItem *item = [items objectAtIndex:0];
    
    NSURL *url = [item valueForProperty:MPMediaItemPropertyAssetURL];
    NSLog(@"MPMediaItemPropertyAssetURL = %@", url);
    
    if (mediaPicker.editing) {
        mediaPicker.editing = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.liveRoom stopBGM];
            [self saveAssetURLToFile: url];
        });
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

//点击取消时回调
- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 将AssetURL(音乐)导出到app的文件夹并播放
- (void)saveAssetURLToFile:(NSURL *)assetURL {
    AVURLAsset *songAsset = [AVURLAsset URLAssetWithURL:assetURL options:nil];
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:songAsset presetName:AVAssetExportPresetAppleM4A];
    NSLog (@"created exporter. supportedFileTypes: %@", exporter.supportedFileTypes);
    exporter.outputFileType = @"com.apple.m4a-audio";
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *exportFile = [docDir stringByAppendingPathComponent:@"exported.m4a"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportFile]) {
        [[NSFileManager defaultManager] removeItemAtPath:exportFile error:nil];
    }
    exporter.outputURL = [NSURL fileURLWithPath:exportFile];
    
    __weak __typeof(self) weakSelf = self;
    // do the export
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        int exportStatus = exporter.status;
        switch (exportStatus) {
            case AVAssetExportSessionStatusFailed: {
                NSLog (@"AVAssetExportSessionStatusFailed: %@", exporter.error);
                break;
            }
            case AVAssetExportSessionStatusCompleted: {
                NSLog(@"AVAssetExportSessionStatusCompleted: %@", exporter.outputURL);
                
                // 播放背景音乐
                dispatch_async(dispatch_get_main_queue(), ^{
                    __strong __typeof(weakSelf) self = weakSelf;
                    if (self == nil) {
                        return;
                    }
                    // _logicView.vMusicPanel.hidden = NO;   // 暂时不加这两个按钮
                    [self.liveRoom playBGM:exportFile];
                    [self.liveRoom setBGMVolume:self->_bgmVolume];
                    [self.liveRoom setMicVolume:self->_micVolume];
                });
                break;
            }
            case AVAssetExportSessionStatusUnknown: { NSLog (@"AVAssetExportSessionStatusUnknown"); break;}
            case AVAssetExportSessionStatusExporting: { NSLog (@"AVAssetExportSessionStatusExporting"); break;}
            case AVAssetExportSessionStatusCancelled: { NSLog (@"AVAssetExportSessionStatusCancelled"); break;}
            case AVAssetExportSessionStatusWaiting: { NSLog (@"AVAssetExportSessionStatusWaiting"); break;}
            default: { NSLog (@"didn't get export status"); break;}
        }
    }];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    _msgInputTextField.text = @"";
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _msgInputTextField.text = textField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString *textMsg = [textField.text stringByTrimmingCharactersInSet:[NSMutableCharacterSet whitespaceCharacterSet]];
    if (textMsg.length <= 0) {
        textField.text = @"";
        //[self alertTips:@"提示" msg:@"消息不能为空" completion:nil];
        return YES;
    }
    
    LiveRoomMsgModel *msgMode = [[LiveRoomMsgModel alloc] init];
    msgMode.type = LiveRoomMsgModeTypeOther;
    msgMode.time = [[NSDate date] timeIntervalSince1970];
    msgMode.userName = _userName;
    msgMode.userMsg = textMsg;
    
    [_msgListView appendMsg:msgMode];
    
    _msgInputTextField.text = @"";
    [_msgInputTextField resignFirstResponder];
    
    // 发送
    [_liveRoom sendRoomTextMsg:textMsg completion:nil];
    
    return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [_msgInputTextField resignFirstResponder];
//    _vBeauty.hidden = YES;
//    _roomCreatorListView.hidden = YES;
//    [self hideToolButtons:NO];
//
//    _touchBeginLocation = [[[event allTouches] anyObject] locationInView:self.view];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    CGPoint location = [[[event allTouches] anyObject] locationInView:self.view];
//    [self endMove:location.x - _touchBeginLocation.x];
}

// 滑动隐藏UI控件
- (void)endMove:(CGFloat)moveX {
    // 目前只需要隐藏消息列表控件
//    [UIView animateWithDuration:0.2 animations:^{
//        if (moveX > 10) {
//            for (UIView *view in self.view.subviews) {
//                if (![view isEqual:self->_msgListView]) {
//                    continue;
//                }
//
//                CGRect rect = view.frame;
//                if (rect.origin.x >= 0 && rect.origin.x < [UIScreen mainScreen].bounds.size.width) {
//                    rect = CGRectOffset(rect, self.view.width, 0);
//                    view.frame = rect;
//                }
//            }
//
//        } else if (moveX < -10) {
//            for (UIView *view in self.view.subviews) {
//                if (![view isEqual:self->_msgListView]) {
//                    continue;
//                }
//
//                CGRect rect = view.frame;
//                if (rect.origin.x >= [UIScreen mainScreen].bounds.size.width) {
//                    rect = CGRectOffset(rect, -self.view.width, 0);
//                    view.frame = rect;
//                }
//            }
//        }
//    }];
}

- (void)clickMsgListCoverView:(UITapGestureRecognizer *)gestureRecognizer {
    _msgListCoverView.hidden = YES;
    
    [_msgInputTextField resignFirstResponder];
    _vBeauty.hidden = YES;
    _roomCreatorListView.hidden = YES;
    [self hideToolButtons:NO];
}

- (void)hideToolButtons:(BOOL)bHide {
    _btnChat.hidden = bHide;
    _btnCamera.hidden = bHide;
    _btnBeauty.hidden = bHide;
    _btnMute.hidden = bHide;
    _btnLog.hidden = bHide;
    _msgListCoverView.hidden = !bHide;
}

#pragma mark - UITableViewDataSource

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"请选择主播";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _roomCreatorList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"RoomCreatorCellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.textLabel.font = [UIFont systemFontOfSize:15];
    }
    
    if (indexPath.row >= _roomCreatorList.count) {
        return cell;
    }
    
    MLVBAnchorInfo *pusherInfo = _roomCreatorList[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"昵称: %@", pusherInfo.userName];
    cell.backgroundColor = [UIColor blackColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row >= _roomCreatorList.count) {
        return;
    }
    
    _roomCreatorListView.hidden = !_roomCreatorListView.hidden;
    
    MLVBAnchorInfo *anchor = _roomCreatorList[indexPath.row];
    self.pkAnchor = anchor;
    [self sendPKRequest:anchor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 20;
}

- (void)sendPKRequest:(MLVBAnchorInfo *)anchor {
    if (anchor.userID == nil) {
        return;
    }
    
    if (_pkStatus == PKStatus_IDLE) {  // 空闲状态
        _pkStatus = PKStatus_REQUESTING;  // 请求PK中
        [self toastTip:@"PK请求已发出，等待对方的接受..." time:10];
        __weak __typeof(self) wself = self;
        [_liveRoom requestRoomPK:anchor.userID completion:^(int errCode, NSString *errMsg, NSString *streamUrl) {
            __strong __typeof(wself) self = wself;
            if (self == nil) {
                return;
            }
            if (errCode == 0) {
                self->_pkStatus = PKStatus_BEING; // PK中
                anchor.accelerateURL = streamUrl;
                [self addPKView:anchor];
                [self toastTip:@"对方接受了您的请求，开始PK" time:2];
            } else {
                self->_pkStatus = PKStatus_IDLE;  // 空闲状态
                [self toastTip:errMsg time:2];
            }
        }];
    }
}

/**
 @method 获取指定宽度width的字符串在UITextView上的高度
 @param textView 待计算的UITextView
 @param width 限制字符串显示区域的宽度
 @result float 返回的高度
 */
- (float)heightForString:(UITextView *)textView andWidth:(float)width {
    CGSize sizeToFit = [textView sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    return sizeToFit.height;
}

- (void)toastTip:(NSString *)toastInfo time:(NSInteger)time {
    dispatch_async(dispatch_get_main_queue(), ^{
        UITextView *toastView = self->_toastView;
        [toastView removeFromSuperview];
        
        CGRect frameRC = [[UIScreen mainScreen] bounds];
        frameRC.origin.y = frameRC.size.height - 110;
        frameRC.size.height -= 110;
        frameRC.size.height = [self heightForString:toastView andWidth:frameRC.size.width];
        
        toastView.frame = frameRC;
        
        toastView.text = toastInfo;
        toastView.backgroundColor = [UIColor whiteColor];
        toastView.alpha = 0.5;
        
        [self.view addSubview:toastView];
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, time * NSEC_PER_SEC);
        
        dispatch_after(popTime, dispatch_get_main_queue(), ^() {
            [toastView removeFromSuperview];
        });
    });
}

- (LiverManageView *)liverManageView {
    if (!_liverManageView) {
        _liverManageView = [LiverManageView new];
        _liverManageView.size = CGSizeMake(SCREEN_WIDTH, [LiverManageView viewHeight]);
        
        _liverManageView.x = 0;
        _liverManageView.y = self.view.height-(safeAreaInsetBottom() + (safeAreaInsetBottom() > 0 ? 0 : 20))-[LiverManageView viewHeight];
        @weakify(self)
        __weak LiverManageView *obj = _liverManageView;
        [_liverManageView.moreBtn addAction:^(UIButton *btn) {
            @strongify(self)
            //obj.hidden = YES;
            [self.liverMorePanVC show];
        }];
        
        [_liverManageView.beautifyBtn addAction:^(UIButton *btn) {
            @strongify(self)
            obj.hidden = YES;
            [self clickBeauty:nil];
        }];
        
        [_liverManageView.musicBtn addAction:^(UIButton *btn) {
            @strongify(self)
            obj.hidden = YES;
            [self clickMusic:nil];
        }];
        
        [_liverManageView.contactBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self requestPK:nil];
        }];
        [_liverManageView.goodsBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self.goodsPanVC show];
        }];
    }
    return _liverManageView;
}

///当选择商品弹窗消失后，需要将选择讲解的商品广播给观众
- (void)sendBroadcastProduct {
    static LiveGoodsObj *lastSelectedObj = nil;
    if(lastSelectedObj == self->_manageGoodsView.selectGoodObj) {
        return;
    }
    lastSelectedObj = self->_manageGoodsView.selectGoodObj;
    
    //同步讲解商品到观众端
    LiveGoodsObj *newObj = [LiveGoodsObj new];
    newObj.ID = self->_manageGoodsView.selectGoodObj.ID;
    newObj.goods_name = self->_manageGoodsView.selectGoodObj.goods_name;
    newObj.goods_show = self->_manageGoodsView.selectGoodObj.goods_show;
    newObj.goods_market_cash = self->_manageGoodsView.selectGoodObj.goods_market_cash;
    [self.anchorToolbarView broadcastProduct:newObj];
    
    [self.productView updateData:newObj];
    [self reFrameAudienceToolBarView];
}

///通知后台讲解商品变化，成功后广播观众
- (void)explainLiveGoods {
    [self request:@"api/live/userLiveGoods/explainLiveGoods" param:@{@"anchor_user_id":UDetail.user.user_id,@"goods_id":self->_manageGoodsView.selectGoodObj.ID} completion:^(BOOL success, id object, NSString *error) {
        if(success) {
            [self sendBroadcastProduct];
        }
    }];
}

- (ManageLiveGoodsView *)manageGoodsView {
    if (!_manageGoodsView) {
        _manageGoodsView = [ManageLiveGoodsView new];
        _manageGoodsView.size = CGSizeMake(SCREEN_WIDTH, [ManageLiveGoodsView viewHeight]);
        @weakify(self)
        [_manageGoodsView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.goodsPanVC dismiss];
            //self.liverManageView.goodsBtn.badge.badgeText = StrF(@"%zd", self->_manageGoodsView.selectCount);
            
            [self explainLiveGoods];
        }];
    }
    return _manageGoodsView;
}

- (HWPanModelVC *)goodsPanVC {
    if (!_goodsPanVC) {
        _goodsPanVC = [HWPanModelVC showInVC:self customView:self.manageGoodsView];
        @weakify(self)
        _goodsPanVC.willDismissblock = ^(id data) {
            @strongify(self)
            //self.liverManageView.goodsBtn.badge.badgeText = StrF(@"%zd", self.manageGoodsView.selectCount);
            [self explainLiveGoods];
        };
    }
    return _goodsPanVC;
}

- (LiverMoreView *)liverMoreView {
    if (!_liverMoreView) {
        _liverMoreView = [LiverMoreView new];
        _liverMoreView.size = CGSizeMake(SCREEN_WIDTH, [LiverMoreView viewHeight]);
        @weakify(self)
        [_liverMoreView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.liverMorePanVC dismiss];
        }];
        
        [_liverMoreView.switchBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self.liverMorePanVC dismiss];
            [self clickCamera:nil];
        }];
        
        [_liverMoreView.notiBtn addAction:^(UIButton *btn) {
            btn.selected = !btn.selected;
        }];
        
        [_liverMoreView.giftBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self.liverMorePanVC dismiss];
            [self.giftPanVC show];
        }];
        
        [_liverMoreView.contentBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self.liverMorePanVC dismiss];
            [self.contectLiverPanVC show];
        }];
        
        [_liverMoreView.stopBtn addAction:^(UIButton *btn) {
            @strongify(self)
            [self.liverMorePanVC dismiss];
            static BOOL isPublishing = YES;
            if(isPublishing) {
                isPublishing = NO;
                [[MLVBLiveRoom sharedInstance] pausePush];
            } else {
                isPublishing = YES;
                [[MLVBLiveRoom sharedInstance] resumePush];
            }
        }];
    }
    return _liverMoreView;
}

- (HWPanModelVC *)liverMorePanVC {
    if (!_liverMorePanVC) {
        _liverMorePanVC = [HWPanModelVC showInVC:self customView:self.liverMoreView];
        @weakify(self)
        _liverMorePanVC.willDismissblock = ^(id data) {
            @strongify(self)
            self.liverManageView.hidden = NO;
        };
    }
    return _liverMorePanVC;
}

- (OnLineViewerView *)viewerView {
    if (!_viewerView) {
        _viewerView = [OnLineViewerView new];
        _viewerView.size = CGSizeMake(SCREEN_WIDTH, [OnLineViewerView viewHeight]);
        @weakify(self)
        [_viewerView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.viewerPanVC dismiss];
        }];
    }
    return _viewerView;
}

- (HWPanModelVC *)viewerPanVC {
    if (!_viewerPanVC) {
        _viewerPanVC = [HWPanModelVC showInVC:self customView:self.viewerView];
    }
    return _viewerPanVC;
}

- (MyReceivedGiftView *)giftView {
    if (!_giftView) {
        _giftView = [MyReceivedGiftView new];
        _giftView.size = CGSizeMake(SCREEN_WIDTH, [MyReceivedGiftView viewHeight]);
        @weakify(self)
        [_giftView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.viewerPanVC dismiss];
        }];
    }
    return _giftView;
}

- (HWPanModelVC *)giftPanVC {
    if (!_giftPanVC) {
        _giftPanVC = [HWPanModelVC showInVC:[[MXRouter sharedInstance] getTopNavigationController] customView:self.giftView];
    }
    return _giftPanVC;
}

- (ContectLiverListView *)contectLiverView {
    if (!_contectLiverView) {
        _contectLiverView = [ContectLiverListView new];
        _contectLiverView.size = CGSizeMake(SCREEN_WIDTH, [ContectLiverListView viewHeight]);
        @weakify(self)
        [_contectLiverView.closeImgView addAction:^(UIView *view) {
            @strongify(self)
            [self.contectLiverPanVC dismiss];
        }];
    }
    return _contectLiverView;
}

- (HWPanModelVC *)contectLiverPanVC {
    if (!_contectLiverPanVC) {
        _contectLiverPanVC = [HWPanModelVC showInVC:[[MXRouter sharedInstance] getTopNavigationController] customView:self.contectLiverView];
    }
    return _contectLiverPanVC;
}


- (ProductView *)productView{
    if(!_productView){
        _productView = ({
            ProductView * object = [[ProductView alloc]initWithFrame:CGRectMake(7.6, self.liverManageView.y - 10 - 80, 240, 80)];
            object.hidden = YES;
            object;
       });
    }
    return _productView;
}

- (LiveOverView *)overView{
    if(!_overView){
        _overView = ({
            LiveOverView * object = [[LiveOverView alloc]initWithFrame:self.view.bounds];
            @weakify(self)
            object.closeBlock = ^(id data) {
                @strongify(self)
                [self dismissViewControllerAnimated:YES completion:nil];
            };
            object;
       });
    }
    return _overView;
}
@end

