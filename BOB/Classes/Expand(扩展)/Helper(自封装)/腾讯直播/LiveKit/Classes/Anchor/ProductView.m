//
//  ProductView.m
//  BOB
//
//  Created by AlphaGo on 2020/12/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ProductView.h"

@interface ProductView ()
@property(nonatomic,strong) UIImageView *topLine;
@property(nonatomic,strong) UIImageView *imgView;
@property(nonatomic,strong) UILabel *nameLbl;
@property(nonatomic,strong) UILabel *moneyLbl;

@property(nonatomic,strong) LiveGoodsObj *goodsObj;
@end

@implementation ProductView
- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self addSubview:self.topLine];
        [self addSubview:self.imgView];
        [self addSubview:self.nameLbl];
        [self addSubview:self.moneyLbl];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    Block_Exec(self.clickBlock,@{@"goods_id":self.goodsObj.ID ?: @"", @"isLive":@(1)});
}

- (void)updateData:(LiveGoodsObj *)obj {
    self.goodsObj = obj;
    
    self.hidden = obj == nil;
    if(obj == nil) {
        return;
    }
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:obj.goods_show]];
    self.nameLbl.text = obj.goods_name;
    self.moneyLbl.text = StrF(@"¥%.2f",obj.goods_market_cash);
}

- (UIImageView *)imgView{
    if(!_imgView){
        _imgView = ({
            UIImageView * object = [[UIImageView alloc]init];
            CGFloat width = (self.height-5-10);
            object.frame = CGRectMake(5, 10, width, width);
            object.backgroundColor = [UIColor lightGrayColor];
            object;
       });
    }
    return _imgView;
}

- (UILabel *)nameLbl{
    if(!_nameLbl){
        _nameLbl = ({
            UILabel * object = [[UILabel alloc]init];
            object.frame = CGRectMake(self.imgView.maxX+8, self.imgView.y+8, self.width-(self.imgView.maxX+8)-5, 20);
            object.font = [UIFont systemFontOfSize:14 weight:UIFontWeightThin];
            object.textColor = [UIColor blackColor];
            object.text = @"";
            object;
       });
    }
    return _nameLbl;
}

- (UILabel *)moneyLbl{
    if(!_moneyLbl){
        _moneyLbl = ({
            UILabel * object = [[UILabel alloc]init];
            object.frame = CGRectMake(self.nameLbl.x, self.imgView.centerY+5, self.nameLbl.width, 20);
            object.font = [UIFont boldSystemFontOfSize:15];
            object.textColor = [UIColor blackColor];
            object.text = @"";
            object;
       });
    }
    return _moneyLbl;
}

- (UIImageView *)topLine{
    if(!_topLine){
        _topLine = ({
            UIImageView * object = [[UIImageView alloc]init];
            object.backgroundColor = [UIColor colorWithHexString:@"#F7746A"];
            object.frame = CGRectMake(0, 0, self.width, 5);
            object;
       });
    }
    return _topLine;
}
@end
