//
//  ProductView.h
//  BOB
//
//  Created by AlphaGo on 2020/12/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveGoodsObj.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductView : UIView
@property(nonatomic,strong) FinishedBlock clickBlock;

- (void)updateData:(LiveGoodsObj *)obj;
@end

NS_ASSUME_NONNULL_END
