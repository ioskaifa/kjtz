//
//  UserApplyAnchorModel.m
//  BOB
//
//  Created by AlphaGo on 2020/12/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserApplyAnchorModel.h"

@implementation UserApplyAnchorModel
- (NSString *)liveTimeString {
    NSInteger hour = _live_time/3600;
    NSInteger min = (_live_time-hour*3600)/60;
    NSInteger second = (_live_time-hour*3600-min*60);
    return StrF(@"%02ld:%02ld:%02ld",hour,min,second);
}
@end
