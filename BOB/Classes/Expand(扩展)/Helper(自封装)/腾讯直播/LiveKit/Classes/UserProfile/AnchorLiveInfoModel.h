//
//  AnchorLiveInfoModel.h
//  BOB
//
//  Created by AlphaGo on 2020/12/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface AnchorLiveInfoModel : BaseObject
@property (nonatomic , copy) NSString              * cover_photo;
@property (nonatomic , copy) NSString              * play_url;
@property (nonatomic , assign) NSInteger              end_time;
@property (nonatomic , copy) NSString              * cre_date;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , assign) NSInteger              fans_num;
@property (nonatomic , assign) NSInteger              start_time;
@property (nonatomic , copy) NSString              * cre_time;
@property (nonatomic , assign) NSInteger              user_id;
@property (nonatomic , copy) NSString              * nick_name;
@property (nonatomic , copy) NSString              * room_no;
@property (nonatomic , assign) NSInteger              _id;
///是否关注主播 0-否 1-是
@property (nonatomic , assign) NSInteger              is_follow;
@property (nonatomic , copy) NSString              * head_photo;
@property (nonatomic , copy) NSString              * status;
@end

NS_ASSUME_NONNULL_END
