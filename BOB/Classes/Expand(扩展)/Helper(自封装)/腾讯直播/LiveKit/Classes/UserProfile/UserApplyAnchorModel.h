//
//  UserApplyAnchorModel.h
//  BOB
//
//  Created by AlphaGo on 2020/12/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserApplyAnchorModel : BaseObject

@property (nonatomic , assign) NSInteger              view_num;
@property (nonatomic , assign) NSInteger              add_fans_num;
@property (nonatomic , assign) NSInteger              live_time;
@property (nonatomic , copy) NSString              * liveTimeString;
@property (nonatomic , copy) NSString              * nick_name;
@property (nonatomic , copy) NSString              * order_num;
@property (nonatomic , copy) NSString              * head_photo;
@property (nonatomic , copy) NSString              * cash_num;

@end

NS_ASSUME_NONNULL_END
