//
//  AnchorLiveInfoModel.m
//  BOB
//
//  Created by AlphaGo on 2020/12/23.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "AnchorLiveInfoModel.h"

@implementation AnchorLiveInfoModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"_id" : @"id"};
}
@end
