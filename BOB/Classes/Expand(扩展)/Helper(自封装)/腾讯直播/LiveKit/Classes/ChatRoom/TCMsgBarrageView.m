/**
 * Module: TCMsgBarrageView
 *
 * Function: 弹幕
 */

#import "TCMsgBarrageView.h"
#import "UIView+Additions.h"
#import <UIImageView+WebCache.h>
#import "TCUtil.h"
#import "ColorMacro.h"

#define MSG_ANIMATE_VIEW_SPACE    30
#define MSG_IMAGEVIEW_WIDTH       27
#define MSG_LABEL_FONT            18
#define MSG_ANIMATE_DURANTION     1  //默认一个弹幕View的运行速度为： 2*SCREEN_WIDTH/MSG_ANIMATE_DURANTION

@implementation TCMsgBarrageView
{
    NSMutableArray        *_unUsedAnimateViewArray;
    NSMutableArray        *_msgModelArray;
    CGFloat               _nextAnimateViewStartTime;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _unUsedAnimateViewArray = [NSMutableArray array];
        _msgModelArray   = [NSMutableArray array];
        _nextAnimateViewStartTime = 1;
        [self initUI];
    }
    return self;
}

- (void)initUI {
    UIView *view = [self creatAnimateView];
    [_unUsedAnimateViewArray addObject:view];
    
    [self startAnimation];
}

///创建弹幕视图
- (UIView *)creatAnimateView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH + MSG_ANIMATE_VIEW_SPACE, 0, 0, self.height)];
    
    UIImageView *headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, self.height-4, self.height-4)];
    headImageView.layer.cornerRadius = headImageView.width/2;
    headImageView.layer.masksToBounds = YES;
    
    UILabel *userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(headImageView.right + 2, 10, 0,14)];
    userNameLabel.backgroundColor = [UIColor clearColor];
    
    UILabel *userMsgLabel = [[UILabel alloc] initWithFrame:CGRectMake(userNameLabel.left, userNameLabel.bottom, 0, 14)];
    userMsgLabel.backgroundColor = [UIColor clearColor];
    
    //弹幕背景图
    UIImageView *backImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, self.height)];
    backImageView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    backImageView.layer.cornerRadius = self.height/2.0;
    //UIImage *image = [UIImage imageNamed:@"Barrage"];
    //UIImage *newImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(24, 35, 5, 10) resizingMode:UIImageResizingModeStretch];
    //backImageView.image = newImage;
    
    UIImageView *giftImgView = [[UIImageView alloc] init];
    
    UILabel *giftNumLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, self.height)];

    
    [view insertSubview:backImageView atIndex:0];
    [view insertSubview:headImageView atIndex:1];
    [view insertSubview:userNameLabel atIndex:2];
    [view insertSubview:userMsgLabel atIndex:3];
    [view insertSubview:giftImgView atIndex:4];
    [view insertSubview:giftNumLbl atIndex:5];
    [self addSubview:view];
    return view;
}

/**
 空心字体

 @param str 文本
 @param textColor 文本颜色
 @param textBorderColor 文本边框颜色
 @param strokeWidth 文件边框宽度
 @return 文本
 */
- (NSMutableAttributedString *)textHollow:(NSString *)str textColor:(UIColor *)textColor textBorderColor:(UIColor *)textBorderColor strokeWidth:(CGFloat)strokeWidth
{
    NSDictionary *dict = @{
                           NSStrokeColorAttributeName:textBorderColor,
                           NSStrokeWidthAttributeName : [NSNumber numberWithFloat:strokeWidth],
                           NSForegroundColorAttributeName:textColor,
                           NSFontAttributeName: [UIFont boldSystemFontOfSize:33]
                           };
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc] initWithString:str attributes:dict];
    return attribtStr;
}

- (void)startAnimation {
    if (_msgModelArray.count != 0) {
        TCMsgModel *msgModel = [_msgModelArray lastObject];
        if (_unUsedAnimateViewArray.count != 0) {
            UIView *view = [_unUsedAnimateViewArray lastObject];
            [_unUsedAnimateViewArray removeObject:view];

            [self animateView:view msg:msgModel];
            
        } else {
            UIView *view = [self creatAnimateView];
            [self animateView:view msg:msgModel];
        }
        [_msgModelArray removeObject:msgModel];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(_nextAnimateViewStartTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self startAnimation];
    });
}

- (void)stopAnimation {
    [self removeAllSubViews];
}

- (void)bulletNewMsg:(TCMsgModel *)msgModel {
    [_msgModelArray insertObject:msgModel atIndex:0];
}

- (void)animateView:(UIView *)aView msg:(TCMsgModel *)msgModel {
    UIView *newView = [self resetViewFrame:aView msg:msgModel];
    CGFloat duration =  MSG_ANIMATE_DURANTION *(SCREEN_WIDTH + newView.width + MSG_ANIMATE_VIEW_SPACE)/(2*SCREEN_WIDTH);
    _nextAnimateViewStartTime = duration*((newView.width + MSG_ANIMATE_VIEW_SPACE)/(SCREEN_WIDTH + newView.width + MSG_ANIMATE_VIEW_SPACE));
    _lastAnimateView = newView;
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGRect frame =  newView.frame;
        newView.frame = CGRectMake(30/*-frame.size.width*/, frame.origin.y, frame.size.width, frame.size.height);
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            newView.frame = CGRectMake(SCREEN_WIDTH + MSG_ANIMATE_VIEW_SPACE, 0, 0, self.height);
            [self->_unUsedAnimateViewArray insertObject:newView atIndex:0];
        });
    }];

}

//调整弹幕frame
- (UIView *)resetViewFrame:(UIView *)aView msg:(TCMsgModel *)msgModel {
    NSAttributedString *userName = [self getAttributedUserNameFromModel:msgModel];
    CGRect nameRect = [userName boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 14) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    NSAttributedString *userMsg = [self getAttributedUserMsgFromModel:msgModel];
    CGRect msgRect = [userMsg boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, self.height - 14) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
//    CGRect giftNumRect = [[self textHollow:StrF(@"x%@",msgModel.buyGiftNum) textColor:[UIColor colorWithHexString:@"#00DBD9"] textBorderColor:[UIColor blackColor] strokeWidth:-3] boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, self.height - 14) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    NSArray *viewArray = aView.subviews;
    if (viewArray.count >= 3) {
        UIImageView *headImageView = viewArray[1];
        [headImageView sd_setImageWithURL:[NSURL URLWithString:[TCUtil transImageURL2HttpsURL:msgModel.userHeadImageUrl]] placeholderImage:[UIImage imageNamed:@"default_user"]];
        
        UILabel *userNamelabel = viewArray[2];
        userNamelabel.attributedText = userName;
        userNamelabel.width = nameRect.size.width;
        
        UILabel *userMsgLabel = viewArray[3];
        userMsgLabel.attributedText = userMsg;
        userMsgLabel.width = msgRect.size.width;
        
        aView.width = headImageView.width + 4 + 10 + (userNamelabel.width > userMsgLabel.width ? userNamelabel.width : userMsgLabel.width);
        
        UIImageView *backImageView = viewArray[0];
        backImageView.width = aView.width;
        
        UIImageView *giftImgView = viewArray[4];
        if(msgModel.buyGiftImgUrl) {
            [giftImgView sd_setImageWithURL:[NSURL URLWithString:msgModel.buyGiftImgUrl]];
            giftImgView.x = aView.width;
            giftImgView.y = -20;
            giftImgView.width = 80;
            giftImgView.height = 80;
            giftImgView.hidden = NO;
        } else {
            giftImgView.hidden = YES;
        }
        
        UILabel *giftNumLbl = viewArray[5];
        if(msgModel.buyGiftNum) {
            giftNumLbl.attributedText = [self textHollow:StrF(@"x%@",msgModel.buyGiftNum) textColor:[UIColor colorWithHexString:@"#00DBD9"] textBorderColor:[UIColor blackColor] strokeWidth:-3];
            giftNumLbl.x = giftImgView.maxX;
            giftNumLbl.width = 80;
            giftNumLbl.hidden = NO;
        } else {
            giftNumLbl.hidden = YES;
        }
    }
    return aView;
}

- (NSAttributedString *)getAttributedUserNameFromModel:(TCMsgModel *)msgModel {
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] init];
    NSMutableAttributedString *userName = [[NSMutableAttributedString alloc] initWithString:msgModel.userName];
    [attribute appendAttributedString:userName];
    
    [attribute addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0,attribute.length)];
    [attribute addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0,userName.length)];
    return attribute;
}

- (NSAttributedString *)getAttributedUserMsgFromModel:(TCMsgModel *)msgModel {
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] init];
    NSMutableAttributedString *userMsg = [[NSMutableAttributedString alloc] initWithString:msgModel.userMsg];
    [attribute appendAttributedString:userMsg];
    
    [attribute addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0,attribute.length)];
    [attribute addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x89eede) range:NSMakeRange(0,userMsg.length)];;
    return attribute;
}
@end
