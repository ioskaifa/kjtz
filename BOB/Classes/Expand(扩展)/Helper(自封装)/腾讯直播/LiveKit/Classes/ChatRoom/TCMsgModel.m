/**
 * Module: TCMsgModel
 *
 * Function: 消息相关的定义
 */

#import "TCMsgModel.h"

@implementation IMUserAble

@end

@implementation TCMsgModel

- (NSString *)userMsg {
    if([self isGift]) {
        return StrF(@"送了%@个%@",self.buyGiftNum,self.buyGiftName);
    } else if([self isBuyProduct]) {
        return StrF(@"购买了%@",self.productName);
    } else if([self isBroadcastProduct]) {
        return StrF(@"主播开始讲解%@",self.liveGoodName);
    } else if([self isOrderInfo]) {
        return @"";
    } else if([self isIBuyedProduct]) {
        return @"";
    }
    return _userMsg;
}

- (NSString *)userName {
    if([self isBuyProduct]) {
        return self.consumer ?: @"";
    }
    return _userName;
}

#pragma mark - 礼物消息
- (BOOL)isGift {
    return [_userMsg hasPrefix:@"#gift:"];
}

- (NSString *)buyGiftNum {
    if([_userMsg hasPrefix:@"#gift:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:6]];
        return [dic valueForKeyPath:@"gift_buy_num"] ?: @"";
    } else {
        return nil;
    }
}

- (NSString *)buyGiftName {
    if([_userMsg hasPrefix:@"#gift:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:6]];
        return [dic valueForKeyPath:@"gift_name"] ?: @"礼物";
    } else {
        return nil;
    }
}

- (NSString *)buyGiftImgUrl {
    if([_userMsg hasPrefix:@"#gift:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:6]];
        return [dic valueForKeyPath:@"gift_show"] ?: @"礼物";
    } else {
        return nil;
    }
}

#pragma mark - 购买商品
- (BOOL)isBuyProduct {
    return [_userMsg hasPrefix:@"#buyProduct:"];
}

- (NSString *)productName {
    if([_userMsg hasPrefix:@"#buyProduct:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:12]];
        return [dic valueForKeyPath:@"productName"] ?: @"";
    } else {
        return @"";
    }
}

- (NSString *)consumer {
    if([_userMsg hasPrefix:@"#buyProduct:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:12]];
        return [dic valueForKeyPath:@"consumer"] ?: @"";
    } else {
        return @"";
    }
}

#pragma mark - 通知主播我购买了商品,仅仅通知主播，让主播去广播给所有关注该条消息
- (BOOL)isIBuyedProduct {
    return [_userMsg hasPrefix:@"#ibuyedProduct:"];
}

- (NSString *)iBuyedProductName {
    if([_userMsg hasPrefix:@"#ibuyedProduct:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:15]];
        return [dic valueForKeyPath:@"iBuyedProductName"] ?: @"";
    } else {
        return @"";
    }
}

- (NSString *)myName {
    if([_userMsg hasPrefix:@"#ibuyedProduct:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:15]];
        return [dic valueForKeyPath:@"myName"] ?: @"";
    } else {
        return @"";
    }
}


#pragma mark - 讲解商品
- (BOOL)isBroadcastProduct {
    return [_userMsg hasPrefix:@"#liveGood:"];
}

- (NSString *)liveID {
    if([_userMsg hasPrefix:@"#liveGood:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:10]];
        return [dic valueForKeyPath:@"id"] ?: @"";
    } else {
        return nil;
    }
}

- (NSString *)liveGoodImgUrl {
    if([_userMsg hasPrefix:@"#liveGood:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:10]];
        return [dic valueForKeyPath:@"goods_show"] ?: @"";
    } else {
        return nil;
    }
}

- (NSString *)liveGoodName {
    if([_userMsg hasPrefix:@"#liveGood:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:10]];
        return [dic valueForKeyPath:@"goods_name"] ?: @"";
    } else {
        return nil;
    }
}

- (NSString *)liveGoodPrice {
    if([_userMsg hasPrefix:@"#liveGood:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:10]];
        return StrF(@"%.2f",[[dic valueForKeyPath:@"goods_market_cash"] doubleValue]);
    } else {
        return nil;
    }
}

#pragma mark - 订单信息
- (BOOL)isOrderInfo {
    return [_userMsg hasPrefix:@"#orderInfo:"];
}

- (NSString *)payCount {
    if([_userMsg hasPrefix:@"#orderInfo:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:11]];
        return [dic valueForKeyPath:@"orderCount"] ?: @"0";
    } else {
        return @"0";
    }
}

- (NSString *)payMoney {
    if([_userMsg hasPrefix:@"#orderInfo:"]) {
        NSDictionary *dic = [MXJsonParser jsonToDictionary:[_userMsg substringFromIndex:11]];
        return [dic valueForKeyPath:@"orderMoney"] ?: @"0";
    } else {
        return @"0";
    }
}
@end


