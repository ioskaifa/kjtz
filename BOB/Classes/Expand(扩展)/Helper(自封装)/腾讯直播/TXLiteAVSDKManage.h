//
//  TXLiteAVSDKManage.h
//  BOB
//
//  Created by AlphaGo on 2020/11/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <TXLiteAVSDK_Smart/TXLiteAVSDK.h>
#import "UserLiveProgressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TXLiteAVSDKManage : NSObject

+(instancetype)shared;

@property(nonatomic,strong) UserLiveProgressModel *userLiveModel;

///注册小直播后台
- (void)registerAccount:(NSString *)account password:(NSString *)password block:(FinishedBlock)block;

///申请开播
- (void)applyLiveProgress:(NSDictionary *)param finish:(FinishedBlock)block;

- (void)closeUserLiveProgress:(FinishedBlock)block;

///启动推流
- (void)startPush;

///结束推流
- (void)stopPush;

///切换前后摄像头
- (void)switchCamera;

///展示美颜工具条
- (void)showBeautyPanel;

///隐藏美颜工具条
- (void)hideBeautyPanel;

///启动预览
- (void)startPreview:(UIView *)baseView;

///停止预览
- (void)stopPreview;
@end

NS_ASSUME_NONNULL_END
