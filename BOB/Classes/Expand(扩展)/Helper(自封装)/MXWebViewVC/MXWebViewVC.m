//
//  MXWebViewVC.m
//  BOB
//
//  Created by mac on 2020/8/8.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MXWebViewVC.h"
#import <WebKit/WebKit.h>

@interface MXWebViewVC ()<WKUIDelegate,WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UIProgressView *progress;

@end

@implementation MXWebViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self loadUrl];
}

- (void)dealloc {
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.webView removeObserver:self forKeyPath:@"title"];
}

- (void)loadUrl {
    if ([StringUtil isEmpty:self.urlString]) {
        return;
    }
    NSURL *url = [NSURL URLWithString:self.urlString];
    if (!url) {
        return;
    }
    if (self.title) {
        [self setNavBarTitle:self.title];
    }
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:urlRequest];
    //监听进度条
    [self.webView addObserver:self
                   forKeyPath:@"estimatedProgress"
                      options:NSKeyValueObservingOptionNew
                      context:NULL];
    //监听title
    [self.webView addObserver:self
                   forKeyPath:@"title"
                      options:NSKeyValueObservingOptionNew
                      context:NULL];
}

#pragma mark KVO的监听代理
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if (object == self.webView) {
        ///进度条
        if ([keyPath isEqualToString:@"estimatedProgress"]){
            [self.progress setAlpha:1.0f];
                       [self.progress setProgress:self.webView.estimatedProgress animated:YES];
                       if(self.webView.estimatedProgress >= 1.0f) {
                           [UIView animateWithDuration:0.5f
                                                 delay:0.3f
                                               options:UIViewAnimationOptionCurveEaseOut
                                            animations:^{
                                                [self.progress setAlpha:0.0f];
                                            }
                                            completion:^(BOOL finished) {
                                                [self.progress setProgress:0.0f animated:NO];
                                            }];
                       }
        }
        
        if ([keyPath isEqualToString:@"title"]){
            if (self.webView.title) {                
                [self setNavBarTitle:self.webView.title];
            }
        }
    }
}

#pragma mark - InitUI
- (void)createUI {
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.view addSubview:self.progress];
    [self.progress mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(2);
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(self.view);
        make.top.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (WKWebView *)webView {
    if (!_webView) {
        _webView = [WKWebView new];
        _webView.navigationDelegate = self;
    }
    return _webView;
}

- (UIProgressView *)progress
{
    if (!_progress) {
        _progress = [[UIProgressView alloc]initWithFrame:CGRectZero];
        _progress.tintColor = [UIColor colorWithHexString:@"#0088FF"];
        _progress.backgroundColor = [UIColor lightGrayColor];
    }
    return _progress;
}

@end
