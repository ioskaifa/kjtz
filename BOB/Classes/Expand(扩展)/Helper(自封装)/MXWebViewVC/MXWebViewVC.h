//
//  MXWebViewVC.h
//  BOB
//
//  Created by mac on 2020/8/8.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MXWebViewVC : BaseViewController

@property (nonatomic, copy) NSString *urlString;

@end

NS_ASSUME_NONNULL_END
