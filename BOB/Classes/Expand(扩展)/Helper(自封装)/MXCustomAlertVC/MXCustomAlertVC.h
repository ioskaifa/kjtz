//
//  MXCustomAlertVC.h
//  AdvertisingMaster
//
//  Created by mac on 2020/10/20.
//  Copyright © 2020 mac. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MXCustomAlertVC : BaseViewController

@property (nonatomic, copy) FinishedBlock confirmFinish;

@property (nonatomic, copy) FinishedBlock cancelFinish;

+ (instancetype)showInViewController:(UIViewController *)superVC contentView:(UIView *)contentView;

@end

NS_ASSUME_NONNULL_END
