//
//  MXCustomAlertVC.m
//  AdvertisingMaster
//
//  Created by mac on 2020/10/20.
//  Copyright © 2020 mac. All rights reserved.
//

#import "MXCustomAlertVC.h"

@interface MXCustomAlertVC ()

@property (nonatomic, strong) UIView *backGroundView;

@property (nonatomic, strong) UIView *contentView;

@end

@implementation MXCustomAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    if ([@"MXCustomAlertVCconfirm" isEqualToString:eventName]) {
        [self disMiss];
        if (self.confirmFinish) {
            self.confirmFinish(userInfo);
        }
    } else if ([@"MXCustomAlertVCCancel" isEqualToString:eventName]) {
        [self disMiss];
        if (self.cancelFinish) {
            self.cancelFinish(userInfo);
        }
    }
}

+ (instancetype)showInViewController:(UIViewController *)superVC contentView:(UIView *)contentView {
    MXCustomAlertVC *showVC = [MXCustomAlertVC new];
    showVC.contentView = contentView;
    if (superVC) {
        [superVC addChildViewController:showVC];
        [superVC.view addSubview:showVC.view];
        [showVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.insets(UIEdgeInsetsZero);
        }];
        [showVC show];
    }
    return showVC;
}

- (void)addContentViewAnimation {
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.calculationMode = kCAAnimationCubic;
    animation.values = @[@1.07,@1.06,@1.05,@1.03,@1.02,@1.01,@1.0];
    animation.duration = 0.2;
    [self.contentView.layer addAnimation:animation forKey:@"transform.scale"];
}

- (void)disMiss {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

#pragma mark - Private Method
- (void)show {
    self.backGroundView.alpha = 0.7;
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.contentView.alpha = 1;
    } completion:nil];
    
    [self addContentViewAnimation];
}

#pragma mark - InitUI
- (void)createUI {
    self.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.backGroundView];
    [self.view addSubview:self.contentView];
    
    [self layoutUI];
}

- (void)layoutUI {
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.contentView.size);
        make.center.mas_equalTo(self.view);
    }];
}

#pragma mark - Init
- (UIView *)backGroundView {
    if (!_backGroundView) {
        _backGroundView = [UIView new];
        _backGroundView.backgroundColor = [UIColor moBlack];
        _backGroundView.alpha = 0;
        @weakify(self);
        [_backGroundView addAction:^(UIView *view) {
            @strongify(self);
            [self disMiss];
        }];
    }
    return _backGroundView;
}

@end
