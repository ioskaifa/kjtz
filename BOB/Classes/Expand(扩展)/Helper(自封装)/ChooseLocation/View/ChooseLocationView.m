//
//  ChooseLocationView.m
//  ChooseLocation
//
//  Created by Sekorm on 16/8/22.
//  Copyright © 2016年 HY. All rights reserved.
//

#import "ChooseLocationView.h"
#import "AddressView.h"
#import "UIView+Frame.h"
#import "AddressTableViewCell.h"
#import "AddressItem.h"
#import "UIView+Utils.h"
#import "UIButton+ActionBlock.h"

#define HYScreenW [UIScreen mainScreen].bounds.size.width

static  CGFloat  const  kHYTopViewHeight = 40; //顶部视图的高度
static  CGFloat  const  kHYTopTabbarHeight = 30; //地址标签栏的高度
static  CGFloat  const  ChooseLocationViewHeight = 320; //整体的高度

@interface ChooseLocationView ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic,weak) AddressView * topTabbar;
@property (nonatomic,weak) UIScrollView * contentView;
@property (nonatomic,weak) UIView * underLine;
@property (nonatomic,strong) NSDictionary *dataSouce;

@property (nonatomic,strong) NSArray * provinceDataSouce;
@property (nonatomic,strong) NSDictionary * cityDataSouce;
@property (nonatomic,strong) NSDictionary * districtDataSouce;
@property (nonatomic,strong) NSArray *townshipDataSouce;

@property (nonatomic,strong) NSIndexPath *provinceSelectedIndexPath;
@property (nonatomic,strong) NSIndexPath *citySelectedIndexPath;
@property (nonatomic,strong) NSIndexPath *districtSelectedIndexPath;
@property (nonatomic,strong) NSIndexPath *townshipSelectedIndexPath;

@property (nonatomic,strong) NSMutableArray * tableViews;
@property (nonatomic,strong) NSMutableArray * topTabbarItems;
@property (nonatomic,strong) UIButton * selectedBtn;

@end

@implementation ChooseLocationView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

#pragma mark - TableViewDelegate
- (void)configTableViews:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath address:(NSArray *)addressArr {
    
    if([self.tableViews indexOfObject:tableView] == 0){
        self.provinceSelectedIndexPath = indexPath;
        //1.1 获取下一级别的数据源(市级别,如果是直辖市时,下级则为区级别)
        //AddressItem * provinceItem = self.dataSouce[indexPath.row];
        //self.cityDataSouce = [[CitiesDataTool sharedManager] queryAllRecordWithShengID:[provinceItem.code substringWithRange:(NSRange){0,2}]];
        NSString *province = [self province:indexPath.row];
        self.cityDataSouce = [[self.provinceDataSouce safeObjectAtIndex:indexPath.row] valueForKey:province];
        if(self.cityDataSouce.count == 0){
            NSInteger count = self.tableViews.count;
            for (int i = 0; i < count && count != 1; i++) {
                [self removeLastItem];
            }
            [self setUpAddress:province];
        }

        NSInteger count = self.tableViews.count;
        for (int i = 1; i < count; i++) {
            [self removeLastItem];
        }
        
        if(addressArr.count <= 1) {
            return;
        }
        [self addTopBarItem];
        [self addTableView];
        [self scrollToNextItem:province];
        
        //之前未选中省，第一次选择省
        //        [self addTopBarItem];
        //        [self addTableView];
        //        //AddressItem * item = self.dataSouce[indexPath.row];
        //        [self scrollToNextItem:province];
        
    }else if ([self.tableViews indexOfObject:tableView] == 1){
        self.citySelectedIndexPath = indexPath;
        //AddressItem * cityItem = self.cityDataSouce[indexPath.row];
        //self.districtDataSouce = [[CitiesDataTool sharedManager] queryAllRecordWithShengID:cityItem.sheng cityID:cityItem.di];
        NSString *city = [self city:indexPath.row];
        self.districtDataSouce = ([self.cityDataSouce isKindOfClass:[NSDictionary class]] ? [self.cityDataSouce valueForKey:city] : nil);
        NSIndexPath * indexPath0 = [tableView indexPathForSelectedRow];
        
        NSInteger count = self.tableViews.count;
        for (int i = 2; i < count; i++) {
            [self removeLastItem];
        }
        
        if ([indexPath0 compare:indexPath] != NSOrderedSame && indexPath0) {
            [self addTopBarItem];
            [self addTableView];
            [self scrollToNextItem:city];
            
        }else if ([indexPath0 compare:indexPath] == NSOrderedSame && indexPath0){
            
            [self scrollToNextItem:city];
        }
        
        if(addressArr.count <= 2) {
            if(city != nil &&  [city isKindOfClass:[NSString class]] && city.length > 0) {
                [self setUpAddress:city];
            }
            return;
        }
        
        [self addTopBarItem];
        [self addTableView];
        //AddressItem * item = self.cityDataSouce[indexPath.row];
        [self scrollToNextItem:city];
        
    }else if ([self.tableViews indexOfObject:tableView] == 2){
        self.districtSelectedIndexPath = indexPath;
        NSString *distric = [self district:indexPath.row];
        self.townshipDataSouce = ([self.districtDataSouce isKindOfClass:[NSArray class]] ? nil : [self.districtDataSouce valueForKey:distric]);
        
        NSInteger count = self.tableViews.count;
        for (int i = 3; i < count; i++) {
            [self removeLastItem];
        }
        
        if(self.townshipDataSouce.count == 0){
            [self setUpAddress:distric];
        }
        
        if(addressArr.count <= 3) {
            return;
        }
        
        [self addTopBarItem];
        [self addTableView];
        [self scrollToNextItem:distric];
    }else if ([self.tableViews indexOfObject:tableView] == 3) {
        self.townshipSelectedIndexPath = indexPath;
        NSString *township = [self township:indexPath.row];
        //self.townshipDataSouce = [self.districtDataSouce valueForKey:distric];
        [self setUpAddress:township];
    }
}

- (void)configWithAddress:(NSString *)address {
    if([StringUtil isEmpty:address]) {
        return;
    }
    
    NSArray *addressArr = [address componentsSeparatedByString:@" "];
    
    NSInteger provinceIndex = [self indexOfProvince:[addressArr safeObjectAtIndex:0]];
    if(provinceIndex == -1) {
        return;
    }
    self.provinceSelectedIndexPath = [NSIndexPath indexPathForRow:provinceIndex inSection:0];
    [self scrollToIndexCell:[self.tableViews lastObject] index:provinceIndex];
    [self configTableViews:[self.tableViews lastObject] indexPath:self.provinceSelectedIndexPath address:addressArr];
    //[self tableView:[self.tableViews lastObject] willSelectRowAtIndexPath:self.provinceSelectedIndexPath];
    
    NSInteger cityIndex = [self indexOfCity:[addressArr safeObjectAtIndex:1]];
    if(cityIndex == -1) {
        return;
    }
    self.citySelectedIndexPath = [NSIndexPath indexPathForRow:cityIndex inSection:0];
    [self scrollToIndexCell:[self.tableViews lastObject] index:cityIndex];
    [self configTableViews:[self.tableViews lastObject] indexPath:self.citySelectedIndexPath address:addressArr];
    //[self tableView:[self.tableViews lastObject] willSelectRowAtIndexPath:self.citySelectedIndexPath];
    
    
    NSInteger districtIndex = [self indexOfDistrict:[addressArr safeObjectAtIndex:2]];
    if(districtIndex == -1) {
        return;
    }
    self.districtSelectedIndexPath = [NSIndexPath indexPathForRow:districtIndex inSection:0];
    [self scrollToIndexCell:[self.tableViews lastObject] index:districtIndex];
    [self configTableViews:[self.tableViews lastObject] indexPath:self.districtSelectedIndexPath address:addressArr];
    //[self tableView:[self.tableViews lastObject] willSelectRowAtIndexPath:self.districtSelectedIndexPath];
    
    NSInteger townshipIndex = [self indexOfTownship:[addressArr safeObjectAtIndex:3]];
    if(townshipIndex == -1) {
        return;
    }
    self.townshipSelectedIndexPath = [NSIndexPath indexPathForRow:townshipIndex inSection:0];
    [self setUpAddress:[addressArr safeObjectAtIndex:3]];
    [self scrollToIndexCell:[self.tableViews lastObject] index:townshipIndex];

}

- (void)scrollToIndexCell:(UITableView *)tableview index:(NSInteger)index {
    [tableview layoutIfNeeded];
    [tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

+ (instancetype)show:(NSString *)address {
    UIButton *bg = [[UIButton alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    bg.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    bg.alpha = 0;
    bg.tag = 20001;
    [bg addAction:^(UIButton *btn) {
        [ChooseLocationView hide];
    }];
    [MoApp.window addSubview:bg];
    [bg addAlpha:1 duration:0.25f block:nil];
    
    ChooseLocationView *chooseLocationView = [[ChooseLocationView alloc]initWithFrame:CGRectMake(0, MoApp.window.height, SCREEN_WIDTH, ChooseLocationViewHeight)];
    chooseLocationView.tag = 20000;
    [MoApp.window addSubview:chooseLocationView];
    CGFloat y = (MoApp.window.height-ChooseLocationViewHeight);
    [UIView animateWithDuration:0.25 animations:^{
        chooseLocationView.y = y;
    }];
//    [chooseLocationView configWithAddress:address];
    return chooseLocationView;
}

+ (void)hide {
    ChooseLocationView *chooseLocationView = [MoApp.window viewWithTag:20000];
    @weakify(chooseLocationView);
    [chooseLocationView moveY:MoApp.window.height+ChooseLocationViewHeight/2 block:^(id data) {
        @strongify(chooseLocationView);
        [chooseLocationView removeFromSuperview];
    }];
    
    UIView *bg = [MoApp.window viewWithTag:20001];
    @weakify(bg);
    [bg addAlpha:0 duration:0.25 block:^(id data) {
        @strongify(bg);
        [bg removeFromSuperview];
    }];
}

#pragma mark - setUp UI

- (void)setUp {
    self.backgroundColor = [UIColor whiteColor];
    NSString *fullPath = [[NSBundle mainBundle] pathForResource:@"arealist" ofType:@"txt"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:fullPath]) {
        //return fullPath;
        NSString *jsonString = [[NSString alloc] initWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"%@",jsonString);
        NSData *stringData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:stringData options:0 error:nil];
        NSLog(@"%@",json);
        self.provinceDataSouce = json;
    }
    
//    UIView * topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0)];
//    [self addSubview:topView];
//    UILabel * titleLabel = [[UILabel alloc]init];
//    titleLabel.text = @"所在地区";
//    [titleLabel sizeToFit];
//    [topView addSubview:titleLabel];
//    titleLabel.centerY = topView.height * 0.5;
//    titleLabel.centerX = topView.width * 0.5;
//    UIView * separateLine = [self separateLine];
//    [topView addSubview: separateLine];
//    separateLine.top = topView.height - separateLine.height;
//    topView.backgroundColor = [UIColor whiteColor];

    
    AddressView * topTabbar = [[AddressView alloc]initWithFrame:CGRectMake(0, 20, self.frame.size.width, kHYTopViewHeight)];
    [self addSubview:topTabbar];
    _topTabbar = topTabbar;
    [self addTopBarItem];
    UIView * separateLine1 = [self separateLine];
    [topTabbar addSubview: separateLine1];
    separateLine1.top = CGRectGetMaxY(topTabbar.frame);
    [_topTabbar layoutIfNeeded];
    topTabbar.backgroundColor = [UIColor whiteColor];
    
    UIView * underLine = [[UIView alloc] initWithFrame:CGRectZero];
    underLine.clipsToBounds = YES;
    underLine.layer.cornerRadius = 1;
    [topTabbar addSubview:underLine];
    _underLine = underLine;
    underLine.height = 2.0f;
    UIButton * btn = self.topTabbarItems.lastObject;
    [self changeUnderLineFrame:btn];
    underLine.top = kHYTopViewHeight-underLine.height-2;
    
    _underLine.backgroundColor = [UIColor blackColor];
    UIScrollView * contentView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(topTabbar.frame), self.frame.size.width, self.height - kHYTopTabbarHeight)];
    contentView.contentSize = CGSizeMake(HYScreenW, 0);
    [self addSubview:contentView];
    _contentView = contentView;
    _contentView.showsHorizontalScrollIndicator = NO;
    _contentView.pagingEnabled = YES;
    _contentView.backgroundColor = [UIColor whiteColor];
    [self addTableView];
    _contentView.delegate = self;
}


- (void)addTableView{

    UITableView * tabbleView = [[UITableView alloc]initWithFrame:CGRectMake(self.tableViews.count * HYScreenW, 0, HYScreenW, _contentView.height)];
    [_contentView addSubview:tabbleView];
    [self.tableViews addObject:tabbleView];
    tabbleView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tabbleView.delegate = self;
    tabbleView.dataSource = self;
    tabbleView.contentInset = UIEdgeInsetsMake(0, 0, 44, 0);
    [tabbleView registerNib:[UINib nibWithNibName:@"AddressTableViewCell" bundle:nil] forCellReuseIdentifier:@"AddressTableViewCell"];
}

- (void)addTopBarItem{
    
    UIButton * topBarItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [topBarItem setTitle:@"未选择" forState:UIControlStateNormal];
    [topBarItem setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [topBarItem setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [topBarItem sizeToFit];
     topBarItem.centerY = _topTabbar.height * 0.5;
    [self.topTabbarItems addObject:topBarItem];
    [_topTabbar addSubview:topBarItem];
    [topBarItem addTarget:self action:@selector(topBarItemClick:) forControlEvents:UIControlEventTouchUpInside];
}

- (NSInteger)indexOfProvince:(NSString *)province {
    if([StringUtil isEmpty:province]) {
        return -1;
    }
    
    __block NSInteger index = -1;
    [self.provinceDataSouce enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj && [obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dic = (NSDictionary *)obj;
            if([[[dic allKeys] firstObject] isEqualToString:province]) {
                index = idx;
                *stop = YES;
            }
        }
    }];
    return index;
}

- (NSInteger)indexOfCity:(NSString *)city {
    if([StringUtil isEmpty:city]) {
        return -1;
    }
    
    if([self.cityDataSouce isKindOfClass:[NSArray class]]) {
        NSInteger index = [(NSArray *)self.cityDataSouce indexOfObject:city];
        if(index != NSNotFound) {
            return index;
        } else {
            return -1;
        }
    }
    
    __block NSInteger index = -1;
    [[self.cityDataSouce allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *str = (NSString *)obj;
        if([str isEqualToString:city]) {
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

- (NSInteger)indexOfDistrict:(NSString *)district {
    if([StringUtil isEmpty:district]) {
        return -1;
    }
    
    __block NSInteger index = -1;
    if([self.districtDataSouce isKindOfClass:[NSArray class]]) {
        [(NSArray *)self.districtDataSouce enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *str = (NSString *)obj;
            if([str isEqualToString:district]) {
                index = idx;
                *stop = YES;
            }
        }];
    } else if([self.districtDataSouce isKindOfClass:[NSDictionary class]]) {
        [[self.districtDataSouce allKeys] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *str = (NSString *)obj;
            if([str isEqualToString:district]) {
                index = idx;
                *stop = YES;
            }
        }];
    }
    
    return index;
}

- (NSInteger)indexOfTownship:(NSString *)township {
    if([StringUtil isEmpty:township]) {
        return -1;
    }
    
    __block NSInteger index = -1;
    [self.townshipDataSouce enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *str = (NSString *)obj;
        if([str isEqualToString:township]) {
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

- (NSString *)province:(NSInteger)row {
    return [[[self.provinceDataSouce safeObjectAtIndex:row] allKeys] firstObject];
}

- (NSString *)city:(NSInteger)row {
    if([self.cityDataSouce isKindOfClass:[NSArray class]]) {
        NSArray *arr = (NSArray *)self.cityDataSouce;
        return [arr safeObjectAtIndex:row] ?: @"";
    }
    return [[self.cityDataSouce allKeys] safeObjectAtIndex:row];
}

- (NSString *)district:(NSInteger)row {
    //如果三级地域没有的话，districtDataSouce会是一个数组
    if([self.districtDataSouce isKindOfClass:[NSDictionary class]]) {
        return [[self.districtDataSouce allKeys] safeObjectAtIndex:row];
    } else if([self.districtDataSouce isKindOfClass:[NSArray class]]) {
        return [(NSArray *)self.districtDataSouce safeObjectAtIndex:row];
    }
    return @"";
}

- (NSString *)township:(NSInteger)row {
    return [self.townshipDataSouce safeObjectAtIndex:row];
}

#pragma mark - TableViewDatasouce

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if([self.tableViews indexOfObject:tableView] == 0){
        return self.provinceDataSouce.count;
    }else if ([self.tableViews indexOfObject:tableView] == 1){
        return self.cityDataSouce.count;
    }else if ([self.tableViews indexOfObject:tableView] == 2){
        return self.districtDataSouce.count;
    }else if ([self.tableViews indexOfObject:tableView] == 3){
        return self.townshipDataSouce.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    AddressTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"AddressTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    AddressItem * item = [[AddressItem alloc] init];
    NSString *name = @"";
    //省级别
    if([self.tableViews indexOfObject:tableView] == 0){
        name = [self province:indexPath.row];
        if(self.provinceSelectedIndexPath.row == indexPath.row) {
            item.isSelected = YES;
        }
    }
    //市级别
    else if ([self.tableViews indexOfObject:tableView] == 1){
        name = [self city:indexPath.row];
        if(self.citySelectedIndexPath.row == indexPath.row) {
            item.isSelected = YES;
        }
    }
    //区级别
    else if ([self.tableViews indexOfObject:tableView] == 2){
        name = [self district:indexPath.row];
        if(self.districtSelectedIndexPath.row == indexPath.row) {
            item.isSelected = YES;
        }
    }
    //乡镇级别
    else if ([self.tableViews indexOfObject:tableView] == 3){
        name = [self township:indexPath.row];
        if(self.townshipSelectedIndexPath.row == indexPath.row) {
            item.isSelected = YES;
        }
    }
    item.name = name;
    cell.item = item;
    return cell;
}

#pragma mark - TableViewDelegate
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([self.tableViews indexOfObject:tableView] == 0){
        self.provinceSelectedIndexPath = indexPath;
        //1.1 获取下一级别的数据源(市级别,如果是直辖市时,下级则为区级别)
        //AddressItem * provinceItem = self.dataSouce[indexPath.row];
        //self.cityDataSouce = [[CitiesDataTool sharedManager] queryAllRecordWithShengID:[provinceItem.code substringWithRange:(NSRange){0,2}]];
        NSString *province = [self province:indexPath.row];
        self.cityDataSouce = [[self.provinceDataSouce safeObjectAtIndex:indexPath.row] valueForKey:province];
        if(self.cityDataSouce.count == 0){
            NSInteger count = self.tableViews.count;
            for (int i = 0; i < count && count != 1; i++) {
                [self removeLastItem];
            }
            [self setUpAddress:province];
            [self finishedChoose];
            return indexPath;
        }
        //1.1 判断是否是第一次选择,不是,则重新选择省,切换省.
        NSIndexPath * indexPath0 = [tableView indexPathForSelectedRow];

//        if (indexPath0 && [indexPath0 compare:indexPath] != NSOrderedSame) {
//
//            for (int i = 1; i < self.tableViews.count; i++) {
//                [self removeLastItem];
//            }
//            [self addTopBarItem];
//            [self addTableView];
//            [self scrollToNextItem:province];
//            return indexPath;
//
//        }else if (indexPath0 && [indexPath0 compare:indexPath] == NSOrderedSame){
//
//            for (int i = 1; i < self.tableViews.count; i++) {
//                [self removeLastItem];
//            }
//            [self addTopBarItem];
//            [self addTableView];
//            [self scrollToNextItem:province];
//            return indexPath;
//        }
        
        NSInteger count = self.tableViews.count;
        for (int i = 1; i < count; i++) {
            [self removeLastItem];
        }
        [self addTopBarItem];
        [self addTableView];
        [self scrollToNextItem:province];
        return indexPath;

        //之前未选中省，第一次选择省
//        [self addTopBarItem];
//        [self addTableView];
//        //AddressItem * item = self.dataSouce[indexPath.row];
//        [self scrollToNextItem:province];
        
    }else if ([self.tableViews indexOfObject:tableView] == 1){
        self.citySelectedIndexPath = indexPath;
        //AddressItem * cityItem = self.cityDataSouce[indexPath.row];
        //self.districtDataSouce = [[CitiesDataTool sharedManager] queryAllRecordWithShengID:cityItem.sheng cityID:cityItem.di];
        
        //如香港特别行政区可能下一级是一个数组，这里特殊判断一下
        if([self.cityDataSouce isKindOfClass:[NSArray class]]) {
            NSArray *arr = (NSArray *)self.cityDataSouce;
            [self setUpAddress:[arr safeObjectAtIndex:indexPath.row]];
            [self finishedChoose];
            return indexPath;
        }
//        if([self.cityDataSouce isKindOfClass:[NSDictionary class]]) {
//            NSArray *arr = [self.cityDataSouce allKeys];
//            [self setUpAddress:[arr safeObjectAtIndex:indexPath.row]];
//            [self finishedChoose];
//            return indexPath;
//        }
        NSString *city = [self city:indexPath.row];
        self.districtDataSouce = [self.cityDataSouce valueForKey:city];
        NSIndexPath * indexPath0 = [tableView indexPathForSelectedRow];
        
        NSInteger count = self.tableViews.count;
        for (int i = 2; i < count; i++) {
            [self removeLastItem];
        }
        
        if ([indexPath0 compare:indexPath] != NSOrderedSame && indexPath0) {
            [self addTopBarItem];
            [self addTableView];
            [self scrollToNextItem:city];
            return indexPath;

        }else if ([indexPath0 compare:indexPath] == NSOrderedSame && indexPath0){
        
            [self scrollToNextItem:city];
            return indexPath;
        }
        
        [self addTopBarItem];
        [self addTableView];
        //AddressItem * item = self.cityDataSouce[indexPath.row];
        [self scrollToNextItem:city];
        
    }else if ([self.tableViews indexOfObject:tableView] == 2){
        self.districtSelectedIndexPath = indexPath;
        NSString *distric = [self district:indexPath.row];
        self.townshipDataSouce = ([self.districtDataSouce isKindOfClass:[NSArray class]] ? nil : [self.districtDataSouce valueForKey:distric]);
        
        NSInteger count = self.tableViews.count;
        for (int i = 3; i < count; i++) {
            [self removeLastItem];
        }
        ///只给选到三级
//        if(self.townshipDataSouce.count == 0){
            [self setUpAddress:distric];
            [self finishedChoose];
            return indexPath;
//        }
        
        [self addTopBarItem];
        [self addTableView];
        [self scrollToNextItem:distric];
    }else if ([self.tableViews indexOfObject:tableView] == 3){
        self.townshipSelectedIndexPath = indexPath;
        NSString *township = [self township:indexPath.row];
        //self.townshipDataSouce = [self.districtDataSouce valueForKey:distric];
        [self setUpAddress:township];
        [self finishedChoose];
    }
    return indexPath;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    AddressItem * item;
//    NSString *name = @"";
//    if([self.tableViews indexOfObject:tableView] == 0){
//        name = [self province:indexPath.row];
//    }else if ([self.tableViews indexOfObject:tableView] == 1){
//        name = [self city:indexPath.row];
//    }else if ([self.tableViews indexOfObject:tableView] == 2){
//        name = [self district:indexPath.row];
//    }else if ([self.tableViews indexOfObject:tableView] == 3){
//        name = [self township:indexPath.row];
//    }
//    item.name = name;
//    item.isSelected = NO;
//    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}



#pragma mark - private 

//点击按钮,滚动到对应位置
- (void)topBarItemClick:(UIButton *)btn{
    
    NSInteger index = [self.topTabbarItems indexOfObject:btn];
    
    [UIView animateWithDuration:0.5 animations:^{
        self.contentView.contentOffset = CGPointMake(index * HYScreenW, 0);
        [self changeUnderLineFrame:btn];
    }];
}

//调整指示条位置
- (void)changeUnderLineFrame:(UIButton  *)btn{
    
    _selectedBtn.selected = NO;
    btn.selected = YES;
    _selectedBtn = btn;
    _underLine.left = btn.centerX-5;
    _underLine.width = 10;
}

//完成地址选择,执行chooseFinish代码块
- (void)setUpAddress:(NSString *)address {
    if(self.topTabbarItems.count == 0) {
        if(address != nil && [address isKindOfClass:[NSString class]] && address.length > 0) {
            self.address = address;
        }
        return;
    }
    
    NSInteger index = self.contentView.contentOffset.x / HYScreenW;
    UIButton * btn = self.topTabbarItems[index];
    [btn setTitle:address forState:UIControlStateNormal];
    [btn sizeToFit];
    [_topTabbar layoutIfNeeded];
    [self changeUnderLineFrame:btn];
    NSMutableString * addressStr = [[NSMutableString alloc] init];
    for (UIButton * btn  in self.topTabbarItems) {
        if (/*[btn.currentTitle isEqualToString:@"县"] || [btn.currentTitle isEqualToString:@"市辖区"] || */[btn.currentTitle isEqualToString:@"不选"]) {
            addressStr = [[addressStr substringToIndex:addressStr.length-1] mutableCopy];
            break;
        }
        [addressStr appendString:btn.currentTitle];
        if([self.topTabbarItems lastObject] == btn) {
            break;
        }
        [addressStr appendString:@" "];
    }
    self.address = addressStr;
}

- (void)finishedChoose {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.chooseFinish) {
            self.chooseFinish(self.address);
        }
        [ChooseLocationView hide];
    });
}

//当重新选择省或者市的时候，需要将下级视图移除。
- (void)removeLastItem{

    [self.tableViews.lastObject performSelector:@selector(removeFromSuperview) withObject:nil withObject:nil];
    [self.tableViews removeLastObject];
    
    [self.topTabbarItems.lastObject performSelector:@selector(removeFromSuperview) withObject:nil withObject:nil];
    [self.topTabbarItems removeLastObject];
}

//滚动到下级界面,并重新设置顶部按钮条上对应按钮的title
- (void)scrollToNextItem:(NSString *)preTitle{
    
    NSInteger index = self.contentView.contentOffset.x / HYScreenW;
    UIButton * btn = self.topTabbarItems[index];
    [btn setTitle:preTitle forState:UIControlStateNormal];
    [btn sizeToFit];
    [_topTabbar layoutIfNeeded];
    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.contentSize = (CGSize){self.tableViews.count * HYScreenW,0};
        CGPoint offset = self.contentView.contentOffset;
        self.contentView.contentOffset = CGPointMake(offset.x + HYScreenW, offset.y);
        [self changeUnderLineFrame: [self.topTabbar.subviews lastObject]];
    }];
}


#pragma mark - <UIScrollView>
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView != self.contentView) return;
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        NSInteger index = scrollView.contentOffset.x / HYScreenW;
        UIButton * btn = weakSelf.topTabbarItems[index];
        [weakSelf changeUnderLineFrame:btn];
    }];
}

#pragma mark - 开始就有地址时.

- (void)setAreaCode:(NSString *)areaCode{
    
    _areaCode = areaCode;
    //2.1 添加市级别,地区级别列表
    [self addTableView];
    [self addTableView];

    //self.cityDataSouce = [[CitiesDataTool sharedManager] queryAllRecordWithShengID:[self.areaCode substringWithRange:(NSRange){0,2}]];
    //self.districtDataSouce = [[CitiesDataTool sharedManager] queryAllRecordWithShengID:[self.areaCode substringWithRange:(NSRange){0,2}] cityID:[self.areaCode substringWithRange:(NSRange){2,2}]];//
  
    //2.3 添加底部对应按钮
    [self addTopBarItem];
    [self addTopBarItem];
    
    NSString * code = [self.areaCode stringByReplacingCharactersInRange:(NSRange){2,4} withString:@"0000"];
    NSString * provinceName = @"";//[[CitiesDataTool sharedManager] queryAllRecordWithAreaCode:code];
    UIButton * firstBtn = self.topTabbarItems.firstObject;
    [firstBtn setTitle:provinceName forState:UIControlStateNormal];
    
    NSString * cityName = @"";//[[CitiesDataTool sharedManager] queryAllRecordWithAreaCode:[self.areaCode stringByReplacingCharactersInRange:(NSRange){4,2} withString:@"00"]];
    UIButton * midBtn = self.topTabbarItems[1];
    [midBtn setTitle:cityName forState:UIControlStateNormal];
    
    NSString * districtName = @"";//[[CitiesDataTool sharedManager] queryAllRecordWithAreaCode:self.areaCode];
    UIButton * lastBtn = self.topTabbarItems.lastObject;
    [lastBtn setTitle:districtName forState:UIControlStateNormal];
    [self.topTabbarItems makeObjectsPerformSelector:@selector(sizeToFit)];
    [_topTabbar layoutIfNeeded];
    
    
    [self changeUnderLineFrame:lastBtn];
    
    //2.4 设置偏移量
    self.contentView.contentSize = (CGSize){self.tableViews.count * HYScreenW,0};
    CGPoint offset = self.contentView.contentOffset;
    self.contentView.contentOffset = CGPointMake((self.tableViews.count - 1) * HYScreenW, offset.y);

    [self setSelectedProvince:provinceName andCity:cityName andDistrict:districtName];
}

//初始化选中状态
- (void)setSelectedProvince:(NSString *)provinceName andCity:(NSString *)cityName andDistrict:(NSString *)districtName {
    
//    for (AddressItem * item in self.dataSouce) {
//        if ([item.name isEqualToString:provinceName]) {
//            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:[self.dataSouce indexOfObject:item] inSection:0];
//            UITableView * tableView  = self.tableViews.firstObject;
//            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
//            [self tableView:tableView didSelectRowAtIndexPath:indexPath];
//            break;
//        }
//    }
//
//    for (int i = 0; i < self.cityDataSouce.count; i++) {
//        AddressItem * item = self.cityDataSouce[i];
//
//        if ([item.name isEqualToString:cityName]) {
//            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:i inSection:0];
//            UITableView * tableView  = self.tableViews[1];
//            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
//            [self tableView:tableView didSelectRowAtIndexPath:indexPath];
//            break;
//        }
//    }
//
//    for (int i = 0; i <self.districtDataSouce.count; i++) {
//        AddressItem * item = self.districtDataSouce[i];
//        if ([item.name isEqualToString:districtName]) {
//            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:i inSection:0];
//            UITableView * tableView  = self.tableViews[2];
//            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
//            [self tableView:tableView didSelectRowAtIndexPath:indexPath];
//            break;
//        }
//    }
}

#pragma mark - getter 方法

//分割线
- (UIView *)separateLine{
    
    UIView * separateLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1 / [UIScreen mainScreen].scale)];
    separateLine.backgroundColor = [UIColor colorWithRed:222/255.0 green:222/255.0 blue:222/255.0 alpha:1];
    return separateLine;
}

- (NSMutableArray *)tableViews{
    
    if (_tableViews == nil) {
        _tableViews = [NSMutableArray array];
    }
    return _tableViews;
}

- (NSMutableArray *)topTabbarItems{
    if (_topTabbarItems == nil) {
        _topTabbarItems = [NSMutableArray array];
    }
    return _topTabbarItems;
}


//省级别数据源
- (NSDictionary *)dataSouce{
    
    if (_dataSouce == nil) {
       
    }
    return _dataSouce;
}
@end
