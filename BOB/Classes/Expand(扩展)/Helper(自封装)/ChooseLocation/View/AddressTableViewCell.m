//
//  AddressTableViewCell.m
//  ChooseLocation
//
//  Created by Sekorm on 16/8/26.
//  Copyright © 2016年 HY. All rights reserved.
//

#import "AddressTableViewCell.h"
#import "AddressItem.h"

@interface AddressTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectFlag;
@property (weak, nonatomic) IBOutlet UIImageView *checkIcon;
@end
@implementation AddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.checkIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-10);
    }];
}

- (void)setItem:(AddressItem *)item {
    _item = item;
    if(item == nil) {
        _item = [[AddressItem alloc] init];
    }
    _addressLabel.text = item.name;
    _addressLabel.textColor = item.isSelected ? [UIColor themeColor] : [UIColor blackColor] ;
    _selectFlag.hidden = !item.isSelected;
}
@end
