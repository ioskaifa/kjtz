//
//  PGDatePickHelper.h
//  Lcwl
//
//  Created by mac on 2018/11/24.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PGDatePickManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface PGDatePickHelper : NSObject
+ (void)showStartTime:(UIViewController<PGDatePickerDelegate> *)sender;
+ (void)showEndTime:(UIViewController<PGDatePickerDelegate> *)sender;
@end

NS_ASSUME_NONNULL_END
