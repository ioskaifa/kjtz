//
//  PGDatePickHelper.m
//  Lcwl
//
//  Created by mac on 2018/11/24.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import "PGDatePickHelper.h"

@implementation PGDatePickHelper
+ (void)showYearAndMonthPickerView:(UIViewController<PGDatePickerDelegate> *)sender topToday:(BOOL)topToday {
    PGDatePickManager *datePickManager = [[PGDatePickManager alloc]init];
    datePickManager.titleLabel.text = @"";
    datePickManager.headerViewBackgroundColor = [UIColor whiteColor];
    
    
    PGDatePicker *datePicker = datePickManager.datePicker;
    datePicker.ascendingSort = YES;
    //设置线条的颜色
    datePicker.lineBackgroundColor = [UIColor clearColor];
    //设置选中行的字体颜色
    datePicker.textColorOfSelectedRow = [UIColor blackColor];
    //设置取消按钮的字体颜色
    datePickManager.cancelButtonTextColor = [UIColor moOrange];
    //设置取消按钮的字体大小
    datePickManager.cancelButtonFont = [UIFont boldSystemFontOfSize:14];
    //设置确定按钮的字体颜色
    datePickManager.confirmButtonTextColor = [UIColor moOrange];
    datePickManager.confirmButtonFont = [UIFont boldSystemFontOfSize:14];
    datePicker.backgroundColor = [UIColor moBackground];
    
    datePicker.delegate = sender;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"yyyy-MM";
    datePicker.maximumDate = [dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]];
    datePicker.minimumDate = [dateFormatter dateFromString: @"1900-01"];
    datePicker.topToday = topToday;
    datePicker.datePickerMode = PGDatePickerModeYearAndMonth;
    [sender presentViewController:datePickManager animated:false completion:nil];
}

+ (void)showStartTime:(UIViewController<PGDatePickerDelegate> *)sender {
    [self showYearAndMonthPickerView:sender topToday:NO];
}

+ (void)showEndTime:(UIViewController<PGDatePickerDelegate> *)sender {
    [self showYearAndMonthPickerView:sender topToday:YES];
}
@end
