//
//  QRCodeScanManage.h
//  RatelBrother
//
//  Created by AlphaGo on 2020/7/28.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QRCodeScanManage : NSObject
+ (void)startScan:(FinishedBlock)block;
@end

NS_ASSUME_NONNULL_END
