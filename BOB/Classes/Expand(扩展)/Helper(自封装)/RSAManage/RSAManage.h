//
//  RASManage.h
//  JiuJiuEcoregion
//
//  Created by mac on 2019/6/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RSAManage : NSObject

+ (NSString *)encryptString:(NSString *)orginStr privateKey:(NSString *)key;
+ (NSString *)encryptString:(NSString *)orginStr;
+ (NSString *)encryptChatString:(NSString *)orginStr;
+ (NSString *)decryptString:(NSString *)encryptStr;
@end

NS_ASSUME_NONNULL_END
