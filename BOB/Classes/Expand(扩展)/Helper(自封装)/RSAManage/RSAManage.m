//
//  RASManage.m
//  JiuJiuEcoregion
//
//  Created by mac on 2019/6/1.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "RSAManage.h"
#import "RSA.h"
#import "AESCipher.h"

static NSString *chatPubkey = @"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbjhApHLDCtYTES22LR3GPRVa8PEQzLD02P/j8YxP1kQsPzuIxaYl0RMS8Qs4oWR1fwcVb6ATQW0s0P9JJrj196CG5WKJtX3ouiEUn8bmbiHA5d0RAYZiUdbY5bj96jq4VhCnlBhf71huWY4tkpFOK8juPJZrm9rqTswScWWE+vQIDAQAB\n-----END PUBLIC KEY-----";

static NSString *debugPubkey = @"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbj+aV4ngjZRm/bt95n4oWHWMaityQPwrkAG6Hec9Gienegw1DkH2NGFOPQy9AGzHiwAevF4CcAiueceXkLRa/nP8HmyhYixMplCWgY71Ay6FBzfSZF2eetPNcrNv7BhLmyM7eVXM56KsB2/qy54csA3pJ6ogNP12FjNJJYRivewIDAQAB\n-----END PUBLIC KEY-----";

static NSString *pubkey = @"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC27A67VIA3Lij3omL05Kn2b4lzHQ45mcPhWVa3b+sR3Xcwsl8ewI/DH8cEG1V9WjJ5XUDeHEHPghx1x2x4drN0TbNAYMmvY7+qnScoxbi0GQXCJFXRM6lEahiMMKeSI34fq/vm6/x++OUNQFyFj56PCKiAGM/NbOZXDoTAH3zucQIDAQAB\n-----END PUBLIC KEY-----";

static NSString *pubkey2 = @"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCBSL+6u2OaFjG2ceNgt3Xnhfb/DJyzzjYx08dLPQAWA1cq75c4Xe5957xbQZwXa3m6PSkrcO9BkXOafgUoApc3cdzcR/wTeQhkZZpReM/WCi1WvGFIM4idQcO6P/aTGM4DpAW71zIu+wpu0gaR9GmbWTwqOskkyd5uFPyrrUUg2wIDAQAB\n-----END PUBLIC KEY-----";

static NSString *privkey = @"-----BEGIN PRIVATE KEY-----\nMIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKjDC7x+0z788+Ubmw0WMW2I0bjOwwJFOZ9C4xcroHphX0WzWT1rrPqxRU6yq8LZ/dB5xo63ahs2zxR0/D8SV45xiT/xntqxVUlwBFxgeJDw2QFSY/JRAoAWIqNW+G0ekJ9ZkN+6scRAj+sTaYYIH2cw0ly9pIyMr9TS7IQzB6prAgMBAAECgYEAmGZi/9sMC5LE8b4HPD8xbbgjpB/bzP4UtjTh/LeiGUI7licLTMMjF9TkQNhq8fCIHC8MVy9dO6w4P0IR1SdMNtfx674b3H6WjSpCXT9B4GVqzv6xiAh1r0wDoE7mJtLiAI5EkPxZm+IEim6D89mVn8/aUjkqkQ7ZLwAu2uvOHKkCQQDSm2rJP3o31pCpEGN2vJMg3Cy2R5woGUejmnnVM6ot8+up0L4z5Q9AmRaEXt1TGyateSt44/yGNEIsRiPYy90VAkEAzSLAwzU68awJmMwDznK44QMdIDGkIweLNJcRD5SO/ne4giQmYnDQTfB4Q48QIiYKp0RwJJRc7PTcxbVF7vJJfwJAWRo16KT5gUw+8bgkTKTlnl5ocEoFsBVZ8Ma3StNL6ZssFjFhdzUu6caa9y/ndXSkPXppQQE74k+Tu4WFPwCpLQJBALfFFXkLe8W7QFGxGwvcvIFfv7zym7+B55RybSdPCBcxe4qjBfwUYpggAC1Nwb9F4y9b4Tbz7pec+RbpQUBBr9MCQEGl3q9ZX/Qh7YFt9bVVHew/WAzJLapCdcrV3mgQQQFHaTvdEGtwYh4t/VwGVFNRHqCN5yS4LlD9YS5sGOf1U3s=\n-----END PRIVATE KEY-----";

@implementation RSAManage

+ (NSString *)encryptString:(NSString *)orginStr privateKey:(NSString *)key {
    NSString *encodeBase64 = aesEncryptString(orginStr, key);
    return encodeBase64;
}
// return base64 encoded string
+ (NSString *)encryptString:(NSString *)orginStr {
    NSString *string = [RSA encryptString:orginStr publicKey:pubkey];
#if OpenDebugLcwl
    string = [RSA encryptString:orginStr publicKey:debugPubkey];
#endif
    return string;
}

+ (NSString *)encryptChatString:(NSString *)orginStr {
    NSString *string = [RSA encryptString:orginStr publicKey:chatPubkey];
#if OpenDebugLcwl
    string = [RSA encryptString:orginStr publicKey:debugPubkey];
#endif
    return string;
}

+ (NSString *)decryptString:(NSString *)encryptStr {
    return [RSA decryptString:encryptStr privateKey:privkey];
}
@end
