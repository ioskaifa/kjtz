//
//  PC_PassPictureViewController.m
//  OverturnOnlineCollege_Students
//
//  Created by Kevin on 16/4/22.
//  Copyright © 2016年 Dongxk. All rights reserved.
//

#import "HelpViewController.h"
#import "PlaceholderTextView.h"
#import "ReactiveObjC.h"
#import "QNManager.h"
#import "MineHelper.h"

#define ScreenWidth [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight [[UIScreen mainScreen] bounds].size.height
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]


@interface HelpViewController ()<LQPhotoPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate>

@property (strong, nonatomic) UIScrollView *scrollView;
@property (nonatomic, strong) PlaceholderTextView * textView;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UILabel *iLabel;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *tfView;
@property (nonatomic, strong) NSMutableArray *bigImageArray;
@property (nonatomic, strong) UIButton *sureBnt;
@property(nonatomic, copy) NSString *showImg;

@end

@implementation HelpViewController{

    NSString *contentStr;
    NSString *phoneStr;
}


- (void)dealloc {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //self.navigationItem.title = Lang(@"意见反馈");
    self.title = Lang(@"意见反馈");
    self.view.backgroundColor = [UIColor whiteColor];
//    self.navigationController.navigationBar.translucent = NO;
//    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"提交" style:UIBarButtonItemStyleDone target:self action:@selector(clickRightBarButtonItem:)];
//    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
//    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    [self initViews];
    
    [self addDefaultNavLeftBtnItem];
    
    [self.sureBnt setTitle:LangLRPH(@"确定提交", @"          ") forState:UIControlStateNormal];
    [self.view addSubview:self.sureBnt];
    [self.sureBnt sizeToFit];
    [self.sureBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bgView.mas_bottom).offset(111);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(self.sureBnt.width));
        make.height.equalTo(@(44));
    }];
}

- (void)navLeftBtnItemClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)clickRightBarButtonItem:(id)sender{
    if(self.textView.text.length <= 0) {
        return;
    }
    
    [NotifyHelper showHUDAddedTo:self.view animated:YES];
    
    RACSignal *uploadImgeSignal = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [[QNManager shared] uploadImages:[self LQPhotoPicker_smallImageArray] assets:[self LQPhotoPicker_getALAssetArray] completion:^(id data) {
            self.showImg = [data componentsJoinedByString:@","];
            [subscriber sendNext:@"images upload finished!"];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    
    RACSignal *commitSignal = [RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        [MineHelper addUserFeedBack:@{@"feedback_content":self.textView.text} completion:^(BOOL success, id object, NSString *error) {
            [NotifyHelper hideHUDForView:self.view animated:YES];
            if(success) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
        return nil;
    }];
    
    if([[self LQPhotoPicker_smallImageArray] count] > 0) {
        [[uploadImgeSignal then:^RACSignal * _Nonnull{
            return commitSignal;
        }] subscribeNext:^(id  _Nullable x) {
            NSLog(@"上传完毕...");
        }];
    } else {
        [commitSignal subscribeNext:^(id  _Nullable x) {
            
        }];
    }
}

- (UICollectionViewFlowLayout *)flowLayout {
    UICollectionViewFlowLayout *layout= [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake((ScreenWidth-30 - 50) / 4, (ScreenWidth-30 - 50) / 4);
    layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = 10;
    return layout;
}

- (void)initViews{

    UILabel *qLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, ScreenWidth - 30, 20)];
    qLabel.text = @"问题和意见";
    qLabel.textColor = [UIColor blackColor];
    qLabel.font = [UIFont systemFontOfSize:16];
    qLabel.backgroundColor = [UIColor clearColor];
    //[self.view addSubview:qLabel];
    
    
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(15, qLabel.frame.origin.y + qLabel.frame.size.height, ScreenWidth-30, 165)];
    self.bgView.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
    [self.view addSubview:self.bgView];
    
    
    _textView = [[PlaceholderTextView alloc]initWithFrame:CGRectMake(10, 10, ScreenWidth-50, 80)];
    _textView.backgroundColor = [UIColor clearColor];
    _textView.delegate = self;
    _textView.font = [UIFont systemFontOfSize:14.f];
    _textView.textColor = [UIColor blackColor];
    _textView.textAlignment = NSTextAlignmentLeft;
    _textView.placeholderColor = RGBCOLOR(0x89, 0x89, 0x89);
    _textView.placeholder = Lang(@"感谢您使用BOB，使用过程中有任何意见或建议请反馈给我们。");
    [self.bgView addSubview:_textView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewEditChanged:) name:UITextViewTextDidChangeNotification object:_textView];
    
    
    _numLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, _textView.frame.origin.y + _textView.frame.size.height, ScreenWidth - 30, 20)];
    _numLabel.font = [UIFont systemFontOfSize:14.f];
    _numLabel.textColor = [UIColor lightGrayColor];
    _numLabel.text = @"0/150";
    _numLabel.backgroundColor = [UIColor whiteColor];
    _numLabel.textAlignment = NSTextAlignmentRight;
    //[view addSubview:_numLabel];

    
    _iLabel = [[UILabel alloc]initWithFrame:CGRectMake(qLabel.frame.origin.x, self.bgView.frame.origin.y + self.bgView.frame.size.height + 10, qLabel.frame.size.width, qLabel.frame.size.height)];
    _iLabel.text = @"请提供相关问题的截图或照片 (0 / 4)";
    _iLabel.textColor = [UIColor blackColor];
    _iLabel.font = [UIFont systemFontOfSize:16];
    _iLabel.backgroundColor = [UIColor clearColor];
    //[self.view addSubview:_iLabel];
    
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _textView.maxY, ScreenWidth, 100)];
    [self.bgView addSubview:_scrollView];
    
    
    _tfView = [[UIView alloc]initWithFrame:CGRectMake(1, _scrollView.frame.origin.y + _scrollView.frame.size.height + 10, ScreenWidth - 2, 44)];
    _tfView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_tfView];
    
    _textField = [[UITextField alloc]initWithFrame:CGRectMake(15, 0, ScreenWidth - 30, 44)];
    _textField.placeholder = @"联系电话";
    _textField.font = [UIFont systemFontOfSize:15];
    _textField.backgroundColor = [UIColor clearColor];
    _textField.textColor = [UIColor blackColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldEditChanged:) name:UITextFieldTextDidChangeNotification object:_textField];
    //[_tfView addSubview:_textField];
    
    
    [self updateViewsFrame];

    self.LQPhotoPicker_superView = _scrollView;
    self.LQPhotoPicker_imgMaxCount = 9;
    self.collectionWidth = ScreenWidth - 30;
    [self LQPhotoPicker_initPickerView];
    self.LQPhotoPicker_delegate = self;
}

- (void)LQPhotoPicker_pickerViewFrameChanged{
   
    [self updateViewsFrame];
}


#pragma mark --------   更新高度：
- (void)updateViewsFrame{
    
    //photoPicker
    [self LQPhotoPicker_updatePickerViewFrameY:2];
    CGFloat allViewHeight = [self LQPhotoPicker_getPickerViewFrame].size.height;
    self.scrollView.frame = CGRectMake(0,  _textView.maxY, ScreenWidth-30, allViewHeight);
    _tfView.frame = CGRectMake(1, _scrollView.frame.origin.y + _scrollView.frame.size.height + 10, ScreenWidth - 2, 44);
    
    _bigImageArray = [self LQPhotoPicker_smallImageArray];
    _iLabel.text = [NSString stringWithFormat:@"请提供相关问题的截图或照片 (%ld / 4)", [_bigImageArray count]];
    
    self.bgView.height = self.scrollView.y+self.scrollView.height;
    [self rightItemEnable];
}


- (void)textViewEditChanged:(NSNotification *)obj{
    
    UITextView *tv = (UITextView *)obj.object;
    if (tv.text.length > 150) {
        tv.text = [tv.text substringToIndex:150];
    }
    contentStr = tv.text;
    _numLabel.text = [NSString stringWithFormat:@"%ld/150", tv.text.length];
     [self rightItemEnable];
}

- (void)textFieldEditChanged:(NSNotification *)obj{

    UITextField *tf = (UITextField *)obj.object;
    if (tf.text.length > 11) {
        tf.text = [tf.text substringToIndex:11];
    }
    phoneStr = tf.text;
    
    [self rightItemEnable];
}

- (void)rightItemEnable{

    if (_bigImageArray.count > 0 || contentStr.length > 0 || phoneStr.length > 0) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }else{
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}


- (UIColor *)getColor:(NSString *)hexColor
{
    if (hexColor.length<=0) {
        return [UIColor clearColor];
    }
    
    unsigned int red,green,blue;
    NSRange range;
    range.length = 2;
    
    range.location = 0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&red];
    
    range.location = 2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&green];
    
    range.location = 4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green / 255.0f) blue:(float)(blue / 255.0f) alpha:1.0f];
}



- (void)addDefaultNavLeftBtnItem {
    UIButton *btn = [self buttonWithIcon:@"public_black_back"];
    if (btn) {
        [btn addTarget:self action:@selector(navLeftBtnItemClick:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* lItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = lItem;
    }
}

- (UIButton*)buttonWithIcon:(NSString*)strImgName{
    UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0,0,44,NavigationBar_Height)];
    [button setImage:[UIImage imageNamed:strImgName] forState:UIControlStateNormal];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    return button;
}

- (UIButton *)sureBnt {
    if(!_sureBnt) {
        _sureBnt = [[UIButton alloc] init];
        _sureBnt.backgroundColor = [UIColor colorWithHexString:@"#1DAAAC"];
        _sureBnt.clipsToBounds = YES;
        _sureBnt.layer.cornerRadius = 22;
        _sureBnt.titleLabel.font = [UIFont systemFontOfSize:16];
        [_sureBnt addTarget:self action:@selector(clickRightBarButtonItem:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sureBnt;
}
@end
