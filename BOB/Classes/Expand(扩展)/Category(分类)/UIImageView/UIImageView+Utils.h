//
//  UIImageView+Utils.h
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (Utils)

///生成图片自身大小的UIImageView
+ (UIImageView *)autoLayoutImgView:(NSString *)img;

@end

NS_ASSUME_NONNULL_END
