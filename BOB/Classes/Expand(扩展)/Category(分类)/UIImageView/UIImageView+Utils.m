//
//  UIImageView+Utils.m
//  BOB
//
//  Created by mac on 2020/9/11.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UIImageView+Utils.h"

@implementation UIImageView (Utils)

+ (UIImageView *)autoLayoutImgView:(NSString *)img {
    UIImageView *object = [UIImageView new];
    object.image = [UIImage imageNamed:img];
    [object sizeToFit];
    object.mySize = object.size;
    return object;
}

@end
