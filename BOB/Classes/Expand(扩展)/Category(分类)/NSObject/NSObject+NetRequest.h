//
//  NSObject+NetRequest.h
//  RatelBrother
//
//  Created by AlphaGo on 2020/4/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NetRequest)

- (void)request:(NSString *)url param:(NSDictionary*)param completion:(RequestResultObjectCallBack)completion;

- (void)request:(NSString *)url param:(NSDictionary*)param useMsg:(BOOL)useMsg useHud:(UIView *)baseView completion:(RequestResultObjectCallBack)completion;

- (void)request:(NSString *)url param:(NSDictionary*)param useMsg:(BOOL)useMsg useHud:(UIView *)baseView useEncryption:(BOOL)useEncryption completion:(RequestResultObjectCallBack)completion;

@end
