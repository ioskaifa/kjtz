//
//  NSObject+AddProperty.h
//
//
//  Created by XXX on 2019/8/11.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (AddProperty)
@property (nonatomic, strong) id custom;
@property (nonatomic, strong) id openUrl;
@property (nonatomic, strong) id view;
@property (nonatomic, strong) NSNumber *num;
@property (nonatomic, strong) NSDictionary *oneDic;
@end


