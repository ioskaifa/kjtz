//
//  NSObject+AddProperty.m
//
//
//  Created by XXX on 2019/8/11.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "NSObject+AddProperty.h"
#import <objc/runtime.h>

@implementation NSObject (AddProperty)
- (void)setCustom:(id)custom {
    SEL key = @selector(custom);
    objc_setAssociatedObject(self, key, custom, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id)custom {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setOpenUrl:(id)openUrl {
    SEL key = @selector(openUrl);
    objc_setAssociatedObject(self, key, openUrl, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id)openUrl {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setView:(id)view {
    SEL key = @selector(view);
    objc_setAssociatedObject(self, key, view, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id)view {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setNum:(NSNumber *)num {
    SEL key = @selector(num);
    objc_setAssociatedObject(self, key, num, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber *)num {
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setOneDic:(NSDictionary *)oneDic {
    SEL key = @selector(oneDic);
    objc_setAssociatedObject(self, key, oneDic, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSDictionary *)oneDic {
    return objc_getAssociatedObject(self, _cmd);
}
@end

