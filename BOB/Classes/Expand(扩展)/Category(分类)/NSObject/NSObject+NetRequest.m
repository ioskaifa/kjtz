//
//  NSObject+NetRequest.m
//  RatelBrother
//
//  Created by AlphaGo on 2020/4/17.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "NSObject+NetRequest.h"

@implementation NSObject (NetRequest)
- (void)request:(NSString *)url param:(NSDictionary*)param completion:(RequestResultObjectCallBack)completion {
    [self request:url param:param useMsg:NO useHud:nil completion:completion];
}

- (void)request:(NSString *)url param:(NSDictionary*)param useMsg:(BOOL)useMsg useHud:(UIView *)baseView completion:(RequestResultObjectCallBack)completion {
    NSString *fullUrl = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,url];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(fullUrl);
        if(useMsg) {
            net.useMsg();
        }
        if(baseView) {
            net.useHud(baseView);
        }
        //net.useEncrypt();
        net.params(param);
        net.finish(^(id data){
            Block_Exec(completion,[data isSuccess],data,[data valueForKey:@"msg"]);
        }).failure(^(id error){
            Block_Exec(completion,NO,nil,[error description]);
        })
        .execute();
    }];
}

- (void)request:(NSString *)url param:(NSDictionary*)param useMsg:(BOOL)useMsg useHud:(UIView *)baseView useEncryption:(BOOL)useEncryption completion:(RequestResultObjectCallBack)completion {
    NSString *fullUrl = [NSString stringWithFormat:@"%@/%@",LcwlServerRoot,url];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(fullUrl);
        if(useMsg) {
            net.useMsg();
        }
        if(baseView) {
            net.useHud(baseView);
        }
        if (useEncryption) {
            net.useEncrypt();
        }
        net.params(param);
        net.finish(^(id data){
            if(useMsg) {
                [NotifyHelper showMessageWithMakeText:[data valueForKey:@"mess"]];
            }
            Block_Exec(completion,[data isSuccess],data,nil);
        }).failure(^(id error){
            Block_Exec(completion,NO,nil,[error description]);
        })
        .execute();
    }];
}
@end
