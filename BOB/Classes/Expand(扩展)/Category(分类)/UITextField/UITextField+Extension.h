//
//  UITextField+Extension.h
//  RatelBrother
//
//  Created by mac on 2019/9/27.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (Extension)
///注意，使用该分类的前提条件是placeHolder已经赋值了，否则将找不到placeholderLabel
- (void)setPlaceholderFont:(UIFont *)font;
///注意，使用该分类的前提条件是placeHolder已经赋值了，否则将找不到placeholderLabel
- (void)setPlaceholderColor:(UIColor *)color;
///注意，使用该分类的前提条件是placeHolder已经赋值了，否则将找不到placeholderLabel
- (void)setPlaceholderNumberOfLines:(NSInteger)numberOfLines;
@end

NS_ASSUME_NONNULL_END
