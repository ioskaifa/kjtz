//
//  NSDate+String.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/4/27.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "NSDate+String.h"
#import "NSDate+Category.h"

@implementation NSDate (String)

+ (NSString*)dateString:(NSString*)date format:(NSString* )format{
    NSDate *time = [self dateWithString:date withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // 2.格式化日期
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;
    return [formatter stringFromDate:time];
}

+ (NSString*)dateString:(NSDate*)date
{
    // 2.格式化日期
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSString *time = [formatter stringFromDate:date];
    return time;
}

+ (NSString *)dateHourMin
{
    NSDate *date = [NSDate date];
    // 2.格式化日期
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"HH:mm";
    NSString *time = [formatter stringFromDate:date];
    return time;
}

+ (NSDate *)dateWithString:(NSString*)string
            withDateFormat:(NSString*)dateFormat{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = dateFormat;
    NSDate *time = [formatter dateFromString:string];
    return time;
}

+ (NSString *)endTimeIntervalDesc:(NSDate *)endTime{
    
    NSTimeInterval ti = [endTime timeIntervalSinceDate:[NSDate date]];
    if (ti <= 0) {
        return @"";
    }
    NSMutableString *resultString = [NSMutableString string];
    NSInteger day = (NSInteger) (ti / D_DAY);
    ti -= day * D_DAY;
    if (day > 0) {
        [resultString appendFormat:@"%ld天",(long)day];
    }
    
    NSInteger hour = (NSInteger) (ti / D_HOUR);
    ti -= hour * D_HOUR;
    
    [resultString appendFormat:@"%ld时",(long)hour];
    
    NSInteger min = (NSInteger) (ti / D_MINUTE);
    ti -= min * D_MINUTE;
    [resultString appendFormat:@"%ld分",(long)hour];
    
    NSInteger sec = ti;
    [resultString appendFormat:@"%ld秒",(long)sec];
    return resultString;
}

+(NSString *)getCurrentTimeTimestampByFormatter:(NSString *)string {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:string];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Shanghai"];
    [formatter setTimeZone:timeZone];
    NSDate *datenow = [NSDate date];//现在时间,你可以输出来看下是什么格式
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
    return timeSp;
}

@end
