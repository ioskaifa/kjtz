//
//  UIViewController+Customize.m
//  meiliyue
//
//  Created by fly on 12-12-14.
//  Copyright (c) 2012年 sjxu. All rights reserved.
//

#import "UIViewController+Customize.h"
#import "UIImage+Color.h"

#define kNavBtnHeight               30
#define kNavBtnIconImgWidth         16
#define kNavBtnIconImgHeight        16
#define kNavBtnIconImgYMagin        6
#define kNavBtnIconImgXMagin        8
#define kNavBtnIconLabXMagin        3
#define kNavBtnIconLabRightMagin    11

#define kNavBtnTag                  10212


@implementation UIViewController (Customize)

- (BOOL)isModal{
    if (self.navigationController != nil) {
        return NO;
    }
    return YES;
}

- (BOOL)isNavRootViewController{
    if(self.navigationController == nil){
        return NO;
    }
    
    UIViewController * pRoot    = [self.navigationController.viewControllers safeObjectAtIndex:0];
    if (nil == pRoot || self == pRoot) {
        return YES;
    }
    return NO;
}


-(void)closeViewAnimated:(BOOL)animated{
    if([self isModal])
        [self dismissViewControllerAnimated:animated completion:nil];
    else {
        [self.navigationController popViewControllerAnimated:animated];
    }
}

- (UIButton*)buttonWithNavBackStyle:(NSString*)strTitle{
    return [self buttonWithNavBackStyle:strTitle withBtnImageName:@"btn_back.png"];
}

- (UIButton*)buttonWithNavBackStyle:(NSString*)strTitle withBtnImageName:(NSString *)strImgName {
    UIFont* font            = [UIFont boldSystemFontOfSize:14.0f];
    CGSize nSize            = [strTitle sizeWithAttributes:@{NSFontAttributeName:font}];
    int nNewWidth           = nSize.width + 29 < 55 ? 55 : nSize.width + 26;
    
    UIButton* btn            = [[UIButton alloc] initWithFrame:CGRectMake(0,0,nNewWidth,kNavBtnHeight)];
    btn.titleLabel.font        = font;
    btn.titleLabel.shadowColor = [UIColor blackColor];
    btn.titleLabel.shadowOffset = CGSizeMake(0, -1.0);
    UIEdgeInsets edge = btn.titleEdgeInsets;
    edge.left +=5;
    btn.titleEdgeInsets = edge;
    
    [btn setTitle:strTitle forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    return btn;
}

- (UIButton*)buttonWithNavLeftStyle:(NSString*)strTitle withBtnImageName:(NSString *)strImgName {
    UIFont* font            = [UIFont boldSystemFontOfSize:12.0f];
    CGSize nSize            = [strTitle sizeWithAttributes:@{NSFontAttributeName:font}];
    int nNewWidth           = nSize.width + 25 < 52 ? 52 : nSize.width + 16;
    
    UIButton* btn            = [[UIButton alloc] initWithFrame:CGRectMake(0,0,nNewWidth,kNavBtnHeight)];
    btn.titleLabel.font        = font;
    btn.titleLabel.shadowColor = [UIColor blackColor];
    btn.titleLabel.shadowOffset = CGSizeMake(0, -1.0);
    UIEdgeInsets edge = btn.titleEdgeInsets;
    edge.left +=5;
    btn.titleEdgeInsets = edge;
    
    [btn setTitle:strTitle forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    return btn;
}

- (UIButton*)buttonWithConfirmStyle:(NSString*)strTitle{
    return [self buttonWithConfirmStyle:strTitle imageName:nil];
}

- (UIButton*)buttonWithIcon:(NSString*)strImgName{
    UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0,0,44,kNavBtnHeight)];
    [button setImage:[UIImage imageNamed:strImgName] forState:UIControlStateNormal];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    return button;
}

- (UIButton*)buttonWithConfirmStyle:(NSString*)strTitle imageName:(NSString *)strImgName{
    UIFont* font            = [UIFont font17];
    CGSize nSize            = [strTitle sizeWithAttributes:@{NSFontAttributeName:font}];
    int nNewWidth           = nSize.width + 25 < 52 ? 52 : nSize.width + 16;
    //wujuan.begin
    if([StringUtil isEmpty:strTitle] && ![StringUtil isEmpty:strImgName]) {
        nNewWidth = 20.0f;
    }
    //wujuan.end
    UIButton* btn            = [[UIButton alloc] initWithFrame:CGRectMake(0,0,nNewWidth,kNavBtnHeight)];
    btn.tag  = kNavBtnTag;
    btn.titleLabel.font        = font;
    btn.titleLabel.shadowColor = [UIColor blackColor];
    btn.titleLabel.shadowOffset = CGSizeMake(0, -1.0);
    //    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -20);
    
    
    [btn setTitle:strTitle forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor moBlack] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [btn setImage:[UIImage imageNamed:strImgName] forState:UIControlStateNormal];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(5, 10, 0, 5)];
    if([StringUtil isEmpty:strTitle] && ![StringUtil isEmpty:strImgName]) {
        [btn setImageEdgeInsets:UIEdgeInsetsMake(0,0,0,0)];
    }
    return btn;
}

- (void)addDefaultBackItem {
    UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0,0,44,kNavBtnHeight)];
    [button setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
    [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
}

- (void)addDefaultNavLeftBtnItem {
    UIButton *btn = [self buttonWithIcon:@"public_white_back"];
    if (btn) {
        [btn addTarget:self action:@selector(navLeftBtnItemClick:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* lItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = lItem;
    }
}

- (void)addNavRightBtnItem:(NSString *)title titleColor:(UIColor *)color {
    UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(0,0,44,kNavBtnHeight)];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    
    if (btn) {
        [btn addTarget:self action:@selector(navRightBtnItemClick:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* lItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = lItem;
    }
}

- (void)removeNavLeftButton{
    UIView *vi = [[UIView alloc] init];
    vi.backgroundColor = [UIColor clearColor];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:vi];
    self.navigationItem.leftBarButtonItem = item;
}

- (void)addNavConfirmButtonWithDefaultAction:(NSString*)strTitle{
    [self addNavConfirmButtonWithDefaultAction:strTitle backGroundImgName:nil];
}

- (void)addNavConfirmButtonWithIconName:(NSString *)strImgName{
    UIButton *btn = [self buttonWithIcon:strImgName];
    if (btn) {
        [btn addTarget:self action:@selector(confirmBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* rItem    = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = rItem;
    }
}

- (void)addNavConfirmButtonWithDefaultAction:(NSString*)strTitle backGroundImgName:(NSString *)strImgName{
    UIButton* btn;
    if (strImgName == nil) {
        btn = [self buttonWithConfirmStyle:strTitle];
    }else{
        btn = [self buttonWithConfirmStyle:strTitle imageName:strImgName];
    }
    if (btn) {
        btn.tag  = kNavBtnTag;
        [btn addTarget:self action:@selector(confirmBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem* rItem    = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItem = rItem;
    }
}

- (void)setConfirmButtonTitle:(NSString *)strTitle{
    UIButton *btn = (UIButton *)[self.navigationController.view viewWithTag:kNavBtnTag];
    if (btn && [[btn superview] isKindOfClass:[UINavigationBar class]]) {
        [btn setTitle:strTitle forState:UIControlStateNormal];
    }
}

- (void)setConfirmButtonEnabled:(BOOL)nEnabled{
    UIButton *btn = (UIButton *)[self.navigationController.view viewWithTag:kNavBtnTag];
    if (btn && [[btn superview] isKindOfClass:[UINavigationBar class]]) {
        btn.enabled = nEnabled;
    }
}


- (void)addNavConfirmButtonWithDefaultAction:(NSString*)strTitle iconImageName:(NSString *)strIconImgName{
    UIView *rView = [self viewWithConfirmStyle:strTitle iconImgName:strIconImgName];
    
    UIBarButtonItem* lItem    = [[UIBarButtonItem alloc] initWithCustomView:rView];
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = lItem;
}

- (void)addNavConfirmButtonWithDefaultAction:(NSString*)strTitle
                               iconImageName:(NSString *)strIconImgName
                         withBackGroundImage:(NSString*)bgImg{
    UIView *rView = [self viewWithConfirmStyle:strTitle iconImgName:strIconImgName withBackGroundImage:bgImg];
    
    UIBarButtonItem* lItem    = [[UIBarButtonItem alloc] initWithCustomView:rView];
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = lItem;
}


- (UIView *)viewWithConfirmStyle:(NSString *)strTitle
                     iconImgName:(NSString *)strIconImgName{
    return [self viewWithConfirmStyle:strTitle iconImgName:strIconImgName withBackGroundImage:@"btn_next_blue.png"];
}

- (UIView *)viewWithConfirmStyle:(NSString *)strTitle
                     iconImgName:(NSString *)strIconImgName
             withBackGroundImage:(NSString*)strBgImg{
    UIFont* font            = [UIFont boldSystemFontOfSize:11.0f];
    CGSize nSize            = [strTitle sizeWithAttributes:@{NSFontAttributeName:font}];
    
    UIImageView *iconImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:strIconImgName]];
    CGFloat imgWidth    = iconImgView.image.size.width /2.0f;
    CGFloat imgHeight   = iconImgView.image.size.height /2.0f;
    iconImgView.frame = CGRectMake(kNavBtnIconImgXMagin, (kNavBtnHeight - imgHeight)/2.0f, imgWidth , imgHeight);
    CGFloat labXMagin = CGRectGetMaxX(iconImgView.frame) + kNavBtnIconLabXMagin;
    UILabel *rLab = [[UILabel alloc] init];
    if (strTitle) {
        rLab.frame = CGRectMake(labXMagin, (kNavBtnHeight - nSize.height)/2.0f, nSize.width, nSize.height);
        rLab.text = strTitle;
        rLab.shadowColor    = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        rLab.shadowOffset   = CGSizeMake(0, -1);
        rLab.textColor      = [UIColor whiteColor];
        rLab.backgroundColor = [UIColor clearColor];
        rLab.font = font;
    }
    
    UIView *rView = [[UIView alloc] init];
    CGFloat rViewWidth = labXMagin + CGRectGetWidth(rLab.frame) + kNavBtnIconLabRightMagin;
    if (!strTitle) {
        rViewWidth -= 5;
    }
    rView.frame =  CGRectMake(0, 0, rViewWidth, kNavBtnHeight);
    UIButton* btn = [[UIButton alloc] initWithFrame:rView.frame];
    if (btn) {
        [rView addSubview:btn];
        [rView addSubview:iconImgView];
        [rView addSubview:rLab];
        
        
        [btn addTarget:self action:@selector(navLeftBtnItemClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return rView;
}

- (void)setNavBarTitle:(NSString *)title {
    UILabel *label = [self navTitleLabel];
    label.text = title;
    self.navigationItem.titleView = label;
}

- (void)setNavBarTitle:(NSString *)title color:(UIColor* )color {
    UILabel *label = [self navTitleLabel];
    label.textColor = color;
    label.text = title;
    self.navigationItem.titleView = label;
}

- (void)setNavigationTitleColor:(UIColor *)color {
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:color}];
}

- (UILabel *)navTitleLabel {
    UILabel *navTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 140, 24)];
    navTitleLabel.backgroundColor = [UIColor clearColor];
    navTitleLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    navTitleLabel.textAlignment = NSTextAlignmentCenter;
    navTitleLabel.textColor = [UIColor moBlack];
    
    return navTitleLabel;
}

#pragma mark - action

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backBtnClick:(id)sender
{
    [self closeViewAnimated:YES];
}

- (void)confirmBtnClick:(id)sender
{
    
}

- (void)navLeftBtnItemClick:(id)sender
{
    
}

- (void)navRightBtnItemClick:(id)sender
{
    
}

- (void)cancelButtonClick:(UIButton *)button
{
    
}
- (void)leftButtonClick:(id)sender
{
    
    
}

- (UIView *)addNavgationBar {
    return [self addNavgationBar:nil];
}

- (UIView *)addNavgationBar:(UIColor *)titleColor {
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom)];
    bar.backgroundColor = [UIColor whiteColor];
    UIButton *titleBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [titleBnt setTitle:Lang(self.title) forState:UIControlStateNormal];
    [bar addSubview:titleBnt];
    
    UIButton *backBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backBnt addAction:^(UIButton *btn) {
        if (self.navigationController) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    [bar addSubview:backBnt];
    
    if(titleColor && [titleColor isKindOfClass:[UIColor class]]) {
        [titleBnt setTitleColor:titleColor forState:UIControlStateNormal];
        [backBnt setImage:[[UIImage imageNamed:@"public_black_back"] jsq_imageMaskedWithColor:titleColor] forState:UIControlStateNormal];
        [backBnt setImage:[[UIImage imageNamed:@"public_black_back"] jsq_imageMaskedWithColor:titleColor] forState:UIControlStateHighlighted];
    } else {
        [backBnt setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateNormal];
        [backBnt setImage:[UIImage imageNamed:@"public_black_back"] forState:UIControlStateHighlighted];
    }

    [self.view addSubview:bar];
    [bar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(@(0));
        make.height.equalTo(@(NavigationBar_Bottom));
    }];
    
    [backBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.bottom.equalTo(@(-5.5));
        make.width.height.equalTo(@(33));
    }];
    
    [titleBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(48));
        make.right.equalTo(@(-48));
        make.centerY.equalTo(backBnt.mas_centerY);
        make.width.height.equalTo(@(20));
    }];
    return bar;
}

- (UIView *)addLucencyNavgationBar {
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom)];
    UIButton *titleBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [titleBnt setTitle:Lang(self.title) forState:UIControlStateNormal];
    titleBnt.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
    titleBnt.tag = 10000;
    [bar addSubview:titleBnt];
    
    UIButton *backBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backBnt setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
    [backBnt setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateHighlighted];
    [backBnt addAction:^(UIButton *btn) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    backBnt.tag = 10001;
    [bar addSubview:backBnt];

    [self.view addSubview:bar];
    [bar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(@(0));
        make.height.equalTo(@(NavigationBar_Bottom));
    }];
    
    [backBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.bottom.equalTo(@(-5.5));
        make.width.height.equalTo(@(33));
    }];
    
    [titleBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(48));
        make.right.equalTo(@(-48));
        make.centerY.equalTo(backBnt.mas_centerY);
        make.width.height.equalTo(@(20));
    }];
    return bar;
}

- (UIView *)addLucencyNavgationBar:(FinishedBlock)backAction {
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom)];
    UIButton *titleBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [titleBnt setTitle:Lang(self.title) forState:UIControlStateNormal];
    titleBnt.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
    titleBnt.tag = 10000;
    [bar addSubview:titleBnt];
    
    UIButton *backBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backBnt setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
    [backBnt setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateHighlighted];
    [backBnt addAction:backAction];
    backBnt.tag = 10001;
    [bar addSubview:backBnt];

    [self.view addSubview:bar];
    [bar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(@(0));
        make.height.equalTo(@(NavigationBar_Bottom));
    }];
    
    [backBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.bottom.equalTo(@(-5.5));
        make.width.height.equalTo(@(33));
    }];
    
    [titleBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(48));
        make.right.equalTo(@(-48));
        make.centerY.equalTo(backBnt.mas_centerY);
        make.width.height.equalTo(@(20));
    }];
    return bar;
}

- (UIView *)addNavgationBar:(NSString *)rightIcon rightAction:(FinishedBlock)rightBlock {
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom)];

    UIButton *titleBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [titleBnt setTitle:Lang(self.title) forState:UIControlStateNormal];
    titleBnt.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
    titleBnt.tag = 10000;
    [bar addSubview:titleBnt];

    UIButton *backBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backBnt setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
    [backBnt setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateHighlighted];
    [backBnt addAction:^(UIButton *btn) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    backBnt.tag = 10001;
    [bar addSubview:backBnt];

    UIButton *rightBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBnt setImage:[UIImage imageNamed:rightIcon] forState:UIControlStateNormal];
    [rightBnt addAction:^(UIButton *btn) {
        Block_Exec(rightBlock,nil);
    }];
    rightBnt.tag = 10002;
    [bar addSubview:rightBnt];

    [self.view addSubview:bar];
    [bar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(@(0));
        make.height.equalTo(@(NavigationBar_Bottom));
    }];

    [backBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.bottom.equalTo(@(-5.5));
        make.width.height.equalTo(@(33));
    }];
    
    [rightBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-15));
        make.centerY.equalTo(titleBnt.mas_centerY);
        make.width.height.equalTo(@(21));
    }];


    [titleBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(48));
        make.right.equalTo(@(-48));
        make.centerY.equalTo(backBnt.mas_centerY);
        make.width.height.equalTo(@(20));
    }];
    return bar;
}
- (UIView *)addNavgationBarWithRightTitle:(NSString *)rightTitle rightAction:(FinishedBlock)rightBlock {
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom)];

    UIButton *titleBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [titleBnt setTitle:Lang(self.title) forState:UIControlStateNormal];
    titleBnt.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
    titleBnt.tag = 10000;
    [bar addSubview:titleBnt];

    UIButton *backBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backBnt setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateNormal];
    [backBnt setImage:[UIImage imageNamed:@"public_white_back"] forState:UIControlStateHighlighted];
    [backBnt addAction:^(UIButton *btn) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    backBnt.tag = 10001;
    [bar addSubview:backBnt];

    UIButton *rightBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBnt.titleLabel.font = [UIFont systemFontOfSize:16];
    [rightBnt setTitle:rightTitle forState:UIControlStateNormal];
    [rightBnt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBnt addAction:^(UIButton *btn) {
        Block_Exec(rightBlock,nil);
    }];
    rightBnt.tag = 10002;
    [bar addSubview:rightBnt];

    [self.view addSubview:bar];
    [bar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(@(0));
        make.height.equalTo(@(NavigationBar_Bottom));
    }];

    [backBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.bottom.equalTo(@(-5.5));
        make.width.height.equalTo(@(33));
    }];

    [rightBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-15));
        make.centerY.equalTo(titleBnt.mas_centerY);
        //make.width.height.equalTo(@(21));
    }];


    [titleBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(48));
        make.right.equalTo(@(-48));
        make.centerY.equalTo(backBnt.mas_centerY);
        make.width.height.equalTo(@(20));
    }];
    return bar;
}

- (UIView *)addNavgationLeftButton:(NSString *)iconName {
    UIView *bar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NavigationBar_Bottom)];

    UIButton *titleBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [titleBnt setTitle:Lang(self.title) forState:UIControlStateNormal];
    titleBnt.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
    titleBnt.tag = 10000;
    [bar addSubview:titleBnt];

    UIImage *iconImg = [UIImage imageNamed:iconName];
    UIButton *backBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [backBnt setImage:iconImg forState:UIControlStateNormal];
    [backBnt setImage:iconImg forState:UIControlStateHighlighted];
    [backBnt addAction:^(UIButton *btn) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    backBnt.tag = 10001;
    [bar addSubview:backBnt];

    [self.view addSubview:bar];
    [bar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(@(0));
        make.height.equalTo(@(NavigationBar_Bottom));
    }];

    [backBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(15));
        make.bottom.equalTo(@(-5.5));
        make.width.equalTo(@(iconImg.size.width));
        make.height.equalTo(@(iconImg.size.height));
    }];

    return bar;
}

//移除最后一个页面
- (void)removeLastVC {
    NSMutableArray *controllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [controllers removeLastObject];
    self.navigationController.viewControllers = controllers;
}

//移除前面的指定的vc
- (void)removeLastVC:(NSString *)vc {
    NSMutableArray *controllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    __block NSUInteger index = -1;
    [controllers enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(obj != [controllers lastObject] && [obj isKindOfClass:NSClassFromString(vc)]) {
            index = idx;
            *stop = YES;
            return;
        }
    }];
    
    if(index != -1) {
        [controllers removeObjectAtIndex:index];
        self.navigationController.viewControllers = controllers;
    }
}
@end



