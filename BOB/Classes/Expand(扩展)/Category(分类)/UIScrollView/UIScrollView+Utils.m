//
//  UIScrollView+Utils.m
//  BOB
//
//  Created by mac on 2020/8/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UIScrollView+Utils.h"

@implementation UIScrollView (Utils)

- (BOOL)isScrollToBottom {
    CGFloat height = self.frame.size.height;
    CGFloat contentOffsetY = self.contentOffset.y;
    CGFloat bottomOffset = self.contentSize.height - contentOffsetY;
    if (bottomOffset <= height + 10) {
        return YES;
    } else {
        return NO;
    }
}

@end
