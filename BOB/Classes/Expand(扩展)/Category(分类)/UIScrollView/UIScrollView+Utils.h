//
//  UIScrollView+Utils.h
//  BOB
//
//  Created by mac on 2020/8/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (Utils)

///是否滑到了底部
- (BOOL)isScrollToBottom;

@end

NS_ASSUME_NONNULL_END
