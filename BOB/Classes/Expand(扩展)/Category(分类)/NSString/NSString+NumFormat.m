//
//  NSString+NumFormat.m
//  MoPal_Developer
//
//  Created by Fly on 15/8/12.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "NSString+NumFormat.h"

#define kMaxShowNumber  9999

@implementation NSString (NumFormat)

+ (NSString*)getFormatTotalString:(NSInteger)count{
    if (count <= kMaxShowNumber) {
        return [NSString stringWithFormat:@"共%ld条",(long)count];
    }
    return [NSString stringWithFormat:@"共%d+",kMaxShowNumber];
}

+ (NSString*)getFormatBrowseCount:(NSInteger)count{
    if (count <= 1000) {
        return [NSString stringWithFormat:@"%ld",(long)count];
    }
    return [NSString stringWithFormat:@"%ldk",(long)(count/1000)];
}

- (BOOL)checkPasswordValidate {
    if(self.length < 6 || self.length > 30) {
        [NotifyHelper showMessageWithMakeText:Lang(@"密码长度必须为6-30位")];
        return NO;
    } else if(!([self checkIsHaveNumAndLetter] == 3)) {
        [NotifyHelper showMessageWithMakeText:Lang(@"密码必须包含字母和数字")];
        return NO;
    }
    return YES;
}

-(int)checkIsHaveNumAndLetter {
    //数字条件
    
    NSRegularExpression *tNumRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:NSRegularExpressionCaseInsensitive error:nil];
    
    
    
    //符合数字条件的有几个字节
    
    NSUInteger tNumMatchCount = [tNumRegularExpression numberOfMatchesInString:self
                                 
                                                                       options:NSMatchingReportProgress
                                 
                                                                         range:NSMakeRange(0, self.length)];
    
    
    
    //英文字条件
    
    NSRegularExpression *tLetterRegularExpression = [NSRegularExpression regularExpressionWithPattern:@"[A-Za-z]" options:NSRegularExpressionCaseInsensitive error:nil];
    
    
    
    //符合英文字条件的有几个字节
    
    NSUInteger tLetterMatchCount = [tLetterRegularExpression numberOfMatchesInString:self options:NSMatchingReportProgress range:NSMakeRange(0, self.length)];
    
    
    
    if (tNumMatchCount == self.length) {
        
        //全部符合数字，表示沒有英文
        
        return 1;
        
    } else if (tLetterMatchCount == self.length) {
        
        //全部符合英文，表示沒有数字
        
        return 2;
        
    } else if (tNumMatchCount + tLetterMatchCount == self.length) {
        
        //符合英文和符合数字条件的相加等于密码长度
        
        return 3;
        
    } else {
        
        return 4;
        
        //可能包含标点符号的情況，或是包含非英文的文字，这里再依照需求详细判断想呈现的错误
        
    }
    
}

- (BOOL)isPureInt{
    NSScanner* scan = [NSScanner scannerWithString:self];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

- (NSString *)showNoUnitFormatPrice{
    if (![self isKindOfClass:[NSString class]]) {
        return self;
    }
    //过滤货币符号
    NSString *resultString = [self copy];
    for (int i = 0; i < [resultString length]; i++) {
        if ([resultString characterAtIndex:i] < '0' ||
            [resultString characterAtIndex:i] > '9') {
            continue;
        } else {
            if (i == 0) break;
            resultString = [resultString substringFromIndex:i];
            break;
        }
    }
    return resultString;
}

- (NSString *)showIntFormatPrice:(BOOL)needUnit{
    if (![self isKindOfClass:[NSString class]]) {
        return self;
    }
    NSString *resultString = [self copy];
    if (!needUnit) {
        resultString = [self showNoUnitFormatPrice];
    }
    if ([resultString rangeOfString:@"."].location != NSNotFound) {
        return [[resultString componentsSeparatedByString:@"."] firstObject];
    }
    return resultString;
}

- (NSString *)formatterMoney {
    
    if ([self rangeOfString:@"."].location != NSNotFound) {
        NSString *left = [[self componentsSeparatedByString:@"."] firstObject];
        NSString *right = [[self componentsSeparatedByString:@"."] lastObject];
        if(right.length >= 2) {
            
            if(![[right substringWithRange:NSMakeRange(1, 1)] isEqualToString:@"0"]) {
                right = [right substringToIndex:2];
            } else if(![[right substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"0"]) {
                right = [right substringToIndex:1];
            } else {
                right = nil;
            }
            
            if(right) {
                return $str(@"%@.%@",left,right);
            } else {
                return left;
            }
        } else if ([right isEqualToString:@"0"]) {
            return left;
        }
        
    } else {
        return self;
    }
    return self;
}

+ (NSString *)deductionPointTranferCash:(double)point {
    NSDecimalNumberHandler *handler = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundBankers scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    NSDecimalNumber *number = [NSDecimalNumber decimalNumberWithString:[@(point) description]];
    NSDecimalNumber *cash = [number decimalNumberByDividingBy:[NSDecimalNumber decimalNumberWithString:@"1"] withBehavior:handler];
    return cash.stringValue;
}

- (NSString *)starReplace {
    if(self.length > 0) {
        NSInteger count = self.length;
        NSString *str = self;
        NSString *resultStr = nil;
        for(NSInteger i = count/4; i < count/2; i++) {
            resultStr = [str stringByReplacingCharactersInRange:NSMakeRange(i+1,1) withString:@"*"];
            str = resultStr;
        }
        return resultStr ?: self;
    }
    return nil;
}

- (NSString *)starReplaceInTelEmail {
    if(self.length > 0) {
        if([self containsString:@"@"]) { //邮箱
            NSArray *arr = [self componentsSeparatedByString:@"@"];
            return [NSString stringWithFormat:@"%@@%@",[[arr firstObject] starReplace],[arr lastObject]];
        } else { //手机号
            NSInteger count = self.length;
            NSString *str = self;
            NSString *resultStr = nil;
            for(NSInteger i = count/4; i <= count/2; i++) {
                resultStr = [str stringByReplacingCharactersInRange:NSMakeRange(i+1,1) withString:@"*"];
                str = resultStr;
            }
            return resultStr ?: self;
        }
    }
    return self;
}

- (NSString *)dateAddDot {
    if(self.length > 0) {
        NSInteger count = self.length;
        NSString *day = [self substringFromIndex:count-2];
        NSString *month = [self substringWithRange:NSMakeRange(count-4, 2)];
        NSString *year = [self substringToIndex:count-4];
        return [NSString stringWithFormat:@"%@.%@.%@",year,month,day];
    }
    return self;
}

/**
 *  将阿拉伯数字转换为中文数字
 */
+ (NSString *)translationArabicNum:(NSInteger)arabicNum
{
    NSString *arabicNumStr = [NSString stringWithFormat:@"%ld",(long)arabicNum];
    NSArray *arabicNumeralsArray = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"0"];
    NSArray *chineseNumeralsArray = @[@"一",@"二",@"三",@"四",@"五",@"六",@"七",@"八",@"九",@"零"];
    NSArray *digits = @[@"个",@"十",@"百",@"千",@"万",@"十",@"百",@"千",@"亿",@"十",@"百",@"千",@"兆"];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:chineseNumeralsArray forKeys:arabicNumeralsArray];
    
    if (arabicNum < 20 && arabicNum > 9) {
        if (arabicNum == 10) {
            return @"十";
        }else{
            NSString *subStr1 = [arabicNumStr substringWithRange:NSMakeRange(1, 1)];
            NSString *a1 = [dictionary objectForKey:subStr1];
            NSString *chinese1 = [NSString stringWithFormat:@"十%@",a1];
            return chinese1;
        }
    }else{
        NSMutableArray *sums = [NSMutableArray array];
        for (int i = 0; i < arabicNumStr.length; i ++)
        {
            NSString *substr = [arabicNumStr substringWithRange:NSMakeRange(i, 1)];
            NSString *a = [dictionary objectForKey:substr];
            NSString *b = digits[arabicNumStr.length -i-1];
            NSString *sum = [a stringByAppendingString:b];
            if ([a isEqualToString:chineseNumeralsArray[9]])
            {
                if([b isEqualToString:digits[4]] || [b isEqualToString:digits[8]])
                {
                    sum = b;
                    if ([[sums lastObject] isEqualToString:chineseNumeralsArray[9]])
                    {
                        [sums removeLastObject];
                    }
                }else
                {
                    sum = chineseNumeralsArray[9];
                }
                
                if ([[sums lastObject] isEqualToString:sum])
                {
                    continue;
                }
            }
            
            [sums addObject:sum];
        }
        NSString *sumStr = [sums  componentsJoinedByString:@""];
        NSString *chinese = [sumStr substringToIndex:sumStr.length-1];
        return chinese;
    }
}
@end
