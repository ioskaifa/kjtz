//
//  NSString+NumFormat.h
//  MoPal_Developer
//
//  Created by Fly on 15/8/12.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NumFormat)

//共条
+ (NSString*)getFormatTotalString:(NSInteger)count;

///阿拉伯数字转化为汉语数字
+ (NSString *)translationArabicNum:(NSInteger)arabicNum;

//点赞数量超过1000（超过1K，单位显示k）
+ (NSString*)getFormatBrowseCount:(NSInteger)count;

///判断密码有效性，字母和数字混合
- (BOOL)checkPasswordValidate;

- (BOOL)isPureInt;

- (NSString *)showNoUnitFormatPrice;
- (NSString *)showIntFormatPrice:(BOOL)needUnit;

- (NSString *)formatterMoney;

+ (NSString *)deductionPointTranferCash:(double)point;

- (NSString *)starReplace;

- (NSString *)starReplaceInTelEmail;

- (NSString *)dateAddDot;
@end
