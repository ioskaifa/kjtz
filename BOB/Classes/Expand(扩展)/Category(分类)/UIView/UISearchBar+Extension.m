//
//  UISearchBar+Extension.m
//  BOB
//
//  Created by mac on 2020/1/6.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UISearchBar+Extension.h"

@implementation UISearchBar (Extension)

- (void)setCornerRadius {
    
}

- (UITextField *)findTextField:(UIView *)superView {
    for(UIView *view in superView.subviews) {
        NSLog(@"findTextField: %@",[view class]);
        if([view isKindOfClass:[UITextField class]]) {
            return (UITextField *)view;
        } else {
            UIView *findView = [self findTextField:view];
            if([findView isKindOfClass:[UITextField class]]) {
                return (UITextField *)findView;
            }
        }
    }
    return nil;
}
@end
