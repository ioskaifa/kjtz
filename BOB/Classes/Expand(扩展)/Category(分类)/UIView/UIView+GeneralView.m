//
//  UIView+GeneralView.m
//  JiuJiuEcoregion
//
//  Created by mac on 2019/6/3.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import "UIView+GeneralView.h"
#import "UIImage+Utils.h"
#import "UIView+AZGradient.h"

@implementation UIView (GeneralView)
+ (UIView *)leftLineRightTitleView:(NSString *)title lineColor:(UIColor *)lineColor offset:(CGFloat)offset {
    UIButton *bnt = [[UIButton alloc] init];
    [bnt setTitle:[NSString stringWithFormat:@" %@",title] forState:UIControlStateNormal];
    bnt.titleLabel.font = [UIFont systemFontOfSize:16];
    bnt.imageEdgeInsets = UIEdgeInsetsMake(0, offset, 0, -offset);
    bnt.titleEdgeInsets = UIEdgeInsetsMake(0, offset, 0, -offset);
    [bnt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [bnt setImage:[UIImage createImageWithColor:lineColor size:CGSizeMake(4, 15)] forState:UIControlStateNormal];
    
    return bnt;
}

+ (UIView *)leftLineRightTitleView:(NSString *)title titleColor:(UIColor *)titleColor lineColor:(UIColor *)lineColor offset:(CGFloat)offset {
    UIButton *bnt = [[UIButton alloc] init];
    [bnt setTitle:[NSString stringWithFormat:@" %@",title] forState:UIControlStateNormal];
    bnt.titleLabel.font = [UIFont systemFontOfSize:16];
    bnt.imageEdgeInsets = UIEdgeInsetsMake(0, offset, 0, -offset);
    bnt.titleEdgeInsets = UIEdgeInsetsMake(0, offset, 0, -offset);
    [bnt setTitleColor:titleColor forState:UIControlStateNormal];
    [bnt setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [bnt setImage:[UIImage createImageWithColor:lineColor size:CGSizeMake(4, 15)] forState:UIControlStateNormal];
    
    return bnt;
}

+ (UIButton *)userGradeButton:(NSString *)grade {
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:@"VIP"] forState:UIControlStateNormal];
    [button setTitle:grade forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:10];
    [button setTitleColor:[UIColor colorWithHexString:@"#D9A336"] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithHexString:@"#25282D"];
    button.clipsToBounds = YES;
    button.layer.cornerRadius = 2;
    [button sizeToFit];
    button.width += 10;
    return button;
}

+ (UIButton *)goldenButton:(NSString *)title font:(UIFont *)font cornerRadius:(CGFloat)cornerRadius {
    UIButton *button = [[UIButton alloc] init];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = font;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithHexString:@"#D9A336"];
    button.clipsToBounds = YES;
    button.layer.cornerRadius = cornerRadius;
    [button sizeToFit];
    button.width += 10;
    return button;
}

- (void)addDefaultGradientBackground {
    [self az_setGradientBackgroundWithColors:@[[UIColor colorWithHexString:@"#FF1010"],[UIColor colorWithHexString:@"#FE2F62"]] locations:nil startPoint:CGPointMake(0, 0) endPoint:CGPointMake(1, 0)];
}

+ (UIButton *)defaultButton:(NSString *)title action:(ActionBlock)action {
    UIButton *defaultButton = [UIButton buttonWithType:UIButtonTypeCustom];
    defaultButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [defaultButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    defaultButton.layer.masksToBounds = YES;
    defaultButton.layer.cornerRadius = 10;
    [defaultButton setTitle:title forState:UIControlStateNormal];
    defaultButton.clipsToBounds =NO;
    [defaultButton setBackgroundColor:[UIColor themeColor]];
    [defaultButton addAction:action];
    return defaultButton;
}

- (UIImageView *)addArrowIconWithRightOffset:(CGFloat)offset {
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
    [self addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(offset));
        make.centerY.equalTo(self);
        make.width.height.equalTo(@(10));
    }];
    return imgView;
}

- (UIButton *)addButtonWithRightOffset:(CGFloat)offset imageName:(NSString *)imageName action:(FinishedBlock)block {
    [[self viewWithTag:101010] removeFromSuperview];

    UIButton *bnt = [[UIButton alloc] init];
    bnt.tag = 101010;
    [bnt setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [self addSubview:bnt];
    [bnt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(offset));
        make.centerY.equalTo(self);
    }];
    [bnt addAction:^(UIButton *btn) {
        Block_Exec(block,nil);
    }];
    return bnt;
}

- (UIView *)addCustomWithRightOffset:(UIView *)view offset:(CGFloat)offset action:(FinishedBlock)block {
    if(([view isKindOfClass:[UIButton  class]] && [(UIButton *)view allTargets].count > 0) || block == nil) {

    } else {
        UIButton *bnt = [[UIButton alloc] init];
        [view addSubview:bnt];
        [bnt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(view);
        }];
        [bnt addAction:^(UIButton *btn) {
            Block_Exec(block,nil);
        }];
    }

    [self addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(offset));
        make.centerY.equalTo(self);
        make.width.equalTo(@(view.width));
        make.height.equalTo(@(view.height));
    }];

    return view;
}

- (UIView *)addFooterButton:(NSString *)title y:(CGFloat)y action:(ActionBlock)action {
    UIView *footer = [[UIView alloc] init];
    UIButton *defaultButton = [UIButton buttonWithType:UIButtonTypeCustom];
    defaultButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [defaultButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    defaultButton.layer.masksToBounds = YES;
    defaultButton.layer.cornerRadius = 22;
    [defaultButton setTitle:title forState:UIControlStateNormal];
    defaultButton.clipsToBounds = NO;
    [defaultButton setBackgroundColor:[UIColor themeColor]];
    [defaultButton addAction:action];
    [footer addSubview:defaultButton];
    [defaultButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(25));
        make.right.equalTo(@(-25));
        make.height.equalTo(@(44));
        make.centerY.equalTo(footer);
    }];
    [self addSubview:footer];
    [footer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(@(0));
        make.top.equalTo(@(y));
        make.height.equalTo(@(44));
    }];
    return footer;
}

- (UIView *)addFooterButton:(NSString *)title height:(CGFloat)height action:(ActionBlock)action {
    UIView *footer = [[UIView alloc] init];
    UIButton *defaultButton = [UIButton buttonWithType:UIButtonTypeCustom];
    defaultButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [defaultButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    defaultButton.layer.masksToBounds = YES;
    defaultButton.layer.cornerRadius = 22;
    [defaultButton setTitle:title forState:UIControlStateNormal];
    defaultButton.clipsToBounds = NO;
    [defaultButton setBackgroundColor:[UIColor themeColor]];
    [defaultButton addAction:action];
    [footer addSubview:defaultButton];
    [defaultButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(25));
        make.right.equalTo(@(-25));
        make.height.equalTo(@(44));
        make.centerY.equalTo(footer);
    }];
    [self addSubview:footer];
    [footer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(@(0));
        make.bottom.equalTo(@(-safeAreaInsetBottom()));
        make.height.equalTo(@(height));
    }];
    return footer;
}

+ (UIButton *)bothEndsButton:(NSMutableAttributedString *)leftTitle rightTitle:(NSMutableAttributedString *)rightTitle action:(ActionBlock)action {
    UIButton *bothEndsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [bothEndsButton addAction:action];

    UILabel *leftLbl = [[UILabel alloc] init];
    leftLbl.attributedText = leftTitle;
    [bothEndsButton addSubview:leftLbl];
    [leftLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bothEndsButton);
    }];

    UILabel *rightLbl = [[UILabel alloc] init];
    rightLbl.attributedText = rightTitle;
    rightLbl.textAlignment = NSTextAlignmentRight;
    [bothEndsButton addSubview:rightLbl];
    [rightLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bothEndsButton);
    }];
    return bothEndsButton;
}

+ (UIView *)bothEndsCell:(id)leftTitle rightTitle:(NSString *)rightTitle rightIcon:(NSString *)rightIcon needArrow:(BOOL)needArrow action:(ActionBlock)action {
    
    UIView *bgView = [[UIView alloc] init];
    bgView.myLeft = 15;
    bgView.myRight = 0;
    bgView.myTop = 0;
    bgView.myBottom = 0;
    bgView.myHeight = 52;
    
    UIButton *rightBnt = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBnt setImage:[UIImage imageNamed:rightIcon] forState:UIControlStateNormal];
    [rightBnt addAction:action];
    
    UIImageView *arrow = nil;
    if(needArrow) {
        arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow"]];
        [bgView addSubview:arrow];
        [arrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-15));
            make.centerY.equalTo(bgView);
        }];
    }

    UILabel *leftLbl = [[UILabel alloc] init];
    if([leftTitle isKindOfClass:[NSMutableAttributedString class]]) {
        leftLbl.attributedText = leftTitle;
    } else {
        leftLbl.text = leftTitle;
        leftLbl.textColor = [UIColor colorWithHexString:@"#666666"];
        leftLbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
    }
    
    [bgView addSubview:leftLbl];
    [leftLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(bgView);
    }];
    
    [bgView addSubview:rightBnt];
    [rightBnt mas_makeConstraints:^(MASConstraintMaker *make) {
        if(needArrow) {
            make.right.equalTo(arrow.mas_left).offset(-5);
        } else {
            make.right.equalTo(@(-15));
        }
        make.centerY.equalTo(bgView);
        if(rightIcon == nil) {
            make.width.equalTo(@(0));
        } else {
            make.width.equalTo(@(22));
        }
        make.height.equalTo(@(22));
    }];
    
    UILabel *rightLbl = [[UILabel alloc] init];
    rightLbl.text = rightTitle;
    rightLbl.textAlignment = NSTextAlignmentRight;
    rightLbl.textColor = [UIColor colorWithHexString:@"#333333"];
    rightLbl.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    [bgView addSubview:rightLbl];
    [rightLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftLbl.mas_left);
        make.top.bottom.equalTo(@(0));
        make.right.equalTo(rightBnt.mas_left).offset(-3);
    }];
    
    UIView *line = [[UIView alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
    [bgView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(@(0));
        make.height.equalTo(@(0.5));
    }];
    
    return bgView;
}

+ (UIView *)leftLabelRightButton:(NSMutableAttributedString *)leftTitle rightTitle:(NSMutableAttributedString *)rightTitle leftGap:(CGFloat)leftGap righGap:(CGFloat)righGap buttonBorderColor:(UIColor *)buttonBorderColor buttonCornerRadius:(CGFloat)buttonCornerRadius buttonFont:(UIFont *)buttonFont buttonHeight:(CGFloat)buttonHeight action:(ActionBlock)action {

    UIView *bgView = [[UIView alloc] init];

    UILabel *leftLbl = [[UILabel alloc] init];
    leftLbl.attributedText = leftTitle;
    [bgView addSubview:leftLbl];
    [leftLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(leftGap));
        make.top.bottom.right.equalTo(@(0));
    }];

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setAttributedTitle:rightTitle forState:UIControlStateNormal];
    button.layer.borderColor = buttonBorderColor.CGColor;
    button.layer.cornerRadius = buttonCornerRadius;
    button.layer.borderWidth = 1;
    button.titleLabel.font = buttonFont;
    [button addAction:action];
    [bgView addSubview:button];
    bgView.custom = button;
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(righGap));
        make.centerY.equalTo(bgView.mas_centerY);
        make.height.equalTo(@(buttonHeight));
    }];
    return bgView;
}
@end


