//
//  UIView+GeneralView.h
//  JiuJiuEcoregion
//  用于业务中需要生成的自定义视图
//  Created by mac on 2019/6/3.
//  Copyright © 2019 AlphaGo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (GeneralView)
+ (UIButton *)userGradeButton:(NSString *)grade;
+ (UIButton *)goldenButton:(NSString *)title font:(UIFont *)font cornerRadius:(CGFloat)cornerRadius;
+ (UIView *)leftLineRightTitleView:(NSString *)title lineColor:(UIColor *)lineColor offset:(CGFloat)offset;
+ (UIView *)leftLineRightTitleView:(NSString *)title titleColor:(UIColor *)titleColor lineColor:(UIColor *)lineColor offset:(CGFloat)offset;
- (void)addDefaultGradientBackground;
+ (UIButton *)defaultButton:(NSString *)title action:(ActionBlock)action;
- (UIImageView *)addArrowIconWithRightOffset:(CGFloat)offset;

- (UIButton *)addButtonWithRightOffset:(CGFloat)offset imageName:(NSString *)imageName action:(FinishedBlock)block;
- (UIButton *)addCustomWithRightOffset:(UIView *)view offset:(CGFloat)offset action:(FinishedBlock)block;
- (UIView *)addFooterButton:(NSString *)title height:(CGFloat)height action:(ActionBlock)action;
- (UIView *)addFooterButton:(NSString *)title y:(CGFloat)y action:(ActionBlock)action;

///左右两边边一个文字，并且可以带有点击事件
+ (UIButton *)bothEndsButton:(NSMutableAttributedString *)leftTitle rightTitle:(NSMutableAttributedString *)rightTitle action:(ActionBlock)action;

+ (UIView *)bothEndsCell:(id)leftTitle rightTitle:(NSString *)rightTitle rightIcon:(NSString *)rightIcon needArrow:(BOOL)needArrow action:(ActionBlock)action;

///左边一个文字，右边一个按钮
+ (UIView *)leftLabelRightButton:(NSMutableAttributedString *)leftTitle rightTitle:(NSMutableAttributedString *)rightTitle leftGap:(CGFloat)leftGap righGap:(CGFloat)righGap buttonBorderColor:(UIColor *)buttonBorderColor buttonCornerRadius:(CGFloat)buttonCornerRadius buttonFont:(UIFont *)buttonFont buttonHeight:(CGFloat)buttonHeight action:(ActionBlock)action;
@end

NS_ASSUME_NONNULL_END


