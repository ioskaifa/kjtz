//
//  UILabel+Extension.h
//  Lcwl
//
//  Created by mac on 2018/12/2.
//  Copyright © 2018 lichangwanglai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Extension)
-(void)count:(CGFloat)toValue completion:(FinishedBlock)block;
- (void)addGestureRecognizer:(nullable id)target action:(nullable SEL)action;

/**
 *  改变行间距
 */
+ (void)changeLineSpaceForLabel:(UILabel *)label WithSpace:(float)space;

/**
 *  改变字间距
 */
+ (void)changeWordSpaceForLabel:(UILabel *)label WithSpace:(float)space;

/**
 *  改变行间距和字间距
 */
+ (void)changeSpaceForLabel:(UILabel *)label withLineSpace:(float)lineSpace WordSpace:(float)wordSpace;

- (void)autoMyLayoutSize;

@end

NS_ASSUME_NONNULL_END
