//
//  NSObject+GCD.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/10/15.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "NSObject+GCD.h"

@implementation NSObject (GCD)

- (void)performGCDOnMainThread:(void(^)(void))block
{
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}

- (void)performGCDAsyn:(void(^)(void))block
{
    if (block) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, block);
    }
}

- (void)performGCDAfter:(NSTimeInterval)seconds block:(void(^)(void))block
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, seconds * NSEC_PER_SEC);
    dispatch_after(popTime, queue, block);
}

@end
