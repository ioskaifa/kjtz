//
//  NSObject+GCD.h
//  MoPal_Developer
//
//  Created by yang.xiangbao on 15/10/15.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (GCD)

- (void)performGCDOnMainThread:(void(^)(void))block;

- (void)performGCDAsyn:(void(^)(void))block;

- (void)performGCDAfter:(NSTimeInterval)seconds block:(void(^)(void))block;

@end
