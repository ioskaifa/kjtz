//
//  MXRouter.h
//  MoPal_Developer
//
//  Created by yang.xiangbao on 16/3/28.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MXRoute(className,param) [MXRouter openURL:[NSString stringWithFormat:@"lcwl://%@",className] parameters:(param)];

@interface MXRouter : NSObject

+ (MXRouter *)sharedInstance;

- (void)configureCurrentVC:(UIViewController *)vc;

+ (void)openURL:(NSString *)URLPattern;

+ (void)openURL:(NSString *)URLPattern parameters:(NSDictionary *)parameters;

- (void)pushViewController:(UIViewController *)viewController;

- (void)popViewController;

- (UIViewController *)getTopViewController;

- (UINavigationController *)getTopNavigationController;

- (UINavigationController *)getMallTopNavigationController;

- (CGFloat)tabbarHeight;

- (NSInteger)curSelectedIndex;

+(void)openVC:(UIViewController*)vc from:(UIViewController*)from;

@end
