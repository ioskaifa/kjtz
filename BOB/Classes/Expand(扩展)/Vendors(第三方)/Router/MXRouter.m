//
//  MXRouter.m
//  MoPal_Developer
//
//  Created by yang.xiangbao on 16/3/28.
//  Copyright © 2016年 MoXian. All rights reserved.
//

#import "MXRouter.h"
#import "MRRouter.h"
#import "MainViewController.h"
#import "ResetPwdVC.h"
//#import "TextChangeVC.h"
//#import "MBWebVC.h"
//#import "SellCenterVC.h"
//#import "TransferOutVC1.h"
//#import "InOutRecordVC.h"
//#import "QianBaoInOutVC.h"
#import "ChatViewVC.h"
#import "MXAlertViewHelper.h"
#import "TextChangeVC.h"
//#import "PublishDynamic.h"
//#import "SocialContactVC.h"
//#import "SocialContactMainVC.h"
#import "YNPageViewController.h"
#import "MyCalendarManageVC.h"
#import "MyQRCodeVC.h"

#import "MesFavoriteTextInfoVC.h"
#import "MesFavoriteImageInfoVC.h"
#import "MesFavoriteVoiceInfoVC.h"
#import "MesFavoriteVideoInfoVC.h"

#import "FilePreviewVC.h"
#import "RealNameAuthVC.h"
#import "AuthManageVC.h"
#import "SelectFileVC.h"
#import "MXWebViewVC.h"
#import "ClientLoginVC.h"
#import "ClientLoginStatusVC.h"
#import "ChooseContactAtVC.h"
#import "ModifyPhoneNextVC.h"
#import "GoodInfoVC.h"
#import "ConfirmOrderVC.h"
#import "AddressManageVC.h"
#import "ShippingAddressVC.h"
#import "MyOrderVC.h"
#import "OrderInfoVC.h"
#import "GoodsSortListVC.h"
#import "MBWebVC.h"
#import "LivePlayerVC.h"
#import "ApplyShowResultVC.h"
#import "WalletDetailVC.h"

#import "MGGoodInfoVC.h"
#import "MGOrderInfoVC.h"

@interface MXRouter ()

@property (nonatomic, weak  ) UIViewController *vc;

@end

@implementation MXRouter

+ (MXRouter *)sharedInstance {
    static MXRouter *router;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        router = [[MXRouter alloc] init];
    });
    return router;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            [self configureRouter];
        });
    }
    return self;
}

- (void)configureCurrentVC:(UIViewController *)vc {
    self.vc = vc;
}

- (UINavigationController *)nav:(NSDictionary *)dict {
    return self.vc.navigationController;
}

- (UINavigationController *)getTopNavigationController {
    MainViewController *rootViewController = (MainViewController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
    return ([rootViewController isKindOfClass:[UITabBarController class]] ? rootViewController.selectedViewController : rootViewController);
}

- (UINavigationController *)getMallTopNavigationController {
    MainViewController *rootViewController = (MainViewController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
    return ([rootViewController isKindOfClass:[UITabBarController class]] ? rootViewController.selectedViewController : rootViewController);
}

- (UIViewController *)getTopViewController {
    UINavigationController *naviViewController = [self getTopNavigationController];
    return [naviViewController.viewControllers lastObject];
}

- (void)pushViewController:(UIViewController *)viewController {
    UIViewController *topVC = [self getTopViewController];
    [topVC.navigationController pushViewController:viewController animated:YES];
}

- (void)popViewController {
    UIViewController *topVC = [self getTopViewController];
    [topVC.navigationController popViewControllerAnimated:YES];
}

- (UIViewController *)rootViewController {
    // 获取根式控制器rootViewController，并将rootViewController设置为当前主控制器（防止菜单弹出时，部分被导航栏或标签栏遮盖）
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIViewController *rootVC = window.rootViewController;
    rootVC.definesPresentationContext = YES;
    return rootVC;
}

//获取当前选中的tab
- (NSInteger)curSelectedIndex {
    MainViewController *rootViewController = (MainViewController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
    if([rootViewController isKindOfClass:[MainViewController class]]) {
        return rootViewController.axcTabBar.selectIndex;
    }
    return -1;
}


- (CGFloat)tabbarHeight {
    MainViewController *mainViewController = (MainViewController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
    return mainViewController.axcTabBar.height;
}

+ (void)openURL:(NSString *)URLPattern {
    [MRRouter openURL:URLPattern];
}

+ (void)openURL:(NSString *)URLPattern parameters:(NSDictionary *)parameters {
    [MRRouter openURL:URLPattern parameters:parameters];
}

- (void)configureRouter {
    @weakify(self)
    
//    [MRRouter registerURL:@"lcwl://PublishDynamic" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
//        
//        PublishDynamic* vc = [[PublishDynamic alloc] init];
//        vc.photosArr = [param valueForKey:@"photosArr"];
//        vc.model = [param valueForKey:@"model"];
//        vc.block = [param valueForKey:@"block"];
//        vc.curSelectVideo = [[param valueForKey:@"curSelectVideo"] boolValue];
//        vc.curSelectImages = [param valueForKey:@"photosArr"];
//        vc.curSelectAssets = [param valueForKey:@"assets"];
//        vc.type = [[param valueForKey:@"type"] integerValue];
//        BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
//        [nav setDefinesPresentationContext:YES];
//        [nav setModalPresentationStyle:UIModalPresentationCurrentContext];
//        
//        UIViewController *weakSelf = [param valueForKey:@"weakSelf"];
//        if(weakSelf != nil && [weakSelf isKindOfClass:[UIViewController class]]) {
//            [weakSelf presentViewController:nav animated:YES completion:nil];
//        } else {
//            [[self rootViewController] presentViewController:nav animated:YES completion:nil];
//        }
//        
//        return nil;
//    }];
    
    [MRRouter registerURL:@"lcwl://call" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",[param valueForKey:@"tel"]]]];
        
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://showAlertViewWithMessage" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        [MXAlertViewHelper showAlertViewWithMessage:[param valueForKey:@"msg"] title:[param valueForKey:@"title"] okTitle:[param valueForKey:@"okTitle"] cancelTitle:[param valueForKey:@"cancelTitle"] completion:^(BOOL cancelled, NSInteger buttonIndex) {
            FinishedBlock block = [param valueForKey:@"block"];
            Block_Exec(block,@(buttonIndex));
        }];

        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://ResetPwdVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        ResetPwdVC *vc = [[ResetPwdVC alloc] init];
        vc.model = param[@"model"];
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://ConfirmOrderVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        ConfirmOrderVC *vc = [ConfirmOrderVC new];
        if (param[@"order_id"]) {
            vc.order_id = [param[@"order_id"] description];
        }
        if (param[@"fromType"]) {
            vc.fromType = [param[@"fromType"] integerValue];
        }
        if (param[@"isFree"]) {
            vc.isFree = [param[@"isFree"] boolValue];
        }
        if (param[@"isLive"]) {
            vc.isLive = [param[@"isLive"] boolValue];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://ApplyShowResultVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        ApplyShowResultVC *vc = [ApplyShowResultVC new];
        if (param[@"applyStatus"]) {
            vc.applyStatus = [param[@"applyStatus"] integerValue];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://LivePlayerVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        LivePlayerVC *vc = [LivePlayerVC new];
        if (param[@"bfUrl"]) {
            vc.bfUrl = [param[@"bfUrl"] description];
        }
        if (param[@"bfType"]) {
            vc.bfType = [param[@"bfType"] integerValue];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MyOrderVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        MyOrderVC *vc = [MyOrderVC new];
        if (param[@"orderStatus"]) {
            vc.orderStatus = [param[@"orderStatus"] integerValue];
        }
        if (param[@"isFree"]) {
            vc.isFree = [param[@"isFree"] boolValue];
        }
        if (param[@"isLive"]) {
            vc.isLive = [param[@"isLive"] boolValue];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://OrderInfoVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        OrderInfoVC *vc = [OrderInfoVC new];
        if (param[@"obj"]) {
            vc.obj = param[@"obj"];
        }
        if (param[@"orderID"]) {
            vc.orderID = param[@"orderID"];
        }
        if (param[@"isFree"]) {
            vc.isFree = [param[@"isFree"] boolValue];
        }
        if (param[@"isLive"]) {
            vc.isLive = [param[@"isLive"] boolValue];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://GoodsSortListVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        GoodsSortListVC *vc = [GoodsSortListVC new];
        if (param[@"category_id"]) {
            vc.category_id = [param[@"category_id"] description];
        }
        if (param[@"bargain_status"]) {
            vc.bargain_status = [param[@"bargain_status"] description];
        }
        if (param[@"day_sale_status"]) {
            vc.day_sale_status = [param[@"day_sale_status"] description];
        }
        if (param[@"new_product_status"]) {
            vc.product_status_new = [param[@"new_product_status"] description];
        }
        if (param[@"brand_buy_status"]) {
            vc.brand_buy_status = [param[@"brand_buy_status"] description];
        }
        if (param[@"have_good_status"]) {
            vc.have_good_status = [param[@"have_good_status"] description];
        }
        if (param[@"one_sla_status"]) {
            vc.one_sla_status = [param[@"one_sla_status"] description];
        }
        if (param[@"key_word"]) {
            vc.key_word = [param[@"key_word"] description];
        }
        if (param[@"isFree"]) {
            vc.isFree = [param[@"isFree"] boolValue];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://ShippingAddressVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        ShippingAddressVC *vc = [ShippingAddressVC new];
        if (param[@"block"]) {
            vc.block = param[@"block"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://AddressManageVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        AddressManageVC *vc = [AddressManageVC new];
        if (param[@"type"]) {
            vc.type = [param[@"type"] integerValue];
        }
        if (param[@"obj"]) {
            vc.obj = param[@"obj"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://WalletDetailVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        WalletDetailVC *vc = [WalletDetailVC new];
        if (param[@"op_type"]) {
            vc.op_type = param[@"op_type"];
        }
        if (param[@"op_order_no"]) {
            vc.op_order_no = param[@"op_order_no"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MBWebVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        MBWebVC *vc = [MBWebVC new];
        if (param[@"url"]) {
            vc.urlString = param[@"url"];
        }
        if (param[@"navTitle"]) {
            vc.navTitle = param[@"navTitle"];
        }
        if (param[@"content"]) {
            vc.content = param[@"content"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MXWebViewVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        MXWebViewVC *vc = [MXWebViewVC new];
        if (param[@"urlString"]) {
            vc.urlString = param[@"urlString"];
        }
        if (param[@"title"]) {
            vc.title = param[@"title"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://RealNameAuthVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        RealNameAuthVC *vc = [RealNameAuthVC new];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
        [self.vc presentViewController:nav animated:YES completion:nil];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://ClientLoginVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        ClientLoginVC *vc = [ClientLoginVC new];
        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
        [self.vc presentViewController:nav animated:YES completion:nil];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://ClientLoginStatusVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        ClientLoginStatusVC *vc = [ClientLoginStatusVC new];
        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
        [self.vc presentViewController:nav animated:YES completion:nil];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://ChooseContactAtVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        ChooseContactAtVC *vc = [ChooseContactAtVC new];
        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
        if (param[@"dataSourceArr"]) {
            vc.dataSourceArr = param[@"dataSourceArr"];
        }
        if (param[@"selectedBlock"]) {
            vc.selectedBlock = param[@"selectedBlock"];
        }
        if (param[@"isShowAllAt"]) {
            vc.isShowAllAt = [param[@"isShowAllAt"] boolValue];
        }
        [self.vc presentViewController:nav animated:YES completion:nil];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://SelectFileVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        SelectFileVC *vc = [SelectFileVC new];
        if (param[@"selectFileBlock"]) {
            vc.selectFileBlock = param[@"selectFileBlock"];
        }
        if (param[@"selectICloudBlock"]) {
            vc.selectICloudBlock = param[@"selectICloudBlock"];
        }
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        nav.modalPresentationStyle = UIModalPresentationFullScreen;
        [self.vc presentViewController:nav animated:YES completion:nil];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MyCalendarManageVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        MyCalendarManageVC *vc = [[MyCalendarManageVC alloc] init];
        if (param[@"type"]) {
            vc.type = [param[@"type"] integerValue];
        }
        if (param[@"obj"]) {
            vc.obj = param[@"obj"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://GoodInfoVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        GoodInfoVC *vc = [GoodInfoVC new];
        if (param[@"goods_id"]) {
            vc.goods_id = [param[@"goods_id"] description];
        }
        if (param[@"live_id"]) {
            vc.live_id = [param[@"live_id"] description];
        }
        if (param[@"isFree"]) {
            vc.isFree = [param[@"isFree"] boolValue];
        }
        if (param[@"isLive"]) {
            vc.isLive = [param[@"isLive"] boolValue];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MGGoodInfoVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        MGGoodInfoVC *vc = [MGGoodInfoVC new];
        if (param[@"goods_id"]) {
            vc.goods_id = [param[@"goods_id"] description];
        }
        if (param[@"orderStatus"]) {
            vc.orderStatus = [param[@"orderStatus"] integerValue];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MGOrderInfoVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        MGOrderInfoVC *vc = [MGOrderInfoVC new];
        if (param[@"part_id"]) {
            vc.part_id = [param[@"part_id"] description];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
//    [MRRouter registerURL:@"lcwl://SocialContactMainVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
//        SocialContactMainVC *mainVC = [SocialContactMainVC instanceVC];
//        SocialContactVC *vc = [mainVC.controllersM lastObject];
//        mainVC.otherId = param[@"otherId"];
//        mainVC.circle_back_img = param[@"circle_back_img"];
//        vc.otherId = param[@"otherId"];
//        
//        if(![StringUtil isEmpty:vc.otherId]) {
//            vc.type = SocialContactTypePersonal;
//        }
//        
//        [[self nav:param] pushViewController:mainVC animated:YES];
//        return nil;
//    }];
    
    [MRRouter registerURL:@"lcwl://ChatViewVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        
        ChatViewVC* chatVC = [[ChatViewVC alloc] initWithChatter:param[@"chatId"] conversationType:[param[@"type"]intValue]];
        chatVC.creatorRole = [param[@"creatorRole"] integerValue];
        chatVC.productInfo = param[@"productInfo"];
        
        [[self nav:param] pushViewController:chatVC animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://FilePreviewVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        
        FilePreviewVC *vc = [FilePreviewVC new];        
        if (param[@"meaage"]) {
            vc.meaage = param[@"meaage"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MyQRCodeVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        
        MyQRCodeVC *vc = [MyQRCodeVC new];
        if (param[@"title"]) {
            vc.title = param[@"title"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MesFavoriteTextInfoVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        
        MesFavoriteTextInfoVC *vc = [MesFavoriteTextInfoVC new];
        if (param[@"obj"]) {
            vc.obj = param[@"obj"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MesFavoriteImageInfoVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        
        MesFavoriteImageInfoVC *vc = [MesFavoriteImageInfoVC new];
        if (param[@"obj"]) {
            vc.obj = param[@"obj"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
   [MRRouter registerURL:@"lcwl://MesFavoriteVoiceInfoVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        
        MesFavoriteVoiceInfoVC *vc = [MesFavoriteVoiceInfoVC new];
        if (param[@"obj"]) {
            vc.obj = param[@"obj"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://MesFavoriteVideoInfoVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        
        MesFavoriteVideoInfoVC *vc = [MesFavoriteVideoInfoVC new];
        if (param[@"obj"]) {
            vc.obj = param[@"obj"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://AuthManageVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        AuthManageVC *vc = [AuthManageVC new];
        if (param[@"user_id"]) {
            vc.user_id = param[@"user_id"];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter registerURL:@"lcwl://ModifyPhoneNextVC" executingBlock:^id(NSString *sourceURL, NSDictionary *param) {
        ModifyPhoneNextVC *vc = [ModifyPhoneNextVC new];
        if (param[@"valid_flag"]) {
            vc.valid_flag = [param[@"valid_flag"] description];
        }
        [[self nav:param] pushViewController:vc animated:YES];
        return nil;
    }];
    
    [MRRouter sharedInstance].defaultExecutingBlock = ^(id object, NSDictionary *parameters) {
        @strongify(self)
        if (!object) {
            NSLog(@"controller 跳转失败");
            return;
        }
        
        UIViewController *vc = object;
        if (object && [object isKindOfClass:[NSString class]]) {
            vc = [[NSClassFromString(object) alloc] init];
            if (!vc) {
                NSLog(@"controller 跳转失败");
                return;
            }
        } else if (![object isKindOfClass:[UIViewController class]]) {
            NSLog(@"controller 跳转失败");
            return;
        }
        
        [parameters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if([vc respondsToSelector:NSSelectorFromString(key)] && obj != nil) {
                [vc setValue:obj forKey:key];
            }
        }];
        [[self nav:parameters] pushViewController:vc animated:YES];
    };
    
    [self onlineRouter];
}

/**
 *  用于动态修改跳转
 */
- (void)onlineRouter {
    
}

+(void)openVC:(UIViewController*)vc from:(UIViewController*)from{
    UINavigationController *naviViewController = [MXRouter sharedInstance].getTopNavigationController;
    NSInteger toIndex = [naviViewController.viewControllers indexOfObject:from];
    if (toIndex == NSNotFound) {
        return;
    }
    NSRange r;r.location = 0;r.length = toIndex+1;
    NSArray* array = [naviViewController.viewControllers subarrayWithRange:r];
    NSMutableArray* tmpArray = [[NSMutableArray alloc]initWithArray:array];
    [tmpArray addObject:vc];
    [naviViewController setViewControllers:tmpArray animated:YES];
}
@end
