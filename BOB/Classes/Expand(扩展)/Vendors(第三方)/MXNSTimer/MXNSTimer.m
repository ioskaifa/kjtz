//
//  MXNSTimer.m
//  BOB
//
//  Created by colin on 2020/12/22.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "MXNSTimer.h"

@interface MXNSTimer ()

@property (nonatomic, strong)NSTimer *timer;

@end

@implementation MXNSTimer

- (instancetype)initWithTimeInterval:(NSTimeInterval)interval Target:(id)target andSelector:(SEL)selector{
    if(self == [super init]){
        self.target = target;
        self.selector = selector;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(dosomething) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    }
    return self;
}

- (void)dosomething{
    id target = self.target;
    SEL selector = self.selector;
    dispatch_async(dispatch_get_main_queue(), ^{
        if([target respondsToSelector:selector]){
            [target performSelector:selector];
        }
    });
}

- (void)closeTimer{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)dealloc{
    NSLog(@"MXNSTimer dealloc");
}

@end
