//
//  MXNSTimer.h
//  BOB
//
//  Created by colin on 2020/12/22.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MXNSTimer : NSObject

@property (nonatomic, weak)id target;
@property (nonatomic, assign)SEL selector;

///创建timer
- (instancetype)initWithTimeInterval:(NSTimeInterval)interval Target:(id)target andSelector:(SEL)selector;
///销毁timer
- (void)closeTimer;

@end

NS_ASSUME_NONNULL_END
