//
//  QBAssetCell.m
//  QBImagePicker
//
//  Created by Katsuma Tanaka on 2015/04/06.
//  Copyright (c) 2015 Katsuma Tanaka. All rights reserved.
//

#import "QBAssetCell.h"

@interface QBAssetCell ()

@end

@implementation QBAssetCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self.selectBtn setImage:[UIImage imageNamed:@"talk_photo_unSelect"] forState:UIControlStateNormal];
    [self.selectBtn setImage:[UIImage imageNamed:@"talk_photo_select"] forState:UIControlStateSelected];
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    // Show/hide overlay view
    self.selectBtn.selected = (selected && self.showsOverlayViewWhenSelected);
}

@end
