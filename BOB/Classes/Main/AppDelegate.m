//
//  AppDelegate.m
//  Lcwl
//
//  Created by AlphaGO on 2018/11/15.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "AppDelegate.h"
#import "MXRemotePush.h"
#import "AppDelegate+Helper.h"
#import "VersionCheck.h"
#import <sqlite3.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "DocumentManager.h"
#import "ChatVoiceManage.h"
#import "SRWebSocketManager.h"
#import <UserNotifications/UserNotifications.h>
#import "PolymerPayManager.h"

@interface AppDelegate ()

{
    FMDatabaseQueue * fmdb;//操作数据库指针
    
}

@end

@implementation AppDelegate

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //[self appInit];
    //[self createDB];
    //[self insertMessage];
    //[self loadShoppintCart:@"CashShoppingCart"];
//    UDetail.user.user_id
    self.router = [MXRouter sharedInstance];
    //注册推送
    [[MXRemotePush sharedInstance] registerRemoteNotification:application];
    [[MXRemotePush sharedInstance] registerVoIPPush:application];
    return [self applicationFinishLaunching];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    [[PolymerPayManager shared] payCallBack:url];
    return YES;
}

- (void)createDB {
    NSString* path = [self.class createChatDirByName:@"ShoppingCart"];
    if ([StringUtil isEmpty:path]) {
        return;
    }
    fmdb = [FMDatabaseQueue databaseQueueWithPath:path];
    [self initChatTB];
}

+(NSString*)createChatDirByName:(NSString*)name {
    NSString * path = [[DocumentManager chatCacheDocDirectory:name]stringByAppendingPathComponent:@"ShoppingCart.db"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:path])
    {
        Boolean b =  [fileManager createFileAtPath:path contents:nil attributes:nil];
        if (b) {
            return path;
        }
    }else{
        return path;
    }
    return @"";
}

-(void)initChatTB {
    [self createCashShoppingCartTB];
    [self createExchageShoppingCartTB];
    
}

//创建好友表
-(BOOL)createCashShoppingCartTB{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * tableSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(id TEXT(100) PRIMARY KEY,name VARCHAR,shop_price VARCHAR,y_price VARCHAR,img VARCHAR,selectCount integer);",@"CashShoppingCart"];
        ret = [db executeUpdate:tableSql];
    }];
    return ret;
}

-(BOOL)createExchageShoppingCartTB{
    __block BOOL ret = FALSE;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * tableSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(id TEXT(100) PRIMARY KEY,name VARCHAR,shop_price VARCHAR,y_price VARCHAR,img VARCHAR,selectCount integer);",@"ExchageShoppingCart"];
        ret = [db executeUpdate:tableSql];
    }];
    return ret;
}

-(BOOL)insertMessage
{
    __block BOOL isSuccess = false;
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString* tbName = @"CashShoppingCart";
        NSString * insertSql=[NSString stringWithFormat:@"insert into %@(id,name,shop_price,y_price,img,selectCount) values (?,?,?,?,?,?);",tbName];
        isSuccess = [db executeUpdate:insertSql,
                     @"231312",
                     @"机锋网冯矿伟",
                     @"198",
                     @"209",
                     @"http://cdn.yhsjdz.com/2019/08/7c617201908232123453823.png",
                     @(3)];
    }];
    return isSuccess;
}

- (NSMutableArray*)loadShoppintCart:(NSString *)tbName {
    __block NSMutableArray* members = [[NSMutableArray alloc]initWithCapacity:10];
    [fmdb inDatabase:^(FMDatabase *db) {
        NSString * selectSql=[NSString stringWithFormat:@"select * from %@",tbName];
        FMResultSet* rs = [db executeQuery:selectSql];
        while ([rs next]) {
            
            NSString *_id = [rs stringForColumn:@"id"];
            NSString *name = [rs stringForColumn:@"name"];
            NSString *shop_price = [rs stringForColumn:@"shop_price"];
            NSString *y_price = [rs stringForColumn:@"y_price"];
            NSString *img = [rs stringForColumn:@"img"];
            NSInteger selectCount = [rs intForColumn:@"selectCount"];
            
            //[members addObject:member];
        }
        [rs close];
    }];
    return members;
}

#pragma mark - 推送注册回调
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[MXRemotePush sharedInstance] registerForRemoteNotificationsForGeTuiWithDeviceToken:deviceToken];
    MLog(@"推送注册成功")
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[MXRemotePush sharedInstance] registerGeTuiError];
    MLog(@"推送注册失败 Error: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    [MXRemotePush setApplicationIconBadgeNumber];
    [MXRemotePush userOffline:(@"0")];
    [[SRWebSocketManager sharedInstance] useDisConnect];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // fix后台回到前台 报 Code=-1005 "网络连接已中断。"
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [VersionCheck checkNewVersion];
        [MXRemotePush userOffline:(@"1")];
        [[SRWebSocketManager sharedInstance] connect:UDetail.user.user_id?:@""];
    });
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    MLog(@"APNS推送........")
    NSDictionary *aps = userInfo[@"aps"];
    if ([aps[@"content-available"] boolValue]) {
        MLog(@"接收到静默APNS......%@", userInfo)
//        UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
//        UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
//        content.body = @"静默推送";
////        UNNotificationSound *customSound = [UNNotificationSound soundNamed:@"voip_call.caf"];
//        content.sound = [UNNotificationSound defaultSound];
//        UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
//        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"Voip_Push"  content:content trigger:trigger];
//        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
//
//        }];
    } else {
        MLog(@"接收到普通APNS......%@", userInfo)
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [MXRemotePush userOffline:(@"0")];
}

- (UIView *)subWindow {
    if (!_subWindow) {
        _subWindow = [[UIView alloc] initWithFrame:CGRectMake(100,200,80,80)];
        [g_window addSubview:_subWindow];
    }
    return _subWindow;
}

@end


