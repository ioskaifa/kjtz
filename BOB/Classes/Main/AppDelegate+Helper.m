//
//  AppDelegate+Helper.m
//  Lcwl
//
//  Created by 王刚  on 2018/11/16.
//  Copyright © 2018年 lichangwanglai. All rights reserved.
//

#import "AppDelegate+Helper.h"
#import "AppAgentConfig.h"
#import "LoginVC.h"
#import "IQKeyboardManager.h"
#import "LcwlChat.h"
#import "NSObject+LoginHelper.h"
#import "ViewController.h"
#import "UIAlertView+MKBlockAdditions.h"
#import <ImSDK/ImSDK.h>
#import "GenerateTestUserSig.h"

@interface AppDelegate ()

@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundUpdateTask;
//@property (nonatomic, strong) IntroductionVC *introductionVC;
@property (nonatomic, assign) BOOL isComeFromThird;

@end

@implementation AppDelegate (Helper)

- (void)appInit {    
    MLog(@"### appInit...%@ %@", UDetail.user.token, UDetail.user.chatToken);
    
//#ifdef SMART
    V2TIMSDKConfig *config = [[V2TIMSDKConfig alloc] init];
    [[V2TIMManager sharedInstance] initSDK:SDKAPPID config:config listener:nil];
//#endif
    
    if (![StringUtil isEmpty:UDetail.user.token]) {
        [self login:@{@"sys_user_account":UDetail.user.user_tel,
                      @"login_type":@"token"}
          superView:MoApp.window.rootViewController.view
         completion:^(BOOL success, NSString *error) {
            
        }];
    }
    if(IOS11_Later) {
        [UITableView appearance].estimatedRowHeight = 0;
        [UITableView appearance].estimatedSectionHeaderHeight = 0;
        [UITableView appearance].estimatedSectionFooterHeight = 0;
    }
    
    [UINavigationBar appearance].shadowImage = [UIImage new];
}

- (BOOL)applicationFinishLaunching {
    MLog();
    [AppAgentConfig configApplicationForCapacity];
    [self initKeyboardUtil];
    [self appWindow];
    [self appInit];
    
    //    ViewController* vc = [[ViewController alloc]init];
    //    BaseNavigationController *nav= [[BaseNavigationController alloc] initWithRootViewController:vc];
    //    [self.window setRootViewController:nav];
    return YES;
}

- (void)appWindow {
    [self setupLoginLogic];
}

- (void)resetKickOffState {
    @synchronized (self) {
        self.isKickOffProcessed = NO;
    }
}

- (void)kickOff:(NSString *)msg {
    @synchronized (self) {
        if(self.isKickOffProcessed) {
            return;
        }
        self.isKickOffProcessed = YES;
        
        //        [UserModel removeFromCache];
        UDetail.user.token = @"";
        UDetail.user.chatToken = @"";
        [UDetail updateUserModel:UDetail.user];
        [[LcwlChat shareInstance].chatManager loginOut];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIAlertView alertViewWithTitle:@"您的账号在另一台设备上登陆,如非本人操作,建议修改密码." message:msg ?: @"" cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(NSInteger buttonIndex) {
                
            } onCancel:^{
                [self loginOutLogic];
            }];
            
        });
    }
}
///商城系统暂时关闭，请等待开放
- (void)closeAPP {
    @synchronized (self) {
        if(self.isKickOffProcessed) {
            return;
        }
        self.isKickOffProcessed = YES;
        
        //        [UserModel removeFromCache];
        UDetail.user.token = @"";
        UDetail.user.chatToken = @"";
        [UDetail updateUserModel:UDetail.user];
        [[LcwlChat shareInstance].chatManager loginOut];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIAlertView alertViewWithTitle:@"商城系统暂时关闭，请等待开放" message:@"" cancelButtonTitle:@"好的" otherButtonTitles:nil onDismiss:^(NSInteger buttonIndex) {
                
            } onCancel:^{
                [self loginOutLogic];
            }];
            
        });
    }
}

#pragma mark - 检查Token
- (BOOL)checkToken {
    NSString* token = UDetail.user.token;
    return ![StringUtil isEmpty:token];
}
#pragma mark - 退出登录逻辑处理
- (void)loginOutLogic {
    if ([StringUtil isEmpty:UDetail.user.token]) {
        UIViewController* tmpVC = self.window.rootViewController;
        if ([tmpVC isKindOfClass:[BaseTabBar class]]) {
            LoginVC* vc = [[LoginVC alloc]init];
            BaseNavigationController *nav= [[BaseNavigationController alloc] initWithRootViewController:vc];
            [self.window setRootViewController:nav];
        }
        return;
    }
    [NotifyHelper showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];
    [self clearUserToken:^(BOOL success, id object, NSString *error) {
        [NotifyHelper hideHUDForView:[[UIApplication sharedApplication] keyWindow] animated:YES];
        UIViewController* tmpVC = self.window.rootViewController;
        if ([tmpVC isKindOfClass:[UITabBarController class]]) {
            LoginVC* vc = [[LoginVC alloc]init];
            BaseNavigationController *nav= [[BaseNavigationController alloc] initWithRootViewController:vc];
            [self.window setRootViewController:nav];
        }
    }];
}
#pragma mark - 登录逻辑处理
- (void)setupLoginLogic {
    
    // 在iOS9下会crash 因为没有检测到rootVC
    if ([self checkToken]) {
        self.window.rootViewController = [[UIViewController alloc] init];
    }
    
    if ([self checkToken]) {
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//        self.mainVC = [ChatTabBar new];
        self.mallVC = [MainViewController new];
        [self.window setRootViewController:self.mallVC];
    }else{
        LoginVC* vc = [[LoginVC alloc]init];
        BaseNavigationController *nav= [[BaseNavigationController alloc] initWithRootViewController:vc];
        [self.window setRootViewController:nav];
    }
    self.window.backgroundColor = [UIColor whiteColor];
}


-(void)enterLoginPage{
    //    LoginPhoneViewController * loginVC = [[LoginPhoneViewController alloc] init];
    //    @weakify(self);
    //    loginVC.didSelectedEnter = ^ (NSInteger index){
    //        @strongify(self);
    //        self.mainVC = [[MainViewController alloc] init];
    //        //BaseNavigationController *nav = [[BaseNavigationController alloc] initWithRootViewController:self.mainVC];
    //        [self.window setRootViewController:self.mainVC];
    //    };
    //    [(BaseNavigationController *)self.window.rootViewController pushViewController:loginVC animated:YES];
    
}

//初始化键盘
-(void)initKeyboardUtil{
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    //控制整个功能是否启用。
    manager.enable = YES;
    //控制点击背景是否收起键盘
    manager.shouldResignOnTouchOutside = YES;
    //控制键盘上的工具条文字颜色是否用户自定义。  注意这个颜色是指textfile的tintcolor
    //    manager.shouldToolbarUsesTextFieldTintColor = YES;
    //中间位置是否显示占位文字
    manager.shouldShowToolbarPlaceholder = NO;
    //设置占位文字的字体
    manager.placeholderFont = [UIFont boldSystemFontOfSize:17];
    //控制是否显示键盘上的工具条。
    manager.enableAutoToolbar = YES;
}
@end

