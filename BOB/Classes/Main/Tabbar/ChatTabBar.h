//
//  ChatTabBar.h
//  BOB
//
//  Created by mac on 2020/9/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseTabBar.h"
#import "BaseTabBar.h"
#import "FriendVC.h"
#import "MomentsVC.h"
#import "MineVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChatTabBar : BaseTabBar

@property (nonatomic, strong) MomentsVC                  *homeVC;

@property (nonatomic, strong) BaseViewController         *chatVC;

@property (nonatomic, strong) BaseViewController         *myServerVC;

@property (nonatomic, strong) MineVC                     *mineVC;

@end

NS_ASSUME_NONNULL_END
