//
//  ChatTabBar.m
//  BOB
//
//  Created by mac on 2020/9/26.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "ChatTabBar.h"
#import "MessageModel.h"
#import "LcwlChat.h"
#import "MXConversation.h"
#import "AudioVibrateManager.h"
#import "DatabaseVisualManager.h"
#import "MXChatManager.h"
#import "ChatVoiceManage.h"
#import "MXRemotePush.h"
#import "MyServerVC.h"

@interface ChatTabBar () <UINavigationControllerDelegate, AxcAE_TabBarDelegate>

@property (nonatomic, strong) NSArray<NSDictionary *> *tabArr;
@property (nonatomic, strong) NSMutableArray *tabBarConfs;
@property (nonatomic, strong) NSMutableArray *tabBarVCs;

@end

@implementation ChatTabBar

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initControllers];
    [self initTabbar];
    [self configNotification];
    
#ifdef OpenDebugLcwl
    [UIApplication sharedApplication].applicationSupportsShakeToEdit = YES;
    [self becomeFirstResponder];
#endif
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.axcTabBar.frame = self.tabBar.bounds;
    //[self.axcTabBar viewDidLayoutItems];
}

- (void)initControllers {
    self.homeVC = [MomentsVC new];
    self.chatVC = [FriendVC new];
    self.myServerVC = [MyServerVC new];
    self.mineVC = [MineVC new];
    
    BaseNavigationController* nav1 = [[BaseNavigationController alloc]initWithRootViewController:self.homeVC];
    nav1.navigationBarHidden = YES;
    nav1.delegate = self;
    
    BaseNavigationController* nav2 = [[BaseNavigationController alloc]initWithRootViewController:self.chatVC];
    nav2.delegate = self;
    
    BaseNavigationController* nav4 = [[BaseNavigationController alloc]initWithRootViewController:self.myServerVC];
    nav4.delegate = self;
    
    BaseNavigationController* nav5 = [[BaseNavigationController alloc]initWithRootViewController:self.mineVC];
    nav5.delegate = self;
    
    self.tabArr =
    @[@{@"vc":nav1,@"normalImg":@"tabbar_消息",@"selectImg":@"tabbar_消息_Sel",@"itemTitle":@"消息"},
      @{@"vc":nav2,@"normalImg":@"tabbar_通讯录",@"selectImg":@"tabbar_通讯录_Sel",@"itemTitle":@"通讯录"},
      @{@"vc":nav4,@"normalImg":@"tabbar_服务",@"selectImg":@"tabbar_服务_Sel",@"itemTitle":@"发现"},
      @{@"vc":nav5,@"normalImg":@"tabbar_我的",@"selectImg":@"tabbar_我的_Sel",@"itemTitle":@"我的"}];
    
    // 1.遍历这个集合
    // 1.1 设置一个保存构造器的数组
    self.tabBarConfs = @[].mutableCopy;
    // 1.2 设置一个保存VC的数组
    self.tabBarVCs = @[].mutableCopy;
    [self.tabArr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 2.根据集合来创建TabBar构造器
        AxcAE_TabBarConfigModel *model = [AxcAE_TabBarConfigModel new];
        // 3.item基础数据三连
        model.itemTitle = [obj objectForKey:@"itemTitle"];
        model.selectImageName = [obj objectForKey:@"selectImg"];
        model.normalImageName = [obj objectForKey:@"normalImg"];
        // 4.设置单个选中item标题状态下的颜色
        model.selectColor = [UIColor moBlueColor];
        model.normalColor = [UIColor blackColor];
        model.automaticHidden = YES;
        // 来点效果好看
        model.interactionEffectStyle = AxcAE_TabBarInteractionEffectStyleShake;
        // 点击背景稍微明显点吧
        //model.selectBackgroundColor = AxcAE_TabBarRGBA(248, 248, 248, 1);
        model.normalBackgroundColor = [UIColor clearColor];
        //        }
        // 点击背景稍微明显点吧
        //model.selectBackgroundColor = AxcAE_TabBarRGBA(248, 248, 248, 1);
        model.normalBackgroundColor = [UIColor clearColor];
        
        // 备注 如果一步设置的VC的背景颜色，VC就会提前绘制驻留，优化这方面的话最好不要这么写
        // 示例中为了方便就在这写了
        UIViewController *vc = [obj objectForKey:@"vc"];
        //vc.view.backgroundColor = [UIColor whiteColor];
        // 5.将VC添加到系统控制组
        [self.tabBarVCs addObject:vc];
        // 5.1添加构造Model到集合
        [self.tabBarConfs addObject:model];
    }];
}

- (void)initTabbar{
    self.viewControllers = self.tabBarVCs;
    
    self.axcTabBar = [AxcAE_TabBar new] ;
    self.axcTabBar.tabBarConfig = self.tabBarConfs;
    // 7.设置委托
    self.axcTabBar.delegate = self;
    self.axcTabBar.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
    
    // 8.添加覆盖到上边
    [self.tabBar addSubview:self.axcTabBar];
    [self changeTabbarItenHierarchy];
}

- (void)changeTabbarItenHierarchy {
    [[self.axcTabBar subviews] enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if([obj isKindOfClass:[AxcAE_TabBarItem class]]) {
            AxcAE_TabBarItem *item = (AxcAE_TabBarItem *)obj;
            [item insertSubview:item.titleLabel aboveSubview:item.icomImgView];
        }
    }];
}

-(void)configNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(redBadgeCountChanged:) name:@"redBadgeCountChanged" object:nil];
    __weak __typeof(self) weakSelf = self;
    [[LcwlChat shareInstance].chatManager addDelegate:weakSelf];
}

-(void)redBadgeCountChanged:(NSNotification *)notification {
    NSDictionary  *dict = [notification userInfo];
    NSInteger nums = [dict[@"nums"]integerValue];
    NSInteger tabBarIndex = [dict[@"tabBarIndex"]integerValue];
    NSString* tip = @"";
    if (nums != 0) {
        tip = [NSString stringWithFormat:@"%ld",nums];
    }
    dispatch_async(dispatch_get_main_queue(), ^(){
        [self.axcTabBar setBadge:tip index:tabBarIndex];
        AxcAE_TabBarItem* item = self.axcTabBar.tabBarItems[tabBarIndex];
        item.itemModel.itemBadgeStyle = -1;
        CGRect badgeRect = item.badgeLabel.frame;
        badgeRect.origin.x = 45;
        badgeRect.origin.y = 0;
        item.badgeLabel.frame = badgeRect;
    });
    [MXRemotePush setApplicationIconBadgeNumber];
}

#pragma mark - AxcAE_TabBarDelegate
- (void)axcAE_TabBar:(AxcAE_TabBar *)tabbar selectIndex:(NSInteger)index{
    [self setSelectedIndex:index];
}

#pragma mark - IMXChatManagerDelegate
-(void)didReceiveMessage:(MessageModel *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        BOOL isEnterBack = ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground);
        MXConversation* con = [[LcwlChat shareInstance].chatManager getLastestConversation];
        ///对语音通话的监听
        [[ChatVoiceManage shareInstance] didReceiveMessage:message];
        if (con == nil || ![con.chat_id isEqualToString:message.chat_with]) {
            //红点更新
            {
                NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithCapacity:0];
                NSInteger nums = [[LcwlChat shareInstance].chatManager getAllUnReadNums];
                [userInfo setValue:[NSNumber numberWithInteger:nums] forKey:@"nums"];
                [userInfo setValue:[NSNumber numberWithInteger:3] forKey:@"tabBarIndex"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"redBadgeCountChanged" object:nil userInfo:userInfo];
                //撤回消息 不需要提醒
                if (message.subtype == kMXMessageTypeWithdraw) {
                    return;
                }
                //登录电脑端，且开启了静音
                if (UDetail.user.client_status && UDetail.user.client_silenceStatus) {
                    return;
                }
                //如果该消息不静音  某个对话的免打扰保存在本地
                if (![[LcwlChat shareInstance].chatManager checkEnableDisturb:message]) {
                    if (UDetail.user.is_notify) {
                        if (UDetail.user.is_vibrate && UDetail.user.is_voice) {
                            [[AudioVibrateManager shareInstance] playAudioAndVibrate];
                        } else if (UDetail.user.is_voice) {
                            [[AudioVibrateManager shareInstance] playSound];
                        } else if (UDetail.user.is_vibrate) {
                            [[AudioVibrateManager shareInstance] playVibrate];
                        }
                    }
                }
            }
        }
    });
}

- (void)monitorClientMessage:(MessageModel *)message {
    if (![message isKindOfClass:MessageModel.class]) {
        return;
    }
    if (message.subtype != kMXMessageTypeClient) {
        return;
    }
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:message.body];
    if (bodyDic[@"attr1"]) {
        kMXClientType clientType = [bodyDic[@"attr1"] integerValue];
        if (clientType == kMXClientTypeLogOut) {
            UDetail.user.client_status = NO;
            [self.homeVC refreshHeaderView];
        }
    }
}

- (BOOL)canBecomeFirstResponder {
#ifdef OpenDebugLcwl
    return YES;
#else
    return NO;
#endif
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
#ifdef OpenDebugLcwl
    [DatabaseVisualManager sharedInstance].dbDocumentPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject];
    [[DatabaseVisualManager sharedInstance] showTables];
#endif
}


@end
