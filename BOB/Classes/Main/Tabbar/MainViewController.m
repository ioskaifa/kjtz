//
//  MainViewController.m
//  MoXian
//
//  Created by 王 刚 on 14-4-17.
//  Copyright (c) 2014年 litiankun. All rights reserved.
//

#import "MainViewController.h"
//#import "MXTabBarButton.h"
#import "BaseNavigationController+ExtendBaseNavigationController.h"
#import "AppDelegate.h"
#import "MXUIDefine.h"
#import "MXDeviceDefine.h"
#import "ViewController.h"
#import "MessageModel.h"
#import "LcwlChat.h"
#import "MXConversation.h"
#import "AudioVibrateManager.h"
#import "DatabaseVisualManager.h"
#import "MXChatManager.h"
#import "MyServerVC.h"
#import "MyCalendarApiHelper.h"
#import "ChatVoiceManage.h"
#import "MXRemotePush.h"

#define kFirstShowCityList @"kFirstShowCityList"

@interface MainViewController ()<AxcAE_TabBarDelegate, UINavigationControllerDelegate>
{
    NSInteger   selectedIndex;
}

@property (nonatomic, strong) NSDate              *lastPlaySoundDate;
@property (nonatomic, strong) NSDate              *lastReceiveMsgDate;
@property (nonatomic, strong) UILocalNotification *notification;
@property (nonatomic, strong) NSArray<NSDictionary *> *tabArr;
@property (nonatomic, strong) NSMutableArray *tabBarConfs;
@property (nonatomic, strong) NSMutableArray *tabBarVCs;


@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initControllers];
    [self initTabbar];
    [MXCache setValue:@"1" forKey:@"first"];
    [self configNotification];
#ifdef OpenDebugLcwl
    [UIApplication sharedApplication].applicationSupportsShakeToEdit = YES;
    [self becomeFirstResponder];
#endif
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void)configNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(redBadgeCountChanged:) name:@"redBadgeCountChanged" object:nil];
    __weak __typeof(self) weakSelf = self;
    [[LcwlChat shareInstance].chatManager addDelegate:weakSelf];
}

-(void)redBadgeCountChanged:(NSNotification *)notification {
    NSDictionary  *dict = [notification userInfo];
    NSInteger nums = [dict[@"nums"]integerValue];
    NSInteger tabBarIndex = [dict[@"tabBarIndex"]integerValue];
    NSString* tip = @"";
    if (nums != 0) {
        tip = [NSString stringWithFormat:@"%ld",nums];
    }
    dispatch_async(dispatch_get_main_queue(), ^(){
        [self.axcTabBar setBadge:tip index:tabBarIndex];
        AxcAE_TabBarItem* item = self.axcTabBar.tabBarItems[tabBarIndex];
        item.itemModel.itemBadgeStyle = -1;
        CGRect badgeRect = item.badgeLabel.frame;
        badgeRect.origin.x = 45;
        badgeRect.origin.y = 0;
        item.badgeLabel.frame = badgeRect;
    });
    [MXRemotePush setApplicationIconBadgeNumber];
}

#pragma mark - IMXChatManagerDelegate
-(void)didReceiveMessage:(MessageModel *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        BOOL isEnterBack = ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground);
        MXConversation* con = [[LcwlChat shareInstance].chatManager getLastestConversation];
        ///对语音通话的监听
        [[ChatVoiceManage shareInstance] didReceiveMessage:message];
        if (con == nil || ![con.chat_id isEqualToString:message.chat_with]) {
            //红点更新
            {
                NSMutableDictionary* userInfo = [[NSMutableDictionary alloc]initWithCapacity:0];
                NSInteger nums = [[LcwlChat shareInstance].chatManager getAllUnReadNums];
                [userInfo setValue:[NSNumber numberWithInteger:nums] forKey:@"nums"];
                [userInfo setValue:[NSNumber numberWithInteger:3] forKey:@"tabBarIndex"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"redBadgeCountChanged" object:nil userInfo:userInfo];
                //撤回消息 不需要提醒
                if (message.subtype == kMXMessageTypeWithdraw) {
                    return;
                }
                //登录电脑端，且开启了静音
                if (UDetail.user.client_status && UDetail.user.client_silenceStatus) {
                    return;
                }
                //如果该消息不静音  某个对话的免打扰保存在本地
                if (![[LcwlChat shareInstance].chatManager checkEnableDisturb:message]) {
                    if (UDetail.user.is_notify) {
                        if (UDetail.user.is_vibrate && UDetail.user.is_voice) {
                            [[AudioVibrateManager shareInstance] playAudioAndVibrate];
                        } else if (UDetail.user.is_voice) {
                            [[AudioVibrateManager shareInstance] playSound];
                        } else if (UDetail.user.is_vibrate) {
                            [[AudioVibrateManager shareInstance] playVibrate];
                        }
                    }
                }
            }
        }
    });
}


- (void)initControllers {
    self.homeVC = [HomeVC new];
    self.mineVC = [MyVC new];    
    self.chatVC = [MomentsVC new];
    self.freeShopVC = [FreeShopVC new];
    self.makeGroupVC = [MakeGroupVC new];
    
    BaseNavigationController* nav1 = [[BaseNavigationController alloc]initWithRootViewController:self.homeVC];
    nav1.navigationBarHidden = YES;
    nav1.delegate = self;
    
    BaseNavigationController* nav2 = [[BaseNavigationController alloc]initWithRootViewController:self.freeShopVC];
    nav2.navigationBarHidden = YES;
    nav2.delegate = self;
    
    BaseNavigationController* nav3 = [[BaseNavigationController alloc]initWithRootViewController:self.chatVC];
    nav3.delegate = self;
    
    BaseNavigationController* nav4 = [[BaseNavigationController alloc]initWithRootViewController:self.makeGroupVC];
    nav4.navigationBarHidden = YES;
    nav4.delegate = self;
    
    BaseNavigationController* nav5 = [[BaseNavigationController alloc]initWithRootViewController:self.mineVC];
    nav4.navigationBarHidden = YES;
    nav4.delegate = self;
    
    self.tabArr =
    @[@{@"vc":nav1,@"normalImg":@"tabbar_首页",@"selectImg":@"tabbar_首页_Sel",@"itemTitle":@"首页"},
      @{@"vc":nav2,@"normalImg":@"tabbar_分类",@"selectImg":@"tabbar_分类_Sel",@"itemTitle":@"免单"},
      @{@"vc":nav4,@"normalImg":@"tabbar_拼团",@"selectImg":@"tabbar_拼团_Sel",@"itemTitle":@"拼团"},
      @{@"vc":nav3,@"normalImg":@"tabbar_聊天",@"selectImg":@"tabbar_聊天_Sel",@"itemTitle":@"聊天"},
      @{@"vc":nav5,@"normalImg":@"tabbar_我的",@"selectImg":@"tabbar_我的_Sel",@"itemTitle":@"我的"}];
    
    // 1.遍历这个集合
    // 1.1 设置一个保存构造器的数组
    self.tabBarConfs = @[].mutableCopy;
    // 1.2 设置一个保存VC的数组
    self.tabBarVCs = @[].mutableCopy;
    [self.tabArr enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 2.根据集合来创建TabBar构造器
        AxcAE_TabBarConfigModel *model = [AxcAE_TabBarConfigModel new];
        // 3.item基础数据三连
        model.itemTitle = [obj objectForKey:@"itemTitle"];
        model.selectImageName = [obj objectForKey:@"selectImg"];
        model.normalImageName = [obj objectForKey:@"normalImg"];
        // 4.设置单个选中item标题状态下的颜色
        model.selectColor = [UIColor blackColor];
        model.normalColor = [UIColor blackColor];
        model.automaticHidden = YES;
        /***********************************/
//        if (idx == 2 ) { // 如果是中间的
//            // 设置凸出 矩形
//            model.bulgeStyle = AxcAE_TabBarConfigBulgeStyleSquare;
//            // 设置凸出高度
//            model.bulgeHeight = 30;
//            // 设置成图片文字展示
//            model.itemLayoutStyle = AxcAE_TabBarItemLayoutStyleTopPictureBottomTitle;
//            // 设置图片
//            model.selectImageName = @"商城_Sel";
//            model.normalImageName = @"商城_Sel";
//            model.selectBackgroundColor = model.normalBackgroundColor = [UIColor clearColor];
//            model.backgroundImageView.hidden = YES;
//            // 设置图片大小c上下左右全边距
//            model.componentMargin = UIEdgeInsetsMake(8, 0, 0, 0 );
//            // 设置图片的高度为40
//            model.icomImgViewSize = CGSizeMake(self.tabBar.frame.size.width / 5, 60);
//            model.titleLabelSize = CGSizeMake(self.tabBar.frame.size.width / 5, 20);
//            // 图文间距0
//            model.pictureWordsMargin = -14;
//            // 设置标题文字字号
//            model.titleLabel.font = [UIFont systemFontOfSize:11];
//            // 设置大小/边长 自动根据最大值进行裁切
//            model.itemSize = CGSizeMake(self.tabBar.frame.size.width / 5 - 5.0 ,self.tabBar.frame.size.height + 20);
//        }else{  // 其他的按钮来点小动画吧
            // 来点效果好看
            model.interactionEffectStyle = AxcAE_TabBarInteractionEffectStyleShake;
            // 点击背景稍微明显点吧
            //model.selectBackgroundColor = AxcAE_TabBarRGBA(248, 248, 248, 1);
            model.normalBackgroundColor = [UIColor clearColor];
//        }
        // 点击背景稍微明显点吧
        //model.selectBackgroundColor = AxcAE_TabBarRGBA(248, 248, 248, 1);
        model.normalBackgroundColor = [UIColor clearColor];
        
        // 备注 如果一步设置的VC的背景颜色，VC就会提前绘制驻留，优化这方面的话最好不要这么写
        // 示例中为了方便就在这写了
        UIViewController *vc = [obj objectForKey:@"vc"];
        //vc.view.backgroundColor = [UIColor whiteColor];
        // 5.将VC添加到系统控制组
        [self.tabBarVCs addObject:vc];
        // 5.1添加构造Model到集合
        [self.tabBarConfs addObject:model];
    }];
}

- (void)initTabbar{
    self.viewControllers = self.tabBarVCs;
    
    self.axcTabBar = [AxcAE_TabBar new] ;
    self.axcTabBar.tabBarConfig = self.tabBarConfs;
    // 7.设置委托
    self.axcTabBar.delegate = self;
    self.axcTabBar.backgroundColor = [UIColor colorWithHexString:@"#F7F7F7"];
    
    // 8.添加覆盖到上边
    [self.tabBar addSubview:self.axcTabBar];
    [self addLayoutTabBar]; // 10.添加适配
    [self changeTabbarItenHierarchy];
    //[self setSelectedIndex:2];
}

- (void)changeTabbarItenHierarchy {
    [[self.axcTabBar subviews] enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if([obj isKindOfClass:[AxcAE_TabBarItem class]]) {
            AxcAE_TabBarItem *item = (AxcAE_TabBarItem *)obj;
            [item insertSubview:item.titleLabel aboveSubview:item.icomImgView];
        }
    }];
}

// 9.实现代理，如下：
static NSInteger lastIdx = 0;
- (void)axcAE_TabBar:(AxcAE_TabBar *)tabbar selectIndex:(NSInteger)index{
    [self setSelectedIndex:index];
    lastIdx = index;
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex{
    [super setSelectedIndex:selectedIndex];
    if(self.axcTabBar.selectIndex == selectedIndex) {
        return;
    }
    
    if(self.axcTabBar){
        self.axcTabBar.selectIndex = selectedIndex;
        //        if([[UIApplication sharedApplication] respondsToSelector:NSSelectorFromString(@"setStatusBarStyle:")]) {
        //            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDarkContent];
        //        }
    }
}

// 10.添加适配
- (void)addLayoutTabBar{
    // 使用重载viewDidLayoutSubviews实时计算坐标 （下边的 -viewDidLayoutSubviews 函数）
    // 能兼容转屏时的自动布局
}
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.axcTabBar.frame = self.tabBar.bounds;
    //[self.axcTabBar viewDidLayoutItems];
}

- (void)monitorClientMessage:(MessageModel *)message {
    if (![message isKindOfClass:MessageModel.class]) {
        return;
    }
    if (message.subtype != kMXMessageTypeClient) {
        return;
    }
    NSDictionary *bodyDic = [MXJsonParser jsonToDictionary:message.body];
    if (bodyDic[@"attr1"]) {
        kMXClientType clientType = [bodyDic[@"attr1"] integerValue];
        if (clientType == kMXClientTypeLogOut) {
            UDetail.user.client_status = NO;
//            [self.homeVC refreshHeaderView];
        }
    }
}

- (BOOL)canBecomeFirstResponder {
#ifdef OpenDebugLcwl
    return YES;
#else
    return NO;
#endif
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
#ifdef DEBUG
    [DatabaseVisualManager sharedInstance].dbDocumentPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject];
    [[DatabaseVisualManager sharedInstance] showTables];
#endif
}

@end

