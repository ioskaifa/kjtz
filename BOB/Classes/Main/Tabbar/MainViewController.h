//
//  MainViewController.h
//  MoXian

//  Created by 王 刚 on 14-4-17.
//  Copyright (c) 2014年 litiankun. All rights reserved.
//  程序首页


#import <UIKit/UIKit.h>
#import "BaseTabBar.h"
#import "FriendVC.h"
#import "MomentsVC.h"
#import "MineVC.h"
#import "HomeVC.h"
#import "MyVC.h"
#import "FreeShopVC.h"
#import "MakeGroupVC.h"

#define No_Login_PhoneNo @""

#define No_Login_Password @""

@interface MainViewController : BaseTabBar

@property (nonatomic, strong) UIImageView                *barBottomView;

@property (nonatomic, strong) UIView                     *moveView;

@property (nonatomic, strong) BaseViewController         *homeVC;

@property (nonatomic, strong) BaseViewController         *chatVC;

@property (nonatomic, strong) BaseViewController         *freeShopVC;

@property (nonatomic, strong) MyVC                     *mineVC;

@property (nonatomic, strong) BaseViewController       *categoryVC;

@property (nonatomic, strong) BaseViewController       *makeGroupVC;

@end
