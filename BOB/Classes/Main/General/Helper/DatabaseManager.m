//
//  DatabaseManager.m
//  MoPal_Developer
//
//  Created by 王 刚 on 14/12/25.
//  Copyright (c) 2014年 MoXian. All rights reserved.
//

#import "DatabaseManager.h"
#import "FMDatabaseQueue.h"
#import "FMDatabaseAdditions.h"
#import "EntityFactory.h"

@implementation DatabaseManager
@synthesize dbQueue;

- (id)init{
    if(self = [super init]){
        self.dbQueue = [EntityFactory mxAppCacheDB];
        [self.dbQueue inDatabase:^(FMDatabase *db) {
            self.db = db;
            if([db open]){
                [db setShouldCacheStatements:YES];
                [self initTable];
            } else {
                self.db = nil;
            }
        }];
    }
    return self;
}

- (id)initCountryDataBase
{
    if(self = [super init]){
        self.dbQueue = [EntityFactory mxAppCacheDB:[self databaseName]];
        [self.dbQueue inDatabase:^(FMDatabase *db) {
            if([db open]){
                [db setShouldCacheStatements:YES];
                [self initTable];
            }
        }];
    }
    return self;
}

- (NSString *)databaseName
{
    return nil;
}

#pragma mark 初始化表格式,需要具体子类去实现
- (void)initTable {
    
}

#pragma mark 获取数据库表名,由具体子类实现
- (NSString *)getTableName {
    return NULL;
}

-(NSString *)setTable:(NSString *)sql{
    return [NSString stringWithFormat:sql,[self getTableName]];
}

- (BOOL)insertWithArray:(NSArray *)pojoArray {
    //    [dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
    BOOL isRollback = NO;
    [self.db beginTransaction];
    @try {
        for (id pojo in pojoArray) {
            BOOL b = [self insertAt:pojo];
            if(!b){
                
            }
        }
    }
    @catch (NSException *exception) {
        [self.db rollback];
        isRollback = YES;
    }
    @finally {
        if(!isRollback){
            [self.db commit];
        }
    }
    //    }];
    return !isRollback ;
}

- (BOOL)insertAt:(id)pojo {
    return NO;
}


// DELETE
- (BOOL)deleteAt:(int)index{
    @try {
        [self.db executeUpdate:[self setTable:@"DELETE FROM %@ WHERE _ID = ?"], [NSNumber numberWithInt:index]];
        if ([self.db hadError]) {
            return NO;
        }
    }
    @catch (NSException *exception) {
        return NO;
    }
    @finally {
    }
    return YES;
}

- (BOOL)deleteAll {
    BOOL success = YES;
    @try {
        [self.db executeUpdate:[self setTable:@"DELETE FROM %@"]];
        if ([self.db hadError]) {
            success = NO;
        }
    }
    @catch (NSException *exception) {
        success = NO;
    }
    @finally {
    }
    //    }];
    return success;
}

- (void)dealloc {
#if !__has_feature(objc_arc)
    [db release];
#else
    self.dbQueue = nil;
    self.db = nil;
#endif
}

@end
