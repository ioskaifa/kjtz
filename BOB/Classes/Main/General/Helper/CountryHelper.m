//
//  CountryHelper.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/1/29.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "CountryHelper.h"
#import "StringUtil.h"
#import "CountryCodeModel.h"
#import "FMDatabaseQueue.h"
#import "FMDatabase.h"
#import "GVUserDefaults+Properties.h"
#import "MXCache.h"

#define SqliteName @"city_mobile_code"
#define CountryName @"mx_city"
#define MobileName @"city_mobile_code"
// 视图
#define MobileViewName @"province_city"


static CountryHelper * database =nil;
static dispatch_once_t onceToken;


@implementation CountryHelper

+(CountryHelper*)sharedDataBase {
    dispatch_once(&onceToken, ^{
        database=[[CountryHelper alloc] initCountryDataBase];
        database.selectedCountrys = [[NSMutableArray alloc]initWithCapacity:2];
    });
    return database;
}

- (id)init {
    MLog(@"%@ 是一个单例，你的用法不正确", self.class);
    return nil;
}

- (NSString *)databaseName
{
    NSString *fullPath = [[NSBundle mainBundle] pathForResource:SqliteName ofType:@"sqlite"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:fullPath]) {
        return fullPath;
    }
    
    return nil;
}

/**
 *  读取本地城市手机区号
 *
 *  @return 城市手机数据 NSMutableArray
 */
-(NSMutableArray*)readMobileCountryDataFromDB{
    NSMutableArray* countryCodeArray = nil;
    countryCodeArray = [self getMobileCountryCodeData];
    return  [self sortCategory:countryCodeArray];
}

-(NSMutableArray*)readCountryDataByPid:(NSInteger)pid{
    NSMutableArray* countryCodeArray = nil;
    countryCodeArray = [self getCountryCodeByPid:pid];
    return  [self sortCategory:countryCodeArray];
}

-(NSMutableArray*)readCountryDataByLevel:(NSInteger)level{
    NSMutableArray* countryCodeArray = nil;
    countryCodeArray = [self getCountryCodeByLevel:level];
    return  [self sortCategory:countryCodeArray];
}

- (NSMutableArray *)readBussnessDataByPid:(NSInteger)pId
{
    NSMutableArray *bussness = nil;
    bussness = [self getBussnissByPid:pId];
    return  [self sortCategory:bussness];
}

#pragma mark -  商圈信息
- (NSMutableArray *)getBussnissByPid:(NSInteger)pid {
    NSString *selectSql = [NSString stringWithFormat:@"select biz_bussniss_area_bd_seq,pub_city_dict_bd_seq,bussniss_area_name from %@  where pub_city_dict_bd_seq = %@", @"mx_business", @(pid)];
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:2];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:selectSql];
        while ([rs next]) {
            CountryCodeModel *country = [[CountryCodeModel alloc] init];
            country.cs_name = [rs stringForColumn:@"bussniss_area_name"];
            country.pid = [rs intForColumn:@"pub_city_dict_bd_seq"];
            country.currentId = [rs intForColumn:@"biz_bussniss_area_bd_seq"];
            [array addObject:country];
        }
        [rs close];
    }];

    return array;
}

- (CountryCodeModel*)getBussnissByBid:(NSInteger)bid {
    NSString * selectSql=[NSString stringWithFormat:@"select biz_bussniss_area_bd_seq,pub_city_dict_bd_seq,bussniss_area_name from %@  where biz_bussniss_area_bd_seq = %@",@"mx_business",@(bid)]; //edit by lhy bussness_area -> mx_business 2016年01月11日
    __block CountryCodeModel* country = nil;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs = [db executeQuery:selectSql];
        while ([rs next]) {
            country = [[CountryCodeModel alloc]init];
            country.cs_name = [rs stringForColumn:@"bussniss_area_name"];
            country.en_name = [rs stringForColumn:@"bussniss_area_name"];
            country.pid = [rs intForColumn:@"pub_city_dict_bd_seq"];
            country.currentId = [rs intForColumn:@"biz_bussniss_area_bd_seq"];
            break;
        }
        [rs close];
    }];
    
    return country;
}

-(NSMutableArray*)sortCategory:(NSMutableArray*) countryCodeArray{
    NSMutableArray* tmpIndexArray = [[NSMutableArray alloc]initWithCapacity:0];
    NSInteger len = ALPHA.length;
    for (int i = 0; i < len; i++) {
        [tmpIndexArray addObject:[NSMutableArray array]];
    }
    for (CountryCodeModel* countryCode in countryCodeArray) {
        NSString* realName = @"";
        if (countryCode.isHot == 1) {
            realName = @"#";
        }
        realName = [NSString stringWithFormat:@"%@%@",realName,countryCode.cs_name];
        NSString* tmpCountryCodeName = [StringUtil pinYinFirstLetterToUppercase:realName];
        NSUInteger firstLetter = [ALPHA rangeOfString:[tmpCountryCodeName substringToIndex:1]].location;
        if (firstLetter != NSNotFound){
            [[tmpIndexArray safeObjectAtIndex:firstLetter] addObject:countryCode];
        }
    }
    return tmpIndexArray;
}

-(NSMutableArray*)getMobileCountryCodeData{
  
    NSString * selectSql=[NSString stringWithFormat:@"select * from %@ order by is_hot desc",MobileName];
    NSMutableArray *array=[NSMutableArray arrayWithCapacity:2];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:selectSql];
        
        while ([rs next]) {
            CountryCodeModel* country = [[CountryCodeModel alloc]init];
            country.code = [rs stringForColumn:@"code"];
            country.isHot = [rs intForColumn:@"is_hot"];
            country.cs_name = [rs stringForColumn:@"cs_name"];
            country.en_name = [rs stringForColumn:@"en_name"];
            country.countryCode = [rs stringForColumn:@"is_code"];
            [array addObject:country];
        }
        
        [rs close];
    }];
    
    return array;

    
}
-(NSMutableArray*)getCountryCodeByLevel:(NSInteger)level{
    NSString * selectSql=[NSString stringWithFormat:@"select has_next,id,cs_name,en_name,id,pid,level from %@  where level = %@",CountryName,@(level)];
    NSMutableArray *array=[NSMutableArray arrayWithCapacity:2];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:selectSql];
        
        while ([rs next]) {
            CountryCodeModel* country = [[CountryCodeModel alloc]init];
            country.cs_name = [rs stringForColumn:@"cs_name"];
            country.en_name = [rs stringForColumn:@"en_name"];
            country.pid = [rs intForColumn:@"pid"];
            country.currentId = [rs intForColumn:@"id"];
            country.hasNext = [rs intForColumn:@"has_next"];
            country.level = [rs intForColumn:@"level"];
            [array addObject:country];
        }
        
        [rs close];
    }];
    
    return array;
}

// 根据PId查询城市列表
-(NSMutableArray*)getCountryCodeByPid:(NSInteger)pid {
    NSString * selectSql=[NSString stringWithFormat:@"select has_next,id,cs_name,en_name,id,pid,level from %@  where pid = %ld",CountryName,(long)pid];
     NSMutableArray *array=[NSMutableArray arrayWithCapacity:2];
    
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs = [db executeQuery:selectSql];
        
        while ([rs next]) {
            CountryCodeModel *country = [[CountryCodeModel alloc]init];
            country.cs_name = [rs stringForColumn:@"cs_name"];
            country.en_name = [rs stringForColumn:@"en_name"];
            country.pid = [rs intForColumn:@"pid"];
            country.currentId = [rs intForColumn:@"id"];
            country.hasNext = [rs intForColumn:@"has_next"];
            country.level = [rs intForColumn:@"level"];
            [array addObject:country];
        }
        
        [rs close];
    }];
    
    return array;
}
-(CountryCodeModel*)readCountryDataById:(NSInteger)_id{
    NSString * selectSql=[NSString stringWithFormat:@"select has_next,id,cs_name,en_name,id,pid,level from %@  where id = %ld",CountryName,(long)_id];
    __block CountryCodeModel *country = [[CountryCodeModel alloc] init];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs=[db executeQuery:selectSql];
        
        while ([rs next]) {
            country.cs_name = [rs stringForColumn:@"cs_name"];
            country.en_name = [rs stringForColumn:@"en_name"];
            country.pid = [rs intForColumn:@"pid"];
            country.currentId = [rs intForColumn:@"id"];
            country.hasNext = [rs intForColumn:@"has_next"];
            country.level = [rs intForColumn:@"level"];
            break;
        }
        
        [rs close];
    }];
    return country;
}

-(CountryCodeModel*)selectCountryCode:(NSString*) country_code {
    if ([StringUtil isEmpty:country_code]) {
        return nil;
    }
    NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where is_code = '%@'",MobileName,country_code];
    __block CountryCodeModel* country = Nil;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        @autoreleasepool {
            FMResultSet *rs = [db executeQuery:selectSql];
            while ([rs next]) {
                country = [[CountryCodeModel alloc]init];
                country.code = [rs stringForColumn:@"code"];
                country.isHot = [rs intForColumn:@"is_hot"];
                country.cs_name = [rs stringForColumn:@"cs_name"];
                country.en_name = [rs stringForColumn:@"en_name"];
                country.countryCode = [rs stringForColumn:@"is_code"];
                break;
            }
            [rs close];
        }
    }];
    
    return country;
}

- (CountryCodeModel *)selectCountryDataByCountryCode:(NSString *)countryCode {
    if ([StringUtil isEmpty:countryCode]) {
        return nil;
    }
    NSString * selectSql = [NSString stringWithFormat:@"select * from %@ where code = '%@'",MobileName,countryCode];
    __block CountryCodeModel* country = Nil;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        @autoreleasepool {
            FMResultSet *rs = [db executeQuery:selectSql];
            while ([rs next]) {
                country = [[CountryCodeModel alloc]init];
                country.code = [rs stringForColumn:@"code"];
                country.isHot = [rs intForColumn:@"is_hot"];
                country.cs_name = [rs stringForColumn:@"cs_name"];
                country.en_name = [rs stringForColumn:@"en_name"];
                country.countryCode = [rs stringForColumn:@"is_code"];
                break;
            }
            [rs close];
        }
    }];
    return country;
}

-(CountryCodeModel*)readCountryCodeCache{
    // 暂时注释掉
    //[MXCache valueForKey:MobileCountryCodeCache];
    id object = nil;
    if ( object == nil) {
        NSString* systemCountryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
        object = [self selectCountryCode:systemCountryCode];
        object = [self selectCountryCode:@"CN"];
        
    }
    
    return object;
}


+(BOOL)isChina{
    CountryCodeModel* country = [[CountryHelper sharedDataBase]readCountryCodeCache];
    if ([country.countryCode isEqualToString:@"CN"]) {
        return YES;
    }
    return NO;
}
+(NSString*)countryCodeParamString:(NSArray *)countryArray{
    NSMutableArray* tmpCountry = [[NSMutableArray alloc]initWithCapacity:0];
    for (CountryCodeModel* model in countryArray) {
        NSString* idString = [NSString stringWithFormat:@"%@",@(model.currentId)];
        [tmpCountry addObject:idString];
    }
    NSString *tempString=[tmpCountry componentsJoinedByString:@"|"];
    return tempString;
}

+(NSString*)composeShowCountryName:(NSArray *)countrys{
    NSMutableArray* countryArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (CountryCodeModel* model in countrys)
        [countryArray addObject:model.cs_name ];

    return [countryArray componentsJoinedByString:@"-"];
}

// 获取省市
- (NSString*)getProviceCityString:(NSString*)cityId langage:(NSInteger)langage {
    
    NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where city_cid = '%@' ",MobileViewName,cityId];
    __block NSString *province_city = Nil;
    __block NSString *cnString = nil;
    __block NSString *enString = nil;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs=[db executeQuery:selectSql];
        
        while ([rs next]) {
            if ([StringUtil isEmpty:[rs stringForColumn:@"pro_cs_name"]]) {
                cnString=[rs stringForColumn:@"c_cs_name"];
                enString=[rs stringForColumn:@"c_en_name"];
            }else{
                cnString=[NSString stringWithFormat:@"%@-%@",[rs stringForColumn:@"pro_cs_name"],[rs stringForColumn:@"c_cs_name"]];
                enString=[NSString stringWithFormat:@"%@-%@",[rs stringForColumn:@"pro_en_name"],[rs stringForColumn:@"c_en_name"]];
            }
            
            break;
        }
        
        [rs close];
    }];
    
    
    if (langage==1) { // 英文
        province_city=enString;
    }else if(langage==0) { // 中文
        province_city=cnString;
    }else{
        province_city=enString;
    }
    return province_city;
}

- (NSString *)getCountryProviceCityString:(NSString *)areaCode langage:(NSInteger)langage{
    if ([StringUtil isEmpty:areaCode]) {
        MLog(@"传入的地区码为空");
        return @"";
    }
    if ([areaCode rangeOfString:@"|"].location == NSNotFound) {
        MLog(@"不是正确的地区码格式");
        return @"";
    }
    NSString *sqlAreaCode = [areaCode stringByReplacingOccurrencesOfString:@"|" withString:@","];
    NSString * selectedSql = [NSString stringWithFormat:@"select * from %@ where id in (%@) order by level asc",CountryName,sqlAreaCode];
    NSMutableArray *resultList = [NSMutableArray arrayWithObjects:@"",@"",@"", nil];
    
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs=[db executeQuery:selectedSql];
        while ([rs next]) {
            NSString *name = @"";
            if (langage == 0) {
                name = [rs stringForColumn:@"cs_name"];
            }else{
                name = [rs stringForColumn:@"en_name"];
            }
            NSInteger level = [rs intForColumn:@"level"];
            if ((level - 1) < [resultList count]) {
                [resultList replaceObjectAtIndex:level - 1 withObject:name];
            }
        }
        [rs close];
    }];
    MLog(@"resultList = %@",[resultList componentsJoinedByString:@"-"]);
    return [resultList componentsJoinedByString:@"-"];
}

- (NSString *)getShowFormatCountryProviceCity:(NSString *)areaCode langage:(NSInteger)langage{
    NSString *result = [self getCountryProviceCityString:areaCode langage:langage];
    if ([StringUtil isEmpty:result] ||
        [result rangeOfString:@"-"].location == NSNotFound) {
        MLog(@"没有获取到数据");
        return @"";
    }
    NSArray* resultList = [result componentsSeparatedByString:@"-"];
    NSString *showResult = @"";
    
    NSString *country = [resultList safeObjectAtIndex:0];
    NSString *provice = [resultList safeObjectAtIndex:1];
    NSString *city    = [resultList safeObjectAtIndex:2];
    //说明是直辖市
    if ([provice isEqualToString:city] ||
        [StringUtil isEmpty:city]) {
        showResult = [NSString stringWithFormat:@"%@-%@",country,provice];
    }else{
        showResult = [NSString stringWithFormat:@"%@-%@",provice,city];
    }
    return showResult;
}

- (CountryCodeModel *)getCountryCodeByAbbreviation:(NSString *)name {
    if (![name isKindOfClass:[NSString class]] ||
        [StringUtil isEmpty:name]) {
        return nil;
    }
    
    CountryCodeModel *model = [MXCache valueForKey:@"name"];
    if (model) {
        return model;
    }
    NSString *afterName = @"";
    if ([name hasSuffix:@"市"]) {
        afterName = [[name componentsSeparatedByString:@"市"] firstObject];
    } else if ([name hasSuffix:@"city"]) {
        afterName = [[name componentsSeparatedByString:@"city"] firstObject];
    } else {
        afterName = name;
    }
    
    NSMutableString * selectSql=[NSMutableString stringWithFormat:@"select * from %@ where (en_name like '%%%@%%' or cs_name like '%%%@%%') and level = 3",CountryName,afterName,afterName];
//    if ([[MXLocationManager shareManager] countryId] > 0) {
//        [selectSql appendFormat:@" and seqs like'%@|%%'",@([MXLocationManager shareManager].countryId)];
//    }
    MLog(@"%@",selectSql);


    NSMutableArray *array = [NSMutableArray arrayWithCapacity:2];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs=[db executeQuery:selectSql];
        
        while ([rs next]) {
            CountryCodeModel* country = [[CountryCodeModel alloc]init];
            country.cs_name = [rs stringForColumn:@"cs_name"];
            country.en_name = [rs stringForColumn:@"en_name"];
            country.pid = [rs intForColumn:@"pid"];
            country.currentId = [rs intForColumn:@"id"];
            country.hasNext = [rs intForColumn:@"has_next"];
            country.level = [rs intForColumn:@"level"];
            [array addObject:country];
        }
        [rs close];
    }];

    if (array.count == 0) {
        return nil;
    } else {
        //省，市有可能重名
        for (NSUInteger index = 0; index < array.count; index++) {
            CountryCodeModel *country = array[index];
            if (country.level == 3) {
                return country;
            }
        }
        
        [MXCache setValue:array[0] forKey:name];
        return array[0];
    }
}

-(NSMutableArray*)getCountrysByAbbreviation:(CountryCodeModel*)model{
    NSMutableArray* tmpArray = [[NSMutableArray alloc] initWithCapacity:0];
    BOOL hasParent = YES;
    NSInteger pid = model.pid;
    while (hasParent) {
        CountryCodeModel* country = [self readCountryDataById:pid];
        if (country == nil) {
            break;
        }
        [tmpArray insertObject:country atIndex:0];
        if (country.pid == 0) {
            hasParent = NO;
        }
        pid = country.pid;
    }
    return tmpArray;
}

// 获取职位名称
- (NSString*)getJobName:(NSString*)jobId langage:(BOOL)isChinese {
    
    NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where job_id = %@ ",@"profes",jobId];
    __block NSString *resultString = @"";
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet * rs=[db executeQuery:selectSql];
        
        while ([rs next]) {
            if (isChinese) {
                resultString = [rs stringForColumn:@"job_name"];
            }else{
                resultString = [rs stringForColumn:@"job_en_name"];
            }
            break;
        }
        [rs close];
    }];

    return resultString;
}

//获取一级职业
//- (NSArray *)getOneLevelOccupation:(BOOL)isChinese{
//    NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where job_itype = 0 ",@"profes"];
//    __block NSMutableArray* occupationArray = [[NSMutableArray alloc] initWithCapacity:0];
//    [self.dbQueue inDatabase:^(FMDatabase *db) {
//        FMResultSet * rs=[db executeQuery:selectSql];
//        while ([rs next]) {
//            OccupationModel *occupationModel = [[OccupationModel alloc] init];
//            if (isChinese) {
//                occupationModel.job_name = [rs stringForColumn:@"job_name"];
//                occupationModel.job_level = [rs intForColumn:@"job_level"];
//                occupationModel.job_id = [rs intForColumn:@"job_id"];
//            }else{
//                occupationModel.job_en_name = [rs stringForColumn:@"job_en_name"];
//                occupationModel.job_level = [rs intForColumn:@"job_level"];
//                occupationModel.job_id = [rs intForColumn:@"job_id"];
//            }
//            [occupationArray addObject:occupationModel];
//        }
//        [rs close];
//    }];
//    return occupationArray;
//}

//获取下一级职业名称
-(NSArray *)getNextLevelOccupationNameByJoblevel:(NSInteger)jobLevel language:(BOOL)isChinese{
//    NSString * selectSql=[NSString stringWithFormat:@"select * from %@ where job_parent = %@ ",@"profes",@(jobLevel)];
//    __block NSMutableArray* occupationNameArray = [[NSMutableArray alloc] initWithCapacity:0];
//    [self.dbQueue inDatabase:^(FMDatabase *db) {
//        FMResultSet * rs=[db executeQuery:selectSql];
//        while ([rs next]) {
//            OccupationModel *occupationModel = [[OccupationModel alloc] init];
//            if (isChinese) {
//                occupationModel.job_name = [rs stringForColumn:@"job_name"];
//                occupationModel.job_level = [rs intForColumn:@"job_level"];
//                occupationModel.job_id = [rs intForColumn:@"job_id"];
//            }else{
//                occupationModel.job_en_name = [rs stringForColumn:@"job_en_name"];
//                occupationModel.job_level = [rs intForColumn:@"job_level"];
//                occupationModel.job_id = [rs intForColumn:@"job_id"];
//            }
//            [occupationNameArray addObject:occupationModel];
//        }
//        [rs close];
//    }];
//    return occupationNameArray;
    return nil;
}

#pragma mark - 读取当前城市Id
- (NSString *)readCurrentCityId
{
//    CountryCodeModel *current = [[CountryHelper sharedDataBase] getCountryCodeByAbbreviation:AppCurrentCity];
//    NSString *currentId = [NSString stringWithFormat:@"%@",@(current.currentId)];
//    return currentId;
    return @"";
}

#pragma mark - 读取当前国家的国家码
- (NSString *)readCurrentCountryCode
{
    // 获取国家码
    CountryCodeModel *countryCode = [[CountryHelper sharedDataBase] readCountryCodeCache];
    if (!countryCode.code) {
        countryCode.code = @"86";
    }
    
    return countryCode.code;
}

#pragma mark - 读取数据库中的国家编号
- (NSInteger)readCountryIdWithCountryName:(NSString *)countryName
{
    if (!countryName) {
        return 0;
    }

    NSString *sql = [NSString stringWithFormat:@"select * from %@ where level = '1' and (cs_name like '%%%@%%' or en_name like '%%%@%%')", CountryName, countryName, countryName];
    MLog(@"%@",sql);

    __block NSInteger countryId = 1;
    
    [self.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        FMResultSet *rs = [db executeQuery:sql];
        
        while ([rs next]) {
            countryId = [rs intForColumn:@"id"];
        }
        
        [rs close];
    }];
    
    return countryId;
}

-(NSString *)readCountryCodeWitchCountryName:(NSString *)countryName countryID:(NSInteger)countryId{
    if (!countryName) {
        return nil;
    }
    
    __block NSString *countryCode = @"";
    NSString *sql = [NSString stringWithFormat:@"select * from %@ where cs_name = '%@' or en_name = '%@'",SqliteName,countryName,countryName];
    [self.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
          FMResultSet *rs = [db executeQuery:sql];
        while ([rs next]) {
            countryCode = [rs stringForColumn:@"is_code"];
        }
        [rs close];
    }];
    
    return countryCode;
    
}

@end
