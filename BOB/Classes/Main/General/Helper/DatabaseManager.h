//
//  DatabaseManager.h
//  MoPal_Developer
//
//  Created by 王 刚 on 14/12/25.
//  Copyright (c) 2014年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
@class FMDatabaseQueue;
#import "FMDatabase.h"
@interface DatabaseManager : NSObject {
    FMDatabaseQueue *dbQueue;
}

@property (nonatomic, strong) FMDatabaseQueue *dbQueue;
@property (nonatomic, strong) FMDatabase *db;

/**
 *  初始化城市数据库
 */
- (id)initCountryDataBase;

/**
 * 初始化表格式,需要具体子类去实现
 */
- (void)initTable ;
/**
 * 获取数据库表名,此函数需要具体子类去实现
 */
- (NSString *)getTableName ;
/**
 * 插一个实体对象到表中,此函数需要具体子类去实现
 */
- (BOOL)insertAt:(id)pojo;


/**
 * 给SQL语句设置表名
 */
-(NSString *)setTable:(NSString *)sql;
/**
 * 批量插入实体对象
 */
- (BOOL)insertWithArray:(NSArray *)pojoArray ;
/**
 * 删除指定ID的数据
 */
- (BOOL)deleteAt:(int)index;
/**
 * 删除表中所有数据
 */
- (BOOL)deleteAll ;

@end