//
//  CountryHelper.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/1/29.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseManager.h"
#import "CountryCodeModel.h"


@interface CountryHelper : DatabaseManager

+(CountryHelper*)sharedDataBase;

//@property (nonatomic, retain) FMDatabase * fmdb;//操作数据库指针
@property (nonatomic, retain) NSMutableArray* selectedCountrys;

//读取手机城市列表
-(NSMutableArray*)readMobileCountryDataFromDB;
-(CountryCodeModel*)selectCountryCode:(NSString*) country_code;

//根据国家码读取
-(CountryCodeModel*)selectCountryDataByCountryCode:(NSString*) countryCode;

//读取国家城市列表
-(NSMutableArray*)readCountryDataByPid:(NSInteger)pid;
-(NSMutableArray*)readCountryDataByLevel:(NSInteger)level;
//读取城市
-(CountryCodeModel*)readCountryDataById:(NSInteger)_id;

- (NSMutableArray *)readBussnessDataByPid:(NSInteger)pId;
- (CountryCodeModel*)getBussnissByBid:(NSInteger)bid;

-(CountryCodeModel*)readCountryCodeCache;

+(NSString*)countryCodeParamString:(NSArray *)countryArray;

+(BOOL)isChina;

+(NSString*)composeShowCountryName:(NSArray*) countrys;

// 获取省市
- (NSString*)getProviceCityString:(NSString*)cityId langage:(NSInteger)langage;
/**
 *  根据传入的地区码反查城市名称
 *
 *  @param areaCode 地区码以"|”分割,level为固定的"国-省-市”
 *  @param langage  语言类型
 *
 *  @return 返回的"国-省-市”
 */
- (NSString *)getCountryProviceCityString:(NSString *)areaCode langage:(NSInteger)langage;
/**
 *  返回格式化的国家、省、市的显示方式，如果是直辖市显示"国-省"，否则显示"省-市"
 *
 *  @param areaCode 地区码以"|”分割,level为固定的"国|省|市”
 *  @param langage  语言类型
 *
 *  @return 返回"国-省"(为直辖市)或"省-市"(一般)
 */
- (NSString *)getShowFormatCountryProviceCity:(NSString *)areaCode langage:(NSInteger)langage;

// 通过市简写得到countrymodel
-(CountryCodeModel*)getCountryCodeByAbbreviation:(NSString*)name;

-(NSMutableArray*)getCountrysByAbbreviation:(CountryCodeModel*)model;

// 获取职位名称
- (NSString*)getJobName:(NSString*)jobId langage:(BOOL)isChinese;

//获取一级职业名称
- (NSArray *)getOneLevelOccupation:(BOOL)isChinese;

//根据一级名称得到二级名称
- (NSArray *)getNextLevelOccupationNameByJoblevel:(NSInteger)jobLevel language:(BOOL)isChinese;

-(NSMutableArray*)sortCategory:(NSMutableArray*) countryCodeArray;

// 根据PId查询城市列表
-(NSMutableArray*)getCountryCodeByPid:(NSInteger)pid;

/**
 *  读取当前城市Id
 *
 *  @return 城市id
 */
- (NSString *)readCurrentCityId;

/**
 *  读取当前国家的国家码
 *
 *  @return 当前国家的国家码
 */
- (NSString *)readCurrentCountryCode;

/**
 *  读取数据库中的国家编号
 *
 *  @param countryName   国家名字
 *
 *  @return 国家编号
 */
- (NSInteger)readCountryIdWithCountryName:(NSString *)countryName;

/// 根据国家名称和国家编号获得国家简写
-(NSString *)readCountryCodeWitchCountryName:(NSString *)countryName countryID:(NSInteger)countryId;
@end
