//
//  EntityFactory.h
//  MoPromo
//  实体工厂
//  此类只提供所有项目中使用的静态类获取及翻释放,
//  项目规定,在普通类中,对于大对象(除基础类型之外的变量),不请允许出现静态变量的定义,统一在此定义及生成管理
//  Created by liuxz on 14-5-29.
//  Copyright (c) 2014年 MOPromo. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HanyuPinyinOutputFormat;
@class FMDatabaseQueue;
@interface EntityFactory : NSObject

/**
 *  获取缓存的控制器集合,KEY为网址域,VALUE为AFHTTPRequestOperationManager对象
 * @return 网络访问控制器缓存字典
 **/
+ (NSMutableDictionary *)httpClientDic ;

///**
// *
// * @return 拼音和汉字转换对象
// **/
//+ (HanyuPinyinOutputFormat *)outputHanyuPinyinFormat ;
/**
 * 应用的缓存数据库对象
 */
+ (FMDatabaseQueue *)mxAppCacheDB ;

+ (FMDatabaseQueue *)mxAppCacheDB:(NSString *)path;


/**
 *  清除缓存,当系统内存不够时,可以调用此函数释放常注内存的对象,尽可能减少内存占用
 **/
+ (void)clearCache ;
///**
// * 定位对象
// */
//+(CLLocationManager*)mxCllocationObject;

@end
