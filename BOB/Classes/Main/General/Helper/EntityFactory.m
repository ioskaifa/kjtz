//
//  EntityFactory.m
//  MoPromo
//  实体工厂
//  此类只提供所有项目中使用的静态类获取及翻释放,
//  项目规定,在普通类中,对于大对象(除基础类型之外的变量),不请允许出现静态变量的定义,统一在此定义及生成管理
//  Created by liuxz on 14-5-29.
//  Copyright (c) 2014年 MOPromo. All rights reserved.
//

#import "EntityFactory.h"
#import "DocumentManager.h"
#import "FMDatabaseQueue.h"
static NSMutableDictionary *_httpClientDic=nil;

static HanyuPinyinOutputFormat *outputHanyuPinyinFormat = nil;

static FMDatabaseQueue *mxAppDb = nil ;


@implementation EntityFactory

+ (NSMutableDictionary *)httpClientDic {
    if (_httpClientDic==nil) {
        _httpClientDic = [NSMutableDictionary dictionaryWithCapacity:2];
    }
    return _httpClientDic;
}

+ (FMDatabaseQueue *)mxAppCacheDB {
    if (mxAppDb) {
        return mxAppDb;
    }
    @try {
        NSString* rootPath = [DocumentManager cacheDocDirectory];
       [[NSFileManager defaultManager] createDirectoryAtPath:rootPath withIntermediateDirectories:YES attributes:nil error:nil];
        NSString* fullPath = [rootPath stringByAppendingPathComponent:@"moPaldeveloper.db"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if(![fileManager fileExistsAtPath:fullPath])
        {
            Boolean b =  [fileManager createFileAtPath:fullPath contents:nil attributes:nil];
            if (!b) {
                return nil;
            }
        }
        mxAppDb = [FMDatabaseQueue databaseQueueWithPath:fullPath];
        return mxAppDb;
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}

+ (FMDatabaseQueue *)mxAppCacheDB:(NSString *)path
{
    if (!path) {
        return nil;
    }
    
    FMDatabaseQueue *mxcitydb = [FMDatabaseQueue databaseQueueWithPath:path];
    return mxcitydb;
}

//此处只是清除内存
+ (void)clearCache {
    @synchronized(_httpClientDic){
        [_httpClientDic removeAllObjects];
        _httpClientDic = nil;
    }
  
//    @synchronized(cacheDocDirectory){
//        cacheDocDirectory = nil;
//    }
    @synchronized(mxAppDb){
        
        @try {
            [mxAppDb close];
        }
        @catch (NSException * e) {
        }
        @finally {
            mxAppDb = nil;
        }
    }
}

#pragma mark -
// 不对外暴露的方法,初始化缓存数据库中的表
//+(void)initTables:(FMDatabase*)db {
//    
////    NSString* sql1=@"CREATE TABLE IF NOT EXISTS group_table(id integer  PRIMARY KEY AUTOINCREMENT,gid VARCHAR DEFAULT NULL, group_name_en VARCHAR DEFAULT NULL,group_name_cs VARCHAR  DEFAULT NULL,group_name VARCHAR  DEFAULT NULL,defaultTypes integer DEFAULT NULL,current_mx_id VARCHAR DEFAULT NULL);";
////    
////    if (![db executeUpdate:sql1]) {
////        DDLogInfo(@"数据库打开失败");
////    }
////    
////    
////    //}
////    //为数据库设置缓存，提高查询效率
////    [db setShouldCacheStatements:YES];
//
//}

//+(CLLocationManager*)mxCllocationObject{
//    if (mxLocation == nil) {
//        mxLocation = [[CLLocationManager alloc] init];
//        mxLocation.distanceFilter = 100.0f;
//        mxLocation.desiredAccuracy = kCLLocationAccuracyBest;
//    }
//    return mxLocation;
//}
@end
