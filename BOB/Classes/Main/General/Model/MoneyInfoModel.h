//
//  MoneyInfoModel.h
//  BOB
//
//  Created by AlphaGo on 2020/2/14.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface MoneyInfoModel : BaseObject
@property (nonatomic, strong) NSString * amount;
@property (nonatomic, strong) NSString * available;
@property (nonatomic, strong) NSString * frozen;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * price;
@end

NS_ASSUME_NONNULL_END
