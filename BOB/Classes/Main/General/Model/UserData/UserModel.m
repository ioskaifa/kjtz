//
//  UserModel.m
//  MoPal_Developer
//
//  Created by 王 刚 on 15/2/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import "UserModel.h"

static NSString * const UserModelCacheKey = @"UserModelCacheKey";

@interface UserModel ()

@end

@implementation UserModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"user_id" : @"id",
             @"head_photo" : @"pic",
             @"user_email":@"email",
             @"user_tel":@"mobile"
    };
}

- (NSString *)format_user_id {
    return [StringUtil formatUserID:self.user_id];
}

- (NSString *)roomNum {
    NSInteger num = (NSInteger)(100 + (arc4random() % ((100 - 200 )+ 1)));
    return StrF(@"%@%ld", self.user_id, num);
}

- (NSString *)formatSex {
    if ([@"1" isEqualToString:self.sex]) {
        return @"男";
    } else {
        return @"女";
    }
}

- (NSString *)smartName {
    return (_nickname.length > 0) ? _nickname : _account;
}

- (void)saveModelToCache{
    NSData* data = [NSKeyedArchiver archivedDataWithRootObject:self];
    if (data) {
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:UserModelCacheKey];
    }
}

+ (UserModel*)modelFromCache{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:UserModelCacheKey];
    if (!data) {
        return nil;
    }
    UserModel *model = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (model && [model isKindOfClass:[UserModel class]]) {
        return model;
    }
    return nil;
}

+ (void)removeFromCache{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UserModelCacheKey];
}

- (void)updateToken:(NSString *)newToken {
    if ([StringUtil isEmpty:newToken]) {
        return;
    }
    _token = newToken;
}

- (void)updateUserId:(NSString *)newUserId {
    if ([StringUtil isEmpty:newUserId]) {
        return;
    }
    self.user_id = newUserId;
}

-(FriendModel* )toFriendModel {
    FriendModel* model = [[FriendModel alloc]init];
    model.userid = self.user_id;
    model.name = self.user_name;
    model.phone = self.user_tel;
    model.avatar = self.head_photo;
    //model.followState = [self.status intValue];
    return model;
}

-(NSDictionary*)generateChatParam{
    return @{@"user_id":self.user_id,
             @"sys_user_account":self.account,
             @"head_photo":self.head_photo,
             @"register_type":self.user_tel.length > 0 ? @"1" : @"2",
             @"user_tel":self.user_tel,
             @"user_email":self.user_email,
             @"sys_system_type":@"chat"
    };
}

- (MoneyInfoModel *)fbModel:(NSString *)coinName {
    for(MoneyInfoModel *model in self.infos_fb) {
        if([model.name isEqualToString:coinName]) {
            return model;
        }
    }
    return nil;
}

- (NSString *)zhibo_account {
    return [NSString stringWithFormat:@"%@_%@",@"clsa",self.user_uid];
}

- (NSString *)zhibo_password {
    NSString *password = [NSString stringWithFormat:@"%@_%@_-forlive",@"clsa",self.user_uid];
    return [StringUtil calcMD5forString:password];
}

- (BOOL)isSevice {
    return [_is_sevice isEqualToString:@"1"];
}
@end
