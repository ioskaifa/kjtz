//
//  UserModel.h
//  MoPal_Developer
//
//  Created by 王 刚 on 15/2/26.
//  Copyright (c) 2015年 MoXian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelProtocol.h"
#import "BaseObject.h"
#import "EnumDefine.h"
#import "FriendModel.h"
#import "MoneyInfoModel.h"
#import "WalletInfoObj.h"

@class MoYouModel;
@class MXWalletInfoModel;

typedef void(^UpdateCompletionBlock)(BOOL success);

#define MoCustomerUserId    @"100001"

@interface UserModel : BaseObject<ModelProtocol>

// token
@property (nonatomic, copy) NSString* token;

@property (nonatomic, copy) NSString* chatToken;

@property (nonatomic, copy) NSString* chatUser_id;
/// 语音通话 服务器地址
@property (nonatomic, copy) NSString* jitsiServer;
/// 语音聊天房间号
@property (nonatomic, copy) NSString* roomNum;
// 用户头像
@property (nonatomic ,copy) NSString* head_photo;
@property (nonatomic ,copy) NSString* circle_back_img;

@property (nonatomic ,copy) NSString* register_type;

///是否是客服
@property (nonatomic ,copy) NSString* is_sevice;
@property (nonatomic ,assign) BOOL isSevice;

@property (nonatomic ,copy) NSString* user_tel;

@property (nonatomic ,copy) NSString* user_email;

@property (nonatomic ,copy) NSString* account;
///用户ID
@property (nonatomic, copy) NSString* user_id;
///用户UID
@property (nonatomic, copy) NSString* user_uid;

///是否注册过小直播后台
@property (nonatomic, copy) NSString* is_register_live;

///小直播账号 --- cls+'_'+uid
@property (nonatomic, copy, readonly) NSString* zhibo_account;
///小直播密码
@property (nonatomic, copy, readonly) NSString* zhibo_password;

@property (nonatomic, copy) NSString* format_user_id;

@property (nonatomic, copy) NSString* qiniu_domain;
// 性别 1男 2女
@property (nonatomic ,copy) NSString* sex;
@property (nonatomic ,copy) NSString* formatSex;
///是否通知 0-否 1-是
@property (nonatomic , assign) BOOL is_notify;
///是否声音 0-否 1-是
@property (nonatomic , assign) BOOL is_voice;
///是否震动 0-否 1-是
@property (nonatomic , assign) BOOL is_vibrate;
//二维码
@property (nonatomic, copy) NSString *qr_code_url;
//区域
@property (nonatomic, strong) NSString* area;


@property (nonatomic ,copy) NSString* user_name;
// 昵称
@property (nonatomic ,copy) NSString* nickname;

// 优先返回昵称,没有昵称再返回account
@property (nonatomic ,copy) NSString* smartName;

@property (nonatomic ,copy) NSString* remark;
//个人签名
@property (nonatomic ,copy) NSString* sign_name;

//密码
@property (nonatomic, copy) NSString *password;

@property (nonatomic, copy) NSString *pay_password;

//验证类型
@property (nonatomic, assign) NSInteger verifyType;

@property (nonatomic, assign) NSInteger totalInvest;

@property (nonatomic, assign) NSInteger levelInvest;
//级别 0 普通 1 vip 2 超级vip
@property (nonatomic, assign) NSInteger level;
//魔贝
@property (nonatomic, assign) NSInteger lockNum;
//红贝
@property (nonatomic, assign) NSInteger flowNum;
//蓝贝
@property (nonatomic, assign) NSInteger repeatNum;
//通证
@property (nonatomic, assign) NSInteger tokenNum;
//通证锁仓
@property (nonatomic, assign) NSInteger tokenLockNum;
//可售额度
@property (nonatomic, assign) NSInteger saleNum;
//可用可售额度
@property (nonatomic, assign) NSInteger saleUseNum;
//激活码
@property (nonatomic, assign) NSInteger activeNum;
//魔晶石
@property (nonatomic, assign) NSInteger stoneNum;
//诚信值
@property (nonatomic, assign) NSInteger credit;
//贡献值
@property (nonatomic, assign) NSInteger devote;
//今天的动态收益
@property (nonatomic, assign) NSInteger quicken;
//昨天的动态收益
@property (nonatomic, assign) NSInteger quickenAgo;
//今日收益状态 0已拿 1未拿
@property (nonatomic, assign) NSInteger isGet;
//用户状态 0正常 1冻结
@property (nonatomic, assign) NSInteger userStatus;
//账号状态 0未激活 1已激活
@property (nonatomic, assign) NSInteger accountStatus;
//激活时间
@property (nonatomic, strong) NSString *activeTime;
//注册时间
@property (nonatomic, copy)   NSString *createTime;

// 暂时不知道是何含义
@property (nonatomic, strong) NSString *contactMobile;

@property (nonatomic, copy)   NSString *realname;

@property (nonatomic, assign) NSInteger isStoneGet;
//可售额度锁仓
@property (nonatomic, assign) NSInteger saleLockNum;
//可售额度钱包
@property (nonatomic, assign) NSInteger saleLockWatNum;

@property (nonatomic, copy)   NSString *idcard;

@property (nonatomic, copy)   NSString *updateTime;

@property (nonatomic, assign) NSInteger freeze;

@property (nonatomic, assign) NSInteger balance;

@property (nonatomic, strong) NSString* avatar;

@property (nonatomic, strong) NSString* webscoket_path;

@property (nonatomic, strong) NSString* total_bb_money;

@property (nonatomic, strong) NSString *grade;
///钱包地址
@property (nonatomic, copy) NSString *sla_address;
@property (nonatomic, copy) NSString *eth_address;

//是否有权限转发
@property (nonatomic, copy) NSString *is_group_send;

@property (nonatomic) NSInteger is_chat;
//实名认证
@property (nonatomic, copy) NSString *auth;

@property (nonatomic, copy) NSString *invite_url;

@property (nonatomic, strong) NSArray<MoneyInfoModel *> *infos_fb;
///客户端登录授权码
@property (nonatomic, copy) NSString *client_authCode;
///客户端登录类型
@property (nonatomic, copy) NSString *client_type;
///客户端登录状态
@property (nonatomic, assign) BOOL client_status;
///客户端登录 手机是否静音
@property (nonatomic, assign) BOOL client_silenceStatus;

///积分
@property (nonatomic, assign) double score_num;
///余额
@property (nonatomic, assign) double balance_num;
///SLA数量
@property (nonatomic, assign) double sla_num;
///冻结SLA数量
@property (nonatomic, assign) double freeze_sla_num;
///钱包信息相关
@property (nonatomic, strong) WalletInfoObj *walletInfoObj;
///用户等级
@property (nonatomic, strong) NSString *user_grade;
///直推人数
@property (nonatomic, strong) NSString *referer_num;
///团队人数
@property (nonatomic, strong) NSString *under_num;
///是否设置支付密码
@property (nonatomic, assign) BOOL isSetPayPassword;
///赠送状态（0：未赠送  1：已赠送），0的时候属于首次登录，需要弹领取礼包
@property (nonatomic, assign) BOOL give_status;
///赠送SLA数量
@property (nonatomic, copy) NSString * give_sla_num;


- (void)updateToken:(NSString *)newToken;

- (void)updateUserId:(NSString *)newUserId;

+ (void)removeFromCache;

+ (UserModel*)modelFromCache;

- (void)saveModelToCache;

-(FriendModel* )toFriendModel;

-(NSDictionary*)generateChatParam;

- (MoneyInfoModel *)fbModel:(NSString *)coinName;

 

@end
