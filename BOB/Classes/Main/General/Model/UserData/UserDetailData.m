//
//  UserDetailData.m
//  RatelBrother
//
//  Created by mac on 2020/5/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "UserDetailData.h"
@implementation UserDetailData
singleton_implementation(UserDetailData)

-(void)updateUserModel:(UserModel*)model{
    if (model == nil) {
        _user = nil;
        [UserModel removeFromCache];
    } else {
        _user = model;
        [model saveModelToCache];
    }
}

-(UserModel*)user{
    if (!_user) {
        _user = [UserModel modelFromCache];
    }
    return _user;
}


- (void)get_userinfo:(nullable MXHttpRequestCallBack)completion{
    if ([StringUtil isEmpty:self.user.token]) {
        return;
    }
    NSString *url = [NSString stringWithFormat:@"%@/api/user/info/getRealUserInfo",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).useEncrypt().params(@{})
        .finish(^(id data){
            NSDictionary* dict = (NSDictionary*)data;
            if ([dict[@"success"] boolValue]) {
                NSDictionary *dataDict = dict[@"data"][@"userInfo"];
                if (dataDict[@"sex"]) {
                    UDetail.user.sex = [dataDict[@"sex"] description];
                }
                if (dataDict[@"area"]) {
                    UDetail.user.area = [dataDict[@"area"] description];
                }
                if (dataDict[@"user_tel"]) {
                    UDetail.user.user_tel = [dataDict[@"user_tel"] description];
                }
                if (dataDict[@"head_photo"]) {
                    UDetail.user.head_photo = [dataDict[@"head_photo"] description];
                }
                if (dataDict[@"nick_name"]) {
                    UDetail.user.nickname = [dataDict[@"nick_name"] description];
                }
                if (dataDict[@"is_vibrate"]) {
                    UDetail.user.is_vibrate = [dataDict[@"is_vibrate"] boolValue];
                }
                if (dataDict[@"is_voice"]) {
                    UDetail.user.is_voice = [dataDict[@"is_voice"] boolValue];
                }
                if (dataDict[@"is_notify"]) {
                    UDetail.user.is_notify = [dataDict[@"is_notify"] boolValue];
                }
                if (dataDict[@"id"]) {
                    UDetail.user.user_id = [dataDict[@"id"] description];
                }
                if (dataDict[@"uid"]) {
                    UDetail.user.user_uid = [dataDict[@"uid"] description];
                }
                if (dataDict[@"score_num"]) {
                    UDetail.user.score_num = [dataDict[@"score_num"] doubleValue];
                }
                if (dataDict[@"balance_num"]) {
                    UDetail.user.balance_num = [dataDict[@"balance_num"] doubleValue];
                }
                if (dataDict[@"eth_address"]) {
                    UDetail.user.eth_address = [dataDict[@"eth_address"] description];
                }
                if (dataDict[@"user_grade"]) {
                    UDetail.user.user_grade = [dataDict[@"user_grade"] description];
                }
                if (dataDict[@"under_num"]) {
                    UDetail.user.under_num = [dataDict[@"under_num"] description];
                }
                if (dataDict[@"referer_num"]) {
                    UDetail.user.referer_num = [dataDict[@"referer_num"] description];
                }
                if (dataDict[@"sla_num"]) {
                    UDetail.user.sla_num = [dataDict[@"sla_num"] doubleValue];
                }
                if (dataDict[@"freeze_sla_num"]) {
                    UDetail.user.freeze_sla_num = [dataDict[@"freeze_sla_num"] doubleValue];
                }
                [UDetail updateUserModel:UDetail.user];
                Block_Exec(completion,YES,nil);
            } else {
                [UDetail updateUserModel:nil];
                [NotifyHelper showMessageWithMakeText:dict[@"msg"]];
                Block_Exec(completion,NO,nil);
            }
        }).failure(^(id error){
            Block_Exec(completion,NO,error);
            
            [NotifyHelper showMessageWithMakeText:error];
        })
        .execute();
    }];
}

- (void)updateWallet:(nullable MXHttpRequestObjectCallBack)block {
    NSString *url = [NSString stringWithFormat:@"%@/api/user/info/getUserWalletInfo",LcwlServerRoot];
    [MXNet Post:^(MXNet *net) {
        net.apiUrl(url).params(nil)
        .finish(^(id data){
            if ([data[@"success"] boolValue]) {
                WalletInfoObj *obj = [WalletInfoObj modelParseWithDict:data[@"data"][@"userInfo"]];                
                UDetail.user.walletInfoObj = obj;
            }
            Block_Exec(block, data, nil);
        }).failure(^(id error){
            Block_Exec(block,nil, error);
        })
        .execute();
    }];
}

@end
