//
//  WalletInfoObj.h
//  BOB
//
//  Created by mac on 2020/9/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import "BaseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletInfoObj : BaseObject
///积分
@property (nonatomic , assign) NSInteger              score_num;
///余额
@property (nonatomic , assign) NSInteger              balance_num;
///用户ID
@property (nonatomic , assign) NSInteger              ID;
///直推人数
@property (nonatomic , assign) NSInteger              referer_num;
///用户等级
@property (nonatomic , assign) NSInteger              user_grade;
///伞下人数
@property (nonatomic , assign) NSInteger              under_num;
///商城打折比例
@property (nonatomic , assign) CGFloat                mall_discount_rate;
///钱包地址
@property (nonatomic , copy) NSString                 *eth_address;

@end

NS_ASSUME_NONNULL_END
