//
//  UserDetailData.h
//  RatelBrother
//
//  Created by mac on 2020/5/18.
//  Copyright © 2020 AlphaGo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"
#import "Singleton.h"
#import "MXHttpCallBackConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserDetailData : NSObject
singleton_interface(UserDetailData)

@property (strong, nonatomic) UserModel *user;

-(void)updateUserModel:(nullable UserModel*)model;

- (void)get_userinfo:(nullable MXHttpRequestCallBack)completion;

- (void)updateWallet:(nullable MXHttpRequestObjectCallBack)block;
    
@end

NS_ASSUME_NONNULL_END
