//
//  AppDelegate.h
//  AdvertisingMaster
//
//  Created by mac on 2019/4/5.
//  Copyright © 2019 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXRouter.h"
#import "MainViewController.h"
#import "ChatTabBar.h"
#import "BaseNavigationController.h"
//#import "StartPageVC.h"
#import "UserModel.h"

// 上线的时候要关闭
#define OpenDebugLcwl 1

// 打开日志,上线的时候要关闭

#define Open_Log_Debug 1

//客服开发中，未完成上线要暂时关闭
#define Kefu_Server_dev 1


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIView *subWindow; 

@property (nonatomic, strong) MXRouter *router;
@property (nonatomic, strong) BaseTabBar *mainVC;
@property (nonatomic, strong) BaseTabBar *mallVC;

@property (nonatomic,assign) BOOL isKickOffProcessed;

@end

